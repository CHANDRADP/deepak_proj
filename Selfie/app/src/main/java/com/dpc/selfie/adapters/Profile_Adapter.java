package com.dpc.selfie.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dpc.selfie.Act.Activity_Profile;
import com.dpc.selfie.R;
import com.dpc.selfie.Utils.DB;
import com.dpc.selfie.customs.CircleImage;
import com.dpc.selfie.models.Profile_Item;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by D-P-C on 11-Mar-15.
 */
public class Profile_Adapter extends RecyclerView.Adapter<Profile_Adapter.ViewHolder> {

    private Context context;

    private DB db;

    public Profile_Adapter(Context context) {
        this.context = context;
        db = new DB(context);

    }


    @Override
    public int getItemCount() {
        if(Activity_Profile.selfies == null)
            return 0;
        else if(Activity_Profile.selfies.size() > 4)
            return 4;
        else
            return Activity_Profile.selfies.size();
    }
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        if(Activity_Profile.selfies.get(i).getImage_url() != null) {
            display_image(Activity_Profile.selfies.get(i).getImage_url(), viewHolder.icon,false);
            return;
        }
        else if(Activity_Profile.selfies.get(i).getImage() != -1) {
            viewHolder.icon.setBackgroundResource(Activity_Profile.selfies.get(i).getImage());
            return;
        }



    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup,final int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_badge_details, viewGroup, false);

        return new ViewHolder(itemView);
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        protected ImageView icon;



        public ViewHolder(View v) {
            super(v);
            icon = (ImageView)v.findViewById(R.id.icon);



        }

    }

    private void display_image(String url,View v,boolean is_circle_image)
    {

        try {
            Picasso.with(context)
                    .load(url)
                    .placeholder(R.drawable.ic_action_person)
                    .error(R.drawable.ic_action_person)
                    .into(is_circle_image ? (CircleImage)v : (ImageView)(v));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

}