package com.dpc.selfie.models;

/**
 * Created by D-P-C on 25-Mar-15.
 */
public class Celebrity {

    private String[] gen = new String[]{"", "Male", "Female"};
    private long id = 0;
    private String f_name = null,m_name = null,l_name = null,status = null,dob = null,image = null,profession;
    private int gender = 0,follow_count = 0;
    private boolean is_following = false;
    private String date;

    public Celebrity(long id, String f_name, String m_name, String l_name, String status, String dob, String image, int gender, int follow_count, boolean is_following,String profession,String date) {
        this.id = id;
        this.f_name = f_name;
        this.m_name = m_name;
        this.l_name = l_name;
        this.status = status;
        this.dob = dob;
        this.image = image;
        this.gender = gender;
        this.follow_count = follow_count;
        this.is_following = is_following;
        this.profession = profession;
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getM_name() {
        return m_name;
    }

    public void setM_name(String m_name) {
        this.m_name = m_name;
    }

    public String getL_name() {
        return l_name;
    }

    public void setL_name(String l_name) {
        this.l_name = l_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public int getFollow_count() {
        return follow_count;
    }

    public void setFollow_count(int follow_count) {
        this.follow_count = follow_count;
    }

    public boolean isIs_following() {
        return is_following;
    }

    public void setIs_following(boolean is_following) {
        this.is_following = is_following;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getGenderName() {
        return gen[gender];
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }
}
