package com.dpc.selfie.Act;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.dpc.selfie.App;

import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.DB;
import com.dpc.selfie.Utils.JSONParser;

import com.dpc.selfie.Utils.LinePageIndicator;
import com.dpc.selfie.adapters.ViewPagerAdapter;
import com.dpc.selfie.customs.BaseActivity;
import com.dpc.selfie.customs.MyProgress;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.Celebrity;
import com.dpc.selfie.models.Server_Params;
import com.dpc.selfie.models.User;

import com.dpc.selfie.tasks.Update_App_Users;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by D-P-C on 30-Jan-15.
 */
public class Activity_Login extends BaseActivity {

    public static final String BROADCAST_RECEIVER = "dpc.fb_login";

    private MyProgress progressDialog;

    private ViewPager viewPager;
    private PagerAdapter adapter;
    int[] t_image;
    private Button btnSignIn, btnLogin, btnSignup;
    LinePageIndicator mIndicator;
    TextView txtTermsConditions;


    private void show_dialog()
    {
        if(progressDialog == null)
        progressDialog = new MyProgress(this);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    private void close_dialog()
    {
        if (progressDialog != null)
            progressDialog.dismiss();
    }
    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (intent.hasExtra(Params.STATUS.get_value())) {
                    //update the UI
                    close_dialog();
                    if (intent.getBooleanExtra(Params.STATUS.get_value(), false)) {

                        if (App.getMyId() > 0) {

                            startActivity(new Intent(Activity_Login.this, Activity_Feeds.class));
                            finish();


                        }


                    } else {
                        C.alert(getApplicationContext(),"Something went wrong, please try again");

                    }
                }
            }
        }
    };

    Request.Callback friends_callback = new Request.Callback() {
        public void onCompleted(Response response) {
            GraphObject graphResponse = response
                    .getGraphObject();

            if (graphResponse != null) {

                FacebookRequestError error = response.getError();
                if (error != null) {
                    return;
                }

                JSONObject object = graphResponse.getInnerJSONObject();

                JSONArray users_array;
                try {
                    users_array = object.getJSONArray(Params.DATA.get_value());
                    ArrayList<Long> ids = new ArrayList<>();

                   // ids.add(db.Get_User_Details(App.getMyId()).getFb_id());
                    for (int i = 0; i < users_array.length(); i++) {
                        JSONObject object1 = users_array.getJSONObject(i);

                        ids.add(object1.getLong(Params.ID.get_value()));
                        print(getApplicationContext(), object1.toString());
                    }

                    if (ids.size() > 0)
                        new Update_App_Users(getApplicationContext(), ids, true).execute();
                    else {
                        close_dialog();
                        if (App.getMyId() > 0) {

                            startActivity(new Intent(Activity_Login.this, Activity_Feeds.class));
                            finish();
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);






        if(!App.is_gcm_updated())
            register();
        if (App.getMyId() > 0 && App.is_fb_loggedin()) {
            startActivity(new Intent(Activity_Login.this, Activity_Feeds.class));
            finish();
            return;
        }
        else if(App.getMyId() > 0 && !App.is_users_updated() && App.is_fb_loggedin())
        {
            Bundle get_params = new Bundle();
            get_params.putString("fields", "id,name,picture");
            final Request request = new Request(Session.getActiveSession(), "me/friends", get_params,
                    HttpMethod.GET, friends_callback);
            show_dialog();
            request.executeAsync();
            return;
        }
        else
        App.setMyId(-1);

        setContentView(R.layout.activity_login);


        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnSignup = (Button) findViewById(R.id.btnSignUp);

        LoginButton fb = (LoginButton) findViewById(R.id.fb);
        //btnSignIn = (Button) findViewById(R.id.btnSignIn);



        txtTermsConditions = (TextView) findViewById(R.id.txtTermsConditions);
        fb.setReadPermissions(Arrays.asList("public_profile", "user_friends",
                "email", "user_birthday"));
        fb.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (!net.isConnectingToInternet()) {
                    net.alert(getSupportFragmentManager());
                }

            }

        });

        txtTermsConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("http://www.google.com");
                Intent i = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(i);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                C.customLoginDialog(Activity_Login.this);

            }
        });

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                C.customSignUpDialog(Activity_Login.this);

            }
        });



       /*btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity_Login.this, Activity_Register.class);
                startActivity(intent);
            }
        });*/

        t_image = new int[] { R.drawable.tutorial_a, R.drawable.tutorial_b,
                R.drawable.tutorial_c, R.drawable.tutorial_d };
        // Locate the ViewPager in viewpager_main.xml
        viewPager = (ViewPager) findViewById(R.id.pager);
        // Pass results to ViewPagerAdapter Class
        adapter = new ViewPagerAdapter(Activity_Login.this, t_image);
        // Binds the Adapter to the ViewPager
        viewPager.setAdapter(adapter);

        // ViewPager Indicator
        mIndicator = (LinePageIndicator) findViewById(R.id.indicator);
        //mIndicator.setFades(false);
        mIndicator.setViewPager(viewPager);



    }




    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(broadcastReceiver, new IntentFilter(BROADCAST_RECEIVER));

    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(broadcastReceiver);
        uiHelper.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        close_dialog();
        if(uiHelper != null)
        uiHelper.onDestroy();
    }

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
        Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
        if (Session.getActiveSession() != null && Session.getActiveSession().isOpened()) {
            App.set_fb_login(true);
            Request.newMeRequest(Session.getActiveSession(), new Request.GraphUserCallback() {

                @Override
                public void onCompleted(GraphUser user, Response response) {

                    if (user != null) {
                        JSONObject obj = response.getGraphObject().getInnerJSONObject();
                        if (obj != null) {
                             String f_name = null, m_name = null, l_name = null, display_name = null, dob = null, email = null;
                            long fb_id = 0;
                            int gender = 0;


                            try {
                                //fb_id = Long.parseLong(user.getId());
                                f_name = user.getFirstName();
                                m_name = user.getMiddleName();
                                l_name = user.getLastName();
                                display_name = user.getName();
                                dob = user.getBirthday();

                                fb_id = obj.getLong(Params.ID.get_value());

                                if (!obj.isNull(Params.EMAIL.get_value())) {
                                    email = obj.getString(Params.EMAIL.get_value());
                                }
                                if (!obj.isNull(Params.GENDER.get_value())) {
                                    String sex = obj.getString(Params.GENDER.get_value());
                                    if (sex.equalsIgnoreCase(Params.MALE.get_value())) {
                                        gender = 1;
                                    } else {
                                        gender = 2;
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            User app_user = new User(0, fb_id, gender, 0, f_name, m_name, l_name, display_name, null, email, dob, null, null, null, null, null);
                            new Login(getApplicationContext(), app_user).execute();
                        }


                    }
                }
            }).executeAsync();
            show_dialog();


        }

    }

    ////////////////check user existence with facebook id or add user
    private class Login extends AsyncTask<String, Long, Boolean> {
        Context c;
        User user = null;

        public Login(Context c, User user) {
            this.c = c;
            this.user = user;

        }


        @Override
        protected Boolean doInBackground(String... params) {

            return check_user();
        }

        @Override
        protected void onPostExecute(Boolean result) {

            if(result)
            {
                Bundle get_params = new Bundle();
                get_params.putString("fields", "id,name,picture");
                final Request request = new Request(Session.getActiveSession(), "me/friends", get_params,
                        HttpMethod.GET, friends_callback);
                request.executeAsync();
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        public boolean check_user() {
            String token = Session.getActiveSession().getAccessToken();
            boolean success = false;
            JSONParser parser = new JSONParser();

            try {
                JSONObject response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                        new Server_Params().login(user,App.get_gcm_id(),token));
                if (response != null) {
                    if (response.has(Params.STATUS.get_value())) {
                        int status = response.getInt(Params.STATUS.get_value());
                        if (status == 200) {

                            user = new C().parse_user(response.getJSONObject(Params.DATA.get_value()).getJSONObject("user"));
                            if (user.getUserid() > 0) {

                                new DB(c).Sync_App_User(user);
                                App.setMyId(user.getUserid());

                                if (token != null) {
                                    App.setToken(token);
                                }
                                if(response.getJSONObject(Params.DATA.get_value()).has(Params.NEW_USER.get_value()))
                                {
                                    App.set_user_mode(response.getJSONObject(Params.DATA.get_value()).getBoolean(Params.NEW_USER.get_value()));
                                }

                                if(response.getJSONObject(Params.DATA.get_value()).has(Params.CELEBRITY.get_value()))
                                {
                                    HashMap<Long,Celebrity> celebrityHashMap = new C().parse_celebrity(response.getJSONObject(Params.DATA.get_value()).getJSONArray(Params.CELEBRITY.get_value()));
                                    if(celebrityHashMap != null && celebrityHashMap.size() > 0) {
                                        ArrayList<Celebrity> celebrities = new ArrayList<>();
                                        for (Celebrity c : celebrityHashMap.values()) {
                                            celebrities.add(c);
                                        }
                                        db.Sync_celebrities(celebrities);
                                    }
                                }



                                success = true;
                            }

                        }
                    }
                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return success;
        }

    }



}
