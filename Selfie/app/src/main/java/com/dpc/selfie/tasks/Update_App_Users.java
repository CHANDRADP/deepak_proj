package com.dpc.selfie.tasks;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.dpc.selfie.Act.Activity_Login;
import com.dpc.selfie.App;
import com.dpc.selfie.GCMIntentService;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.DB;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.Notification;
import com.dpc.selfie.models.Server_Params;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by D-P-C on 17-Feb-15.
 */
public class Update_App_Users  extends AsyncTask<String, Long, Boolean> {
    private Context c;
    private ArrayList<Long> ids = new ArrayList<>();
    private boolean is_fb_users = false;
    private Notification notification;
    private DB db;

    public Update_App_Users(Context c,ArrayList<Long> ids,boolean is_fb_users) {
        this.c = c;
        this.ids = ids;
        this.is_fb_users = is_fb_users;
        db = new DB(c);

    }

    public Update_App_Users(Context c, ArrayList<Long> ids, boolean is_fb_users, Notification notification) {
        this.c = c;
        this.ids = ids;
        this.is_fb_users = is_fb_users;
        this.notification = notification;
        db = new DB(c);
    }


    @Override
    protected Boolean doInBackground(String... params) {

        return get_users();
    }

    @Override
    protected void onPostExecute(Boolean result) {

        Intent i = new Intent(Activity_Login.BROADCAST_RECEIVER);
        i.putExtra(Params.STATUS.get_value(),result);
        c.sendBroadcast(i);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    public boolean get_users()
    {
        boolean success = false;
        JSONParser parser = new JSONParser();
        JSONObject response = null;
        try {

            response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                    new Server_Params().update_users(ids,is_fb_users));
            if(response != null)
            {
                Log.d("response", response.toString());
                if(response.has(Params.STATUS.get_value()))
                {
                    int status = response.getInt(Params.STATUS.get_value());
                    if (status == 200)
                    {


                        JSONObject data = response.getJSONObject(Params.DATA.get_value());
                        JSONArray jsonArray = data.getJSONArray("user");
                        for (int i = 0; i < jsonArray.length();i++)
                        {
                            JSONObject object = jsonArray.getJSONObject(i);
                            db.Sync_App_User(new C().parse_user(object));
                        }
                        success = true;
                        App.users_updated(success);
                        if(notification != null)
                        {
                            long id = new DB(c).add_notification(notification);
                            if(id > 0)
                            {
                                notification.setId(id);
                                //generate notification
                                if(notification.getType() == 1 && App.get_feed())
                                    GCMIntentService.generateNotification(c, notification);
                                else if(notification.getType() == 2 && App.get_comment())
                                    GCMIntentService.generateNotification(c, notification);
                                else if(notification.getType() == 3 && App.get_rating())
                                    GCMIntentService.generateNotification(c, notification);
                            }
                        }



                    }
                }
            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return success;
    }

}