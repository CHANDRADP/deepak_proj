package com.dpc.selfie.Act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.dpc.selfie.App;
import com.dpc.selfie.R;
import com.dpc.selfie.adapters.Templates_Adapter;
import com.dpc.selfie.customs.BaseActivity;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.interfaces.OnCardClickListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class Activity_Collage_Templates extends BaseActivity implements View.OnClickListener {

    private RecyclerView templates_list;
    private Templates_Adapter templates_adapter;
    RecyclerView.LayoutManager mLayoutManager;
    ImageView selected_image;


    private ImageView image211, image212, image221, image222, image311, image312, image313, image321, image322, image323;
    private ImageView image331, image332, image333, image341, image342, image343;
    private ImageView image411, image412, image413, image414, image421, image422, image423, image424, image431, image432;
    private ImageView image433, image434, image441, image442, image443, image444;
    private ImageView image511, image512, image513, image514, image515, image521, image522, image523, image524, image525;
    private ImageView image531, image532, image533, image534, image535, image541, image542, image543, image544, image545;
    private int TWO_IMAGES = 2, THREE_IMAGES = 3, FOUR_IMAGES = 4, FIVE_IMAGES = 5;
    private int no_of_images = 0;
    private int REQUEST_CAMERA = 111;
    private  int SELECT_FILE = 222;
    private LinearLayout layout_two_one, layout_two_two, layout_three_one, layout_three_two, layout_three_three, layout_three_four, layout_four_one, layout_four_two, layout_four_three, layout_four_four, layout_five_one, layout_five_two, layout_five_three, layout_five_four;

    private int[] two_images = new int[]{R.drawable.template_two_one, R.drawable.template_two_two, R.drawable.template_two_three, R.drawable.template_two_four};
    private int[] three_images = new int[]{R.drawable.template_three_one, R.drawable.template_three_two, R.drawable.template_three_three, R.drawable.template_three_four};
    private int[] four_images = new int[]{R.drawable.template_four_one, R.drawable.template_four_two, R.drawable.template_four_three, R.drawable.template_four_four};
    private int[] five_images = new int[]{R.drawable.template_five_one, R.drawable.template_five_two, R.drawable.template_five_three, R.drawable.template_five_four};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_collage__templates);

        no_of_images = getIntent().getIntExtra(Params.NO_OF_IMAGES.get_value(),0);

        layout_two_one = (LinearLayout) findViewById(R.id.layout_two_one);
        layout_two_two = (LinearLayout) findViewById(R.id.layout_two_two);

        layout_three_one = (LinearLayout) findViewById(R.id.layout_three_one);
        layout_three_two = (LinearLayout) findViewById(R.id.layout_three_two);
        layout_three_three = (LinearLayout) findViewById(R.id.layout_three_three);
        layout_three_four = (LinearLayout) findViewById(R.id.layout_three_four);

        layout_four_one = (LinearLayout) findViewById(R.id.layout_four_one);
        layout_four_two = (LinearLayout) findViewById(R.id.layout_four_two);
        layout_four_three = (LinearLayout) findViewById(R.id.layout_four_three);
        layout_four_four = (LinearLayout) findViewById(R.id.layout_four_four);

        layout_five_one = (LinearLayout) findViewById(R.id.layout_five_one);
        layout_five_two = (LinearLayout) findViewById(R.id.layout_five_two);
        layout_five_three = (LinearLayout) findViewById(R.id.layout_five_three);
        layout_five_four = (LinearLayout) findViewById(R.id.layout_five_four);

        image211 = (ImageView) findViewById(R.id.image211);
        image212 = (ImageView) findViewById(R.id.image212);

        image221 = (ImageView) findViewById(R.id.image221);
        image222 = (ImageView) findViewById(R.id.image222);

        image311 = (ImageView) findViewById(R.id.image311);
        image312 = (ImageView) findViewById(R.id.image312);
        image313 = (ImageView) findViewById(R.id.image313);

        image321 = (ImageView) findViewById(R.id.image321);
        image322 = (ImageView) findViewById(R.id.image322);
        image323 = (ImageView) findViewById(R.id.image323);

        image331 = (ImageView) findViewById(R.id.image331);
        image332 = (ImageView) findViewById(R.id.image332);
        image333 = (ImageView) findViewById(R.id.image333);

        image341 = (ImageView) findViewById(R.id.image341);
        image342 = (ImageView) findViewById(R.id.image342);
        image343 = (ImageView) findViewById(R.id.image343);

        image411 = (ImageView) findViewById(R.id.image411);
        image412 = (ImageView) findViewById(R.id.image412);
        image413 = (ImageView) findViewById(R.id.image413);
        image414 = (ImageView) findViewById(R.id.image414);

        image421 = (ImageView) findViewById(R.id.image421);
        image422 = (ImageView) findViewById(R.id.image422);
        image423 = (ImageView) findViewById(R.id.image423);
        image424 = (ImageView) findViewById(R.id.image424);

        image431 = (ImageView) findViewById(R.id.image431);
        image432 = (ImageView) findViewById(R.id.image432);
        image433 = (ImageView) findViewById(R.id.image433);
        image434 = (ImageView) findViewById(R.id.image434);

        image441 = (ImageView) findViewById(R.id.image441);
        image442 = (ImageView) findViewById(R.id.image442);
        image443 = (ImageView) findViewById(R.id.image443);
        image444 = (ImageView) findViewById(R.id.image444);

        image511 = (ImageView) findViewById(R.id.image511);
        image512 = (ImageView) findViewById(R.id.image512);
        image513 = (ImageView) findViewById(R.id.image513);
        image514 = (ImageView) findViewById(R.id.image514);
        image515 = (ImageView) findViewById(R.id.image515);

        image521 = (ImageView) findViewById(R.id.image521);
        image522 = (ImageView) findViewById(R.id.image522);
        image523 = (ImageView) findViewById(R.id.image523);
        image524 = (ImageView) findViewById(R.id.image524);
        image525 = (ImageView) findViewById(R.id.image525);

        image531 = (ImageView) findViewById(R.id.image531);
        image532 = (ImageView) findViewById(R.id.image532);
        image533 = (ImageView) findViewById(R.id.image533);
        image534 = (ImageView) findViewById(R.id.image534);
        image535 = (ImageView) findViewById(R.id.image535);

        image541 = (ImageView) findViewById(R.id.image541);
        image542 = (ImageView) findViewById(R.id.image542);
        image543 = (ImageView) findViewById(R.id.image543);
        image544 = (ImageView) findViewById(R.id.image544);
        image545 = (ImageView) findViewById(R.id.image545);

        image211.setOnClickListener(this);
        image212.setOnClickListener(this);

        image221.setOnClickListener(this);
        image222.setOnClickListener(this);

        image311.setOnClickListener(this);
        image312.setOnClickListener(this);
        image313.setOnClickListener(this);

        image321.setOnClickListener(this);
        image322.setOnClickListener(this);
        image323.setOnClickListener(this);

        image331.setOnClickListener(this);
        image332.setOnClickListener(this);
        image333.setOnClickListener(this);

        image341.setOnClickListener(this);
        image342.setOnClickListener(this);
        image343.setOnClickListener(this);

        image411.setOnClickListener(this);
        image412.setOnClickListener(this);
        image413.setOnClickListener(this);
        image414.setOnClickListener(this);

        image421.setOnClickListener(this);
        image422.setOnClickListener(this);
        image423.setOnClickListener(this);
        image424.setOnClickListener(this);

        image431.setOnClickListener(this);
        image432.setOnClickListener(this);
        image433.setOnClickListener(this);
        image434.setOnClickListener(this);

        image441.setOnClickListener(this);
        image442.setOnClickListener(this);
        image443.setOnClickListener(this);
        image444.setOnClickListener(this);

        image511.setOnClickListener(this);
        image512.setOnClickListener(this);
        image513.setOnClickListener(this);
        image514.setOnClickListener(this);
        image515.setOnClickListener(this);

        image521.setOnClickListener(this);
        image522.setOnClickListener(this);
        image523.setOnClickListener(this);
        image524.setOnClickListener(this);
        image525.setOnClickListener(this);

        image531.setOnClickListener(this);
        image532.setOnClickListener(this);
        image533.setOnClickListener(this);
        image534.setOnClickListener(this);
        image535.setOnClickListener(this);

        image541.setOnClickListener(this);
        image542.setOnClickListener(this);
        image543.setOnClickListener(this);
        image544.setOnClickListener(this);
        image545.setOnClickListener(this);




        templates_list = (RecyclerView) findViewById(R.id.templates_list);

        templates_list.setHasFixedSize(true);

        GridLayoutManager llm = new GridLayoutManager(this,1);
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        templates_list.setLayoutManager(llm);

        if(no_of_images == TWO_IMAGES)
        {
            templates_adapter = new Templates_Adapter(Activity_Collage_Templates.this, two_images);
        }
        else if(no_of_images == THREE_IMAGES)
        {
            templates_adapter = new Templates_Adapter(Activity_Collage_Templates.this, three_images);
        }
        else if(no_of_images == FOUR_IMAGES)
        {
            templates_adapter = new Templates_Adapter(Activity_Collage_Templates.this, four_images);
        }
        else if(no_of_images == FIVE_IMAGES)
        {
            templates_adapter = new Templates_Adapter(Activity_Collage_Templates.this, five_images);
        }


        templates_list.setAdapter(templates_adapter);

        templates_adapter.setOnCardClickListener(new OnCardClickListener()
        {
            @Override
            public void onClick(View v, int position)
            {
                Log.d("Templates",String.valueOf(position));
                if(no_of_images == TWO_IMAGES)
                {
                    if(position == 0)
                    {
                        layout_two_one.setVisibility(View.VISIBLE);
                        layout_two_two.setVisibility(View.GONE);
                        layout_three_one.setVisibility(View.GONE);
                        layout_three_two.setVisibility(View.GONE);
                        layout_three_three.setVisibility(View.GONE);
                        layout_three_four.setVisibility(View.GONE);
                        layout_four_one.setVisibility(View.GONE);
                        layout_four_two.setVisibility(View.GONE);
                        layout_four_three.setVisibility(View.GONE);
                        layout_four_four.setVisibility(View.GONE);
                        layout_five_one.setVisibility(View.GONE);
                        layout_five_two.setVisibility(View.GONE);
                        layout_five_three.setVisibility(View.GONE);
                        layout_five_four.setVisibility(View.GONE);

                    }
                    else if(position == 1)
                    {
                        layout_two_one.setVisibility(View.GONE);
                        layout_two_two.setVisibility(View.VISIBLE);
                        layout_three_one.setVisibility(View.GONE);
                        layout_three_two.setVisibility(View.GONE);
                        layout_three_three.setVisibility(View.GONE);
                        layout_three_four.setVisibility(View.GONE);
                        layout_four_one.setVisibility(View.GONE);
                        layout_four_two.setVisibility(View.GONE);
                        layout_four_three.setVisibility(View.GONE);
                        layout_four_four.setVisibility(View.GONE);
                        layout_five_one.setVisibility(View.GONE);
                        layout_five_two.setVisibility(View.GONE);
                        layout_five_three.setVisibility(View.GONE);
                        layout_five_four.setVisibility(View.GONE);
                    }
                    else if(position == 2)
                    {
                        layout_two_one.setVisibility(View.GONE);
                        layout_two_two.setVisibility(View.GONE);
                        layout_three_one.setVisibility(View.GONE);
                        layout_three_two.setVisibility(View.GONE);
                        layout_three_three.setVisibility(View.GONE);
                        layout_three_four.setVisibility(View.GONE);
                        layout_four_one.setVisibility(View.GONE);
                        layout_four_two.setVisibility(View.GONE);
                        layout_four_three.setVisibility(View.GONE);
                        layout_four_four.setVisibility(View.GONE);
                        layout_five_one.setVisibility(View.GONE);
                        layout_five_two.setVisibility(View.GONE);
                        layout_five_three.setVisibility(View.GONE);
                        layout_five_four.setVisibility(View.GONE);
                    }
                    else if(position == 3)
                    {
                        layout_two_one.setVisibility(View.GONE);
                        layout_two_two.setVisibility(View.GONE);
                        layout_three_one.setVisibility(View.GONE);
                        layout_three_two.setVisibility(View.GONE);
                        layout_three_three.setVisibility(View.GONE);
                        layout_three_four.setVisibility(View.GONE);
                        layout_four_one.setVisibility(View.GONE);
                        layout_four_two.setVisibility(View.GONE);
                        layout_four_three.setVisibility(View.GONE);
                        layout_four_four.setVisibility(View.GONE);
                        layout_five_one.setVisibility(View.GONE);
                        layout_five_two.setVisibility(View.GONE);
                        layout_five_three.setVisibility(View.GONE);
                        layout_five_four.setVisibility(View.GONE);
                    }
                }
                else if(no_of_images == THREE_IMAGES)
                {
                    if(position == 0)
                    {
                        layout_two_one.setVisibility(View.GONE);
                        layout_two_two.setVisibility(View.GONE);
                        layout_three_one.setVisibility(View.VISIBLE);
                        layout_three_two.setVisibility(View.GONE);
                        layout_three_three.setVisibility(View.GONE);
                        layout_three_four.setVisibility(View.GONE);
                        layout_four_one.setVisibility(View.GONE);
                        layout_four_two.setVisibility(View.GONE);
                        layout_four_three.setVisibility(View.GONE);
                        layout_four_four.setVisibility(View.GONE);
                        layout_five_one.setVisibility(View.GONE);
                        layout_five_two.setVisibility(View.GONE);
                        layout_five_three.setVisibility(View.GONE);
                        layout_five_four.setVisibility(View.GONE);
                    }
                    else if(position == 1)
                    {
                        layout_two_one.setVisibility(View.GONE);
                        layout_two_two.setVisibility(View.GONE);
                        layout_three_one.setVisibility(View.GONE);
                        layout_three_two.setVisibility(View.VISIBLE);
                        layout_three_three.setVisibility(View.GONE);
                        layout_three_four.setVisibility(View.GONE);
                        layout_four_one.setVisibility(View.GONE);
                        layout_four_two.setVisibility(View.GONE);
                        layout_four_three.setVisibility(View.GONE);
                        layout_four_four.setVisibility(View.GONE);
                        layout_five_one.setVisibility(View.GONE);
                        layout_five_two.setVisibility(View.GONE);
                        layout_five_three.setVisibility(View.GONE);
                        layout_five_four.setVisibility(View.GONE);
                    }
                    else if(position == 2)
                    {
                        layout_two_one.setVisibility(View.GONE);
                        layout_two_two.setVisibility(View.GONE);
                        layout_three_one.setVisibility(View.GONE);
                        layout_three_two.setVisibility(View.GONE);
                        layout_three_three.setVisibility(View.VISIBLE);
                        layout_three_four.setVisibility(View.GONE);
                        layout_four_one.setVisibility(View.GONE);
                        layout_four_two.setVisibility(View.GONE);
                        layout_four_three.setVisibility(View.GONE);
                        layout_four_four.setVisibility(View.GONE);
                        layout_five_one.setVisibility(View.GONE);
                        layout_five_two.setVisibility(View.GONE);
                        layout_five_three.setVisibility(View.GONE);
                        layout_five_four.setVisibility(View.GONE);
                    }
                    else if(position == 3)
                    {
                        layout_two_one.setVisibility(View.GONE);
                        layout_two_two.setVisibility(View.GONE);
                        layout_three_one.setVisibility(View.GONE);
                        layout_three_two.setVisibility(View.GONE);
                        layout_three_three.setVisibility(View.GONE);
                        layout_three_four.setVisibility(View.VISIBLE);
                        layout_four_one.setVisibility(View.GONE);
                        layout_four_two.setVisibility(View.GONE);
                        layout_four_three.setVisibility(View.GONE);
                        layout_four_four.setVisibility(View.GONE);
                        layout_five_one.setVisibility(View.GONE);
                        layout_five_two.setVisibility(View.GONE);
                        layout_five_three.setVisibility(View.GONE);
                        layout_five_four.setVisibility(View.GONE);
                    }
                }
                else if(no_of_images == FOUR_IMAGES)
                {
                    if(position == 0)
                    {
                        layout_two_one.setVisibility(View.GONE);
                        layout_two_two.setVisibility(View.GONE);
                        layout_three_one.setVisibility(View.GONE);
                        layout_three_two.setVisibility(View.GONE);
                        layout_three_three.setVisibility(View.GONE);
                        layout_three_four.setVisibility(View.GONE);
                        layout_four_one.setVisibility(View.VISIBLE);
                        layout_four_two.setVisibility(View.GONE);
                        layout_four_three.setVisibility(View.GONE);
                        layout_four_four.setVisibility(View.GONE);
                        layout_five_one.setVisibility(View.GONE);
                        layout_five_two.setVisibility(View.GONE);
                        layout_five_three.setVisibility(View.GONE);
                        layout_five_four.setVisibility(View.GONE);
                    }
                    else if(position == 1)
                    {
                        layout_two_one.setVisibility(View.GONE);
                        layout_two_two.setVisibility(View.GONE);
                        layout_three_one.setVisibility(View.GONE);
                        layout_three_two.setVisibility(View.GONE);
                        layout_three_three.setVisibility(View.GONE);
                        layout_three_four.setVisibility(View.GONE);
                        layout_four_one.setVisibility(View.GONE);
                        layout_four_two.setVisibility(View.VISIBLE);
                        layout_four_three.setVisibility(View.GONE);
                        layout_four_four.setVisibility(View.GONE);
                        layout_five_one.setVisibility(View.GONE);
                        layout_five_two.setVisibility(View.GONE);
                        layout_five_three.setVisibility(View.GONE);
                        layout_five_four.setVisibility(View.GONE);
                    }
                    else if(position == 2)
                    {
                        layout_two_one.setVisibility(View.GONE);
                        layout_two_two.setVisibility(View.GONE);
                        layout_three_one.setVisibility(View.GONE);
                        layout_three_two.setVisibility(View.GONE);
                        layout_three_three.setVisibility(View.GONE);
                        layout_three_four.setVisibility(View.GONE);
                        layout_four_one.setVisibility(View.GONE);
                        layout_four_two.setVisibility(View.GONE);
                        layout_four_three.setVisibility(View.VISIBLE);
                        layout_four_four.setVisibility(View.GONE);
                        layout_five_one.setVisibility(View.GONE);
                        layout_five_two.setVisibility(View.GONE);
                        layout_five_three.setVisibility(View.GONE);
                        layout_five_four.setVisibility(View.GONE);
                    }
                    else if(position == 3)
                    {
                        layout_two_one.setVisibility(View.GONE);
                        layout_two_two.setVisibility(View.GONE);
                        layout_three_one.setVisibility(View.GONE);
                        layout_three_two.setVisibility(View.GONE);
                        layout_three_three.setVisibility(View.GONE);
                        layout_three_four.setVisibility(View.GONE);
                        layout_four_one.setVisibility(View.GONE);
                        layout_four_two.setVisibility(View.GONE);
                        layout_four_three.setVisibility(View.GONE);
                        layout_four_four.setVisibility(View.VISIBLE);
                        layout_five_one.setVisibility(View.GONE);
                        layout_five_two.setVisibility(View.GONE);
                        layout_five_three.setVisibility(View.GONE);
                        layout_five_four.setVisibility(View.GONE);
                    }
                }
                else if(no_of_images == FIVE_IMAGES)
                {
                    if(position == 0)
                    {
                        layout_two_one.setVisibility(View.GONE);
                        layout_two_two.setVisibility(View.GONE);
                        layout_three_one.setVisibility(View.GONE);
                        layout_three_two.setVisibility(View.GONE);
                        layout_three_three.setVisibility(View.GONE);
                        layout_three_four.setVisibility(View.GONE);
                        layout_four_one.setVisibility(View.GONE);
                        layout_four_two.setVisibility(View.GONE);
                        layout_four_three.setVisibility(View.GONE);
                        layout_four_four.setVisibility(View.GONE);
                        layout_five_one.setVisibility(View.VISIBLE);
                        layout_five_two.setVisibility(View.GONE);
                        layout_five_three.setVisibility(View.GONE);
                        layout_five_four.setVisibility(View.GONE);
                    }
                    else if(position == 1)
                    {
                        layout_two_one.setVisibility(View.GONE);
                        layout_two_two.setVisibility(View.GONE);
                        layout_three_one.setVisibility(View.GONE);
                        layout_three_two.setVisibility(View.GONE);
                        layout_three_three.setVisibility(View.GONE);
                        layout_three_four.setVisibility(View.GONE);
                        layout_four_one.setVisibility(View.GONE);
                        layout_four_two.setVisibility(View.GONE);
                        layout_four_three.setVisibility(View.GONE);
                        layout_four_four.setVisibility(View.GONE);
                        layout_five_one.setVisibility(View.GONE);
                        layout_five_two.setVisibility(View.VISIBLE);
                        layout_five_three.setVisibility(View.GONE);
                        layout_five_four.setVisibility(View.GONE);
                    }
                    else if(position == 2)
                    {
                        layout_two_one.setVisibility(View.GONE);
                        layout_two_two.setVisibility(View.GONE);
                        layout_three_one.setVisibility(View.GONE);
                        layout_three_two.setVisibility(View.GONE);
                        layout_three_three.setVisibility(View.GONE);
                        layout_three_four.setVisibility(View.GONE);
                        layout_four_one.setVisibility(View.GONE);
                        layout_four_two.setVisibility(View.GONE);
                        layout_four_three.setVisibility(View.GONE);
                        layout_four_four.setVisibility(View.GONE);
                        layout_five_one.setVisibility(View.GONE);
                        layout_five_two.setVisibility(View.GONE);
                        layout_five_three.setVisibility(View.VISIBLE);
                        layout_five_four.setVisibility(View.GONE);
                    }
                    else if(position == 3)
                    {
                        layout_two_one.setVisibility(View.GONE);
                        layout_two_two.setVisibility(View.GONE);
                        layout_three_one.setVisibility(View.GONE);
                        layout_three_two.setVisibility(View.GONE);
                        layout_three_three.setVisibility(View.GONE);
                        layout_three_four.setVisibility(View.GONE);
                        layout_four_one.setVisibility(View.GONE);
                        layout_four_two.setVisibility(View.GONE);
                        layout_four_three.setVisibility(View.GONE);
                        layout_four_four.setVisibility(View.GONE);
                        layout_five_one.setVisibility(View.GONE);
                        layout_five_two.setVisibility(View.GONE);
                        layout_five_three.setVisibility(View.GONE);
                        layout_five_four.setVisibility(View.VISIBLE);
                    }
                }
            }
        });




    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity__collage__templates, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case android.R.id.home:

                super.onBackPressed();
                break;
        }

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_post) {

            String filepath = create_image();
            Intent intent1=new Intent(Activity_Collage_Templates.this,Activity_Edit_First.class);
            intent1.putExtra("FilePath",filepath );
            intent1.putExtra("url",filepath);
            intent1.putExtra("CollageActive",true);
            startActivity(intent1);
            return true;
        }



        return super.onOptionsItemSelected(item);
    }

    public String create_image()
    {
        File file = null;
        View content = findViewById(R.id.CustomLayout);
        content.setDrawingCacheEnabled(true);
        Bitmap bitmap = content.getDrawingCache();
        try
        {
            file = new File(App.APP_FOLDER+"/Selfie"+System.currentTimeMillis()+".jpg");
            {
                if(!file.exists())
                {
                    file.createNewFile();
                }
                FileOutputStream ostream = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, ostream);
                ostream.close();
                content.invalidate();
                Log.d("Image File Path", file.getAbsolutePath());

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return file.getAbsolutePath();
    }


    private void selectImage(ImageView image) {
        selected_image = image;
        final ArrayList<String> items = new ArrayList();
        items.add("Capture from Camera");
        items.add("Select from Gallery");
        items.add("Cancel");

        final Dialog dialog = new Dialog(Activity_Collage_Templates.this);
        dialog.setContentView(R.layout.image_select_options_dialog);
        dialog.setTitle("Title...");

        ListView list = (ListView) dialog.findViewById(R.id.optionsList);
        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        list.setAdapter(itemsAdapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                dialog.dismiss();
                if(position == 0)
                {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                }
                else if(position == 1)
                {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, SELECT_FILE);
                }
                else if(position == 2)
                {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                selected_image.setImageBitmap(thumbnail);

            } else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                        null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();

                String selectedImagePath = cursor.getString(column_index);

                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(selectedImagePath, options);

                selected_image.setImageBitmap(bm);
            }
        }
    }



        @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.image211:
                selectImage(image211);
                break;

            case R.id.image212:
                selectImage(image212);
                break;

            case R.id.image221:
                selectImage(image221);
                break;

            case R.id.image222:
                selectImage(image222);
                break;

            case R.id.image311:
                selectImage(image311);
                break;

            case R.id.image312:
                selectImage(image312);
                break;

            case R.id.image313:
                selectImage(image313);
                break;

            case R.id.image321:
                selectImage(image321);
                break;

            case R.id.image322:
                selectImage(image322);
                break;

            case R.id.image323:
                selectImage(image323);
                break;

            case R.id.image331:
                selectImage(image331);
                break;

            case R.id.image332:
                selectImage(image332);
                break;

            case R.id.image333:
                selectImage(image333);
                break;

            case R.id.image341:
                selectImage(image341);
                break;

            case R.id.image342:
                selectImage(image342);
                break;

            case R.id.image343:
                selectImage(image343);
                break;

            case R.id.image411:
                selectImage(image411);
                break;

            case R.id.image412:
                selectImage(image412);
                break;

            case R.id.image413:
                selectImage(image413);
                break;

            case R.id.image414:
                selectImage(image414);
                break;

            case R.id.image421:
                selectImage(image421);
                break;

            case R.id.image422:
                selectImage(image422);
                break;

            case R.id.image423:
                selectImage(image423);
                break;

            case R.id.image424:
                selectImage(image424);
                break;

            case R.id.image431:
                selectImage(image431);
                break;

            case R.id.image441:
                selectImage(image441);
                break;

            case R.id.image442:
                selectImage(image442);
                break;

            case R.id.image443:
                selectImage(image443);
                break;

            case R.id.image444:
                selectImage(image444);
                break;

            case R.id.image511:
                selectImage(image511);
                break;

            case R.id.image512:
                selectImage(image512);
                break;

            case R.id.image513:
                selectImage(image513);
                break;

            case R.id.image514:
                selectImage(image514);
                break;

            case R.id.image515:
                selectImage(image515);
                break;

            case R.id.image521:
                selectImage(image521);
                break;

            case R.id.image522:
                selectImage(image522);
                break;

            case R.id.image523:
                selectImage(image523);
                break;

            case R.id.image524:
                selectImage(image524);
                break;

            case R.id.image525:
                selectImage(image525);
                break;

            case R.id.image531:
                selectImage(image531);
                break;

            case R.id.image532:
                selectImage(image532);
                break;

            case R.id.image533:
                selectImage(image533);
                break;

            case R.id.image534:
                selectImage(image534);
                break;

            case R.id.image535:
                selectImage(image535);
                break;

            case R.id.image541:
                selectImage(image541);
                break;

            case R.id.image542:
                selectImage(image542);
                break;

            case R.id.image543:
                selectImage(image543);
                break;

            case R.id.image544:
                selectImage(image544);
                break;

            case R.id.image545:
                selectImage(image545);
                break;
        }

    }
}