package com.dpc.selfie.enums;

/**
 * Created by D-P-C on 17-Feb-15.
 */
public enum Params {
    NONE("none"),
    PAGE("page"),
    TAGS("tags"),
    TAG("tag"),
    USER_ID("u_id"),
    ID("id"),
    FB_ID("fb_id"),
    FRIENDS("friends"),
    NAME("name"),
    PICTURE("picture"),
    DATA("data"),
    FEEDS("feeds"),
    BRANDS_FEED("brands_feed"),
    BRAND_ID("brand_id"),
    FIRST_NAME("f_name"),
    MIDDLE_NAME("m_name"),
    LAST_NAME("l_name"),
    DISPLAY_NAME("display_name"),
    NUMBER("number"),
    EMAIL("email"),
    DOB("dob"),
    GENDER("gender"),
    MALE("male"),
    STATUS("status"),
    FROM_NOTIFICATIONS("from_notification"),
    POINTS("points"),
    THUMBNAIL("thumbnail"),
    IMAGE("image"),
    IMAGE_ID("img_id"),
    DATE("date"),
    BADGES("badge_type"),
    TOKEN("token"),
    TYPE("type"),
    SELFIE("selfie"),
    RATING("rating"),
    RATING_COUNT("rate_count"),
    MY_RATING("my_rating"),
    COMMENT("comment"),
    COMMENT_COUNT("comment_count"),
    PHONE_TYPE("p_type"),
    PHONE_MODEL("p_model"),
    PUSH_ID("push_id"),
    APP_VERSION("app_version"),
    NEW_USER("new_user"),
    CONTENT("content"),
    CELEBRITY("celebrity"),
    CELEBRITY_ID("celebrity_id"),
    FOLLOW_COUNT("follow_count"),
    FOLLOWING("following"),
    PROFESSION("profession"),
    READ_STATUS("read_status"),
    COUNTRY_CODE("c_code"),
    COMMENT_ID("comment_id"),
    SEARCH_KEYWORD("search_keyword"),
    CATEGORY_NAME("category_name"),
    CATEGORY_ID("category_id"),
    CATEGORY_ID_SELECTED("category_id_selected"),
    OFFER_ID("offer_id"),
    BRAND_NAME("brand_name"),
    BRAND_LOGO("brand_logo"),
    OFFER_DESCRIPTION("offer_description"),
    FRAME_ID("frame_id"),
    BRAND_FRAMES("brand_frames"),
    NO_OF_IMAGES("no_of_images")
    ;

    // Constructor
    private Params(final String value) {
        this.value = value;
    }

    // Internal state
    private String value;

    public String get_value() {
        return value;
    }
}
