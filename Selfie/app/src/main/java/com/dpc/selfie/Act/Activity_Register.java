package com.dpc.selfie.Act;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioButton;

import com.dpc.selfie.App;
import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.DB;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.customs.BaseActivity;
import com.dpc.selfie.customs.EText;

import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.Celebrity;
import com.dpc.selfie.models.Server_Params;
import com.dpc.selfie.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class Activity_Register extends BaseActivity implements View.OnClickListener {

    EText et_fName, et_mName, et_lName, et_dob, et_email, et_mobile, et_profession, et_country_code, et_status;
    Button btnRegister;
    private int day,month,year;
    String token = null;
    private RadioButton male, female;
    private SimpleDateFormat date1 = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        btnRegister = (Button) findViewById(R.id.btnRegister);
        et_fName = (EText) findViewById(R.id.et_firstName);
        et_mName = (EText) findViewById(R.id.et_middleName);
        et_lName = (EText) findViewById(R.id.et_lastName);
        et_dob = (EText) findViewById(R.id.et_dob);
        et_email = (EText) findViewById(R.id.et_email);
        et_mobile = (EText) findViewById(R.id.et_mobile);
        et_profession = (EText) findViewById(R.id.et_profession);
        et_country_code = (EText) findViewById(R.id.et_country_code);
        et_status = (EText) findViewById(R.id.et_status);
        male = (RadioButton) findViewById(R.id.radioMale);
        female = (RadioButton) findViewById(R.id.radioFemale);

        btnRegister.setOnClickListener(this);
        et_dob.setOnClickListener(this);

        final Calendar c = Calendar.getInstance();
        day = c.get(Calendar.DAY_OF_MONTH);
        month = c.get(Calendar.MONTH);
        year = c.get(Calendar.YEAR);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        et_dob.setOnFocusChangeListener(new View.OnFocusChangeListener(){

            @SuppressWarnings("deprecation")
            @Override
            public void onFocusChange(View arg0, boolean focused) {
                // TODO Auto-generated method stub
                if(focused)
                {
                    showDialog(1);
                }
            }

        });

    }

    private void validate_input_fields()
    {
        int gender = 0;
        et_fName.setText("Deepak");
        et_lName.setText("Jaikalyani");
        et_mName.setText("D");
        et_email.setText("jaikalyani.deepak@gmail.com");
        et_status.setText("Cool");
        et_country_code.setText("91");
        et_dob.setText("14/10/1989");
        et_mobile.setText("8123772458");
        et_profession.setText("SE");
        String f_name = et_fName.getText().toString();
        String m_name = et_mName.getText().toString();
        String l_name = et_lName.getText().toString();
        String displayName = f_name+" "+l_name;
        String dob = et_dob.getText().toString();
        String email = et_email.getText().toString();
        String country_code = et_country_code.getText().toString();
        String mobile = et_mobile.getText().toString();
        String profession = et_profession.getText().toString();
        String status = et_status.getText().toString();
        if(male.isChecked())
        {
            gender = 1;
        }
        else if(female.isChecked())
        {
            gender = 2;
        }

        f_name.trim();
        m_name.trim();
        l_name.trim();
        email.trim();
        mobile.trim();
        country_code.trim();

        if (f_name != null && f_name.length() > 0 && f_name.matches("[a-zA-Z]+"))
        {
            m_name = et_mName.getText().toString();
            if (m_name == null || m_name.matches("[a-zA-Z]+"))
            {
                l_name = et_lName.getText().toString();
                if (l_name != null && l_name.length() > 0 && l_name.matches("[a-zA-Z]+"))
                {
                    dob = et_dob.getText().toString();
                    if (dob != null && dob.length() > 9)
                    {
                        if (gender != 0)
                        {
                            email = et_email.getText().toString();
                            if (email != null && email.length() > 0 && email.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+"))
                            {
                                country_code = et_country_code.getText().toString();
                                if (country_code == null || country_code.matches("[0-9]+"))
                                {
                                    mobile = et_mobile.getText().toString();
                                    if (mobile == null || (mobile.length() == 10 && mobile.matches("[0-9]+")))
                                    {
                                        profession = et_profession.getText().toString();
                                        if (profession != null && profession.length() > 0 && profession.matches("[a-zA-Z]+"))
                                        {
                                            status = et_status.getText().toString();
                                            if (status != null && status.length() > 0 && status.matches("[a-zA-Z0-9]+"))
                                            {
                                                if (net.isConnectingToInternet())
                                                {
                                                    User app_user = new User(0, 0, gender, 0, f_name, m_name, l_name, displayName, mobile, email, dob, status, null, null, null, null);
                                                    new Register(getApplicationContext(), app_user, country_code, profession).execute();
                                                }
                                                else
                                                {
                                                    net.alert(getSupportFragmentManager());
                                                    return;
                                                }
                                            }
                                            else
                                            {
                                                C.alert(getApplicationContext(),
                                                        "Please enter your status");
                                            }
                                        }
                                        else
                                        {
                                            C.alert(getApplicationContext(),
                                                    "Please enter your profession");
                                        }
                                    }
                                    else
                                    {
                                        C.alert(getApplicationContext(),
                                                "Please enter proper phone number");
                                    }
                                }
                                else
                                {
                                    C.alert(getApplicationContext(),
                                            "Please enter proper country code");
                                }
                            }
                            else
                            {
                                C.alert(getApplicationContext(),
                                        "Please enter proper email id");
                            }
                        }
                        else
                        {
                            C.alert(getApplicationContext(),
                                    "Please enter your gender");
                        }
                    }
                    else
                    {
                         C.alert(getApplicationContext(),
                                 "Please enter your date of birth");
                    }
                }
                else
                {
                    C.alert(getApplicationContext(),
                            "Please enter proper last name.");
                }
            }
            else
            {
                C.alert(getApplicationContext(),
                        "Please enter proper middle name.");
            }
        }
        else
        {
            C.alert(getApplicationContext(),
                    "Please enter proper first name");
        }

    }
    @SuppressWarnings("deprecation")
    @Override
    protected Dialog onCreateDialog(int id)
    {
        DatePickerDialog dialog = new DatePickerDialog(this, startdatePickerListener, year, month,day);
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {
                    et_email.requestFocus();
                }
            }
        });
        return dialog;
    }

    private DatePickerDialog.OnDateSetListener startdatePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            Calendar calendar = Calendar.getInstance();

            calendar.set(selectedYear, selectedMonth, selectedDay);
            String date = date1.format(calendar.getTime());

            et_dob.setText(date);
            et_email.requestFocus();
        }


    };

    private  DatePickerDialog.OnCancelListener cancelListener = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
            et_email.requestFocus();
        }
    };

    private class Register extends AsyncTask<String, Long, Boolean>
    {
        Context c;
        User user = null;
        String c_code = null, profession =null, app_version = null;

        public Register(Context c, User user, String c_code, String profession)
        {
            this.c = c;
            this.user = user;
            this.c_code = c_code;
            this.profession = profession;
            app_version = App.get_version_from_manifest(getApplicationContext());
        }

        @Override
        protected Boolean doInBackground(String... params)
        {
            return register_user();
        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            if(result)
            {
                C.alert(getApplicationContext(),
                        "Registered Successfully");
                Intent intent = new Intent(Activity_Register.this, Activity_Feeds.class);
                startActivity(intent);
            }
        }

        @Override
        protected void onCancelled()
        {
            super.onCancelled();
        }


        public boolean register_user()
        {
            token = "notloggedinwithfb";
            boolean success = false;
            JSONParser parser = new JSONParser();
            try
            {
                JSONObject response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                        new Server_Params().add_u(user, token, App.get_gcm_id(), c_code, profession, app_version ));
                if (response != null)
                {
                    if (response.has(Params.STATUS.get_value()))
                    {
                        int status = response.getInt(Params.STATUS.get_value());
                        if (status == 200)
                        {
                            user = new C().parse_user(response.getJSONObject(Params.DATA.get_value()).getJSONObject("user"));
                            if (user.getUserid() > 0)
                            {
                                new DB(c).Sync_App_User(user);
                                App.setMyId(user.getUserid());
                                App.setToken(token);

                                if(response.getJSONObject(Params.DATA.get_value()).has(Params.NEW_USER.get_value()))
                                {
                                    App.set_user_mode(response.getJSONObject(Params.DATA.get_value()).getBoolean(Params.NEW_USER.get_value()));
                                }
                                if(response.getJSONObject(Params.DATA.get_value()).has(Params.CELEBRITY.get_value()))
                                {
                                    HashMap<Long,Celebrity> celebrityHashMap = new C().parse_celebrity(response.getJSONObject(Params.DATA.get_value()).getJSONArray(Params.CELEBRITY.get_value()));
                                    if(celebrityHashMap != null && celebrityHashMap.size() > 0)
                                    {
                                        ArrayList<Celebrity> celebrities = new ArrayList<>();
                                        for (Celebrity c : celebrityHashMap.values())
                                        {
                                            celebrities.add(c);
                                        }
                                        db.Sync_celebrities(celebrities);
                                    }
                                }
                                success = true;
                            }
                        }
                    }
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            return success;
        }
    }



    @SuppressWarnings("deprecation")
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btnRegister:
                validate_input_fields();
                break;

            case R.id.home:

                super.onBackPressed();

                break;


            case R.id.et_dob:

                showDialog(1);
                break;
        }
    }
}
