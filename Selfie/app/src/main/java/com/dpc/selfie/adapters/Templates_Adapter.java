package com.dpc.selfie.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.DB;
import com.dpc.selfie.Utils.Edit;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.interfaces.OnCardClickListener;
import com.dpc.selfie.models.BrandFrames;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class Templates_Adapter extends RecyclerView.Adapter<Templates_Adapter.ViewHolder> {

    private Context context;

    private int selected_posi = 0;

    private OnCardClickListener onCardClickListener;


    Bitmap image = null;
    private int[] templates_list;


    public void setOnCardClickListener(OnCardClickListener onCardClickListener)
    {
        this.onCardClickListener = onCardClickListener;
    }
    public Templates_Adapter(Context context, int[] templates_list) {
        this.context = context;
        this.templates_list = templates_list;

    }


    @Override
    public int getItemCount()
    {
        return templates_list.length;
    }
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i)
    {
        viewHolder.root.setBackgroundResource(templates_list[i]);
        viewHolder.name.setVisibility(View.GONE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup,final int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_edit, viewGroup, false);


        return new ViewHolder(itemView);
    }





    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        protected ImageView imageView;
        protected Text name;
        protected LinearLayout root;


        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            imageView = (ImageView)v.findViewById(R.id.image);
            name = (Text)v.findViewById(R.id.name);
            root = (LinearLayout) v.findViewById(R.id.root);
        }

        @Override
        public void onClick(View v) {
            selected_posi = getPosition();
            notifyDataSetChanged();
            if(onCardClickListener != null)
            {

                onCardClickListener.onClick(v,selected_posi);
            }
        }
    }

}