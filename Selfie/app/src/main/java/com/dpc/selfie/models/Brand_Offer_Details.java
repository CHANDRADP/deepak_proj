package com.dpc.selfie.models;

/**
 * Created by Deepak on 21-07-2015.
 */
public class Brand_Offer_Details
{

    private long offer_id, brand_id;
    private String brand_name, brand_logo, offer_description;

    public Brand_Offer_Details(long offer_id, long brand_id, String brand_name, String brand_logo, String offer_description)
    {
        this.offer_id = offer_id;
        this.brand_id = brand_id;
        this.brand_name = brand_name;
        this.brand_logo = brand_logo;
        this.offer_description = offer_description;
    }

    public long getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(long brand_id) {
        this.brand_id = brand_id;
    }

    public long getOffer_id() {
        return offer_id;
    }

    public void setOffer_id(long offer_id) {
        this.offer_id = offer_id;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getBrand_logo() {
        return brand_logo;
    }

    public void setBrand_logo(String brand_logo) {
        this.brand_logo = brand_logo;
    }

    public String getOffer_description() {
        return offer_description;
    }

    public void setOffer_description(String offer_description) {
        this.offer_description = offer_description;
    }
}
