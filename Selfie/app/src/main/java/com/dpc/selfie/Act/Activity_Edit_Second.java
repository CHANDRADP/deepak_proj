package com.dpc.selfie.Act;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.dpc.selfie.App;
import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.Edit;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.adapters.Search_Adapter;
import com.dpc.selfie.amazon.UploadService;
import com.dpc.selfie.customs.BaseActivity;
import com.dpc.selfie.customs.DividerItemDecorator;
import com.dpc.selfie.customs.EText;
import com.dpc.selfie.customs.Fb_Dialog;
import com.dpc.selfie.customs.MyProgress;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.interfaces.OnButtonClickListener;
import com.dpc.selfie.interfaces.OnCardClickListener;
import com.dpc.selfie.models.SearchData;
import com.dpc.selfie.models.Server_Params;
import com.dpc.selfie.stickyHeader.RecyclerItemClickListener;
import com.dpc.selfie.tasks.Post_Selfie;
import com.facebook.UiLifecycleHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;


public class Activity_Edit_Second extends BaseActivity implements SearchView.OnQueryTextListener {

    private File outFile;
    private RecyclerView sub_list, font_list, search_list;
    private LinearLayout ll_edit, ll_status, ll_seek;
    private EText status;
    private Handler handler;
    private Runnable runnable;
    private boolean brand_attached = false;
    private FrameLayout search_list_bg;
    private MyProgress myProgress;
    private String image_path = null, mystatus = "";
    private PopupWindow popup;
    public static HashMap<Long,SearchData> search_map = new HashMap<>();
    private ArrayList<SearchData> search_results = new ArrayList<>();
    private Toolbar toolbar;
    private EText et_status;
    private boolean tag_enabled = false;
    private Search_Adapter search_adapter;
    private ImageView postPic;

    ImageView image;
    LinearLayout contestLayout, tagLayout;



    private BroadcastReceiver uploadStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {

            final Bundle b = intent.getExtras();
            int state = b.getInt(UploadService.UPLOAD_STATE);
            if (state == 0) {
                myProgress = new MyProgress(Activity_Edit_Second.this);
                myProgress.setTitle("Uploading...");
                myProgress.setCancelable(false);
                myProgress.show();
                return;
            }
            if (state == 1) {
                //change status
                if (myProgress != null) {
                    new Post_Selfie(Activity_Edit_Second.this, myProgress, b.getString(UploadService.MESSAGE), mystatus, brand_attached).execute();

                    return;
                }
                return;
            }
            if (state < 0) {
                if (myProgress != null) {
                    myProgress.dismiss();
                }
                C.alert(getApplicationContext(), b.getString(UploadService.MESSAGE));
            }
            if (state == 100) {
                Fb_Dialog fb_dialog = new Fb_Dialog();
                fb_dialog.clickListener = new OnButtonClickListener() {
                    @Override
                    public void setOKSubmitListener(int value) {
                        if (value > 0) {
                            C.alert(getApplicationContext(), "Posting on facebook...");
                            publishStory(b.getString(Params.IMAGE.get_value()));
                        }
                        Intent i = new Intent(Activity_Edit_Second.this, Activity_Feeds.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);;
                    }
                };
                fb_dialog.show(getSupportFragmentManager(), "");
            }

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_edit_second);

        image =(ImageView) findViewById(R.id.image);
        contestLayout = (LinearLayout) findViewById(R.id.Contest_Layout);
        tagLayout = (LinearLayout) findViewById(R.id.Tag_Layout);

        et_status = (EText) findViewById(R.id.et_status);
        popup = new PopupWindow(this);

        Bitmap bitmap = BitmapFactory.decodeFile(this.getIntent().getStringExtra("url"));
        image_path = this.getIntent().getStringExtra("url");
        image.setImageBitmap(bitmap);

        toolbar =  (Toolbar) findViewById(R.id.toolbar);
        postPic = (ImageView) toolbar.findViewById(R.id.postPic);

        search_list_bg = (FrameLayout) findViewById(R.id.search_list_bg);


        search_list = (RecyclerView) findViewById(R.id.search_list);
        search_list.setLayoutManager(new LinearLayoutManager(this));
        final SearchView my_search_view = (SearchView) findViewById(R.id.my_search_view);
        my_search_view.setQueryHint("Search friend");
        my_search_view.setVisibility(View.GONE);
        search_list.setVisibility(View.GONE);


        postPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mystatus = et_status.getText().toString();
                if(mystatus.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(),"Please enter some status",Toast.LENGTH_LONG).show();
                }
                else
                {
                    myProgress = new MyProgress(Activity_Edit_Second.this);
                    myProgress.setTitle("Uploading...");
                    myProgress.show();


                    Intent intent = new Intent(Activity_Edit_Second.this, UploadService.class);
                    intent.putExtra(Params.SELFIE.get_value(), image_path);
                    myProgress.dismiss();
                    startService(intent);
                }

            }
        });





        my_search_view.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub

                Toast.makeText(getBaseContext(), String.valueOf(hasFocus),
                        Toast.LENGTH_SHORT).show();
            }
        });

        //*** setOnQueryTextListener ***
        my_search_view.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                // TODO Auto-generated method stub

                Toast.makeText(getBaseContext(), query,
                        Toast.LENGTH_SHORT).show();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String search_text) {
                // TODO Auto-generated method stub

                if(search_text.length()>2)
                {
                    Log.d("Search","Hi");
                    new do_search_task().execute(search_text);
                }

                if(search_text.length()==0)
                {
                    search_list.setVisibility(View.GONE);

                }
                return false;
            }
        });


        image.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

            if(tag_enabled)
            {
                Log.d("You touched", motionEvent.getX()+" "+motionEvent.getY());
                Point p = new Point();
                p.x = (int)motionEvent.getX();
                p.y = (int)motionEvent.getY();

                showPopup(Activity_Edit_Second.this, p, my_search_view);

                invalidateOptionsMenu();
            }
            return false;
            }
        });

        contestLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tag_enabled = false;
                my_search_view.setVisibility(View.GONE);
                invalidateOptionsMenu();
                Intent intent = new Intent(Activity_Edit_Second.this, Activity_Contest_Category.class);
                intent.putExtra("image_path", image_path);
                startActivity(intent);
            }
        });



        tagLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tag_enabled = true;
            }
        });

        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);
    }


    // *****************All finctions defined here ******************************//



    // *************** Override functions *************************//

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(uploadStateReceiver, new IntentFilter(UploadService.UPLOAD_STATE_CHANGED_ACTION));
        uiHelper.onStop();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(uploadStateReceiver);

    }

    @Override
    protected void onPause() {
        super.onPause();


    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    public void onBackPressed() {
        if(!App.is_new_user()) {
            remove_callbacks();
            super.onBackPressed();

        }
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(outFile != null && outFile.exists())
        {
            outFile.delete();
        }

        File dir = new File(App.APP_FOLDER);
        if(dir.exists())
        {
            File[] files = dir.listFiles();
            if(files != null && files.length > 0)
            {
                for(int i = 0;i < files.length;i++)
                {
                    files[i].delete();
                }
            }
        }
        uiHelper.onDestroy();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(popup.isShowing())
        {
            getMenuInflater().inflate(R.menu.menu_edit_search, menu);
        }
        else
        {
            getMenuInflater().inflate(R.menu.menu_activity__edit__second, menu);
        }



        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {



        switch (item.getItemId()) {
            case android.R.id.home:
                if(!App.is_new_user())
                {
                    remove_callbacks();
                    super.onBackPressed();
                }

                break;
            case R.id.post:



                mystatus = et_status.getText().toString();
                if(mystatus.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(),"Please enter some status",Toast.LENGTH_LONG).show();
                }
                else
                {
                    /*myProgress = new MyProgress(Activity_Edit_Second.this);
                    myProgress.setTitle("Uploading...");
                    myProgress.show();


                    Intent intent = new Intent(Activity_Edit_Second.this, UploadService.class);
                    intent.putExtra(Params.SELFIE.get_value(),image_path);
                    myProgress.dismiss();
                    startService(intent);*/

                }



                break;
        }
        return super.onOptionsItemSelected(item);
    }



    //********** End of Overriden functions***************** //

    private void showPopup(final Activity context, Point p, SearchView my_search_view) {

        my_search_view.setVisibility(View.VISIBLE);
        my_search_view.setIconified(false);
        int popupWidth = 100;
        int popupHeight = 200;
        Text txtMsg;
        RelativeLayout triangle;

        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.main);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.tag_edit_box, viewGroup);

        txtMsg = (Text) layout.findViewById(R.id.txtMsg);
        triangle = (RelativeLayout) layout.findViewById(R.id.leftTriangle);


        txtMsg.setMinimumWidth(150);




        // Creating the PopupWindow
        popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(150);
        popup.setHeight(popupHeight);
        popup.setFocusable(true);

        int width = txtMsg.getWidth();

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)triangle.getLayoutParams();
        params.setMargins(40, -43, 0, 0);
        triangle.setLayoutParams(params);


        int action_bar_height = C.getActionBarHeight(Activity_Edit_Second.this);

        // Some offset to align the popup a bit to the right, and a bit down, relative to button's position.
        int OFFSET_X = 0;
        int OFFSET_Y = toolbar.getHeight();

        // Clear the default translucent background
        popup.setBackgroundDrawable(new BitmapDrawable());

        // Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x + OFFSET_X, p.y + OFFSET_Y);


    }



    private void remove_callbacks()
    {
        if(handler != null && runnable != null)
        {
            handler.removeCallbacks(runnable);
        }
    }



    public class do_search_task  extends AsyncTask<String, Long, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            String search_text = params[0];

            return get_search_result(search_text);
        }

        @Override
        protected void onPostExecute(Boolean result) {

            if(search_results != null && search_results.size() > 0)
            {
                search_list.setVisibility(View.VISIBLE);
                search_adapter = new Search_Adapter(Activity_Edit_Second.this, search_results);
                search_list.setAdapter(search_adapter);
                search_list.addItemDecoration(new DividerItemDecorator(Activity_Edit_Second.this,DividerItemDecorator.VERTICAL_LIST));
                search_list.bringToFront();

                search_adapter.setOnCardClickListener(new OnCardClickListener() {
                    @Override
                    public void onClick(View v, int position) {
                        search_list.setVisibility(View.GONE);
                        SearchData data = search_results.get(position);
                        ((Text)popup.getContentView().findViewById(R.id.txtMsg)).setText(data.getF_name()+" "+data.getL_name());
                        Log.d("Search Name",data.getF_name()+" "+data.getL_name());
                    }
                });





            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        public boolean get_search_result(String search_text)
        {
            boolean success = false;
            JSONParser parser = new JSONParser();
            JSONObject response = null;
            try {

                response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                        new Server_Params().get_searched_user(search_text));
                if(response != null)
                {
                    Log.d("response", response.toString());
                    if(response.has(Params.STATUS.get_value()))
                    {
                        int status = response.getInt(Params.STATUS.get_value());
                        if (status == 200)
                        {


                            JSONObject data = response.getJSONObject(Params.DATA.get_value());
                            JSONArray array = data.getJSONArray("user");
                            search_map = new C().parse_search_result(array);

                            search_results.clear();
                            for(SearchData searchData : search_map.values())
                            {
                                search_results.add(searchData);
                            }

                            success = true;




                        }
                    }
                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return success;
        }

    }



    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }
}
