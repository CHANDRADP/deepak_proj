package com.dpc.selfie.models;

/**
 * Created by D-P-C on 24-Mar-15.
 */
public class Notification {
    private int type = -1;
    private long id = 0,userid = 0,imageid = 0;
    private String user_name = null,content = null,time = null;
    private int read_status = -1;




    public Notification(int type, long id, long userid, long imageid, String user_name, String content, String time, int read_status) {
        this.type = type;
        this.id = id;
        this.userid = userid;
        this.imageid = imageid;
        this.user_name = user_name;
        this.content = content;
        this.time = time;
        this.read_status = read_status;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int isRead_status() {
        return read_status;
    }

    public void setRead_status(int read_status) {
        this.read_status = read_status;
    }

    public long getImageid() {
        return imageid;
    }

    public void setImageid(long imageid) {
        this.imageid = imageid;
    }
}
