package com.dpc.selfie.Act;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.dpc.selfie.App;
import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.adapters.Comment_Adapter;
import com.dpc.selfie.adapters.Rating_Adapter;
import com.dpc.selfie.customs.BaseActivity;
import com.dpc.selfie.customs.CircleImage;
import com.dpc.selfie.customs.DividerItemDecorator;
import com.dpc.selfie.customs.EText;
import com.dpc.selfie.customs.MyProgress;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.customs.TextB;
import com.dpc.selfie.customs.WheelMenu;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.Comment;
import com.dpc.selfie.models.Feed;
import com.dpc.selfie.models.Rating;
import com.dpc.selfie.models.Server_Params;
import com.dpc.selfie.models.User;
import com.dpc.selfie.tasks.Update_Rating;
import com.dpc.selfie.tasks.Update_comment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class Activity_Selfie extends BaseActivity implements View.OnClickListener {

    private Feed feed;
    private RelativeLayout comment_view;
    private Toolbar actionbar;
    private RecyclerView rate_list, comment_list;
    private TextB name;
    private Text status, time, rate_count1, comment_count1, rate_count2, comment_count2, no_comments;
    private ImageView rate_icon, comment_icon,selfie,camera;
    private EText comment;
    private CircleImage image;
    private RelativeLayout ll_wheel;
    private WheelMenu wheelMenu;
    private Animation inAnimation, outAnimation, open, close;
    protected Comment_Adapter comment_adapter;
    private long image_id = -1;
    private User user = null;
    private boolean from_notification = false;

    private BroadcastReceiver selfie_post_Receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


            Bundle b = intent.getExtras();
            long state = b.getLong(Params.IMAGE_ID.get_value());
            if(state > 0)
            {
                        if(feed != null)
                        {
                            feed.setRating(b.getInt(Params.RATING.get_value()));
                            update_views();
                        }

            }


        }
    };
    private void toggle_menu(boolean open) {

        if (open) {
            comment_view.setVisibility(View.VISIBLE);
            inAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    comment_view.setVisibility(View.VISIBLE);

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            comment_view.startAnimation(inAnimation);
        } else {

            comment_view.startAnimation(outAnimation);
            comment_view.setVisibility(View.GONE);

        }
    }

    private void toggle_rate(boolean _open) {
        if (_open) {
            ll_wheel.setVisibility(View.VISIBLE);
            open.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    ll_wheel.setVisibility(View.VISIBLE);

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            ll_wheel.startAnimation(open);
        } else {

            ll_wheel.setVisibility(View.GONE);
            ll_wheel.startAnimation(close);

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_selfie);
        Intent i = this.getIntent();
        if (i.hasExtra(Params.IMAGE_ID.get_value())) {
            image_id = i.getLongExtra(Params.IMAGE_ID.get_value(), -1);
        }
        if (i.hasExtra(Params.ID.get_value())) {
            long notification_id = i.getLongExtra(Params.ID.get_value(), -1);
            db.update_read_status(notification_id,1);
            NotificationManager nMgr = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
            nMgr.cancel((int) notification_id);
        }
        if (i.hasExtra(Params.FROM_NOTIFICATIONS.get_value())) {
            from_notification = i.getBooleanExtra(Params.FROM_NOTIFICATIONS.get_value(), false);

        }

        inAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_down);
        outAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_up);

        open = AnimationUtils.loadAnimation(this, R.anim.slide_out);
        close = AnimationUtils.loadAnimation(this, R.anim.slide_in);

        init_views();

        new get_Selfie_info(image_id).execute();
    }

    @Override
    protected void onNewIntent(Intent i) {
        super.onNewIntent(i);

        if (i.hasExtra(Params.IMAGE_ID.get_value())) {
            image_id = i.getLongExtra(Params.IMAGE_ID.get_value(), -1);
            new get_Selfie_info(image_id).execute();
        }
        if (i.hasExtra(Params.ID.get_value())) {
            long notification_id = i.getLongExtra(Params.ID.get_value(), -1);
            db.update_read_status(notification_id,1);
            NotificationManager nMgr = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
            nMgr.cancel((int) notification_id);
        }
        if (i.hasExtra(Params.FROM_NOTIFICATIONS.get_value())) {
            from_notification = i.getBooleanExtra(Params.FROM_NOTIFICATIONS.get_value(), false);

        }
    }

    private void init_views() {
        actionbar = (Toolbar) findViewById(R.id.toolbar);
        actionbar.setTitle("Feed");
        actionbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (comment_view.getVisibility() == View.VISIBLE) {
                    toggle_menu(false);
                    return;
                }
                if (ll_wheel.getVisibility() == View.VISIBLE) {
                    toggle_rate(false);
                    return;
                }
                go_back();
            }
        });
        Toolbar comment_toolbar = (Toolbar) findViewById(R.id.comment_toolbar);
        comment_toolbar.setNavigationIcon(R.drawable.back);
        comment_toolbar.setTitle("Comments");

        image = (CircleImage) findViewById(R.id.icon);
        selfie = (ImageView) findViewById(R.id.image);
        name = (TextB) findViewById(R.id.name);
        status = (Text) findViewById(R.id.status);
        time = (Text) findViewById(R.id.time);

        wheelMenu = (WheelMenu)findViewById(R.id.rate_wheel);
        rate_icon = (ImageView) findViewById(R.id.rate_icon);
        comment_icon = (ImageView) findViewById(R.id.comment_icon);
        camera = (ImageView)findViewById(R.id.camera);
        camera.setOnClickListener(this);
        rate_icon.setOnClickListener(this);
        comment_icon.setOnClickListener(this);


        rate_list = (RecyclerView) findViewById(R.id.rate_list);
        rate_list.setHasFixedSize(true);
        GridLayoutManager llm1 = new GridLayoutManager(this, 1);
        llm1.setOrientation(GridLayoutManager.HORIZONTAL);
        rate_list.setLayoutManager(llm1);

        comment_view = (RelativeLayout) findViewById(R.id.comment_view);
        comment_list = (RecyclerView) findViewById(R.id.comment_list);
        comment = (EText) findViewById(R.id.comment);
        rate_count1 = (Text) findViewById(R.id.rate_count_one);
        rate_count2 = (Text) findViewById(R.id.rate_count_two);
        comment_count1 = (Text) findViewById(R.id.comment_count_one);
        comment_count2 = (Text) findViewById(R.id.comment_count_two);


        no_comments = (Text) findViewById(R.id.no_comments);
        TextB send = (TextB) findViewById(R.id.send);
        send.setOnClickListener(this);
        ll_wheel = (RelativeLayout) findViewById(R.id.ll_wheel);

        LinearLayoutManager llm2 = new LinearLayoutManager(this);
        llm2.setOrientation(LinearLayoutManager.VERTICAL);
        comment_list.setLayoutManager(llm2);
        comment_list.addItemDecoration(new DividerItemDecorator(this, DividerItemDecorator.VERTICAL_LIST));

        comment_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggle_menu(false);

            }
        });

        selfie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Activity_Selfie.this,Activity_Large_Image.class).putExtra(Params.IMAGE.get_value(),feed.getImage_url()));
            }
        });
    }

    private void update_views() {
        if(feed != null) {
            user = db.Get_User_Details(feed.getUser_id());
            display_image(feed.getImage_url(), selfie, false);
            display_image(user.getImage(), image, true);
            name.setText(user.getF_name());
            String s = feed.getStatus();
            if( s != null && s.length() > 0)
                s = s.replaceAll("(#[A-Za-z0-9_-]+)",
                        "<font color='#2980b9'>" + "$0" + "</font>");
            else
            s = "";
            status.setText(Html.fromHtml(s));
            time.setText(C.getDateDifference(C.UTC_to_Local(feed.getTime())));
            rate_count1.setText(String.valueOf(feed.getRating()));
            comment_count1.setText(String.valueOf(feed.getComment_count()));
            if (feed.getMy_rating() > 0) {
                rate_icon.setBackgroundResource(R.drawable.like_red);
            } else
                rate_icon.setBackgroundResource(R.drawable.like_normal);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(selfie_post_Receiver,new IntentFilter(Activity_Feeds.POST_STATE));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(selfie_post_Receiver);
    }

    @Override
    public void onBackPressed() {
        if (comment_view.getVisibility() == View.VISIBLE) {
            toggle_menu(false);
            camera.setVisibility(View.VISIBLE);
            return;
        }
        if (ll_wheel.getVisibility() == View.VISIBLE) {

            toggle_rate(false);
            return;
        }
        go_back();
    }
    private void go_back() {
            if(!from_notification)
            {
                Intent i = new Intent(this, Activity_Feeds.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);
            }

            super.onBackPressed();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

       return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.camera:
                startActivity(new Intent(this, Activity_Edit_First.class));
                break;
            case R.id.send:
                String txt = comment.getText().toString().trim();
                if (txt.length() > 0) {
                    comment.setText("");
                    no_comments.setVisibility(View.GONE);
                    update_comment_list(txt);

                } else {
                    C.alert(this, "Please enter a text");
                }
                break;
            case R.id.ll_wheel:
                toggle_rate(false);
                break;

            case R.id.rate_icon:

                wheelMenu.setDivCount(10);
                wheelMenu.setWheelImage(R.drawable.rating);

                wheelMenu.setWheelChangeListener(new WheelMenu.WheelChangeListener() {
                    @Override
                    public void onSelectionChange(WheelMenu v, int selectedPosition) {
                        toggle_rate(false);
                        new Update_Rating(Activity_Selfie.this,-1, feed.getSelfie_id(), selectedPosition).execute();
                        feed.setMy_rating(selectedPosition);
                        rate_icon.setBackgroundResource(R.drawable.like_red);


                    }
                });
                int rate = feed.getMy_rating();
                if (rate == 0)
                    rate = 1;
                wheelMenu.setAlternateTopDiv(rate);
                toggle_rate(true);
                break;
            case R.id.comment_icon:
                open_comments();
                break;


        }
    }

    private void open_comments() {

        if (user != null) {
            rate_list.setAdapter(new Rating_Adapter(getApplicationContext(), feed.getUsers_rating()));
            comment_adapter = new Comment_Adapter(this, feed.getUsers_comment());
            comment_list.setAdapter(comment_adapter);
            if (comment_adapter.getItemCount() > 0) {
                no_comments.setVisibility(View.GONE);
                comment_list.smoothScrollToPosition(comment_adapter.getItemCount() - 1);

            } else {
                no_comments.setVisibility(View.VISIBLE);
                comment.requestFocus();
            }


            rate_count2.setText("Likes (" + String.valueOf(feed.getRating_count()) + ")");
            camera.setVisibility(View.GONE);
            toggle_menu(true);
        }
    }

    private void update_comment_list(String txt) {
        Comment comment = null;
        long comment_id = 0;
        if (txt != null) {
            try {
                comment_id = new Update_comment(this, feed.getSelfie_id(), txt).execute().get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            comment = new Comment(App.getMyId(),null,null, txt, C.get_UTC_date(), comment_id);
            feed.add_comment(comment);
            comment_count1.setText(String.valueOf(feed.getComment_count()));
        }

        if (comment_adapter == null) {
            comment_adapter = new Comment_Adapter(this, feed.getUsers_comment());
            comment_list.setAdapter(comment_adapter);
        } else {
            ArrayList<Comment> comments = feed.getUsers_comment();
            comment_adapter.update_items(comment);
            comment_list.smoothScrollToPosition(comments.size() - 1);

        }


    }

    //////////////////////////////get selfie ratings and comments
    private class get_Selfie_info extends AsyncTask<Long, Long, Boolean> {
        private MyProgress myProgress;
        private long id = -1;

        public get_Selfie_info(long id) {
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            myProgress = new MyProgress(Activity_Selfie.this);
            myProgress.setCancelable(false);
            myProgress.setTitle("Loading...");
            myProgress.show();
        }

        @Override
        protected Boolean doInBackground(Long... params) {

            return get_info();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                update_views();
            } else
                C.alert(getApplicationContext(), "Error retrieving details, please try after some time");
            if(myProgress != null)
                myProgress.dismiss();

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        public boolean get_info() {
            boolean success = false;
            JSONParser parser = new JSONParser();
            JSONObject response = null;
            try {

                response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                        new Server_Params().get_selfie_info(id));
                if (response != null) {

                    if (response.has(Params.STATUS.get_value())) {
                        int status = response.getInt(Params.STATUS.get_value());

                        if (status == 200) {


                            JSONArray data = response.getJSONArray(Params.DATA.get_value());
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject image_data = data.getJSONObject(i);
                                JSONObject image = image_data.getJSONObject(Params.IMAGE.get_value());

                             /*   JSONArray tags_array = image_data.getJSONArray(Params.TAGS.get_value());
                                ArrayList<Tag> tags = new ArrayList<>();
                                for (int j = 0; j < tags_array.length(); j++) {
                                    JSONObject tag = tags_array.getJSONObject(j);
                                    tags.add(new Tag(tag.getInt(Params.TAG.get_value())));
                                }
                             */
                                JSONArray rating_array = image_data.getJSONArray(Params.RATING.get_value());
                                ArrayList<Rating> ratings = new ArrayList<>();
                                for (int j = 0; j < rating_array.length(); j++) {
                                    JSONObject rating = rating_array.getJSONObject(j);
                                    ratings.add(new Rating(rating.getLong(Params.USER_ID.get_value()),
                                            rating.getString(Params.IMAGE.get_value()),
                                            rating.getInt(Params.RATING.get_value()),
                                            rating.getString(Params.DATE.get_value())
                                    ));
                                }

                                JSONArray comment_array = image_data.getJSONArray(Params.COMMENT.get_value());
                                ArrayList<Comment> comments = new ArrayList<>();
                                for (int j = 0; j < comment_array.length(); j++) {
                                    JSONObject comment = comment_array.getJSONObject(j);
                                    comments.add(new Comment(comment.getLong(Params.USER_ID.get_value()),
                                            comment.getString(Params.FIRST_NAME.get_value()),
                                            comment.getString(Params.IMAGE.get_value()),
                                            comment.getString(Params.COMMENT.get_value()),
                                            comment.getString(Params.DATE.get_value()),
                                            comment.getLong(Params.COMMENT_ID.get_value())
                                    ));
                                }

                                feed = new Feed(image.getLong(Params.USER_ID.get_value()),
                                        image.getLong(Params.ID.get_value()),
                                        image.getString(Params.IMAGE.get_value()),
                                        image.getString(Params.STATUS.get_value()),
                                        image.getString(Params.DATE.get_value()),
                                        image_data.getInt(Params.MY_RATING.get_value()),
                                        image.getInt(Params.RATING.get_value()),
                                        null, ratings, comments,false);
                            }

                            success = true;

                        }
                    }


                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return success;
        }

    }
}
