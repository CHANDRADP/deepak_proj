package com.dpc.selfie.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dpc.selfie.Act.Activity_Contest_Brand_Offers;
import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.customs.CircleImage;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.BrandCategory;
import com.dpc.selfie.models.Celebrity;
import com.dpc.selfie.models.SearchData;
import com.dpc.selfie.models.Server_Params;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Edwin on 28/02/2015.
 */
public class BrandCategory_Adapter  extends RecyclerView.Adapter<BrandCategory_Adapter.ViewHolder> {


    private Context context;
    private String image_path;
    private ArrayList<BrandCategory> category_list = new ArrayList<>();



    public BrandCategory_Adapter(Context context, ArrayList<BrandCategory> category_list, String image_path) {
        super();
        this.context = context;
        this.category_list = category_list;
        this.image_path = image_path;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.category_grid_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {


        BrandCategory bcategory = category_list.get(position);
        viewHolder.text.setText(bcategory.getCategory_name());
        display_image(bcategory.getCategory_image_url(),viewHolder.imgThumbnail, false);
        viewHolder.imgThumbnail.setScaleType(ImageView.ScaleType.FIT_XY);


        viewHolder.category_body.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BrandCategory category = category_list.get(position);
                Intent intent = new Intent(context, Activity_Contest_Brand_Offers.class);
                intent.putExtra(Params.CATEGORY_ID_SELECTED.get_value(), category.getCategory_id());
                intent.putExtra("image_path", image_path);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {

        return category_list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView imgThumbnail;
        public Text text;
        public RelativeLayout category_body;

        public ViewHolder(View itemView) {
            super(itemView);
            imgThumbnail = (ImageView)itemView.findViewById(R.id.img_thumbnail);
            text = (Text)itemView.findViewById(R.id.text);
            category_body = (RelativeLayout) itemView.findViewById(R.id.category_body);
        }
    }

    protected void display_image(String url,View v,boolean is_circle_image)
    {

        try {
            Picasso.with(context)
                    .load(url)
                    .placeholder(R.drawable.ic_action_person)
                    .error(R.drawable.ic_action_person)
                    .into(is_circle_image ? (CircleImage)v : (ImageView)(v));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }





}