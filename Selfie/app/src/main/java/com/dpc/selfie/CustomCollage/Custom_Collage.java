package com.dpc.selfie.CustomCollage;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dpc.selfie.Act.Activity_Collage;
import com.dpc.selfie.Act.Activity_Edit_First;
import com.dpc.selfie.App;
import com.dpc.selfie.R;
import com.dpc.selfie.customs.BaseActivity;
import com.dpc.selfie.enums.Params;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

public class Custom_Collage extends BaseActivity {

    private PhotoSortrView photoSorter;
    private RelativeLayout main;
    private BroadcastReceiver broadcastReceiver;

    int ADDIMAGES = 777;
    private ArrayList selected_ids = new ArrayList();
    private ArrayList added_ids = new ArrayList();
    private ArrayList<Drawable> selected_drawables = new ArrayList();
    private ArrayList<Bitmap> selected_bitmaps = new ArrayList();

    public static int TOTAL_IMAGES = 0;

    float topLeftX, topLeftY, topRightX, topRightY, bottomLeftX, bottomLeftY, bottomRightX, bottomRightY;
    float leftCenterX, leftCenterY, topCenterX, topCenterY, rightCenterX, rightCenterY, bottomCenterX, bottomCenterY;
    float mainCenterX, mainCenterY;
    float overlapImageTopLeftX, overlapImageTopLeftY, overlapImageTopRightX, overlapImageTopRightY;
    float overlapImageBottomLeftX, overlapImageBottomLeftY, overlapImageBottomRightX, overlapImageBottomRightY;
    float firstCenterX, firstCenterY, secondCenterX, secondCenterY, thirdCenterX, thirdCenterY, forthCenterX, forthCenterY;
    float imageWidth, imageHeight;

    Bitmap paddedBitmap = null;

    boolean dynamic = false;

    public static float VIEW_WIDTH = 0;
    public static float VIEW_HEIGHT = 0;

    RelativeLayout trashLayout;

    //int[] images = { R.drawable.sample1, R.drawable.sample2,
    //  R.drawable.sample3 };


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom__collage);
        photoSorter = (PhotoSortrView) findViewById(R.id.photosortr);
        main = (RelativeLayout) findViewById(R.id.Main);

        trashLayout = (RelativeLayout) findViewById(R.id.trashLayout);

        trashLayout.setVisibility(View.GONE);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        final Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y - getActionBarHeight()- getStatusBarHeight();
        /*DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels -  - getActionBarHeight()- getStatusBarHeight() ;
        int width = displaymetrics.widthPixels;*/

        VIEW_WIDTH = width;
        VIEW_HEIGHT = height;
        //int height = size.x;

        //FrameLayout.LayoutParams parms = new FrameLayout.LayoutParams(width,width);
        //main.setLayoutParams(parms);

        broadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive (Context context, Intent intent)
            {
                String color = null;

               // dynamic = intent.getBooleanExtra("dynamic", false);

                if(intent.hasExtra("released"))
                {
                    boolean released = intent.getBooleanExtra("released", false);
                    if(released == false)
                    {
                        Vibrator mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                        mVibrator.vibrate(50);
                        if(dynamic)
                        {
                            trashLayout.setVisibility(View.VISIBLE);
                            int canvas_height = com.dpc.selfie.CustomCollage.PhotoSortrView.CANVAS_HEIGHT;
                            int delete_area_height = canvas_height/10;
                            RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,delete_area_height);
                            parms.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,R.id.Main);
                            trashLayout.setLayoutParams(parms);
                        }
                    }
                    else if(released && dynamic)
                    {
                        trashLayout.setVisibility(View.GONE);
                        invalidateOptionsMenu();
                    }
                }
                if(intent.hasExtra("swap"))
                {
                    boolean swap = intent.getBooleanExtra("swap", false);
                    if(swap == true)
                    {
                        int index1 = intent.getIntExtra("index1",0);
                        int index2 = intent.getIntExtra("index2",0);
                        //Log.d("Swap between", Integer.toString(index1)+" ,"+Integer.toString(index2));
                        Collections.swap(selected_bitmaps, index1, index2);
                        photoSorter.clearCanvasAllImages();
                        //drawSwappedImages(index1, index2);
                        drawImagesNew();
                    }

                }

                if(intent.hasExtra("changeColor"))
                {
                    boolean colorChange = intent.getBooleanExtra("changeColor", false);
                    if(colorChange == false)
                    {
                        color = intent.getStringExtra("color");
                    }
                    else if(colorChange == true)
                    {
                        color = intent.getStringExtra("color");
                        //Log.d("Change Background","Hi");
                        if(color.equalsIgnoreCase("red"))
                        {
                            trashLayout.setBackgroundResource(R.color.red_transparent);
                        }
                        else if(color.equalsIgnoreCase("blue"))
                        {
                            trashLayout.setBackgroundResource(R.color.blue_transparent);
                        }

                    }
                }
            }
        };

        topLeftX = 0f;
        topLeftY = 0f;
        topRightX = width;
        topRightY = 0f;
        bottomLeftX = 0f;
        bottomLeftY = height;
        bottomRightX = width;
        bottomRightY = height;
        leftCenterX = 0;
        leftCenterY = height/2;
        topCenterX = width/2;
        topCenterY = 0;
        rightCenterX = width;
        rightCenterY = height/2;
        bottomCenterX = width/2;
        bottomCenterY = height;
        mainCenterX = width/2;    // 5th image center X
        mainCenterY = height/2;   // 5th image center Y
        firstCenterX = mainCenterX/2;    // 1st image center X
        firstCenterY = mainCenterY/2;    // 1st image center Y
        secondCenterX = (topCenterX + rightCenterX)/2;   // 2nd image center X
        secondCenterY = (topCenterY + rightCenterY)/2;   // 2nd image center Y
        thirdCenterX = (leftCenterX + bottomCenterX)/2;   // 3rd image center X
        thirdCenterY = (leftCenterY + bottomCenterY)/2;   // 3rd image center Y
        forthCenterX = (mainCenterX + width)/2;   // 4th image center X
        forthCenterY = (mainCenterY + height)/2;   // 4th image center Y

        overlapImageTopLeftX = mainCenterX/2;
        overlapImageTopLeftY = mainCenterY/2;
        overlapImageTopRightX = (topCenterX+rightCenterX)/2;
        overlapImageTopRightY = rightCenterY/2;
        overlapImageBottomLeftX = bottomCenterX/2;
        overlapImageBottomLeftY = (leftCenterY+bottomCenterY)/2;
        overlapImageBottomRightX = (mainCenterX+bottomRightX)/2;
        overlapImageBottomRightY = (mainCenterY+bottomRightY)/2;

        imageWidth = width/2;
        imageHeight = height/2;

        //Log.d("Image 1 Center", Float.toString(firstCenterX)+" ,"+Float.toString(firstCenterY));
        //Log.d("Image 2 Center", Float.toString(secondCenterX)+" ,"+Float.toString(secondCenterY));
        //Log.d("Image 3 Center", Float.toString(thirdCenterX)+" ,"+Float.toString(thirdCenterY));
        //Log.d("Image 4 Center", Float.toString(forthCenterX)+" ,"+Float.toString(forthCenterY));
        //Log.d("Image 5 Center", Float.toString(mainCenterX)+" ,"+Float.toString(mainCenterY));

        Intent intent = getIntent();
        selected_ids = intent.getStringArrayListExtra("selected_ids");
        dynamic = intent.getBooleanExtra("dynamic", false);
        TOTAL_IMAGES = selected_ids.size();
        drawImages(selected_ids);

        if(dynamic)
        {
            setCustomTitle("Dynamic Collage");
        }
        else
        {
            setCustomTitle("Custom Collage");
        }
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public int getActionBarHeight() {
        int actionBarHeight = 0;
        TypedValue tv = new TypedValue();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv,
                    true))
                actionBarHeight = TypedValue.complexToDimensionPixelSize(
                        tv.data, getResources().getDisplayMetrics());
        } else {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,
                    getResources().getDisplayMetrics());
        }
        return actionBarHeight;
    }

    public String create_image()
    {
        File file = null;
        View content = findViewById(R.id.photosortr);
        content.setDrawingCacheEnabled(true);
        Bitmap bitmap = content.getDrawingCache();
        try
        {
            file = new File(App.APP_FOLDER+"/Selfie"+System.currentTimeMillis()+".jpg");
            {
                if(!file.exists())
                {
                    file.createNewFile();
                }
                FileOutputStream ostream = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, ostream);
                ostream.close();
                content.invalidate();
                Log.d("Image File Path", file.getAbsolutePath());

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return file.getAbsolutePath();
    }



    public void drawImages(ArrayList selected_ids)
    {

        ArrayList<Float> list_cx = new ArrayList<>();
        list_cx.add(firstCenterX);
        list_cx.add(secondCenterX);
        list_cx.add(thirdCenterX);
        list_cx.add(forthCenterX);
        list_cx.add(mainCenterX);

        ArrayList<Float> list_cy = new ArrayList<>();
        list_cy.add(firstCenterY);
        list_cy.add(secondCenterY);
        list_cy.add(thirdCenterY);
        list_cy.add(forthCenterY);
        list_cy.add(mainCenterY);

        for(int i=0;i<selected_ids.size();i++)
        {
            // photoSorter.addImages(DynamicCollage.this,getResources().getDrawable(images[i]), false);


            URL url = null;
            try
            {
                url = new URL(Activity_Collage.selfies.get(Integer.parseInt(String.valueOf(selected_ids.get(i)))).getImage_url());
                Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());

                BitmapDrawable bd = new BitmapDrawable(getApplicationContext().getResources(), image);
                padBitmap(image);
                Bitmap m1 = paddedBitmap ;
                selected_bitmaps.add(m1);
                paddedBitmap = null; // to free memory


                Drawable drawable = new BitmapDrawable(getResources(), m1);

                int resID = getResources().getIdentifier("drawable",
                        "drawable", getPackageName());
                photoSorter.addImages(Custom_Collage.this,drawable, false, list_cx.get(i), list_cy.get(i), imageWidth, imageHeight, i, dynamic, false);

                selected_drawables.add(bd);

                System.gc();
                image = null;
                m1=null;

            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void drawImagesNew()
    {

        ArrayList<Float> list_cx = new ArrayList<>();
        list_cx.add(firstCenterX);
        list_cx.add(secondCenterX);
        list_cx.add(thirdCenterX);
        list_cx.add(forthCenterX);
        list_cx.add(mainCenterX);

        ArrayList<Float> list_cy = new ArrayList<>();
        list_cy.add(firstCenterY);
        list_cy.add(secondCenterY);
        list_cy.add(thirdCenterY);
        list_cy.add(forthCenterY);
        list_cy.add(mainCenterY);

        for(int i=0;i<selected_ids.size();i++)
        {
            // photoSorter.addImages(DynamicCollage.this,getResources().getDrawable(images[i]), false);





                Drawable drawable = new BitmapDrawable(getResources(),selected_bitmaps.get(i));


                photoSorter.addImages(Custom_Collage.this,drawable, false, list_cx.get(i), list_cy.get(i), imageWidth, imageHeight, i, dynamic, false);





        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(dynamic && photoSorter.getTotalImages()<5)
        {
            Log.d("Photo", Integer.toString(photoSorter.getTotalImages()));
            getMenuInflater().inflate(R.menu.dynamic_collage_menu, menu);
        }
        else
        {
            getMenuInflater().inflate(R.menu.menu_custom__collage, menu);
        }

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:

                super.onBackPressed();
                break;

            case R.id.AddImage:
                Log.d("Total Images", Integer.toString(photoSorter.getTotalImages()));
                Intent intent=new Intent(Custom_Collage.this,Activity_Collage.class);
                intent.putExtra(Params.USER_ID.get_value(), App.getMyId());
                intent.putExtra("AddImages",true);
                intent.putExtra("PresentCount", photoSorter.getTotalImages());
                startActivityForResult(intent, ADDIMAGES);// Activity is started with requestCode 777

                Toast.makeText(getApplicationContext(), "Add",Toast.LENGTH_LONG).show();
                break;

            case R.id.Save:
                Toast.makeText(getApplicationContext(), "Save",Toast.LENGTH_LONG).show();
                String filepath = create_image();
                Intent intent1=new Intent(Custom_Collage.this,Activity_Edit_First.class);
                intent1.putExtra("FilePath",filepath );
                intent1.putExtra("url",filepath);
                intent1.putExtra("CollageActive",true);
                startActivity(intent1);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==ADDIMAGES)
        {
            added_ids =data.getStringArrayListExtra("selected_ids");
            if(added_ids.size()>0)
            {
                drawImages(added_ids);
            }
            invalidateOptionsMenu();
        }
    }


    public void padBitmap(Bitmap bitmap)
    {
        int paddingX=20;
        int paddingY=20;


       /* if (bitmap.getWidth() == bitmap.getHeight())
        {
            paddingX = 30;
            paddingY = 30;
        }
        else if (bitmap.getWidth() > bitmap.getHeight())
        {
            paddingX = 30;//(bitmap.getWidth() - bitmap.getHeight())/2;
            paddingY = 30;//(bitmap.getWidth() - bitmap.getHeight())/2;
        }
        else
        {
            paddingX = 30;//(bitmap.getHeight() - bitmap.getWidth())/2;
            paddingY = 30;//(bitmap.getHeight() - bitmap.getWidth())/2;
        }*/

            paddedBitmap = Bitmap.createBitmap(
                    bitmap.getWidth() + paddingX,
                    bitmap.getHeight() + paddingY,
                    Bitmap.Config.ARGB_8888);



        Canvas canvas = new Canvas(paddedBitmap);
        canvas.drawARGB(255,255, 255, 255); // this represents app blue color
        canvas.drawBitmap(
                bitmap,
                paddingX / 2,
                paddingY / 2,
                new Paint(Paint.FILTER_BITMAP_FLAG));


        //paddedBitmap = null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter(com.dpc.selfie.CustomCollage.PhotoSortrView.BROADCAST_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }


}
