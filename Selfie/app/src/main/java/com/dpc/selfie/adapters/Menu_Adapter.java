package com.dpc.selfie.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.dpc.selfie.App;
import com.dpc.selfie.R;
import com.dpc.selfie.Utils.DB;
import com.dpc.selfie.customs.Text;

/**
 * Created by D-P-C on 13-Mar-15.
 */
public class Menu_Adapter extends BaseAdapter {
    private  int[] icons = new int[]{R.drawable.profile_bg,R.drawable.notification_bg, R.drawable.friends_bg, R.drawable.settings_bg,R.drawable.profile_bg, R.drawable.notification_bg };
    private  String[] items = new String[]{"Profile","Notifications", "Friends", "Settings","Create Collage", "Brands"};
    private Context context;
    private ViewHolder holder;

    public Menu_Adapter(Context context) {
        this.context = context;
    }

    public Menu_Adapter(Context context,int[] icons,String[] details) {
        this.context = context;
        this.icons = icons;
        this.items = details;
    }

    private class ViewHolder {
        ImageView icon;
        Text title,count;
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        View v = convertView;
        if (v == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = mInflater.inflate(R.layout.item_menu, null);
            holder = new ViewHolder();
            holder.icon = (ImageView) v.findViewById(R.id.icon);
            holder.title = (Text) v.findViewById(R.id.details);
            holder.count = (Text) v.findViewById(R.id.count);
            v.setTag(holder);
        } else
            holder = (ViewHolder) v.getTag();
        if(position == 1)
        {
            holder.count.setVisibility(View.VISIBLE);
            holder.count.setText(String.valueOf(new DB(context).get_notifications_unread_count()));
        }
        else
            holder.count.setVisibility(View.GONE);

        holder.icon.setBackgroundResource(icons[position]);
        holder.title.setText(items[position]);

        return v;
    }

}