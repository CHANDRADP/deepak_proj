package com.dpc.selfie.models;

/**
 * Created by D-P-C on 31-Jan-15.
 */
public class User {
    private String[] gen = new String[]{"", "Male", "Female"};
    private long userid = 0,fb_id = 0;
    private int  gender = 0, points = 0;
    private boolean selected = false;
    private String f_name = null,m_name = null, l_name = null, display_name = null, number = null, email = null, dob = null, status = null, thumbnail = null, image = null, date = null, badges = "0";

    public User(long userid, long fb_id, int gender, int points, String f_name,String m_name, String l_name, String display_name, String number, String email, String dob, String status, String thumbnail, String image, String date, String badges) {
        this.userid = userid;
        this.fb_id = fb_id;
        this.gender = gender;
        this.points = points;
        this.f_name = f_name;
        this.m_name = m_name;
        this.l_name = l_name;
        this.display_name = display_name;
        this.number = number;
        this.email = email;
        this.dob = dob;
        this.status = status;
        this.thumbnail = thumbnail;
        this.image = image;
        this.date = date;
        this.badges = badges;
    }

    public User(long fb_id, String display_name, String image) {
        this.fb_id = fb_id;
        this.display_name = display_name;
        this.image = image;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public long getFb_id() {
        return fb_id;
    }

    public void setFb_id(int fb_id) {
        this.fb_id = fb_id;
    }

    public int getGenderValue() {
        return gender;
    }

    public String getGender() {
        return gen[gender];
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String[] getBadges_Array() {
        String[] badge = badges.split(";");
        return badge;
    }
    public String getBadges()
    {
        return badges;
    }

    public void setBadge_type(String badges) {
        this.badges = badges;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getM_name() {
        return m_name;
    }

    public void setM_name(String m_name) {
        this.m_name = m_name;
    }

    public String getL_name() {
        return l_name;
    }

    public void setL_name(String l_name) {
        this.l_name = l_name;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
