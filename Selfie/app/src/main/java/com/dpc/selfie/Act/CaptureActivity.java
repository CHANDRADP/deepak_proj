package com.dpc.selfie.Act;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import com.dpc.selfie.App;
import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.customs.BaseActivity;
import com.dpc.selfie.customs.Camera_Preview;
import com.dpc.selfie.customs.Text;
import com.facebook.UiLifecycleHelper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class CaptureActivity extends BaseActivity {

    private File outFile;

    private RelativeLayout camera_view;

    private Camera_Preview preview;


    private Bitmap bitmap = null;

    private boolean image_from_gallery = false;

    private ImageView flash, imageview;

    SurfaceView surfaceView;

    private Text time;

    private SeekBar time_bar;

    private int self_time = 10;

    private static int RESULT_LOAD_IMG = 111;

    private Handler handler;

    private Runnable runnable;


    public static final int FLIP_VERTICAL = 1;
    public static final int FLIP_HORIZONTAL = 2;



    /////////////////////



    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("");

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.capture_activity);
        this.create_image();
        init_views();


        if(!this.checkCameraHardware())
        {
            C.alert(getApplicationContext(),"No camera on this device");
            finish();
            return;
        }

        boolean is_front = true;
        if(findFrontFacingCamera() < 0)
        {
            is_front = false;
        }

         imageview = (ImageView) findViewById(R.id.imageView);
         surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
         preview = new Camera_Preview(this, (SurfaceView) findViewById(R.id.surfaceView), is_front,checkFlash());
         preview.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
         ((RelativeLayout) findViewById(R.id.camera)).addView(preview);
         preview.setKeepScreenOn(true);



         RelativeLayout.LayoutParams params =  new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
         params.addRule(RelativeLayout. BELOW, R.id.camera);
         View v = getLayoutInflater().inflate(R.layout.cam_buttons,null);
         v.setLayoutParams(params);
         ((RelativeLayout) findViewById(R.id.camera)).addView(v);
         final ImageView gallery = (ImageView) v.findViewById(R.id.gallery);
         ImageView collage = (ImageView) v.findViewById(R.id.collage);

        time_bar = (SeekBar) v.findViewById(R.id.timer_bar);
        time = (Text)findViewById(R.id.time);

        time.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                preview.getmCamera().autoFocus(new Camera.AutoFocusCallback() {
                    @Override
                    public void onAutoFocus(boolean arg0, Camera camera) {
                        preview.getmCamera().takePicture(shutterCallback, rawCallback, jpegCallback);
                    }
                });
            }
        });

        collage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // C.collage_type_popup(CaptureActivity.this);
                Intent intent = new Intent(CaptureActivity.this, Activity_Collage_Templates_Categories.class);
                startActivity(intent);
            }
        });

        time_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {
                if (self_time > 0 )
                {
                    time.setBackgroundResource(R.color.transparent);
                    time.setText(String.valueOf(self_time));
                    time.setEnabled(false);
                    C.alert(getApplicationContext(),String.valueOf(self_time));
                    handler = new Handler();
                    runnable = new Runnable() {
                        public void run() {
                            if (self_time == 0) {
                                preview.getmCamera().autoFocus(new Camera.AutoFocusCallback() {
                                    @Override
                                    public void onAutoFocus(boolean arg0, Camera camera) {
                                        preview.getmCamera().takePicture(shutterCallback, rawCallback, jpegCallback);
                                    }
                                });
                            } else {
                                handler.postDelayed(runnable, 1000);
                                --self_time;
                                time.setText(String.valueOf(self_time));
                            }

                        }
                    };
                    handler.postDelayed(runnable, 1000);
                }
                else
                {
                    if(handler != null && runnable != null)
                    {
                        handler.removeCallbacks(runnable);
                    }
                    time.setText("");
                    time.setBackgroundResource(R.drawable.take_photo);
                    time.setEnabled(true);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                self_time = progress;
                time.setBackgroundResource(R.color.transparent);
                time.setText(String.valueOf(self_time));
                time.setEnabled(false);

            }
        });
        ((ImageView) v.findViewById(R.id.timer)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                toggle_timer();
            }
        });
        if(is_front) {
            ((ImageView) v.findViewById(R.id.change)).setVisibility(View.VISIBLE);
            ((ImageView) v.findViewById(R.id.gallery)).setVisibility(View.VISIBLE);
            ((ImageView) v.findViewById(R.id.change)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    preview.change_camera();
                    preview.getmCamera().startPreview();
                    if (preview.is_front_enabled()) {
                        flash.setVisibility(View.GONE);
                    } else {
                        flash.setVisibility(View.VISIBLE);
                        gallery.setVisibility(View.GONE);
                    }
                }
            });

            ((ImageView) v.findViewById(R.id.gallery)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    image_from_gallery = true;
                    // Create intent to Open Image applications like Gallery, Google Photos
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
                }
            });
        }
        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);


        if(checkFlash())
        {
            flash = (ImageView) v.findViewById(R.id.flash);
            flash.setBackgroundResource(R.drawable.flash_off);
            flash.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    preview.toggle_flash();
                    preview.getmCamera().startPreview();
                    if(preview.is_flash_enabled())
                    {
                        flash.setBackgroundResource(R.drawable.flash_on);
                    }
                    else
                    {
                        flash.setBackgroundResource(R.drawable.flash_off);
                    }
                }
            });
        }



        preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(preview.getmCamera() != null)
                    preview.getmCamera().autoFocus(new Camera.AutoFocusCallback() {
                        @Override
                        public void onAutoFocus(boolean arg0, Camera camera) {
                        }
                    });
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMG && data != null && data.getData() != null) {

            Uri uri = data.getData();
            byte[] inputData = new byte[0];
            InputStream iStream = null;
            try {
                iStream = getContentResolver().openInputStream(uri);
                inputData = getBytes(iStream);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),uri);


                // bitmap = convertToMutable(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }

            camera_view.setVisibility(View.GONE);
            new SaveImageTask().execute(inputData);
        }


    }


    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }




    private int findFrontFacingCamera() {
        int cameraId = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {

                cameraId = i;
                break;
            }
        }
        return cameraId;
    }



    private void toggle_timer()
    {
        if(time_bar.getVisibility() == View.GONE) {
            time_bar.setVisibility(View.VISIBLE);

        }
        else
        {
            time_bar.setVisibility(View.GONE);

        }
    }







    public Bitmap flipImage(Bitmap src, int type) {
        // create new matrix for transformation
        Matrix matrix = new Matrix();
        // if vertical
        if(type == FLIP_VERTICAL) {
            // y = y * -1
            matrix.preScale(1.0f, -1.0f);

        }
        // if horizonal
        else if(type == FLIP_HORIZONTAL) {
            // x = x * -1
            matrix.preScale(-1.0f, 1.0f);
            // unknown type
        } else {
            return null;
        }
        if(Camera_Preview.is_front)
        {
            matrix.postRotate(-90);
        }
        else
        {
            matrix.postRotate(-270);
        }

        // return transformed image
        return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
    }

    public Bitmap rotateImage(Bitmap src, int degrees) {
        // create new matrix for transformation
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        // return transformed image
        return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
    }





    /** Check if this device has a camera */
    private boolean checkCameraHardware() {
        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }
    private boolean checkFlash()
    {
        return getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    private void init_views() {

        camera_view = (RelativeLayout) findViewById(R.id.camera);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(outFile != null && outFile.exists())
        {
            outFile.delete();
        }

        File dir = new File(App.APP_FOLDER);
        if(dir.exists())
        {
            File[] files = dir.listFiles();
            if(files != null && files.length > 0)
            {
                for(int i = 0;i < files.length;i++)
                {
                    files[i].delete();
                }
            }
        }

        uiHelper.onDestroy();


    }



    @Override
    public void onBackPressed() {
        if(!App.is_new_user()) {

            super.onBackPressed();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_activity_edit_first, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Log.d("", String.valueOf(item.getItemId()));
        return super.onOptionsItemSelected(item);
    }

    //////////////////////////////// callbacks for camera
    Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() {
        public void onShutter() {

        }
    };

    Camera.PictureCallback rawCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {

        }
    };

    Camera.PictureCallback jpegCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            //myProgress = new MyProgress(CaptureActivity.this);
            //myProgress.setTitle("Saving image...");
            //myProgress.setCancelable(false);
           // myProgress.show();
            //camera_view.setVisibility(View.GONE);
            preview.getmCamera().stopPreview();
            preview = null;
            surfaceView.setVisibility(View.GONE);
            imageview.setVisibility(View.VISIBLE);
            try
            {
                BitmapFactory.Options options=new BitmapFactory.Options();// Create object of bitmapfactory's option method for further option use
                if(!Camera_Preview.is_front)
                {
                    options.inSampleSize = 2;
                    bitmap = rotateImage( BitmapFactory.decodeByteArray(data, 0, data.length,options),90);
                }
                else if(Camera_Preview.is_front)
                {
                    bitmap =flipImage( BitmapFactory.decodeByteArray(data, 0, data.length,options),1);
                }
                else if(!image_from_gallery)
                {
                    bitmap =flipImage( BitmapFactory.decodeByteArray(data, 0, data.length,options),1);
                }
                else
                {
                    bitmap = rotateImage( BitmapFactory.decodeByteArray(data, 0, data.length, options), -270);
                }

            } catch (OutOfMemoryError exception)
            {
                exception.printStackTrace();

            }
           Bitmap bitmap12= Bitmap.createScaledBitmap(bitmap, imageview.getWidth(), imageview.getHeight(), false);

            imageview.setImageBitmap(bitmap12);
            //imageview.setBackgroundResource(R.color.red);
            new SaveImageTask().execute(data);
        }
    };

    private void create_image()
    {
        File dir = new File(App.APP_FOLDER);
        dir.mkdirs();
        String fileName = String.format("%d.jpg", System.currentTimeMillis());
        outFile = new File(dir, fileName);
    }



    /////////////////////////save image
    public class SaveImageTask extends AsyncTask<byte[], Void, Boolean>
    {
        @Override
        protected Boolean doInBackground(byte[]... data)
        {

            try
            {
                FileOutputStream out = new FileOutputStream(outFile);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

                out.flush();
                out.close();

                bitmap.recycle();
                bitmap = null;
             //   bitmap = BitmapFactory.decodeFile(outFile.getAbsolutePath());
                //bitmap =  C.get_lowsize_bitmap(outFile.getAbsolutePath(), Camera_Preview.is_front ? 270 : 90);
                return true;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            super.onPostExecute(result);
           // getSupportActionBar().show();
            if (result)
            {
                  Intent i = new Intent(CaptureActivity.this,Activity_Edit_First.class);
                  i.putExtra("url",outFile.getAbsolutePath());
                  startActivity(i);
                  overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                  finish();

            }
            else
                C.alert(getApplicationContext(), "Failed to capture the photo, please try again");

        }
    }
}
