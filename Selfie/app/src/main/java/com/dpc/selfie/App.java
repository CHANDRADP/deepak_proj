package com.dpc.selfie;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;

import com.dpc.selfie.Utils.C;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.tasks.Update_App_version;
import com.google.android.gcm.GCMRegistrar;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.Picasso;

import java.io.File;

/**
 * Created by user on 31-Jan-15.
 */
public class App extends Application {

    private static SharedPreferences APP_DATA;

    private static final String MY_ID = "app_user_id";

    private static final String USERS_UPDATED = "u_updated";

    private static final String FB_LOGIN = "fb_login";

    private static final String SETTING_SOUND = "_sound",SETTING_VIBRATION = "_vibration",SETTING_PRIVACY = "_privacy",SETTING_REQUEST = "_request",SETTING_RATING = "_rating",SETTING_COMMENT = "_comment",SETTING_FEED ="_feed";

    public static final String APP_FOLDER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Selfie";


    @Override
    public void onCreate() {
        super.onCreate();

        APP_DATA = PreferenceManager.getDefaultSharedPreferences(this);
        register();
        Create_App_Folder();

        if(App.get_app_version() == null || !App.get_app_version().equals(App.get_version_from_manifest(this.getApplicationContext())))
        {
            new Update_App_version(this.getApplicationContext()).execute();
        }

    }


    public boolean Create_App_Folder()
    {
        boolean created = false;
        File folder = new File(APP_FOLDER);

        if (!folder.exists())
        {
           created = folder.mkdir();

        }
        else
        {
            created = true;
        }
        return created;
    }


    public static void set_app_version(Context context)
    {
        SharedPreferences.Editor editor = APP_DATA.edit();
        editor.putString(Params.APP_VERSION.get_value(), App.get_version_from_manifest(context));
        editor.commit();
    }
    public static String get_app_version()
    {
        return APP_DATA.getString(Params.APP_VERSION.get_value(), null);
    }

    public static String get_version_from_manifest(Context context)
    {
        String version = "1.0";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = String.valueOf(pInfo.versionName);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }

    public static boolean is_fb_loggedin()
    {
        return APP_DATA.getBoolean(App.FB_LOGIN, false);
    }
    public static void set_fb_login(boolean login)
    {
        SharedPreferences.Editor editor = APP_DATA.edit();
        editor.putBoolean(FB_LOGIN, login);
        editor.commit();
    }
    public static void set_user_mode(boolean new_user)
    {
        SharedPreferences.Editor editor = APP_DATA.edit();
        editor.putBoolean(Params.NEW_USER.get_value(), new_user);
        editor.commit();
    }
    public static boolean is_new_user()
    {
        return APP_DATA.getBoolean(Params.NEW_USER.get_value(), false);
    }
    public static  void gcm_updated(boolean update)
    {
        SharedPreferences.Editor editor = APP_DATA.edit();
        editor.putBoolean("UPDATE_GCM", update);
        editor.commit();
    }

    public static  boolean is_gcm_updated()
    {
        return APP_DATA.getBoolean("UPDATE_GCM", false);
    }
    public static  void update_gcm_id(String gcmid)
    {
        SharedPreferences.Editor editor = APP_DATA.edit();
        editor.putString(Params.PUSH_ID.get_value(), gcmid);
        editor.commit();
    }
    public static String get_gcm_id()
    {
        return APP_DATA.getString(Params.PUSH_ID.get_value(), null);
    }

    public static void setMyId(long id) {
        SharedPreferences.Editor editor = APP_DATA.edit();
        editor.putLong(MY_ID, id);
        editor.commit();
    }
    public static long getMyId() {
        return APP_DATA.getLong(MY_ID,0);
    }

    public static void users_updated(boolean updated) {
        SharedPreferences.Editor editor = APP_DATA.edit();
        editor.putBoolean(USERS_UPDATED, updated);
        editor.commit();
    }
    public static boolean is_users_updated() {
        return APP_DATA.getBoolean(USERS_UPDATED,false);
    }

    public static void setToken(String token) {
        SharedPreferences.Editor editor = APP_DATA.edit();
        editor.putString(Params.TOKEN.get_value(), token);
        editor.commit();
    }
    public static void erase_app_data()
    {
        SharedPreferences.Editor editor = APP_DATA.edit();
        editor.clear();
        editor.commit();
    }
    public static String getToken() {
        return APP_DATA.getString(Params.TOKEN.get_value(),null);
    }


    //////////////////////////////////app settings
    public static void update_sound(boolean value) {
        SharedPreferences.Editor editor = APP_DATA.edit();
        editor.putBoolean(SETTING_SOUND, value);
        editor.commit();
    }
    public static boolean get_sound() {
        return APP_DATA.getBoolean(SETTING_SOUND,true);
    }

    public static void update_vibration(boolean value) {
        SharedPreferences.Editor editor = APP_DATA.edit();
        editor.putBoolean(SETTING_VIBRATION, value);
        editor.commit();
    }
    public static boolean get_vibration() {
        return APP_DATA.getBoolean(SETTING_VIBRATION,true);
    }

    public static void update_privacy(boolean value) {
        SharedPreferences.Editor editor = APP_DATA.edit();
        editor.putBoolean(SETTING_PRIVACY, value);
        editor.commit();
    }
    public static boolean get_privacy() {
        return APP_DATA.getBoolean(SETTING_PRIVACY,true);
    }

    public static void update_feed(boolean value) {
        SharedPreferences.Editor editor = APP_DATA.edit();
        editor.putBoolean(SETTING_FEED, value);
        editor.commit();
    }
    public static boolean get_feed() {
        return APP_DATA.getBoolean(SETTING_FEED,true);
    }

    public static void update_request(boolean value) {
        SharedPreferences.Editor editor = APP_DATA.edit();
        editor.putBoolean(SETTING_REQUEST, value);
        editor.commit();
    }
    public static boolean get_request() {
        return APP_DATA.getBoolean(SETTING_REQUEST,true);
    }

    public static void update_rating(boolean value) {
        SharedPreferences.Editor editor = APP_DATA.edit();
        editor.putBoolean(SETTING_RATING, value);
        editor.commit();
    }
    public static boolean get_rating() {
        return APP_DATA.getBoolean(SETTING_RATING,true);
    }

    public static void update_comment(boolean value) {
        SharedPreferences.Editor editor = APP_DATA.edit();
        editor.putBoolean(SETTING_COMMENT, value);
        editor.commit();
    }
    public static boolean get_comment() {
        return APP_DATA.getBoolean(SETTING_COMMENT,true);
    }
    ///////////////////get GCM_ID
    private String gcmId = null;
    void register()
    {
        gcmId = GCMRegistrar.getRegistrationId(this.getApplicationContext());
        if (gcmId.equals("") || gcmId == null)
        {
            GCMRegistrar.register(this, C.SENDER_ID);
        }
        else
        {

            final Context context = this.getApplicationContext();
            if (GCMRegistrar.isRegisteredOnServer(this.getApplicationContext()))
            {

                gcmId = GCMRegistrar.getRegistrationId(this.getApplicationContext());
                App.update_gcm_id(gcmId);
            }
            else
            {

                GCMRegistrar.setRegisteredOnServer(context, true);

            }

        }
        Log.i("gcmId", "Device registered: regId = " + gcmId);
    }
}
