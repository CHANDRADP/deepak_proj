package com.dpc.selfie.Act;

import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.adapters.BrandCategory_Adapter;
import com.dpc.selfie.customs.BaseActivity;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.BrandCategory;
import com.dpc.selfie.models.Server_Params;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Activity_Contest_Category extends BaseActivity {

    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;
    private HashMap<Long,BrandCategory> brand_category_map = new HashMap<>();
    private ArrayList<BrandCategory> category_list = new ArrayList<>();
    private String image_path;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_activity_contest_category);

        // Calling the RecyclerView
        mRecyclerView = (RecyclerView)findViewById(R.id.category_list);
        mRecyclerView.setHasFixedSize(true);

        image_path = getIntent().getStringExtra("image_path");

        // The number of Columns
        mLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(mLayoutManager);

        brand_category_map.clear();
        new Get_All_Brand_Categories().execute();



    }

    public class Get_All_Brand_Categories  extends AsyncTask<String, Long, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {

            return get_all_brand_categories();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            mAdapter = new BrandCategory_Adapter(Activity_Contest_Category.this, category_list, image_path);
            mRecyclerView.setAdapter(mAdapter);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        public boolean get_all_brand_categories()
        {
            boolean success = false;
            JSONParser parser = new JSONParser();
            JSONObject response = null;
            try {

                response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                        new Server_Params().get_brand_category());
                if(response != null)
                {
                    Log.d("response", response.toString());
                    if(response.has(Params.STATUS.get_value()))
                    {
                        int status = response.getInt(Params.STATUS.get_value());
                        if (status == 200)
                        {


                            JSONObject obj = response.getJSONObject(Params.DATA.get_value());
                            JSONArray array = obj.getJSONArray("brand_categories");
                            brand_category_map = new C().parse_brand_category(array);

                            for(BrandCategory categories : brand_category_map.values())
                            {
                                category_list.add(categories);
                            }

                            success = true;




                        }
                    }
                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return success;
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity__contest__category, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case android.R.id.home:

                super.onBackPressed();
                break;
        }

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;

        }

        return super.onOptionsItemSelected(item);
    }
}