package com.dpc.selfie.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.dpc.selfie.App;
import com.dpc.selfie.R;
import com.dpc.selfie.Utils.DB;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.interfaces.OnCardClickListener;
import com.dpc.selfie.models.Setting;

import java.util.ArrayList;

/**
 * Created by D-P-C on 17-Mar-15.
 */
public class Settings_Adapter extends RecyclerView.Adapter<Settings_Adapter.ViewHolder> {

    private Context context;

    private DB db;

    private ArrayList<Setting> items;

    private OnCardClickListener onCardClickListener;

    public void setOnCardClickListener(OnCardClickListener onCardClickListener)
    {
        this.onCardClickListener = onCardClickListener;
    }

    public Settings_Adapter(Context context,ArrayList<Setting> items) {
        this.context = context;
        db = new DB(context);
        this.items = items;

    }


    @Override
    public int getItemCount() {
        if(items == null)
            return 0;
        else
            return items.size();
    }
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder,final int i) {
        final Setting item = items.get(i);


        if(!item.isHeader()) {
            viewHolder.sub_header.setText(item.getName());
            viewHolder.header.setText("");
            viewHolder.radioButton.setVisibility(View.VISIBLE);
            viewHolder.root.setBackgroundResource(R.drawable.white);
            if (i == items.size() - 1) {
                viewHolder.radioButton.setBackgroundResource(R.drawable.arrow_next_grey);
            } else
            {
                viewHolder.radioButton.setBackgroundResource(item.isChecked() ? R.drawable.radio_blue : R.drawable.radio_grey);

                viewHolder.radioButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        item.setChecked(!item.isChecked());
                        viewHolder.radioButton.setBackgroundResource(item.isChecked() ? R.drawable.radio_blue : R.drawable.radio_grey);
                        if (i == 1) {
                            App.update_sound(item.isChecked());
                        } else if (i == 2) {
                            App.update_vibration(item.isChecked());
                        } else if (i == 3) {
                            App.update_privacy(item.isChecked());
                        } else if (i == 5) {
                            App.update_feed(item.isChecked());
                        } else if (i == 6) {
                            App.update_rating(item.isChecked());
                        } else if (i == 7) {
                            App.update_comment(item.isChecked());
                        }
                    }
                });
        }
        }
        else {
            viewHolder.sub_header.setText("");
            viewHolder.header.setText(item.getName());
            viewHolder.root.setBackgroundResource(R.drawable.gray);
            viewHolder.radioButton.setVisibility(View.GONE);
        }


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup,final int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_settings_head, viewGroup, false);

        return new ViewHolder(itemView);
    }


    public class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{
        private RelativeLayout root;
        protected ImageView radioButton;
        protected Text header,sub_header;


        public ViewHolder(View v) {
            super(v);
            root = (RelativeLayout)v.findViewById(R.id.root);
            radioButton = (ImageView)v.findViewById(R.id.radio);
            header = (Text)v.findViewById(R.id.header);
            sub_header = (Text)v.findViewById(R.id.sub_header);



        }

        @Override
        public void onClick(View v) {
            onCardClickListener.onClick(v,getPosition());
        }
    }

}