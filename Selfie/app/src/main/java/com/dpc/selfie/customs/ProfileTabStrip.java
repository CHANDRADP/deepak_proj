package com.dpc.selfie.customs;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;

/**
 * Created by D-P-C on 11-Mar-15.
 */
public class ProfileTabStrip extends LinearLayout {

    private static final int DEFAULT_BOTTOM_BORDER_THICKNESS_DIPS = 2;
    private static final int DEFAULT_BOTTOM_BORDER_COLOR_ALPHA = 0xFF33B5E5;
    private static final int SELECTED_INDICATOR_THICKNESS_DIPS = 5;
    private static final int DEFAULT_SELECTED_INDICATOR_COLOR = 0xFF33B5E5;

   // private static final int DEFAULT_DIVIDER_THICKNESS_DIPS = 2;
   // private static final int DEFAULT_DIVIDER_COLOR_ALPHA = 0xFF33B5E5;
   // private static final float DEFAULT_DIVIDER_HEIGHT = 3.5f;

    private final int mBottomBorderThickness;
    private final Paint mBottomBorderPaint;

    private  int mSelectedIndicatorThickness;
    private final Paint mSelectedIndicatorPaint;

    private final int mDefaultBottomBorderColor;

    // private final Paint mDividerPaint;
   //  private final float mDividerHeight;

    private int mSelectedPosition;
    private float mSelectionOffset;

    private ProfileTabsLayout.TabColorizer mCustomTabColorizer;
    private final SimpleTabColorizer mDefaultTabColorizer;

    private Context context;

    ProfileTabStrip(Context context) {
        this(context, null);
        this.context = context;
    }

    ProfileTabStrip(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setWillNotDraw(false);

        final float density = getResources().getDisplayMetrics().density;

        TypedValue outValue = new TypedValue();
        context.getTheme().resolveAttribute(android.R.attr.colorForeground, outValue, true);
        final int themeForegroundColor =  outValue.data;

        mDefaultBottomBorderColor = 0xFFFFFFFF;

        mDefaultTabColorizer = new SimpleTabColorizer();
        mDefaultTabColorizer.setIndicatorColors(getResources().getColor(R.color.blue));
        mDefaultTabColorizer.setDividerColors(0xFF33B5E5);


        mBottomBorderThickness = (int) (DEFAULT_BOTTOM_BORDER_THICKNESS_DIPS * density);
        mBottomBorderPaint = new Paint();
        mBottomBorderPaint.setColor(mDefaultBottomBorderColor);

        mSelectedIndicatorThickness = (int) (SELECTED_INDICATOR_THICKNESS_DIPS * density);
        mSelectedIndicatorPaint = new Paint();

        // mDividerHeight = DEFAULT_DIVIDER_HEIGHT;
         // mDividerPaint = new Paint();
         // mDividerPaint.setStrokeWidth((int) (DEFAULT_DIVIDER_THICKNESS_DIPS * density));
    }
    public void set_DEFAULT_DIVIDER_THICKNESS_DIPS(int size)
    {
        this.mSelectedIndicatorThickness = size;
    }
    void setCustomTabColorizer(ProfileTabsLayout.TabColorizer customTabColorizer) {
        mCustomTabColorizer = customTabColorizer;
        invalidate();
    }

    void setSelectedIndicatorColors(int... colors) {
        // Make sure that the custom colorizer is removed
        mCustomTabColorizer = null;
        mDefaultTabColorizer.setIndicatorColors(colors);
        invalidate();
    }

    void setDividerColors(int... colors) {
        // Make sure that the custom colorizer is removed
        mCustomTabColorizer = null;
        mDefaultTabColorizer.setDividerColors(colors);
        invalidate();
    }

    void onViewPagerPageChanged(int position, float positionOffset) {
        mSelectedPosition = position;
        mSelectionOffset = positionOffset;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        final int height = getHeight();
        final int childCount = getChildCount();
     //    final int dividerHeightPx = (int) (Math.min(Math.max(0f, mDividerHeight), 1f) * height);
        final ProfileTabsLayout.TabColorizer tabColorizer = mCustomTabColorizer != null
                ? mCustomTabColorizer
                : mDefaultTabColorizer;
        // Vertical separators between the titles
       //   int separatorTop = (height - dividerHeightPx) / 2;

        for (int i = 0; i < childCount; i++) {
            ImageView txt = (ImageView)getChildAt(i).findViewById(R.id.txt);

            if(i == mSelectedPosition)
            {
                if(i == 0)
                {
                    txt.setBackgroundResource(R.drawable.profile_info_blue);
                }
             //   else if(i == 1)
             //   {
             //       txt.setBackgroundResource(R.drawable.badges_blue);
             //   }
                else if(i == 1)
                {
                    txt.setBackgroundResource(R.drawable.photos_blue);
                }
            }
            else
            {

                if(i == 0)
                {
                    txt.setBackgroundResource(R.drawable.profile_info_grey);
                }
            //    else if(i == 1)
             //   {
             //       txt.setBackgroundResource(R.drawable.badges_grey);
             //   }
                else if(i == 1)
                {
                    txt.setBackgroundResource(R.drawable.photos_grey);
                }
            }
          //  View child = getChildAt(i);
         //    mDividerPaint.setColor(tabColorizer.getDividerColor(i));
         //    canvas.drawLine(child.getRight(), separatorTop, child.getRight(),
          //           separatorTop + dividerHeightPx, mDividerPaint);
        }



        // Thin underline along the entire bottom edge
          canvas.drawRect(0, height - mBottomBorderThickness, getWidth(), height, mBottomBorderPaint);

        // Thick colored underline below the current selection
        if (childCount > 0) {
            View selectedTitle = getChildAt(mSelectedPosition);
            int left = selectedTitle.getLeft();
            int right = selectedTitle.getRight();
            int color = tabColorizer.getIndicatorColor(mSelectedPosition);

            if (mSelectionOffset > 0f && mSelectedPosition < (getChildCount() - 1)) {
                int nextColor = tabColorizer.getIndicatorColor(mSelectedPosition + 1);
                if (color != nextColor) {
                    color = blendColors(nextColor, color, mSelectionOffset);
                }

                // Draw the selection partway between the tabs
                View nextTitle = getChildAt(mSelectedPosition + 1);
                left = (int) (mSelectionOffset * nextTitle.getLeft() +
                        (1.0f - mSelectionOffset) * left);
                right = (int) (mSelectionOffset * nextTitle.getRight() +
                        (1.0f - mSelectionOffset) * right);
            }

            mSelectedIndicatorPaint.setColor(color);

            canvas.drawRect(left, height - mSelectedIndicatorThickness, right,
                    height, mSelectedIndicatorPaint);
        }



    }

    /**
     * Set the alpha value of the {@code color} to be the given {@code alpha} value.
     */
    private static int setColorAlpha(int color, byte alpha) {
        return Color.argb(alpha, Color.red(color), Color.green(color), Color.blue(color));
    }

    /**
     * Blend {@code color1} and {@code color2} using the given ratio.
     *
     * @param ratio of which to blend. 1.0 will return {@code color1}, 0.5 will give an even blend,
     *              0.0 will return {@code color2}.
     */
    private static int blendColors(int color1, int color2, float ratio) {
        final float inverseRation = 1f - ratio;
        float r = (Color.red(color1) * ratio) + (Color.red(color2) * inverseRation);
        float g = (Color.green(color1) * ratio) + (Color.green(color2) * inverseRation);
        float b = (Color.blue(color1) * ratio) + (Color.blue(color2) * inverseRation);
        return Color.rgb((int) r, (int) g, (int) b);
    }

    private static class SimpleTabColorizer implements ProfileTabsLayout.TabColorizer {
        private int[] mIndicatorColors;
        private int[] mDividerColors;

        @Override
        public final int getIndicatorColor(int position) {
            return mIndicatorColors[position % mIndicatorColors.length];
        }

        @Override
        public final int getDividerColor(int position) {
            return mDividerColors[position % mDividerColors.length];
        }

        void setIndicatorColors(int... colors) {
            mIndicatorColors = colors;
        }

        void setDividerColors(int... colors) {
            mDividerColors = colors;
        }
    }
}