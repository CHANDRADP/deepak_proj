package com.dpc.selfie.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dpc.selfie.R;
import com.dpc.selfie.Utils.DB;
import com.dpc.selfie.customs.Text;

/**
 * Created by D-P-C on 09-Mar-15.
 */
public class Details_Adapter extends RecyclerView.Adapter<Details_Adapter.ViewHolder> {

    private Context context;

    private DB db;

    private String[] details ;

    private int[] icons;




   public Details_Adapter(Context context,int[] icons,String[] details) {
        this.context = context;
        db = new DB(context);
        this.details = details;
        this.icons = icons;


    }


    @Override
    public int getItemCount() {
        if(details == null)
            return 0;
        else
            return details.length;
    }
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        viewHolder.icon.setBackgroundResource(icons[i]);
        viewHolder.details.setText(details[i]);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup,final int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_profile_details, viewGroup, false);

        return new ViewHolder(itemView);
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        protected ImageView icon;
        protected Text details;


        public ViewHolder(View v) {
            super(v);

            icon = (ImageView)v.findViewById(R.id.icon);
            details = (Text)v.findViewById(R.id.details);


        }


    }

}