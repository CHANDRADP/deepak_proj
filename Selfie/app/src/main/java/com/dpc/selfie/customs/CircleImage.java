package com.dpc.selfie.customs;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.dpc.selfie.R;

public class CircleImage extends ImageView {

    public CircleImage(Context context) {
        super(context);
    }

    public CircleImage(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CircleImage(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    private boolean my_pic = false;

    @Override
    protected void onDraw(Canvas canvas) {
        BitmapDrawable drawable = (BitmapDrawable) getDrawable();

        if (drawable == null) {
            return;
        }

        if (getWidth() == 0 || getHeight() == 0) {
            return;
        }

        Bitmap fullSizeBitmap = drawable.getBitmap();

        int scaledWidth = getMeasuredWidth();
        int scaledHeight = getMeasuredHeight();

        Bitmap mScaledBitmap;
        if (scaledWidth == fullSizeBitmap.getWidth()
                && scaledHeight == fullSizeBitmap.getHeight()) {
            mScaledBitmap = fullSizeBitmap;
        } else {
            mScaledBitmap = Bitmap.createScaledBitmap(fullSizeBitmap,
                    scaledWidth, scaledHeight, true);
        }
        ;
        BitmapShader shader;
        shader = new BitmapShader(mScaledBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        Paint paint;
        RectF rect = new RectF(0.0f, 0.0f, scaledWidth, scaledHeight);
        if(my_pic)
        {
            paint = new Paint();
            paint.setAntiAlias(true);
            paint.setStrokeWidth(3);
            paint.setColor(getResources().getColor(R.color.red));

            canvas.drawRoundRect(rect, scaledWidth/2, scaledHeight/2, paint);
        }

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setShader(shader);

        if(my_pic)
        rect = new RectF(3.0f, 3.0f, scaledWidth-3, scaledHeight-3);
        canvas.drawRoundRect(rect, scaledWidth/2, scaledHeight/2, paint);



    }
    public void draw_border(boolean border)
    {
        my_pic = border;
        invalidate();
    }


}
