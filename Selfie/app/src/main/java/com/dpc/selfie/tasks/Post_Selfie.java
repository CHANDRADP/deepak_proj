package com.dpc.selfie.tasks;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.dpc.selfie.Act.Activity_Feeds;
import com.dpc.selfie.App;
import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.DB;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.amazon.UploadService;
import com.dpc.selfie.customs.MyProgress;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.Server_Params;
import com.dpc.selfie.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by D-P-C on 03-Mar-15.
 */
public class Post_Selfie extends AsyncTask<String, Long, Boolean> {
    private Activity c;
    private String selfie_url = null, status = null;
    private MyProgress myProgress;
    private long image_id = -1;
    private boolean is_tagged = false;
    private int brand_id = -1;
    private HashMap<String,Integer> brands = new HashMap<>();

    public Post_Selfie(Activity c,MyProgress myProgress, String selfie_url, String status,boolean is_tagged) {
        this.c = c;
        this.selfie_url = selfie_url;
        this.status = status;
        this.myProgress = myProgress;
        this.is_tagged = is_tagged;
        brands.put("RCB",1);
    }


    @Override
    protected Boolean doInBackground(String... params) {
        Pattern MY_PATTERN = Pattern.compile("#(\\w+|\\W+)");
        Matcher mat = MY_PATTERN.matcher(status);
        List<String> str =new ArrayList<String>();
        while (mat.find()) {
            str.add(mat.group(1));
            Log.d("brand",mat.group(1));
        }
        for(int i = 0;i < str.size();i++)
        {
            if(brands.containsKey(str.get(i).toUpperCase()))
            {
                is_tagged = true;
                brand_id = 1;
                break;
            }

        }
        return post_selfie();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (result) {

            if(image_id > 0)
            {
                DB db = new DB(c);
                User user = db.Get_User_Details(App.getMyId());
                int points = user.getPoints()+5;
                if(is_tagged)
                    points += 5;
                user.setPoints(points);

               long id = db.Sync_App_User(user);

                C.alert(c, String.valueOf(id));
                Intent i = new Intent(Activity_Feeds.POST_STATE);
                i.putExtra(Params.IMAGE_ID.get_value(),image_id);
                i.putExtra(Params.IMAGE.get_value(),selfie_url);
                i.putExtra(Params.STATUS.get_value(),status);
                c.sendBroadcast(i);

            }
        }
        else
            C.alert(c,c.getResources().getString(R.string.post_failure));
        if(myProgress != null)
            myProgress.dismiss();

        Intent intent = new Intent(UploadService.UPLOAD_STATE_CHANGED_ACTION);
        intent.putExtra(UploadService.UPLOAD_STATE,100);
        intent.putExtra(Params.IMAGE.get_value(),selfie_url);
        c.sendBroadcast(intent);


    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    public boolean post_selfie() {
        boolean success = false;
        JSONParser parser = new JSONParser();
        JSONObject response = null;
        try {

            response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                    new Server_Params().post_selfie(selfie_url, status,brand_id));
            if (response != null) {
                Log.d("response", response.toString());
                if (response.has(Params.STATUS.get_value())) {
                    int status = response.getInt(Params.STATUS.get_value());
                    if (status == 200) {
                        success = true;
                        App.set_user_mode(false);
                        User user = new DB(c.getApplicationContext()).Get_User_Details(App.getMyId());
                        //user.setImage(selfie_url);  // modified as per new requirement
                        new DB(c.getApplicationContext()).Sync_App_User(user);
                        image_id = response.getJSONObject(Params.DATA.get_value()).getLong(Params.ID.get_value());


                    }
                }
            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return success;
    }

}
