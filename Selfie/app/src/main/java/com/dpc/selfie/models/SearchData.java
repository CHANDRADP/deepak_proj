package com.dpc.selfie.models;

/**
 * Created by Deepak on 13-07-2015.
 */
public class SearchData {

    private String f_name = null,l_name = null,status = null,image = null;
    private long id = 0;


    public SearchData(long id, String f_name, String l_name, String status, String image) {
        this.id = id;
        this.f_name = f_name;
        this.l_name = l_name;
        this.status = status;
        this.image = image;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getL_name() {
        return l_name;
    }

    public void setL_name(String l_name) {
        this.l_name = l_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
