package com.dpc.selfie.Act;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.dpc.selfie.App;
import com.dpc.selfie.CustomCollage.Custom_Collage;
import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.adapters.Profile_Adapter;
import com.dpc.selfie.customs.BaseActivity;
import com.dpc.selfie.customs.TextB;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.Celebrity;
import com.dpc.selfie.models.Profile_Item;
import com.dpc.selfie.models.Server_Params;
import com.dpc.selfie.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Activity_Collage extends BaseActivity {

    private RecyclerView selfie_list;
    private Celebrity celebrity;
    private boolean is_celebrity = false;
    private User user = null;
    public static ArrayList<Profile_Item> selfies;
    private long user_id;
    private String[] details = null;
    TextB viewAll;
    private GridView gridView;
    int nextActivity = 0;
    private GridViewAdapter gridAdapter;
    private ArrayList selected_ids = new ArrayList();
    boolean addImages = false;
    int ADDIMAGES = 777;
    int presentCount = 0;


    @Override
    protected void onResume() {
        super.onResume();
        selected_ids.clear();
        selfies = new ArrayList<>();
        new Get_Selfies().execute();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_collage);

        gridView = (GridView) findViewById(R.id.gridView);




        final Intent intent = this.getIntent();
        if(intent.hasExtra(Params.USER_ID.get_value()))
        {
            user_id = intent.getLongExtra(Params.USER_ID.get_value(), -1);
            is_celebrity = intent.getBooleanExtra(Params.CELEBRITY.get_value(),false);
            if(user_id > 0)
            {
                if(is_celebrity) {
                    celebrity = Activity_Friends.celebrities_map.get(user_id);
                    details = new String[]{celebrity.getDob(),
                            celebrity.getProfession(),
                            String.valueOf(celebrity.getFollow_count())+" followers"};
                }
                else {
                    user = db.Get_User_Details(user_id);
                    String number = user.getNumber();
                    String dob = user.getDob();
                    if(number == null || number.equalsIgnoreCase("null"))
                        number = "Not available";
                    if(dob == null || dob.equalsIgnoreCase("null"))
                        dob = "Not available";
                    details = new String[]{dob,
                            number,
                            user.getEmail()};
                }
            }
        }

        if(intent.hasExtra("nextActivity"))
        {
            nextActivity = intent.getIntExtra("nextActivity",0);
        }
        if(intent.hasExtra("AddImages"))
        {
            addImages = intent.getBooleanExtra("AddImages",false);
            presentCount = intent.getIntExtra("PresentCount", 0);
            Log.d("dd","dd");
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_collage, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (!App.is_new_user()) {

                    super.onBackPressed();
                }

                break;
            case R.id.collage:

                if(selected_ids.size()<2 && !addImages)
                {
                    Toast.makeText(getApplicationContext(), "Min 2 images required", Toast.LENGTH_LONG).show();
                }
                else if(selected_ids.size()> 5)
                {
                    Toast.makeText(getApplicationContext(), "Max 5 images", Toast.LENGTH_LONG).show();
                }
                else if(selected_ids.size() == 0 && addImages)
                {
                    Toast.makeText(getApplicationContext(), "Add min 1 image ", Toast.LENGTH_LONG).show();
                }
                else if(addImages)// if this activity is called only to add more images
                {
                    Intent intent=new Intent();
                    intent.putExtra("selected_ids",selected_ids);
                    setResult(777,intent);
                    finish();//finishing activity
                }
                else
                {
                   if(nextActivity == 0)
                   {
                       Intent intent = new Intent(Activity_Collage.this, Custom_Collage.class);
                       intent.putExtra("selected_ids", selected_ids);
                       intent.putExtra("dynamic", false);
                       startActivity(intent);
                   }
                    else if(nextActivity == 1)
                   {
                       Intent intent = new Intent(Activity_Collage.this, Custom_Collage.class);
                       intent.putExtra("selected_ids", selected_ids);
                       intent.putExtra("dynamic", true);
                       startActivity(intent);
                   }
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void update_views()
    {
        if(selfie_list != null)
        {
            selfie_list.setAdapter(new Profile_Adapter(Activity_Collage.this));
        }
    }

    public class Get_Selfies  extends AsyncTask<String, Long, Boolean>
    {

        public Get_Selfies()
        {
        }

        @Override
        protected Boolean doInBackground(String... params)
        {

            return get_selfies();
        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            if (result)
            {
                gridAdapter = new GridViewAdapter(getApplicationContext(), R.layout.grid_item_layout,selfies);
                gridView.setAdapter(gridAdapter);
            }
        }

        @Override
        protected void onCancelled()
        {
            super.onCancelled();
        }
    }

    public boolean get_selfies()
    {
        boolean success = false;
        JSONParser parser = new JSONParser();
        JSONObject response = null;
        try
        {
            long user_id = 0;
            if(is_celebrity)
            {
                user_id = celebrity.getId();
            }
            else
            {
                user_id = user.getUserid();
            }

            response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                    new Server_Params().get_selfies(user_id));
            if (response != null)
            {
                Log.d("response", response.toString());
                if (response.has(Params.STATUS.get_value()))
                {
                    int status = response.getInt(Params.STATUS.get_value());
                    if (status == 200)
                    {
                        JSONArray data = response.getJSONArray(Params.DATA.get_value());
                        for(int i = 0;i < data.length();i++)
                        {
                            JSONObject image = data.getJSONObject(i);

                            String url = image.getString(Params.IMAGE.get_value());
                            long id = image.getLong(Params.ID.get_value());
                            selfies.add(new Profile_Item(id,url));
                        }
                        success = true;
                    }
                }
            }
        } catch (JSONException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return success;
    }

    public class GridViewAdapter extends ArrayAdapter {
        private Context context;
        private int layoutResourceId;
        private ArrayList data = new ArrayList();


        public GridViewAdapter(Context context, int layoutResourceId, ArrayList data) {
            super(context, layoutResourceId, data);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.data = data;
        }

        class ViewHolder {

            ImageView image, rightmark;

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            View row = convertView;
            ViewHolder holder = null;

            if (row == null)
            {
                LayoutInflater inflater = getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);
                holder = new ViewHolder();
                holder.image = (ImageView) row.findViewById(R.id.image);
                holder.rightmark = (ImageView) row.findViewById(R.id.rightmark);

                row.setTag(holder);
            } else {
                holder = (ViewHolder) row.getTag();
            }
            display_image(Activity_Collage.selfies.get(position).getImage_url(), holder.image, false);
            holder.image.setScaleType(ImageView.ScaleType.FIT_XY);

            final ViewHolder finalHolder = holder;
            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    String item = Integer.toString(position);
                    if((presentCount+selected_ids.size())>4)
                    {
                        Toast.makeText(getApplicationContext(), "Only 5 Images Allowed", Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        if(selected_ids.contains(item))
                        {
                            finalHolder.rightmark.setVisibility(View.GONE);
                            selected_ids.remove(item);
                        }
                        else
                        {
                            selected_ids.add(item);
                            finalHolder.rightmark.setVisibility(View.VISIBLE);
                        }
                        Log.d("Selected ids", selected_ids.toString());
                    }
                }
            });

            return row;
        }
    }
}
