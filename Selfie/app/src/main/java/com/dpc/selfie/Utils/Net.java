/**
* @author D-P-C
* Jul 22, 2014 2014

*/
package com.dpc.selfie.Utils;

/**
 * @author D-P-C
 *
 */
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.dpc.selfie.interfaces.OnButtonClickListener;


public class Net implements OnButtonClickListener {
 
    private Context _context;
 
    public Net(Context context){
        this._context = context;
    }
 
    /**
     * Checking for all possible internet providers
     * **/
    public boolean isConnectingToInternet(){
        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
          if (connectivity != null)
          {
              NetworkInfo[] info = connectivity.getAllNetworkInfo();
              if (info != null)
                  for (int i = 0; i < info.length; i++)
                      if (info[i].getState() == NetworkInfo.State.CONNECTED)
                      {
                          return true;
                      }
 
          }
          return false;
    }
    public void alert(FragmentManager fragment_manager)
    {
    	MyAlert alert = new MyAlert();
    	Bundle b = new Bundle();
		b.putString("head", "Net Alert");
		b.putString("body", "Problem with internet connection, please check your data connection");
		alert.ok_listener = this;
		alert.setArguments(b);
		alert.show(fragment_manager, "");
    }

	@Override
	public void setOKSubmitListener(int value) {
		// TODO Auto-generated method stub
		
	}
}
