package com.dpc.selfie.Act;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.dpc.selfie.R;
import com.dpc.selfie.adapters.Notification_Adapter;
import com.dpc.selfie.customs.BaseActivity;
import com.dpc.selfie.customs.DividerItemDecorator;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.interfaces.OnCardClickListener;
import com.dpc.selfie.models.Notification;

import java.util.ArrayList;

public class Activity_Notifications extends BaseActivity {

    private RecyclerView list;

    private Notification_Adapter notification_adapter;

    private ArrayList<Notification> notifications = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        list = (RecyclerView)findViewById(R.id.list);
        LinearLayoutManager llm2 = new LinearLayoutManager(this);
        llm2.setOrientation(LinearLayoutManager.VERTICAL);
        list.setLayoutManager(llm2);
        list.addItemDecoration(new DividerItemDecorator(this, DividerItemDecorator.VERTICAL_LIST));
        notifications = db.get_notifications();
        notification_adapter = new Notification_Adapter(this,notifications);
        list.setAdapter(notification_adapter);
        notification_adapter.setOnCardClickListener(new OnCardClickListener() {
            @Override
            public void onClick(View v, int position) {
                Intent i = new Intent(Activity_Notifications.this,Activity_Selfie.class);
                i.putExtra(Params.IMAGE_ID.get_value(),notifications.get(position).getImageid());
                i.putExtra(Params.ID.get_value(),notifications.get(position).getId());
                i.putExtra(Params.FROM_NOTIFICATIONS.get_value(),true);
                startActivity(i);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                super.onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
