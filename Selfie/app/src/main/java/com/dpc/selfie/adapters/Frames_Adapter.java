package com.dpc.selfie.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.DB;
import com.dpc.selfie.Utils.Edit;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.interfaces.OnCardClickListener;
import com.dpc.selfie.models.BrandFrames;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class Frames_Adapter extends RecyclerView.Adapter<Frames_Adapter.ViewHolder> {

    private Context context;

    private int selected_posi = 0;

    private OnCardClickListener onCardClickListener;

    private Edit edit;
    Bitmap image = null;
    String captured_image = null;
    private ArrayList<BrandFrames> data_list = new ArrayList<>();
    private int TYPE_FRAMES = 2;
    private String image_url = null;
    Bitmap captured_image_bitmap;

    public void setOnCardClickListener(OnCardClickListener onCardClickListener)
    {
        this.onCardClickListener = onCardClickListener;
    }
    public Frames_Adapter(Context context,ArrayList<BrandFrames> data_list, String image_path) {
        this.context = context;
        this.data_list = data_list;
        this.captured_image = image_path;
        edit = new Edit(this.context,image);
    }


    @Override
    public int getItemCount()
    {
        return data_list.size();
    }
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i)
    {
        captured_image_bitmap = BitmapFactory.decodeFile(captured_image);

        BrandFrames frames_item = data_list.get(i);

        image_url = frames_item.getFrame_url();

        new DownloadBitmapTask(viewHolder.imageView).execute();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup,final int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_edit, viewGroup, false);


        return new ViewHolder(itemView);
    }


    private class DownloadBitmapTask extends AsyncTask<String, Void, String> {

        ImageView imageView;

        private DownloadBitmapTask(ImageView imageView) {
            this.imageView = imageView;
        }

        @Override
        protected String doInBackground(String... urls) {

            URL url = null;
            try {
                url = new URL(image_url);
                image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            edit.setBitmap(captured_image_bitmap);
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageBitmap(edit.get_preview(context, TYPE_FRAMES, image));

        }


    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        //protected ImageView imageView;
        protected ImageView imageView;
        protected Text name;
        protected LinearLayout root;


        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            imageView = (ImageView)v.findViewById(R.id.image);
            name = (Text)v.findViewById(R.id.name);
            root = (LinearLayout) v.findViewById(R.id.root);
        }

        @Override
        public void onClick(View v) {
            selected_posi = getPosition();
            notifyDataSetChanged();
            if(onCardClickListener != null)
            {

                onCardClickListener.onClick(v,selected_posi);
            }
        }
    }

}