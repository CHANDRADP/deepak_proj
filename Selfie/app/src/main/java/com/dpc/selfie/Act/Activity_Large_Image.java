package com.dpc.selfie.Act;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.dpc.selfie.R;
import com.dpc.selfie.adapters.Comment_Adapter;
import com.dpc.selfie.adapters.Menu_Adapter;
import com.dpc.selfie.adapters.Rating_Adapter;
import com.dpc.selfie.customs.BaseActivity;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.customs.TouchImageView;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.Feed;
import com.dpc.selfie.models.User;

import java.util.ArrayList;

public class Activity_Large_Image extends BaseActivity {

    int comment_count = 0;
    int rate_count = 0;
    Text txtCount;
    String comment_postfix = null, likes_postfix = null;







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_large_image);
        //txtCount = (Text) findViewById(R.id.txtCount);

        String image = this.getIntent().getStringExtra(Params.IMAGE.get_value());

        comment_count = this.getIntent().getIntExtra(Params.COMMENT_COUNT.get_value(),0);
        rate_count = this.getIntent().getIntExtra(Params.RATING_COUNT.get_value(),0);
        //txtCount.setText(display_likes_comments_count());
        final TouchImageView imageView = (TouchImageView)findViewById(R.id.image);
        display_image(image,imageView,false);


        getSupportActionBar().setTitle("");
    }

   /* private String display_likes_comments_count()
    {
        String countStatus = null;
        if(comment_count == 0)
        {
            comment_postfix = "";
        }
        else if(comment_count == 1)
        {
            comment_postfix = "Comment";
        }
        else
        {
            comment_postfix = "Comments";
        }
        if(rate_count == 0)
        {
            likes_postfix = "";
        }
        else if(rate_count == 1)
        {
            likes_postfix = "Like";
        }
        else
        {
            likes_postfix = "Likes";
        }

        if(comment_count == 0 && rate_count == 0)
        {
            countStatus = "";
        }
        else if(comment_count == 0 && rate_count > 0)
        {
            countStatus = rate_count+" "+likes_postfix;
        }
        else if(comment_count > 0 && rate_count == 0)
        {
            countStatus = comment_count+" "+comment_postfix;
        }
        else if(comment_count > 0 && rate_count > 0)
        {
            countStatus = comment_count+" "+comment_postfix + " "+rate_count+ " "+likes_postfix;
        }
        return countStatus;
    }*/


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.menu_large__image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();

                break;

            case R.id.Tag:
                Log.d("tag clicked", "hi");

                break;
        }

        return super.onOptionsItemSelected(item);
    }



}
