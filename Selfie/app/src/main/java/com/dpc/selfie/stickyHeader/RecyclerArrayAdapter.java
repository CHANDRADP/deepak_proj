package com.dpc.selfie.stickyHeader;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;


public abstract class RecyclerArrayAdapter<Feed, VH extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<VH> {
    private ArrayList<Feed> items = new ArrayList<Feed>();

    public RecyclerArrayAdapter() {
        setHasStableIds(true);
    }

    public void add(Feed object) {
        items.add(object);
        notifyDataSetChanged();
    }

    public void add(int index, Feed object) {
        items.add(index, object);
        notifyDataSetChanged();
    }

    public void addAll(Collection<? extends Feed> collection) {
        if (collection != null) {
            items.addAll(collection);
            notifyDataSetChanged();
        }
    }

    public void addAll(Feed... items) {
        addAll(Arrays.asList(items));
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    public void remove(Feed object) {
        items.remove(object);
        notifyDataSetChanged();
    }

    public Feed getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}