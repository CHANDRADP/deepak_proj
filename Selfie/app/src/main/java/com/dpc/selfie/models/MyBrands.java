package com.dpc.selfie.models;

/**
 * Created by user on 4/11/2015.
 */
public class MyBrands {
    private String logo_url = null, brand_Name = null;
    private int no_of_offers = 0 ;

    public MyBrands(String logo_url, String brand_Name, int no_of_offers) {
        this.logo_url = logo_url;
        this.brand_Name = brand_Name;
        this.no_of_offers = no_of_offers;
    }

    public String getLogo_url() {
        return logo_url;
    }

    public void setLogo_url(String logo_url) {
        this.logo_url = logo_url;
    }

    public String getBrand_Name() {
        return brand_Name;
    }

    public void setBrand_Name(String brand_Name) {
        this.brand_Name = brand_Name;
    }

    public int getNo_of_offers() {
        return no_of_offers;
    }

    public void setNo_of_offers(int no_of_offers) {
        this.no_of_offers = no_of_offers;
    }
}
