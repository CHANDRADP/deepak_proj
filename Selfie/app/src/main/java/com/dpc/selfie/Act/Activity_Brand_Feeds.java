package com.dpc.selfie.Act;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.dpc.selfie.App;
import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.customs.BaseActivity;
import com.dpc.selfie.customs.CircleImage;
import com.dpc.selfie.customs.DividerItemDecorator;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.customs.TextB;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.BrandFeed;
import com.dpc.selfie.models.Server_Params;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Activity_Brand_Feeds extends BaseActivity implements View.OnClickListener {


    private int refresh_position = 0,new_items_count = 0;

    private boolean feeds_completed = false;

    private ProgressBar loading;

    private long brand_id,page = 0;

    private ImageView camera;

    private RecyclerView feed_list;

    private LinearLayoutManager feed_list_manager;

    private Brand_Feed_Adapter feeds_adapter;

    private ArrayList<BrandFeed> feeds = new ArrayList<>();





    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand_feeds);
        Intent i = this.getIntent();
        if(i.hasExtra(Params.BRAND_ID.get_value()))
        {
            brand_id = i.getLongExtra(Params.BRAND_ID.get_value(),-1);
        }
        init_views();


        feed_list.setHasFixedSize(true);
        feed_list_manager = new LinearLayoutManager(this);
        feed_list_manager.setOrientation(LinearLayoutManager.VERTICAL);
        feed_list.setLayoutManager(feed_list_manager);
        feed_list.addItemDecoration(new DividerItemDecorator(this,DividerItemDecorator.VERTICAL_LIST));

        feeds_adapter = new Brand_Feed_Adapter();
        feed_list.setAdapter(feeds_adapter);

        feed_list.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(reached_lsat_position() && !feeds_completed)
                {
                    new Get_Feeds().execute();
                }


            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        });

        new Get_Feeds().execute();



    }

    private boolean reached_lsat_position()
    {
        if(feed_list_manager != null)
        {
            int last_position = feed_list_manager.findLastCompletelyVisibleItemPosition();
            if(last_position == feeds.size() - 1)
            {
                return  true;
            }
        }
        return false;
    }

    private void init_views()
    {
        feed_list = (RecyclerView) findViewById(R.id.feed_list);
        loading = (ProgressBar)findViewById(R.id.loading);


        camera = (ImageView)findViewById(R.id.camera);
        camera.setOnClickListener(this);



    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }



    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return true;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.camera:
                startActivity(new Intent(this,Activity_Edit_First.class));
                break;

        }
    }

    //////////////////////////////get user selfies

    class Get_Feeds extends AsyncTask<String, Long, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(String... params) {
            if(feeds.size() > 0)
            {
                refresh_position = feeds.size() - 1;
            }
            return get_feeds();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            loading.setVisibility(View.GONE);
            if(result) {
                if(feeds != null && feeds.size() > refresh_position) {
                    feeds_adapter.notifyItemRangeInserted(refresh_position + 1, new_items_count);
                }

            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        public boolean get_feeds() {
            boolean success = false;
            JSONParser parser = new JSONParser();
            JSONObject response;
            try {

                response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                        new Server_Params().get_brand_feeds(page,brand_id));
                if (response != null) {
                    Log.d("response", response.toString());
                    if (response.has(Params.STATUS.get_value())) {
                        int status = response.getInt(Params.STATUS.get_value());
                        if (status == 200) {
                            new_items_count = 0;


                            JSONArray feeds_array = response.getJSONArray(Params.DATA.get_value());
                            for (int i = 0; i < feeds_array.length(); i++) {
                                JSONObject image_data = feeds_array.getJSONObject(i);
                                JSONObject user = image_data.getJSONObject("user");


                                ++new_items_count;
                                String img_status = null;
                                if(!image_data.isNull(Params.STATUS.get_value()))
                                    img_status = image_data.getString(Params.STATUS.get_value());
                                feeds.add(new BrandFeed(
                                        image_data.getLong(Params.ID.get_value()),
                                        user.getLong(Params.USER_ID.get_value()),
                                        user.getString(Params.FIRST_NAME.get_value()),
                                        user.getString(Params.MIDDLE_NAME.get_value()),
                                        user.getString(Params.LAST_NAME.get_value()),
                                        user.getString(Params.IMAGE.get_value()),
                                        user.getString(Params.SELFIE.get_value()),
                                        img_status,
                                        user.getString(Params.DATE.get_value()),
                                        user.getInt(Params.RATING.get_value()),
                                        image_data.getInt(Params.COMMENT_COUNT.get_value())));
                            }

                            if(new_items_count == 0)
                            {
                                feeds_completed = true;
                            }
                            if(feeds.size() > 0)
                            {

                                page = feeds.get(feeds.size() - 1).getTag_id();
                            }
                            success = true;

                        }
                    }
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return success;
        }

    }



    /**
     * Created by D-P-C on 19-Feb-15.
     */
    public class Brand_Feed_Adapter extends RecyclerView.Adapter<Brand_Feed_Adapter.ViewHolder> {
        public Brand_Feed_Adapter()
        {}

        @Override
        public int getItemCount() {
            return feeds.size();
        }
        @Override
        public void onBindViewHolder(final ViewHolder viewHolder,final int i) {
            final BrandFeed feed = feeds.get(i);


                    if (feed.getUser_id() == App.getMyId())
                        viewHolder.dp.draw_border(true);
                    else
                        viewHolder.dp.draw_border(false);

                    display_image(feed.getImage(),viewHolder.dp,true);
                    viewHolder.name.setText(feed.getF_name());


            String status = feed.getStatus();
            if( status != null && status.length() > 0) {
                status =  status.replaceAll("(#[A-Za-z0-9_-]+)",
                        "<font color='#2980b9'>" + "$0" + "</font>");
            }
            else
            status = "";
                viewHolder.status.setText(Html.fromHtml(status));

                viewHolder.time.setText(C.getDateDifference(C.UTC_to_Local(feed.getDate())));

                if (feed.getrating() > 0)
                    viewHolder.rate_count.setText(String.valueOf(feed.getrating()));
                else
                    viewHolder.rate_count.setText("");



                if (feed.getComment_count() > 0)
                    viewHolder.comment_count.setText(String.valueOf(feed.getComment_count()));
                else
                    viewHolder.comment_count.setText("");


            viewHolder.rate.setBackgroundResource(R.drawable.like_normal);
            display_image(feed.getSelfie(),viewHolder.image,false);

            viewHolder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                        startActivity(new Intent(Activity_Brand_Feeds.this,Activity_Large_Image.class).putExtra(Params.IMAGE.get_value(),feed.getSelfie()));
                }
            });


        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup,final int i) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.item_feeds, viewGroup, false);

            return new ViewHolder(itemView);
        }


        public class ViewHolder extends RecyclerView.ViewHolder {
            protected ImageView image,rate,comment,time_indicator;
            protected CircleImage dp;
            protected TextB name;
            protected Text time,rate_count,comment_count,status;


            public ViewHolder(View v) {
                super(v);


                dp = (CircleImage)v.findViewById(R.id.icon);
                name = (TextB)v.findViewById(R.id.name);
                status = (Text)v.findViewById(R.id.status);
                time = (Text)v.findViewById(R.id.time);
                rate_count = (Text)v.findViewById(R.id.rate_count);
                comment_count = (Text)v.findViewById(R.id.comment_count);
                rate = (ImageView)v.findViewById(R.id.rate);
                comment = (ImageView)v.findViewById(R.id.comment);
                image = (ImageView)  v.findViewById(R.id.image);

                time_indicator = (ImageView)  v.findViewById(R.id.clock);




            }


        }

    }


    //////////////////////////////////////

}
