package com.dpc.selfie.Act;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.Edit;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.adapters.BrandOffers_Adapter;
import com.dpc.selfie.adapters.Edit_Adapter;
import com.dpc.selfie.adapters.Frames_Adapter;
import com.dpc.selfie.amazon.UploadService;
import com.dpc.selfie.customs.BaseActivity;
import com.dpc.selfie.customs.DividerItemDecorator;
import com.dpc.selfie.customs.MyProgress;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.interfaces.OnCardClickListener;
import com.dpc.selfie.models.BrandFrames;
import com.dpc.selfie.models.Brand_Offer_Details;
import com.dpc.selfie.models.Server_Params;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class Activity_Brand_Frames extends BaseActivity {

    private ImageView image;
    private RecyclerView frames_list;
    private Frames_Adapter frames_adapter;
    RecyclerView.LayoutManager mLayoutManager;
    private long brand_id;
    private Edit edit;
    private MyProgress myProgress;
    private RelativeLayout custom_layout;
    private Bitmap captured_bitmap;
    private int TYPE_FRAMES = 2;
    private ArrayList<BrandFrames> data_list = new ArrayList<>();
    private String image_path;
    private HashMap<Long, BrandFrames> brand_frames_map = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_brand_frames);

        image = (ImageView) findViewById(R.id.image);
        frames_list = (RecyclerView) findViewById(R.id.frames_list);
        custom_layout  = (RelativeLayout) findViewById(R.id.CustomLayout);
        frames_list.setHasFixedSize(true);

        GridLayoutManager llm = new GridLayoutManager(this,1);
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        frames_list.setLayoutManager(llm);

        brand_id = getIntent().getLongExtra(Params.BRAND_ID.get_value(),0);
        image_path = getIntent().getStringExtra("image_path");

        brand_frames_map.clear();
        new Get_Brand_Frames().execute();
        captured_bitmap = BitmapFactory.decodeFile(image_path);
        image.setImageBitmap(captured_bitmap);
        image.setScaleType(ImageView.ScaleType.FIT_XY);

        edit = new Edit(Activity_Brand_Frames.this,captured_bitmap);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity__brand__frames, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case android.R.id.home:

                super.onBackPressed();
                break;

            case R.id.action_post:

                new Post_Image_Task().execute();

                break;
        }

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }



        return super.onOptionsItemSelected(item);
    }

    public class Post_Image_Task extends AsyncTask<String, Long, Boolean> {

        String path = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            myProgress = new MyProgress(Activity_Brand_Frames.this);
            myProgress.setTitle("Uploading...");
            myProgress.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {

            path = C.create_image_from_customView(Activity_Brand_Frames.this);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            myProgress.dismiss();
            Intent intent = new Intent(Activity_Brand_Frames.this, UploadService.class);
            intent.putExtra(Params.SELFIE.get_value(), path);
            myProgress.dismiss();
            startService(intent);

        }
    }



        public class Get_Brand_Frames extends AsyncTask<String, Long, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {

            return get_brand_frames(brand_id);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            frames_adapter = new Frames_Adapter(Activity_Brand_Frames.this, data_list, image_path);
            frames_list.setAdapter(frames_adapter);

            frames_adapter.setOnCardClickListener(new OnCardClickListener()
            {
                @Override
                public void onClick(View v, int position)
                {
                    edit.setBitmap(captured_bitmap);
                    BrandFrames frames = data_list.get(position);
                    Bitmap bitmapframe = getBitmapFromURL(frames.getFrame_url());
                    image.setImageBitmap(edit.get_preview(Activity_Brand_Frames.this, TYPE_FRAMES, bitmapframe));
                }
            });

        }

        public Bitmap getBitmapFromURL(String src) {
            try {
                URL url = new URL(src);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        public boolean get_brand_frames(long brand_id) {
            boolean success = false;
            JSONParser parser = new JSONParser();
            JSONObject response = null;
            try {

                response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                        new Server_Params().get_brand_frames(brand_id));
                if (response != null)
                {
                    Log.d("response", response.toString());
                    if (response.has(Params.STATUS.get_value()))
                    {
                        int status = response.getInt(Params.STATUS.get_value());
                        if (status == 200)
                        {
                            JSONObject obj = response.getJSONObject(Params.DATA.get_value());
                            JSONArray array = obj.getJSONArray("frames");
                            brand_frames_map = new C().parse_brand_frames(array);

                            for (BrandFrames frames : brand_frames_map.values())
                            {
                                data_list.add(frames);
                            }

                            success = true;
                        }
                    }
                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return success;
        }
    }
}