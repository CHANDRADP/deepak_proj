package com.dpc.selfie.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.dpc.selfie.App;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.enums.*;
import com.dpc.selfie.enums.Process;
import com.google.android.gcm.GCMRegistrar;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Update_GCM_Task extends AsyncTask<String,String,String>
{
	private Context c;
	private String gcm_id;
	JSONParser jParser = new JSONParser();
	JSONObject response = null;
	ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
	
	public  Update_GCM_Task(Context c, String gcm_id)
	{
		this.c = c;
		this.gcm_id = gcm_id;
		
	}
	@Override
	protected void onPreExecute() {
		
        
    }
	@Override
	protected String doInBackground(String... arg0) {
		// TODO Auto-generated method stub
		params.add(new BasicNameValuePair(Process.UPDATE.get_value(), Process.UPDATE_USER.get_value()));
		params.add(new BasicNameValuePair(Params.ID.get_value(), String.valueOf(App.getMyId())));
        params.add(new BasicNameValuePair(Params.USER_ID.get_value(), String.valueOf(App.getMyId())));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(), App.getToken()));
		params.add(new BasicNameValuePair(Params.PUSH_ID.get_value(),gcm_id));
        response = jParser.makeHttpRequest(C.SERVER_URL, "POST", params);
		GCMRegistrar.setRegisteredOnServer(c, true);
		return null;
	}
	@Override
	protected void onProgressUpdate(String... values) {
		// TODO Auto-generated method stub
		super.onProgressUpdate(values);
	}
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);

        if (response != null) {

            if (response.has(Params.STATUS.get_value())) {
                int status = 0;
                try {
                    status = response.getInt(Params.STATUS.get_value());
                    if (status == 200) {
                        App.gcm_updated(true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
