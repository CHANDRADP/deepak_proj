package com.dpc.selfie.foldablelayouts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dpc.selfie.Act.Activity_Feeds;
import com.dpc.selfie.R;
import com.dpc.selfie.models.Feed;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;

public class PaintingsAdapter extends ItemsAdapter<Painting> implements View.OnClickListener {

    public PaintingsAdapter(Context context, ArrayList<Feed> feeds) {
        super(context);

        setItemsList(Arrays.asList(Painting.getAllPaintings(context.getResources(), feeds)));
    }

    @Override
    protected View createView(Painting item, int pos, ViewGroup parent, LayoutInflater inflater) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.foldable_list_item, parent, false);
        ViewHolder vh = new ViewHolder();
        vh.image = Views.find(view, R.id.list_item_image);
        vh.image.setOnClickListener(this);
        vh.title = Views.find(view, R.id.list_item_title);
        view.setTag(vh);

        return view;
    }

    @Override
    protected void bindView(Painting item, int pos, View convertView) {
        ViewHolder vh = (ViewHolder) convertView.getTag();

        vh.image.setTag(item);
        Picasso.with(convertView.getContext()).load(item.getImageUrl()).noFade().into(vh.image);
        vh.title.setText(item.getStatus());
    }

    @Override
    public void onClick(View view) {
        if (view.getContext() instanceof Activity_Feeds) {
            Activity_Feeds activity = (Activity_Feeds) view.getContext();
           // activity.openDetails(view, (Painting) view.getTag());
        }
    }

    private static class ViewHolder {
        ImageView image;
        TextView title;
    }

}
