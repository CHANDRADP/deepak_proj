package com.dpc.selfie.Act;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.dpc.selfie.App;
import com.dpc.selfie.R;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.enums.Params;

public class Activity_Collage_Templates_Categories extends ActionBarActivity implements View.OnClickListener {

    private Text templateDynamic, template1, template2, template3, template4, template5;
    private int DYNAMIC_COLLAGE = 1;
    private int TWO_IMAGES = 2, THREE_IMAGES = 3, FOUR_IMAGES = 4, FIVE_IMAGES = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_collage_templates_categories);

        template1 = (Text) findViewById(R.id.template1);
        template2 = (Text) findViewById(R.id.template2);
        template3 = (Text) findViewById(R.id.template3);
        template4 = (Text) findViewById(R.id.template4);
        template5 = (Text) findViewById(R.id.template5);

        template1.setOnClickListener(this);
        template2.setOnClickListener(this);
        template3.setOnClickListener(this);
        template4.setOnClickListener(this);
        template5.setOnClickListener(this);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_collage_templates, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view)
    {

        Intent intent;
        switch (view.getId())
        {
            case R.id.template1:
                    intent = new Intent(Activity_Collage_Templates_Categories.this, Activity_Collage.class);
                    intent.putExtra(Params.USER_ID.get_value(), App.getMyId());
                    intent.putExtra("nextActivity", DYNAMIC_COLLAGE);
                    startActivity(intent);

                break;

            case R.id.template2:
                intent = new Intent(Activity_Collage_Templates_Categories.this, Activity_Collage_Templates.class);
                intent.putExtra(Params.NO_OF_IMAGES.get_value(), TWO_IMAGES);
                startActivity(intent);

                break;


            case R.id.template3:
                intent = new Intent(Activity_Collage_Templates_Categories.this, Activity_Collage_Templates.class);
                intent.putExtra(Params.NO_OF_IMAGES.get_value(), THREE_IMAGES);
                startActivity(intent);

                break;


            case R.id.template4:
                intent = new Intent(Activity_Collage_Templates_Categories.this, Activity_Collage_Templates.class);
                intent.putExtra(Params.NO_OF_IMAGES.get_value(), FOUR_IMAGES);
                startActivity(intent);

                break;


            case R.id.template5:
                intent = new Intent(Activity_Collage_Templates_Categories.this, Activity_Collage_Templates.class);
                intent.putExtra(Params.NO_OF_IMAGES.get_value(), FIVE_IMAGES);
                startActivity(intent);

                break;


        }
    }
}
