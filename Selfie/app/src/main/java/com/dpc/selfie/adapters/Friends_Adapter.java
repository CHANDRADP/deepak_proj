package com.dpc.selfie.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.dpc.selfie.R;
import com.dpc.selfie.customs.CircleImage;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.customs.TextB;
import com.dpc.selfie.models.Celebrity;
import com.dpc.selfie.models.User;

import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by D-P-C on 18-Feb-15.
 */
public class Friends_Adapter extends BaseAdapter {
    private LayoutInflater inflater;
    private ViewHolder holder;
    private View v;
    private List<User> items = new ArrayList<>();
    ArrayList<Celebrity> celebrities = new ArrayList<>();
    private Context c;
    private class ViewHolder {
        TextB name;
        Text status;
        CircleImage image;
        ImageView invite;

    }
    public Friends_Adapter(Context c,List<User> l)
    {
        this.c = c;
        this.items = l;

    }



    public void update_list(List<User> l)
    {
        this.items = l;
        notifyDataSetChanged();
    }
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        User user = items.get(position);

        v = convertView;
        if (v == null)
        {
            inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_user, null);
            holder = new ViewHolder();
            holder.name = (TextB) v.findViewById(R.id.name);
            holder.status = (Text) v.findViewById(R.id.status);
            holder.image = (CircleImage) v.findViewById(R.id.image);

            v.setTag(holder);

        }
        else
            holder = (ViewHolder) v.getTag();

        display_image(user.getImage(), holder.image,true);

        holder.name.setText(user.getDisplay_name());
        String status = user.getStatus();
        if(status == null)
            status = "No status";
        holder.status.setText(status);


        return v;

    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return items.size();
    }


    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return items.get(position);
    }


    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    private void display_image(String url,View v,boolean is_circle_image)
    {

        try {
            Picasso.with(c)
                    .load(url)
                    .placeholder(R.drawable.ic_action_person)
                    .error(R.drawable.ic_action_person)
                    .into(is_circle_image ? (CircleImage)v : (ImageView)(v));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
