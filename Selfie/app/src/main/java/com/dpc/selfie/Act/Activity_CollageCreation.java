package com.dpc.selfie.Act;

import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dpc.selfie.App;
import com.dpc.selfie.R;
import com.dpc.selfie.customs.BaseActivity;
import com.dpc.selfie.customs.ColorPickerDialog;
import com.dpc.selfie.customs.ColorPickerSwatch;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Activity_CollageCreation extends BaseActivity implements View.OnLongClickListener, View.OnDragListener
{
    ImageView image1, image2, image3, image4, image5;
    ImageView selectedImg, swapImage;
    int swapLocation, longclick_location;
    private ArrayList selected_ids = new ArrayList();
    private ArrayList<Bitmap> selected_bitmaps = new ArrayList();
    RelativeLayout collageImage;
    LinearLayout fifthimageLayout;
    private final String IMAGE1 = "IMAGE1";
    private final String IMAGE2 = "IMAGE2";
    private final String IMAGE3 = "IMAGE3";
    private final String IMAGE4 = "IMAGE4";
    private final String IMAGE5 = "IMAGE5";
    private int _xDelta;
    private int _yDelta;

    ColorPickerDialog colorPickerDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        selected_ids = intent.getStringArrayListExtra("selected_ids");
        Log.d("sel",selected_ids.toString());



        for(int i=0;i<selected_ids.size();i++)
        {
            URL url = null;
            try
            {
                url = new URL(Activity_Collage.selfies.get(Integer.parseInt(String.valueOf(selected_ids.get(i)))).getImage_url());
                Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                selected_bitmaps.add(image);
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        colorPickerDialog = new ColorPickerDialog();
        colorPickerDialog.initialize(R.string.dialog_title, new int[] { Color.CYAN, Color.LTGRAY, Color.BLACK, Color.BLUE, Color.GREEN, Color.MAGENTA, Color.RED, Color.GRAY, Color.YELLOW }, Color.YELLOW, 3, 2);
        colorPickerDialog.setOnColorSelectedListener(new ColorPickerSwatch.OnColorSelectedListener() {

            @Override
            public void onColorSelected(int color) {
                Toast.makeText(Activity_CollageCreation.this, "selectedColor : " + color, Toast.LENGTH_SHORT).show();
                change_bg(color);
            }
        });

        display();
    }

    public void display()
    {
        if(selected_ids.size()==5)
        {
            setContentView(R.layout.collage_template_a);
            image1 = (ImageView) findViewById(R.id.image1);
            image2 = (ImageView) findViewById(R.id.image2);
            image3 = (ImageView) findViewById(R.id.image3);
            image4 = (ImageView) findViewById(R.id.image4);
            image5 = (ImageView) findViewById(R.id.image5);
            fifthimageLayout = (LinearLayout) findViewById(R.id.fifthImageLayout);

            image1.setTag(IMAGE1);
            image2.setTag(IMAGE2);
            image3.setTag(IMAGE3);
            image4.setTag(IMAGE4);
            image5.setTag(IMAGE5);

            image1.setOnLongClickListener(this);
            image2.setOnLongClickListener(this);
            image3.setOnLongClickListener(this);
            image4.setOnLongClickListener(this);
            image5.setOnLongClickListener(this);

            image1.setOnDragListener(this);
            image2.setOnDragListener(this);
            image3.setOnDragListener(this);
            image4.setOnDragListener(this);
            image5.setOnDragListener(this);

            image1.setImageBitmap(selected_bitmaps.get(0));
            image2.setImageBitmap(selected_bitmaps.get(1));
            image3.setImageBitmap(selected_bitmaps.get(2));
            image4.setImageBitmap(selected_bitmaps.get(3));
            image5.setImageBitmap(selected_bitmaps.get(4));

            image1.setScaleType(ImageView.ScaleType.FIT_XY);
            image2.setScaleType(ImageView.ScaleType.FIT_XY);
            image3.setScaleType(ImageView.ScaleType.FIT_XY);
            image4.setScaleType(ImageView.ScaleType.FIT_XY);
            image5.setScaleType(ImageView.ScaleType.FIT_XY);


        }
        else if(selected_ids.size()==2)
        {
            setContentView(R.layout.collage_template_b);
            image1 = (ImageView) findViewById(R.id.image1);
            image2 = (ImageView) findViewById(R.id.image2);

            image1.setTag(IMAGE1);
            image2.setTag(IMAGE2);

            image1.setOnLongClickListener(this);
            image2.setOnLongClickListener(this);

            image1.setOnDragListener(this);
            image2.setOnDragListener(this);

            image1.setImageBitmap(selected_bitmaps.get(0));
            image2.setImageBitmap(selected_bitmaps.get(1));


            //display_image(Activity_Collage.selfies.get(Integer.parseInt(String.valueOf(selected_ids.get(0)))).getImage_url(), image1, false);
            //display_image(Activity_Collage.selfies.get(Integer.parseInt(String.valueOf(selected_ids.get(1)))).getImage_url(), image2, false);
            image1.setScaleType(ImageView.ScaleType.FIT_XY);
            image2.setScaleType(ImageView.ScaleType.FIT_XY);
        }
        else if(selected_ids.size()==3)
        {
            setContentView(R.layout.collage_template_c);
            image1 = (ImageView) findViewById(R.id.image1);
            image2 = (ImageView) findViewById(R.id.image2);
            image3 = (ImageView) findViewById(R.id.image3);

            image1.setTag(IMAGE1);
            image2.setTag(IMAGE2);
            image3.setTag(IMAGE3);

            image1.setOnLongClickListener(this);
            image2.setOnLongClickListener(this);
            image3.setOnLongClickListener(this);

            image1.setOnDragListener(this);
            image2.setOnDragListener(this);
            image3.setOnDragListener(this);

            image1.setImageBitmap(selected_bitmaps.get(0));
            image2.setImageBitmap(selected_bitmaps.get(1));
            image3.setImageBitmap(selected_bitmaps.get(2));

            image1.setScaleType(ImageView.ScaleType.CENTER_CROP);
            image2.setScaleType(ImageView.ScaleType.CENTER_CROP);
            image3.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }
        else if(selected_ids.size()==4)
        {
            setContentView(R.layout.collage_template_d);
            image1 = (ImageView) findViewById(R.id.image1);
            image2 = (ImageView) findViewById(R.id.image2);
            image3 = (ImageView) findViewById(R.id.image3);
            image4 = (ImageView) findViewById(R.id.image4);

            image1.setTag(IMAGE1);
            image2.setTag(IMAGE2);
            image3.setTag(IMAGE3);
            image4.setTag(IMAGE4);

            image1.setOnLongClickListener(this);
            image2.setOnLongClickListener(this);
            image3.setOnLongClickListener(this);
            image4.setOnLongClickListener(this);

            image1.setOnDragListener(this);
            image2.setOnDragListener(this);
            image3.setOnDragListener(this);
            image4.setOnDragListener(this);

            image1.setImageBitmap(selected_bitmaps.get(0));
            image2.setImageBitmap(selected_bitmaps.get(1));
            image3.setImageBitmap(selected_bitmaps.get(2));
            image4.setImageBitmap(selected_bitmaps.get(3));

            image1.setScaleType(ImageView.ScaleType.FIT_XY);
            image2.setScaleType(ImageView.ScaleType.FIT_XY);
            image3.setScaleType(ImageView.ScaleType.FIT_XY);
            image4.setScaleType(ImageView.ScaleType.FIT_XY);
        }
        collageImage = (RelativeLayout) findViewById(R.id.collageImage);


    }

    public  void change_bg(int color)
    {
        collageImage.setBackgroundColor(color);
        if(selected_ids.size()==5)
        {
            fifthimageLayout.setBackgroundColor(color);
        }
    }

    @Override
    public boolean onLongClick(View v)
    {
        String tag = v.getTag().toString();
        if(tag == IMAGE1)
        {
            selectedImg = image1;
            longclick_location = 0;
        }
        else if(tag == IMAGE2)
        {
            selectedImg = image2;
            longclick_location = 1;
        }
        else if(tag == IMAGE3)
        {
            selectedImg = image3;
            longclick_location = 2;
        }
        else if(tag == IMAGE4)
        {
            selectedImg = image4;
            longclick_location = 3;
        }
        else if(tag == IMAGE5)
        {
            selectedImg = image5;
            longclick_location = 4;
        }
        ClipData dragData = ClipData.newPlainText("TAG", tag);

        View.DragShadowBuilder myShadow = null;

        if (tag.equals(IMAGE1))
        {
            myShadow = new View.DragShadowBuilder(image1);
        }
        else if(tag.equals(IMAGE2))
        {
            myShadow = new View.DragShadowBuilder(image2);
        }

        else if(tag.equals(IMAGE3))
        {
            myShadow = new View.DragShadowBuilder(image3);
        }
        else if(tag.equals(IMAGE4))
        {
            myShadow = new View.DragShadowBuilder(image4);
        }
        else if(tag.equals(IMAGE5))
        {
            myShadow = new View.DragShadowBuilder(image5);
        }

        v.startDrag(dragData, myShadow, null, 0);

        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_activity_collage_creation, menu);
        return true;
    }
    public void create_image()
    {
        View content = findViewById(R.id.collageImage);
        content.setDrawingCacheEnabled(true);
        Bitmap bitmap = content.getDrawingCache();
        try
        {
            File file = new File(App.APP_FOLDER+"/Selfie"+System.currentTimeMillis()+".jpg");
            {
                if(!file.exists())
                {
                    file.createNewFile();
                }
                FileOutputStream ostream = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, ostream);
                ostream.close();
                content.invalidate();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.action_settings)
        {
            create_image();
            return true;
        }
        else if(id == R.id.background)
        {
            colorPickerDialog.show(getSupportFragmentManager(), "colorpicker");
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onDrag(View v, DragEvent event)
    {

        final int action = event.getAction();
        switch (action)
        {
            case DragEvent.ACTION_DRAG_STARTED:
               // ((ImageView) v).setColorFilter(Color.BLUE);
               // v.invalidate();
                return true;
            case DragEvent.ACTION_DRAG_ENTERED:
               // ((ImageView) v).setColorFilter(Color.GREEN);
                if(v.getTag()==IMAGE1)
                {
                    swapLocation = 0;
                    swapImage = image1;
                }
                else if(v.getTag() == IMAGE2)
                {
                    swapLocation = 1;
                    swapImage = image2;
                }
                else if(v.getTag() == IMAGE3)
                {
                    swapLocation = 2;
                    swapImage = image3;
                }
                else if(v.getTag() == IMAGE4)
                {
                    swapLocation = 3;
                    swapImage = image4;
                }
                else if(v.getTag() == IMAGE5)
                {
                    swapLocation = 4;
                    swapImage = image5;
                }
               // v.invalidate();

                return true;
            case DragEvent.ACTION_DRAG_LOCATION:
                return true;
            case DragEvent.ACTION_DRAG_EXITED:
                //((ImageView) v).setColorFilter(Color.BLUE);
                int x_cord = (int) event.getX();
                int y_cord = (int) event.getY();
                Log.d("DropView",Integer.toString(x_cord)+" "+Integer.toString(y_cord));
               // v.invalidate();
                return true;

            case DragEvent.ACTION_DROP:
                ClipData dragData = event.getClipData();
                final String tag = dragData.getItemAt(0).getText().toString();
                Toast.makeText(Activity_CollageCreation.this, "Dragged image is " + tag,
                        Toast.LENGTH_LONG).show();
                ((ImageView) v).clearColorFilter();
                v.invalidate();


                Log.d("Swapping between", swapLocation+" "+longclick_location);
                Collections.swap(selected_bitmaps, swapLocation, longclick_location);
                display();

                return true;
            case DragEvent.ACTION_DRAG_ENDED:
                ((ImageView) v).clearColorFilter();
               v.invalidate();
                if (event.getResult()) {
                    String tag1 = v.getTag().toString();
                    Log.d("The tag is", v.getTag().toString());
                    Toast.makeText(Activity_CollageCreation.this, "The drop was handled.",
                            Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(Activity_CollageCreation.this, "The drop didn't work.",
                            Toast.LENGTH_LONG).show();
                    image3.setImageBitmap(null);
                }

                return true;

            default:
                break;
        }

        return false;
    }



}
