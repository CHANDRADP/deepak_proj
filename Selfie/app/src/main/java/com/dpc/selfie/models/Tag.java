package com.dpc.selfie.models;

/**
 * Created by D-P-C on 09-Mar-15.
 */
public class Tag {
    private String[] tags = new String[]{"","","",""};
    private int tag = -1;

    public Tag(int tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tags[tag];
    }

    public void setTag(int tag) {
        this.tag = tag;
    }
}
