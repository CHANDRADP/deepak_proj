package com.dpc.selfie.models;

/**
 * Created by D-P-C on 03-Mar-15.
 */
public class Rating {

    private long user_id;
    private String user_image = null;
    private int rating;
    private String date = null;

    public Rating(long user_id,String user_image, int rating, String date) {
        this.user_id = user_id;
        this.user_image = user_image;
        this.rating = rating;
        this.date = date;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
