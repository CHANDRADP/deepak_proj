/*
 * Code based off the PhotoSortrView from Luke Hutchinson's MTPhotoSortr
 * example (http://code.google.com/p/android-multitouch-controller/)
 *
 * License:
 *   Dual-licensed under the Apache License v2 and the GPL v2.
 */
package com.dpc.selfie.CustomCollage;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.dpc.selfie.Act.Activity_Collage;

public class ImageEntity extends MultiTouchEntity {

    private double INITIAL_SCALE_FACTORX = 0.44; // to change default image size
    private double INITIAL_SCALE_FACTORY = 0.44; // to change default image size

    private transient Drawable mDrawable;

    private Drawable mResourceId;

    boolean long_clicked=false;

    float imageWidth, imageHeight, imageAngle = 0.0f;

    Context context;
    boolean isdynamic = false;
    boolean captured = false;




    public ImageEntity(Context context, Drawable resourceId, Resources res, boolean long_clicked, float imageWidth, float imageHeight, float angle)  {
        super(res);

    //    mDrawable = resourceId;
        mResourceId = resourceId;
        this.context = context;
        this.long_clicked = long_clicked;
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
        this.imageAngle = angle;

    }

    public ImageEntity(ImageEntity e, Resources res) {
        super(res);

        mDrawable = e.mDrawable;
        mResourceId = e.mResourceId;
        mScaleX = e.mScaleX;
        mScaleY = e.mScaleY;
        mCenterX = e.mCenterX;
        mCenterY = e.mCenterY;
        mAngle = e.mAngle;
    }

    public void draw(Canvas canvas, boolean selected, Bitmap bitmap) {
        canvas.save();

        float dx = (mMaxX + mMinX)/2 ;
        float dy = (mMaxY + mMinY)/2;


        /*if(selected)
        {
            Bitmap b1 = padBitmap(bitmap);
            Drawable drawable = new BitmapDrawable(context.getResources(), b1);
            mDrawable = drawable;
        }
        else
        {

            Drawable drawable = new BitmapDrawable(context.getResources(), bitmap);
            mDrawable = drawable;
        }*/

       /* if(selected)
        {
            Bitmap bitmap1 = ((BitmapDrawable)mDrawable).getBitmap();
            Bitmap b2 = padBitmap(bitmap1, 20);
            Drawable d = new BitmapDrawable(context.getResources(),b2);
            mDrawable = d;
        }
        else
        {

            Bitmap bitmap1 = ((BitmapDrawable)mDrawable).getBitmap();
            Bitmap b2 = padBitmap(bitmap1, 0);
            Drawable d = new BitmapDrawable(context.getResources(),b2);
            mDrawable = d;
        }*/

     //   Log.d("Called The Draw function","hi");

            mDrawable.setBounds((int) mMinX, (int) mMinY, (int) mMaxX, (int) mMaxY);



        canvas.translate(dx, dy);
        canvas.rotate(mAngle * 180.0f / (float) Math.PI);
        canvas.translate(-dx, -dy);
       // mDrawable.setAlpha(255);

        //Log.d("Clip Bounds", canvas.getClipBounds().toString());



        mDrawable.draw(canvas);

        canvas.restore();
    }


    /**
     * Called by activity's onPause() method to free memory used for loading the images
     */
    @Override
    public void unload() {
        this.mDrawable = null;
    }

    /** Called by activity's onResume() method to load the images */
    @Override
    public void load(Context context, float startMidX, float startMidY, int tag, boolean dynamic, boolean captured) {
        Resources res = context.getResources();
        getMetrics(res);
        isdynamic = dynamic;
        this.captured = captured;
        mStartMidX = startMidX;
        mStartMidY = startMidY;
        mDrawable = mResourceId;



        mWidth = (int)imageWidth;//mDrawable.getIntrinsicWidth();
        mHeight =(int)imageHeight;//= mDrawable.getIntrinsicHeight();
        // mWidth = 300;
        //mHeight = 400;

        //mWidth = mDrawable.getIntrinsicWidth();
        //mHeight = mDrawable.getIntrinsicHeight();

        //  Log.d("Widthjjjj  ",Integer.toString(mWidth));
        // Log.d("Heightjjjj  ",Integer.toString(mHeight));

        float centerX;
        float centerY;
        float scaleX;
        float scaleY;
        float angle;
        if (mFirstLoad)
        {
            centerX = startMidX;
            centerY = startMidY;

            if(isdynamic)
            {
                INITIAL_SCALE_FACTORX = 0.3;
                INITIAL_SCALE_FACTORY = 0.3;
            }
            if(captured)
            {
                INITIAL_SCALE_FACTORX = 0.65;
                INITIAL_SCALE_FACTORY = 0.68;
            }

            float scaleFactorX = (float) (Math.max(mDisplayWidth, mDisplayHeight) /
                    (float) Math.max(mWidth, mHeight) * INITIAL_SCALE_FACTORX);
            float scaleFactorY = (float) (Math.max(mDisplayWidth, mDisplayHeight) /
                    (float) Math.max(mWidth, mHeight) * INITIAL_SCALE_FACTORY);
            scaleX = scaleFactorX;
            scaleY = scaleFactorY;
          //  angle = 0.0f;
            angle = imageAngle;


            mFirstLoad = false;
        }
        else
        {
            centerX = mCenterX;
            centerY = mCenterY;
            scaleX = mScaleX;
            scaleY = mScaleY;
           // angle = mAngle;
            angle = imageAngle;
        }
        setPos(centerX, centerY, scaleX, scaleY, imageAngle, tag); // mAngle

    }

    public void loadCustom(Context context, float startMidX, float startMidY, int tag, boolean dynamic) {
        Resources res = context.getResources();
        getMetrics(res);
        isdynamic = dynamic;

        mStartMidX = startMidX;
        mStartMidY = startMidY;
        mDrawable = mResourceId;



        mWidth = (int)imageWidth;//mDrawable.getIntrinsicWidth();
        mHeight =(int)imageHeight;//= mDrawable.getIntrinsicHeight();
        // mWidth = 300;
        //mHeight = 400;

        //mWidth = mDrawable.getIntrinsicWidth();
        //mHeight = mDrawable.getIntrinsicHeight();

        //  Log.d("Widthjjjj  ",Integer.toString(mWidth));
        // Log.d("Heightjjjj  ",Integer.toString(mHeight));

        float centerX;
        float centerY;
        float scaleX;
        float scaleY;
        float angle;
        if (mFirstLoad)
        {
            centerX = startMidX;
            centerY = startMidY;

            if(isdynamic)
            {
                INITIAL_SCALE_FACTORX = 0.65;
                INITIAL_SCALE_FACTORY = 0.65;
            }

            float scaleFactorX = (float) (Math.max(mDisplayWidth, mDisplayHeight) /
                    (float) Math.max(mWidth, mHeight) * INITIAL_SCALE_FACTORX);
            float scaleFactorY = (float) (Math.max(mDisplayWidth, mDisplayHeight) /
                    (float) Math.max(mWidth, mHeight) * INITIAL_SCALE_FACTORY);
            scaleX = scaleFactorX;
            scaleY = scaleFactorY;
            //angle = 0.0f;
            angle = imageAngle;

            mFirstLoad = false;
        }
        else
        {
            centerX = mCenterX;
            centerY = mCenterY;
            scaleX = mScaleX;
            scaleY = mScaleY;
           // angle = mAngle;
            angle = imageAngle;
        }
        setPos(centerX, centerY, scaleX, scaleY, imageAngle, tag); //mAngle

    }
}
