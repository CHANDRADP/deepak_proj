package com.dpc.selfie.Act;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import com.dpc.selfie.App;
import com.dpc.selfie.CustomCollage.PhotoSortrView;
import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.Edit;
import com.dpc.selfie.adapters.Edit_Adapter;
import com.dpc.selfie.customs.BaseActivity;
import com.dpc.selfie.customs.EditTabStrip;
import com.dpc.selfie.customs.MyProgress;
import com.dpc.selfie.effects.GPUImageFilterTools;
import com.dpc.selfie.effects.filter.IF1977Filter;
import com.dpc.selfie.effects.filter.IFAmaroFilter;
import com.dpc.selfie.effects.filter.IFBrannanFilter;
import com.dpc.selfie.effects.filter.IFEarlybirdFilter;
import com.dpc.selfie.effects.filter.IFHefeFilter;
import com.dpc.selfie.effects.filter.IFHudsonFilter;
import com.dpc.selfie.effects.filter.IFInkwellFilter;
import com.dpc.selfie.effects.filter.IFLomoFilter;
import com.dpc.selfie.effects.filter.IFLordKelvinFilter;
import com.dpc.selfie.effects.filter.IFNashvilleFilter;
import com.dpc.selfie.effects.filter.IFRiseFilter;
import com.dpc.selfie.effects.filter.IFSierraFilter;
import com.dpc.selfie.effects.filter.IFSutroFilter;
import com.dpc.selfie.effects.filter.IFToasterFilter;
import com.dpc.selfie.effects.filter.IFValenciaFilter;
import com.dpc.selfie.effects.filter.IFWaldenFilter;
import com.dpc.selfie.effects.filter.IFXprollFilter;
import com.dpc.selfie.effects.library.GPUImage;
import com.dpc.selfie.effects.library.GPUImage3x3ConvolutionFilter;
import com.dpc.selfie.effects.library.GPUImageBrightnessFilter;
import com.dpc.selfie.effects.library.GPUImageColorInvertFilter;
import com.dpc.selfie.effects.library.GPUImageContrastFilter;
import com.dpc.selfie.effects.library.GPUImageDirectionalSobelEdgeDetectionFilter;
import com.dpc.selfie.effects.library.GPUImageEmbossFilter;
import com.dpc.selfie.effects.library.GPUImageExposureFilter;
import com.dpc.selfie.effects.library.GPUImageFilter;
import com.dpc.selfie.effects.library.GPUImageFilterGroup;
import com.dpc.selfie.effects.library.GPUImageGammaFilter;
import com.dpc.selfie.effects.library.GPUImageGrayscaleFilter;
import com.dpc.selfie.effects.library.GPUImageHighlightShadowFilter;
import com.dpc.selfie.effects.library.GPUImageHueFilter;
import com.dpc.selfie.effects.library.GPUImageLookupFilter;
import com.dpc.selfie.effects.library.GPUImageMonochromeFilter;
import com.dpc.selfie.effects.library.GPUImageNormalBlendFilter;
import com.dpc.selfie.effects.library.GPUImageOpacityFilter;
import com.dpc.selfie.effects.library.GPUImagePixelationFilter;
import com.dpc.selfie.effects.library.GPUImagePosterizeFilter;
import com.dpc.selfie.effects.library.GPUImageRGBFilter;
import com.dpc.selfie.effects.library.GPUImageSaturationFilter;
import com.dpc.selfie.effects.library.GPUImageSepiaFilter;
import com.dpc.selfie.effects.library.GPUImageSharpenFilter;
import com.dpc.selfie.effects.library.GPUImageSobelEdgeDetection;
import com.dpc.selfie.effects.library.GPUImageToneCurveFilter;
import com.dpc.selfie.effects.library.GPUImageTwoInputFilter;
import com.dpc.selfie.effects.library.GPUImageView;
import com.dpc.selfie.effects.library.GPUImageVignetteFilter;
import com.dpc.selfie.effects.library.GPUImageWhiteBalanceFilter;
import com.dpc.selfie.interfaces.OnCardClickListener;
import com.dpc.selfie.models.Editor;
import com.facebook.UiLifecycleHelper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Activity_Edit_First extends BaseActivity implements GPUImage.OnPictureSavedListener {

    private int[] frames = new int[]{R.drawable.default_frame_ten,R.drawable.default_frame_two,R.drawable.default_frame_three,R.drawable.default_frame_four,R.drawable.default_frame_five,R.drawable.default_frame_six,R.drawable.default_frame_seven,R.drawable.default_frame_eight,R.drawable.default_frame_nine};
    private final String[] names_frames = new String[]{"Frame1","Frame2", "Frame3", "Frame4", "Frame5", "Frame6", "Frame7", "Frame8", "Frame9"};
    private final String[] names_three = new String[]{"Brightness","Contrast", "Sepia", "Vignette"};
    private final String[] my_effects = new String[]{"1977","Amaro", "Brannan", "Earlybird", "Hefe", "Hudson", "Inkwell", "Lumo", "Lordkelvin", "Nashville", "Rise","Sierra", "Emboss", "Toaster"," Walden", "Xproll","Tonecurve", "Lookup" };
    private int main_position = 0,sub_position = 0,seek_position = 1;
    private String text = "";
    private ArrayList<Editor> edit_options = new ArrayList<Editor>();
    private Point point = new Point(0,0);
    private Edit_Adapter sub_adapter;
    private File outFile;
    private RecyclerView sub_list;
    private LinearLayout ll_edit, ll_seek;
    private PhotoSortrView textImage;

    private EditTabStrip tabs;
    private LinearLayout scroll;


    private RelativeLayout customLayout, framesContainer;
    private Edit edit;

    private MyProgress myProgress;
    private Bitmap bitmap = null, original_bitmap;
    private boolean collage_active = false;
    private String imageFilePath;
    private SeekBar seekBar = null;
    public static final int FLIP_VERTICAL = 1;
    public static final int FLIP_HORIZONTAL = 2;
    public static boolean ACTIVITY_EDIT = false;
    private boolean image_from_gallery = false;
    public static boolean ACTIVITY_EDIT_MOVABLE = false;
    public static boolean ACTIVITY_EDIT_SAVED = false;
    private static int RESULT_LOAD_IMG = 1;

    private int TAB_EFFECTS = 0, TAB_BRIGHTNESS = 1, TAB_FRAMES = 2, TAB_TEXT = 3;

    private Handler handler;
    private Runnable runnable;

    private GPUImageFilter mFilter;
    private GPUImageFilterTools.FilterAdjuster mFilterAdjuster;
    private GPUImageView image;
    GPUImageFilter filter;
    String savedfileName;
    ArrayList<GPUImageFilter> filterList = new ArrayList<GPUImageFilter>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setTitle("");
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_edit_first);
        this.create_image();
        init_views();

        edit_options.add(null);
        edit_options.add(null);
        edit_options.add(null);
        edit_options.add(null);


        ACTIVITY_EDIT_SAVED = false;
        set_edit_adapter();

        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);
    }

    private void init_views()
    {
        bitmap = BitmapFactory.decodeFile(this.getIntent().getStringExtra("url"));
        image = (GPUImageView) findViewById(R.id.image);
        image.setPreserveEGLContextOnPause(true);

        framesContainer = (RelativeLayout) findViewById(R.id.framesContainer);
        image.setImage(bitmap);

        scroll = (LinearLayout)findViewById(R.id.scroll);

        tabs = new EditTabStrip(this);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        param.gravity = Gravity.CENTER_VERTICAL;
        scroll.addView(tabs, param);
        populateTabStrip();
        sub_list = (RecyclerView)findViewById(R.id.edit_list);

        sub_list.setHasFixedSize(true);
        GridLayoutManager llm = new GridLayoutManager(this,1);
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        sub_list.setLayoutManager(llm);

        seekBar = (SeekBar) findViewById(R.id.volume_bar);
        ll_seek = (LinearLayout)findViewById(R.id.ll_seek);
        ll_seek.setVisibility(View.GONE);
        ll_edit = (LinearLayout)findViewById(R.id.bottom);
        customLayout = (RelativeLayout) findViewById(R.id.CustomLayout);


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                if (mFilterAdjuster != null) {
                    mFilterAdjuster.adjust(progress);
                }
                image.requestRender();

            }
        });

    }


    // This function is to update the Edit tab strip images
    private void set_edit_adapter()
    {


        if(main_position == TAB_EFFECTS || main_position == TAB_BRIGHTNESS || main_position == TAB_FRAMES|| main_position == TAB_TEXT )       {

            ACTIVITY_EDIT_MOVABLE = false;
            original_bitmap = bitmap;

            sub_list.setVisibility(View.VISIBLE);
            if(main_position == TAB_EFFECTS)
            {
                sub_adapter = new Edit_Adapter(getApplicationContext(), my_effects, main_position,new Edit().Create_Thumbnail(bitmap));
            }
            else if(main_position == TAB_BRIGHTNESS)
            {
                sub_adapter = new Edit_Adapter(getApplicationContext(), names_three, main_position,new Edit().Create_Thumbnail(bitmap));
            }
            else if(main_position == TAB_FRAMES)
            {
                sub_adapter = new Edit_Adapter(getApplicationContext(), names_frames, main_position,new Edit().Create_Thumbnail(bitmap));
            }
            else if(main_position == TAB_TEXT)
            {

            }
            sub_list.setAdapter(sub_adapter);

            // click event of adapter items like effects
            sub_adapter.setOnCardClickListener(new OnCardClickListener()
            {
                @Override
                public void onClick(View v, int position)
                {
                    seekBar.setProgress(50);
                    sub_position = position;

                    if (main_position == TAB_EFFECTS)
                    {
                        get_effect_by_position(position);
                        image.requestRender();

                    }

                    if (main_position == TAB_BRIGHTNESS)
                    {
                        open_seekbar();
                        get_seekbar_effect_by_position(position);
                        image.requestRender();
                    }
                    if (main_position == TAB_FRAMES)
                    {
                        get_frame_effect_by_position(position);
                        image.requestRender();
                    }

                }
            });
        }

    }

    @Override
    public void onPictureSaved(Uri uri) {
        Log.d("Image Saved", uri.getPath());
        String path = App.APP_FOLDER+"/"+savedfileName;
        Intent i = new Intent(Activity_Edit_First.this, Activity_Edit_Second.class);
        i.putExtra("url", path);
        startActivity(i);

    }


// Code for bottom tabs click events

    private class TabClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            for (int i = 0; i < tabs.getChildCount(); i++) {
                if (v == tabs.getChildAt(i)) {

                    main_position = i;
                    image.requestRender();

                    //sub_position = seek_position = 0;

                    text = "";
                    tabs.onViewPagerPageChanged(main_position,0);

                        sub_position = 0;
                        set_edit_adapter();
                        ll_seek.setVisibility(View.GONE);

                    return;
                }
            }
        }
    }

    // *****************All finctions defined here ******************************//



    // *************** Override functions *************************//

    @Override
    protected void onStart() {
        super.onStart();

        uiHelper.onStop();
    }

    @Override
    protected void onStop() {
        super.onStop();


    }

    @Override
    protected void onPause() {
        super.onPause();
        image.onPause();
        ACTIVITY_EDIT = false;

    }

    @Override
    protected void onResume() {
        super.onResume();
        image.onResume();
        ACTIVITY_EDIT = true;

    }

    @Override
    public void onBackPressed() {
        if(!App.is_new_user()) {
            remove_callbacks();
            super.onBackPressed();

        }
    }





    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(outFile != null && outFile.exists())
        {
            outFile.delete();
        }

        File dir = new File(App.APP_FOLDER);
        if(dir.exists())
        {
            File[] files = dir.listFiles();
            if(files != null && files.length > 0)
            {
                for(int i = 0;i < files.length;i++)
                {
                    files[i].delete();
                }
            }
        }
        uiHelper.onDestroy();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_edit_first, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                if(!App.is_new_user())
                {
                    remove_callbacks();
                    super.onBackPressed();
                }

                break;

            case R.id.apply:

                filterList.add(filter);

                break;

            case R.id.save:

                savedfileName = System.currentTimeMillis() + ".jpg";
                image.saveToPictures("Selfie", savedfileName, this);



                break;
        }
        return super.onOptionsItemSelected(item);
    }



    //********** End of Overriden functions***************** //


    private void open_seekbar()
    {

        set_edit_adapter();
        if(main_position == 0) {
            ll_seek.setVisibility(View.GONE);

        }
        else
        {
            ll_seek.setVisibility(View.VISIBLE);
            seekBar.setProgress(50);

        }
        sub_position = 0;
    }




    private void applyEffect(GPUImageFilter filter)
     {
         GPUImageFilterGroup filterGroup = new GPUImageFilterGroup();


         for(int i = 0;i<=filterList.size()-1;i++)
         {
             filterGroup.addFilter(filterList.get(i));
         }

         filterGroup.addFilter(filter);

         mFilterAdjuster = new GPUImageFilterTools.FilterAdjuster(filter);
        image.setFilter(filterGroup);
    }




    private void create_image()
    {
        File dir = new File(App.APP_FOLDER);
        dir.mkdirs();
        String fileName = String.format("%d.jpg", System.currentTimeMillis());
        outFile = new File(dir, fileName);
    }

    private void remove_callbacks()
    {
        if(handler != null && runnable != null)
        {
            handler.removeCallbacks(runnable);
        }
    }

    private void get_effect_by_position(int position)
    {
        if(position == 0)
        {
            filter = createFilterForType(FilterType.I_1977, position);
        }
        else if(position == 1)
        {
            filter = createFilterForType(FilterType.I_AMARO, position);

        }
        else if(position == 2)
        {
            filter = createFilterForType(FilterType.I_BRANNAN, position);

        }
        else if(position == 3)
        {
            filter = createFilterForType(FilterType.I_EARLYBIRD, position);

        }
        else if(position == 4)
        {
            filter = createFilterForType(FilterType.I_HEFE, position);

        }
        else if(position == 5)
        {
            filter = createFilterForType(FilterType.I_HUDSON, position);

        }
        else if(position == 6)
        {
            filter = createFilterForType(FilterType.I_INKWELL, position);

        }
        else if(position == 7)
        {
            filter = createFilterForType(FilterType.I_LOMO, position);

        }
        else if(position == 8)
        {
            filter = createFilterForType(FilterType.I_LORDKELVIN, position);

        }
        else if(position == 9)
        {
            filter = createFilterForType(FilterType.I_NASHVILLE, position);

        }
        else if(position == 10)
        {
            filter = createFilterForType(FilterType.I_RISE, position);

        }
        else if(position == 11)
        {
            filter = createFilterForType(FilterType.I_SIERRA, position);

        }
        else if(position == 12)
        {
            filter = createFilterForType(FilterType.EMBOSS, position);

        }
        else if(position == 13)
        {
            filter = createFilterForType(FilterType.I_TOASTER, position);

        }
        else if(position == 14)
        {
            filter = createFilterForType(FilterType.I_WALDEN, position);

        }
        else if(position == 15)
        {
            filter = createFilterForType(FilterType.I_XPROII, position);

        }
        else if(position == 16)
        {
            filter = createFilterForType(FilterType.TONE_CURVE, position);

        }
        else if(position == 17)
        {
            filter = createFilterForType(FilterType.LOOKUP_AMATORKA, position);

        }
        applyEffect(filter);


    }

    private void get_seekbar_effect_by_position(int position)
    {
        if(position == 0)
        {
            filter = createFilterForType(FilterType.BRIGHTNESS, position);

        }
        else if(position == 1)
        {
            filter = createFilterForType(FilterType.CONTRAST, position);

        }
        else if(position == 2)
        {
            filter = createFilterForType(FilterType.SEPIA, position);

        }
        else if(position == 3)
        {
            filter = createFilterForType(FilterType.VIGNETTE, position);

        }
        applyEffect(filter);

    }


    private void get_frame_effect_by_position(int position)
    {
        filter = createFilterForType(FilterType.BLEND_NORMAL, position);
        applyEffect(filter);

    }


    private void switchFilterTo(final GPUImageFilter filter) {
        if (mFilter == null
                || (filter != null && !mFilter.getClass().equals(filter.getClass()))) {
            mFilter = filter;
            image.setFilter(mFilter);
            mFilterAdjuster = new GPUImageFilterTools.FilterAdjuster(mFilter);
        }
    }

    private GPUImageFilter createBlendFilter(Context context, Class<? extends GPUImageTwoInputFilter> filterClass, int frameposition) {
        try {
            GPUImageTwoInputFilter filter = filterClass.newInstance();
            filter.setBitmap(BitmapFactory.decodeResource(context.getResources(), frames[frameposition]));
            image.setFilter(filter);

            return filter;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private enum FilterType {
        CONTRAST, GRAYSCALE, SHARPEN, SEPIA, SOBEL_EDGE_DETECTION, THREE_X_THREE_CONVOLUTION, FILTER_GROUP, EMBOSS, POSTERIZE, GAMMA, BRIGHTNESS, INVERT, HUE, PIXELATION,
        SATURATION, EXPOSURE, HIGHLIGHT_SHADOW, MONOCHROME, OPACITY, RGB, WHITE_BALANCE, VIGNETTE, TONE_CURVE, BLEND_COLOR_BURN, BLEND_COLOR_DODGE, BLEND_DARKEN, BLEND_DIFFERENCE,
        BLEND_DISSOLVE, BLEND_EXCLUSION, BLEND_SOURCE_OVER, BLEND_HARD_LIGHT, BLEND_LIGHTEN, BLEND_ADD, BLEND_DIVIDE, BLEND_MULTIPLY, BLEND_OVERLAY, BLEND_SCREEN, BLEND_ALPHA,
        BLEND_COLOR, BLEND_HUE, BLEND_SATURATION, BLEND_LUMINOSITY, BLEND_LINEAR_BURN, BLEND_SOFT_LIGHT, BLEND_SUBTRACT, BLEND_CHROMA_KEY, BLEND_NORMAL, LOOKUP_AMATORKA,
        I_1977, I_AMARO, I_BRANNAN, I_EARLYBIRD, I_HEFE, I_HUDSON, I_INKWELL, I_LOMO, I_LORDKELVIN, I_NASHVILLE, I_RISE, I_SIERRA, I_SUTRO,
        I_TOASTER, I_VALENCIA, I_WALDEN, I_XPROII
    }

    private GPUImageFilter createFilterForType(final FilterType type, int frame_position) {
        switch (type) {
            case CONTRAST:
                return new GPUImageContrastFilter(2.0f);
            case GAMMA:
                return new GPUImageGammaFilter(2.0f);
            case INVERT:
                return new GPUImageColorInvertFilter();
            case PIXELATION:
                return new GPUImagePixelationFilter();
            case HUE:
                return new GPUImageHueFilter(90.0f);
            case BRIGHTNESS:
                return new GPUImageBrightnessFilter(0.0f);
            case GRAYSCALE:
                return new GPUImageGrayscaleFilter();
            case SEPIA:
                return new GPUImageSepiaFilter();
            case SHARPEN:
                GPUImageSharpenFilter sharpness = new GPUImageSharpenFilter();
                sharpness.setSharpness(2.0f);
                return sharpness;
            case SOBEL_EDGE_DETECTION:
                return new GPUImageSobelEdgeDetection();
            case THREE_X_THREE_CONVOLUTION:
                GPUImage3x3ConvolutionFilter convolution = new GPUImage3x3ConvolutionFilter();
                convolution.setConvolutionKernel(new float[] {
                        -1.0f, 0.0f, 1.0f,
                        -2.0f, 0.0f, 2.0f,
                        -1.0f, 0.0f, 1.0f
                });
                return convolution;
            case EMBOSS:
                return new GPUImageEmbossFilter();
            case POSTERIZE:
                return new GPUImagePosterizeFilter();
            case FILTER_GROUP:
                List<GPUImageFilter> filters = new LinkedList<GPUImageFilter>();
                filters.add(new GPUImageContrastFilter());
                filters.add(new GPUImageDirectionalSobelEdgeDetectionFilter());
                filters.add(new GPUImageGrayscaleFilter());
                return new GPUImageFilterGroup(filters);
            case SATURATION:
                return new GPUImageSaturationFilter(1.0f);
            case EXPOSURE:
                return new GPUImageExposureFilter(0.0f);
            case HIGHLIGHT_SHADOW:
                return new GPUImageHighlightShadowFilter(0.0f, 1.0f);
            case MONOCHROME:
                return new GPUImageMonochromeFilter(1.0f, new float[]{0.6f, 0.45f, 0.3f, 1.0f});
            case OPACITY:
                return new GPUImageOpacityFilter(1.0f);
            case RGB:
                return new GPUImageRGBFilter(1.0f, 1.0f, 1.0f);
            case WHITE_BALANCE:
                return new GPUImageWhiteBalanceFilter(5000.0f, 0.0f);
            case VIGNETTE:
                PointF centerPoint = new PointF();
                centerPoint.x = 0.5f;
                centerPoint.y = 0.5f;
                return new GPUImageVignetteFilter(centerPoint, new float[] {0.0f, 0.0f, 0.0f}, 0.3f, 0.75f);
            case TONE_CURVE:
                GPUImageToneCurveFilter toneCurveFilter = new GPUImageToneCurveFilter();
                toneCurveFilter.setFromCurveFileInputStream(
                        Activity_Edit_First.this.getResources().openRawResource(R.raw.tone_cuver_sample));
                return toneCurveFilter;
           /* case BLEND_DIFFERENCE:
                return createBlendFilter( Activity_Edit_First.this, GPUImageDifferenceBlendFilter.class);
            case BLEND_SOURCE_OVER:
                return createBlendFilter( Activity_Edit_First.this, GPUImageSourceOverBlendFilter.class);
            case BLEND_COLOR_BURN:
                return createBlendFilter( Activity_Edit_First.this, GPUImageColorBurnBlendFilter.class);
            case BLEND_COLOR_DODGE:
                return createBlendFilter( Activity_Edit_First.this, GPUImageColorDodgeBlendFilter.class);
            case BLEND_DARKEN:
                return createBlendFilter( Activity_Edit_First.this, GPUImageDarkenBlendFilter.class);
            case BLEND_DISSOLVE:
                return createBlendFilter( Activity_Edit_First.this, GPUImageDissolveBlendFilter.class);
            case BLEND_EXCLUSION:
                return createBlendFilter( Activity_Edit_First.this, GPUImageExclusionBlendFilter.class);


            case BLEND_HARD_LIGHT:
                return createBlendFilter( Activity_Edit_First.this, GPUImageHardLightBlendFilter.class);
            case BLEND_LIGHTEN:
                return createBlendFilter( Activity_Edit_First.this, GPUImageLightenBlendFilter.class);
            case BLEND_ADD:
                return createBlendFilter( Activity_Edit_First.this, GPUImageAddBlendFilter.class);
            case BLEND_DIVIDE:
                return createBlendFilter( Activity_Edit_First.this, GPUImageDivideBlendFilter.class);
            case BLEND_MULTIPLY:
                return createBlendFilter( Activity_Edit_First.this, GPUImageMultiplyBlendFilter.class);
            case BLEND_OVERLAY:
                return createBlendFilter( Activity_Edit_First.this, GPUImageOverlayBlendFilter.class);
            case BLEND_SCREEN:
                return createBlendFilter( Activity_Edit_First.this, GPUImageScreenBlendFilter.class);
            case BLEND_ALPHA:
                return createBlendFilter( Activity_Edit_First.this, GPUImageAlphaBlendFilter.class);
            case BLEND_COLOR:
                return createBlendFilter( Activity_Edit_First.this, GPUImageColorBlendFilter.class);
            case BLEND_HUE:
                return createBlendFilter( Activity_Edit_First.this, GPUImageHueBlendFilter.class);
            case BLEND_SATURATION:
                return createBlendFilter( Activity_Edit_First.this, GPUImageSaturationBlendFilter.class);
            case BLEND_LUMINOSITY:
                return createBlendFilter( Activity_Edit_First.this, GPUImageLuminosityBlendFilter.class);
            case BLEND_LINEAR_BURN:
                return createBlendFilter( Activity_Edit_First.this, GPUImageLinearBurnBlendFilter.class);
            case BLEND_SOFT_LIGHT:
                return createBlendFilter( Activity_Edit_First.this, GPUImageSoftLightBlendFilter.class);
            case BLEND_SUBTRACT:
                return createBlendFilter( Activity_Edit_First.this, GPUImageSubtractBlendFilter.class);
            case BLEND_CHROMA_KEY:
                return createBlendFilter( Activity_Edit_First.this, GPUImageChromaKeyBlendFilter.class);*/
            case BLEND_NORMAL:
                return createBlendFilter( Activity_Edit_First.this, GPUImageNormalBlendFilter.class, frame_position);

            case LOOKUP_AMATORKA:
                GPUImageLookupFilter amatorka = new GPUImageLookupFilter();
                amatorka.setBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.lookup_amatorka));
                return amatorka;

            case I_1977:
                return new IF1977Filter( Activity_Edit_First.this);
            case I_AMARO:
                return new IFAmaroFilter( Activity_Edit_First.this);
            case I_BRANNAN:
                return new IFBrannanFilter( Activity_Edit_First.this);
            case I_EARLYBIRD:
                return new IFEarlybirdFilter( Activity_Edit_First.this);
            case I_HEFE:
                return new IFHefeFilter( Activity_Edit_First.this);
            case I_HUDSON:
                return new IFHudsonFilter( Activity_Edit_First.this);
            case I_INKWELL:
                return new IFInkwellFilter( Activity_Edit_First.this);
            case I_LOMO:
                return new IFLomoFilter( Activity_Edit_First.this);
            case I_LORDKELVIN:
                return new IFLordKelvinFilter( Activity_Edit_First.this);
            case I_NASHVILLE:
                return new IFNashvilleFilter( Activity_Edit_First.this);
            case I_RISE:
                return new IFRiseFilter( Activity_Edit_First.this);
            case I_SIERRA:
                return new IFSierraFilter( Activity_Edit_First.this);
            case I_SUTRO:
                return new IFSutroFilter( Activity_Edit_First.this);
            case I_TOASTER:
                return new IFToasterFilter( Activity_Edit_First.this);
            case I_VALENCIA:
                return new IFValenciaFilter( Activity_Edit_First.this);
            case I_WALDEN:
                return new IFWaldenFilter( Activity_Edit_First.this);
            case I_XPROII:
                return new IFXprollFilter( Activity_Edit_First.this);

            default:
                throw new IllegalStateException("No filter of that type!");
        }

    }



    /////////////////////////////////////////////
    private void populateTabStrip() {

        final View.OnClickListener tabClickListener = new TabClickListener();
        tabs.setWeightSum(3);
        for (int i = 0; i < 3; i++) {
            View tabView = null;
            ImageView tabTitleView = null;

            tabView = getLayoutInflater().inflate(R.layout.large_image, tabs,false);
            tabTitleView = (ImageView) tabView.findViewById(R.id.image);

            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    0,
                    FrameLayout.LayoutParams.MATCH_PARENT, 1.0f);
            param.gravity = Gravity.CENTER;
            tabView.setLayoutParams(param);

            if (i == 0) {
                tabTitleView.setBackgroundResource(R.drawable.effects_selected);
            } else if (i == 1) {
                tabTitleView.setBackgroundResource(R.drawable.brightness_selected);
            } else if (i == 2) {
                tabTitleView.setBackgroundResource(R.drawable.frames_selected);
            }

            tabView.setOnClickListener(tabClickListener);

            tabs.addView(tabView);
        }
    }


}
