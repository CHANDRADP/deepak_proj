/***
 * Copyright (c) 2012 readyState Software Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.dpc.selfie.amazon;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.DeleteObjectsResult;
import com.amazonaws.services.s3.model.MultiObjectDeleteException;
import com.dpc.selfie.App;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.enums.Params;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class UploadService extends IntentService {

    public static final String UPLOAD_STATE_CHANGED_ACTION = "com.dpc.selfie.UPLOAD_STATE_CHANGED_ACTION";
    public static final String UPLOAD_CANCELLED_ACTION = "com.dpc.wtp.UPLOAD_CANCELLED_ACTION";
    public static final String UPLOAD_STATE = "upload_state";
    public static final String DELETE_IMAGE = "delete";
    public static final String MESSAGE = "msg";

    private AmazonS3Client s3Client;
    private Uploader uploader;


    public UploadService() {
        super("com.dpc.selfie");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        s3Client = new AmazonS3Client(
                new BasicAWSCredentials(C.S3_AK, C.S3_SK));


        IntentFilter f = new IntentFilter();
        f.addAction(UPLOAD_CANCELLED_ACTION);
        registerReceiver(uploadCancelReceiver, f);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String image = null,selfie = null;
        if (intent.hasExtra(Params.IMAGE.get_value())) {
            image = intent.getStringExtra(Params.IMAGE.get_value());
            if (image != null) {
                image = new C().get_compressed_image(image,false);
                if (image == null) {
                    broadcastState(-1, "Error in retrieving image");
                    return;
                }
                File IMG_ToUpload = new File(image);
                String s3BucketName = C.S3_BUCKET;

                // create a new uploader for the image
                String s3_image_name = String.valueOf(App.getMyId())+File.separator+"SELFIE_" + System.currentTimeMillis();
                s3_image_name += image.substring(image.lastIndexOf("."));

                uploader = new Uploader(this, s3Client, s3BucketName, s3_image_name, IMG_ToUpload);

                broadcastState(0, "Uploading...");

                try {
                    String s3_img_Location = uploader.start(); // initiate the image upload
                    if (s3_img_Location != null) {
                        if (IMG_ToUpload.exists()) {
                            String thumbnail = new C().Create_Thumbnail(image);
                            File THUMB_ToUpload = new File(thumbnail);
                            IMG_ToUpload.delete();

                            // create a new uploader for the thumbnail
                            String s3_thumb_name = String.valueOf(App.getMyId())+File.separator+"THUMB_" + System.currentTimeMillis();
                            s3_thumb_name += image.substring(image.lastIndexOf("."));
                            uploader = new Uploader(this, s3Client, s3BucketName, s3_thumb_name, THUMB_ToUpload);

                            String s3_thumb_Location = uploader.start(); // initiate the thumbnail upload
                            if (s3_thumb_Location != null) {
                                THUMB_ToUpload.delete();
                            }

                        }
                    }

                    broadcastState(1, "Profile Picture updated successfully");
                } catch (UploadIterruptedException uie) {
                    broadcastState(-1, "User interrupted");
                } catch (Exception e) {
                    e.printStackTrace();
                    broadcastState(-1, "Error in uploading the image, please try again");
                }
            } else {
                broadcastState(-1, "Error in retrieving image");
            }
            return ;
        }
        else if (intent.hasExtra(Params.SELFIE.get_value())) {
            selfie = intent.getStringExtra(Params.SELFIE.get_value());
            if(selfie != null)
            {
                selfie = new C().get_compressed_image(selfie,true);
                if (selfie == null) {
                    broadcastState(-1, "Error in retrieving image");
                    return;
                }
                File IMG_ToUpload = new File(selfie);
                String s3BucketName = C.S3_BUCKET;

                // create a new uploader for the image
                String s3_image_name = String.valueOf(App.getMyId())+File.separator+"SELFIE_" + System.currentTimeMillis();
                s3_image_name += selfie.substring(selfie.lastIndexOf("."));

                uploader = new Uploader(this, s3Client, s3BucketName, s3_image_name, IMG_ToUpload);

                broadcastState(0, "Uploading...");

                try {
                    String s3_img_Location = uploader.start(); // initiate the image upload
                    if (s3_img_Location != null) {
                        if (IMG_ToUpload.exists()) {
                            IMG_ToUpload.delete();
}
                    }

                    broadcastState(1, s3_img_Location);
                } catch (UploadIterruptedException uie) {
                    broadcastState(-1, "User interrupted");
                } catch (Exception e) {
                    e.printStackTrace();
                    broadcastState(-1, "Error in uploading the image, please try again");
                }
            } else {
                broadcastState(-1, "Error in retrieving image");
            }
            return ;
        }
        if(intent.hasExtra(DELETE_IMAGE))
        {
            image = intent.getStringExtra(DELETE_IMAGE);
            Log.d("Event", "deleting images");
            DeleteObjectsRequest multiObjectDeleteRequest = new DeleteObjectsRequest(C.S3_BUCKET);
            List<DeleteObjectsRequest.KeyVersion> keys = new ArrayList<DeleteObjectsRequest.KeyVersion>();
            if(image != null)
            keys.add(new DeleteObjectsRequest.KeyVersion(image));

            multiObjectDeleteRequest.setKeys(keys);

            try {
                DeleteObjectsResult delObjRes = s3Client.deleteObjects(multiObjectDeleteRequest);
                System.out.format("Deleted Successfully");
                C.alert(getApplicationContext(),"Deleted Successfully");

            } catch (MultiObjectDeleteException e) {
                // Process exception.
            }
        }
    stopSelf();
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(uploadCancelReceiver);
        super.onDestroy();
    }

    private void broadcastState(int state, String msg) {
        Intent intent = new Intent(UPLOAD_STATE_CHANGED_ACTION);
        Bundle b = new Bundle();
        b.putInt(UPLOAD_STATE, state);
        b.putString(MESSAGE, msg);
        intent.putExtras(b);
        sendBroadcast(intent);
    }


    private BroadcastReceiver uploadCancelReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (uploader != null) {
                uploader.interrupt();
            }

        }

    };


}
