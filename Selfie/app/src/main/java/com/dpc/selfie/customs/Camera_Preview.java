package com.dpc.selfie.customs;

/**
 * Created by D-P-C on 10-Mar-15.
 */

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.hardware.camera2.CameraManager;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import com.dpc.selfie.Utils.C;

import java.io.IOException;
import java.util.List;

public class Camera_Preview extends ViewGroup implements SurfaceHolder.Callback {
    private final String TAG = "Preview";
    private Context context;
    private SurfaceView mSurfaceView;
    private SurfaceHolder mHolder;
    private Size mPreviewSize;
    private List<Size> mSupportedPreviewSizes;
    private Camera mCamera;
    public static boolean is_front = false, is_previewing = true;

    private boolean flash = false,isFlash_available;
    public Camera_Preview(Context context, SurfaceView sv, boolean is_front, boolean isFlash_available) {
        super(context);
        this.context = context;
        this.mSurfaceView = sv;
        this.is_front = is_front;
        this.isFlash_available = isFlash_available;
        mHolder = mSurfaceView.getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }


    public Camera getmCamera() {
        return this.mCamera;
    }


    public void refresh() {
        if (!is_previewing)
            mCamera.startPreview();
    }

    public void disable() {
        if (is_previewing) {
            mCamera.stopPreview();
            is_previewing = false;
        }

    }

    public void change_camera() {
        this.is_front = !is_front;
        mCamera.stopPreview();
        mCamera.release();
        surfaceCreated(mHolder);
    }
    public boolean is_front_enabled()
    {
        return is_front;
    }
    public boolean is_flash_enabled()
    {
        return flash;
    }
    public void toggle_flash()
    {
        mCamera.stopPreview();
        mCamera.release();
        surfaceCreated(mHolder);
        flash = !flash;
    }


    public void surfaceCreated(SurfaceHolder holder) {
        // The Surface has been created, acquire the camera and tell it where
        // to draw.
        mHolder = holder;
        int cameraId = 0;
        if (is_front) {
            cameraId = findFrontFacingCamera();
        }
        else
            cameraId = findBackFacingCamera();

        if (cameraId < 0) {
            if (is_front)
                C.alert(context, "Front camera not available");
            else
                C.alert(context, "Camera not available");
        } else {

            try {
                mCamera = Camera.open(cameraId);

                mCamera.setPreviewDisplay(mHolder);
                mCamera.setDisplayOrientation(90);
                Camera.Parameters p = mCamera.getParameters();
                mSupportedPreviewSizes = p.getSupportedPreviewSizes();
                p.set("camera-id", 2);
                p.setJpegQuality(100);
                Size size = getBestPreviewSize(0, 0, p);

                //     p.setPictureSize(size.width, size.height);

                if(isFlash_available) {
                    if (flash)
                        p.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                    else
                        p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                }
                mCamera.setParameters(p);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        if (mCamera != null) {

            mCamera.stopPreview();

            Camera.Parameters parameters = mCamera.getParameters();
            if (mPreviewSize != null) {
                parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
            }
            requestLayout();

            mCamera.setParameters(parameters);

            mCamera.startPreview();
            is_previewing = true;

        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            is_previewing = false;
        }
    }

    public int findFrontFacingCamera() {
        int cameraId = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {

                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

    private int findBackFacingCamera() {
        int cameraId = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {

                cameraId = i;
                break;
            }
        }
        return cameraId;
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // We purposely disregard child measurements because act as a
        // wrapper to a SurfaceView that centers the camera preview instead
        // of stretching it.
        final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        setMeasuredDimension(width, height);
        if (mSupportedPreviewSizes != null) {
            mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width, height);
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        if (changed && getChildCount() > 0) {
            final View child = getChildAt(0);

            final int width = r - l;
            final int height = b - t;

            int previewWidth = width;
            int previewHeight = height;
            if (mPreviewSize != null) {
                previewWidth = mPreviewSize.width;
                previewHeight = mPreviewSize.height;
            }

            // Center the child SurfaceView within the parent.
            if (width * previewHeight > height * previewWidth) {
                final int scaledChildWidth = previewWidth * height / previewHeight;
                child.layout((width - scaledChildWidth) / 2, 0,
                        (width + scaledChildWidth) / 2, height);
            } else {
                final int scaledChildHeight = previewHeight * width / previewWidth;
                child.layout(0, (height - scaledChildHeight) / 2,
                        width, (height + scaledChildHeight) / 2);
            }
        }
    }

    private Camera.Size getBestPreviewSize(int w, int h, Camera.Parameters parameters){
        Camera.Size bestSize = null;
        List<Camera.Size> sizeList = parameters.getSupportedPreviewSizes();

        bestSize = sizeList.get(0);

        for(int i = 1; i < sizeList.size(); i++){
            if((sizeList.get(i).width * sizeList.get(i).height) >
                    (bestSize.width * bestSize.height)){
                bestSize = sizeList.get(i);
            }
        }
        C.alert(getContext(),String.valueOf(bestSize.width)+":"+String.valueOf(bestSize.height));
        return bestSize;
    }
    private Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) w / h;
        if (sizes == null) return null;

        Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        // Try to find an size match aspect ratio and size
        for (Size size : sizes) {
            optimalSize = size;

            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }

        }

        // Cannot find the one match the aspect ratio, ignore the requirement
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Size size : sizes) {
                optimalSize = size;
                 if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }

            }
        }
        return optimalSize;
    }


}