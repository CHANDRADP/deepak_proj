package com.dpc.selfie.Act;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

import com.dpc.selfie.R;
import com.dpc.selfie.customs.BaseActivity;

/**
 * Created by D-P-C on 30-Jan-15.
 */
public class Splash extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_splash);
        generate_SHA_key_for_facebook();
        new Thread() {

            public void run() {

                try {

                    Intent i = new Intent(Splash.this, Activity_Login.class);
                    Thread.sleep(200);

                    startActivity(i);
                    finish();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }
}
