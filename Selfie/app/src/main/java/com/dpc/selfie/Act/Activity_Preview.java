package com.dpc.selfie.Act;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dpc.selfie.App;
import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.adapters.ImagePagerAdapter;
import com.dpc.selfie.customs.BaseActivity;
import com.dpc.selfie.customs.MyProgress;
import com.dpc.selfie.enums.*;
import com.dpc.selfie.enums.Process;
import com.dpc.selfie.models.Server_Params;
import com.dpc.selfie.models.User;

import org.json.JSONException;
import org.json.JSONObject;

public class Activity_Preview extends BaseActivity {
    private ViewPager viewPager;
    private LinearLayout gallery;
    private int selected_position = 0,old_position = 0;
    private int h = 0;
    private ImagePagerAdapter imagePagerAdapter;
    private long user_id = -1;
    int route=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        if(this.getIntent().hasExtra(Params.ID.get_value()))
        {
            user_id = this.getIntent().getLongExtra(Params.ID.get_value(),-1);
        }
        init_views();
    }

    private void init_views()
    {
        viewPager = (ViewPager)findViewById(R.id.viewpager);
        gallery = (LinearLayout)findViewById(R.id.gallery);
        imagePagerAdapter = new ImagePagerAdapter(this);
        viewPager.setAdapter(imagePagerAdapter);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                old_position = selected_position;
                selected_position = position;
                update_gallery();

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        generate_gallery();
    }


    private void generate_gallery()
    {
        int selfie_size = 0;
       final GalleryClickListener galleryClickListener = new GalleryClickListener();
        h = (int)getResources().getDimension(R.dimen.hundred);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(h,h);
        params.gravity = Gravity.CENTER_VERTICAL;

        try
        {
            if(Activity_Profile.selfies.size()!=0)
            {
                route =1;
                selfie_size = Activity_Profile.selfies.size();
            }
        }
        catch (Exception e)
        {
            Log.d("Caught Exception", e.toString());
        }

        try
        {
            if(selfie_size==0)
            {
                route = 2;
                selfie_size = Activity_Collage.selfies.size();
            }
        }
        catch (Exception e)
        {
            Log.d("Caught Exception", e.toString());
        }

        for(int i=0;i<Activity_Profile.selfies.size();i++)
        {
            View v = getLayoutInflater().inflate(R.layout.small_image,null);
            v.setLayoutParams(params);
            display_image(Activity_Profile.selfies.get(i).getImage_url(),(ImageView)v.findViewById(R.id.icon),false);
            if(i == 0) {
                v.setPadding(3, 3, 3, 3);
                v.setBackgroundResource(R.drawable.gallery_selected);
            }
            gallery.addView(v);
            v.setOnClickListener(galleryClickListener);
        }

    }



    private void update_gallery()
    {
        if(old_position != selected_position) {
            View v1 = gallery.getChildAt(old_position);
            v1.setPadding(0,0,0,0);


            v1 = gallery.getChildAt(selected_position);
            v1.setPadding(3, 3, 3, 3);
            v1.setBackgroundResource(R.drawable.gallery_selected);

        }
    }
    private void update_viewpager()
    {
        if(selected_position > -1) {
           viewPager.setCurrentItem(selected_position,true);
        }
    }



    private class GalleryClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            for (int i = 0; i < gallery.getChildCount(); i++) {
                if (v == gallery.getChildAt(i)) {
                    old_position = selected_position;
                    selected_position = i;
                    update_gallery();
                    update_viewpager();
                    return;
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_preview, menu);
        if(user_id > 0)
        {
            if(App.getMyId() == user_id)
            {
                menu.findItem(R.id.delete).setVisible(true);
            }
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                super.onBackPressed();
                break;
            case R.id.delete:
                if(Activity_Profile.selfies.size() > 0)
                {
                    new Delete_Image().execute();
                }

                break;


        }
        return super.onOptionsItemSelected(item);
    }
    private void refresh_items()
    {
        Activity_Profile.selfies.remove(selected_position);
        gallery.removeViewAt(selected_position);
        viewPager.setAdapter(new ImagePagerAdapter(this));
        if(Activity_Profile.selfies.size() > 0)
        {
            View v = null;
            if(gallery.getChildAt(selected_position) != null)
            {
                v = gallery.getChildAt(selected_position);
            }
            else
            {
                v = gallery.getChildAt(selected_position-1);
                selected_position--;
            }
            if(v != null) {
                v.setPadding(3, 3, 3, 3);
                v.setBackgroundResource(R.drawable.gallery_selected);
            }

            viewPager.setCurrentItem(selected_position,true);
        }
        else
        {
            App.set_user_mode(true);
            startActivity(new Intent(this,Activity_Edit_First.class));
            finish();
        }

    }
    /////////////////delete selfie
    private class Delete_Image extends AsyncTask<String, Long, Boolean>
    {
        private MyProgress myProgress;
        private long id = -1;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            myProgress = new MyProgress(Activity_Preview.this);
            myProgress.setCancelable(false);
            myProgress.setTitle("Deleting...");
            myProgress.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {
        id = Activity_Profile.selfies.get(selected_position).getId();
        return delete_image();
    }

        @Override
        protected void onPostExecute(Boolean result) {
        if(result)
        {
            refresh_items();
            Intent intent = new Intent(Activity_Feeds.POST_STATE);
            intent.putExtra(Process.DELETE_IMAGE.get_value(),1);
            intent.putExtra(Params.IMAGE_ID.get_value(),id);
            sendBroadcast(intent);
            C.alert(getApplicationContext(),"Deleted successfully");
        }else
            {
                C.alert(getApplicationContext(),"Error in deleting photo, please try again");
            }
        if(myProgress != null)
            myProgress.dismiss();


    }

        @Override
        protected void onCancelled() {
        super.onCancelled();
    }

    public boolean delete_image() {
        boolean success = false;
        JSONParser parser = new JSONParser();
        JSONObject response = null;
        try {

            response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                    new Server_Params().delete_selfie(id));
            if (response != null) {
                Log.d("response", response.toString());
                if (response.has(Params.STATUS.get_value())) {
                    int status = response.getInt(Params.STATUS.get_value());
                    if (status == 200) {
                        JSONObject data = response.getJSONObject(Params.DATA.get_value());
                      int  id = data.getInt(Params.ID.get_value());
                        if(id > 0)
                        success = true;

                        User user = db.Get_User_Details(App.getMyId());
                        user.setImage(data.getString(Params.IMAGE.get_value()));
                        db.Sync_App_User(user);
                        if(Activity_Profile.selfies.size() == 1)
                        {
                            App.set_user_mode(true);
                        }

                    }
                }
            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return success;
    }

}
}
