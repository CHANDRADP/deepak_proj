package com.dpc.selfie.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dpc.selfie.R;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.models.MyBrands;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by D-P-C on 18-Feb-15.
 */
public class MyBrands_Adapter extends BaseAdapter {
    private LayoutInflater inflater;
    private ViewHolder holder;
    private View v;
    private List<MyBrands> items = new ArrayList<>();
    private Context c;
    private ImageLoader imageLoader;
    private DisplayImageOptions displayImageOptions;
    private class ViewHolder {
        TextView txtBrand;
        Text txtOffers;
        ImageView logo_Image;
    }
    public static final int[] logos = new int[] {R.drawable.duracell,
            R.drawable.pizzahut,
            R.drawable.dominos,
            R.drawable.nestle,
            R.drawable.apple,
            R.drawable.kelloggs,
            R.drawable.panasonic,
            R.drawable.disney,

    };


    public MyBrands_Adapter(Context c, List<MyBrands> l, ImageLoader imageLoader)
    {
        this.c = c;
        this.items = l;
        this.imageLoader = imageLoader;
        this.displayImageOptions = new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.ic_action_person)
                .showImageForEmptyUri(R.drawable.ic_action_person)
                .showImageOnFail(R.drawable.ic_action_person)
                .cacheInMemory(true)
                        //	.cacheOnDisc(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new FadeInBitmapDisplayer(5))
                .build();

    }



    public void update_list(List<MyBrands> l)
    {
        this.items = l;
        notifyDataSetChanged();
    }
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        MyBrands brand = items.get(position);

        v = convertView;
        if (v == null)
        {
            inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_mybrand, null);
            holder = new ViewHolder();
            holder.txtBrand = (TextView) v.findViewById(R.id.txtBrand);
            holder.txtOffers = (Text) v.findViewById(R.id.txtOffers);
            holder.logo_Image = (ImageView) v.findViewById(R.id.logo_Image);

            v.setTag(holder);

        }
        else
            holder = (ViewHolder) v.getTag();


        /*imageLoader.displayImage(brand.getLogo_url(), holder.logo_Image, displayImageOptions, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                //  holder.image.setBackgroundResource(R.drawable.user);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });*/
        int drawable_id = logos[Integer.parseInt(brand.getLogo_url())];

        holder.logo_Image.setImageResource(drawable_id);


        holder.txtBrand.setText(brand.getBrand_Name());
        //int points = brand.getNo_of_points();
        int offers = brand.getNo_of_offers();
        holder.txtOffers.setText(Integer.toString(offers));
        return v;

    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return items.size();
    }


    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return items.get(position);
    }


    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

}
