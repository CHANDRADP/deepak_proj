package com.dpc.selfie.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dpc.selfie.Act.Activity_Feeds;
import com.dpc.selfie.App;
import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.DB;
import com.dpc.selfie.Utils.Edit;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.Utils.Net;
import com.dpc.selfie.customs.CircleImage;
import com.dpc.selfie.customs.EText;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.customs.TextB;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.BrandOffers;
import com.dpc.selfie.models.Celebrity;
import com.dpc.selfie.models.Comment;
import com.dpc.selfie.models.Server_Params;
import com.dpc.selfie.models.User;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

/**
 * Created by D-P-C on 06-Mar-15.
 */
public class Comment_Adapter extends RecyclerView.Adapter<Comment_Adapter.ViewHolder> {

    private Context context;

    private DB db;

    private Net net;

    String token = null;

    private ArrayList<Comment> comments ;

    String newComment = null;

    boolean edit_status = false;
    boolean delete_status = false;

    int comment_position = 0;


    public Comment_Adapter(Context context, ArrayList<Comment> comments) {
        this.context = context;
        db = new DB(context);
        this.comments = comments;

    }

    public void update_items(Comment comment)
    {
        if(comment != null)
        {
           // this.comments.add(comment);
            notifyItemInserted(comments.size() - 1);
        }


    }
    @Override
    public int getItemCount() {
        if(comments == null)
            return 0;
        else
            return comments.size();
    }
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        final Comment comment= comments.get(i);
        if(App.getMyId() != comment.getUser_id())
        {
            viewHolder.edit.setVisibility(View.GONE);
            viewHolder.delete.setVisibility(View.GONE);
        }
        User user = db.Get_User_Details(comment.getUser_id());
        String name = comment.getU_name();
        if(user != null && App.getMyId() == comment.getUser_id())
            name = user.getF_name();
        net = new Net(context);
        display_image(comment.getUser_image(),viewHolder.dp,true);
        viewHolder.name.setText(name);
        viewHolder.comment.setText(comment.getComment());
        viewHolder.time.setText(C.getDateDifference(C.UTC_to_Local(comment.getDate())));

        viewHolder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    comment_position = i;

                    final Dialog dialog = new Dialog(context);

                    //setting custom layout to dialog
                    dialog.setContentView(R.layout.custom_edit_comment_dialog);
                    final EText txtEditedComment = (EText) dialog.findViewById(R.id.txtEditedComment);
                    Button btnSave = (Button) dialog.findViewById(R.id.btnSave);
                    Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
                    txtEditedComment.setText("");
                    txtEditedComment.append(comment.getComment());
                    dialog.show();

                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.cancel();
                        }
                    });

                    btnSave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            boolean result = false;
                            if(comment.getComment().equalsIgnoreCase(txtEditedComment.getText().toString()))
                            {
                                Toast.makeText(context,"Comment not changed.", Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                dialog.cancel();
                                newComment = txtEditedComment.getText().toString();
                                if(net.isConnectingToInternet())
                                {
                                    try {
                                        result = new EditCommentTask(context, viewHolder.comment, comment_position, newComment, comment.getComment_id()).execute().get();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    } catch (ExecutionException e) {
                                        e.printStackTrace();
                                    }
                                }
                                else
                                {
                                    ((Activity)context).runOnUiThread(new Runnable() {
                                        public void run() {
                                                C.alert(context,
                                                        "No Internet Connection");
                                        }
                                    });
                                }

                                if(result)
                                {
                                    comment.setComment(newComment);
                                }

                            }
                        }
                    });



            }
        });

        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                comment_position = i;
                final Dialog dialog = new Dialog(context);

                //setting custom layout to dialog
                dialog.setContentView(R.layout.custom_confirmation_dialog);
                Text txtMessage = (Text) dialog.findViewById(R.id.txtMessage);
                Button btnYes = (Button) dialog.findViewById(R.id.btnYes);
                Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
                txtMessage.setText("Are you sure, you want to delete the comment?");
                dialog.show();

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean result = false;
                        dialog.cancel();

                        if(net.isConnectingToInternet())
                        {
                            try
                            {
                                result = new DeleteCommentTask(context, comment_position, comment.getComment_id()).execute().get();
                            }
                            catch (InterruptedException e)
                            {
                                e.printStackTrace();
                            }
                            catch (ExecutionException e)
                            {
                                e.printStackTrace();
                            }
                        }
                        else
                        {
                            ((Activity)context).runOnUiThread(new Runnable()
                            {
                                public void run()
                                {
                                        C.alert(context,
                                                "No Internet Connection");
                                }
                            });
                        }

                        if(result)
                        {
                            comment.setComment(newComment);
                        }
                    }
                });
            }
        });

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup,final int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_comment, viewGroup, false);

        return new ViewHolder(itemView);
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        protected CircleImage dp;
        protected TextB name;
        protected Text comment,time, edit, delete;


        public ViewHolder(View v) {
            super(v);

            dp = (CircleImage)v.findViewById(R.id.image);
            name = (TextB)v.findViewById(R.id.name);
            comment = (Text)v.findViewById(R.id.comment);
            time = (Text)v.findViewById(R.id.time);
            edit = (Text)v.findViewById(R.id.txtEdit);
            delete = (Text)v.findViewById(R.id.txtDelete);
        }
    }

    private void display_image(String url,View v,boolean is_circle_image)
    {

        try {
            Picasso.with(context)
                    .load(url)
                    .placeholder(R.drawable.ic_action_person)
                    .error(R.drawable.ic_action_person)
                    .into(is_circle_image ? (CircleImage)v : (ImageView)(v));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }



    private class EditCommentTask extends AsyncTask<String, Long, Boolean>
    {
        Context c;

        String comment = null;
        long comment_id = 0;
        Text txtComment;
        int comment_position = 0;

        public EditCommentTask(Context c, Text txtComment, int comment_position, String comment, long comment_id)
        {
            this.c = c;
            this.comment = comment;
            this.comment_id = comment_id;
            this.txtComment = txtComment;
            this.comment_position = comment_position;
        }

        @Override
        protected Boolean doInBackground(String... params)
        {

            return edit_comment(comment,comment_id);

        }

        @Override
        protected void onPostExecute(Boolean result)
        {

        }

        @Override
        protected void onCancelled()
        {
            super.onCancelled();
        }


        public boolean edit_comment(final String comment, long comment_id)
        {

            JSONParser parser = new JSONParser();
            try
            {
                JSONObject response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                        new Server_Params().update_c(comment, comment_id));
                if (response != null)
                {
                    if (response.has(Params.STATUS.get_value()))
                    {
                        final int status = response.getInt(Params.STATUS.get_value());
                        JSONObject data = response.getJSONObject(Params.DATA.get_value());

                        final int id = data.getInt(Params.ID.get_value());


                            ((Activity)context).runOnUiThread(new Runnable() {
                                public void run() {

                                    if (status == 200 && id > 0)
                                    {
                                        edit_status = true;
                                        C.alert(context,
                                                "Edited Successfully");
                                        txtComment.setText(newComment);
                                        Comment comment = comments.get(comment_position);
                                        comment.setComment(newComment);



                                    }
                                    else
                                    {
                                        C.alert(context,
                                                "Failed to update");
                                    }
                                }
                            });




                    }
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            return edit_status;
        }
    }




    private class DeleteCommentTask extends AsyncTask<String, Long, Boolean>
    {
        Context c;

        long comment_id = 0;
        int comment_position = 0;

        public DeleteCommentTask(Context c, int comment_position, long comment_id)
        {
            this.c = c;
            this.comment_id = comment_id;
            this.comment_position = comment_position;
        }

        @Override
        protected Boolean doInBackground(String... params)
        {

            return delete_comment(comment_id);

        }

        @Override
        protected void onPostExecute(Boolean result)
        {

        }

        @Override
        protected void onCancelled()
        {
            super.onCancelled();
        }


        public boolean delete_comment(long comment_id)
        {

            JSONParser parser = new JSONParser();
            try
            {
                JSONObject response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                        new Server_Params().delete_c(comment_id));
                if (response != null)
                {
                    if (response.has(Params.STATUS.get_value()))
                    {
                        final int status = response.getInt(Params.STATUS.get_value());
                        JSONObject data = response.getJSONObject(Params.DATA.get_value());

                        final int id = data.getInt(Params.ID.get_value());


                        ((Activity)context).runOnUiThread(new Runnable() {
                            public void run() {

                                if (status == 200 && id > 0)
                                {
                                    delete_status = true;
                                    C.alert(context,
                                            "Deleted Comment Successfully");

                                    Comment comment = comments.get(comment_position);
                                    comments.remove(comment);

                                    notifyDataSetChanged();
                                }
                                else
                                {
                                    C.alert(context,
                                            "Failed to Delete Comment");
                                }
                            }
                        });




                    }
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            return delete_status;
        }
    }


}