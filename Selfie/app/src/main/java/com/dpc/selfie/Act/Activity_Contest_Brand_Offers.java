package com.dpc.selfie.Act;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.adapters.BrandCategory_Adapter;
import com.dpc.selfie.adapters.BrandOffers_Adapter;
import com.dpc.selfie.customs.BaseActivity;
import com.dpc.selfie.customs.DividerItemDecorator;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.BrandCategory;
import com.dpc.selfie.models.Brand_Offer_Details;
import com.dpc.selfie.models.Server_Params;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Activity_Contest_Brand_Offers extends BaseActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private long category_id_selected = 0;
    private HashMap<Long,Brand_Offer_Details> brand_offers_map = new HashMap<>();
    private ArrayList<Brand_Offer_Details> offers_list = new ArrayList<>();
    private String image_path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity__contest__brand_offers);

        image_path = getIntent().getStringExtra("image_path");

        category_id_selected = getIntent().getLongExtra(Params.CATEGORY_ID_SELECTED.get_value(),0);

        mRecyclerView = (RecyclerView) findViewById(R.id.offer_list);

        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.addItemDecoration(new DividerItemDecorator(this,DividerItemDecorator.VERTICAL_LIST));
        new Get_Offers_By_CategoryId().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_contest_brand_offers, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case android.R.id.home:

                super.onBackPressed();
                break;
        }

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Toast.makeText(getApplicationContext(), "Settings Clicked",
                    Toast.LENGTH_SHORT).show();
            return true;
        } else if (id == R.id.action_search) {
            Toast.makeText(getApplicationContext(), "Search Clicked",
                    Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class Get_Offers_By_CategoryId  extends AsyncTask<String, Long, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {

            return get_offers_by_category_id(category_id_selected);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            mAdapter = new BrandOffers_Adapter(offers_list, Activity_Contest_Brand_Offers.this, image_path);
            mRecyclerView.setAdapter(mAdapter);

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        public boolean get_offers_by_category_id(long category_id)
        {
            boolean success = false;
            JSONParser parser = new JSONParser();
            JSONObject response = null;
            try {

                response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                        new Server_Params().get_offers_by_categoryId(category_id));
                if(response != null)
                {
                    Log.d("response", response.toString());
                    if(response.has(Params.STATUS.get_value()))
                    {
                        int status = response.getInt(Params.STATUS.get_value());
                        if (status == 200)
                        {
                            JSONObject obj = response.getJSONObject(Params.DATA.get_value());
                            JSONArray array = obj.getJSONArray("offers");
                            brand_offers_map = new C().parse_brand_offers(array);

                            for(Brand_Offer_Details offers : brand_offers_map.values())
                            {
                                offers_list.add(offers);
                            }
                            success = true;
                        }
                    }
                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return success;
        }

    }

}