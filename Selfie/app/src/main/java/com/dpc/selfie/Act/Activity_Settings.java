package com.dpc.selfie.Act;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.dpc.selfie.App;
import com.dpc.selfie.R;
import com.dpc.selfie.adapters.Settings_Adapter;
import com.dpc.selfie.customs.DividerItemDecorator;
import com.dpc.selfie.interfaces.OnCardClickListener;
import com.dpc.selfie.models.Setting;

import java.util.ArrayList;

public class Activity_Settings extends ActionBarActivity {

    private final String[] names = new String[]{"General","Sound Effects","Vibrations","Privacy Mode","Notifications","Feed","Ratings","Comments","#Selfie","About"};
    private int i = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ArrayList<Setting> settings = new ArrayList<>();

        settings.add(new Setting(names[i++],false,true));
        settings.add(new Setting(names[i++], App.get_sound(),false));
        settings.add(new Setting(names[i++],App.get_vibration(),false));
        settings.add(new Setting(names[i++],App.get_privacy(),false));
        settings.add(new Setting(names[i++],false,true));
        settings.add(new Setting(names[i++],App.get_feed(),false));
        settings.add(new Setting(names[i++],App.get_rating(),false));
        settings.add(new Setting(names[i++],App.get_comment(),false));
        settings.add(new Setting(names[i++],false,true));
        settings.add(new Setting(names[i++],false,false));

        final RecyclerView list = (RecyclerView)findViewById(R.id.list);
        LinearLayoutManager llm2 = new LinearLayoutManager(this);
        llm2.setOrientation(LinearLayoutManager.VERTICAL);
        list.setLayoutManager(llm2);
        list.addItemDecoration(new DividerItemDecorator(this,DividerItemDecorator.VERTICAL_LIST));

        final Settings_Adapter settings_adapter = new Settings_Adapter(this,settings);
        list.setAdapter(settings_adapter);
        settings_adapter.setOnCardClickListener(new OnCardClickListener() {
            @Override
            public void onClick(View v, int position) {

            }
        });


    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
