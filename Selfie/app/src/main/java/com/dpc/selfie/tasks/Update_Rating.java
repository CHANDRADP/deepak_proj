package com.dpc.selfie.tasks;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.dpc.selfie.Act.Activity_Feeds;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.Server_Params;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by D-P-C on 06-Mar-15.
 */
public class Update_Rating extends AsyncTask<String, Long, Boolean> {
    private Context c;
    private int position = 0;
    private long image_id;
    private int rating;

    public Update_Rating(Context c,int position,long image_id, int rating) {
        this.c = c;
        this.position = position;
        this.image_id = image_id;
        this.rating = rating;
    }


    @Override
    protected Boolean doInBackground(String... params) {

        return update_rating();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if(result)
        {
            Intent intent = new Intent(Activity_Feeds.POST_STATE);
            intent.putExtra("position",position);
            intent.putExtra(Params.RATING.get_value(),rating);
            intent.putExtra(Params.IMAGE_ID.get_value(),image_id);
            c.sendBroadcast(intent);
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    public boolean update_rating() {
        boolean success = false;
        JSONParser parser = new JSONParser();
        JSONObject response = null;
        try {

            response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                    new Server_Params().update_rating(image_id, rating));
            if (response != null) {
                Log.d("response", response.toString());
                if (response.has(Params.STATUS.get_value())) {
                    int status = response.getInt(Params.STATUS.get_value());
                    if (status == 200) {
                        rating = response.getJSONObject(Params.DATA.get_value()).getInt(Params.RATING.get_value());
                        success = true;

                    }
                }
            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return success;
    }

}