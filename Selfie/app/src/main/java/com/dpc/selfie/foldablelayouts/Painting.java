package com.dpc.selfie.foldablelayouts;

import android.content.res.Resources;
import android.content.res.TypedArray;

import com.dpc.selfie.R;
import com.dpc.selfie.models.Feed;

import java.util.ArrayList;


public class Painting
{

    private final String image_url;
    private final String status;

    public static ArrayList<String> url_list = new ArrayList<>();
    public static ArrayList<String> status_list = new ArrayList<>();

    private Painting(String image_url, String status)
    {
        this.image_url = image_url;
        this.status = status;
    }

    public String getImageUrl() {
        return image_url;
    }

    public String getStatus() {
        return status;
    }

    public static Painting[] getAllPaintings(Resources res, ArrayList<Feed> feeds) {


        String comment = null, image_url = null, rating = null, status = null, time = null, user_comment = null, user_rating = null;

        for(int p=0;p<=feeds.size()-1;p++)
        {
            Feed feed = feeds.get(p);
            image_url = feed.getImage_url();
            status = feed.getStatus();
            url_list.add(image_url);
            status_list.add(status);
        }
        String[] string_urls = url_list.toArray(new String[url_list.size()]);
        String[] string_status = status_list.toArray(new String[status_list.size()]);
        //String[] years = res.getStringArray(R.array.paintings_years);
       // String[] locations = res.getStringArray(R.array.paintings_locations);
       // TypedArray images = res.obtainTypedArray(R.array.paintings_images);

        int size = url_list.size();
        Painting[] paintings = new Painting[size];

        for (int i = 0; i < size; i++) {
            paintings[i] = new Painting(string_urls[i], string_status[i]);
        }

        return paintings;
    }

}
