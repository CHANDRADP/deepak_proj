package com.dpc.selfie.models;

/**
 * Created by D-P-C on 24-Mar-15.
 */
public class DeletePositionImageId {
    private int position = 0, imageId = 0;

    public DeletePositionImageId(int position, int imageId) {
        this.position = position;
        this.imageId = imageId;

    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
