package com.dpc.selfie.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.Server_Params;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by D-P-C on 06-Mar-15.
 */
public class Update_comment  extends AsyncTask<String, Long, Long> {
    private Context c;
    private long image_id;
    private String comment = null;

    public Update_comment(Context c,long image_id, String comment) {
        this.c = c;
        this.image_id = image_id;
       this.comment = comment;
    }


    @Override
    protected Long doInBackground(String... params) {
        long comment_id = update_comment();

        return comment_id;
    }


    protected void onPostExecute(Boolean result) {

    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    public long update_comment() {
        boolean success = false;
        long comment_id = 0;
        JSONParser parser = new JSONParser();
        JSONObject response = null;
        try {

            response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                    new Server_Params().add_comment(image_id, comment));
            if (response != null) {
                Log.d("response", response.toString());
                if (response.has(Params.STATUS.get_value())) {
                    int status = response.getInt(Params.STATUS.get_value());
                    JSONObject data = response.getJSONObject(Params.DATA.get_value());
                    comment_id = data.getInt(Params.ID.get_value());
                    if (status == 200) {
                        success = true;

                    }
                }
            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return comment_id;
    }

}