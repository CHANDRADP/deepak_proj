package com.dpc.selfie.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.dpc.selfie.App;
import com.dpc.selfie.Utils.C;

import java.util.ArrayList;

/**
 * Created by D-P-C on 19-Feb-15.
 */
public class Feed {
    private long user_id = -1,selfie_id = -1;
    private String image_url = null,status = null,time = null;
    private int my_rating = 0,rating = 0,rating_count = 0,comment_count = 0;
    private ArrayList<Tag> tags;
    private ArrayList<Rating> users_rating;
    private ArrayList<Comment> users_comment;
    private boolean is_brand = false;


    public Feed(long user_id, long selfie_id, String image_url, String status, String time, int my_rating,int rating,ArrayList<Tag> tags,ArrayList<Rating> users_rating, ArrayList<Comment> users_comment,boolean is_brand) {
        this.user_id = user_id;
        this.selfie_id = selfie_id;
        this.image_url = image_url;
        this.status = status;
        this.time = time;
        this.my_rating = my_rating;
        this.rating = rating;
        this.tags = tags;
        this.users_rating = users_rating;
        this.users_comment = users_comment;

        if(users_rating != null)
            rating_count = users_rating.size();

        if(users_comment != null)
            comment_count = users_comment.size();

        this.is_brand = is_brand;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public long getSelfie_id() {
        return selfie_id;
    }

    public void setSelfie_id(long selfie_id) {
        this.selfie_id = selfie_id;
    }

    public String getImage_url() {
        return image_url;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getRating_count() {
        if(users_rating != null)
        {
            this.rating_count = users_rating.size();
        }
       return this.rating_count;
    }

    public void setRating_count(int rating_count) {
        this.rating_count = rating_count;
    }

    public int getMy_rating() {
        return my_rating;
    }

    public void setMy_rating(int my_rating) {
        if(this.my_rating == 0)
        {
            if(this.users_rating == null)
            {
                this.users_rating = new ArrayList<>();

            }

            this.users_rating.add(new Rating(App.getMyId(),null,my_rating, C.get_UTC_date()));

        }
        else
        for(int i = 0;i < users_rating.size();i++)
        {
            if(App.getMyId() == users_rating.get(i).getUser_id())
            {
                users_rating.get(i).setRating(my_rating);
                break;
            }
        }
        this.my_rating = my_rating;

    }

    public int getComment_count() {
        if(users_comment != null) {
            comment_count = users_comment.size();
        }

       return comment_count;
    }

    public void setComment_count(int comment_count) {
        this.comment_count = comment_count;
    }

    public ArrayList<Tag> getTags() {
        return tags;
    }

    public void setTags(ArrayList<Tag> tags) {
        this.tags = tags;
    }

    public ArrayList<Rating> getUsers_rating() {
        if(users_rating == null)
            users_rating = new ArrayList<>();
        return users_rating;
    }

    public void setUsers_rating(ArrayList<Rating> users_rating) {
        this.users_rating = users_rating;

    }

    public ArrayList<Comment> getUsers_comment() {
        if(users_comment == null)
            users_comment = new ArrayList<>();
        return users_comment;
    }

    public void setUsers_comment(ArrayList<Comment> users_comment) {
        this.users_comment = users_comment;

    }

    public void add_comment(Comment comment)
    {
        if(this.users_comment == null)
        {
            this.users_comment = new ArrayList<>();
        }
        this.users_comment.add(comment);
    }

    public boolean isIs_brand() {
        return is_brand;
    }

    public void setIs_brand(boolean is_brand) {
        this.is_brand = is_brand;
    }



}
