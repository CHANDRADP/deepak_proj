package com.dpc.selfie.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.DB;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.customs.CircleImage;
import com.dpc.selfie.customs.MyProgress;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.customs.TextB;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.Celebrity;
import com.dpc.selfie.models.Server_Params;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by D-P-C on 26-Mar-15.
 */
public class Celebrity_Adapter extends BaseAdapter {
    private LayoutInflater inflater;
    private ViewHolder holder;
    private View v;
    private ArrayList<Celebrity> celebrities = new ArrayList<>();
    private Activity c;
   private class ViewHolder {
        TextB name;
        Text status;
        CircleImage image;
        ImageView invite;

    }
    public Celebrity_Adapter(Activity c,ArrayList<Celebrity> l)
    {
        this.c = c;
        this.celebrities = l;


    }



    public void update_list(ArrayList<Celebrity> l)
    {
        this.celebrities = l;
        notifyDataSetChanged();
    }
    public View getView(final int position, View convertView, ViewGroup parent)
    {
       Celebrity celebrity = celebrities.get(position);

        v = convertView;
        if (v == null)
        {
            inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_user, null);
            holder = new ViewHolder();
            holder.name = (TextB) v.findViewById(R.id.name);
            holder.status = (Text) v.findViewById(R.id.status);
            holder.image = (CircleImage) v.findViewById(R.id.image);
            holder.invite = (ImageView) v.findViewById(R.id.invite);


            v.setTag(holder);

        }
        else
            holder = (ViewHolder) v.getTag();

        display_image(celebrity.getImage(), holder.image,true);


        holder.name.setText(celebrity.getF_name());
        String status = celebrity.getStatus();
        if(status == null)
            status = "No status";
        holder.status.setText(status);

        if(celebrity.isIs_following())
        {
            holder.invite.setBackgroundResource(R.drawable.follow_back_grey);
        }
        else
        {
            holder.invite.setBackgroundResource(R.drawable.follow_back_blue);
        }

        holder.invite.setVisibility(View.VISIBLE);

        holder.invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                C.alert(c,"clicked");
                boolean follow = true;
                if(celebrities.get(position).isIs_following())
                {
                    follow = false;
                }

                new Update_Follow(position,follow).execute();
            }
        });


        return v;

    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return celebrities.size();
    }


    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return celebrities.get(position);
    }


    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    protected void display_image(String url,View v,boolean is_circle_image)
    {

        try {
            Picasso.with(c)
                    .load(url)
                    .placeholder(R.drawable.ic_action_person)
                    .error(R.drawable.ic_action_person)
                    .into(is_circle_image ? (CircleImage)v : (ImageView)(v));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
    ///////////////////get all celebrities
    public class Update_Follow  extends AsyncTask<String, Long, Boolean> {
        private int position;
        private boolean add_follow = false;

        private MyProgress myProgress;
        public Update_Follow(int position,boolean add_follow)
        {
            this.position = position;
            this.add_follow = add_follow;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            myProgress = new MyProgress(c);
            myProgress.setCancelable(false);
            if(add_follow)
            {
                myProgress.setTitle("Following...");
            }
            else
                myProgress.setTitle("Unfollowing...");
            myProgress.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {

            return update_follow();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result)
            {
                DB db = new DB(c);
                Celebrity celebrity = db.Get_Celebrity_Details(celebrities.get(position).getId());
                if(celebrity == null)
                    celebrity = celebrities.get(position);
                if(add_follow) {
                    celebrity.setIs_following(add_follow);
                    ArrayList<Celebrity> celebrities1 = new ArrayList<>();
                    celebrities1.add(celebrity);
                    db.Sync_celebrities(celebrities1);
                }
                else
                {
                    db.remove_follow(celebrity.getId());
                }

                celebrities.get(position).setIs_following(add_follow);
                notifyDataSetChanged();
            }
            if(myProgress != null)
                myProgress.dismiss();

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        public boolean update_follow()
        {
            boolean success = false;
            JSONParser parser = new JSONParser();
            JSONObject response = null;
            try {


                response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                        new Server_Params().update_follow(celebrities.get(position).getId(),add_follow));
                if(response != null)
                {
                    Log.d("response", response.toString());
                    if(response.has(Params.STATUS.get_value()))
                    {
                        int status = response.getInt(Params.STATUS.get_value());
                        if (status == 200)
                        {
                            if(response.getJSONObject(Params.DATA.get_value()).getLong(Params.ID.get_value()) > 0)
                            {
                                success = true;
                            }

                        }
                    }
                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return success;
        }

    }

}
