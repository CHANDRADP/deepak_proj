package com.dpc.selfie.customs;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;
/**
 * Created by D-P-C on 16-Feb-15.
 */
public class Text extends TextView{

    public Text(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public Text(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Text(Context context) {
        super(context);
        init();
    }

    private void init() {
        set_bold(false);
    }

    public void set_bold(boolean bold)
    {
        Typeface tf;
        if(bold)
            tf = Typeface.createFromAsset(getContext().getAssets(), "font_four.otf");
        else
            tf = Typeface.createFromAsset(getContext().getAssets(), "font_four.otf");
        if(tf != null)
        setTypeface(tf);

    }

	

}
