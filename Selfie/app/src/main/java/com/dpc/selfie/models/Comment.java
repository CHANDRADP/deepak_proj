package com.dpc.selfie.models;

/**
 * Created by D-P-C on 03-Mar-15.
 */
public class Comment {

    private long user_id;
    private String u_name,user_image,comment = null,date = null;
    private long comment_id = 0;

    public Comment(long user_id,String u_name,String user_image, String comment, String date, long comment_id) {
        this.user_id = user_id;
        this.u_name = u_name;
        this.user_image = user_image;
        this.comment = comment;
        this.date = date;
        this.comment_id = comment_id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getU_name() {
        return u_name;
    }

    public void setU_name(String u_name) {
        this.u_name = u_name;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getComment_id() {
        return comment_id;
    }

    public void setComment_id(long comment_id) {
        this.comment_id = comment_id;
    }
}
