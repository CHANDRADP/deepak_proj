package com.dpc.selfie.Act;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dpc.selfie.R;
import com.dpc.selfie.adapters.MyBrands_Adapter;
import com.dpc.selfie.customs.BaseActivity;
import com.dpc.selfie.models.BrandOffers;
import com.dpc.selfie.models.MyBrands;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

public class Activity_MyBrands extends BaseActivity {

    private ArrayList<MyBrands> brands = new ArrayList<>();
    ListView brandsListView;
    private Menu appmenu;
    MyBrands item;
    int redeem_count = 0;
    TextView txtPoints;
    int myPoints = 30;
    int selected_brand = 0;
    ImageLoader imageLoader = ImageLoader.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity__my_brands);
        brandsListView = (ListView) findViewById(R.id.brandsListView);
        brands = db.get_all_brand_details();
        brandsListView.setAdapter(new MyBrands_Adapter(Activity_MyBrands.this,brands,imageLoader));
        setbrands_list_listener();

        //updateMenuTitles(Integer.toString(myPoints));
    }

    private void setbrands_list_listener()
    {

        if(brandsListView != null)
        {
            brandsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    final Dialog dialog = new Dialog(Activity_MyBrands.this);

                    //setting custom layout to dialog
                    item = brands.get(position);
                    dialog.setContentView(R.layout.custom_brands_dialog);
                    dialog.setTitle("Select Offer");
                    ArrayList<BrandOffers> offers = new ArrayList<>();
                    offers = db.get_all_offers();
                    ListView offersListView = (ListView) dialog.findViewById(R.id.offersListView);
                    txtPoints = (TextView) dialog.findViewById(R.id.txtPoints);
                    txtPoints.setText(Integer.toString(myPoints));
                    selected_brand = position;
                    offersListView.setAdapter(new BrandOffers_Adapter(Activity_MyBrands.this,offers ));
                    dialog.show();
                }
            });
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity__my_brands, menu);
        appmenu = menu;
        MenuItem menuitem = menu.findItem(R.id.appPoints);
        menuitem.setTitle(Integer.toString(myPoints));

        return true;
    }

    private void updateMenuTitles(String title) {

        MenuItem mymenu = appmenu.findItem(R.id.appPoints);
            mymenu.setTitle(title);



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                super.onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    protected void onResume() {
        super.onResume();


    }

    public class BrandOffers_Adapter extends BaseAdapter {
        private LayoutInflater inflater;
        private ViewHolder holder;
        private View v;
        private List<BrandOffers> items = new ArrayList<>();
        private Context c;
        private class ViewHolder {
            TextView txtOfferName;
            Button btnRedeem;
        }



        public BrandOffers_Adapter(Context c,List<BrandOffers> l) {
            this.c = c;
            this.items = l;
        }


        public void update_list(List<BrandOffers> l)
        {
            this.items = l;
            notifyDataSetChanged();
        }
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            final BrandOffers brand = items.get(position);

            v = convertView;
            if (v == null)
            {
                inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.item_offer, null);
                holder = new ViewHolder();
                holder.txtOfferName = (TextView) v.findViewById(R.id.txtOfferName);
                holder.btnRedeem = (Button) v.findViewById(R.id.btnRedeem);
                v.setTag(holder);

            }
            else
                holder = (ViewHolder) v.getTag();

            holder.txtOfferName.setText(brand.getOffer_name());
            holder.btnRedeem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    redeem_count = redeem_count + 1;
                    if(myPoints >= brand.getAmount())
                    {
                        int balance = myPoints - brand.getAmount();
                        myPoints = balance;
                        txtPoints.setText(Integer.toString(balance));
                        updateMenuTitles(Integer.toString(myPoints));
                        items.remove(position);
                        notifyDataSetChanged();
                        MyBrands item = brands.get(selected_brand);
                        item.setNo_of_offers(item.getNo_of_offers() - redeem_count);
                        brandsListView.setAdapter(new MyBrands_Adapter(Activity_MyBrands.this,brands,imageLoader));
                        redeem_count = 0;
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "You don't have sufficient balance to reedem the offer.", Toast.LENGTH_LONG).show();
                    }


                }
            });

            return v;

        }


        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return items.size();
        }


        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return items.get(position);
        }


        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

    }

}
