/**
 * PhotoSorterView.java
 *
 * (c) Luke Hutchison (luke.hutch@mit.edu)
 *
 * TODO: Add OpenGL acceleration.
 *
 * Released under the Apache License v2.
 */
package com.dpc.selfie.CustomCollage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;


public class TextPhotoSortrView extends View implements View.OnLongClickListener,
        MultiTouchController.MultiTouchObjectCanvas<MultiTouchEntity> {

    // private static final int[] IMAGES = { R.drawable.m74hubble };
    private Path drawPath;
    // drawing and canvas paint
    private Paint drawPaint, canvasPaint;
    // initial color
    private int paintColor = Color.TRANSPARENT;
    // canvas
    private Canvas drawCanvas;
    // canvas bitmap
    private Bitmap canvasBitmap;

    boolean long_clicked = false;

    public static int CANVAS_HEIGHT = 0;
    public static int CANVAS_WIDTH = 0;
    public static float DELETE_REGION = 0;
    public static float ZOOMED_IMAGE_CENTERY = 0;
    public static float ZOOMED_IMAGE_CENTERX = 0;

    float cx;
    float cy;
    public static final String BROADCAST_ACTION = "com.dpc.selfie.CustomCollage";

    Activity mycontext;
    boolean isdynamic = false;

    boolean select_active = false;

    // public PhotoSortrView(Context context, AttributeSet attrs) {
    // super(context, attrs);

    // }
    private ArrayList<MultiTouchEntity> imageIDs = new ArrayList<MultiTouchEntity>();

    // --

    private MultiTouchController<MultiTouchEntity> multiTouchController = new MultiTouchController<MultiTouchEntity>(
            this);

    // --

    private MultiTouchController.PointInfo currTouchPoint = new MultiTouchController.PointInfo();

    private boolean mShowDebugInfo = true;

    private static final int UI_MODE_ROTATE = 1, UI_MODE_ANISOTROPIC_SCALE = 2;

    private int mUIMode = UI_MODE_ROTATE;

    // --

    private static final float SCREEN_MARGIN = 100;

    private int width, height, displayWidth, displayHeight;

    // ---------------------------------------------------------------------------------------------------

    public TextPhotoSortrView(Context context) {
        this(context, null);

    }

    public TextPhotoSortrView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        setupDrawing();
    }

    public TextPhotoSortrView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);

        // loadImages(context);
    }

    private void init(Context context) {
        Resources res = context.getResources();
        setBackgroundColor(Color.TRANSPARENT);

        DisplayMetrics metrics = res.getDisplayMetrics();
        this.displayWidth = res.getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ? Math
                .max(metrics.widthPixels, metrics.heightPixels) : Math.min(
                metrics.widthPixels, metrics.heightPixels);
        this.displayHeight = res.getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ? Math
                .min(metrics.widthPixels, metrics.heightPixels) : Math.max(
                metrics.widthPixels, metrics.heightPixels);
    }

    /** Called by activity's onResume() method to load the images */
    public void addImages(Activity context, Drawable resourceId, boolean long_clicked, float cx, float cy, float imageWidth, float imageHeight, int tag, boolean dynamic) {
        this.mycontext = context;
        this.isdynamic = dynamic;
        Resources res = context.getResources();
        imageIDs.add(new ImageEntity(context, resourceId, res, long_clicked, imageWidth, imageHeight, 0.0f));

        imageIDs.get(imageIDs.size() - 1).load(context, cx, cy, tag, isdynamic,true);
        long_clicked = false;
        invalidate();
    }

    public float getInsideCoordinatesX()
    {
        double myRandomNo = Math.random();
        cx = getWidth() / 2 + (float) (myRandomNo* (displayWidth - 2 * SCREEN_MARGIN));
        cy = getHeight() / 2 + (float) (myRandomNo * (displayHeight - 2 * SCREEN_MARGIN));
        return cx;
    }

    public int getTotalImages()
    {
        return imageIDs.size();
    }



    public void removeAllImages() {

        imageIDs.removeAll(imageIDs);
        invalidate();
    }

    /**
     * Called by activity's onPause() method to free memory used for loading the
     * images
     */
    public void removeImage() {
        // Resources res = context.getResources();
        // int n = imageIDs.size();
        // for (int i = 0; i < n; i++)
        if (imageIDs.size() > 0) {
            imageIDs.remove(imageIDs.size() - 1);
        }

        invalidate();
    }

    // ---------------------------------------------------------------------------------------------------

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        boolean selected = false;
        canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);
        canvas.drawPath(drawPath, drawPaint);
       // canvas.drawARGB(255, 41,128,185);
        canvas.drawARGB(255, 255,255,255);
        CANVAS_HEIGHT = this.getHeight();
        CANVAS_WIDTH = this.getWidth();
        int delete_height = CANVAS_HEIGHT/10;

        DELETE_REGION = CANVAS_HEIGHT - (delete_height);
        int n = imageIDs.size();
        for (int i = 0; i < n; i++)
        {

            if(i== n-1)
            {
                selected = true;
            }
            imageIDs.get(i).draw(canvas, selected, canvasBitmap);

        }


    }

    // ---------------------------------------------------------------------------------------------------

    public void trackballClicked() {
        mUIMode = (mUIMode + 1) % 3;
        invalidate();
    }

    /** Pass touch events to the MT controller */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float touchX = event.getX();
        float touchY = event.getY();
        // respond to down, move and up events
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                drawPath.moveTo(touchX, touchY);
                break;
            case MotionEvent.ACTION_MOVE:
                drawPath.lineTo(touchX, touchY);
                // Log.d("Touch coordinates: ",Float.toString(touchX)+" "+Float.toString(touchY));
                break;
            case MotionEvent.ACTION_UP:
                drawPath.lineTo(touchX, touchY);
                drawCanvas.drawPath(drawPath, drawPaint);
                drawPath.reset();
                break;
            default:
                return false;
        }
        // redraw
        invalidate();
        // return true;

        return multiTouchController.onTouchEvent(event, mycontext, isdynamic);
    }

    /**
     * Get the image that is under the single-touch point, or return null
     * (canceling the drag op) if none
     */
    public MultiTouchEntity getDraggableObjectAtPoint(MultiTouchController.PointInfo pt) {
        float x = pt.getX(), y = pt.getY();
        int n = imageIDs.size();
        for (int i = n - 1; i >= 0; i--) {
            ImageEntity im = (ImageEntity) imageIDs.get(i);
            if (im.containsPoint(x, y)) {

                return im;
            }
        }
        return null;
    }

    /**
     * Select an object for dragging. Called whenever an object is found to be
     * under the point (non-null is returned by getDraggableObjectAtPoint()) and
     * a drag operation is starting. Called with null when drag op ends.
     */
    public void selectObject(MultiTouchEntity img, MultiTouchController.PointInfo touchPoint, boolean swapflag) {
        currTouchPoint.set(touchPoint);



        if(swapflag || isdynamic)
        {
            if (img != null) {
                // Move image to the top of the stack when selected
                drawPaint.setColor(Color.TRANSPARENT);
                Log.d("active, active",Boolean.toString(swapflag));
                imageIDs.remove(img);
                imageIDs.add(img);


            } else {
                // Called with img == null when drag stops.
            }

            invalidate();


        }

    }



    @Override
    public void selectObjectLongClick(MultiTouchEntity obj, MultiTouchController.PointInfo touchPoint, boolean released) {

        Intent intent = new Intent(BROADCAST_ACTION);
        intent.putExtra("released",released);
        if(obj!=null)
        {
            intent.putExtra("tag",obj.getTag());
        }

        mycontext.sendBroadcast(intent);
    }

    @Override
    public void swapImages(int long_clicked_location, int swap_location) {

        if(!isdynamic)
        {
            Intent intent = new Intent(BROADCAST_ACTION);
            intent.putExtra("swap",true);
            intent.putExtra("index1", long_clicked_location);
            intent.putExtra("index2", swap_location);
            mycontext.sendBroadcast(intent);
        }
    }

    @Override
    public void deleteImage() {
        Log.d("Delete Image Called", Integer.toString(imageIDs.size()));
        if(imageIDs.size()>0)
        {
            imageIDs.remove(imageIDs.size()-1);
        }
        invalidate();
    }

    @Override
    public void changeDeleteAreaColor(String color) {
        Intent intent = new Intent(BROADCAST_ACTION);
        intent.putExtra("changeColor",true);
        intent.putExtra("color", color);
        mycontext.sendBroadcast(intent);
    }

    @Override
    public void changeTouchableState() {

    }


    /*@Override
    public void deleteImage() {
        Log.d("Delete Image Called", Integer.toString(imageIDs.size()));
         if(imageIDs.size()>0)
         {
             imageIDs.remove(imageIDs.size()-1);
         }
        invalidate();
    }*/


    /**
     * Get the current position and scale of the selected image. Called whenever
     * a drag starts or is reset.
     */
    public void getPositionAndScale(MultiTouchEntity img,
                                    MultiTouchController.PositionAndScale objPosAndScaleOut) {
        // FIXME affine-izem (and fix the fact that the anisotropic_scale part
        // requires averaging the two scale factors)
        objPosAndScaleOut.set(img.getCenterX(), img.getCenterY(),
                (mUIMode & UI_MODE_ANISOTROPIC_SCALE) == 0,
                (img.getScaleX() + img.getScaleY()) / 2,
                (mUIMode & UI_MODE_ANISOTROPIC_SCALE) != 0, img.getScaleX(),
                img.getScaleY(), (mUIMode & UI_MODE_ROTATE) != 0,
                img.getAngle());
    }

    /** Set the position and scale of the dragged/stretched image. */
    public boolean setPositionAndScale(MultiTouchEntity img,
                                       MultiTouchController.PositionAndScale newImgPosAndScale, MultiTouchController.PointInfo touchPoint) {
        currTouchPoint.set(touchPoint);
        boolean ok = ((ImageEntity) img).setPos(newImgPosAndScale);
        if (ok)
            invalidate();
        return ok;
    }

    public boolean pointInObjectGrabArea(MultiTouchController.PointInfo pt, MultiTouchEntity img, float scale) {
        //Log.d("Image height", Integer.toString(img.getHeight()));
        //Log.d("scale", Float.toString(scale));
        //Log.d("Get Center  X", Float.toString(img.getCenterX()));
        //Log.d("Get Center  Y", Float.toString(img.getCenterY()));
        //Log.d("Get Min  X", Float.toString(img.getMinX()));
        //Log.d("Get Min  Y", Float.toString(img.getMinY()));
        // Log.d("Get Max  X", Float.toString(img.getMaxX()));
        //Log.d("Get Max  Y", Float.toString(img.getMaxY()));
        ZOOMED_IMAGE_CENTERY = (img.getMinY()+img.getMaxY())/2;
        ZOOMED_IMAGE_CENTERX = (img.getMinX()+img.getMaxX())/2;

        //Log.d("ZoomedImageCenter", Float.toString(ZOOMED_IMAGE_CENTER));
        // Log.d("DeleteRegion", Float.toString(DELETE_REGION));

        return false;
    }

    private void setupDrawing() {

        // prepare for drawing and setup paint stroke properties
        drawPath = new Path();
        drawPaint = new Paint();
        drawPaint.setColor(paintColor);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(10);
        drawPaint.setStyle(Paint.Style.STROKE);
        // drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);
        canvasPaint = new Paint(Paint.DITHER_FLAG);

    }

    // size assigned to view
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (w > 0 && h > 0) {

            super.onSizeChanged(w, h, oldw, oldh);
            canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            drawCanvas = new Canvas(canvasBitmap);
        }
    }

    // draw the view - will be called after touch event

    // register user touches as drawing action

    // update color
    public void setColor(String newColor) {
        invalidate();
        paintColor = Color.parseColor(newColor);
        drawPaint.setColor(paintColor);
    }

    public void clearCanvas() {
        drawCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        invalidate();

    }
    public void clearCanvasAllImages() {
        imageIDs.clear();
        invalidate();

    }

    public void setTranspertColor() {
        drawPaint.setColor(Color.TRANSPARENT);
    }

    @Override
    public boolean onLongClick(View view) {
        return false;
    }
}
