package com.dpc.selfie.adapters;

/**
 * Created by D-P-C on 18-Mar-15.
 */
import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.dpc.selfie.Act.Activity_Profile;
import com.dpc.selfie.R;
import com.dpc.selfie.customs.CircleImage;
import com.dpc.selfie.models.Profile_Item;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ImagePagerAdapter extends PagerAdapter{

    private Context context;

    private LayoutInflater inflater;


    public ImagePagerAdapter(Context c)
    {
        context = c;
       inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }



    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public void finishUpdate(View container) {
    }

    @Override
    public int getCount() {
        if(Activity_Profile.selfies == null)
            return 0;
        else
            return Activity_Profile.selfies.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        Profile_Item item = Activity_Profile.selfies.get(position);

       View v = inflater.inflate(R.layout.pager_item, view, false);
        final ImageView imageView = (ImageView) v.findViewById(R.id.image);
        final ProgressBar spinner = (ProgressBar) v.findViewById(R.id.loading);
        display_image(item.getImage_url(), imageView,false);


        ((ViewPager) view).addView(v, 0);
        return v;
    }


    @Override
    public boolean isViewFromObject(View view, Object o) {
        return o == view;
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public void startUpdate(View container) {
    }

    private void display_image(String url,View v,boolean is_circle_image)
    {

        try {
            Picasso.with(context)
                    .load(url)
                    .placeholder(R.drawable.ic_action_person)
                    .error(R.drawable.ic_action_person)
                    .into(is_circle_image ? (CircleImage)v : (ImageView)(v));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }


}