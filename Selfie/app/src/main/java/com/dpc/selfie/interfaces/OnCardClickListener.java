package com.dpc.selfie.interfaces;

import android.view.View;

/**
 * Created by D-P-C on 19-Feb-15.
 */
public interface OnCardClickListener {
    void onClick(View v, int position);
}
