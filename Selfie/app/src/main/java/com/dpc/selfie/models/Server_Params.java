package com.dpc.selfie.models;

import android.os.Build;
import android.util.Log;

import com.dpc.selfie.App;

import com.dpc.selfie.enums.Params;
import com.dpc.selfie.enums.Process;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 * Created by D-P-C on 02-Feb-15.
 */
public class Server_Params {

    ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

    public String get_version() {
        return Build.VERSION.RELEASE;
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public ArrayList<NameValuePair> login(User user,String gcm_id,String token) {

        params.add(new BasicNameValuePair(Process.ADD.get_value(), Process.ADD_USER.get_value()));

        params.add(new BasicNameValuePair(Params.FB_ID.get_value(), String.valueOf(user.getFb_id())));
        params.add(new BasicNameValuePair(Params.FIRST_NAME.get_value(), user.getF_name()));
        params.add(new BasicNameValuePair(Params.MIDDLE_NAME.get_value(), user.getM_name()));
        params.add(new BasicNameValuePair(Params.LAST_NAME.get_value(), user.getL_name()));
        params.add(new BasicNameValuePair(Params.DISPLAY_NAME.get_value(), user.getDisplay_name()));
        params.add(new BasicNameValuePair(Params.DOB.get_value(), user.getDob()));
        params.add(new BasicNameValuePair(Params.EMAIL.get_value(), user.getEmail()));
        params.add(new BasicNameValuePair(Params.GENDER.get_value(), String.valueOf(user.getGenderValue())));
        params.add(new BasicNameValuePair(Params.PUSH_ID.get_value(), gcm_id));
        params.add(new BasicNameValuePair(Params.PHONE_TYPE.get_value(), "1"));//for android
        params.add(new BasicNameValuePair(Params.PHONE_MODEL.get_value(), this.getDeviceName()+" : Android "+this.get_version()));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(),token));

        return params;
    }
    public ArrayList<NameValuePair> get_app_version_params(String version,String gcm_id)
    {

        params.add(new BasicNameValuePair(Process.UPDATE.get_value(),Process.UPDATE_USER.get_value()));
        params.add(new BasicNameValuePair(Params.USER_ID.get_value(),String.valueOf(App.getMyId())));
        params.add(new BasicNameValuePair(Params.APP_VERSION.get_value(),version));
        params.add(new BasicNameValuePair(Params.PUSH_ID.get_value(),gcm_id));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(),App.getToken()));


        return params;
    }

    public ArrayList<NameValuePair> add_u(User user, String token, String gcm_id, String c_code, String profession, String app_version)
    {
        params.add(new BasicNameValuePair("add", "add_u")); // method parameters
        params.add(new BasicNameValuePair(Params.FB_ID.get_value(), "123456789"));
        params.add(new BasicNameValuePair(Params.FIRST_NAME.get_value(), user.getF_name()));
        params.add(new BasicNameValuePair(Params.MIDDLE_NAME.get_value(), user.getM_name()));
        params.add(new BasicNameValuePair(Params.LAST_NAME.get_value(), user.getL_name()));
        params.add(new BasicNameValuePair(Params.DISPLAY_NAME.get_value(), user.getDisplay_name()));
        params.add(new BasicNameValuePair(Params.COUNTRY_CODE.get_value(), c_code));
        params.add(new BasicNameValuePair(Params.NUMBER.get_value(), user.getNumber()));
        params.add(new BasicNameValuePair(Params.EMAIL.get_value(), user.getEmail()));
        params.add(new BasicNameValuePair(Params.DOB.get_value(), user.getDob()));
        params.add(new BasicNameValuePair(Params.GENDER.get_value(), String.valueOf(user.getGenderValue())));
        params.add(new BasicNameValuePair(Params.STATUS.get_value(), user.getStatus()));
        params.add(new BasicNameValuePair(Params.PUSH_ID.get_value(), gcm_id));
        params.add(new BasicNameValuePair(Params.PHONE_TYPE.get_value(), "1"));//for android
        params.add(new BasicNameValuePair(Params.PHONE_MODEL.get_value(), this.getDeviceName()+" : Android "+this.get_version()));
        params.add(new BasicNameValuePair(Params.APP_VERSION.get_value(),app_version));
        params.add(new BasicNameValuePair(Params.CELEBRITY.get_value(),"1"));// " 1 "- celebrity, " -1 " - user
        params.add(new BasicNameValuePair(Params.PROFESSION.get_value(),profession));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(),token));

        return params;
    }


    public ArrayList<NameValuePair> update_c(String comment, long comment_id)
    {
        params.add(new BasicNameValuePair(Process.UPDATE.get_value(), Process.UPDATE_COMMENT.get_value())); // method parameters
        params.add(new BasicNameValuePair(Params.USER_ID.get_value(), String.valueOf(App.getMyId())));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(),App.getToken()));
        params.add(new BasicNameValuePair(Params.ID.get_value(), String.valueOf(comment_id)));
        params.add(new BasicNameValuePair(Params.COMMENT.get_value(), comment));
        return params;
    }

    public ArrayList<NameValuePair> update_users(ArrayList<Long> user_ids,boolean is_fb) {

        String ids = "";
        if(is_fb)
        params.add(new BasicNameValuePair(Process.GET.get_value(), Process.GET_ALL_FB_USERS.get_value()));
        else
            params.add(new BasicNameValuePair(Process.GET.get_value(), Process.GET_ALL_USERS.get_value()));

        ids = String.valueOf(user_ids.get(0));
        for(int i = 1; i < user_ids.size();i++) {
         ids += ","+String.valueOf(user_ids.get(i));
        }
        if(is_fb)
        params.add(new BasicNameValuePair(Params.ID.get_value(), ids));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(), App.getToken()));
        params.add(new BasicNameValuePair(Params.USER_ID.get_value(), String.valueOf(App.getMyId())));

        return params;
    }

    public ArrayList<NameValuePair> update_user_details(User user) {


        params.add(new BasicNameValuePair(Params.TOKEN.get_value(), App.getToken()));
        params.add(new BasicNameValuePair(Params.USER_ID.get_value(), String.valueOf(App.getMyId())));

        if(user.getImage() != null)
            params.add(new BasicNameValuePair(Params.IMAGE.get_value(), user.getImage()));
        if(user.getThumbnail() != null)
            params.add(new BasicNameValuePair(Params.THUMBNAIL.get_value(), user.getThumbnail()));
        if(user.getBadges() != null)
            params.add(new BasicNameValuePair(Params.BADGES.get_value(), user.getBadges()));
        if(user.getPoints() == 0)
            params.add(new BasicNameValuePair(Params.POINTS.get_value(), String.valueOf(user.getPoints())));
        if(user.getStatus() != null)
            params.add(new BasicNameValuePair(Params.STATUS.get_value(), user.getStatus()));


        return params;
    }

    public ArrayList<NameValuePair> post_selfie(String selfie_url,String selfie_status,int brand_id) {

        params.add(new BasicNameValuePair(Process.ADD.get_value(), Process.ADD_IMAGE.get_value()));
        params.add(new BasicNameValuePair(Params.STATUS.get_value(), selfie_status));
        params.add(new BasicNameValuePair(Params.IMAGE.get_value(), selfie_url));
        if(brand_id > 0)
            params.add(new BasicNameValuePair(Params.BRAND_ID.get_value(), String.valueOf(brand_id)));
        params.add(new BasicNameValuePair(Params.USER_ID.get_value(), String.valueOf(App.getMyId())));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(), App.getToken()));

        return params;
    }

    public ArrayList<NameValuePair> update_status(String status) {

        params.add(new BasicNameValuePair(Process.UPDATE.get_value(), Process.UPDATE_USER.get_value()));
        params.add(new BasicNameValuePair(Params.ID.get_value(), String.valueOf(App.getMyId())));
        params.add(new BasicNameValuePair(Params.STATUS.get_value(), status));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(), App.getToken()));

        return params;
    }

    public ArrayList<NameValuePair> update_rating(long image_id,int rating) {

        params.add(new BasicNameValuePair(Process.UPDATE.get_value(), Process.UPDATE_RATING.get_value()));
        params.add(new BasicNameValuePair(Params.RATING.get_value(), String.valueOf(rating)));
        params.add(new BasicNameValuePair(Params.IMAGE_ID.get_value(), String.valueOf(image_id)));
        params.add(new BasicNameValuePair(Params.USER_ID.get_value(), String.valueOf(App.getMyId())));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(), App.getToken()));

        return params;
    }

    public ArrayList<NameValuePair> add_comment(long image_id,String comment) {

        params.add(new BasicNameValuePair(Process.ADD.get_value(), Process.ADD_COMMENT.get_value()));
        params.add(new BasicNameValuePair(Params.COMMENT.get_value(), comment));
        params.add(new BasicNameValuePair(Params.IMAGE_ID.get_value(), String.valueOf(image_id)));
        params.add(new BasicNameValuePair(Params.USER_ID.get_value(), String.valueOf(App.getMyId())));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(), App.getToken()));

        return params;
    }

    public ArrayList<NameValuePair> get_selfies(long user_id) {

        params.add(new BasicNameValuePair(Process.GET.get_value(), Process.GET_USER_TIMELINE.get_value()));
        params.add(new BasicNameValuePair(Params.USER_ID.get_value(), String.valueOf(App.getMyId())));
        params.add(new BasicNameValuePair(Params.ID.get_value(), String.valueOf(user_id)));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(), App.getToken()));

        return params;
    }



    public ArrayList<NameValuePair> get_feeds(long page) {

        params.add(new BasicNameValuePair(Process.GET.get_value(), Process.GET_ALL_FEEDS.get_value()));
        params.add(new BasicNameValuePair(Params.USER_ID.get_value(), String.valueOf(App.getMyId())));
        params.add(new BasicNameValuePair(Params.PAGE.get_value(), String.valueOf(page)));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(), App.getToken()));

        return params;
    }

    public ArrayList<NameValuePair> get_searched_user(String search_text) {

        params.add(new BasicNameValuePair(Process.GET.get_value(), Process.GET_SEARCHED_USER.get_value()));
        params.add(new BasicNameValuePair(Params.USER_ID.get_value(), String.valueOf(App.getMyId())));
        params.add(new BasicNameValuePair(Params.SEARCH_KEYWORD.get_value(), search_text));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(), App.getToken()));

        return params;
    }

    public ArrayList<NameValuePair> get_brand_category() {

        params.add(new BasicNameValuePair(Process.GET.get_value(), Process.GET_BRAND_CATEGORY.get_value()));
        params.add(new BasicNameValuePair(Params.USER_ID.get_value(), String.valueOf(App.getMyId())));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(), App.getToken()));

        return params;
    }

    public ArrayList<NameValuePair> get_offers_by_categoryId(long category_id) {

        params.add(new BasicNameValuePair(Process.GET.get_value(), Process.GET_BRAND_OFFERS.get_value()));
        params.add(new BasicNameValuePair(Params.USER_ID.get_value(), String.valueOf(App.getMyId())));
        params.add(new BasicNameValuePair(Params.CATEGORY_ID.get_value(), String.valueOf(category_id)));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(), App.getToken()));

        return params;
    }

    public ArrayList<NameValuePair> get_brand_frames(long brand_id) {

        params.add(new BasicNameValuePair(Process.GET.get_value(), Process.GET_BRAND_FRAMES.get_value()));
        params.add(new BasicNameValuePair(Params.USER_ID.get_value(), String.valueOf(App.getMyId())));
        params.add(new BasicNameValuePair(Params.BRAND_ID.get_value(), String.valueOf(brand_id)));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(), App.getToken()));

        return params;
    }


    public ArrayList<NameValuePair> get_brand_feeds(long page,long brand_id) {

        params.add(new BasicNameValuePair(Process.GET.get_value(), Process.GET_BRAND_FEEDS.get_value()));
        params.add(new BasicNameValuePair(Params.USER_ID.get_value(), String.valueOf(App.getMyId())));
        params.add(new BasicNameValuePair(Params.BRAND_ID.get_value(), String.valueOf(brand_id)));
        params.add(new BasicNameValuePair(Params.PAGE.get_value(), String.valueOf(page)));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(), App.getToken()));

        return params;
    }

    public ArrayList<NameValuePair> get_celebrities() {

        params.add(new BasicNameValuePair(Process.GET.get_value(), Process.GET_CELEBRITIES.get_value()));
        params.add(new BasicNameValuePair(Params.USER_ID.get_value(), String.valueOf(App.getMyId())));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(), App.getToken()));

        return params;
    }

    public ArrayList<NameValuePair> update_follow(long cele_id,boolean following) {

        params.add(new BasicNameValuePair(Process.UPDATE.get_value(), Process.UPDATE_FOLLOW.get_value()));
        params.add(new BasicNameValuePair(Params.USER_ID.get_value(), String.valueOf(App.getMyId())));
        params.add(new BasicNameValuePair(Params.CELEBRITY_ID.get_value(), String.valueOf(cele_id)));
        params.add(new BasicNameValuePair(Params.FOLLOWING.get_value(), String.valueOf(following ? 1 : -1)));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(), App.getToken()));

        return params;
    }

    public ArrayList<NameValuePair> delete_selfie(long img_id) {

        params.add(new BasicNameValuePair(Process.DELETE.get_value(), Process.DELETE_IMAGE.get_value()));
        params.add(new BasicNameValuePair(Params.USER_ID.get_value(), String.valueOf(App.getMyId())));
        params.add(new BasicNameValuePair(Params.IMAGE_ID.get_value(), String.valueOf(img_id)));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(), App.getToken()));

        return params;
    }

    public ArrayList<NameValuePair> delete_c(long comment_id) {

        params.add(new BasicNameValuePair(Process.DELETE.get_value(), Process.DELETE_COMMENT.get_value()));
        params.add(new BasicNameValuePair(Params.USER_ID.get_value(), String.valueOf(App.getMyId())));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(), App.getToken()));
        params.add(new BasicNameValuePair(Params.ID.get_value(), String.valueOf(comment_id)));
        return params;
    }




    public ArrayList<NameValuePair> update_user_friends(ArrayList<Long> user_ids) {

        String ids = null;
        if(user_ids.size() > 0) {
            ids = user_ids.toArray().toString();
            ids = ids.substring(1, ids.length() - 2);
        }

        params.add(new BasicNameValuePair(Process.UPDATE.get_value(), Process.UPDATE_USER_FRIENDS.get_value()));
        params.add(new BasicNameValuePair(Params.USER_ID.get_value(), String.valueOf(App.getMyId())));
        if(ids != null)
        params.add(new BasicNameValuePair(Params.FRIENDS.get_value(), ids));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(), App.getToken()));

        return params;
    }

    public ArrayList<NameValuePair> get_selfie_info(long image_id) {

        params.add(new BasicNameValuePair(Process.GET.get_value(), Process.GET_SELFIE_INFO.get_value()));
        params.add(new BasicNameValuePair(Params.IMAGE_ID.get_value(), String.valueOf(image_id)));
        params.add(new BasicNameValuePair(Params.USER_ID.get_value(), String.valueOf(App.getMyId())));
        params.add(new BasicNameValuePair(Params.TOKEN.get_value(), App.getToken()));

        return params;
    }
}
