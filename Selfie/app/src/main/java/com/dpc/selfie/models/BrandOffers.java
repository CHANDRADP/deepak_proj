package com.dpc.selfie.models;

/**
 * Created by user on 4/11/2015.
 */
public class BrandOffers {
    private String offer_name = null;
    int amount = 0;


    public BrandOffers(String offer_name, int amount) {
        this.offer_name = offer_name;
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getOffer_name() {
        return offer_name;
    }

    public void setOffer_name(String offer_name) {
        this.offer_name = offer_name;
    }


}
