package com.dpc.selfie.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.ExifInterface;

import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.provider.MediaStore;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dpc.selfie.Act.Activity_Collage;
import com.dpc.selfie.App;
import com.dpc.selfie.R;
import com.dpc.selfie.customs.Camera_Preview;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.BrandCategory;
import com.dpc.selfie.models.BrandFrames;
import com.dpc.selfie.models.Brand_Offer_Details;
import com.dpc.selfie.models.Celebrity;
import com.dpc.selfie.models.SearchData;
import com.dpc.selfie.models.User;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

/**
 * Created by D-P-C on 30-Jan-15.
 */
public class C {
    //facebook constants
    public static final String PENDING_PUBLISH_KEY = "pending_publish_permissions";

    //Amazon account details
    public static final String S3_AK = "AKIAJ35OY3433F2MDGWA";
    public static final String S3_SK = "5LpNvQvsibrS9JT8cGMdNfmGrEssdG7g75FMwJLd";
    public static final String S3_BUCKET = "selfie-user-pics";

    //App constants
    public final static String SERVER_URL = "http://ec2-54-148-132-161.us-west-2.compute.amazonaws.com/selfie_test/S_API.php";//http://10.0.2.2/Selfie/S_API.php";
    //public final static String SERVER_URL = "http://192.168.1.3/selfie_test/S_API.php";
    public static final String SENDER_ID = "999488980802";

    public static final int FLIP_VERTICAL = 1;
    public static final int FLIP_HORIZONTAL = 2;


    //toast message
    public static void alert(Context cxt, String message) {
        if (message != null)
            Toast.makeText(cxt, message, Toast.LENGTH_SHORT).show();
    }

    public static String get_UTC_date() {
        final Date currentTime = new Date();
        SimpleDateFormat utc = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        utc.setTimeZone(TimeZone.getTimeZone("UTC"));
        return utc.format(currentTime);
    }

    //UTC to local time
    public static String UTC_to_Local(String time) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat df1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            if(time.contains("-"))
            date = df.parse(time);
            else if(time.contains("/"))
                date = df1.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        df.setTimeZone(TimeZone.getDefault());
        String formattedDate = df.format(date);
        return formattedDate;
    }

    //get time difference
    public static String getDateDifference(String date) {

        DateFormat sdf = null;
        if (date.contains("-")) {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        } else if (date.contains("/")) {
            sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        }

        Date thenDate = null;
        try {
            thenDate = sdf.parse(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Calendar now = Calendar.getInstance();
        Calendar then = Calendar.getInstance();
        now.setTime(new Date());
        then.setTime(thenDate);

        // Get the represented date in milliseconds
        long nowMs = now.getTimeInMillis();
        long thenMs = then.getTimeInMillis();

        // Calculate difference in milliseconds
        long diff = nowMs - thenMs;

        // Calculate difference in seconds

        long diffMinutes = diff / (60 * 1000);
        long diffHours = diff / (60 * 60 * 1000);
        long diffDays = diff / (24 * 60 * 60 * 1000);

        if (diffMinutes < 60) {
            if (diffMinutes == 1)
                return diffMinutes + " minute ago";
            else if (diffMinutes <= 0)
                return "Now";
            else
                return diffMinutes + " minute ago";
        } else if (diffHours < 24) {
            if (diffHours == 1)
                return diffHours + " hour ago";
            else
                return diffHours + " hours ago";
        } else if (diffDays < 30) {
            if (diffDays == 1)
                return diffDays + " day ago";
            else
                return diffDays + " days ago";
        } else {
            return "a long time ago..";
        }
    }

    public static Bitmap fastblur(Bitmap sentBitmap, int radius) {

        // Stack Blur v1.0 from
        // http://www.quasimondo.com/StackBlurForCanvas/StackBlurDemo.html
        // Java Author: Mario Klingemann <mario at quasimondo.com>
        // http://incubator.quasimondo.com

        // created Feburary 29, 2004
        // Android port : Yahel Bouaziz <yahel at kayenko.com>
        // http://www.kayenko.com
        // ported april 5th, 2012

        // This is a compromise between Gaussian Blur and Box blur
        // It creates much better looking blurs than Box Blur, but is
        // 7x faster than my Gaussian Blur implementation.

        // I called it Stack Blur because this describes best how this
        // filter works internally: it creates a kind of moving stack
        // of colors whilst scanning through the image. Thereby it
        // just has to add one new block of color to the right side
        // of the stack and remove the leftmost color. The remaining
        // colors on the topmost layer of the stack are either added on
        // or reduced by one, depending on if they are on the right or
        // on the left side of the stack.

        // If you are using this algorithm in your code please add
        // the following line:
        // Stack Blur Algorithm by Mario Klingemann <mario@quasimondo.com>

        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

        if (radius < 1) {
            return (null);
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = ( 0xff000000 & pix[yi] ) | ( dv[rsum] << 16 ) | ( dv[gsum] << 8 ) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);
    }

    public static void customLoginDialog(final Activity activity_context)
    {
        final Dialog dialog = new Dialog(activity_context);

        //setting custom layout to dialog
        dialog.setContentView(R.layout.login_dialog_layout);
       //dialog.setTitle("Custom Dialog");

        //adding button click event
        Text txtForgotPassword;
        txtForgotPassword = (Text) dialog.findViewById(R.id.txtForgotPassword);
        Button dismissButton = (Button) dialog.findViewById(R.id.btnSignIn);

        SpannableString str = new SpannableString("Forgot Password");
        str.setSpan(new UnderlineSpan(), 0, str.length(), Spanned.SPAN_PARAGRAPH);
        txtForgotPassword.setText(str);

        txtForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                customForgotPasswordDialog(activity_context);
            }
        });

        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().getAttributes().windowAnimations = R.style.MyCustomDialogTheme;
        dialog.show();
    }

    public static void customSignUpDialog(Activity activity_context)
    {
        final Dialog dialog = new Dialog(activity_context);

        //setting custom layout to dialog
        dialog.setContentView(R.layout.signup_dialog_layout);
        //dialog.setTitle("Custom Dialog");


        Button dismissButton = (Button) dialog.findViewById(R.id.btnSignup);


        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().getAttributes().windowAnimations = R.style.MyCustomDialogTheme;
        dialog.show();
    }

    public static void customForgotPasswordDialog(Activity activity_context)
    {
        final Dialog dialog = new Dialog(activity_context);

        //setting custom layout to dialog
        dialog.setContentView(R.layout.forgot_password_dialog);
        //dialog.setTitle("Custom Dialog");


        Button dismissButton = (Button) dialog.findViewById(R.id.btnSend);


        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().getAttributes().windowAnimations = R.style.MyCustomDialogTheme;
        dialog.show();
    }


    /////////////////get the user details from the json object
    public User parse_user(JSONObject object) {

        String f_name = null, m_name = null, l_name = null, display_name = null, number = null, email = null, dob = null, status1 = null, image = null, thumbnail = null, date = null, badges = null;
        int gender = 0, points = 0;
        long id = 0, fb_id = 0;


        try {

            if (!object.isNull(Params.ID.get_value())) {
                id = object.getLong(Params.ID.get_value());
            }
            if (!object.isNull(Params.FB_ID.get_value())) {
                fb_id = object.getLong(Params.FB_ID.get_value());
            }
            if (!object.isNull(Params.FIRST_NAME.get_value())) {
                f_name = object.getString(Params.FIRST_NAME.get_value());
            }
            if (!object.isNull(Params.MIDDLE_NAME.get_value())) {
                m_name = object.getString(Params.MIDDLE_NAME.get_value());
            }
            if (!object.isNull(Params.LAST_NAME.get_value())) {
                l_name = object.getString(Params.LAST_NAME.get_value());
            }
            if (!object.isNull(Params.DISPLAY_NAME.get_value())) {
                display_name = object.getString(Params.DISPLAY_NAME.get_value());
            }
            if (!object.isNull(Params.NUMBER.get_value())) {
                number = object.getString(Params.NUMBER.get_value());
            }
            if (!object.isNull(Params.EMAIL.get_value())) {
                email = object.getString(Params.EMAIL.get_value());
            }
            if (!object.isNull(Params.DOB.get_value())) {
                dob = object.getString(Params.DOB.get_value());
            }
            if (!object.isNull(Params.STATUS.get_value())) {
                status1 = object.getString(Params.STATUS.get_value());
            }
            if (!object.isNull(Params.IMAGE.get_value())) {
                image = object.getString(Params.IMAGE.get_value());
            }
            if (!object.isNull(Params.THUMBNAIL.get_value())) {
                thumbnail = object.getString(Params.THUMBNAIL.get_value());
            }
            if (!object.isNull(Params.DATE.get_value())) {
                date = object.getString(Params.DATE.get_value());
            }
            if (!object.isNull(Params.BADGES.get_value())) {
                badges = object.getString(Params.BADGES.get_value());
            }
            if (!object.isNull(Params.POINTS.get_value())) {
                points = object.getInt(Params.POINTS.get_value());
            }
            if (!object.isNull(Params.GENDER.get_value())) {
                gender = object.getInt(Params.GENDER.get_value());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new User(id, fb_id, gender, points, f_name, m_name, l_name, display_name, number, email, dob, status1, thumbnail, image, date, badges);

    }

    public HashMap<Long,Celebrity> parse_celebrity(JSONArray jsonArray) {


        HashMap<Long,Celebrity> celebrities = new HashMap<>();


        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                String f_name = null, m_name = null, l_name = null, dob = null, status1 = null, image = null, date = null, profession = null;
                int gender = 0, followers_count = 0;
                long id = 0;
                JSONObject object = jsonArray.getJSONObject(i);
                if (!object.isNull(Params.ID.get_value())) {
                    id = object.getLong(Params.ID.get_value());
                }
                if (!object.isNull(Params.FIRST_NAME.get_value())) {
                    f_name = object.getString(Params.FIRST_NAME.get_value());
                }
                if (!object.isNull(Params.MIDDLE_NAME.get_value())) {
                    m_name = object.getString(Params.MIDDLE_NAME.get_value());
                }
                if (!object.isNull(Params.LAST_NAME.get_value())) {
                    l_name = object.getString(Params.LAST_NAME.get_value());
                }
                if (!object.isNull(Params.DOB.get_value())) {
                    dob = object.getString(Params.DOB.get_value());
                }
                if (!object.isNull(Params.STATUS.get_value())) {
                    status1 = object.getString(Params.STATUS.get_value());
                }
                if (!object.isNull(Params.IMAGE.get_value())) {
                    image = object.getString(Params.IMAGE.get_value());
                }
                if (!object.isNull(Params.DATE.get_value())) {
                    date = object.getString(Params.DATE.get_value());
                }
                if (!object.isNull(Params.GENDER.get_value())) {
                    gender = object.getInt(Params.GENDER.get_value());
                }
                if (!object.isNull(Params.FOLLOW_COUNT.get_value())) {
                    followers_count = object.getInt(Params.FOLLOW_COUNT.get_value());
                }
                if (!object.isNull(Params.PROFESSION.get_value())) {
                    profession = object.getString(Params.PROFESSION.get_value());
                }

                celebrities.put(id,new Celebrity(id, f_name, m_name, l_name, status1, dob, image, gender, followers_count, false,profession, date));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return celebrities;

    }


    public HashMap<Long,SearchData> parse_search_result(JSONArray jsonArray) {


        HashMap<Long,SearchData> search = new HashMap<>();


        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                String f_name = null, l_name = null,status1 = null, image = null;

                long id = 0;
                JSONObject object = jsonArray.getJSONObject(i);
                if (!object.isNull(Params.ID.get_value())) {
                    id = object.getLong(Params.ID.get_value());
                }
                if (!object.isNull(Params.FIRST_NAME.get_value())) {
                    f_name = object.getString(Params.FIRST_NAME.get_value());
                }

                if (!object.isNull(Params.LAST_NAME.get_value())) {
                    l_name = object.getString(Params.LAST_NAME.get_value());
                }

                if (!object.isNull(Params.STATUS.get_value())) {
                    status1 = object.getString(Params.STATUS.get_value());
                }
                if (!object.isNull(Params.IMAGE.get_value())) {
                    image = object.getString(Params.IMAGE.get_value());
                }

                search.put(id,new SearchData(id, f_name, l_name, status1, image));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return search;

    }

    public HashMap<Long,BrandCategory> parse_brand_category(JSONArray jsonArray) {


        HashMap<Long,BrandCategory> category = new HashMap<>();


        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                String category_name = null, category_image_url = null;

                long id = 0;
                JSONObject object = jsonArray.getJSONObject(i);
                if (!object.isNull(Params.ID.get_value())) {
                    id = object.getLong(Params.ID.get_value());
                }
                if (!object.isNull(Params.CATEGORY_NAME.get_value())) {
                    category_name = object.getString(Params.CATEGORY_NAME.get_value());
                }

                if (!object.isNull(Params.IMAGE.get_value())) {
                    category_image_url = object.getString(Params.IMAGE.get_value());
                }


                category.put(id,new BrandCategory(id, category_name, category_image_url));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return category;

    }

    public HashMap<Long,Brand_Offer_Details> parse_brand_offers(JSONArray jsonArray) {


        HashMap<Long,Brand_Offer_Details> offers = new HashMap<>();


        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                String brand_name = null, brand_logo = null, offer_description = null;
                long offer_id = 0, id = 0, brand_id = 0;

                JSONObject object = jsonArray.getJSONObject(i);
                if (!object.isNull(Params.ID.get_value())) {
                    offer_id = object.getLong(Params.OFFER_ID.get_value());
                }
                if (!object.isNull(Params.NAME.get_value())) {
                    brand_name = object.getString(Params.NAME.get_value());
                }

                if (!object.isNull(Params.IMAGE.get_value())) {
                    brand_logo = object.getString(Params.IMAGE.get_value());
                }

                if (!object.isNull(Params.OFFER_DESCRIPTION.get_value())) {
                    offer_description = object.getString(Params.OFFER_DESCRIPTION.get_value());
                }

                if (!object.isNull(Params.BRAND_ID.get_value())) {
                    brand_id = object.getLong(Params.BRAND_ID.get_value());
                }


                offers.put(id,new Brand_Offer_Details(offer_id, brand_id, brand_name, brand_logo, offer_description));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return offers;

    }

    public HashMap<Long,BrandFrames> parse_brand_frames(JSONArray jsonArray) {


        HashMap<Long,BrandFrames> frames = new HashMap<>();


        try {

            for (int i = 0; i < jsonArray.length(); i++)
            {
                long frame_id = 0;
               // long id = 0;
                String frame_url = null;

                JSONObject object = jsonArray.getJSONObject(i);
                if (!object.isNull(Params.FRAME_ID.get_value()))
                {
                    frame_id = object.getLong(Params.FRAME_ID.get_value());
                }
                if (!object.isNull(Params.BRAND_FRAMES.get_value()))
                {
                    frame_url = object.getString(Params.BRAND_FRAMES.get_value());
                }

                frames.put(frame_id,new BrandFrames(frame_id, frame_url));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return frames;

    }


    public static  int getNotificationBarHeight(Activity activity_context) {
        int result = 0;
        int resourceId = activity_context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = activity_context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static float get_my_progressbar_value(int seek_position)
    {
        float value = 0;
        if(seek_position > 0 && seek_position <= 10)
        {
            value = 0.1f;
        }
        else if(seek_position > 10 && seek_position <= 20)
        {
            value = 0.2f;
        }
        else if(seek_position > 20 && seek_position <= 30)
        {
            value = 0.3f;
        }
        else if(seek_position > 30 && seek_position <= 40)
        {
            value = 0.4f;
        }
        else if(seek_position > 40 && seek_position <= 50)
        {
            value = 0.5f;
        }
        else if(seek_position > 50 && seek_position <= 60)
        {
            value = 0.6f;
        }
        else if(seek_position > 60 && seek_position <= 70)
        {
            value = 0.7f;
        }
        else if(seek_position > 70 && seek_position <= 80)
        {
            value = 0.8f;
        }
        else if(seek_position > 80 && seek_position <= 90)
        {
            value = 0.9f;
        }
        else if(seek_position > 90 && seek_position <= 100)
        {
            value = 1.0f;
        }
        Log.d("Value", Float.toString(value));
        return value;
    }

    public static float get_saturation_progressbar_value(int seek_position)
    {
        float value = 0;
        if(seek_position > 0 && seek_position <= 10)
        {
            value = -1.0f;
        }
        else if(seek_position > 10 && seek_position <= 20)
        {
            value = -0.8f;
        }
        else if(seek_position > 20 && seek_position <= 30)
        {
            value = -0.6f;
        }
        else if(seek_position > 30 && seek_position <= 40)
        {
            value = -0.4f;
        }
        else if(seek_position > 40 && seek_position <= 50)
        {
            value = -0.2f;
        }
        else if(seek_position > 50 && seek_position <= 60)
        {
            value = 0.0f;
        }
        else if(seek_position > 60 && seek_position <= 70)
        {
            value = 0.2f;
        }
        else if(seek_position > 70 && seek_position <= 80)
        {
            value = 0.4f;
        }
        else if(seek_position > 80 && seek_position <= 90)
        {
            value = 0.6f;
        }
        else if(seek_position > 90 && seek_position <= 100)
        {
            value = 0.8f;
        }
        Log.d("Value", Float.toString(value));
        return value;
    }

    public static Bitmap roundCornerImage(Bitmap src, float round) {
        // Source image size
        int width = src.getWidth();
        int height = src.getHeight();
        // create result bitmap output
        Bitmap result = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        // set canvas for painting
        Canvas canvas = new Canvas(result);
        canvas.drawARGB(0, 0, 0, 0);

        // configure paint
        final Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.BLACK);

        // configure rectangle for embedding
        final Rect rect = new Rect(0, 0, width, height);
        final RectF rectF = new RectF(rect);

        // draw Round rectangle to canvas
        canvas.drawRoundRect(rectF, round, round, paint);

        // create Xfer mode
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        // draw source image to canvas
        canvas.drawBitmap(src, rect, rect, paint);

        // return final image
        return result;
    }

    public static String create_image_from_customView(Activity activity_context)
    {
        File file = null;
        View content = activity_context.findViewById(R.id.CustomLayout);
        content.setDrawingCacheEnabled(true);
        Bitmap bitmap = content.getDrawingCache();
        try
        {
            file = new File(App.APP_FOLDER+"/Selfie"+System.currentTimeMillis()+".jpg");
            {
                if(!file.exists())
                {
                    file.createNewFile();
                }
                FileOutputStream ostream = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, ostream);
                ostream.close();
                content.invalidate();
                Log.d("Image File Path", file.getAbsolutePath());

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return file.getAbsolutePath();
    }

    public static Uri get_image_for_crop(Activity activity_context, GLSurfaceView image, Bitmap bitmap)
    {
        File file = null;
        View content = activity_context.findViewById(R.id.CustomLayout);
        content.setDrawingCacheEnabled(true);
        image.requestRender();

        Bitmap bitmapcrop = bitmap;
        try
        {
            file = new File(App.APP_FOLDER+"/Selfie"+System.currentTimeMillis()+".jpg");
            {
                if(!file.exists())
                {
                    file.createNewFile();
                }
                FileOutputStream ostream = new FileOutputStream(file);
                bitmapcrop.compress(Bitmap.CompressFormat.PNG, 100, ostream);
                ostream.close();
                content.invalidate();
                Log.d("Image File Path", file.getAbsolutePath());

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        Uri external = Uri.fromFile(file);
        return external;
    }

    public static void collage_type_popup(final Activity activity_context)
    {


        ArrayList items = new ArrayList();
        items.add("Predefined Templates");
        items.add("Dynamic Collage");

        final Dialog dialog = new Dialog(activity_context);


        dialog.setContentView(R.layout.custom_collage_type_dialog);
        dialog.setTitle("Select Mode");

        ListView ModeListView = (ListView) dialog.findViewById(R.id.offersListView);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity_context,
                android.R.layout.simple_list_item_1, android.R.id.text1, items);

        ModeListView.setAdapter(adapter);

        ModeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                if(position==0)
                {
                    intent = new Intent(activity_context, Activity_Collage.class);
                    intent.putExtra(Params.USER_ID.get_value(),App.getMyId());
                    intent.putExtra("nextActivity", position);
                    activity_context.startActivity(intent);
                    dialog.cancel();
                }
                else if(position==1)
                {
                    intent = new Intent(activity_context, Activity_Collage.class);
                    intent.putExtra(Params.USER_ID.get_value(),App.getMyId());
                    intent.putExtra("nextActivity", position);
                    activity_context.startActivity(intent);
                    dialog.cancel();
                }
            }
        });


        dialog.show();
    }


    public static Bitmap flipImage(Bitmap src, int type) {
        // create new matrix for transformation
        Matrix matrix = new Matrix();
        // if vertical
        if(type == FLIP_VERTICAL) {
            // y = y * -1
            matrix.preScale(1.0f, -1.0f);

        }
        // if horizonal
        else if(type == FLIP_HORIZONTAL) {
            // x = x * -1
            matrix.preScale(-1.0f, 1.0f);
            // unknown type
        } else {
            return null;
        }
        if(Camera_Preview.is_front)
        {
            matrix.postRotate(-90);
        }
        else
        {
            matrix.postRotate(-270);
        }

        // return transformed image
        return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
    }

    public static Bitmap rotateImage(Bitmap src, int degrees) {
        // create new matrix for transformation
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        // return transformed image
        return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
    }




    public static int getNavigationBarHeight(Activity activity_context)
    {
        int result = 0;
        int resourceId = activity_context.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = activity_context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static int getStatusBarHeight(Activity activity_context) {
        Rect r = new Rect();
        Window w = activity_context.getWindow();
        w.getDecorView().getWindowVisibleDisplayFrame(r);
        return r.top;
    }



    public static  int getActionBarHeight(Activity activity_context) {
        int actionBarHeight = 0;
        TypedValue tv = new TypedValue();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (activity_context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv,
                    true))
                actionBarHeight = TypedValue.complexToDimensionPixelSize(
                        tv.data, activity_context.getResources().getDisplayMetrics());
        } else {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,
                    activity_context.getResources().getDisplayMetrics());
        }
        return actionBarHeight;
    }


    public static Bitmap padBitmap(Bitmap bitmap)
    {
        int paddingX=20;
        int paddingY=20;

        Bitmap paddedBitmap = null;

        paddedBitmap = Bitmap.createBitmap(
                bitmap.getWidth() + paddingX,
                bitmap.getHeight() + paddingY,
                Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(paddedBitmap);
        canvas.drawARGB(0,0,0,0); // this represents app blue color
        canvas.drawBitmap(
                bitmap,
                paddingX / 2,
                paddingY / 2,
                new Paint(Paint.FILTER_BITMAP_FLAG));

        return paddedBitmap;
    }



    public String get_compressed_image(String imageUri, boolean is_selfie) {

        String filePath = imageUri;
        Bitmap scaledBitmap = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        if (actualHeight == 0 || actualWidth == 0) {
            return null;
        }


        options.inSampleSize = calculateInSampleSize(options, actualWidth,
                actualHeight);

        // inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

        // this options allow android to claim the bitmap memory if it runs low
        // on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            // load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight,
                    Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2,
                middleY - bmp.getHeight() / 2, new Paint(
                        Paint.FILTER_BITMAP_FLAG));

        // check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = App.APP_FOLDER + "/" + "IMG_" + System.currentTimeMillis() + ".jpg";
        try {
            out = new FileOutputStream(filename);

            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
            baos.writeTo(out);

            out.flush();
            out.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (is_selfie) {
            File file = new File(imageUri);
            if (file.exists()) {
                file.delete();
            }
        }
        return filename;

    }




    public static Bitmap get_lowsize_bitmap(String filePath, int degree) {

        Bitmap scaledBitmap = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BitmapFactory.Options options = new BitmapFactory.Options();

//			by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//			you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//			max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//			width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//			setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//			inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//			this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//				load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        //check the rotation of the image and display it properly
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        try {
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }


        try {
            FileOutputStream out = new FileOutputStream(filePath);

//				write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            baos.writeTo(out);

            out.flush();
            out.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return scaledBitmap;
    }

    // get compressed bitmap using bitmap as input

    public static Bitmap get_lowsize_bitmap_from_bitmap(Bitmap bmp) {

        Bitmap scaledBitmap = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BitmapFactory.Options options = new BitmapFactory.Options();

//			by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//			you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;


        int actualHeight = bmp.getHeight();
        int actualWidth = bmp.getWidth();

//			max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//			width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//			setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//			inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//			this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];


        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        //check the rotation of the image and display it properly
        Matrix matrix = new Matrix();

        try {
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }



        return scaledBitmap;
    }


    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    //Create thumbnail of image
    public String Create_Thumbnail(String image_path) {
        String filename = null;
        try {

            final int THUMBNAIL_SIZE = 64;

            FileInputStream fis = new FileInputStream(image_path);
            Bitmap imageBitmap = BitmapFactory.decodeStream(fis);

            Float width = new Float(imageBitmap.getWidth());
            Float height = new Float(imageBitmap.getHeight());
            Float ratio = width / height;
            imageBitmap = Bitmap.createScaledBitmap(imageBitmap, (int) (THUMBNAIL_SIZE * ratio), THUMBNAIL_SIZE, false);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            FileOutputStream out = null;
            filename = App.APP_FOLDER + "/" + "THUMB_" + System.currentTimeMillis() + ".jpg";
            try {
                out = new FileOutputStream(filename);

                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                baos.writeTo(out);

                out.flush();
                out.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (Exception ex) {

        }
        ///////////////////////////////////////////

        return filename;
    }
}