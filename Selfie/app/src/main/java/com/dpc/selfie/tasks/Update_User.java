package com.dpc.selfie.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.Server_Params;
import com.dpc.selfie.models.User;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by D-P-C on 27-Mar-15.
 */
public class Update_User extends AsyncTask<String, Long, Boolean> {
    private Context c;
    private User user;

    public Update_User(Context c,User user) {
        this.c = c;
        this.user = user;
    }


    @Override
    protected Boolean doInBackground(String... params) {

        return update_user();
    }

    @Override
    protected void onPostExecute(Boolean result) {

    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    public boolean update_user() {
        /*boolean success = false;
        JSONParser parser = new JSONParser();
        JSONObject response = null;
        try {

            response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                    new Server_Params().update_rating(image_id, rating));
            if (response != null) {
                Log.d("response", response.toString());
                if (response.has(Params.STATUS.get_value())) {
                    int status = response.getInt(Params.STATUS.get_value());
                    if (status == 200) {
                        success = true;

                    }
                }
            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return success;*/
        return  false;
    }

}