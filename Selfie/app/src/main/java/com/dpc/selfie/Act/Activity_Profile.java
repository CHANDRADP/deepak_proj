package com.dpc.selfie.Act;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.adapters.Details_Adapter;
import com.dpc.selfie.adapters.Profile_Adapter;
import com.dpc.selfie.customs.BaseActivity;
import com.dpc.selfie.customs.CircleImage;
import com.dpc.selfie.customs.CircleSeekbar;
import com.dpc.selfie.customs.EText;
import com.dpc.selfie.customs.MyProgress;
import com.dpc.selfie.customs.ProfileTabsLayout;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.customs.TextB;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.Celebrity;
import com.dpc.selfie.models.Profile_Item;
import com.dpc.selfie.models.Server_Params;
import com.dpc.selfie.models.User;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Activity_Profile extends BaseActivity implements View.OnClickListener{


    private String[] details = null;

    final int[] detail_icons = new int[]{R.drawable.cupcake,R.drawable.phone,R.drawable.mail};

    final int[] detail_cele_icons = new int[]{R.drawable.cupcake,R.drawable.phone,R.drawable.mail};

   // private static final String[] tabs = new String[]{"Selfie friends","Invite friends"};

    public static ArrayList<Profile_Item> selfies;


    private boolean is_celebrity = false,edit_mode = false;

    private User user = null;

    private Celebrity celebrity;

    private CircleImage dp;

    private ImageView bg_image,edit;

    private TextB name,all;

    private Text points;

    private EText edit_status;

    private ProfileTabsLayout mSlidingTabLayout;

    private CircleSeekbar circleSeekbar;

    private ViewPager viewPager;

    private SamplePagerAdapter adapter;

    private RecyclerView selfie_list;

    private int width = 0,height = 0,u_points = 0;

    private long user_id;

    private boolean running = true;

    final Runnable r = new Runnable() {
        public void run() {
            double offset = 2.7777;
            double progress = 0;
            double total = u_points/10;
            //double p_limit = (360/u_points)*u_points;
            while(progress < total && running)
            {

                circleSeekbar.setProgress((int)progress);
                progress += offset;

                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            if(u_points == 1000)
            {
                circleSeekbar.setProgress(100);
            }
            running = false;


        }
    };

    private  int[] get_width_for_lower_versions()
    {
        DisplayMetrics display = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(display);
        int[] dimen = new int[]{display.widthPixels,display.heightPixels};
        return dimen;
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private  int[] get_width_for_higher_versions()
    {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int[] dimen = new int[]{size.x,size.y};
        return dimen;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        init_views();
        final Intent intent = this.getIntent();
        if(intent.hasExtra(Params.USER_ID.get_value()))
        {
            user_id = intent.getLongExtra(Params.USER_ID.get_value(), -1);
            is_celebrity = intent.getBooleanExtra(Params.CELEBRITY.get_value(),false);
            if(user_id > 0)
            {
                if(is_celebrity) {
                    celebrity = Activity_Friends.celebrities_map.get(user_id);
                    details = new String[]{celebrity.getDob(),
                            celebrity.getProfession(),
                            String.valueOf(celebrity.getFollow_count())+" followers"};
                }
                else {
                    user = db.Get_User_Details(user_id);
                    String number = user.getNumber();
                    String dob = user.getDob();
                    if(number == null || number.equalsIgnoreCase("null"))
                        number = "Not available";
                    if(dob == null || dob.equalsIgnoreCase("null"))
                        dob = "Not available";
                    details = new String[]{dob,
                            number,
                            user.getEmail()};
                }
            }


        }

        selfies = new ArrayList<>();
        new Get_Selfies().execute();





    }

    @Override
    protected void onResume() {
        super.onResume();
        update_views();
    }

    int[] dimen = null;
    private void init_views()
    {

        bg_image = (ImageView)findViewById(R.id.bg);
        edit = (ImageView)findViewById(R.id.edit);
        edit_status = (EText)findViewById(R.id.edit_status);

        dp = (CircleImage)findViewById(R.id.dp);
        circleSeekbar = (CircleSeekbar)findViewById(R.id.circleseekbar);
        name = (TextB)findViewById(R.id.name);
        points = (Text)findViewById(R.id.points);


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        adapter = new SamplePagerAdapter();
        viewPager.setAdapter(adapter);

        mSlidingTabLayout = (ProfileTabsLayout) findViewById(R.id.tabs);
        mSlidingTabLayout.setCustomTabView(R.layout.item_tab, R.id.txt);
        mSlidingTabLayout.setViewPager(viewPager);

        edit.setOnClickListener(this);
    }

    private void update_views()
    {
        String image = null,_name = null,_points = null;
        if(is_celebrity)
        {
            _name = celebrity.getF_name();
            image = celebrity.getImage();
            _points = celebrity.getStatus();
        }
        else
        {
            user = db.Get_User_Details(user_id);
            _name = user.getDisplay_name();
            image = user.getImage();
            _points = String.valueOf(user.getPoints());
        }
        if (android.os.Build.VERSION.SDK_INT >= 13) {
            dimen = get_width_for_higher_versions();

        } else if (android.os.Build.VERSION.SDK_INT < 13) {
            dimen = get_width_for_lower_versions();

        }
        int viewTop = getWindow().findViewById(Window.ID_ANDROID_CONTENT).getTop();
        if(dimen != null && dimen.length == 2)
        Picasso.with(getApplicationContext())
                .load(image)
                .resize(dimen[0],dimen[1]-viewTop)
                .placeholder(R.drawable.ic_action_person)
                .error(R.drawable.ic_action_person)
                .into(bg_image);

        Picasso.with(getApplicationContext())
                .load(image)
                .resize(dimen[0],dimen[1]-viewTop)
                .placeholder(R.drawable.ic_action_person)
                .error(R.drawable.ic_action_person)
                .into(dp);

        name.setText(_name);
        if(is_celebrity) {
            if(_points == "" || _points == null)
                _points = "No status";
            points.setText(_points);
            edit.setBackgroundResource(R.drawable.ic_action_edit);
          //  edit.setVisibility(View.VISIBLE);
        }
        else
        {
            circleSeekbar.setVisibility(View.VISIBLE);

            points.setText(_points + " points");
            u_points = Integer.parseInt(_points);
            Thread s = new Thread(r);
            s.start();
        }

        if(selfie_list != null) {
            selfie_list.setAdapter(new Profile_Adapter(Activity_Profile.this));
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                super.onBackPressed();
                break;


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.edit:
                if(!edit_mode)
                {
                    edit_mode = !edit_mode;
                    points.setVisibility(View.GONE);
                    edit_status.setText(points.getText());
                    edit.setBackgroundResource(R.drawable.ic_action_send_now);
                    edit_status.setVisibility(View.VISIBLE);
                }
                else
                {
                    String txt = edit_status.getText().toString();
                    if(txt != null && txt.length() > 0)
                    {
                        edit_mode = !edit_mode;
                        points.setVisibility(View.VISIBLE);
                        edit_status.setText(points.getText());
                        edit.setBackgroundResource(R.drawable.ic_action_edit);
                        edit_status.setVisibility(View.GONE);
                        new Update_Status().execute(txt);
                    }
                    else
                    {
                        C.alert(getApplicationContext(),"Please type your status");
                    }


                }
                break;
        }
    }

    /////////////////////////viewpager adapter
    class SamplePagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {
            return o == view;
        }

        @Override
        public String getPageTitle(int position) {

            return null;
        }
        @Override
        public Object instantiateItem(ViewGroup view,final int position) {
            View v = null;

            if(position == 0)
            {
                v= getLayoutInflater().inflate(R.layout.profile_details,
                        view, false);
                final RecyclerView list = (RecyclerView) v.findViewById(R.id.list);
                list.setHasFixedSize(true);
                LinearLayoutManager llm = new LinearLayoutManager(Activity_Profile.this);
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                list.setLayoutManager(llm);
                list.setAdapter(new Details_Adapter(Activity_Profile.this,
                        detail_icons,
                        details


                ));


            }
            else if(position == 1)
            {

                v= getLayoutInflater().inflate(R.layout.profile_details,
                        view, false);
                v.findViewById(R.id.top_divider).setVisibility(View.VISIBLE);
               all = (TextB) v.findViewById(R.id.all);
                all.setVisibility(View.VISIBLE);
                all.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Activity_Profile.this,Activity_Preview.class);
                        intent.putExtra(Params.ID.get_value(),user_id);
                        startActivity(intent);
                    }
                });

                selfie_list = (RecyclerView) v.findViewById(R.id.list);
                selfie_list.setHasFixedSize(true);
                GridLayoutManager llm = new GridLayoutManager(Activity_Profile.this, 4);
                llm.setOrientation(GridLayoutManager.VERTICAL);
                selfie_list.setLayoutManager(llm);



            }

            ((ViewPager) view).addView(v, 0);
            return v;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);

        }

    }

    //////////////////////////////////////////////// get_user_selfies
    public class Get_Selfies  extends AsyncTask<String, Long, Boolean> {

        public Get_Selfies() {

        }


        @Override
        protected Boolean doInBackground(String... params) {

            return get_selfies();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result)
            {
                if(selfie_list != null) {
                        selfie_list.setAdapter(new Profile_Adapter(Activity_Profile.this));
                }
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        public boolean get_selfies() {
            boolean success = false;
            JSONParser parser = new JSONParser();
            JSONObject response = null;
            try {

                long user_id = 0;
                if(is_celebrity)
                    user_id = celebrity.getId();
                else
                user_id = user.getUserid();
                response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                        new Server_Params().get_selfies(user_id));
                if (response != null) {
                    Log.d("response", response.toString());
                    if (response.has(Params.STATUS.get_value())) {
                        int status = response.getInt(Params.STATUS.get_value());
                        if (status == 200) {

                            JSONArray data = response.getJSONArray(Params.DATA.get_value());
                            for(int i = 0;i < data.length();i++)
                            {
                                JSONObject image = data.getJSONObject(i);
                            //    JSONObject image = image_data.getJSONObject(Params.IMAGE.get_value());

                                String url = image.getString(Params.IMAGE.get_value());
                                long id = image.getLong(Params.ID.get_value());
                                selfies.add(new Profile_Item(id,url));

                            }

                            success = true;

                        }
                    }
                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return success;
        }

    }

    ////////////////update user status
    public class Update_Status extends AsyncTask<String, Long, Boolean> {
        private MyProgress myProgress;
        private String status_txt;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            myProgress = new MyProgress(Activity_Profile.this);
            myProgress.setCancelable(false);
            myProgress.setTitle("Updating...");
            myProgress.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            status_txt = params[0];
            return update_status();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result)
            {
                points.setText(status_txt);
            }
            if(myProgress != null)
                myProgress.dismiss();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        public boolean update_status() {
        boolean success = false;
        JSONParser parser = new JSONParser();
        JSONObject response = null;
        try {

            response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                    new Server_Params().update_status(status_txt));
            if (response != null) {
                Log.d("response", response.toString());
                if (response.has(Params.STATUS.get_value())) {
                    int status = response.getInt(Params.STATUS.get_value());
                    if (status == 200) {
                        success = true;

                    }
                }
            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return success;

        }

    }


}
