package com.dpc.selfie.Act;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.adapters.Celebrity_Adapter;
import com.dpc.selfie.adapters.Friends_Adapter;
import com.dpc.selfie.customs.BaseActivity;
import com.dpc.selfie.customs.FriendsTabsLayout;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.Celebrity;
import com.dpc.selfie.models.Server_Params;
import com.dpc.selfie.models.User;
import com.dpc.selfie.tasks.Update_App_Users;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by D-P-C on 09-Mar-15.
 */
public class Activity_Friends extends BaseActivity {

    private static final String[] tabs = new String[]{"Selfie friends","Follow Celebs"};
    private FriendsTabsLayout mSlidingTabLayout;
    private ViewPager viewPager;
    private SamplePagerAdapter adapter;
    private Friends_Adapter list_adapter;

    private View empty;

    private ListView app_list,celebrity_list;

    private ArrayList<User> app_users = new ArrayList<>();

    public static HashMap<Long,Celebrity> celebrities_map = new HashMap<>();

    private ArrayList<Celebrity> celebrities = new ArrayList<>();


    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (intent.hasExtra(Params.STATUS.get_value())) {
                    //update UI
                    app_users = db.Get_App_Users();
                    setApp_list_listener();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_friends);
        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);
        mSlidingTabLayout = (FriendsTabsLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        adapter = new SamplePagerAdapter();
        viewPager.setAdapter(adapter);

         mSlidingTabLayout.setCustomTabView(R.layout.item_friends_tab, R.id.txt);
        mSlidingTabLayout.setViewPager(viewPager);
        celebrities_map.clear();
        new Update_Celebrities().execute();


        ((ImageView)findViewById(R.id.camera)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Activity_Friends. this, Activity_Edit_First.class));
            }
        });

        Bundle get_params = new Bundle();
        get_params.putString("fields", "id,name,picture");
        final Request request = new Request(Session.getActiveSession(), "me/friends", get_params,
                HttpMethod.GET, friends_callback);
        request.executeAsync();

    }



    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(broadcastReceiver, new IntentFilter(Activity_Login.BROADCAST_RECEIVER));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;

        }


        return super.onOptionsItemSelected(item);
    }
    class SamplePagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return tabs.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {
            return o == view;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return tabs[position];
        }
        @Override
        public Object instantiateItem(ViewGroup view,final int position) {
            View v = getLayoutInflater().inflate(R.layout.friends_viewpagr_item,
                    view, false);

            if(position == 0)
            {

                app_list = (ListView) v.findViewById(R.id.list);
                empty = v.findViewById(R.id.empty);
                app_users = db.Get_App_Users();
                if(app_users.size() > 0)
                {
                    empty.setVisibility(View.GONE);
                    setApp_list_listener();

                }




            }
            else if(position == 1)
            {

                celebrity_list = (ListView) v.findViewById(R.id.list);
                v.findViewById(R.id.empty).setVisibility(View.GONE);


            }
            ((ViewPager) view).addView(v, 0);
            return v;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);

        }

    }

    private void setApp_list_listener()
        {

        if(app_list != null)
        {
            if(list_adapter == null) {
                list_adapter = new Friends_Adapter(getApplicationContext(), app_users);
                app_list.setAdapter(list_adapter);
                app_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent i = new Intent(Activity_Friends.this,Activity_Profile.class);
                        i.putExtra(Params.USER_ID.get_value(),app_users.get(position).getUserid());
                        i.putExtra(Params.CELEBRITY.get_value(),false);
                        startActivity(i);
                    }
                });
            }
            else
            list_adapter.update_list(app_users);

        }
    }

    Request.Callback friends_callback = new Request.Callback() {
        public void onCompleted(Response response) {
            GraphObject graphResponse = response
                    .getGraphObject();

            if (graphResponse != null) {

                FacebookRequestError error = response.getError();
                if (error != null) {
                    print(getApplicationContext(),error.toString());
                    return;
                }

                JSONObject object = graphResponse.getInnerJSONObject();
                print(getApplicationContext(),object.toString());
                JSONArray users_array;
                try {
                    users_array = object.getJSONArray(Params.DATA.get_value());
                    ArrayList<Long> ids = new ArrayList<>();

                    for (int i = 0; i < users_array.length(); i++) {
                        JSONObject object1 = users_array.getJSONObject(i);
                        long id = object1.getLong(Params.ID.get_value());
                        if(!db.is_user_exist(id))
                        ids.add(id);
                        print(getApplicationContext(), object1.toString());
                    }

                    if (ids.size() > 0)
                        new Update_App_Users(getApplicationContext(), ids, true).execute();



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }
    };

    private void setcelebrity_list_listener()
    {

        if(celebrity_list != null)
        {
            celebrity_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(Activity_Friends.this,Activity_Profile.class);
                    i.putExtra(Params.USER_ID.get_value(),celebrities.get(position).getId());
                    i.putExtra(Params.CELEBRITY.get_value(),true);
                    startActivity(i);
                }
            });
        }
    }


    ///////////////////get all celebrities
    public class Update_Celebrities  extends AsyncTask<String, Long, Boolean> {
       public Update_Celebrities()
        {
         }



        @Override
        protected Boolean doInBackground(String... params) {

            return get_celebrities();
        }

        @Override
        protected void onPostExecute(Boolean result) {

           if(celebrities != null && celebrities.size() > 0)
           {
               celebrity_list.setAdapter(new Celebrity_Adapter(Activity_Friends.this,celebrities));

           }
            else
           {
               celebrities = db.Get_Celebrities();
               if(celebrities.size() > 0)
               {
                   celebrity_list.setAdapter(new Celebrity_Adapter(Activity_Friends.this,celebrities));

               }
           }
            setcelebrity_list_listener();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        public boolean get_celebrities()
        {
            boolean success = false;
            JSONParser parser = new JSONParser();
            JSONObject response = null;
            try {

                response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                        new Server_Params().get_celebrities());
                if(response != null)
                {
                    Log.d("response", response.toString());
                    if(response.has(Params.STATUS.get_value()))
                    {
                        int status = response.getInt(Params.STATUS.get_value());
                        if (status == 200)
                        {


                            JSONArray data = response.getJSONArray(Params.DATA.get_value());
                            celebrities_map = new C().parse_celebrity(data);
                            ArrayList<Long> followers = db.Get_Celebrity_ids();
                            for (int i = 0; i < followers.size();i++)
                            {
                                if(celebrities_map.containsKey(followers.get(i)))
                                {
                                    celebrities_map.get(followers.get(i)).setIs_following(true);

                                }
                            }
                            for(Celebrity celebrity : celebrities_map.values())
                            {
                                celebrities.add(celebrity);
                            }
                            followers = null;
                            success = true;




                        }
                    }
                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return success;
        }

    }



}