package com.dpc.selfie.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dpc.selfie.App;
import com.dpc.selfie.R;
import com.dpc.selfie.Utils.DB;
import com.dpc.selfie.customs.CircleImage;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.models.Rating;
import com.dpc.selfie.models.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by D-P-C on 06-Mar-15.
 */
public class Rating_Adapter extends RecyclerView.Adapter<Rating_Adapter.ViewHolder> {

    private Context context;

    private DB db;

    private ArrayList<Rating> ratings ;

    public Rating_Adapter(Context context, ArrayList<Rating> ratings) {
        this.context = context;
        db = new DB(context);
        this.ratings = ratings;

    }


    @Override
    public int getItemCount() {
        if(ratings == null)
            return 0;
        else
        return ratings.size();
    }
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Rating rating = ratings.get(i);
        User user = db.Get_User_Details(rating.getUser_id());



        if(user != null && user.getUserid() == App.getMyId()) {
            viewHolder.dp.draw_border(true);
            display_image(user.getImage(),viewHolder.dp,true);

        }
        else
        {
            viewHolder.dp.draw_border(false);
            display_image(rating.getUser_image(),viewHolder.dp,true);

        }
//        viewHolder.name.setText(user.getF_name());
        viewHolder.rating.setText(String.valueOf(rating.getRating()));

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup,final int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_rating, viewGroup, false);

        return new ViewHolder(itemView);
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        protected CircleImage dp;
        protected Text name,rating;


        public ViewHolder(View v) {
            super(v);
            dp = (CircleImage)v.findViewById(R.id.image);
            name = (Text)v.findViewById(R.id.name);
            rating = (Text)v.findViewById(R.id.rating);


        }

    }

    private void display_image(String url,View v,boolean is_circle_image)
    {

        try {
            Picasso.with(context)
                    .load(url)
                    .placeholder(R.drawable.ic_action_person)
                    .error(R.drawable.ic_action_person)
                    .into(is_circle_image ? (CircleImage)v : (ImageView)(v));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

}