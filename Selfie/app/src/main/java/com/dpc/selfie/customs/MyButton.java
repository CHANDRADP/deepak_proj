package com.dpc.selfie.customs;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by D-P-C on 16-Feb-15.
 */
public class MyButton extends Button{

    public MyButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
      //  init();
    }

    public MyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
       // init();
    }

    public MyButton(Context context) {
        super(context);
      //  init();
    }

    private void init() {
        //set_bold(false);
    }
    public void set_bold(boolean bold)
    {
        Typeface tf;
        if(bold)
        tf = Typeface.createFromAsset(getContext().getAssets(),"Proximab.otf");
        else
            tf = Typeface.createFromAsset(getContext().getAssets(),"Proxima.otf");
        if(tf != null)
        setTypeface(tf);

    }


}

