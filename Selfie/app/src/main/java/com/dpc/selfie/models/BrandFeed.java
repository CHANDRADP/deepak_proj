package com.dpc.selfie.models;

/**
 * Created by D-P-C on 28-Mar-15.
 */
public class BrandFeed {

    private long tag_id = -1,user_id = -1;
    private String f_name,m_name,l_name,image = null,selfie = null,status = null,date = null;
    private int rating = 0,comment_count = 0;

    public BrandFeed(long tag_id,long user_id, String f_name, String m_name, String l_name, String image, String selfie,String status, String date, int rating, int comment_count) {
        this.tag_id = tag_id;
        this.user_id = user_id;
        this.f_name = f_name;
        this.m_name = m_name;
        this.l_name = l_name;
        this.image = image;
        this.selfie = selfie;
        this.status = status;
        this.date = date;
        this.rating = rating;
        this.comment_count = comment_count;
    }

    public long getTag_id() {
        return tag_id;
    }

    public void setTag_id(long tag_id) {
        this.tag_id = tag_id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getM_name() {
        return m_name;
    }

    public void setM_name(String m_name) {
        this.m_name = m_name;
    }

    public String getL_name() {
        return l_name;
    }

    public void setL_name(String l_name) {
        this.l_name = l_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSelfie() {
        return selfie;
    }

    public void setSelfie(String selfie) {
        this.selfie = selfie;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getrating() {
        return rating;
    }

    public void setRate_count(int rate_count) {
        this.rating = rate_count;
    }

    public int getComment_count() {
        return comment_count;
    }

    public void setComment_count(int comment_count) {
        this.comment_count = comment_count;
    }
}
