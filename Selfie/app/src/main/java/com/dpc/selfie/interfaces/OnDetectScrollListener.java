package com.dpc.selfie.interfaces;
 public interface OnDetectScrollListener {

    void onUpScrolling();

    void onDownScrolling();
    
    void onLeftScrolling();
    
    void onRightScrolling();
}
