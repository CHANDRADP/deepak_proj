package com.dpc.selfie.adapters;

/**
 * Created by Deepak on 05-07-2015.
 */
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dpc.selfie.R;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.interfaces.OnCardClickListener;


public class Font_Adapter extends RecyclerView.Adapter<Font_Adapter.ViewHolder>
{

    private Context context;

    private int selected_posi = 0;

   // private int[] frames = new int[]{R.drawable.rcbframe, R.drawable.frame_five, R.drawable.frame_six, R.drawable.frame_seven, R.drawable.frame_eight, R.drawable.frame_one, R.drawable.frame_two, R.drawable.frame_three, R.drawable.frame_four};

    private OnCardClickListener onCardClickListener;

    private int font_position;


    public void setOnCardClickListener(OnCardClickListener onCardClickListener) {
        this.onCardClickListener = onCardClickListener;
    }

    public Font_Adapter(Context context) {
        this.context = context;
    }


    @Override
    public int getItemCount() {
        return 7; // number of fonts
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        font_position = i;
       /* if (selected_posi == i) {

            //if(names != null)
            //viewHolder.name.setTextColor(context.getResources().getColor(R.color.blue));
        } else {
            //viewHolder.imageView.setBackgroundResource(R.drawable.blue);
            //if(names != null)
            // viewHolder.name.setTextColor(context.getResources().getColor(R.color.white));
        }*/


        Typeface tf = null;
        if(font_position == 0)
        {
            tf = Typeface.createFromAsset(context.getAssets(), "font_one.ttf");
        }
        else if(font_position == 1)
        {
            tf = Typeface.createFromAsset(context.getAssets(), "font_two.ttf");
        }
        else if(font_position == 2)
        {
            tf = Typeface.createFromAsset(context.getAssets(), "font_five.ttf");
        }
        else if(font_position == 3)
        {
            tf = Typeface.createFromAsset(context.getAssets(), "font_six.ttf");
        }
        else if(font_position ==4)
        {
            tf = Typeface.createFromAsset(context.getAssets(), "font_seven.ttf");
        }
        else if(font_position == 5)
        {
            tf = Typeface.createFromAsset(context.getAssets(), "font_eight.otf");
        }
        else if(font_position ==6)
        {
            tf = Typeface.createFromAsset(context.getAssets(), "font_nine.ttf");
        }

        viewHolder.font_text.setTypeface(tf);
        viewHolder.font_text.setText("Deepak");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_font, viewGroup, false);
        return new ViewHolder(itemView);
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected Text font_text;
        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            font_text = (Text) v.findViewById(R.id.font_text);
        }

        @Override
        public void onClick(View v) {
            selected_posi = getPosition();
            notifyDataSetChanged();
            if (onCardClickListener != null)
            {
                onCardClickListener.onClick(v, selected_posi);
            }
        }
    }

}