package com.dpc.selfie.models;

/**
 * Created by D-P-C on 17-Mar-15.
 */
public class Setting {

    private String name = null;
    private boolean checked = false,header = false;

    public Setting(String name, boolean checked, boolean header) {
        this.name = name;
        this.checked = checked;
        this.header = header;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isHeader() {
        return header;
    }

    public void setHeader(boolean header) {
        this.header = header;
    }
}
