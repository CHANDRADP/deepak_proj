package com.dpc.selfie.tasks;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.dpc.selfie.R;


public class Display_Image extends AsyncTask<Bitmap, Long, Bitmap> {
    Bitmap image = null;
	ImageView img;
	Context c;


	public Display_Image(Context c, Bitmap image, ImageView img) {
		this.image = image;
		this.img = img;
		this.c = c;

	}


	

	@Override
	protected Bitmap doInBackground(Bitmap... params) {

		return null;
	}

	@Override
	protected void onPostExecute(Bitmap result) {
		img.setImageBitmap(image);
        final Animation anim_in  = AnimationUtils.loadAnimation(c, R.anim.fadein);
        anim_in.setAnimationListener(new Animation.AnimationListener() {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation) {}
        });
        img.startAnimation(anim_in);
        image = null;

	}



}