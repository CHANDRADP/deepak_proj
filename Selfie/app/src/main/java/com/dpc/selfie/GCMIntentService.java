package com.dpc.selfie;

import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Vibrator;
import android.util.Log;

import com.dpc.selfie.Act.Activity_Notifications;
import com.dpc.selfie.Act.Activity_Selfie;
import com.dpc.selfie.App;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.DB;
import com.dpc.selfie.Utils.WakeLocker;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.Notification;
import com.dpc.selfie.models.User;
import com.dpc.selfie.tasks.Update_App_Users;
import com.dpc.selfie.tasks.Update_GCM_Task;
import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GCMIntentService extends GCMBaseIntentService {
	private static final String TAG = "GCMIntentService";
	public GCMIntentService() {
        super(C.SENDER_ID);
        
       
    }

    /**
     * Method called on device registered
     **/
    @Override
    protected void onRegistered(Context context, String registrationId) {
        Log.i(TAG, "Device registered: regId = " + registrationId);
        App.gcm_updated(false);
        App.update_gcm_id(registrationId);
        GCMRegistrar.setRegisteredOnServer(context, true);
        new Update_GCM_Task(context,registrationId).execute();
		
     
       
    }

    /**
     * Method called on device un registred
     * */
    @Override
    protected void onUnregistered(Context context, String registrationId) {
    	GCMRegistrar.setRegisteredOnServer(context, false);
        App.gcm_updated(false);
        App.update_gcm_id(null);

    }

  
    /////////////////////////

    /**
     * Method called on Receiving a new message
     * */
    @Override
    protected void onMessage(Context context, Intent intent) 
    {
        WakeLocker.acquire(getApplicationContext());

            String message = intent.getExtras().getString(Params.DATA.get_value());
            C.alert(getApplicationContext(),message);
            try {
                JSONObject object = new JSONObject(message);
                Log.i(TAG, object.toString());
                Notification notification = null;
                int type = -1;
                long user_id = -1, selfie_id = -1;
                String image = null,time = null;
                int read_status = -1;
                if(object.has(Params.TYPE.get_value()))
                {
                    type = object.getInt(Params.TYPE.get_value());
                    user_id = object.getLong(Params.USER_ID.get_value());
                    selfie_id = object.getLong(Params.IMAGE_ID.get_value());
                    time = C.get_UTC_date();//object.getString(Params.DATE.get_value());
                    notification = new Notification(type,-1,user_id,selfie_id,null,null,time,read_status);

                    User user = new DB(context).Get_User_Details(user_id);
                    if(user == null)
                    {
                        ArrayList<Long> ids = new ArrayList<Long>();
                        ids.add(user_id);
                        new Update_App_Users(getApplicationContext(), ids, false,notification).execute();
                    }
                    else
                    {
                        if(type == 1) {
                            image = object.getString(Params.IMAGE.get_value());
                            user.setImage(image);
                            new DB(context).Sync_App_User(user);
                        }

                        long id = new DB(context).add_notification(notification);
                        if(id > 0)
                        {
                            notification.setId(id);
                            //generate notification
                            Log.d("feed",String.valueOf(notification.getType())+":"+String.valueOf(App.get_feed()));
                            Log.d("comment",String.valueOf(notification.getType())+":"+String.valueOf(App.get_rating()));
                            Log.d("rating",String.valueOf(notification.getType())+":"+String.valueOf(App.get_comment()));
                            if(notification.getType() == 1 && App.get_feed())
                                generateNotification(context, notification);
                            else if(notification.getType() == 2 && App.get_comment())
                                generateNotification(context, notification);
                            else if(notification.getType() == 3 && App.get_rating())
                                generateNotification(context, notification);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }



			
			

		
		
    
		WakeLocker.release();
       
    }
    
   
    /**
     * Method called on receiving a deleted message
     * */
    @Override
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
      //  generateNotification(context, message);
    }

    /**
     * Method called on Error
     * */
    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
        
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        return super.onRecoverableError(context, errorId);
    }



    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static void generateNotification(Context context,Notification notification) {
        User user = new DB(context).Get_User_Details(notification.getUserid());
        String message_content = user.getF_name();
        if(notification.getType() == 1)
        {
            message_content += " posted a new selfie";
        }
        else if(notification.getType() == 2)
        {
            message_content += " commented on a selfie";
        }
        else if(notification.getType() == 3)
        {
            message_content += " rated on your selfie";
        }
        Intent notificationIntent = new Intent(context, Activity_Selfie.class);
        notificationIntent.putExtra(Params.IMAGE_ID.get_value(), notification.getImageid());
        notificationIntent.putExtra(Params.ID.get_value(), notification.getId());
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent contentIntent = PendingIntent.getActivity(context,
                (int) System.currentTimeMillis(), notificationIntent,
                0);


        NotificationManager nm = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        Resources res = context.getResources();
        android.app.Notification.Builder builder = new android.app.Notification.Builder(context);

        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.cplogo)
                .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.cplogo))
                .setTicker(message_content)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                        //.setOngoing(ongoing)
                .setDefaults(App.get_sound() ? android.app.Notification.DEFAULT_SOUND : 0)
               // .setDefaults(App.get_vibration() ? 0 : android.app.Notification.DEFAULT_VIBRATE)
                .setContentTitle(res.getString(R.string.app_name))
                .setContentText(message_content);

        android.app.Notification n = builder.build();

        nm.notify((int) notification.getId(), n);
        if(App.get_vibration()) {
            Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            // Vibrate for 500 milliseconds
            v.vibrate(500);
        }
    }
   
  
   
}
