package com.dpc.selfie.adapters;

/**
 * Created by Deepak on 20-07-2015.
 */
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dpc.selfie.Act.Activity_Brand_Frames;
import com.dpc.selfie.R;
import com.dpc.selfie.customs.CircleImage;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.BrandCategory;
import com.dpc.selfie.models.Brand_Offer_Details;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class BrandOffers_Adapter extends RecyclerView.Adapter<BrandOffers_Adapter.ViewHolder> {

    public Context context;
    public String image_path;
    public ArrayList<Brand_Offer_Details> offer_list = new ArrayList<>();


    // Provide a suitable constructor (depends on the kind of dataset)
    public BrandOffers_Adapter(ArrayList<Brand_Offer_Details> offer_list, Context context, String image_path) {
        this.offer_list = offer_list;
        this.image_path = image_path;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public BrandOffers_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_brand_offers, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        final Brand_Offer_Details offer_details = offer_list.get(position);
        viewHolder.txtBrandOffer.setText(offer_details.getOffer_description());
        viewHolder.txtBrandName.setText(offer_details.getBrand_name());
        display_image(offer_details.getBrand_logo(), viewHolder.imgBrandIcon, false);



        viewHolder.main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "You Clicked "+position, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(context, Activity_Brand_Frames.class);
                intent.putExtra("image_path", image_path);
                intent.putExtra(Params.BRAND_ID.get_value(), offer_details.getBrand_id());
                context.startActivity(intent);
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return offer_list.size();
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public Text txtBrandName;
        ImageView imgBrandIcon;
        TextView txtBrandOffer;
        LinearLayout main;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            txtBrandName = (Text) itemLayoutView.findViewById(R.id.brand_name);
            txtBrandOffer = (TextView) itemLayoutView.findViewById(R.id.brand_offer);
            imgBrandIcon = (ImageView) itemLayoutView.findViewById(R.id.brand_icon);
            main = (LinearLayout) itemLayoutView.findViewById(R.id.main);

        }
    }

    protected void display_image(String url,View v,boolean is_circle_image)
    {
        try {
            Picasso.with(context)
                    .load(url)
                    .placeholder(R.drawable.ic_action_person)
                    .error(R.drawable.ic_action_person)
                    .into(is_circle_image ? (CircleImage)v : (ImageView)(v));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}