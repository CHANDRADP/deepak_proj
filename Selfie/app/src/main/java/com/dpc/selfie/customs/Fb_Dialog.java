package com.dpc.selfie.customs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.dpc.selfie.R;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.interfaces.OnButtonClickListener;
import com.facebook.Session;
import com.facebook.SessionDefaultAudience;

import java.util.Arrays;
import java.util.List;

/**
 * Created by D-P-C on 03-Apr-15.
 */
public class Fb_Dialog extends DialogFragment{

    private String image_url = null;
    public OnButtonClickListener clickListener;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        Bundle args = this.getArguments();

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.R.color.transparent));
        //   Rect displayRectangle = new Rect();
        //   Window window = this.getSherlockActivity().getWindow();
        //  window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        //   LayoutInflater inflater = (LayoutInflater)getSherlockActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //   View layout = inflater.inflate(R.layout.review_dialog, null);
        //  layout.setMinimumWidth((int)(displayRectangle.width() * 0.9f));
        //   layout.setMinimumHeight((int)(displayRectangle.height() * 0.5f));
        //  RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        dialog.setContentView(R.layout.fb_dialog);

        dialog.getWindow().setBackgroundDrawable(null);
        dialog.show();
        final Button ok = (Button)dialog.findViewById(R.id.ok);
        final Button cancel = (Button)dialog.findViewById(R.id.cancel);

        ok.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            clickListener.setOKSubmitListener(1);

            }

        });
        cancel.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
            clickListener.setOKSubmitListener(-1);
            dialog.dismiss();

            }

        });

        return dialog;
    }



}