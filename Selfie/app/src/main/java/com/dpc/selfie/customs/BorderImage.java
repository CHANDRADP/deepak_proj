package com.dpc.selfie.customs;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by D-P-C on 18-Mar-15.
 */
public class BorderImage extends ImageView {

    private static final int PADDING = 8;
    private static final float STROKE_WIDTH = 0.5f;

    private Paint mBorderPaint;

    private boolean selected = false;

    public BorderImage(Context context) {
        this(context, null);
    }

    public BorderImage(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
      //  setPadding(PADDING, PADDING, PADDING, PADDING);
    }

    public BorderImage(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initBorderPaint();
    }

    private void initBorderPaint() {
        mBorderPaint = new Paint();
        mBorderPaint.setAntiAlias(true);
        mBorderPaint.setStyle(Paint.Style.STROKE);
        if(selected)
        mBorderPaint.setColor(Color.BLACK);
        else
            mBorderPaint.setColor(Color.WHITE);
        mBorderPaint.setStrokeWidth(STROKE_WIDTH);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int height = this.getMeasuredHeight();
        int width = this.getMeasuredWidth();
        canvas.drawRect(0, 0, width, height, mBorderPaint);
       // canvas.drawRect(PADDING, PADDING, getWidth() - PADDING, getHeight() - PADDING, mBorderPaint);
    }

    public void change_selection(boolean selected)
    {
        this.selected = selected;
        initBorderPaint();
        invalidate();
    }
}