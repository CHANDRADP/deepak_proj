package com.dpc.selfie.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


import com.dpc.selfie.App;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.Server_Params;

import org.json.JSONException;
import org.json.JSONObject;

public class Update_App_version extends AsyncTask<String, Long, Boolean> {
    private Context c;


    public Update_App_version(Context c) {
        this.c = c;

    }


    @Override
    protected Boolean doInBackground(String... params) {

        return update_version();
    }

    @Override
    protected void onPostExecute(Boolean result) {

        if (result)
        {
            App.set_app_version(c);
        }


    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    public boolean update_version()
    {
        boolean success = false;
        JSONParser parser = new JSONParser();
        JSONObject response = null;
        try {

            response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                    new Server_Params().get_app_version_params(App.get_version_from_manifest(c),App.get_gcm_id()));

                if (response != null) {
                    Log.d("response", response.toString());
                    if (response.has(Params.STATUS.get_value())) {
                        int status = response.getInt(Params.STATUS.get_value());
                        if (status == 200) {
                            success = true;

                        }
                    }
                }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return success;
    }

}