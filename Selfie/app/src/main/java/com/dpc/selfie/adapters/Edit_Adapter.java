package com.dpc.selfie.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.DB;
import com.dpc.selfie.Utils.Edit;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.interfaces.OnCardClickListener;

/**
 * Created by D-P-C on 14-Mar-15.
 */
public class Edit_Adapter extends RecyclerView.Adapter<Edit_Adapter.ViewHolder> {

    private Context context;

    private int selected_posi = 0;

    private int[] frames = new int[]{R.drawable.default_frame_ten,R.drawable.default_frame_two,R.drawable.default_frame_three,R.drawable.default_frame_four,R.drawable.default_frame_five,R.drawable.default_frame_six,R.drawable.default_frame_seven,R.drawable.default_frame_eight,R.drawable.default_frame_nine};


    private int[] effects_one = new int[]{R.drawable.effect_1977,R.drawable.effect_amaro,R.drawable.effect_brannan,R.drawable.effect_earlybird,R.drawable.effect_hefe,R.drawable.effect_hudson,R.drawable.effect_inkwell,R.drawable.effect_lomo,R.drawable.effect_lordkelvin, R.drawable.effect_nashville, R.drawable.effect_rise, R.drawable.effect_sierra, R.drawable.effect_sutro, R.drawable.effect_toaster, R.drawable.effect_walden, R.drawable.effect_xproll, R.drawable.effect_tonecurve, R.drawable.effect_lookup};

    private int[] effects_two = new int[]{R.drawable.effect_brightness,R.drawable.effect_contrast, R.drawable.effect_sepia, R.drawable.effect_vignette};


    private final String[] names_two = new String[]{"Autofix","Vignette","Temperature","Tint","Crossprocess","Documentary","Duotone","Filllight","Flipvert","Fliphor","Grain"," Grayscale"," Lomoish","Negative","Posterize"," Rotate","Saturate","Sepia","Sharpen"};

    private final String[] names_three = new String[]{"Brightness","Contrast"};

    private String[] names;

    private int type;

    private OnCardClickListener onCardClickListener;

    private Edit edit;

    private Bitmap bitmap;

    public void setOnCardClickListener(OnCardClickListener onCardClickListener)
    {
        this.onCardClickListener = onCardClickListener;
    }
    public Edit_Adapter(Context context,String[] names,int type,Bitmap bitmap) {
        this.context = context;
        this.names = names;
        this.type = type;
        this.bitmap = bitmap;
        edit = new Edit(this.context,bitmap);

    }


    @Override
    public int getItemCount() {

            if(type == 0) {
                return 18;
            }
            if(type == 1)
            {
                return 4;
            }
            if(type == 2)
            {
               return 9;
            }
            if(type == 5)
            {
                return 51;
            }

        return 0;
    }
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        //viewHolder.imageView.setPadding(2,2,2,2);
        if(selected_posi == i)
        {
            //viewHolder.imageView.setBackgroundResource(R.drawable.new_state);
            viewHolder.root.setBackgroundResource(R.drawable.new_state);
            //if(names != null)
            //viewHolder.name.setTextColor(context.getResources().getColor(R.color.blue));
        }
        else
        {
            //viewHolder.imageView.setBackgroundResource(R.drawable.blue);
            //if(names != null)
           // viewHolder.name.setTextColor(context.getResources().getColor(R.color.white));
        }

            edit.setBitmap(bitmap);
            if(type == 0) {
                //viewHolder.imageView.setImageBitmap(C.roundCornerImage(edit.get_preview(context, type, i, 0, effects[i], null, null),30));
                viewHolder.name.setText(names[i]);
                //viewHolder.name.setVisibility(View.GONE);
                //viewHolder.imageView.setBackgroundResource(R.drawable.bigbang);
                viewHolder.root.setBackgroundResource(effects_one[i]);



            }
            else if(type == 1) {
                viewHolder.name.setText(names[i]);
                viewHolder.root.setBackgroundResource(effects_two[i]);
            }
            else if(type == 2) {
                //viewHolder.imageView.setImageBitmap(C.roundCornerImage(edit.get_preview(context, type, i, 0, effects_one[i], null, null),30));
                viewHolder.name.setText(names[i]);
                viewHolder.imageView.setVisibility(View.VISIBLE);
                viewHolder.imageView.setImageBitmap(edit.get_preview(context, type, i, 0,frames[i],null,null));
            }
            else if(type == 4)
            {

            }
            else if(type == 5)
            {



            }
            else if(type == 6) {
                viewHolder.imageView.setImageBitmap(C.roundCornerImage(edit.get_preview(context, type, i, 0, 0, null, null),30));
                //viewHolder.name.setText(names[i]);
            }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup,final int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_edit, viewGroup, false);


        return new ViewHolder(itemView);
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        //protected ImageView imageView;
        protected ImageView imageView;
        protected Text name;
        protected LinearLayout root;


        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            imageView = (ImageView)v.findViewById(R.id.image);
            name = (Text)v.findViewById(R.id.name);
            root = (LinearLayout) v.findViewById(R.id.root);
        }

        @Override
        public void onClick(View v) {
            selected_posi = getPosition();
            notifyDataSetChanged();
            if(onCardClickListener != null)
            {
                onCardClickListener.onClick(v,selected_posi);
            }
        }
    }

}