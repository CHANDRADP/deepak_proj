package com.dpc.selfie.Act;


import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.dpc.selfie.App;
import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.adapters.Comment_Adapter;
import com.dpc.selfie.adapters.Menu_Adapter;
import com.dpc.selfie.adapters.Rating_Adapter;
import com.dpc.selfie.adapters.Search_Adapter;
import com.dpc.selfie.customs.BaseActivity;
import com.dpc.selfie.customs.DividerItemDecorator;
import com.dpc.selfie.customs.EText;
import com.dpc.selfie.customs.ImageLoader;
import com.dpc.selfie.customs.MyGesture;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.customs.TextB;
import com.dpc.selfie.customs.WheelMenu;
import com.dpc.selfie.enums.*;
import com.dpc.selfie.enums.Process;

import com.dpc.selfie.interfaces.OnCardClickListener;
import com.dpc.selfie.interfaces.OnDetectScrollListener;
import com.dpc.selfie.models.Comment;
import com.dpc.selfie.models.DeletePositionImageId;
import com.dpc.selfie.models.Feed;
import com.dpc.selfie.models.Rating;
import com.dpc.selfie.models.SearchData;
import com.dpc.selfie.models.Server_Params;
import com.dpc.selfie.models.User;
import com.dpc.selfie.stickyHeader.DividerDecoration;
import com.dpc.selfie.stickyHeader.RecyclerArrayAdapter;
import com.dpc.selfie.stickyHeader.RecyclerItemClickListener;
import com.dpc.selfie.stickyHeader.StickyRecyclerHeadersAdapter;
import com.dpc.selfie.stickyHeader.StickyRecyclerHeadersDecoration;
import com.dpc.selfie.stickyHeader.StickyRecyclerHeadersTouchListener;
import com.dpc.selfie.tasks.Update_Rating;
import com.dpc.selfie.tasks.Update_comment;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ExecutionException;

public class Activity_Feeds extends BaseActivity implements View.OnClickListener, OnDetectScrollListener {

    public static final String POST_STATE = "com.dpc.selfie.post_state";

    private final Class[] activites = new Class[]{Activity_Profile.class,Activity_Notifications.class,Activity_Friends.class,Activity_Settings.class, Activity_Collage.class, Activity_MyBrands.class};

    //private final int[] richColors = new int[]{R.color.rich_blue, R.color.rich_green, R.color.rich_orange, R.color.rich_pink, R.color.rich_red, R.color.rich_yellow};

    private int feed_position = -1,refresh_position = 0,new_items_count = 0;

    private boolean feeds_completed = false;

    private ListView suggestion_list;

    private LinearLayout list_bg;
    private FrameLayout search_list_bg;

    private ProgressBar loading;

    private long page = 0;

    private GestureDetector gestureDetector;

    private WheelMenu wheelMenu;

    private Animation inAnimation,outAnimation,open,close;

    private RelativeLayout comment_view,ll_wheel,root;

    private LinearLayout ll_menu;

    private Toolbar toolbar;

    private ImageView camera;

    private EText comment;

    private Text rate_count,no_comments;

    private RecyclerView rate_list,comment_list, feed_list, search_list;

    private View wheel_layout;

    private LinearLayoutManager feed_list_manager;

    private ListView list_menu;

    private  Feed_Adapter feeds_adapter;
    private ArrayList<User> app_users = new ArrayList<>();
    private Search_Adapter search_adapter;
    private boolean run_once = false;

    public static HashMap<Long,SearchData> search_map = new HashMap<>();
    private ArrayList<SearchData> search_results = new ArrayList<>();

    private ImageLoader imageLoader;
   private boolean user_posted_image = false;




    protected Comment_Adapter comment_adapter;

    public  ArrayList<Feed> feeds = new ArrayList<>();

    private void remove_item(long id)
    {
        for(Feed feed: feeds)
        {
            if(feed.getSelfie_id() == id)
            {
                int index = feeds.indexOf(feed);
                feeds.remove(index);
                feeds_adapter.notifyItemRemoved(index);
                break;
            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent me){
        gestureDetector.onTouchEvent(me);
        return super.dispatchTouchEvent(me);
    }



    private BroadcastReceiver selfie_post_Receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


            Bundle b = intent.getExtras();
            long state = b.getLong(Params.IMAGE_ID.get_value());

            if(state > 0)
            {
                if(b.containsKey("position"))
                {
                    int position = b.getInt("position");
                    if(feeds.size() >= position)
                    {
                        if(state  > 0 && feeds.get(position).getSelfie_id() == state)
                        {
                            feeds.get(position).setRating(b.getInt(Params.RATING.get_value()));
                            feeds_adapter.notifyItemChanged(position);
                        }
                    }
                }
                else  if(b.containsKey(Process.DELETE_IMAGE.get_value()))
                {
                    remove_item(state);

                }
                else {

                    user_posted_image = true;

                    Feed feed = new Feed(App.getMyId(),
                            state,
                            b.getString(Params.IMAGE.get_value()),
                            b.getString(Params.STATUS.get_value()),
                            C.get_UTC_date(),
                            0,
                            0,
                            null,
                            null,
                            null,
                            false);
                    feeds.add(0, feed);
                    feeds_adapter.add(0,feeds.get(0));
                    feeds_adapter.notifyItemInserted(0);
                    feed_list.smoothScrollToPosition(0);



                }

            }


        }
    };
    private void toggle_menu(boolean open, final boolean is_menu)
    {

        if(open)
        {

            inAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    if(is_menu)
                        ll_menu.setVisibility(View.VISIBLE);
                    else
                        comment_view.setVisibility(View.VISIBLE);

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            if(is_menu) {
                list_menu.setAdapter(new Menu_Adapter(this));
                ll_menu.startAnimation(inAnimation);
            }
            else
                comment_view.startAnimation(inAnimation);
        }
        else
        {


            outAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {


                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            if(is_menu) {
                ll_menu.startAnimation(outAnimation);
                ll_menu.setVisibility(View.GONE);
            }
            else
            {
                comment_view.startAnimation(outAnimation);
                comment_view.setVisibility(View.GONE);
            }
        }
    }

    private void toggle_rate(boolean _open)
    {
        if(_open)
        {
            ll_wheel.setVisibility(View.VISIBLE);
            open.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    ll_wheel.setVisibility(View.VISIBLE);

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            ll_wheel.startAnimation(open);
        }
        else
        {

            ll_wheel.setVisibility(View.GONE);
            close.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {


                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            ll_wheel.startAnimation(close);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.my_search_view:
                Log.d("Hi ","Hi");

                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_feeds);
        MyGesture myGesture = new MyGesture(this);
        myGesture.set_min_distance(3);
        gestureDetector = new GestureDetector(getApplicationContext(), myGesture);
        inAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_down);
        outAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_up);

        open = AnimationUtils.loadAnimation(this, R.anim.slide_out);
        close = AnimationUtils.loadAnimation(this, R.anim.slide_in);

        init_views();



        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ll_menu.getVisibility() == View.GONE) {
                    toggle_menu(true,true);
                }
            }
        });

        new Get_Feeds().execute();
        if(App.is_new_user())
        {
            startActivity(new Intent(this,Activity_Edit_First.class));
        }
    }




    @Override
    protected void onResume() {
        super.onResume();
        if(feeds_adapter != null)
            feeds_adapter.notifyDataSetChanged();
    }

    private void init_views()
    {
        root = (RelativeLayout)findViewById(R.id.root);
        toolbar =  (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Feed");
        Toolbar comment_toolbar = (Toolbar) findViewById(R.id.comment_toolbar);
        comment_toolbar.setNavigationIcon(R.drawable.back);
        comment_toolbar.setTitle("Comments");
        //feed_list = (RecyclerView) findViewById(R.id.feed_list);
        feed_list = (RecyclerView) findViewById(R.id.feed_list);

       // list_bg = (LinearLayout)findViewById(R.id.search_list_bg);
        imageLoader = new ImageLoader(getApplicationContext());
        search_list_bg = (FrameLayout) findViewById(R.id.search_list_bg);
        search_list = (RecyclerView) findViewById(R.id.search_list);
        search_list.setLayoutManager(new LinearLayoutManager(this));
        SearchView my_search_view = (SearchView) toolbar.findViewById(R.id.my_search_view);
        my_search_view.setQueryHint("Search friend");


        my_search_view.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub

                Toast.makeText(getBaseContext(), String.valueOf(hasFocus),
                        Toast.LENGTH_SHORT).show();
            }
        });

        //*** setOnQueryTextListener ***
        my_search_view.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                // TODO Auto-generated method stub

                Toast.makeText(getBaseContext(), query,
                        Toast.LENGTH_SHORT).show();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String search_text) {
                // TODO Auto-generated method stub

                if(search_text.length()>2)
                {
                    new do_search_task().execute(search_text);
                }

                if(search_text.length()==0)
                {
                    search_list.setVisibility(View.GONE);

                }
                return false;
            }
        });
        //    feeds_adapter = new F_Adapter();
        //   feed_list.setAdapter(feeds_adapter);
   /*     feed_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(feeds.get(position).isIs_brand())
                {
                    Intent i = new Intent(Activity_Feeds.this, Activity_Brand_Feeds.class);
                    i.putExtra(Params.BRAND_ID.get_value(), feeds.get(position).getSelfie_id());
                    startActivity(i);
                }
            }
        });
        */






        ll_menu = (LinearLayout)findViewById(R.id.ll_menu);


        ll_menu.setVisibility(View.GONE);
        list_menu = (ListView)findViewById(R.id.menu);


        rate_list = (RecyclerView) findViewById(R.id.rate_list);
        rate_list.setHasFixedSize(true);
        GridLayoutManager llm1 = new GridLayoutManager(this,1);
        llm1.setOrientation(GridLayoutManager.HORIZONTAL);
        rate_list.setLayoutManager(llm1);

        comment_view = (RelativeLayout)findViewById(R.id.comment_view);
        comment_list = (RecyclerView) findViewById(R.id.comment_list);
        comment = (EText)findViewById(R.id.comment);
        rate_count = (Text)findViewById(R.id.rate_count);
        no_comments = (Text)findViewById(R.id.no_comments);
        TextB send = (TextB) findViewById(R.id.send);

        loading = (ProgressBar)findViewById(R.id.loading);


        send.setOnClickListener(this);

        LinearLayoutManager llm2 = new LinearLayoutManager(this);
        llm2.setOrientation(LinearLayoutManager.VERTICAL);
        comment_list.setLayoutManager(llm2);
        comment_list.addItemDecoration(new DividerItemDecorator(this,DividerItemDecorator.VERTICAL_LIST));

        list_menu.setAdapter(new Menu_Adapter(this));
        list_menu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(new Intent(Activity_Feeds.this,activites[position]));
                if(position == 4)
                {
                    collage_type_popup();
                }
                else
                {
                    i = new Intent(new Intent(Activity_Feeds.this,activites[position]));
                    if(position == 0 || position == 4)
                        i.putExtra(Params.USER_ID.get_value(),App.getMyId());
                    startActivity(i);
                }
                toggle_menu(false,true);
            }
        });

        comment_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggle_menu(false,false);
                camera.setVisibility(View.VISIBLE);
                comment_adapter = null;
            }
        });

        camera = (ImageView)findViewById(R.id.camera);
        camera.setOnClickListener(this);

         //feed_list.setHasFixedSize(true);
        feed_list_manager = new LinearLayoutManager(this);
        feed_list_manager.setOrientation(LinearLayoutManager.VERTICAL);
        feed_list.setLayoutManager(feed_list_manager);
        feed_list.addItemDecoration(new DividerItemDecorator(this,DividerItemDecorator.VERTICAL_LIST));

        feeds_adapter = new Feed_Adapter();
        feed_list.setAdapter(feeds_adapter);

        feed_list.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if(reached_lsat_position() && !feeds_completed)
                {
                    if(!run_once)
                    {
                        new Get_Feeds().execute();
                        run_once = true;
                    }

                }


            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        });


    }

    public class do_search_task  extends AsyncTask<String, Long, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            String search_text = params[0];

            return get_search_result(search_text);
        }

        @Override
        protected void onPostExecute(Boolean result) {

            if(search_results != null && search_results.size() > 0)
            {
                search_list.setVisibility(View.VISIBLE);
                search_adapter = new Search_Adapter(Activity_Feeds.this, search_results);
                search_list.setAdapter(search_adapter);
                search_list.addItemDecoration(new DividerItemDecorator(Activity_Feeds.this,DividerItemDecorator.VERTICAL_LIST));
                search_list.bringToFront();

                search_adapter.setOnCardClickListener(new OnCardClickListener() {
                    @Override
                    public void onClick(View v, int position) {
                        SearchData data = search_results.get(position);
                        Intent i = new Intent(Activity_Feeds.this,Activity_Profile.class);
                        i.putExtra(Params.USER_ID.get_value(),data.getId());
                        i.putExtra(Params.CELEBRITY.get_value(),false);
                        startActivity(i);
                    }
                });


            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        public boolean get_search_result(String search_text)
        {
            boolean success = false;
            JSONParser parser = new JSONParser();
            JSONObject response = null;
            try {

                response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                        new Server_Params().get_searched_user(search_text));
                if(response != null)
                {
                    Log.d("response", response.toString());
                    if(response.has(Params.STATUS.get_value()))
                    {
                        int status = response.getInt(Params.STATUS.get_value());
                        if (status == 200)
                        {


                            JSONObject data = response.getJSONObject(Params.DATA.get_value());
                            JSONArray array = data.getJSONArray("user");
                            search_map = new C().parse_search_result(array);

                            search_results.clear();
                            for(SearchData searchData : search_map.values())
                            {
                                search_results.add(searchData);
                            }

                            success = true;




                        }
                    }
                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return success;
        }

    }


    private boolean reached_lsat_position()
    {
        if(feed_list_manager != null)
        {
            int last_position = feed_list_manager.findLastVisibleItemPosition();//.findLastCompletelyVisibleItemPosition();
            if(last_position == feeds.size() - 1)
            {
                Log.d("Reached last position","hi");
                return  true;
            }
        }
        return false;
    }

    private void collage_type_popup()
    {


        ArrayList items = new ArrayList();
        items.add("Predefined Templates");
        items.add("Dynamic Collage");

        final Dialog dialog = new Dialog(Activity_Feeds.this);


        dialog.setContentView(R.layout.custom_collage_type_dialog);
        dialog.setTitle("Select Mode");

        ListView ModeListView = (ListView) dialog.findViewById(R.id.offersListView);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, items);

        ModeListView.setAdapter(adapter);

        ModeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                if(position==0)
                {
                    intent = new Intent(Activity_Feeds.this, Activity_Collage.class);
                    intent.putExtra(Params.USER_ID.get_value(),App.getMyId());
                    intent.putExtra("nextActivity", position);
                    startActivity(intent);
                    dialog.cancel();
                }
                else if(position==1)
                {
                    intent = new Intent(Activity_Feeds.this, Activity_Collage.class);
                    intent.putExtra(Params.USER_ID.get_value(),App.getMyId());
                    intent.putExtra("nextActivity", position);
                    startActivity(intent);
                    dialog.cancel();
                }
            }
        });


        dialog.show();
    }

    public boolean delete_image(int image_id, int delete_position) {
        boolean success = false;
        JSONParser parser = new JSONParser();
        JSONObject response = null;
        try {

            response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                    new Server_Params().delete_selfie(image_id));
            if (response != null) {
                Log.d("response", response.toString());
                if (response.has(Params.STATUS.get_value())) {
                    int status = response.getInt(Params.STATUS.get_value());
                    if (status == 200) {
                        JSONObject data = response.getJSONObject(Params.DATA.get_value());
                        int  id = data.getInt(Params.ID.get_value());
                        if(id > 0)
                            success = true;
                        feeds.remove(delete_position);
                        User user = db.Get_User_Details(App.getMyId());
                        user.setImage(data.getString(Params.IMAGE.get_value()));
                        db.Sync_App_User(user);
                        if(feeds.size() == 1)
                        {
                            App.set_user_mode(true);
                        }

                    }
                }
            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return success;
    }

    private void delete_popup(final int image_id, final int feed_position)
    {
        ArrayList items = new ArrayList();
        items.add("Delete the Image");

        final Dialog dialog = new Dialog(Activity_Feeds.this);

        dialog.setContentView(R.layout.custom_collage_type_dialog_no_title);
        //dialog.setTitle("Select Mode");

        ListView ModeListView = (ListView) dialog.findViewById(R.id.deleteListView);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, items);

        ModeListView.setAdapter(adapter);

        ModeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                if(position==0)
                {
                    DeletePositionImageId deleteObject = new DeletePositionImageId(feed_position, image_id);
                    new DeleteImageTask().execute(deleteObject);///////////////
                    dialog.cancel();
                }
            }
        });
        dialog.show();
    }

    private class DeleteImageTask extends AsyncTask<DeletePositionImageId, Void, String> {

        boolean deleteResult = false;
        int delete_position = 0;
        int delete_id = 0;

        @Override
        protected String doInBackground(DeletePositionImageId... id) {
            DeletePositionImageId[] image_id = id ;
            delete_id = image_id[0].getImageId();
            delete_position = image_id[0].getPosition();
            deleteResult = delete_image(delete_id, delete_position);
            return null;
        }
        @Override
        protected void onPostExecute(String result) {

            if(deleteResult)
            {

                if(feeds != null && feeds.size() > refresh_position) {
                    feeds_adapter.remove(feeds_adapter.getItem(delete_position));
                    feeds_adapter.notifyDataSetChanged();
                }
            }
        }
    }







    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(selfie_post_Receiver, new IntentFilter(POST_STATE));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(selfie_post_Receiver);
    }



    @Override
    public void onBackPressed() {
        if(ll_menu.getVisibility() == View.VISIBLE) {
            toggle_menu(false,true);
            return;
        }
        else if(comment_view.getVisibility() == View.VISIBLE)
        {
            toggle_menu(false,false);
            camera.setVisibility(View.VISIBLE);
            comment_adapter = null;
            onResume();
            return;
        }
        else if(ll_wheel != null && ll_wheel.getVisibility() == View.VISIBLE)
        {
            toggle_rate(false);
            return;
        }
        else
        {
            super.onBackPressed();
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.ll_menu:
                if(ll_menu.getVisibility() == View.VISIBLE) {
                    toggle_menu(false,true);
                }
                break;
            case R.id.camera:
                startActivity(new Intent(this,CaptureActivity.class));
                break;
            case R.id.send:
                String txt = comment.getText().toString().trim();
                if(txt.length() > 0)
                {
                    comment.setText("");
                    no_comments.setVisibility(View.GONE);
                    update_comment_list(txt);
                }
                else
                {
                    C.alert(this,"Please enter a text");
                }
                break;
            case R.id.ll_wheel:
                toggle_rate(false);
                break;

        }
    }

    @Override
    public void onUpScrolling() {
        if(ll_menu.getVisibility() == View.VISIBLE) {
            toggle_menu(false,true);
        }

    }

    @Override
    public void onDownScrolling() {

    }

    @Override
    public void onLeftScrolling() {

    }

    @Override
    public void onRightScrolling() {

    }

    private void update_comment_list(String txt)
    {
        Comment comment = null;
        long comment_id = 0;
        if(txt != null) {
            try {
                comment_id = new Update_comment(this, feeds.get(feed_position).getSelfie_id(), txt).execute().get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            comment = new Comment(App.getMyId(),null,null, txt, C.get_UTC_date(), comment_id);
            feeds.get(feed_position).add_comment(comment);
        }

        if(comment_adapter == null) {
            comment_adapter = new Comment_Adapter(Activity_Feeds.this, feeds.get(feed_position).getUsers_comment());
            comment_list.setAdapter(comment_adapter);
        }
        else
        {
            comment_adapter.update_items(comment);
            comment_list.smoothScrollToPosition(feeds.get(feed_position).getUsers_comment().size()-1);

        }

        feeds_adapter.notifyItemChanged(feed_position);


    }
    //////////////////////////////get user selfies

    class Get_Feeds extends AsyncTask<String, Long, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(String... params) {
            if(feeds.size() > 0)
            {
                refresh_position = feeds.size() - 1;
            }
            return get_feeds();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            loading.setVisibility(View.GONE);
            if(result) {
                //feeds_adapter = new Feed_Adapter(Activity_Feeds.this, feeds, imageLoader);
                //setCardClickListener();
                // feed_list.setAdapter(feeds_adapter);
                if(feeds != null && feeds.size() > refresh_position) {
                    //  feeds_adapter.notifyItemRangeInserted(refresh_position + 1, new_items_count);
                    //feeds_adapter.add(new Feed(0,0,null,null,null,0,0,null,null,null,false));
                    feeds_adapter.addAll(feeds);
                    feed_list.setAdapter(feeds_adapter);
                    if(page>0)
                    {
                        feed_list.scrollToPosition(refresh_position);
                    }


                    // Set layout manager
                //    int orientation = getLayoutManagerOrientation(getResources().getConfiguration().orientation);
                //    final LinearLayoutManager layoutManager = new LinearLayoutManager(Activity_Feeds.this, orientation, false);
                    feed_list.setLayoutManager(feed_list_manager);

                    // Add the sticky headers decoration
                    final StickyRecyclerHeadersDecoration headersDecor = new StickyRecyclerHeadersDecoration(feeds_adapter);
                    feed_list.addItemDecoration(headersDecor);

                    // Add decoration for dividers between list items
                    feed_list.addItemDecoration(new DividerDecoration(Activity_Feeds.this));

                    // Add touch listeners
                    StickyRecyclerHeadersTouchListener touchListener =
                            new StickyRecyclerHeadersTouchListener(feed_list, headersDecor);
                    touchListener.setOnHeaderClickListener(
                            new StickyRecyclerHeadersTouchListener.OnHeaderClickListener() {
                                @Override
                                public void onHeaderClick(View header, int position, long headerId) {
                                    Toast.makeText(Activity_Feeds.this, "Header position: " + position + ", id: " + headerId,
                                            Toast.LENGTH_SHORT).show();
                                }
                            });
                    feed_list.addOnItemTouchListener(touchListener);
                    feed_list.addOnItemTouchListener(new RecyclerItemClickListener(Activity_Feeds.this, new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            feeds_adapter.remove(feeds_adapter.getItem(position));
                        }
                    }));
                    feeds_adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                        @Override public void onChanged() {
                            headersDecor.invalidateHeaders();
                        }
                    });
                    //feeds_adapter.notifyDataSetChanged();
                }

            }
        }
        private int getLayoutManagerOrientation(int activityOrientation) {
            if (activityOrientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                return LinearLayoutManager.VERTICAL;
            } else {
                return LinearLayoutManager.HORIZONTAL;
            }
        }
        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        public boolean get_feeds() {
            boolean success = false;
            JSONParser parser = new JSONParser();
            JSONObject response;
            try {

                response = parser.makeHttpRequest(C.SERVER_URL, "POST",
                        new Server_Params().get_feeds(page));
                if (response != null) {
                    Log.d("response", response.toString());
                    if (response.has(Params.STATUS.get_value())) {
                        int status = response.getInt(Params.STATUS.get_value());
                        if (status == 200) {
                            new_items_count = 0;


                            JSONObject data = response.getJSONObject(Params.DATA.get_value());
                            JSONArray feeds_array = data.getJSONArray(Params.FEEDS.get_value());
                            for (int i = 0; i < feeds_array.length(); i++) {
                                JSONObject image_data = feeds_array.getJSONObject(i);
                                JSONObject image = image_data.getJSONObject(Params.IMAGE.get_value());

                             /*   JSONArray tags_array = image_data.getJSONArray(Params.TAGS.get_value());
                                ArrayList<Tag> tags = new ArrayList<>();
                                for (int j = 0;j < tags_array.length();j++)
                                {
                                    JSONObject tag = tags_array.getJSONObject(j);
                                    tags.add(new Tag(tag.getInt(Params.TAG.get_value())));
                                }
                                */
                                JSONArray rating_array = image_data.getJSONArray(Params.RATING.get_value());
                                ArrayList<Rating> ratings = new ArrayList<>();
                                for (int j = 0;j < rating_array.length();j++)
                                {
                                    JSONObject rating = rating_array.getJSONObject(j);
                                    ratings.add(new Rating(rating.getLong(Params.USER_ID.get_value()),
                                            rating.getString(Params.IMAGE.get_value()),
                                            rating.getInt(Params.RATING.get_value()),
                                            rating.getString(Params.DATE.get_value())
                                    ));
                                }

                                JSONArray comment_array = image_data.getJSONArray(Params.COMMENT.get_value());
                                ArrayList<Comment> comments = new ArrayList<>();
                                for (int j = 0;j < comment_array.length();j++)
                                {
                                    JSONObject comment = comment_array.getJSONObject(j);

                                    comments.add(new Comment(comment.getLong(Params.USER_ID.get_value()),
                                            comment.getString(Params.FIRST_NAME.get_value()),
                                            comment.getString(Params.IMAGE.get_value()),
                                            comment.getString(Params.COMMENT.get_value()),
                                            comment.getString(Params.DATE.get_value()),
                                            comment.getLong(Params.ID.get_value())
                                    ));
                                }
                                ++new_items_count;
                                String img_status = null;
                                if(!image.isNull(Params.STATUS.get_value()))
                                    img_status = image.getString(Params.STATUS.get_value());
                                feeds.add(new Feed(image.getLong(Params.USER_ID.get_value()),
                                        image.getLong(Params.ID.get_value()),
                                        image.getString(Params.IMAGE.get_value()),
                                        img_status,
                                        image.getString(Params.DATE.get_value()),
                                        image_data.getInt(Params.MY_RATING.get_value()),
                                        image.getInt(Params.RATING.get_value()),
                                        null,ratings,comments,false));
                            }
                            JSONArray brands_array = data.getJSONArray(Params.BRANDS_FEED.get_value());
                            for (int i = 0; i < brands_array.length(); i++) {
                                JSONObject brands_data = brands_array.getJSONObject(i);
                                Feed feed = new Feed(-1,
                                        brands_data.getLong(Params.ID.get_value()),
                                        brands_data.getString(Params.SELFIE.get_value()),
                                        brands_data.getString(Params.IMAGE.get_value()),
                                        brands_data.getString(Params.NAME.get_value()),
                                        -1,-1,null,null,null,true
                                );
                                if(feeds.size() > 20) {
                                    Random random = new Random();

                                    int min =  0;
                                    int max = feeds.size()-1;
                                    int posi =  random.nextInt(max - min + 1) + min;
                                    feeds.add(posi,feed);
                                }
                            }
                            if(new_items_count == 0)
                            {
                                feeds_completed = true;
                            }
                            if(feeds.size() > 0)
                            {

                                page = feeds.get(feeds.size() - 1).getSelfie_id();
                            }
                            success = true;

                        }
                    }
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return success;
        }

    }

    public Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable){
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            File file =  new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS), "share_image_" + System.currentTimeMillis() + ".png");
            file.getParentFile().mkdirs();
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }


    /**
     * Created by D-P-C on 19-Feb-15.
     */
    /**
     * Created by D-P-C on 19-Feb-15.
     */
    public class Feed_Adapter extends RecyclerArrayAdapter<Feed,Feed_Adapter.ViewHolder> implements StickyRecyclerHeadersAdapter<RecyclerView.ViewHolder>{


        @Override
        public long getHeaderId(int position) {

                return getItem(position).getSelfie_id();

        }

        @Override
        public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.sticky_header, parent, false);
            return new RecyclerView.ViewHolder(view) {
            };
        }

        @Override
        public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, final int position) {
            //TextView textView = (TextView) holder.itemView;
            Text txtName = (Text) holder.itemView.findViewById(R.id.txtName);
            ImageView image = (ImageView) holder.itemView.findViewById(R.id.image);

            /*if(user_posted_image)
            {
                User currentUser = db.Get_User_Details(App.getMyId());
                if(App.getMyId()!=-1) // if it is not a brand "RCB"
                {
                    txtName.setText(currentUser.getDisplay_name());
                    display_image(currentUser.getImage(),image,true);
                    holder.itemView.setBackgroundColor(Color.WHITE);
                }

                user_posted_image = false;
            }
            else
            {*/
                User currentUser = db.Get_User_Details(getItem(position).getUser_id());
                if(getItem(position).getUser_id()!=-1) // if it is not a brand "RCB"
                {
                    txtName.setText(currentUser.getDisplay_name());
                    imageLoader.DisplayImage(currentUser.getImage(),R.drawable.ic_action_person, image);
                    display_image(currentUser.getImage(),image,true);
                    holder.itemView.setBackgroundColor(Color.WHITE);
                }

           // }

            txtName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(App.getMyId() == getItem(position).getUser_id())
                    {
                        Intent i = new Intent(new Intent(Activity_Feeds.this,Activity_Profile.class));
                        i.putExtra(Params.USER_ID.get_value(),App.getMyId());
                        startActivity(i);
                    }

                }
            });

            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (App.getMyId() == getItem(position).getUser_id()) {
                        Intent i = new Intent(new Intent(Activity_Feeds.this, Activity_Profile.class));
                        i.putExtra(Params.USER_ID.get_value(), App.getMyId());
                        startActivity(i);
                    }
                }
            });


        }


        @Override
        public int getItemCount() {
            return feeds.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
            public ImageView image, rate, comment, time_indicator, imgDelete, flag;
            public Text time, rate_count, comment_count, status;

            public ViewHolder(View itemView) {
                super(itemView);
                status = (Text) itemView.findViewById(R.id.status);
                time = (Text) itemView.findViewById(R.id.time);
                rate_count = (Text) itemView.findViewById(R.id.rate_count);
                comment_count = (Text) itemView.findViewById(R.id.comment_count);
                rate = (ImageView) itemView.findViewById(R.id.rate);
                comment = (ImageView) itemView.findViewById(R.id.comment);
                image = (ImageView) itemView.findViewById(R.id.image);
                flag = (ImageView) itemView.findViewById(R.id.flag);
                imgDelete = (ImageView) itemView.findViewById(R.id.imgDelete);

                time_indicator = (ImageView) itemView.findViewById(R.id.clock);
            }

            @Override
            public void onClick(View v) {
                if (feeds.get(getPosition()).isIs_brand()) {
                    Intent i = new Intent(Activity_Feeds.this, Activity_Brand_Feeds.class);
                    i.putExtra(Params.BRAND_ID.get_value(), feeds.get(getPosition()).getSelfie_id());
                    startActivity(i);
                }

            }
        }

        @Override
        public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
            final Feed feed = feeds.get(i);

            if (feed.isIs_brand()) {
                viewHolder.status.setText("");
                viewHolder.time.setText("");
                viewHolder.rate.setVisibility(View.GONE);
                viewHolder.comment.setVisibility(View.GONE);
                viewHolder.time_indicator.setVisibility(View.GONE);
                viewHolder.comment_count.setText("");
                viewHolder.rate_count.setText("");

              /*  viewHolder.image.f(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (feed.isIs_brand()) {
                            Intent i = new Intent(Activity_Feeds.this, Activity_Brand_Feeds.class);
                            i.putExtra(Params.BRAND_ID.get_value(), feed.getSelfie_id());
                            startActivity(i);
                        }
                    }
                });*/

            } else {
                viewHolder.rate.setVisibility(View.VISIBLE);
                viewHolder.comment.setVisibility(View.VISIBLE);
                viewHolder.time_indicator.setVisibility(View.VISIBLE);
                viewHolder.image.setScaleType(ImageView.ScaleType.FIT_XY);

                final User user = db.Get_User_Details(feed.getUser_id());

              /*  if (user != null) {
                    if (user.getUserid() == App.getMyId())
                        viewHolder.dp.draw_border(true);
                    else
                        viewHolder.dp.draw_border(false);
                    viewHolder.name.setText(user.getF_name());

                    display_image(user.getImage(),viewHolder.dp,true);
                }*/

                String status = feed.getStatus();
                if(status != null && status.length() > 0) {
                    status = status.replaceAll("(#[A-Za-z0-9_-]+)",
                            "<font color='#2980b9'>" + "$0" + "</font>");

                }
                else
                {
                    status = "";
                }
                viewHolder.status.setText(Html.fromHtml(status));

                viewHolder.time.setText(C.getDateDifference(C.UTC_to_Local(feed.getTime())));

                if (feed.getRating_count() > 0)
                    viewHolder.rate_count.setText(String.valueOf(feed.getRating()));
                else
                    viewHolder.rate_count.setText("");

                if (feed.getMy_rating() > 0) {
                    viewHolder.rate.setBackgroundResource(R.drawable.like_red);
                } else {
                    viewHolder.rate.setBackgroundResource(R.drawable.like_normal);
                }

                if (feed.getComment_count() > 0)
                    viewHolder.comment_count.setText(String.valueOf(feed.getComment_count()));
                else
                    viewHolder.comment_count.setText("");



                viewHolder.rate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (wheel_layout != null)
                            root.removeView(wheel_layout);

                        wheel_layout = LayoutInflater.from(getApplicationContext()).inflate(R.layout.wheel, null, true);
                        ll_wheel = (RelativeLayout) wheel_layout.findViewById(R.id.ll_wheel);
                        ll_wheel.setOnClickListener(Activity_Feeds.this);
                        wheelMenu = (WheelMenu) wheel_layout.findViewById(R.id.rate_wheel);
                        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                        wheel_layout.setLayoutParams(params);

                        root.addView(wheel_layout);


                        wheelMenu.setDivCount(10);
                        wheelMenu.setWheelImage(R.drawable.rating);
                        feed_position = i;
                        int rate = feeds.get(feed_position).getMy_rating();
                        if (rate == 0)
                            rate = 1;
                        wheelMenu.setAlternateTopDiv(rate);
                        toggle_rate(true);

                        wheelMenu.setWheelChangeListener(new WheelMenu.WheelChangeListener() {
                            @Override
                            public void onSelectionChange(WheelMenu v, int selectedPosition) {
                                toggle_rate(false);
                                C.alert(getApplicationContext(), String.valueOf(selectedPosition));
                                if (feed_position > -1) {
                                    if (feeds.get(feed_position).getMy_rating() != selectedPosition) {
                                        new Update_Rating(Activity_Feeds.this,feed_position, feeds.get(feed_position).getSelfie_id(), selectedPosition).execute();
                                        feeds.get(feed_position).setMy_rating(selectedPosition);
                                        feeds_adapter.notifyItemChanged(feed_position);

                                    }
                                }
                            }
                        });



                    }
                });

                viewHolder.comment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        feed_position = i;
                        camera.setVisibility(View.GONE);
                        open_comments();

                    }
                });

                /*viewHolder.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       if(!feed.isIs_brand())
                        {


                            Intent intent = new Intent(Activity_Feeds.this, Activity_Large_Image.class);

                            intent.putExtra(Params.IMAGE.get_value(),feeds.get(i).getImage_url());
                            intent.putExtra(Params.COMMENT_COUNT.get_value(),feeds.get(i).getComment_count());
                            intent.putExtra(Params.RATING_COUNT.get_value(),feeds.get(i).getRating_count());
                            startActivity(intent);
                        }

                    }
                });*/

                viewHolder.imgDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        feed_position = i;
                        //Log.d("Getting feed", Integer.toString(feed_position));
                       // Log.d("Selfie Id", Integer.toString((int)feed.getSelfie_id()));
                       // delete_popup((int)feed.getSelfie_id(), feed_position);
                    }
                });

                viewHolder.flag.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Picasso.with(getApplicationContext()).load(feed.getImage_url()).into(viewHolder.image);
                        Uri bmpUri = getLocalBitmapUri(viewHolder.image);
                        if (bmpUri != null) {
                            // Construct a ShareIntent with link to image
                            Intent shareIntent = new Intent();
                            shareIntent.setAction(Intent.ACTION_SEND);
                            shareIntent.putExtra(Intent.EXTRA_TEXT, "Posted By Deepak Jaikalyani");
                            shareIntent.putExtra(Intent.EXTRA_TITLE, "Title By Deepak Jaikalyani");
                            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject By Deepak Jaikalyani");
                            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                            shareIntent.setType("image/*");
                            // Launch sharing dialog for image
                            startActivity(Intent.createChooser(shareIntent, "Share Image"));
                        } else {
                            Log.d("Sharing Failed", "From Intent");
                        }
                    }
                });



            }


            //Logic for random color
            //int idx = new Random().nextInt(richColors.length);
            int randomColor = (R.color.white);
            viewHolder.image.setBackgroundColor(getResources().getColor(randomColor));

            // Displaying main image
            display_image(feed.getImage_url(),viewHolder.image,false);
            //int loader = R.drawable.ic_action_person;
            //imageLoader.DisplayImage(feed.getImage_url(), loader, viewHolder.image);



        }
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.item_feeds, viewGroup, false);

            return new ViewHolder(itemView);
        }
    }


    private void open_comments()
    {
        User user = db.Get_User_Details(feeds.get(feed_position).getUser_id());
        if(user != null)
        {
            rate_list.setAdapter(new Rating_Adapter(getApplicationContext(),feeds.get(feed_position).getUsers_rating()));
            comment_adapter = new Comment_Adapter(Activity_Feeds.this, feeds.get(feed_position).getUsers_comment());
            comment_list.setAdapter(comment_adapter);
            if(comment_adapter.getItemCount() > 0) {
                no_comments.setVisibility(View.GONE);
                comment_list.smoothScrollToPosition(comment_adapter.getItemCount() - 1);

            }
            else {
                no_comments.setVisibility(View.VISIBLE);

            }


            rate_count.setText("Likes ("+String.valueOf(feeds.get(feed_position).getRating_count())+")");
            camera.setVisibility(View.GONE);
            toggle_menu(true,false);
        }
    }




        //////////////////////////////////////

}
