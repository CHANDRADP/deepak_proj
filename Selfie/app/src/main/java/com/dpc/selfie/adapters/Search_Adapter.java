package com.dpc.selfie.adapters;

/**
 * Created by Deepak on 13-07-2015.
 */
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.DB;
import com.dpc.selfie.Utils.JSONParser;
import com.dpc.selfie.customs.CircleImage;
import com.dpc.selfie.customs.MyProgress;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.customs.TextB;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.interfaces.OnCardClickListener;
import com.dpc.selfie.models.Celebrity;
import com.dpc.selfie.models.SearchData;
import com.dpc.selfie.models.Server_Params;

import com.dpc.selfie.stickyHeader.RecyclerItemClickListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Search_Adapter extends RecyclerView.Adapter<Search_Adapter.MyviewHolder> {

    private View v;
    private ArrayList<SearchData> search_results = new ArrayList<>();
    private Activity c;
    private int selected_posi = 0;
    private OnCardClickListener onCardClickListener;



    public void setOnCardClickListener(OnCardClickListener onCardClickListener)
    {
        this.onCardClickListener = onCardClickListener;
    }

    public Search_Adapter(Context context, ArrayList<SearchData> searchdata)
    {

        search_results = searchdata;
        this.search_results = searchdata;
    }

    @Override
    public MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search, null);
        MyviewHolder holder = new MyviewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyviewHolder holder, int position) {

        SearchData current = search_results.get(position);
        display_image(current.getImage(),holder.image, true);
        holder.name.setText(current.getF_name()+" "+current.getL_name());
        holder.status.setText(current.getStatus());


    }

    @Override
    public int getItemCount() {
        return search_results.size();
    }




    class MyviewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {

        TextB name;
        Text status;
        CircleImage image;
        ImageView invite;

        public MyviewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            name = (TextB) itemView.findViewById(R.id.name);
            status = (Text) itemView.findViewById(R.id.status);
            image = (CircleImage) itemView.findViewById(R.id.image);
            invite = (ImageView) itemView.findViewById(R.id.invite);
        }


        @Override
        public void onClick(View v) {
            selected_posi = getPosition();
            notifyDataSetChanged();
            if(onCardClickListener != null)
            {
                onCardClickListener.onClick(v,selected_posi);

            }
        }
    }

    protected void display_image(String url,View v,boolean is_circle_image)
    {

        try {
            Picasso.with(c)
                    .load(url)
                    .placeholder(R.drawable.ic_action_person)
                    .error(R.drawable.ic_action_person)
                    .into(is_circle_image ? (CircleImage)v : (ImageView)(v));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

}
