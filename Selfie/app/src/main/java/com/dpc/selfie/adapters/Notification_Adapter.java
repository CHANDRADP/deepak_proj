package com.dpc.selfie.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.DB;
import com.dpc.selfie.customs.CircleImage;
import com.dpc.selfie.customs.Text;
import com.dpc.selfie.customs.TextB;
import com.dpc.selfie.interfaces.OnCardClickListener;
import com.dpc.selfie.models.Notification;
import com.dpc.selfie.models.User;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by D-P-C on 24-Mar-15.
 */
public class Notification_Adapter extends RecyclerView.Adapter<Notification_Adapter.UserViewHolder> {

    private ArrayList<Notification> notifications = new ArrayList<>();

    private OnCardClickListener onCardClickListener;

    private DB db;

    private Context context;

    public Notification_Adapter(Context context,ArrayList<Notification> notifications) {
        this.context = context;
        this.notifications = notifications;

        db = new DB(context);
    }

    public void setOnCardClickListener(OnCardClickListener onCardClickListener)
    {
        this.onCardClickListener = onCardClickListener;
    }
    @Override
    public int getItemCount() {
        return notifications.size();
    }
    @Override
    public void onBindViewHolder(UserViewHolder userViewHolder, int i) {
        Notification notification = notifications.get(i);
        User user = db.Get_User_Details(notification.getUserid());
        display_image(user.getImage(),userViewHolder.image,true);
        userViewHolder.name.setText(user.getDisplay_name());
        String content = "Posted a selfie";
        if(notification.getType() == 2)
            content = "Commented on a selfie";
        else if(notification.getType() == 3)
            content = "rated on your selfie";
        userViewHolder.content.setText(content);

        userViewHolder.time.setText(C.getDateDifference(C.UTC_to_Local(notification.getTime())));

        if(notification.isRead_status() < 0)
        {
            userViewHolder.read_status.setBackgroundResource(R.drawable.arrow_next_blue);
        }
        else
        {
            userViewHolder.read_status.setBackgroundResource(R.drawable.arrow_next_grey);
        }

        userViewHolder.read_status.setVisibility(View.VISIBLE);
        userViewHolder.time.setVisibility(View.VISIBLE);



    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup viewGroup,final int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_user, viewGroup, false);


        return new UserViewHolder(itemView);
    }


    public class UserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        protected TextB name;
        protected Text content,time;
        protected CircleImage image;
        ImageView read_status;

        public UserViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            name =  (TextB) v.findViewById(R.id.name);
            content = (Text)  v.findViewById(R.id.status);
            time = (Text)  v.findViewById(R.id.time);
            image = (CircleImage)  v.findViewById(R.id.image);
            read_status = (ImageView) v.findViewById(R.id.invite);

        }

        @Override
        public void onClick(View v) {
            onCardClickListener.onClick(v,getPosition());
            notifications.get(getPosition()).setRead_status(1);
            notifyItemChanged(getPosition());
        }
    }

    private void display_image(String url,View v,boolean is_circle_image)
    {

        try {
            Picasso.with(context)
                    .load(url)
                    .placeholder(R.drawable.ic_action_person)
                    .error(R.drawable.ic_action_person)
                    .into(is_circle_image ? (CircleImage)v : (ImageView)(v));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

}