package com.dpc.selfie.models;

import android.graphics.Bitmap;
import android.graphics.Point;

/**
 * Created by D-P-C on 14-Mar-15.
 */
public class Editor {
    private Bitmap bitmap;
    private int main_position = 0,sub_position = 0,value = 0, frame_id;
    private String txt = "";
    private Point point;

    public Editor(int main_position, int sub_position, int value, int frame_id, String txt,Point point) {
        this.main_position = main_position;
        this.sub_position = sub_position;
        this.value = value;
        this.frame_id = frame_id;
        this.txt = txt;
        this.point = point;
    }



    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public int getMain_position() {
        return main_position;
    }

    public void setMain_position(int main_position) {
        this.main_position = main_position;
    }

    public int getSub_position() {
        return sub_position;
    }

    public void setSub_position(int sub_position) {
        this.sub_position = sub_position;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getFrame_id() {
        return frame_id;
    }

    public void setFrame_id(int frame_id) {
        this.frame_id = frame_id;
    }

    public String getTxt() {
        return txt;
    }

    public void setTxt(String txt) {
        this.txt = txt;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }


    public String toString() {
        return "main : "+main_position+" sub : "+sub_position+" seek : "+value+" frame : "+frame_id+" text :"+txt;
    }
}
