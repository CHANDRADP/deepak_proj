package com.dpc.selfie.enums;

/**
 * Created by D-P-C on 17-Feb-15.
 */
////Server process with given data
public enum Process {
    NONE("none"),
    ADD("add"),
    UPDATE("update"),
    GET("get"),
    DELETE("delete"),

    ADD_USER("add_u"),
    ADD_IMAGE("add_i"),
    ADD_BADGE("add_b"),
    ADD_RATING("add_r"),
    ADD_COMMENT("add_c"),
    ADD_FOLLOW("add_f"),


    UPDATE_USER("update_u"),
    UPDATE_USER_FRIENDS("update_u_friends"),
    UPDATE_IMAGE("update_i"),
    UPDATE_BADGE("update_b"),
    UPDATE_TAGS("update_t"),
    UPDATE_RATING("update_r"),
    UPDATE_FOLLOW("update_f"),
    UPDATE_COMMENT("update_c"),

    DELETE_USER("delete_u"),
    DELETE_IMAGE("delete_i"),
    DELETE_FOLLOW("delete_f"),
    DELETE_COMMENT("delete_c"),

    GET_ALL_USERS("get_all_user_info"),
    GET_ALL_FEEDS("get_selfie_feeds"),
    GET_BRAND_FEEDS("get_brand_feeds"),
    GET_ALL_FB_USERS("get_all_fb_user_info"),
    GET_USER_TIMELINE("get_user_timeline"),
    GET_SELFIE_INFO("get_selfie_info"),
    GET_CELEBRITIES("get_celebrities"),
    GET_SEARCHED_USER("get_searched_user"),
    GET_BRAND_CATEGORY("get_brand_category"),
    GET_BRAND_OFFERS("get_brand_offers"),
    GET_BRAND_FRAMES("get_brand_frames")
    ;

    // Constructor
    private Process(final String value) {
        this.value = value;
    }

    // Internal state
    private String value;

    public String get_value() {
        return value;
    }
}
