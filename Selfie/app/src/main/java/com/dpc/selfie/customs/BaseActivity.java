package com.dpc.selfie.customs;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.SpannableString;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.dpc.selfie.App;
import com.dpc.selfie.R;
import com.dpc.selfie.Utils.C;
import com.dpc.selfie.Utils.DB;
import com.dpc.selfie.Utils.Net;
import com.dpc.selfie.enums.Params;
import com.dpc.selfie.tasks.Update_GCM_Task;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionDefaultAudience;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.facebook.widget.FacebookDialog;
import com.google.android.gcm.GCMRegistrar;

import com.squareup.picasso.LruCache;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;


/**
 * Created by D-P-C on 30-Jan-15.
 */
public class BaseActivity extends ActionBarActivity {


    protected static LruCache lruCache;

    protected static Picasso picasso;

    protected static final List<String> PERMISSIONS = Arrays
            .asList("publish_actions");


    protected boolean pending_publish_permissions = false;

    protected UiLifecycleHelper uiHelper;

    protected Net net;

    protected DB db;

    protected Session fb_session = null;


    protected FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
        @Override
        public void onError(FacebookDialog.PendingCall pendingCall,
                            Exception error, Bundle data) {
            Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
        }

        @Override
        public void onComplete(FacebookDialog.PendingCall pendingCall,
                               Bundle data) {
            Log.d("HelloFacebook", "Success!");
        }
    };
    protected Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state,
                         Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    private void onSessionStateChange(Session session, SessionState state,
                                      Exception exception) {
        if (state.isOpened()) {
            App.set_fb_login(true);
            if (pending_publish_permissions
                    && state.equals(SessionState.OPENED_TOKEN_UPDATED)) {
                pending_publish_permissions = false;
                set_pending_publish_permissions(pending_publish_permissions);
               publishStory(null);
            }
        }
        else
            App.set_fb_login(false);
    }
    public void set_pending_publish_permissions( boolean value)
    {
        SharedPreferences APP_DATA = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = APP_DATA.edit();
        editor.putBoolean(C.PENDING_PUBLISH_KEY, value);
        editor.commit();
    }
    public boolean get_pending_publish_permissions()
    {
        SharedPreferences APP_DATA = PreferenceManager.getDefaultSharedPreferences(this);
        return APP_DATA.getBoolean(C.PENDING_PUBLISH_KEY,false);

    }
    public void setCustomTitle(String title)
    {
        SpannableString s = new SpannableString(title);
      //  s.setSpan(new TypefaceSpan(this, "Proximab.otf"), 0, s.length(),
       //         Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        // Update the action bar title with the TypefaceSpan instance
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(s);


    }

    protected boolean is_session_active()
    {

        if(fb_session == null)
            fb_session = Session.getActiveSession();
        if(fb_session != null)
        {
            return true;
        }
        return false;

    }

    protected void print(Context context,String msg)
    {
        Log.d(context.getClass().getSimpleName(),msg);
    }
    @Override
    protected void onStop() {
        super.onStop();
        if(uiHelper != null)
        uiHelper.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(lruCache == null)
        lruCache = new LruCache(getApplicationContext());
        if(picasso == null)
        picasso = new Picasso.Builder(getApplicationContext()).memoryCache(lruCache).build();
            pending_publish_permissions =
                    get_pending_publish_permissions();

        db = new DB(this);
        net = new Net(getApplicationContext());
        if(!App.is_gcm_updated() && App.get_gcm_id() != null)
        {
            new Update_GCM_Task(this,App.get_gcm_id()).execute();
        }

    }
    public static void clear_picasso_cache()
    {
        if(lruCache != null)
        {
            lruCache.clear();
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(uiHelper != null)
        uiHelper.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(uiHelper != null)
        uiHelper.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }


    @Override
    public FragmentManager getSupportFragmentManager() {
        return super.getSupportFragmentManager();
    }

    @Override
    public void finish() {
        super.finish();

    }
    protected boolean pendingPublishReauthorization = false;
    private void request_permissions()
    {
        Session session = Session.getActiveSession();
        List<String> permissions = session.getPermissions();
        if (!permissions.contains(PERMISSIONS)) {
            pendingPublishReauthorization = true;
            set_pending_publish_permissions(pending_publish_permissions);
            requestPublishPermissions(session);
            return;
        }
    }
    private static final int REAUTH_ACTIVITY_CODE = 100;

    private void requestPublishPermissions(Session session) {
        if (session != null) {
            Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(this, PERMISSIONS)
                    // demonstrate how to set an audience for the publish permissions,
                    // if none are set, this defaults to FRIENDS
                    .setDefaultAudience(SessionDefaultAudience.FRIENDS)
                    .setRequestCode(REAUTH_ACTIVITY_CODE);
            session.requestNewPublishPermissions(newPermissionsRequest);
        }
    }
    ///share app on facebook timeline
    protected void publishStory(String image) {
        Session session = Session.getActiveSession();

        if (session != null) {

            // Check for publish permissions
            List<String> permissions = session.getPermissions();
            if (!isSubsetOf(PERMISSIONS, permissions) && !pending_publish_permissions) {
                pending_publish_permissions = true;
                set_pending_publish_permissions(pending_publish_permissions);
                Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(
                        this, PERMISSIONS);
                session.requestNewPublishPermissions(newPermissionsRequest);
                return;
            }
            else if(image != null)
            {

                post_selfie_on_fb(image);
            }


        }

    }

    private boolean isSubsetOf(Collection<String> subset,
                               Collection<String> superset) {
        for (String string : subset) {
            if (!superset.contains(string)) {
                return false;
            }
        }
        return true;
    }

    public void generate_SHA_key_for_facebook()
    {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.dpc.selfie",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
    ////////////////////common functions
    /**
     * Check whether the camera is available or not
     * */
    public boolean isCameraAvailable() {
        if (getApplicationContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            return true;
        } else {
            return false;
        }
    }
    private String gcmId = null;
    protected void register()
    {
        gcmId = GCMRegistrar.getRegistrationId(this.getApplicationContext());
        if (gcmId.equals("") || gcmId == null)
        {
            GCMRegistrar.register(this, C.SENDER_ID);
        }
        else
        {

            final Context context = this.getApplicationContext();
            if (GCMRegistrar.isRegisteredOnServer(this.getApplicationContext()))
            {

                gcmId = GCMRegistrar.getRegistrationId(this.getApplicationContext());
                new Update_GCM_Task(getApplicationContext(),gcmId);
                App.update_gcm_id(gcmId);
            }
            else
            {

                GCMRegistrar.setRegisteredOnServer(context, true);

            }

        }
        Log.i("gcmId", "Device registered: regId = " + gcmId);
    }

    public void post_selfie_on_fb(String picture) {
        final Session session = Session.getActiveSession();
        Bundle postParams = new Bundle();
        postParams.putString("name", "chandra");
        postParams.putString("caption", "testing");
        postParams.putString("description", "description");
        postParams.putString("link", "http://www.wtp.club");
        postParams.putString("picture", picture);
        //postParams.putString("place", "155021662189");

        Request.Callback callback= new Request.Callback() {
            public void onCompleted(Response response) {
                GraphObject graphResponse = response
                        .getGraphObject();

                if(graphResponse != null)
                {
                    JSONObject obj = graphResponse.getInnerJSONObject();
                    String postId = null;
                    try {
                        postId = obj.getString("id");
                        if(postId != null)
                        {
                            C.alert(getApplicationContext(), "Posted successfully");
                        }

                    } catch (JSONException e) {
                        Log.i("fb",
                                "JSON error "+ e.getMessage());
                    }
                    FacebookRequestError error = response.getError();
                    if (error != null) {
                        C.alert(getApplicationContext(), "error in sharing,please try after some time");
                    }
                }

            }
        };

        final Request request = new Request(session, "me/feed", postParams,
                HttpMethod.POST, callback);
        RequestAsyncTask task = new RequestAsyncTask(request);
        task.execute();



    }

    protected void display_image(String url,View v,boolean is_circle_image)
    {

        try {
            picasso.with(getApplicationContext())
                    .load(url)
                    .placeholder(null)
                    .error(R.drawable.ic_action_person)
                    .into(is_circle_image ? (CircleImage)v : (ImageView)(v));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
