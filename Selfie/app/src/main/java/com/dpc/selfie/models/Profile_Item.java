package com.dpc.selfie.models;

/**
 * Created by D-P-C on 11-Mar-15.
 */
public class Profile_Item {
    private int image;
    private long id = -1;
    private String image_url;

    public Profile_Item(int image, String image_url) {
        this.image = image;
        this.image_url = image_url;
    }

    public Profile_Item(long id, String image_url) {
        this.id = id;
        this.image_url = image_url;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
