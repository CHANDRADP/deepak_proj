package com.dpc.selfie.models;

/**
 * Created by Deepak on 23-07-2015.
 */
public class BrandFrames {

    private long frame_id;
    private String frame_url;

    public BrandFrames(long frame_id, String frame_url) {
        this.frame_id = frame_id;
        this.frame_url = frame_url;
    }

    public long getFrame_id() {
        return frame_id;
    }

    public void setFrame_id(long frame_id) {
        this.frame_id = frame_id;
    }

    public String getFrame_url() {
        return frame_url;
    }

    public void setFrame_url(String frame_url) {
        this.frame_url = frame_url;
    }
}
