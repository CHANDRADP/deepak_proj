package com.dpc.selfie.Utils;

import android.content.ContentValues;
import android.content.Context;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import com.dpc.selfie.App;

import com.dpc.selfie.enums.Params;
import com.dpc.selfie.models.BrandOffers;
import com.dpc.selfie.models.Celebrity;
import com.dpc.selfie.models.MyBrands;
import com.dpc.selfie.models.Notification;
import com.dpc.selfie.models.User;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by D-P-C on 31-Jan-15.
 */
public class DB extends SQLiteOpenHelper {

    private static final String DB_NAME = "self_db";

    private static int DATABASE_VERSION = 1;

    private final String TABLE_USER = "self_users";

    private final String TABLE_NOTIFICATION = "self_notify";

    private final String TABLE_CELEBRITY = "self_celebrity";



    public DB(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);


    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
            onCreate(db);
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_APP_USERS_TABLE = "CREATE TABLE IF NOT EXISTS "+ TABLE_USER +"(" +
                Params.ID.get_value()+" INTEGER PRIMARY KEY AUTOINCREMENT," +
                Params.USER_ID.get_value()+" INTEGER," +
                Params.FB_ID.get_value()+" INTEGER," +
                Params.FIRST_NAME.get_value()+" TEXT DEFAULT NULL," +
                Params.MIDDLE_NAME.get_value()+" TEXT DEFAULT NULL," +
                Params.LAST_NAME.get_value()+" TEXT DEFAULT NULL," +
                Params.DISPLAY_NAME.get_value()+" TEXT DEFAULT NULL," +
                Params.NUMBER.get_value()+" TEXT DEFAULT NULL," +
                Params.EMAIL.get_value()+" TEXT NOT NULL," +
                Params.DOB.get_value()+" TEXT DEFAULT NULL," +
                Params.GENDER.get_value()+" INTEGER NOT NULL," +
                Params.STATUS.get_value()+" TEXT DEFAULT NULL," +
                Params.POINTS.get_value()+" INTEGER DEFAULT 0," +
                Params.BADGES.get_value()+" TEXT DEFAULT NULL," +
                Params.THUMBNAIL.get_value()+" TEXT DEFAULT NULL," +
                Params.IMAGE.get_value()+" TEXT DEFAULT NULL," +
                Params.DATE.get_value()+" NUMERIC NOT NULL" +
                ")" ;

        String CREATE_CELEBRITY_TABLE = "CREATE TABLE IF NOT EXISTS "+ TABLE_CELEBRITY +"(" +
                Params.ID.get_value()+" INTEGER PRIMARY KEY AUTOINCREMENT," +
                Params.USER_ID.get_value()+" INTEGER," +
                Params.FIRST_NAME.get_value()+" TEXT DEFAULT NULL," +
                Params.MIDDLE_NAME.get_value()+" TEXT DEFAULT NULL," +
                Params.LAST_NAME.get_value()+" TEXT DEFAULT NULL," +
                Params.DOB.get_value()+" TEXT DEFAULT NULL," +
                Params.GENDER.get_value()+" INTEGER NOT NULL," +
                Params.STATUS.get_value()+" TEXT DEFAULT NULL," +
                Params.IMAGE.get_value()+" TEXT DEFAULT NULL," +
                Params.PROFESSION.get_value()+" TEXT DEFAULT NULL," +
                Params.DATE.get_value()+" NUMERIC NOT NULL" +
                ")" ;

        String CREATE_NOTIFICATION_TABLE = "CREATE TABLE IF NOT EXISTS "+ TABLE_NOTIFICATION +"(" +
                Params.ID.get_value()+" INTEGER PRIMARY KEY AUTOINCREMENT," +
                Params.TYPE.get_value()+" INTEGER," +
                Params.USER_ID.get_value()+" INTEGER," +
                Params.IMAGE_ID.get_value()+" INTEGER," +
                Params.CONTENT.get_value()+" TEXT DEFAULT NULL," +
                Params.READ_STATUS.get_value()+" INTEGER DEFAULT -1," +
                Params.DATE.get_value()+" NUMERIC NOT NULL" +
                ")" ;

        db.execSQL(CREATE_APP_USERS_TABLE);
        db.execSQL(CREATE_CELEBRITY_TABLE);
        db.execSQL(CREATE_NOTIFICATION_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIFICATION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CELEBRITY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);

    }

    public void Erase_DB() {
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIFICATION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CELEBRITY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);

        db.close();
    }
    /////////////////////DB functions
    public long Sync_App_User(User u)
    {

        long insert = -1;
        SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(Params.USER_ID.get_value(), String.valueOf(u.getUserid()));
            values.put(Params.FB_ID.get_value(), String.valueOf(u.getFb_id()));
            values.put(Params.FIRST_NAME.get_value(), u.getF_name());
            values.put(Params.LAST_NAME.get_value(), u.getL_name());
            values.put(Params.DISPLAY_NAME.get_value(), u.getDisplay_name());
        if (u.getNumber() != null)
            values.put(Params.NUMBER.get_value(), u.getNumber());
            values.put(Params.EMAIL.get_value(), u.getEmail());
            values.put(Params.DOB.get_value(), u.getDob());
            values.put(Params.GENDER.get_value(),u.getGenderValue());
            if(u.getStatus() != null)
            values.put(Params.STATUS.get_value(), u.getStatus());
            values.put(Params.POINTS.get_value(), u.getPoints());
        if (u.getThumbnail() != null)
            values.put(Params.THUMBNAIL.get_value(), u.getThumbnail());
        if (u.getImage() != null)
            values.put(Params.IMAGE.get_value(), u.getImage());
            values.put(Params.DATE.get_value(), u.getDate());
            if(u.getBadges() != null)
            values.put(Params.BADGES.get_value(), u.getBadges());
            Cursor c = db.query(TABLE_USER, null, Params.USER_ID.get_value()+"= ?", new String[]{String.valueOf(u.getUserid())}, null, null, null);
            if(c != null)
            {
                if(c.moveToNext())
                {
                    if(c.getCount() == 0) {
                        insert = db.insertOrThrow(TABLE_USER, null, values);
                    }
                    else
                    {
                        insert = db.update(TABLE_USER, values, Params.USER_ID.get_value() + " = ?",
                                new String[] { String.valueOf(u.getUserid()) });
                    }
                }
                else
                {
                    insert = db.insertOrThrow(TABLE_USER, null, values);
                }

                c.close();
            }

         db.close();
        return insert;
    }
    public long Sync_App_Users(ArrayList<User> users)
    {

       	long insert = -1;
        SQLiteDatabase db = this.getWritableDatabase();
        for(int i = 0 ; i < users.size() ; i++)
        {
            User u = users.get(i);
            ContentValues values = new ContentValues();
            values.put(Params.USER_ID.get_value(), String.valueOf(u.getUserid()));
            values.put(Params.FB_ID.get_value(), String.valueOf(u.getFb_id()));
            values.put(Params.FIRST_NAME.get_value(), u.getF_name());
            values.put(Params.LAST_NAME.get_value(), u.getL_name());
            values.put(Params.DISPLAY_NAME.get_value(), u.getDisplay_name());
            if (u.getNumber() != null)
                values.put(Params.NUMBER.get_value(), u.getNumber());
            values.put(Params.EMAIL.get_value(), u.getEmail());
            values.put(Params.DOB.get_value(), u.getDob());
            values.put(Params.GENDER.get_value(),u.getGenderValue());
            if(u.getStatus() != null)
                values.put(Params.STATUS.get_value(), u.getStatus());
            values.put(Params.POINTS.get_value(), u.getPoints());
            if (u.getThumbnail() != null)
                values.put(Params.THUMBNAIL.get_value(), u.getThumbnail());
            if (u.getImage() != null)
                values.put(Params.IMAGE.get_value(), u.getImage());
            values.put(Params.DATE.get_value(), u.getDate());
            if(u.getBadges() != null)
                values.put(Params.BADGES.get_value(), u.getBadges());
            Cursor c = db.query(TABLE_USER, null, Params.USER_ID.get_value()+"= ?", new String[]{String.valueOf(u.getUserid())}, null, null, null);
            if(c != null)
            {
                if(c.moveToLast())
                {
                    if(c.getCount() == 0) {
                        insert = db.insert(TABLE_USER, null, values);
                    }
                    else
                    {
                        insert = db.update(TABLE_USER, values, Params.USER_ID.get_value() + " = ?",
                                new String[] { String.valueOf(u.getUserid()) });
                    }
                }
                else
                {
                    insert = db.insert(TABLE_USER, null, values);
                }

                c.close();
            }



        }
        if(db != null)
        db.close();
        return insert;
    }

    public ArrayList<MyBrands> get_all_brand_details()
    {
        ArrayList<MyBrands> brandlist = new ArrayList<MyBrands>();
        SQLiteDatabase db = this.getReadableDatabase();

        MyBrands brand1 = new MyBrands("0","Duracell",20);
        MyBrands brand2 = new MyBrands("1","Pizza Hut",15);
        MyBrands brand3 = new MyBrands("2","Dominos",30);
        MyBrands brand4 = new MyBrands("3","Nestle",12);
        MyBrands brand5 = new MyBrands("4","Apple",5);
        MyBrands brand6 = new MyBrands("5","Kelloggs",20);
        MyBrands brand7 = new MyBrands("6","Panasonic",21);
        MyBrands brand8 = new MyBrands("7","Disney",60);
        brandlist.add(brand1);
        brandlist.add(brand2);
        brandlist.add(brand3);
        brandlist.add(brand4);
        brandlist.add(brand5);
        brandlist.add(brand6);
        brandlist.add(brand7);
        brandlist.add(brand8);

        //db.close();
        return brandlist;
    }

    public ArrayList<BrandOffers> get_all_offers()
    {
        ArrayList<BrandOffers> brandlist = new ArrayList<BrandOffers>();
        SQLiteDatabase db = this.getReadableDatabase();

        BrandOffers offer1 = new BrandOffers("5 Rs. off",5);
        BrandOffers offer2 = new BrandOffers("10 Rs. off",10);
        BrandOffers offer3 = new BrandOffers("20% off",20);
        BrandOffers offer4 = new BrandOffers("30 Rs. off",30);
        BrandOffers offer5 = new BrandOffers("40% off",40);
        BrandOffers offer6 = new BrandOffers("50 Rs. off",50);
        BrandOffers offer7 = new BrandOffers("15 Rs. off",15);
        BrandOffers offer8 = new BrandOffers("25% off",25);
        BrandOffers offer9 = new BrandOffers("35 Rs. off",35);
        BrandOffers offer10 = new BrandOffers("45% off",45);

        brandlist.add(offer1);
        brandlist.add(offer2);
        brandlist.add(offer3);
        brandlist.add(offer4);
        brandlist.add(offer5);
        brandlist.add(offer6);
        brandlist.add(offer7);
        brandlist.add(offer8);
        brandlist.add(offer9);
        brandlist.add(offer10);


        //db.close();
        return brandlist;
    }




    public ArrayList<User> Get_App_Users()
    {
        ArrayList<User> users = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_USER, null, null, null, null, null, Params.FIRST_NAME.get_value() + " DESC");
        if(c != null)
        {
            while(c.moveToNext())
            {
                long id = c.getLong(c.getColumnIndex(Params.USER_ID.get_value()));
                if(id != App.getMyId())
                {

                    User contact = new User(
                            id,
                            c.getLong(c.getColumnIndex(Params.FB_ID.get_value())),
                            c.getInt(c.getColumnIndex(Params.GENDER.get_value())),
                            c.getInt(c.getColumnIndex(Params.POINTS.get_value())),
                            c.getString(c.getColumnIndex(Params.FIRST_NAME.get_value())),
                            c.getString(c.getColumnIndex(Params.MIDDLE_NAME.get_value())),
                            c.getString(c.getColumnIndex(Params.LAST_NAME.get_value())),
                            c.getString(c.getColumnIndex(Params.DISPLAY_NAME.get_value())),
                            c.getString(c.getColumnIndex(Params.NUMBER.get_value())),
                            c.getString(c.getColumnIndex(Params.EMAIL.get_value())),
                            c.getString(c.getColumnIndex(Params.DOB.get_value())),
                            c.getString(c.getColumnIndex(Params.STATUS.get_value())),
                            c.getString(c.getColumnIndex(Params.THUMBNAIL.get_value())),
                            c.getString(c.getColumnIndex(Params.IMAGE.get_value())),
                            c.getString(c.getColumnIndex(Params.DATE.get_value())),
                            c.getString(c.getColumnIndex(Params.BADGES.get_value()))
                    );
                    users.add(contact);
                }

            }
            c.close();
        }
       db.close();
        return users;
    }

    public HashMap<Long,User> Get_App_Users_Hashmap()
    {
        HashMap<Long,User> users = new HashMap<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_USER, null, null, null, null, null, Params.FIRST_NAME.get_value() + " DESC");
        if(c != null)
        {
            while(c.moveToNext())
            {
                if(c.getLong(2) != (App.getMyId()))
                {

                    User contact = new User(
                            c.getLong(c.getColumnIndex(Params.USER_ID.get_value())),
                            c.getLong(c.getColumnIndex(Params.FB_ID.get_value())),
                            c.getInt(c.getColumnIndex(Params.GENDER.get_value())),
                            c.getInt(c.getColumnIndex(Params.POINTS.get_value())),
                            c.getString(c.getColumnIndex(Params.FIRST_NAME.get_value())),
                            c.getString(c.getColumnIndex(Params.MIDDLE_NAME.get_value())),
                            c.getString(c.getColumnIndex(Params.LAST_NAME.get_value())),
                            c.getString(c.getColumnIndex(Params.DISPLAY_NAME.get_value())),
                            c.getString(c.getColumnIndex(Params.NUMBER.get_value())),
                            c.getString(c.getColumnIndex(Params.EMAIL.get_value())),
                            c.getString(c.getColumnIndex(Params.DOB.get_value())),
                            c.getString(c.getColumnIndex(Params.STATUS.get_value())),
                            c.getString(c.getColumnIndex(Params.THUMBNAIL.get_value())),
                            c.getString(c.getColumnIndex(Params.IMAGE.get_value())),
                            c.getString(c.getColumnIndex(Params.DATE.get_value())),
                            c.getString(c.getColumnIndex(Params.BADGES.get_value()))
                    );
                    users.put(c.getLong(c.getColumnIndex(Params.FB_ID.get_value())),contact);
                }

            }
            c.close();
        }
        db.close();
        return users;
    }
    public User Get_User_Details(long id)
    {
        User user = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_USER,null, Params.USER_ID.get_value() + " = ?", new String[] { String.valueOf(id) }, null, null, null);
        if(c != null)
        {
            if(c.moveToNext())
            {
                 user = new User(
                            c.getLong(c.getColumnIndex(Params.USER_ID.get_value())),
                            c.getLong(c.getColumnIndex(Params.FB_ID.get_value())),
                            c.getInt(c.getColumnIndex(Params.GENDER.get_value())),
                            c.getInt(c.getColumnIndex(Params.POINTS.get_value())),
                            c.getString(c.getColumnIndex(Params.FIRST_NAME.get_value())),
                            c.getString(c.getColumnIndex(Params.MIDDLE_NAME.get_value())),
                            c.getString(c.getColumnIndex(Params.LAST_NAME.get_value())),
                            c.getString(c.getColumnIndex(Params.DISPLAY_NAME.get_value())),
                            c.getString(c.getColumnIndex(Params.NUMBER.get_value())),
                            c.getString(c.getColumnIndex(Params.EMAIL.get_value())),
                            c.getString(c.getColumnIndex(Params.DOB.get_value())),
                            c.getString(c.getColumnIndex(Params.STATUS.get_value())),
                            c.getString(c.getColumnIndex(Params.THUMBNAIL.get_value())),
                            c.getString(c.getColumnIndex(Params.IMAGE.get_value())),
                            c.getString(c.getColumnIndex(Params.DATE.get_value())),
                            c.getString(c.getColumnIndex(Params.BADGES.get_value()))
                    );


            }
            c.close();
        }
        db.close();
        return user;
    }

    public boolean is_user_exist(long fb_id)
    {
        boolean user = false;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_USER,null, Params.FB_ID.get_value() + " = ?", new String[] { String.valueOf(fb_id) }, null, null, null);
        if(c != null)
        {
            if(c.moveToNext())
            {
                user = true;

            }
            c.close();
        }
        db.close();
        return user;
    }
    /////////////////////////celebrity functions
    public long Sync_celebrities(ArrayList<Celebrity> celebrities)
    {

        long insert = -1;
        SQLiteDatabase db = this.getWritableDatabase();
        for(int i = 0 ; i < celebrities.size() ; i++)
        {
            Celebrity celebrity = celebrities.get(i);
            ContentValues values = new ContentValues();
            values.put(Params.USER_ID.get_value(), celebrity.getId());
            values.put(Params.FIRST_NAME.get_value(), celebrity.getF_name());
            values.put(Params.MIDDLE_NAME.get_value(), celebrity.getM_name());
            values.put(Params.LAST_NAME.get_value(), celebrity.getL_name());
            values.put(Params.DOB.get_value(), celebrity.getDob());
            values.put(Params.GENDER.get_value(),celebrity.getGender());
            if(celebrity.getStatus() != null)
                values.put(Params.STATUS.get_value(), celebrity.getStatus());
            if (celebrity.getImage() != null)
                values.put(Params.IMAGE.get_value(), celebrity.getImage());
            values.put(Params.DATE.get_value(), celebrity.getDate());

            Cursor c = db.query(TABLE_CELEBRITY, null, Params.USER_ID.get_value()+"= ?", new String[]{String.valueOf(celebrity.getId())}, null, null, null);
            if(c != null)
            {
                if(c.moveToLast())
                {
                    if(c.getCount() == 0) {
                        insert = db.insert(TABLE_CELEBRITY, null, values);
                    }
                    else
                    {
                        insert = db.update(TABLE_CELEBRITY, values, Params.USER_ID.get_value() + " = ?",
                                new String[] { String.valueOf(celebrity.getId()) });
                    }
                }
                else
                {
                    insert = db.insert(TABLE_CELEBRITY, null, values);
                }

                c.close();
            }



        }
        if(db != null)
            db.close();
        return insert;
    }

    public Celebrity Get_Celebrity_Details(long id)
    {
        Celebrity celebrity = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_CELEBRITY,null, Params.USER_ID.get_value() + " = ?", new String[] { String.valueOf(id) }, null, null, null);
        if(c != null)
        {
            if(c.moveToNext())
            {
                celebrity = new Celebrity(
                        c.getLong(c.getColumnIndex(Params.USER_ID.get_value())),
                        c.getString(c.getColumnIndex(Params.FIRST_NAME.get_value())),
                        c.getString(c.getColumnIndex(Params.MIDDLE_NAME.get_value())),
                        c.getString(c.getColumnIndex(Params.LAST_NAME.get_value())),
                        c.getString(c.getColumnIndex(Params.STATUS.get_value())),
                        c.getString(c.getColumnIndex(Params.DOB.get_value())),
                        c.getString(c.getColumnIndex(Params.IMAGE.get_value())),
                        c.getInt(c.getColumnIndex(Params.GENDER.get_value())),
                        0,
                        true,
                        c.getString(c.getColumnIndex(Params.PROFESSION.get_value())),
                        c.getString(c.getColumnIndex(Params.DATE.get_value()))
                        );


            }
            c.close();
        }
        db.close();
        return celebrity;
    }
    public ArrayList<Celebrity> Get_Celebrities()
    {
        ArrayList<Celebrity> celebrities = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_CELEBRITY, null, null, null, null, null, Params.FIRST_NAME.get_value() + " DESC");
        if(c != null)
        {
            while(c.moveToNext())
            {
                if(c.getLong(2) != (App.getMyId()))
                {

                    Celebrity  celebrity = new Celebrity(
                            c.getLong(c.getColumnIndex(Params.USER_ID.get_value())),
                            c.getString(c.getColumnIndex(Params.FIRST_NAME.get_value())),
                            c.getString(c.getColumnIndex(Params.MIDDLE_NAME.get_value())),
                            c.getString(c.getColumnIndex(Params.LAST_NAME.get_value())),
                            c.getString(c.getColumnIndex(Params.STATUS.get_value())),
                            c.getString(c.getColumnIndex(Params.DOB.get_value())),
                            c.getString(c.getColumnIndex(Params.IMAGE.get_value())),
                            c.getInt(c.getColumnIndex(Params.GENDER.get_value())),
                            0,
                            true,
                            c.getString(c.getColumnIndex(Params.PROFESSION.get_value())),
                            c.getString(c.getColumnIndex(Params.DATE.get_value()))
                    );
                    celebrities.add(celebrity);
                }

            }
            c.close();
        }
        db.close();
        return celebrities;
    }
    public HashMap<Long,Celebrity> Get_Celebrity_Hashmap()
    {
        HashMap<Long,Celebrity> celebrityHashMap = new HashMap<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_CELEBRITY, null, null, null, null, null, Params.FIRST_NAME.get_value() + " DESC");
        if(c != null)
        {
            while(c.moveToNext())
            {
                if(c.getLong(2) != (App.getMyId()))
                {

                    Celebrity  celebrity = new Celebrity(
                            c.getLong(c.getColumnIndex(Params.USER_ID.get_value())),
                            c.getString(c.getColumnIndex(Params.FIRST_NAME.get_value())),
                            c.getString(c.getColumnIndex(Params.MIDDLE_NAME.get_value())),
                            c.getString(c.getColumnIndex(Params.LAST_NAME.get_value())),
                            c.getString(c.getColumnIndex(Params.STATUS.get_value())),
                            c.getString(c.getColumnIndex(Params.DOB.get_value())),
                            c.getString(c.getColumnIndex(Params.IMAGE.get_value())),
                            c.getInt(c.getColumnIndex(Params.GENDER.get_value())),
                            0,
                            true,
                            c.getString(c.getColumnIndex(Params.PROFESSION.get_value())),
                            c.getString(c.getColumnIndex(Params.DATE.get_value()))
                    );
                    celebrityHashMap.put(c.getLong(c.getColumnIndex(Params.USER_ID.get_value())),celebrity);
                }

            }
            c.close();
        }
        db.close();
        return celebrityHashMap;
    }

    public ArrayList<Long> Get_Celebrity_ids()
    {
        ArrayList<Long> ids = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_CELEBRITY, null, null, null, null, null, Params.FIRST_NAME.get_value() + " DESC");
        if(c != null)
        {
            while(c.moveToNext())
            {
                if(c.getLong(2) != (App.getMyId()))
                {
                    ids.add(c.getLong(c.getColumnIndex(Params.USER_ID.get_value())));
                }

            }
            c.close();
        }
        db.close();
        return ids;
    }

    public long remove_follow(long id)
    {
        long delete = -1;
        SQLiteDatabase db = this.getWritableDatabase();
        delete = db.delete(TABLE_CELEBRITY,Params.USER_ID.get_value()+" = ?",new String[]{String.valueOf(id)});
        db.close();
        return delete;

    }
    /////////////////////////notification functions
    public long add_notification(Notification notify)
    {

        long insert = -1;
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Params.TYPE.get_value(), notify.getType());
        values.put(Params.USER_ID.get_value(), notify.getUserid());
        values.put(Params.IMAGE_ID.get_value(), notify.getImageid());
        if(notify.getContent() != null)
        values.put(Params.CONTENT.get_value(), notify.getContent());
        values.put(Params.READ_STATUS.get_value(), notify.isRead_status());
        values.put(Params.DATE.get_value(), notify.getTime());

        insert = db.insertOrThrow(TABLE_NOTIFICATION, null, values);

        db.close();
        return insert;
    }

    public long update_read_status(long id,int status)
    {
        long delete = -1;
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Params.READ_STATUS.get_value(), status);

        delete =db.update(TABLE_NOTIFICATION,values,Params.ID.get_value() + "= ?",new String[]{String.valueOf(id)});
        db.close();
        return delete;
    }

    public long delete_notification(long id)
    {
        long delete = -1;
        SQLiteDatabase db = this.getWritableDatabase();

        delete =db.delete(TABLE_NOTIFICATION,Params.ID.get_value() + "= ?",new String[]{String.valueOf(id)});
        db.close();
        return delete;
    }

    public ArrayList<Notification> get_notifications()
    {
        ArrayList<Notification> notifications = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_NOTIFICATION, null, null, null, null, null, Params.ID.get_value() + " DESC");
        if(c != null)
        {
            while(c.moveToNext())
            {

                long user_id = c.getLong(c.getColumnIndex(Params.USER_ID.get_value()));
                User user = Get_User_Details(user_id);
                if(user != null) {
                    Notification notification = new Notification(
                            c.getInt(c.getColumnIndex(Params.TYPE.get_value())),
                            c.getLong(c.getColumnIndex(Params.ID.get_value())),
                            user_id,
                            c.getLong(c.getColumnIndex(Params.IMAGE_ID.get_value())),
                            user.getF_name(),
                            c.getString(c.getColumnIndex(Params.CONTENT.get_value())),
                            c.getString(c.getColumnIndex(Params.DATE.get_value())),
                            c.getInt(c.getColumnIndex(Params.READ_STATUS.get_value()))
                    );
                    notifications.add(notification);
                }


            }
            c.close();
        }
        db.close();
        return notifications;
    }

    public int get_notifications_unread_count()
    {
        int count = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_NOTIFICATION, null, null, null, null, null, Params.ID.get_value() + " DESC");
        if(c != null)
        {
            while(c.moveToNext())
            {

                if(c.getInt(c.getColumnIndex(Params.READ_STATUS.get_value())) < 0)
                {
                    count++;
                }

            }
            c.close();
        }
        db.close();
        return count;
    }
}
