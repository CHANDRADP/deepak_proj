# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\Softwares\Android\Android_SDK/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontskipnonpubliclibraryclassmembers
-dontpreverify
-dontshrink
-verbose

-injars      bin/classes
-injars 	 libs
-outjars     bin/classes-processed.jar

-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*

#-dontwarn org.apache.**
#-dontwarn org.slf4j.**
#-dontwarn org.json.*
#-dontwarn org.mortbay.**
#-dontwarn org.apache.log4j.**
#-dontwarn org.apache.commons.logging.**
#-dontwarn org.apache.commons.logging.**
#-dontwarn org.apache.commons.codec.binary.**
#-dontwarn javax.xml.**
#-dontwarn javax.management.**
#-dontwarn java.lang.management.**
-dontwarn android.support.**
#-dontwarn com.google.code.**
#-dontwarn oauth.signpost.**

-dontwarn bolts.**
-dontwarn com.google.android.gms.**
-dontwarn com.google.api.client.googleapis.extensions.android.gms.**
#-dontwarn **CompatHoneycomb
#-dontwarn org.htmlcleaner.*



-keep class javax.**  { *; }
-keep class org.**  { *; }

-keep class java.lang.management.**  { *; }
-keep class com.google.code.**  { *; }
-keep class com.google.android.gms.**  { *; }


-keep class java.lang.reflect.**
-keepattributes *Annotation*
-keep public class com.google.vending.licensing.ILicensingService
-keep public class com.android.vending.licensing.ILicensingService
-keep public class * extends android.app.Application
-keep public class * extends android.app.Activity
-keep public class * extends android.app.PreferenceActivity
-keep public class * extends android.view.View
-keep public class * extends android.widget.BaseAdapter
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * implements android.view.View.OnTouchListener
-keep public class * implements android.view.View.OnClickListener

-keep public class * extends com.readystatesoftware.mapviewballoons.BalloonItemizedOverlay<OverlayItem>


-keep public class * extends android.support.v4.app.Fragment
-keep public class * extends android.app.Fragment

-keepclasseswithmembernames class * {
    native <methods>;
}



-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers class * extends android.content.Context {
    public void *(android.view.View);
    public void *(android.view.MenuItem);
}

-keepclassmembers class fqcn.of.javascript.interface.for.webview {
public *;
}

-keepclassmembers class **.R$* {
    public static <fields>;
}

# for facebook
-keepattributes Signature
-keep class com.facebook.** {
   *;
}


# for image loader
-keep class com.nostra13.universalimageloader.** {*;}
-keep  interface com.nostra13.universalimageloader.** {*;}

# for supporing library
-keep class android.support.v4.app.** { *; }
-keep interface android.support.v4.app.** { *; }

# for supporing library
-keep class android.support.v7.app.** { *; }
-keep interface android.support.v7.app.** { *; }


# for google gcm
-keep class com.google.android.gcm.**{ *; }



# for render script
-keep class android.support.v8.renderscript.**{ *; }

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keep class com.android.vending.licensing.ILicensingService

-assumenosideeffects class android.util.Log {
 #   public static *** d(...);
 #   public static *** v(...);
 #   public static *** i(...);
 #   public static *** w(...);
 #   public static *** e(...);
 #   public static *** wtf(...);
}