package com.dpc.wtp.activities;

import android.content.Intent;
import android.os.Bundle;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.customs.MyActivity;
import com.facebook.AppEventsLogger;

public class Splash extends MyActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getSupportActionBar().hide();
		super.generate_SHA_key_for_facebook();
		this.setContentView(R.layout.ac_splash);
		new Thread() {

            public void run() {

                try {
                	
                    Intent i = new Intent(Splash.this,Activity_Login.class);
                    Thread.sleep(2000);

                    startActivity(i);

                    finish();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
		AppEventsLogger.activateApp(this,getResources().getString(R.string.fb_app_id));
	}
	

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		AppEventsLogger.deactivateApp(this,getResources().getString(R.string.fb_app_id));
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	

}
