package com.dpc.wtp.activities;

import java.io.ByteArrayOutputStream;
import java.io.File;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.amazon.UploadService;
import com.dpc.wtp.customs.CircleImage;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.customs.MyProgress;
import com.dpc.wtp.customs.ProgressWheel;
import com.dpc.wtp.tasks.Photo_Update_Task;
import com.facebook.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class Activity_Profile extends MyActivity implements OnClickListener{
	
	private Text name;
	
	private CircleImage image;
	
	private LinearLayout change_photo;
	
	private Uri image_uri;
	
	private String image_path;
	
	private static final int PICK_FROM_CAMERA = 1;
	
	private static final int PICK_FROM_FILE = 2;
	
	private Net_Detect net;
	
	private ProgressWheel profile_points;

    private MyProgress myProgress;
	
	private boolean running;
	
	private int progress = 0,points = 0;
	
	final Runnable r = new Runnable() {
		public void run() {
			running = true;
			double break_points = 0.0;
			int previous = 0;
			
			if(points <= 1000)
			{
				App.profile_bg_color = getResources().getColor(R.color.transparent);
				App.profile_current_color = getResources().getColor(R.color.c_one);
				break_points = 1000;
				
			}
			else if(points <= 5000)
			{
				App.profile_bg_color = getResources().getColor(R.color.c_one);
				App.profile_current_color = getResources().getColor(R.color.c_two);
				break_points = 5000;
				previous = 1000;
			}
			else if(points <= 10000)
			{
				App.profile_bg_color = getResources().getColor(R.color.c_two);
				App.profile_current_color = getResources().getColor(R.color.c_three);
				break_points = 10000;
				previous = 5000;
			}
			else if(points <= 25000)
			{
				App.profile_bg_color = getResources().getColor(R.color.c_three);
				App.profile_current_color = getResources().getColor(R.color.c_four);
				break_points = 25000;
				previous = 10000;
			}
			else if(points <= 50000)
			{
				App.profile_bg_color = getResources().getColor(R.color.c_four);
				App.profile_current_color = getResources().getColor(R.color.c_five);
				break_points = 50000;
				previous = 25000;
			}
			else if(points <= 75000)
			{
				App.profile_bg_color = getResources().getColor(R.color.c_five);
				App.profile_current_color = getResources().getColor(R.color.c_six);
				break_points = 75000;
				previous = 50000;
			}
			else if(points <= 100000)
			{
				App.profile_bg_color = getResources().getColor(R.color.c_six);
				App.profile_current_color = getResources().getColor(R.color.c_seven);
				break_points = 100000;
				previous = 75000;
			}
			else if(points <= 150000)
			{
				App.profile_bg_color = getResources().getColor(R.color.c_seven);
				App.profile_current_color = getResources().getColor(R.color.c_eight);
				break_points = 150000;
				previous = 100000;
			}
			else if(points <= 200000)
			{
				App.profile_bg_color = getResources().getColor(R.color.c_eight);
				App.profile_current_color = getResources().getColor(R.color.c_nine);
				break_points = 200000;
				previous = 150000;
			}
			else if(points <= 300000)
			{
				App.profile_bg_color = getResources().getColor(R.color.c_nine);
				App.profile_current_color = getResources().getColor(R.color.c_ten);
				break_points = 300000;
				previous = 200000;
			}
			else if(points <= 400000)
			{
				App.profile_bg_color = getResources().getColor(R.color.c_ten);
				App.profile_current_color = getResources().getColor(R.color.c_eleven);
				break_points = 400000;
				previous = 300000;
			}
			else if(points <= 500000)
			{
				App.profile_bg_color = getResources().getColor(R.color.c_eleven);
				App.profile_current_color = getResources().getColor(R.color.c_twelve);
				break_points = 500000;
				previous = 400000;
			}
			profile_points.setRimColor(App.profile_bg_color);
			profile_points.setBarColor(App.profile_current_color);
			profile_points.setTextColor(App.profile_current_color);
			double p_limit = ((360/(break_points-previous)) * (points - previous));
			while(progress < p_limit) 
			{
				
				profile_points.incrementProgress();
				int p = (int) ((progress/(360/(break_points-previous))+previous));
				profile_points.setText(String.valueOf((p)));
				progress++;
				
				try {
					Thread.sleep(2);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			profile_points.setText(String.valueOf((points)));
			running = false;
		}
    };
	private void init_net_details()
	{
		net = new Net_Detect(this);
		
	}
    public Activity_Profile()
    {
        options = new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.user)
                .showImageForEmptyUri(R.drawable.user)
                .showImageOnFail(R.drawable.user)
                .cacheInMemory(true)
//	.cacheOnDisc(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
              //  .displayer(new FadeInBitmapDisplayer(5))
                .build();
    }
	@Override
	public void onCreate(Bundle savedInstanceState) {
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.actionbar);
		final Text title = (Text)findViewById(android.R.id.title);
		title.setText(R.string.ac_profile);
		super.onCreate(savedInstanceState);
        app_tracker.setScreenName(C.SCREEN.SCREEN_PROFILE.get_screen());
        app_tracker.send(new HitBuilders.AppViewBuilder().build());
		init_net_details();
		this.setContentView(R.layout.ac_profile);
		set_layout();
	//	if(App.get_userpoints() != null)
    		points = App.get_userpoints();
    	if(!running) {
			progress = 0;
			profile_points.resetCount();
			Thread s = new Thread(r);
			s.start();
		}

	}

	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
	}

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter f = new IntentFilter();
        f.addAction(UploadService.UPLOAD_STATE_CHANGED_ACTION);
        registerReceiver(uploadStateReceiver, f);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(uploadStateReceiver);
    }

    @Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	//	unregisterReceiver(NewMessageReceiver);
		AppEventsLogger.deactivateApp(this,getResources().getString(R.string.fb_app_id));
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;
	}



	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
		AppEventsLogger.activateApp(this,getResources().getString(R.string.fb_app_id));
		
	//	registerReceiver(NewMessageReceiver, new IntentFilter(
	//			C.BROADCAST_MSG));
		update_views();
	}
	
	private void update_views()
	{
		/*Bitmap bitmap = new C().get_image(App.get_thumbnail());
        if(bitmap == null)
       	 bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.user);
        image.setImageBitmap(new C().getRoundedCornerBitmap(bitmap));*/
        imageLoader.displayImage(App.get_thumbnail(),image,options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {
                image.setImageBitmap(new C().getRoundedCornerBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.user)));
            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                if(bitmap != null)
                image.setImageBitmap(new C().getRoundedCornerBitmap(bitmap));
            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
    	
		 return super.onCreateOptionsMenu(menu);
    }
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId())
		{
	
		case android.R.id.home:
			super.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
		case R.id.change_photo:
			
			open_image_selector();
			break;
		
		
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    
		if (resultCode != Activity.RESULT_OK) return;
	   
		Bitmap thumb = null,image = null;
		
		if (requestCode == PICK_FROM_FILE) 
		{
			image_uri = data.getData(); 
			image_path = getRealPathFromURI(image_uri); //from Gallery 
		
			if (image_path == null)
				image_path = image_uri.getPath(); //from File Manager
			
			if (image_path == null) 
			image_path	= image_uri.getPath();
			
		}
			
		if(net.isConnectingToInternet())
		{


			
			///////////////////////////////////
            Intent intent = new Intent(this, UploadService.class);
            intent.putExtra(PARAMS.IMAGE.get_param(), image_path);
            this.startService(intent);
            ///////////////////////////////////////
		//	new Photo_Update_Task(this,image_string,thumbnail).execute();
		}
		else
			C.net_alert(getApplicationContext());
		
	}
	
	private void set_layout() {
		change_photo = (LinearLayout)findViewById(R.id.change_photo);
		
		name = (Text)findViewById(R.id.name);
		
		image = (CircleImage)findViewById(R.id.image);
		
		change_photo.setOnClickListener(this);
		name.setText(App.get_username());
		profile_points = (ProgressWheel)findViewById(R.id.progress);
		
	}

	void open_image_selector()
	{
		final String [] items			= new String [] {"From Camera", "From SD Card"};				
		ArrayAdapter<String> adapter	= new ArrayAdapter<String> (this, android.R.layout.select_dialog_item,items);
		AlertDialog.Builder builder		= new AlertDialog.Builder(this);
		
		builder.setTitle("Select Image");
		builder.setAdapter( adapter, new DialogInterface.OnClickListener() 
		{
			public void onClick( DialogInterface dialog, int item ) 
			{
				if (item == 0) 
				{
					Intent intent 	 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					File file		 = new File(Environment.getExternalStorageDirectory(),
							   			"tmp_avatar_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
					image_uri = Uri.fromFile(file);

					try {			
						intent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri);
						intent.putExtra("return-data", true);
						
						startActivityForResult(intent, PICK_FROM_CAMERA);
					} catch (Exception e) {
						e.printStackTrace();
					}			
					
					dialog.cancel();
				} 
				else 
				{
					Intent intent = new Intent();
					
	                intent.setType("image/*");
	                intent.setAction(Intent.ACTION_GET_CONTENT);
	                
	                startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_FILE);
				}
			}
		} );
		
		final AlertDialog dialog = builder.create();
		dialog.show();
	}
	public String getRealPathFromURI(Uri contentUri) {
        String [] proj 		= {MediaStore.Images.Media.DATA};
        @SuppressWarnings("deprecation")
		Cursor cursor 		= this.managedQuery( contentUri, proj, null, null,null);
        
        if (cursor == null) return null;
        
        int column_index 	= cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        
        cursor.moveToFirst();

        return cursor.getString(column_index);
	}
	
	
	private final BroadcastReceiver NewMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent i) 
		{
			if(i.hasExtra(PARAMS.IMAGE.get_param()))
			{
                if(myProgress != null) {
                    myProgress.dismiss();
                }
				update_views();
			}
		}
	};

    private BroadcastReceiver uploadStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Bundle b = intent.getExtras();
            int state = b.getInt(UploadService.UPLOAD_STATE);
            if(state == 0)
            {
                myProgress = new MyProgress(Activity_Profile.this);
                myProgress.setTitle("Uploading...");
                myProgress.setCancelable(false);
                myProgress.show();
                return;
            }
            if(state == 1)
            {
                if(b.containsKey(PARAMS.IMAGE.get_param()))
                if(myProgress != null)
                {
                    myProgress.dismiss();
                 //  C.alert(getApplicationContext(),b.getString(UploadService.MESSAGE));
                    imageLoader.clearMemoryCache();
                    imageLoader.clearDiscCache();
                    update_views();
                    return;
                }
                return;
            }
            if(state < 0)
            {
                if(myProgress != null) {
                    myProgress.dismiss();
                }
                C.alert(getApplicationContext(),b.getString(UploadService.MESSAGE));
                return;

            }


        }
    };

}
