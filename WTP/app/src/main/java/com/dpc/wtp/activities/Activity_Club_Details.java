package com.dpc.wtp.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Models.V_Details;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.Utils.TextB;
import com.dpc.wtp.adapters.Club_Details_Adapter;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.customs.Review_Dialog;
import com.dpc.wtp.tasks.Display_Image;
import com.dpc.wtp.tasks.Stars_task;
import com.facebook.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.util.ArrayList;

public class Activity_Club_Details extends MyActivity implements OnClickListener{
	
	private LinearLayout.LayoutParams wrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
	
	private LinearLayout.LayoutParams mini ;
	
	private String vd_id = "0";

    private ScrollView scroll;

	private V_Details club = null;
	
	private TextB name;
    private Text address;

    //private Text time;
    private Text details;
   // private Text price;
    private Text payment;

    private ImageView s1;
    private ImageView s2;
    private ImageView s3;
    private ImageView s4;
    private ImageView s5;
    private ImageView one;
    private ImageView two;
    private ImageView three;
    private ImageView four;
    private ImageView five;
    private ImageView c_image;
	
	private ListView list;
	
	private ArrayList<Event> list_items = new ArrayList<Event>();

    private Net_Detect net;
	
	private boolean toggle = false;
	
	
	public Activity_Club_Details()
	{
		options = new DisplayImageOptions.Builder()
		.showStubImage(R.drawable.default_bg)
		.showImageForEmptyUri(R.drawable.default_bg)
		.showImageOnFail(R.drawable.default_bg)
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}
	private void init_net_details()
	{
		net = new Net_Detect(this);
		
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.actionbar);
		final Text title = (Text)findViewById(android.R.id.title);
		title.setText(R.string.ac_clubdetails);
		super.onCreate(savedInstanceState);
        app_tracker.setScreenName(C.SCREEN.SCREEN_VENUE_DETAILS.get_screen());
        app_tracker.send(new HitBuilders.AppViewBuilder().build());
        mini = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, (int)getResources().getDimension(R.dimen.button_height));
		init_net_details();
		db = new DatabaseHelper(this);
		Intent i = this.getIntent();
		if(i.hasExtra(PARAMS.VD_ID.get_param()))
		{
			vd_id = i.getStringExtra(PARAMS.VD_ID.get_param());
			club = C.clubs.get(vd_id);
            if(club == null)
            {
                finish();
            }
		}
		setContentView(R.layout.ac_club_details);
        new C().blur_it(this);
		list = (ListView)findViewById(R.id.list);
	//	View v = getLayoutInflater().inflate(R.layout.temp_club, null);
	//	list.addHeaderView(v);
		
		set_layout();
	
		list.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int posi,
					long arg3) 
			{
				Intent i = new Intent(getApplicationContext(),Activity_Event_Details.class);
					i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					i.putExtra(PARAMS.EVENT_ID.get_param(), list_items.get(posi).get_event_id());
					startActivity(i);

			}
			
		});

		
	}
	@Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
    	
	//	getSupportMenuInflater().inflate(R.menu.share_menu, menu);
			
        return super.onCreateOptionsMenu(menu);
    }
	 @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId())
		{
	
		case R.id.share:
			share( "Club : "+club.get_venue_name(), "content");
			break;
		case android.R.id.home:
			super.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
    }
    public void share(String subject, String content)
	{
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("text/plain");
		i.putExtra(Intent.EXTRA_SUBJECT,subject);
		i.putExtra(Intent.EXTRA_TEXT, content);
		startActivity(Intent.createChooser(i, "Share via"));
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
		AppEventsLogger.activateApp(this,getResources().getString(R.string.fb_app_id));
		
		registerReceiver(NewMessageReceiver, new IntentFilter(
				C.BROADCAST_MSG));
		update_views();
	}
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		AppEventsLogger.deactivateApp(this,getResources().getString(R.string.fb_app_id));
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;
		unregisterReceiver(NewMessageReceiver);
	}
	
	private void update_views()
	{
		name.setText(club.get_venue_name());
		address.setText(club.get_venue_address());
		//price.setText("Cost for two : Rs"+event.get_entry_price()+"/-");
		//time.setText("Hours - "+event.get_event_s_time()+" to "+event.get_event_e_time());
		payment.setText("Accepts Credit Card & Cash");
		details.setText(club.get_details());
		if(club.get_images().size() > 0)
		{
			if(db.is_image_available(club.get_images().get(0).get_id()))
			{
				new Display_Image(this,club.get_images().get(0).get_id(),c_image).execute();
				
			}
			else
			imageLoader.displayImage(club.get_images().get(0).get_image(), c_image, options, null);
		}
		else
				c_image.setBackgroundResource(R.drawable.default_bg);
			
		set_stars();
		set_rating();
		list_items.clear();
		for(Event e : C.items.values())
		{
			if(club.get_vd_id().equals(e.get_venue_id()))
			{
				list_items.add(e);
			}
		}

        Club_Details_Adapter list_adapter = new Club_Details_Adapter(this, list_items,app_tracker);
		list.setAdapter(list_adapter);
         list.setAdapter(list_adapter);
        C.updateListViewHeight(list);
        scroll.smoothScrollTo(0,0);
	}
	private void set_layout() {

        scroll = (ScrollView)findViewById(R.id.scroll);
		name = (TextB) findViewById(R.id.c_name);
		address = (Text) findViewById(R.id.c_address);
        TextB reviews = (TextB) findViewById(R.id.reviews);
		
		//price = (Text) v.findViewById(R.id.c_price);
		//time = (Text) v.findViewById(R.id.c_time);
		payment = (Text) findViewById(R.id.payment);
		details = (Text)findViewById(R.id.description);
		
		c_image = (ImageView)findViewById(R.id.club_image);
        ImageView map = (ImageView) findViewById(R.id.c_map);
		
		one = (ImageView)findViewById(R.id.star_one);
		two = (ImageView)findViewById(R.id.star_two);
		three = (ImageView)findViewById(R.id.star_three);
		four = (ImageView)findViewById(R.id.star_four);
		five = (ImageView)findViewById(R.id.star_five);
		
		s1 = (ImageView)findViewById(R.id.s1);
		s2 = (ImageView)findViewById(R.id.s2);
		s3 = (ImageView)findViewById(R.id.s3);
		s4 = (ImageView)findViewById(R.id.s4);
		s5 = (ImageView)findViewById(R.id.s5);

        LinearLayout call = (LinearLayout) findViewById(R.id.call);
		
		c_image.setOnClickListener(this);
		map.setOnClickListener(this);
		s1.setOnClickListener(this);
		s2.setOnClickListener(this);
		s3.setOnClickListener(this);
		s4.setOnClickListener(this);
		s5.setOnClickListener(this);
		call.setOnClickListener(this);
		reviews.setOnClickListener(this);
		details.setOnClickListener(this);
		
		
		
		
	}
	private void set_stars()
	{
		int[] star = C.set_rating(club.get_stars());
		s1.setBackgroundResource(star[0]);
		s2.setBackgroundResource(star[1]);
		s3.setBackgroundResource(star[2]);
		s4.setBackgroundResource(star[3]);
		s5.setBackgroundResource(star[4]);
	}
	private void set_rating()
	{
		int[] star = C.set_rating(club.get_rating());
		one.setBackgroundResource(star[0]);
		two.setBackgroundResource(star[1]);
		three.setBackgroundResource(star[2]);
		four.setBackgroundResource(star[3]);
		five.setBackgroundResource(star[4]);
		
	}
	
	@Override
	public void onClick(View v) {
		
		switch(v.getId())
		{
			case R.id.club_image:
				Intent i = new Intent(this,Activity_ImagePager.class);
				i.putExtra(PARAMS.ID.get_param(), vd_id);
				i.putExtra(PARAMS.ARTIST.get_param(), false);
				//startActivity(i);
				break;
			case R.id.c_map:
				C.open_maps_for_location(this, club.get_venue_address());
				break;
			
			case R.id.s1:
				update_stars(1);
				break;
			case R.id.s2:
				update_stars(2);
				
				break;
			case R.id.s3:
				update_stars(3);
				break;
			case R.id.s4:
				update_stars(4);
				break;
			case R.id.s5:
				update_stars(5);
				break;
			case R.id.call:
				if(club.get_venue_contactinfo() != null || !club.get_venue_contactinfo().equals("null"))
				{
					String nos = club.get_venue_contactinfo().split(",")[0];
					C.open_dialer(getApplicationContext(), nos);
                    app_tracker.send(new C().generate_analytics(C.CATEGORY.VENUES.get_category(),C.ACTION.ACTION_CALL.get_action(),club.get_venue_name()));

				}
				break;
			case R.id.reviews:
				Review_Dialog reviews = new Review_Dialog(this);
				Bundle args = new Bundle();
				args.putString(PARAMS.VD_ID.get_param(), vd_id);
				reviews.setArguments(args);
				reviews.show(getSupportFragmentManager(), "");
				break;
			case R.id.description:
				C.expand(this,details,toggle);
				toggle = !toggle;
				
			
		}
		
	}
	
	private void update_stars(int star_no)
	{
		
		
		if(net.isConnectingToInternet())
		{
			if(club.get_stars() <= 0)
			{
				club.set_stars(String.valueOf(star_no));
				
				new Stars_task(this,club.get_vd_id(),String.valueOf(club.get_stars()),true,PARAMS.VENUE.get_param()).execute();
			}
			else if(club.get_stars() != star_no)
			{
				club.set_stars(String.valueOf(star_no));
				
				new Stars_task(this,club.get_vd_id(),String.valueOf(club.get_stars()),false,PARAMS.VENUE.get_param()).execute();
			}
		}
		else
		{
			club.set_stars(String.valueOf(star_no));
			
			db.update_clubs_rating(club.get_vd_id(), star_no, club.get_stars() <= 0 ? 1 : 2);
		}
		set_stars();
		C.update_clubs(club);
		
	}
	
	//////////////////////////
	private BroadcastReceiver NewMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) 
		{
			if(intent.hasExtra(PARAMS.VENUE_DETAILS.get_param()) || intent.hasExtra(PARAMS.EVENT_ID.get_param()))
			update_views();
		}
	};
}
