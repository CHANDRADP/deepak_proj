package com.dpc.wtp.adapters;


import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.dpc.wtp.R;
import com.dpc.wtp.Models.Artist;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.tasks.Display_Image;
import com.dpc.wtp.tasks.Following_Task;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class Artist_Adapter extends BaseAdapter {
	LayoutInflater inflater;
	ViewHolder holder;
	View v;
	List<Artist> items;
	Context c;
	ImageLoader loader;
	DisplayImageOptions options;
	private Net_Detect net;
	DatabaseHelper db;
	private class ViewHolder {
		Text a_name,genre,date,count;
		ImageView a_image,favorite;
		
		
	}
	public Artist_Adapter(Context c,List<Artist> l, ImageLoader loader,
			DisplayImageOptions options)
	{
		this.c = c;
		this.items = l;
		this.loader = loader;
		db = new DatabaseHelper(c);
		net = new Net_Detect(this.c);
		this.options = new DisplayImageOptions.Builder()
    	.showStubImage(R.drawable.artist)
    	.showImageForEmptyUri(R.drawable.artist)
    	.showImageOnFail(R.drawable.artist)
    //	.cacheInMemory(true)
    	//.cacheOnDisc(true)
                .displayer(new FadeInBitmapDisplayer(C.ANIM_DURATION))
    	.bitmapConfig(Bitmap.Config.RGB_565)
    	.build();
	}
	
	public Artist_Adapter(Context applicationContext,
			ArrayList<Artist> list_items) {
		// TODO Auto-generated constructor stub
	}

	public View getView(final int position, View convertView, ViewGroup parent)
	{
		final Artist artist = items.get(position);
		
		v = convertView;
		if (v == null) 
		{
			 inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 v = inflater.inflate(R.layout.artist_details_header, null);
			 holder = new ViewHolder();
			 holder.a_name = (Text) v.findViewById(R.id.name);
			 holder.genre = (Text) v.findViewById(R.id.genre_type);
			// holder.location = (Text) v.findViewById(R.id.location);
			 holder.date = (Text) v.findViewById(R.id.date);
			 holder.count = (Text) v.findViewById(R.id.count);
			 holder.a_image = (ImageView) v.findViewById(R.id.icon);
			 holder.favorite = (ImageView) v.findViewById(R.id.favorite);
			 v.setTag(holder);
			 
		}
		else
			holder = (ViewHolder) v.getTag();
		

        holder.a_image.setImageBitmap(null);
		if(artist.get_images().size() > 0) {
            if (db.is_image_available(artist.get_images().get(0).get_id())) {
                new Display_Image(c, artist.get_images().get(0).get_id(), holder.a_image).execute();

            } else {
                loader.displayImage(artist.get_images().get(0).get_image(), holder.a_image, options, new ImageLoadingListener() {

                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view,
                                                FailReason failReason) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view,
                                                  Bitmap loadedImage) {
                        // TODO Auto-generated method stub
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        loadedImage.compress(Bitmap.CompressFormat.JPEG, 80, baos);
                        byte[] b = baos.toByteArray();
                        String image = Base64.encodeToString(b, Base64.DEFAULT);

                        long id1 = db.add_images_for_optimization(artist.get_images().get(0).get_id(), image);
                        if (id1 > 0) {
                            Log.d("optimized image saved", "optimized image saved");
                        }


                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        // TODO Auto-generated method stub

                    }

                });
            }
        }
		else
        {
            holder.a_image.setBackgroundResource(R.drawable.artist);
        }
		
		set_text(holder.a_name,artist.get_name());
		set_text(holder.genre,artist.get_genre_type());
		set_text(holder.count,"Followers  "+"<font color='#BE1E2D'>"+artist.get_following_count()+"</font>");
		//set_text(holder.location,artist.get_place());
		set_text(holder.date,artist.get_event_date());
		
		set_favorite(holder.favorite,artist.is_following());
		
		holder.favorite.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ImageView img = (ImageView)v;
				
				items.get(position).set_following(items.get(position).is_following() ? "-1" : "1");
				
				set_favorite(img,items.get(position).is_following());
				
				//int count = Integer.parseInt(items.get(position).get_following_count()) + (items.get(position).is_following() ? 1 : -1);
				//items.get(position).set_following_count(String.valueOf(count));
				
				if(net.isConnectingToInternet())
				{
					
					new Following_Task(c.getApplicationContext(),items.get(position).get_id(),items.get(position).is_following()).execute();

				}
				else
				{
					int following = 1;
					Artist a = items.get(position);
					if(a.is_following())
						following = -1;
					
					db.update_artist_follow(items.get(position).get_id(), following);
				}
				
					
			}
			
		});
		
		return v;

	}

	private void set_favorite(ImageView img,boolean following)
	{
		if(!following)
		{
			img.setBackgroundResource(R.drawable.favorite_n);
		}
		else
		{
			img.setBackgroundResource(R.drawable.favorite_p);
		}
	}
	private void set_text(Text t_view, String txt) {
		// TODO Auto-generated method stub
		t_view.setText(Html.fromHtml(txt));
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}


	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
	}


	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

}