package com.dpc.wtp.customs;

import android.graphics.Bitmap;
import android.os.Bundle;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.activities.App;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MyDialog extends SherlockDialogFragment{
    protected Tracker app_tracker = null;
	protected DatabaseHelper db;
	protected  ImageLoader imageLoader = ImageLoader.getInstance();
	protected DisplayImageOptions options = new DisplayImageOptions.Builder()
	.showStubImage(R.drawable.user)
	.showImageForEmptyUri(R.drawable.user)
	.showImageOnFail(R.drawable.user)
	.cacheInMemory(true)
	.cacheOnDisc(true)
	.bitmapConfig(Bitmap.Config.RGB_565)
	.build();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app_tracker = ((App) getSherlockActivity().getApplication()).getTracker(
                App.WTP_Tracker.WTP_TRACKER);
    }
}
