package com.dpc.wtp.activities;

import java.util.ArrayList;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.dpc.wtp.R;
import com.dpc.wtp.Models.Artist;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.DeveloperKey;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.adapters.Artist_Details_Adapter;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.services.WTP_Service;
import com.dpc.wtp.tasks.Display_Image;
import com.dpc.wtp.tasks.Following_Task;
import com.facebook.AppEventsLogger;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

public class Activity_Artist_Details extends MyActivity implements OnClickListener, YouTubePlayer.OnInitializedListener{
	
	private String artist_id = "0",youtube_url = null,sc_url = null;
	
	private Artist artist;
	
	private Text name,genre,details,date,count;
	
	private ImageView a_image,favorite,fb,twitter;
	
	private ListView list;
	
	private ScrollView scroll;
	
	private ArrayList<Event> list_items = new ArrayList<Event>();
	
	private Artist_Details_Adapter list_adapter;
	
	private boolean toggle = false,is_playlist = false;
	
	private Net_Detect net;
    WTP_Service wtp_service;

    YouTubePlayerSupportFragment youTubePlayerFragment;
	
	public Activity_Artist_Details()
	{

		options = new DisplayImageOptions.Builder()
		.showStubImage(R.drawable.artist)
		.showImageForEmptyUri(R.drawable.artist)
		.showImageOnFail(R.drawable.artist)
		.cacheInMemory(true)
		//.cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}
	private void init_net_details()
	{
		net = new Net_Detect(this);
		
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.actionbar);
		final Text title = (Text)findViewById(android.R.id.title);
		title.setText(R.string.ac_artistdetails);
		super.onCreate(savedInstanceState);
        app_tracker.setScreenName(C.SCREEN.SCREEN_ARTIST_DETAILS.get_screen());
        app_tracker.send(new HitBuilders.AppViewBuilder().build());
        db = new DatabaseHelper(this);
		init_net_details();
		Intent i = this.getIntent();
		if(i.hasExtra(PARAMS.ARTIST_ID.get_param()))
		{
			artist_id = i.getStringExtra(PARAMS.ARTIST_ID.get_param());
			artist = C.artists.get(artist_id);
			Log.d("artist_id", artist_id);
		}
		setContentView(R.layout.ac_artist_details);
        new C().blur_it(this);
		
		
		set_layout();

        wtp_service = new WTP_Service();
        wtp_service.create_connection(getApplicationContext());
	
		
		list.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int posi,
					long arg3) 
			{
				Intent i = new Intent(getApplicationContext(),Activity_Event_Details.class);
					i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					i.putExtra(PARAMS.EVENT_ID.get_param(), list_items.get(posi).get_event_id());
					startActivity(i);
				
			}
			
		});


		
	}

	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
		AppEventsLogger.activateApp(this,getResources().getString(R.string.fb_app_id));
		
		registerReceiver(NewMessageReceiver, new IntentFilter(
				C.BROADCAST_MSG));
		update_views();
	}
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		AppEventsLogger.deactivateApp(this,getResources().getString(R.string.fb_app_id));

		unregisterReceiver(NewMessageReceiver);
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;
	}

    @Override
    protected void onStop() {
        super.onStop();
        if(youTubePlayerFragment != null)
        {
            youTubePlayerFragment.onStop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(youTubePlayerFragment != null)
        {
          //  youTubePlayerFragment.onDestroy();
        }
    }

    private void update_views()
	{
		if(artist != null)
		{
			name.setText(artist.get_name());
			//location.setText(artist.get_place());
			genre.setText(artist.get_genre_type());
			date.setText(artist.get_event_date());
			count.setText(artist.get_following_count());
			set_favorite_icon();
			if(artist.get_details() != null)
			details.setText(artist.get_details());
			//else
			//	details.setVisibility(View.GONE);
			
			if(artist.get_images().size() > 0) {
                if (db.is_image_available(artist.get_images().get(0).get_id())) {
                    new Display_Image(this, artist.get_images().get(0).get_id(), a_image).execute();

                } else
                    imageLoader.displayImage(artist.get_images().get(0).get_image(), a_image, options, null);
            }
            else
            {
                a_image.setImageDrawable(getResources().getDrawable(R.drawable.artist));
            }
			

		}
		list_items.clear();
		for(Event e : C.items.values())
		{
            for(int i = 0;i < e.getLineups().size();i++)
            if(e.getLineups().get(i).getArtistid().equalsIgnoreCase(artist_id))
			{
				list_items.add(e);
                break;
				
			}
		}
		
		list_adapter = new Artist_Details_Adapter(this,list_items);
		list.setAdapter(list_adapter);
		C.updateListViewHeight(list);
		scroll.smoothScrollTo(0,0);

	}



	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}
	@Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
    	
		getSupportMenuInflater().inflate(R.menu.artist, menu);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && youtube_url != null) {
            menu.findItem(R.id.youtube).setVisible(true);

        }
        if(sc_url != null)
            menu.findItem(R.id.sound_cloud).setVisible(true);
        return super.onCreateOptionsMenu(menu);
    }
	 @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId())
		{

            case R.id.youtube:
                Intent intent = null;
                if(is_playlist)
                intent = YouTubeStandalonePlayer.createPlaylistIntent(this, DeveloperKey.DEVELOPER_KEY, youtube_url);
                else
                    intent = YouTubeStandalonePlayer.createVideoIntent(this, DeveloperKey.DEVELOPER_KEY, youtube_url);
                if(intent != null)
                startActivity(intent);
                break;
		case R.id.sound_cloud:
			 startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(sc_url)));
             app_tracker.send(new C().generate_analytics(C.CATEGORY.ARTISTS.get_category(),C.ACTION.ACTION_VISITED.get_action(),C.LABEL.SOUND_CLOUD.get_label()));

			break;
		case android.R.id.home:
			super.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
    }

	private void set_layout() {

        findViewById(R.id.a_hide).setVisibility(View.GONE);
		scroll = (ScrollView)findViewById(R.id.scroll);
		
		list = (ListView)findViewById(R.id.list);
		
		name = (Text) findViewById(R.id.name);
		genre = (Text) findViewById(R.id.genre_type);
	//	location = (Text) v.findViewById(R.id.location);
		date = (Text)findViewById(R.id.date);
		count = (Text)findViewById(R.id.count1);
		details = (Text)findViewById(R.id.description);
		
		a_image = (ImageView)findViewById(R.id.icon);
		favorite = (ImageView)findViewById(R.id.favorite1);
		fb = (ImageView)findViewById(R.id.fb);
		twitter = (ImageView)findViewById(R.id.twitter);
		
		
		favorite.setOnClickListener(this);
		fb.setOnClickListener(this);
		twitter.setOnClickListener(this);
		
		details.setOnClickListener(this);



        if(artist.get_tracks().size() > 0)
        {
            for(int i = 0;i < artist.get_tracks().size();i++)
            {
                if (artist.get_tracks().get(i).get_type() == 1)
                {
                    String url = artist.get_tracks().get(i).get_url();
                    String[] split = null;
                    if(url.contains("&"))
                    {
                        split = url.split("&");
                        if(split != null && split.length > 1)
                        {
                            url = split[split.length - 1];
                            split = null;
                            split = url.split("=");
                            if (split != null)
                            {
                                youtube_url = split[split.length - 1];
                                is_playlist = true;
                            }
                        }
                    }
                    else {
                        split = url.split("=");
                        if (split != null)
                        {
                            youtube_url = split[split.length - 1];
                        }
                    }
                    //break;
                }
                else if(artist.get_tracks().get(i).get_type() == 2)
                {
                    sc_url = artist.get_tracks().get(i).get_url();
                }
            }

            if (youtube_url == null) {
                this.getSupportFragmentManager().findFragmentById(R.id.youtube_fragment).getView().setVisibility(View.GONE);
            }
            else
            {
                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    youTubePlayerFragment = (YouTubePlayerSupportFragment) this.getSupportFragmentManager().findFragmentById(R.id.youtube_fragment);
                    youTubePlayerFragment.initialize(DeveloperKey.DEVELOPER_KEY, this);
                }

            }
        }
        else
        {

            this.getSupportFragmentManager().findFragmentById(R.id.youtube_fragment).getView().setVisibility(View.GONE);
        }
        invalidateOptionsMenu();


		
	}
	private void set_favorite_icon()
	{
		if(artist.is_following())
		{
			favorite.setBackgroundResource(R.drawable.favorite_p);

        }
		else
		{
			favorite.setBackgroundResource(R.drawable.favorite_n);

		}
		
	}
	@Override
	public void onClick(View v) {
		
		switch(v.getId())
		{
			case R.id.favorite1:
				if(net.isConnectingToInternet())
				{
					new Following_Task(getApplicationContext(),artist.get_id(),!artist.is_following()).execute();
					
					///set_favorite(favorite,!artist.is_following());
					artist.set_following(artist.is_following() ? "-1" : "1");
                    set_favorite_icon();
					
					//int count = Integer.parseInt(artist.get_following_count()) + (artist.is_following() ? 1 : -1);
					//artist.set_following_count(String.valueOf(count));
                    app_tracker.send(new C().generate_analytics(C.CATEGORY.ARTISTS.get_category(),C.ACTION.ACTION_FAVORITE.get_action(),artist.get_name()));

                }
				else
				C.net_alert(getApplicationContext());
				break;
			case R.id.fb:

                if(artist.get_fbid() == null && artist.get_fbname() == null)
                {
                    C.alert(getApplicationContext(),"Details not available");
                }
                else {

                    String fb = artist.get_fbname();
                    String[] split = fb.split("/");
                    if(split != null)
                    {
                        startActivity(C.getOpenFacebookIntent(getApplicationContext(), artist.get_fbid(), split[split.length - 1]));
                        app_tracker.send(new C().generate_analytics(C.CATEGORY.ARTISTS.get_category(),C.ACTION.ACTION_VISITED.get_action(),C.LABEL.FB.get_label()));

                    }
                    else
                        C.alert(getApplicationContext(),"Details not available");

                }
                break;
			case R.id.twitter:
                if(artist.get_twitid() == null && artist.get_twitname() == null)
                {
                    C.alert(getApplicationContext(),"Details not available");
                }
                else {
                    String twit = artist.get_twitname();
                    String[] split = twit.split("/");
                    if(split != null) {
                        startActivity(C.getTwitterIntent(getApplicationContext(), artist.get_twitid(), split[split.length - 1]));
                        app_tracker.send(new C().generate_analytics(C.CATEGORY.ARTISTS.get_category(), C.ACTION.ACTION_VISITED.get_action(), C.LABEL.TWIT.get_label()));
                    }
                    else
                        C.alert(getApplicationContext(),"Details not available");
                }
                    break;
			case R.id.description:
                C.expand(this,details,toggle);
                toggle = !toggle;

				
				break;
			
		}
		
	}
//////////////////////////
	private final BroadcastReceiver NewMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.hasExtra(PARAMS.ARTIST.get_param()))
				update_views();
		}
	};

	private static final int RECOVERY_DIALOG_REQUEST = 1;

	@Override
	public void onInitializationFailure(Provider arg0,
			YouTubeInitializationResult arg1) {
		if (arg1.isUserRecoverableError()) {
			arg1.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
		} else {
			String errorMessage = String.format(
					getString(R.string.error_player), arg1.toString());
			C.alert(this, errorMessage);
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == RECOVERY_DIALOG_REQUEST) {
			// Retry initialization if user performed a recovery action
			getYouTubePlayerProvider().initialize(DeveloperKey.DEVELOPER_KEY,
					this);
		}
	}

	public YouTubePlayer.Provider getYouTubePlayerProvider() {
		return (YouTubePlayerSupportFragment) getSupportFragmentManager()
				.findFragmentById(R.id.youtube_fragment);
	}

	@Override
	public void onInitializationSuccess(Provider arg0, YouTubePlayer player,
			boolean arg2) {
		// TODO Auto-generated method stub

	//	player.cuePlaylist("PL56D792A831D0C362");
        if(youtube_url != null)
            if(is_playlist)
            player.cuePlaylist(youtube_url);
        else
                player.cueVideo(youtube_url);

	}
}
