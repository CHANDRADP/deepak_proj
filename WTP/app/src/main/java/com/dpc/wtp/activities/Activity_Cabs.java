package com.dpc.wtp.activities;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.dpc.wtp.R;
import com.dpc.wtp.Models.Cab;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.adapters.Cab_Adapter;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.services.WTP_Service;
import com.facebook.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;

public class Activity_Cabs extends MyActivity
{
	private ArrayList<Cab> list_items = new ArrayList<Cab>();
	
	private Cab_Adapter list_adapter;
	
	private ListView list;
    WTP_Service wtp_service;
	
    
	
	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.actionbar);
		final Text title = (Text)findViewById(android.R.id.title);
		title.setText(R.string.ac_cab);
        super.onCreate(savedInstanceState);
        app_tracker.setScreenName(C.SCREEN.SCREEN_CAB.get_screen());
        app_tracker.send(new HitBuilders.AppViewBuilder().build());
        this.setContentView(R.layout.ac_settings);
  
		list = (ListView)findViewById(R.id.list);
        list.setPadding(5, 0, 5, 0);
		//int[] colors = {R.color.white,R.color.white,R.color.white}; // red for the example
		list.setDivider(new ColorDrawable(getResources().getColor(R.color.txt_header)));
		list.setDividerHeight(1);
		//list.setBackgroundResource(R.color.list_row_info_bg);
        wtp_service = new WTP_Service();
        wtp_service.create_connection(getApplicationContext());


		
		list.setOnItemClickListener(
                new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int posi,
					long arg3)
            {
				C.open_dialer(getApplicationContext(), list_items.get(posi).get_number());
                app_tracker.send(new C().generate_analytics(C.CATEGORY.CAB.get_category(),C.ACTION.ACTION_CALL.get_action(),list_items.get(posi).get_name()));

            }
			
		});
		
		new Update().execute();

		
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
		AppEventsLogger.activateApp(this,getResources().getString(R.string.fb_app_id));
		
		
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		AppEventsLogger.deactivateApp(this,getResources().getString(R.string.fb_app_id));
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;
	}
	@Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
    	
			
        return super.onCreateOptionsMenu(menu);
    }
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId())
		{
	
		case android.R.id.home:
			super.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
    }
    /////////////////////////////////////////
	 
	public class Update extends AsyncTask<String, Long, Boolean> {
		
		@Override
		protected Boolean doInBackground(String... params) {
			list_items.add(new Cab("Easy Cabs","+918043434343",R.drawable.easy,null));
			list_items.add(new Cab("Meru Cabs","+918044224422",R.drawable.meru,null));
			list_items.add(new Cab("Ola Cabs","+918033553355",R.drawable.ola,null));
			list_items.add(new Cab("Taxi For Sure","+918060601010",R.drawable.taxi,null));
			return null;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			
			list_adapter = new Cab_Adapter(getApplicationContext(),list_items,imageLoader,options);
			list.setAdapter(list_adapter);
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
		}
}
}
