package com.dpc.wtp.tasks;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;

import com.dpc.wtp.JSONParser;
import com.dpc.wtp.Models.Server_Params;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.activities.App;

public class Update_App_version extends AsyncTask<String, Long, Boolean> {
    private Context c;

    public Update_App_version(Context c) {
        this.c = c;

    }


    @Override
    protected Boolean doInBackground(String... params) {

        return update_version();
    }

    @Override
    protected void onPostExecute(Boolean result) {

        if (result)
        {
            App.set_version(c);
        }


    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    public boolean update_version()
    {
        boolean success = false;
        JSONParser parser = new JSONParser();
        JSONObject user = null;
        try {

            user = parser.makeHttpRequest(C.SERVER_URL, "POST",
                    new Server_Params().get_app_version_params(App.get_version_from_manifest(c)));
            if(user != null)
            if(user.has(PARAMS.STATUS.get_param()))
            if(user.getInt(PARAMS.STATUS.get_param()) > 0)
            {
                success = true;
            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return success;
    }

}