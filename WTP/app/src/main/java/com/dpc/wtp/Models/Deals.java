package com.dpc.wtp.Models;



public class Deals {
    private String id, vd_id, name,type, u_id, sdate, edate,total_count,remaining_count,details;
    private Images image;
    public Deals(String id, String vd_id, String name,String type, String sdate, String edate,String total_count,String remaining_count,String details, Images image) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.vd_id = vd_id;
        this.sdate = sdate;
        this.edate = edate;
        this.total_count = total_count;
        this.remaining_count = remaining_count;
        this.details = details;
        this.image = image;
    }

    public Deals(String name, String id, String vdid, String code, String edate) {
        this.name = name;
        this.id = id;
        this.vd_id = vdid;
        this.u_id = code;
        this.edate = edate;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Deals() {
        // TODO Auto-generated constructor stub
    }


    public void set_unique_id(String id) {
        this.u_id = id;
    }

    public String get_id() {
        return id;
    }

    public String get_venue_id() {
        return vd_id;
    }

    public String get_name() {
        return name;
    }

    public String get_unique_id() {
        return u_id;
    }

    public String get_type() {
        return type;
    }

    public String get_s_date() {
        return sdate;
    }

    public String get_e_date() {
        return edate;
    }

    public String get_total_count() {
        return total_count;
    }

    public String get_remaining_count() {
        return remaining_count;
    }

    public void set_s_date(String date) {
        this.sdate = date;
    }

    public void set_e_date(String date) {
        this.edate = date;
    }

    public Images get_image() {
        return image;
    }

    public void setTotal_count(String total_count) {
        this.total_count = total_count;
    }

    public void setRemaining_count(String remaining_count) {
        this.remaining_count = remaining_count;
    }
}
