package com.dpc.wtp.adapters;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.customs.MyFragment;
import com.dpc.wtp.fragments.FB_Friends_Fragment;
import com.dpc.wtp.fragments.Favorite_Fragment;
import com.dpc.wtp.fragments.WTP_Friends_Fragment;

public class Favorite_tabs_pager_adapter extends FragmentPagerAdapter {
	private int count,friend_favorite = 0;
	private String txt;
	public Favorite_tabs_pager_adapter(FragmentManager fm, int count, int friend_favorite, String event_txt) {
		super(fm);
		this.count = count;
		this.friend_favorite = friend_favorite;
		this.txt = event_txt;
	}
	@Override
	public MyFragment getItem(final int index) {
		MyFragment fragment = null;
		if(friend_favorite == 1)
		{
			Bundle b = new Bundle();
			b.putString(PARAMS.EVENT_ID.get_param(), txt);
			if(index == 0)
			{
				fragment = new FB_Friends_Fragment();
				fragment.setArguments(b);
			}
			else
			{
				//fragment = new WTP_Friends_Fragment();
				fragment.setArguments(b);
			}
		}
		else if(friend_favorite == 2)
		{
			Bundle b = new Bundle();
			if(index == 0)
			{
				
				fragment = new Favorite_Fragment();
				b.putString(PARAMS.FAVORITE.get_param(), "1");
				
				fragment.setArguments(b);
			}
			else
			{
				fragment = new Favorite_Fragment();
				b.putString(PARAMS.FAVORITE.get_param(), "2");
				fragment.setArguments(b);
			}
		}
		return fragment;
		

		
	}
	
	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return count;
	}
	
	@Override
	public int getItemPosition(Object object) {
		
	  return super.getItemPosition(object);
	}

}
