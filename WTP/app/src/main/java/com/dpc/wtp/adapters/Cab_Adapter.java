package com.dpc.wtp.adapters;


import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.dpc.wtp.R;
import com.dpc.wtp.Models.Cab;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.Text;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class Cab_Adapter extends BaseAdapter {
	LayoutInflater inflater;
	ViewHolder holder;
	View v;
	List<Cab> items = new ArrayList<Cab>();
	Context c;
	ImageLoader loader;
	//int[] cabs = new int[]{R.drawable.easy,R.drawable.meru,R.drawable.ola,R.drawable.tfs};
	DisplayImageOptions options;
	private class ViewHolder {
		Text name,number;
		ImageView image,call;
		
		
	}
	public Cab_Adapter(Context c,List<Cab> l,ImageLoader loader,DisplayImageOptions options)
	{
		this.c = c;
		this.items = l;
		this.loader = loader;
		this.options = options;
		
	}
	
	
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		Cab cab = items.get(position);
		
		v = convertView;
		if (v == null) 
		{
			 inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 v = inflater.inflate(R.layout.row_cabs, null);
			 holder = new ViewHolder();
			 holder.name = (Text) v.findViewById(R.id.name);
			 holder.number = (Text) v.findViewById(R.id.number);
			 holder.image = (ImageView) v.findViewById(R.id.image);
			 holder.call = (ImageView) v.findViewById(R.id.call);
			 v.setTag(holder);
			 
		}
		else
			holder = (ViewHolder) v.getTag();
		
		holder.name.setText(cab.get_name());
		holder.number.setText(cab.get_number());
		holder.image.setBackgroundResource(cab.get_image());
		holder.call.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				C.open_dialer(c, items.get(position).get_number());
			}
			
		});
		//loader.displayImage(contact.get_thumbnail(), holder.user_pick, options);
		
		
		return v;

	}

	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}


	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
	}


	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

}