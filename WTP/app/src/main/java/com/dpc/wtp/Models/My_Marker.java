package com.dpc.wtp.Models;

public class My_Marker {
private String id,name,address,distance;
private boolean is_user;
private double[] loc;
public My_Marker(String id,String name,String address,String distance,double[] loc,boolean isuser)
{
	this.id = id;
	this.name = name;
	this.address = address;
	this.distance = distance;
	this.loc = loc;
	this.is_user = isuser;
	}

public String get_id()
{
	return id;}
public String get_name()
{
	return name;}
public String get_address()
{
	return address;
	}
public String get_distance()
{
	return distance;}
public boolean is_user()
{
	return is_user;}

public double get_lat()
{
	return loc[0];
	}
public double get_lng()
{
	return loc[1];
	}
}
