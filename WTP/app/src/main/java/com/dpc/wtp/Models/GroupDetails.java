package com.dpc.wtp.Models;

public class GroupDetails
{
    private String group_name, creation_date, group_admin, group_users;
    private int servergroupid;


    public GroupDetails(String group_name, String creation_date, String group_admin, String group_users, int servergroupid) {
        this.group_name = group_name;
        this.creation_date = creation_date;
        this.group_admin = group_admin;
        this.group_users = group_users;
        this.servergroupid = servergroupid;
    }

    public int getServergroupid() {
        return servergroupid;
    }

    public void setServergroupid(int servergroupid) {
        this.servergroupid = servergroupid;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getGroup_admin() {
        return group_admin;
    }

    public void setGroup_admin(String group_admin) {
        this.group_admin = group_admin;
    }

    public String getGroup_users() {
        return group_users;
    }

    public void setGroup_users(String group_users) {
        this.group_users = group_users;
    }
}
