package com.dpc.wtp.tasks;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.dpc.wtp.JSONParser;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Models.Server_Params;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.activities.App;

public class Update_Going_Status extends AsyncTask<String, Long, Boolean> {
	private boolean add = false;
	private String event_id;
	private String type = null;
	Context c;

	public Update_Going_Status(Context c,String event_id,String type,boolean add) {
		this.c = c;
		this.event_id = event_id;
		this.add = add;
		this.type = type;
		
	}

	

	@Override
	protected Boolean doInBackground(String... params) {
		
		return update_going_type();
	}

	@Override
	protected void onPostExecute(Boolean result) {
		//dialog.dismiss();
		
		if (result) 
		{
			Event e = C.items.get(event_id);
			e.set_goingtype(Integer.parseInt(type));
			int count = Integer.parseInt(e.get_event_going_count());
			if(e.get_event_going_type() == 1)
			{
				e.set_goingcount(String.valueOf(++count));
				
			}
			else
			{
				e.set_goingcount(String.valueOf(--count));
			}
			C.update_items(e);
			
		}
		else
		{
			C.alert(c, "Error in updating going status");
		}
		Intent i = new Intent(C.BROADCAST_MSG);
		i.putExtra(PARAMS.EVENT_ID.get_param(), "1");
		c.sendBroadcast(i);
	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
	}
	
	public boolean update_going_type()
	{
		boolean success = false;
		JSONParser parser = new JSONParser();
		JSONObject user = null;
		try {
		if(add)
		{
		user = parser.makeHttpRequest(C.SERVER_URL, "POST",
				new Server_Params().get_add_attend_params(App.get_userid(),type, event_id));
            if(user != null)
			if(user.getInt(PARAMS.STATUS.get_param()) > 0)
			{
				success = true;
			}
		}
		else
		{
			user = parser.makeHttpRequest(C.SERVER_URL, "POST",
					new Server_Params().get_update_attend_params(App.get_userid(),type, event_id));
            if(user != null)
			if(user.getInt(PARAMS.STATUS.get_param()) > 0)
			{
				success = true;
			}
		}
		
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return success;
	}

}