package com.dpc.wtp.activities;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.actionbarsherlock.view.MenuItem;
import com.dpc.wtp.R;
import com.dpc.wtp.Models.Images;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.customs.TouchImageView;
import com.dpc.wtp.tasks.Display_Image;
import com.facebook.AppEventsLogger;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

public class Activity_ImagePager extends MyActivity implements OnClickListener{
	private ArrayList<Images> images = new ArrayList<Images>();
	private ViewPager pager;
	private String id = null;
	private boolean is_artist = false;
	private ImagePagerAdapter ia;
	private DatabaseHelper db;
	public void onCreate(Bundle savedInstanceState) {
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.actionbar);
		final Text title = (Text)findViewById(android.R.id.title);
		title.setText("Images");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ac_image_pager);
		db = new DatabaseHelper(this);
		Intent  i = this.getIntent();
		id = i.getStringExtra(PARAMS.ID.get_param());
		is_artist = i.getBooleanExtra(PARAMS.ARTIST.get_param(), false);
		if(id != null)
		{
			if(is_artist)
			{
				//images = C.artists.get(id).g
			}
			else
			{
				images = C.clubs.get(id).get_images();
			}
		}
		if(images != null)
		{
			if(images.size() == 0)
			{
				C.alert(getApplicationContext(), "No images to dispaly");
			}
		}
		pager = (ViewPager) findViewById(R.id.pager);
		ia = new ImagePagerAdapter();
		pager.setAdapter(ia);


    }
	
	
	
    @Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		AppEventsLogger.deactivateApp(this,getResources().getString(R.string.fb_app_id));
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;

    }



	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
		AppEventsLogger.activateApp(this,getResources().getString(R.string.fb_app_id));
		
	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId())
		{
	
			case android.R.id.home:
			super.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
    }
	 
	@Override
	public void onClick(View v) {
		
		
	}
	
	private class ImagePagerAdapter extends PagerAdapter{

		private LayoutInflater inflater = getLayoutInflater();;
		

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((View) object);
		}

		@Override
		public void finishUpdate(View container) {
		}

		@Override
		public int getCount() {
			if(images != null)
			return images.size();
			else 
				return 0;
		}

		@Override
		public Object instantiateItem(ViewGroup view, final int position) {
			
			View pagercontents = inflater.inflate(R.layout.item_pager_image, view, false);
			final TouchImageView imageView = (TouchImageView) pagercontents.findViewById(R.id.image);
		
			 
			 
			final ProgressBar spinner = (ProgressBar) pagercontents.findViewById(R.id.loading);
			if(db.is_image_available(images.get(position).get_id()))
			{
				new Display_Image(Activity_ImagePager.this,images.get(position).get_id(),imageView).execute();
				
			}
			else
			{
				
			imageLoader.displayImage(images.get(position).get_image(), imageView, options, new SimpleImageLoadingListener() {
				@Override
				public void onLoadingStarted(String imageUri, View view) {
					imageView.setEnabled(false);
					spinner.setVisibility(View.VISIBLE);
				}

				@Override
				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
					String message = null;
					switch (failReason.getType()) {
						case IO_ERROR:
							message = "Input/Output error";
							break;
						case DECODING_ERROR:
							message = "Image can't be decoded";
							break;
						case NETWORK_DENIED:
							message = "Downloads are denied";
							break;
						case OUT_OF_MEMORY:
							message = "Out Of Memory error";
							break;
						case UNKNOWN:
							message = "Unknown error";
							break;
					}
					
					spinner.setVisibility(View.GONE);
				}

				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					imageView.setEnabled(true);
					spinner.setVisibility(View.GONE);
					ByteArrayOutputStream baos = new ByteArrayOutputStream(); 
					loadedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);   
					byte[] b = baos.toByteArray(); 
					String image = Base64.encodeToString(b, Base64.DEFAULT);
					
					long id1 = db.add_images_for_optimization(images.get(position).get_id(), image);
					if(id1 > 0)
					{
						Log.d("optimized image saved", "optimized image saved");
					}
				}
			});
			}
			((ViewPager) view).addView(pagercontents, 0);
			return pagercontents;
		}
		

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View container) {
		}

		
		
			
	}
	
	
}
