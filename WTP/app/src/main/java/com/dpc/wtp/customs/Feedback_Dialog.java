package com.dpc.wtp.customs;

import android.R.color;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.dpc.wtp.R;
import com.dpc.wtp.Models.Review;
import com.dpc.wtp.Models.V_Details;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.EText;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.activities.App;
import com.dpc.wtp.adapters.Review_Adapter;
import com.dpc.wtp.interfaces.ListViewClickListener;
import com.dpc.wtp.tasks.Display_Image;
import com.dpc.wtp.tasks.Feedback_Task;
import com.dpc.wtp.tasks.Review_Task;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

@SuppressLint("ValidFragment")
public class Feedback_Dialog extends MyDialog{
	
	private Context c;
	private Net_Detect net;
	public Feedback_Dialog(Context c) {
		// TODO Auto-generated constructor stub
			this.c = c;
	}

		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			net = new Net_Detect(this.getSherlockActivity().getApplicationContext());
			
		}
		
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			final Dialog dialog = new Dialog(c);
			dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
			dialog.setContentView(R.layout.feedback_dialog);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(color.transparent));
			dialog.show();
			
			Text header = (Text)dialog.findViewById(R.id.header);
			Button send = (Button)dialog.findViewById(R.id.send);
			Button cancel = (Button)dialog.findViewById(R.id.cancel);
			final EText txt = (EText)dialog.findViewById(R.id.e_txt);
			
			send.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String feedback = txt.getText().toString();
					if(txt.length() > 0)
					{
                        if (net.isConnectingToInternet()) {
                            new Feedback_Task(c, feedback).execute();
                            dismiss();
                        }
                        else
                        {
                            C.net_alert(c);
                        }
					}
                    else
                    {
                        C.alert(getSherlockActivity().getApplicationContext(),"Please type your feedback");
                    }
					
						
				}
				
			});
			cancel.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dismiss();
					
						
				}
				
			});
			return dialog;
		}

		

}
