package com.dpc.wtp.fragments;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.dpc.wtp.R;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Models.WTP_Contact;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.EText;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.Utils.TextB;
import com.dpc.wtp.adapters.Invite_Friends_Adapter;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.customs.MyFragment;
import com.dpc.wtp.tasks.Invite_Task;
import com.facebook.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;

public class WTP_Friends_Fragment extends MyActivity implements OnClickListener
{
	private ArrayList<WTP_Contact> list_items = new ArrayList<WTP_Contact>();
	
	private Invite_Friends_Adapter list_adapter;
	
	private ListView list;
	
	private EText ed;
	
	private TextB invite,sendtoall;
	
	private String event_id = null;
    
	
    @Override
	public void onCreate(Bundle savedInstanceState) 
	{
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar);
        final Text title = (Text)findViewById(android.R.id.title);
        title.setText(R.string.ac_invite_frnds);
        super.onCreate(savedInstanceState);
        app_tracker.setScreenName(C.SCREEN.SCREEN_INVITE_FRIENDS.get_screen());
        app_tracker.send(new HitBuilders.AppViewBuilder().build());
        Intent i = this.getIntent();
        if(i != null)
        {
            if(i.hasExtra(PARAMS.EVENT_ID.get_param()))
            {
                event_id = i.getStringExtra(PARAMS.EVENT_ID.get_param());
            }
        }
        list_items = new DatabaseHelper(getApplicationContext()).get_app_users();
        setContentView(R.layout.frag_invite_frnds);
        findViewById(R.id.bottom).setVisibility(View.VISIBLE);
        list = (ListView)findViewById(R.id.list);
        list_adapter = new Invite_Friends_Adapter(getApplicationContext(),list_items,imageLoader,options);
        list.setAdapter(list_adapter);
        ed = (EText)findViewById(R.id.txt);
        invite = (TextB)findViewById(R.id.send);
        invite.setOnClickListener(this);
        sendtoall = (TextB)findViewById(R.id.sendtoall);
        sendtoall.setOnClickListener(this);
        if(event_id != null)
        {
            Event e = C.items.get(event_id);
            ed.setText("Event name 	: "+e.get_event_name()+"\n"+
                    "Event date : "+e.get_event_date()+"\n"+
                    "Address    :"+C.clubs.get(e.get_venue_id()).get_venue_address());
        }

        list.setOnItemClickListener(new OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int posi,
                                    long arg3) {


            }

        });

       /* Bundle b = this.getArguments();
        if(b != null)
        if(b.containsKey(PARAMS.EVENT_ID.get_param()))
        {
        	event_id = b.getString(PARAMS.EVENT_ID.get_param(),null);
        }
        */
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch(item.getItemId())
        {

            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    /*
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		list_items = new DatabaseHelper(getApplicationContext()).get_app_users();
		View v = inflater.inflate(R.layout.frag_invite_frnds, container, false);
		v.findViewById(R.id.bottom).setVisibility(View.VISIBLE);
		list = (ListView)v.findViewById(R.id.list);
		list_adapter = new Invite_Friends_Adapter(getApplicationContext(),list_items,imageLoader,options);
		list.setAdapter(list_adapter);
		ed = (EText)v.findViewById(R.id.txt);
		invite = (TextB)v.findViewById(R.id.send);
		invite.setOnClickListener(this);
		sendtoall = (TextB)v.findViewById(R.id.sendtoall);
		sendtoall.setOnClickListener(this);
		if(event_id != null)
		{
			Event e = C.items.get(event_id); 
			ed.setText("Event name 	: "+e.get_event_name()+"\n"+
						"Event date : "+e.get_event_date()+"\n"+
						"Address    :"+C.clubs.get(e.get_venue_id()).get_venue_address());
		}

		list.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int posi,
					long arg3) {
				
				
			}
			
		});

		
		return v;
	}
    */
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		AppEventsLogger.activateApp(this,getResources().getString(R.string.fb_app_id));
		
	}
	
	
	
    @Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		AppEventsLogger.deactivateApp(this,getResources().getString(R.string.fb_app_id));
		
	}
	/////////////////////////////////////////
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		String[] uids = null;
		switch(v.getId())
		{
		case R.id.send:
			ArrayList<WTP_Contact> contacts = list_adapter.get_selected();
			uids = new String[contacts.size()];
			for(int  i = 0;i < contacts.size();i++)
			{
				uids[i] = contacts.get(i).get_wtp_id();
				Log.d("uid", contacts.get(i).get_wtp_id());
			}
			new Invite_Task(this,uids,event_id).execute();
            app_tracker.send(new C().generate_analytics(C.CATEGORY.EVENTS.get_category(),C.ACTION.ACTION_INVITE.get_action(),C.LABEL.SINGLE.get_label()));

            finish();
			break;
		case R.id.sendtoall:
			uids = new String[list_items.size()];
			for(int  i = 0;i < list_items.size();i++)
			{
				uids[i] = list_items.get(i).get_wtp_id();
				
			}
			new Invite_Task(this,uids,event_id).execute();
            app_tracker.send(new C().generate_analytics(C.CATEGORY.EVENTS.get_category(),C.ACTION.ACTION_INVITE.get_action(),C.LABEL.ALL.get_label()));

            finish();
			break;
		}
	}
     
	
}
