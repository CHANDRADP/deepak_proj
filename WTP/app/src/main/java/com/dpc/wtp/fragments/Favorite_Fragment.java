package com.dpc.wtp.fragments;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.dpc.wtp.R;
import com.dpc.wtp.Models.Artist;
import com.dpc.wtp.Models.V_Details;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.activities.Activity_Artist_Details;
import com.dpc.wtp.activities.Activity_Club_Details;
import com.dpc.wtp.adapters.Favorite_Adapter;
import com.dpc.wtp.adapters.Following_Adapter;
import com.dpc.wtp.customs.MyFragment;
import com.facebook.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
public class Favorite_Fragment extends MyFragment
{
	private ArrayList<Artist> list_items_following;
	
	private ArrayList<V_Details> list_items_favorite;
	
	private Favorite_Adapter favorite_adapter;
	
	private Following_Adapter following_adapter;
	
	private ListView list;
	
	private int tab_no = 0;
	
	private Bundle b;
	
	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setHasOptionsMenu(true);
        app_tracker.setScreenName(C.SCREEN.SCREEN_FAVORITES.get_screen());
        app_tracker.send(new HitBuilders.AppViewBuilder().build());
        options = new DisplayImageOptions.Builder()
    	.showStubImage(R.drawable.artist)
    	.showImageForEmptyUri(R.drawable.artist)
    	.showImageOnFail(R.drawable.artist)
    	.cacheInMemory(true)
    	.cacheOnDisc(true)
    	.bitmapConfig(Bitmap.Config.RGB_565)
    	.build();
        b = this.getArguments();
        if(b != null)
	        if(b.containsKey(PARAMS.FAVORITE.get_param()))
	        {
	        	tab_no = Integer.parseInt(b.getString(PARAMS.FAVORITE.get_param(),null));
	        }
        
    }
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.frag_invite_frnds, container, false);
		list = (ListView)v.findViewById(R.id.list);
		
		
		

		list.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int posi,
					long arg3) {
				Intent i;
				if(tab_no == 1)
				{
					i = new Intent(getSherlockActivity().getApplicationContext(),Activity_Artist_Details.class);
					i.putExtra(PARAMS.ARTIST_ID.get_param(), list_items_following.get(posi).get_id());
				}
				else
				{
					i = new Intent(getSherlockActivity().getApplicationContext(),Activity_Club_Details.class);
					i.putExtra(PARAMS.VD_ID.get_param(), list_items_favorite.get(posi).get_vd_id());
				}
				startActivity(i);
				
			}
			
		});
		
		
		return v;
	}
	
	
	
	
	
	
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		AppEventsLogger.deactivateApp(this.getSherlockActivity(),getResources().getString(R.string.fb_app_id));
		
		getSherlockActivity().unregisterReceiver(NewMessageReceiver);
	}



	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		AppEventsLogger.activateApp(this.getSherlockActivity(),getResources().getString(R.string.fb_app_id));
		
		getSherlockActivity().registerReceiver(NewMessageReceiver, new IntentFilter(
				C.BROADCAST_MSG));
		new Update().execute();
	}
	//////////////////////////////
	public class Update extends AsyncTask<String, Long, Boolean> {
		
		@Override
		protected Boolean doInBackground(String... params) {
			if(tab_no == 1)
			{
				list_items_following = new ArrayList<Artist>();
				for(Artist a : C.artists.values())
				{
					if(a.is_following())
					{
						list_items_following.add(a);
					}
				}
				
			}
			else if(tab_no == 2)
			{
				list_items_favorite = new ArrayList<V_Details>();
				
			}
			return null;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			
			if(tab_no == 1)
			{
				following_adapter = new Following_Adapter(getSherlockActivity().getApplicationContext(),list_items_following,imageLoader,options);
				list.setAdapter(following_adapter);
			}
			else if(tab_no == 2)
			{
				favorite_adapter = new Favorite_Adapter(getSherlockActivity().getApplicationContext(),list_items_favorite,imageLoader,options);
				list.setAdapter(favorite_adapter);
			}

		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
		}
}
	
    /////////////////////////////////////////
    
	private final BroadcastReceiver NewMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent i) 
		{
			if(i.hasExtra(PARAMS.ARTIST.get_param()))
			{
				update(context);
			}
		}
	};
	
}
