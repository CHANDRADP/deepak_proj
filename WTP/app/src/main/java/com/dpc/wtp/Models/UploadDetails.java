package com.dpc.wtp.Models;


public class UploadDetails
{
    private String textmsg, serverurl;
    private int mediatype, rowId, isgroup, to;


    public UploadDetails(String textmsg, int mediatype, int rowid, String serverurl, int isgroup, int to_userid) {

        this.textmsg = textmsg;
        this.mediatype = mediatype;
        this.rowId = rowid;
        this.serverurl = serverurl;
        this.isgroup = isgroup;
        this.to = to_userid;

    }

    public int getMediatype() {
        return mediatype;
    }

    public void setMediatype(int mediatype) {
        this.mediatype = mediatype;
    }

    public int getRowId() {
        return rowId;
    }

    public void setRowId(int rowId) {
        this.rowId = rowId;
    }

    public int getIsgroup() {
        return isgroup;
    }

    public void setIsgroup(int isgroup) {
        this.isgroup = isgroup;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public String getServerurl() {
        return serverurl;
    }

    public void setServerurl(String serverurl) {
        this.serverurl = serverurl;
    }



    public String getTextmsg() {
        return textmsg;
    }

    public void setTextmsg(String textmsg) {
        this.textmsg = textmsg;
    }
}
