package com.dpc.wtp.Utils;

import java.util.Comparator;

import com.dpc.wtp.Models.WTP_Contact;

public class User_Comparator implements Comparator<WTP_Contact> {
    public int compare(WTP_Contact c1, WTP_Contact c2) {
        return c1.get_name().compareTo(c2.get_name());
    }
}