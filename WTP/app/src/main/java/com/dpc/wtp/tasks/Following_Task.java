package com.dpc.wtp.tasks;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.dpc.wtp.JSONParser;
import com.dpc.wtp.Models.Server_Params;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.activities.App;

public class Following_Task extends AsyncTask<String, Long, Boolean> {
	private boolean add = false;
	private String artist_id;
	Context c;

	public Following_Task(Context c,String artist_id,boolean add) {
		this.c = c;
		this.artist_id = artist_id;
		Log.d("artist_id", artist_id);
		this.add = add;
		
	}

	

	@Override
	protected Boolean doInBackground(String... params) {
		
		return update_favorite(this.artist_id);
	}

	@Override
	protected void onPostExecute(Boolean result) {
		//dialog.dismiss();
		if (result) 
		{
			
			
			if(add)
			{
				
				C.alert(c, "Favorite added successfully");
				C.update_favorites(artist_id, add);
				
			}
			else
			{
				
				C.alert(c, "Favorite removed successfully");
				C.update_favorites(artist_id, add);
				
			}
			
			
			
		}
		else
		{
			C.alert(c, "Error in updating favorite");
		}
		Intent i = new Intent(C.BROADCAST_MSG);
		i.putExtra(PARAMS.ARTIST.get_param(), "1");
		c.sendBroadcast(i);

	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
	}
	
	public boolean update_favorite(String id)
	{
		boolean success = false;
		JSONParser parser = new JSONParser();
		JSONObject user = null;
		try {
		if(add)
		{
		user = parser.makeHttpRequest(C.SERVER_URL, "POST",
				new Server_Params().get_add_follow_params(App.get_userid(), this.artist_id));
			if(user.getInt(PARAMS.STATUS.get_param()) > 0)
			{
				success = true;
			}
		}
		else
		{
			user = parser.makeHttpRequest(C.SERVER_URL, "POST",
					new Server_Params().get_remove_follow_params(App.get_userid(), this.artist_id));
			if(user.getInt(PARAMS.STATUS.get_param()) > 0)
			{
				success = true;
			}
		}
		
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return success;
	}

}