package com.dpc.wtp.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.dpc.wtp.fragments.Active_Users_Fragment;
import com.dpc.wtp.fragments.Groups_Fragment;
import com.dpc.wtp.fragments.NonActive_Users_Fragment;

public class Share_ViewPager_Adapter extends FragmentPagerAdapter {

    public Share_ViewPager_Adapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                Log.d("Calling active users", "Hi");
                return new Active_Users_Fragment();
            case 1:
                Log.d("Calling non active users", "Hi");
                return new NonActive_Users_Fragment();
            case 2:
                Log.d("Calling group users", "Hi");
                return new Groups_Fragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 3;
    }
}
