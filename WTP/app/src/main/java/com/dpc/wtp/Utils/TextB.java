package com.dpc.wtp.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextB extends TextView{
    public TextB(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextB(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextB(Context context) {
        super(context);
        init();
    }

    private void init() {
    	
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"Proximab.otf");
        setTypeface(tf);
    //	setTypeface(Typeface.DEFAULT_BOLD);
    }

}
