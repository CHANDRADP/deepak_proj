package com.dpc.wtp.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class EText extends EditText{
    public EText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public EText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EText(Context context) {
        super(context);
        init();
    }

    private void init() {
    	Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"Proxima.otf");
        setTypeface(tf);
    }

}
