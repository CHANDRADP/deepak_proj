package com.dpc.wtp.custom_views;

import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by D-P-C on 23/12/2014.
 */
public abstract class ScrollTouchListener extends GestureDetector.SimpleOnGestureListener implements View.OnTouchListener {

    private double x = 0;

    private double y = 0;


    public ScrollTouchListener()
    {

    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
                            float distanceY) {

        Log.d("scrolling","working");
        if (e1.getY() > e2.getY())
        {
            onUpScrolling();

        }
        else
        {
            onDownScrolling();

        }

        return super.onScroll(e1, e2, distanceX, distanceY);
    }
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        final float distanceY = Math.abs(e1.getY() - e2.getY());
        final float distanceX = Math.abs(e1.getX() - e2.getX());
    	if (e1.getY() > e2.getY()) // bottom to up
			{
				onUpScrolling();

			}
			else
			{
				onDownScrolling();

			}




         if (e1.getX() > e2.getX()) // bottom to up
            {
                onLeftScrolling();

            }
            else
            {
                onRightScrolling();

            }


        return false;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return true;
    }
    @Override
    public boolean onTouch(View view, MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                // User started scrolling
                x = event.getX();
                y = event.getY();
                onScrollStarted(x, y);
                return true;

            case MotionEvent.ACTION_UP:
                // User stopped scrolling
                x = event.getX();
                y = event.getY();
                onScrollEnded(x, y);
                return true;

            case MotionEvent.ACTION_MOVE:

                // Finger was moved get new x and y coordinates
                // and calculate the difference
                double newX = event.getX();
                double newY = event.getY();

                double difX =  x - newX;
                double difY =  y - newY;

                onScroll(difX, difY);

                x = newX;
                y = newY;
                return true;

            default:
                return false;
        }
    }

    protected abstract void onScrollStarted(double x, double y);
    protected abstract void onScroll(double deltaX, double deltaY);
    protected abstract void onScrollEnded(double x, double y);
    protected abstract void onUpScrolling();

    protected abstract void onDownScrolling();

    protected abstract void onLeftScrolling();

    protected abstract void onRightScrolling();
}