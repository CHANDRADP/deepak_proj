package com.dpc.wtp.adapters;


import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.Html.ImageGetter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.dpc.wtp.R;
import com.dpc.wtp.activities.ActivityChatWindow;

public class Sticker_Adapter extends BaseAdapter{
	private Context cxt;
	private static LayoutInflater inflater = null;
	private boolean sticker = false;
	
	public static final int[] stickers = new int[] {R.drawable.a,
		R.drawable.b,
		R.drawable.c,
		R.drawable.d,
		R.drawable.e,
        R.drawable.f,
        R.drawable.g,
        R.drawable.h,
        
		};
	
	public Sticker_Adapter(Context c,boolean sticker) {
		this.cxt = c;
		this.sticker = sticker;
		inflater = (LayoutInflater)cxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	@Override
	public int getCount() {
		if(!sticker)
		    return ActivityChatWindow.stickers.length;
		else
			return stickers.length;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView == null){
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.smiley_bg, null);
			holder.sticker = (ImageView)convertView.findViewById(R.id.imageView1);
			convertView.setTag(holder);
		}
		else{
			holder = (ViewHolder)convertView.getTag();
		}
		if(sticker)
		holder.sticker.setImageResource(stickers[position]);
		else
			holder.sticker.setImageResource(ActivityChatWindow.stickers[position]);
		return convertView;
	}
	public static class ViewHolder{
		public ImageView sticker;
	}

}
