/**
 * @author D-P-C
 * Jul 22, 2014 2014

 */
package com.dpc.wtp.Utils;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.CalendarContract.Events;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.PhoneLookup;
import android.provider.Settings;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.Element;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;
import android.text.Spannable;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dpc.wtp.Models.Ads;
import com.dpc.wtp.Models.Lineup;
import com.dpc.wtp.R;
import com.dpc.wtp.Models.Artist;
import com.dpc.wtp.Models.Deals;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Models.Guestlist;
import com.dpc.wtp.Models.Images;
import com.dpc.wtp.Models.Review;
import com.dpc.wtp.Models.Tracks;
import com.dpc.wtp.Models.V_Details;
import com.dpc.wtp.Models.WTP_Contact;
import com.dpc.wtp.activities.ActivityChatWindow;
import com.dpc.wtp.activities.App;
import com.dpc.wtp.customs.ProgressPieView;
import com.google.android.gms.analytics.HitBuilders;

/**
 * @author D-P-C
 * 
 */
//phone_type
//1 => android
//2 => iphone
//3 => windows

///////////////////////////////////////////////////////////////////////////////////////

//Venue types 

// type == 1 => bar 
//					sub_type == 1 => lounge bar
//					sub_type == 2 => micro brewery
//					sub_type == 3 => restro bar
//					sub_type == 4 => sports bar

// type == 2 => night_club
// type == 3 => pub
// type == 4 => micro brewery
// type == 5 => concerts

///////////////////////////////////////////////////////////////////////////////////////
//type == 1 => cover_charge,entry_price,dress_code,guest_list
//type == 2 => cover_charge,entry_price,dress_code,guest_list
//type == 3 or 4 => cover_charge,entry_price,dress_code,guest_list
//type == 5 => ticket_price
//if(guest_list = 0) => No guestlist is required,walkin free

///////////////////////////////////
//Artist music types

//Bollywood => 1
//Deep House => 2
//EDM => 3
//Electronic => 4
//Hip Hop => 5
//House => 6
//Jazz => 7
//Pop => 8
//Progressive House => 9
//Psy- Trance => 10
//RnB => 11
//Rock => 12
//Techno => 13
//Techno House => 14
//Trance => 15







public class C {

    //Amazon account details
    public static final String S3_AK = "AKIAJ35OY3433F2MDGWA";
    public static final String S3_SK = "5LpNvQvsibrS9JT8cGMdNfmGrEssdG7g75FMwJLd";
    public static final String S3_BUCKET = "wtp-user-pics";
    public static final String S3_IMG_PREFIX = "https://s3.amazonaws.com/wtp-user-pics/";
    //Twitter details
	public static String TWITTER_CONSUMER_KEY = "8FNdlyZMdn9YYuZq1hZCwoXxE";
    public static String TWITTER_CONSUMER_SECRET = "KDz8vIpWxzuvzubzEzc10XK6qKeAufIT5wbUxiLySFQ3JmgwYR";
    public static String TWITTER_CALLBACK_URL_EVENT = "oauth://com.dpc.wtp.event";
   // public static String TWITTER_CALLBACK_URL_LOGIN = "oauth://com.dpc.wtp.login";
    public static String URL_PARAMETER_TWITTER_OAUTH_VERIFIER = "oauth_verifier";
    public static String PREFERENCE_TWITTER_OAUTH_TOKEN="TWITTER_OAUTH_TOKEN";
    public static String PREFERENCE_TWITTER_OAUTH_TOKEN_SECRET="TWITTER_OAUTH_TOKEN_SECRET";
    public static String PREFERENCE_TWITTER_IS_LOGGED_IN="TWITTER_IS_LOGGED_IN";
    public static String STRING_EXTRA_AUTHENCATION_URL = "AuthencationUrl";
    /////////////////////////////////////////////////////////////
    
    public static final String app_logo = "https://s3.amazonaws.com/wtp-user-pics/wtp/512.png";
    
    public static final String app_caption = "WTP Mobile App for Event and Party information";
	
    public static final String app_info = "WTP provides the complete lowdown on the happening music events and parties at your favorite clubs in your city.Download WTP on your mobile to make life simpler and easy.";
	
    public static final String event_caption = "Dance Smile Giggle and Marvel";
    
    public static final String app_url = "http://wtp.club";
    
    public static Location location = null;



    public static ArrayList<Ads> event_ads = new ArrayList<Ads>();

    public static ArrayList<Ads> club_ads = new ArrayList<Ads>();

    public static ArrayList<Ads> deal_ads = new ArrayList<Ads>();

    public static ArrayList<Ads> artist_ads = new ArrayList<Ads>();

    public static ArrayList<Ads> full_ads = new ArrayList<Ads>();
	
	public static ArrayList<ArrayList<Event>> events = new ArrayList<ArrayList<Event>>();
	
	public static HashMap<String,Event> items = new HashMap<String,Event>();
	
	public static HashMap<String,V_Details> clubs = new HashMap<String,V_Details>();
	
	public static HashMap<String,ArrayList<Deals>> deals = new HashMap<String,ArrayList<Deals>>();
	
	//public static ArrayList<Deals> my_deals = new ArrayList<Deals>();
	
	//public static ArrayList<Guestlist> my_guestlist = new ArrayList<Guestlist>();
	
	public static HashMap<String,Artist> artists = new HashMap<String,Artist>();

    public final static int ANIM_DURATION = 2;



    // Chat Constants

    public static final int RESULT_LOAD_IMAGE = 1;
    public static final int RESULT_LOAD_AUDIO = 2;
    public static final int RESULT_LOAD_VIDEO = 3;
    public static final int RESULT_RECORD_AUDIO = 4;

    public static int MSG_ID_OF_PLAYED_AUDIO = -1;
    public static final int DEFAULT = 7;
    public static final int PLAYING = 8;
    public static final int PAUSED = 9;

    public static final int MESSAGE = 1;
    public static final int IMAGE = 2;
    public static final int AUDIO = 3;
    public static final int VIDEO = 4;
    public static final int STICKER = 5;

    public static final int GROUP_MEMBERS_UPDATE = 6;
    public static final int GROUP_INVITE = 7;
    public static final int GROUP_NAME_UPDATE = 8;
    public static final int DELIVERY_ACKNOWLEDGEMENT = 9;
    public static final int TYPING_STATUS = 10;
    public static final int ONLINE_STATUS_REQUEST = 11;
    public static final int ONLINE_STATUS_REPLY = 12;


    public static final int ISDATE = 1;
    public static final int ISNOTDATE = 0;

    public static final int TYPING = 1;
    public static final int STOPPED_TYPING = 0;

    public static final int ISGROUP = 1;
    public static final int ISNOTGROUP = 0;

    public static final int POSTER_ME = 1;
    public static final int POSTER_FRIEND = 0;

    public static final int STATUS_READ = 1;
    public static final int STATUS_UNREAD = 0;

    public static final int STATUS_NOT_SENT = 0;
    public static final int STATUS_SENT = 1;
    public static final int STATUS_DELIVERED = 2;
    public static final int STATUS_RECEIVED = 3;

    public static final int NOT_DOWNLOADED = 1;
    public static final int DOWNLOADING = 2;
    public static final int DOWNLOADED = 3;

    public static final int NOT_UPLOADED = 4;
    public static final int UPLOADING = 5;
    public static final int UPLOADED = 6;

    public static  int MY_CHAT_STATUS = 0;
    public static final int ME_ONLINE = 1;
    public static final int ME_OFFLINE = 0;

    public static  int MY_ONLINE_STATUS = 0;


    public static int ACTIVITY_CHAT_WINDOW_RUNNING_STATUS = 0;
    public static final int ACTIVITY_RUNNING = 1;
    public static final int ACTIVITY_STOPPED = 0;





    public static void update_items(Event event)
	{
		items.remove(event.get_event_id());
		items.put(event.get_event_id(), event);
	}
	public static void update_clubs(V_Details club)
	{
		clubs.remove(club.get_vd_id());
		clubs.put(club.get_vd_id(), club);
	}
	
	
	
	public static void update_favorites(String artist_id,boolean added)
	{
		for(Artist a : artists.values())
		{
			if(a.get_id().equals(artist_id))
			{
				a.set_following(added ? "1" : "-1");
				int count = Integer.parseInt(a.get_following_count()) + (added ? 1 : -1);
				a.set_following_count(String.valueOf(count));
				artists.put(a.get_id(), a);
			}
		}

		
	}
	public static final String BROADCAST_MSG = "com.dpc.wtp.listupdate";
	
	public static final String BROADCAST_MSG_CLUB = "com.dpc.wtp.club";
	
	public static final String SENDER_ID = "999488980802";

	public static final String SERVER_URL = "http://ec2-54-148-132-161.us-west-2.compute.amazonaws.com/apis/Actions.php";// "http://tracksdpc.site88.net/WTP_Test/Actions.php";

    public static final String TC_URL = "http://wtp.club/inner1/tandc.html";

    public Map<String, String> generate_analytics(String category, String action, String label)
    {
        return new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .setLabel(label)
                .setValue(1)
                .build();
    }
    public static enum SCREEN {
        SCREEN_LOGIN("Screen_Login"),
        SCREEN_REGISTER("Screen_Register"),
        SCREEN_PROFILE("Screen_User_Profile"),
        SCREEN_CITY("Screen_City"),
        SCREEN_HOME("Screen_Home"),
        SCREEN_EVENTS("Screen_Events"),
        SCREEN_VENUES("Screen_venues"),
        SCREEN_DEALS("Screen_Deals"),
        SCREEN_ARTISTS("Screen_Artists"),
        SCREEN_EVENT_DETAILS("Screen_Event_Details"),
        SCREEN_VENUE_DETAILS("Screen_Venue_Details"),
        SCREEN_DEAL_DETAILS("Screen_Deal_Details"),
        SCREEN_ARTIST_DETAILS("Screen_Artist_Details"),
        SCREEN_INVITE_FRIENDS("Screen_Invite_friends"),
        SCREEN_MY_APPROVALS("Screen_My_Approvals"),
        SCREEN_NEARBY_PLACES("Screen_Nearby_Places"),
        SCREEN_SETTINGS("Screen_Settings"),
        SCREEN_CAB("Screen_Cab"),
        SCREEN_CALENDAR("Screen_Calendar"),
        SCREEN_FAVORITES("Screen_Favorites"),
        ;
        // Constructor
        private SCREEN(final String screen) {
            this.screen = screen;
        }

        // Internal state
        private String screen;

        public String get_screen() {
            return screen;
        }
    };

    public static enum CATEGORY {
        APP_SCREEN("APP_SCREENS"),
        LOGIN("APP_LOGIN"),
        EVENTS("EVENTS"),
        VENUES("VENUES"),
        DEALS("DEALS"),
        ARTISTS("ARTISTS"),
        BAR("BAR"),
        NIGHT_CLUB("NIGHT_CLUB"),
        PUB("PUB"),
        MICRO_BREWERY("MICRO_BREWERY"),
        FOOD("FOOD"),
        HAPPY_HOURS("HAPPY_HOURS"),
        HOT_DEALS("HOT_DEALS"),
        BOLLYWOOD("BOLLYWOOD"),
        DEEPHOUSE("DEEP_HOUSE"),
        EDM("EDM"),
        ELECTRONIC("ELECTRONIC"),
        HIPHOP("HIP-HOP"),
        HOUSE("HOUSE"),
        JAZZ("JAZZ"),
        POP("POP"),
        PROGRESSIVEHOUSE("PROGRESSIVE_HOUSE"),
        PSY_TRANCE("PSY-TRANCE"),
        RNB("RnB"),
        ROCK("ROCK"),
        TECHNO("TECHNO"),
        TECHNO_HOUSE("TECHNO HOUSE"),
        TRANCE("TRANCE"),
        CAB("CAB"),
        APP_SHARE("APP_SHARE"),
        EVENT_SHARE("EVENT_SHARE"),
        RATING("RATING"),
        FAVORITE("FAVORITE"),
        SEARCH("SEARCH"),
        SLIDE_MENU("SLIDE_MENU"),
        LOCATION("CHANGE_LOCATION"),
        ;
        // Constructor
        private CATEGORY(final String category) {
            this.category = category;
        }

        // Internal state
        private String category;

        public String get_category() {
            return category;
        }
    };
    public static enum ACTION {
        ACTION_OK("Select"),
        ACTION_CANCEL("Cancel"),
        ACTION_ATTEND("Attend"),
        ACTION_FAVORITE("Favorite"),
        ACTION_REVIEW("Review"),
        ACTION_CALL("Call"),
        ACTION_VISITED("Visited"),
        ACTION_SHARE("Share"),
        ACTION_REQUEST("Request"),
        ACTION_BUY("Buy_Ticket"),
        ACTION_INVITE("Invite_Friends"),
        ;
        // Constructor
        private ACTION(final String action) {
            this.action = action;
        }

        // Internal state
        private String action;

        public String get_action() {
            return action;
        }
    };
    public static enum LABEL {
        FB("Facebook"),
        TWIT("Twitter"),
        WTP("WTP"),
        YES("Yes"),
        NO("No"),
        BOOK_CAB("Book_cab"),
        SOUND_CLOUD("Sound_Cloud"),
        SINGLE("Single"),
        ALL("All"),
        ACTION_SHARE("Share"),

        ;
        // Constructor
        private LABEL(final String label) {
            this.label = label;
        }

        // Internal state
        private String label;

        public String get_label() {
            return label;
        }
    };


    public static enum CITY {
        NONE("none"),
        Bangalore("1"),
        Chennai("2"),
        Delhi("3"),
        Hydrabad("4"),
        Kolkata("5"),
        Mumbai("6"),
        Pune("7"),

        ;
        // Constructor
        private CITY(final String city) {
            this.city = city;
        }

        // Internal state
        private String city;

        public String get_city() {
            return city;
        }
    };


	public static enum METHOD {
								NONE("none"),
								INSERT_USER("InsertUser"),
								LOGIN("CheckUser"),
								LOGIN_FB("Check_fb_User"),
								LOGIN_TWITTER("Check_twit_User"),
								UPDATE_FB("Update_fb_User"),
								UPDATE_TWITTER("Update_twit_User"),
								SEND_PASSWORD("send_password"),
                                ADD_FEEDBACK("add_feedback"),
								GET_APP_USERS("     "),
								VENUE_DETAILS("v_details"),
								CONCERT_DETAILS("c_details"),
								ARTIST_DETAILS("artist_details"),
								ADD_FOLLOW("add_follow"),
								REMOVE_FOLLOW("remove_follow"),
								ADD_FAVORITE("add_favorite"),
								REMOVE_FAVORITE("remove_favorite"),
								ADD_PHOTO("add_photo"),
								UPDATE_PHOTO("update_photo"),
								UPDATE_STATUS("update_status"),
								ADD_ATTEND("add_attend"),
								UPDATE_ATTEND("update_attend"),
								SEARCH_VENUES("search_venues"),
								SEARCH_BY_LOCATION("events_by_location"),
								ADD_STAR("add_star"),
								UPDATE_STAR("update_star"),
								ADD_GUEST("add_guest"),
                                DEAL_REQUEST("add_deal_request"),
								GET_E_DETAILS("get_e_details"),
								INVITE_TO_EVENT("invite_to_event"),
								UPDATE_GCMID("update_gcmid"),
								UPDATE_POINTS("update_points"),
								UPDATE_TUTRS("update_tuto"),
								ADD_REVIEW("add_review"),
                                UPDATE_APP_VERSION("update_version"),
								;
		// Constructor
		private METHOD(final String method) {
			this.method = method;
		}

		// Internal state
		private String method;

		public String get_method() {
			return method;
		}
	};

	public static enum PARAMS {
		NONE("none"),PHONE_TYPE("phone_type"), DATA("data"), USER_NAME("user_name"), USER_ID("user_id"),GENDER("gender"), EMAIL(
				"email_id"), PASSWORD("password"), C_CODE("c_code"), NUMBER(
				"number"), DOB("dob"), IMAGE("image"), THUMBNAIL("thumbnail"), GCM_ID(
				"gcm_id"),TUTORIAL("tuto"), TIME("u_time"), FACEBOOK_ID("fb_id"), TWITTER_ID(
				"twit_id"),FACEBOOK_NAME("fb_name"),TWITTER_NAME("twit_name"), STATUS("status"), V_ID("v_id"), VD_ID("vd_id"), C_ID("c_id"), TYPE(
				"type"),BAR_TYPE("bar_type"),CUISINE("cuisine"), MUSIC_TYPE("m_type"), NAME("name"), PARTY_NAME(
				"party_name"), LANGUAGE("language"), CONTACT_INFO(
				"contact_info"), DATE("date"),PERCENTAGE("percentage"),S_DATE("s_date"),E_DATE("e_date"), S_TIME("s_time"), E_TIME(
				"e_time"), LOCATION("location"), ADDRESS("address"), AREA(
				"area"), POST_CODE("post_code"), CITY("city"), STATE("state"), PLACE(
				"place"), DJ_NAME("dj_name"), DJ_LINEUP("dj_lineup"), GENRE(
				"genre"), DRESS_CODE("dress_code"), ENTRY_RULE("entry_rule"), ENTRY_PRICE(
				"entry_price"),GUEST_LIST("guest_list"), GOING_COUNT("going_count"), MAYBE_COUNT("maybe_count"), COVER_CHARGE(
				"cover_charge"), DEALS("deals"), ARTIST_ID("artist_id"), TICKET_CLASS(
				"t_class"), TICKEC_PRICE("ticket_price"), BID_OFFER("bid_offer"), FAVORITE_COUNT(
				"f_count"), IMAGE_ID("image_id"), FOLLOW_COUNT(
				"f_count"), EVENT_ID("e_id"), DETAILS("details"), TRACK_ID(
				"track_id"), FF_ID("ff_id"), IMAGES("images"),LIQUOR_MENU("liquor_menu"), THUMBNAILS(
				"thumbnails"), ARTIST("artist"), TRACKS("tracks"), FOLLOWING(
				"is_following"), ID("id"),ATTEND("attend"),ATTEND_TYPE("attend_type"),PHONE_MODEL("phone_model"),VERSION("version"),
				VENUE("venue"),VENUE_DETAILS("venue_details"),FAVORITE("is_favorite"),
				FOLLOW("follow"),STARS("stars"),STAR_ID("star_id"),STAR_NO("star_no"),
				GUEST_COUNT("guest_count"),IN_GUEST_LIST("in_guest_list"),GUEST_TIME("guest_list_time"),
				GUEST_ID("guest_id"),DEAL_ID("deal_id"),RATING("rating"),MESSAGE("message"),REVIEW("review"),REVIEWS("reviews"),POINTS("points"),URL("url"),TICKET_LINK("ticket_link"),CAPTION("caption"),UNIQUE_CODE("code"),TOKEN("token"),TOKEN_STR("token_str"),MAP("map"),PRIORITY("priority"), AUDIO("audio"), VIDEO("video");
		// Constructor
		private PARAMS(final String method) {
			this.param = method;
		}

		// Internal state
		private String param;

		public String get_param() {
			return param;
		}
	};
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void blur_it(Activity a)
    {
        Bitmap bitmapOriginal = BitmapFactory.decodeResource(a.getResources(), R.drawable.a_bg);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            RenderScript rs = RenderScript.create(a);
            final Allocation input = Allocation.createFromBitmap(rs, bitmapOriginal); //use this constructor for best performance, because it uses USAGE_SHARED mode which reuses memory
            final Allocation output = Allocation.createTyped(rs, input.getType());
            final ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
            script.setRadius(8f);
            script.setInput(input);
            script.forEach(output);
            output.copyTo(bitmapOriginal);
        }
        else
        {

            int numPixels = bitmapOriginal.getWidth()* bitmapOriginal.getHeight();
            int[] pixels = new int[numPixels];

            //Get JPEG pixels.  Each int is the color values for one pixel.
            bitmapOriginal.getPixels(pixels, 0, bitmapOriginal.getWidth(), 0, 0, bitmapOriginal.getWidth(), bitmapOriginal.getHeight());

            //Create a Bitmap of the appropriate format.
            bitmapOriginal = Bitmap.createBitmap(bitmapOriginal.getWidth(), bitmapOriginal.getHeight(), Bitmap.Config.ARGB_8888);

            //Set RGB pixels.
            bitmapOriginal.setPixels(pixels, 0, bitmapOriginal.getWidth(), 0, 0, bitmapOriginal.getWidth(), bitmapOriginal.getHeight());


        }
        ((View)a.getWindow().getDecorView().findViewById(android.R.id.content)).setBackgroundResource(R.drawable.a_bg);

    }
	public static Intent getOpenFacebookIntent(Context context,String id,String name) {

		   try {
			   if(id != null)
			   {
				   context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
				   return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/"+id));
			   }
			   else
			   {
				   return new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/"+name));
			   }
		   } catch (Exception e) {
		    return new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/"+name));
		   }
		}
	public static Intent getTwitterIntent(Context c,String id,String name)
	{
		Intent intent = null;
		try {
			if(id != null)
			   {
				// get the Twitter app if possible
				c.getPackageManager().getPackageInfo("com.twitter.android", 0);
				intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?user_id="+id));
			   }
			else
			{
				intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/"+name));
			}
		    //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		} catch (Exception e) {
		    // no Twitter app, revert to browser
		    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/"+name));
		}
		return intent;
	}

    public static boolean is_olderday(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            Log.d("erroe","The dates must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();

        cal2.setTime(date2);
        return is_olderday(cal1, cal2);
    }
    public static boolean is_olderday(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.DAY_OF_YEAR) < cal2.get(Calendar.DAY_OF_YEAR));
    }
	public static String get_date_suffix(String date)
	{
		
		if(date.endsWith("1"))
    	{
			//date += " st";
    	}
    	else if(date.endsWith("2"))
    	{
    		//date += " nd";
    	}
    	else if(date.endsWith("3"))
    	{
    		//date += " rd";
    	}
    	else
    	{
    		//date += " th";
    	}
		return date;
	}
	
	@TargetApi(Build.VERSION_CODES.FROYO)
	public  Bitmap get_image(String thumb)
	{
		Bitmap bitmap = null;
		if(thumb != null)
		{
			byte[] decodedString = Base64.decode(thumb, Base64.DEFAULT);
			bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
		}
		
		return bitmap;
	}
	public  Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
	    Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
	         bitmap.getHeight(), Config.ARGB_8888);
	    Canvas canvas = new Canvas(output);

	    final int color = 0xff424242;
	    final Paint paint = new Paint();
	    final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
	    final RectF rectF = new RectF(rect);
	    final float roundPx = 20;

	    paint.setAntiAlias(true);
	    canvas.drawARGB(0, 0, 0, 0);
	    paint.setColor(color);

	    canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

	    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
	    canvas.drawBitmap(bitmap, rect, rect, paint);

	    return output;
	    }
	public static int[] set_rating(int count)
	{
		int stars[] = new int[]{R.drawable.star_n,R.drawable.star_n,R.drawable.star_n,R.drawable.star_n,R.drawable.star_n};
		if(count == 1)
			stars[0] = R.drawable.star_p;
		else if(count == 2)
			stars[0] = stars[1] = R.drawable.star_p;
		else if(count == 3)
			stars[0] = stars[1] = stars[2] = R.drawable.star_p;
		else if(count == 4)
			stars[0] = stars[1] = stars[2] = stars[3] = R.drawable.star_p;
		else if(count == 5)
			stars[0] = stars[1] = stars[2] = stars[3] = stars[4] = R.drawable.star_p;
		return stars;
	}
	public  int[] set_stars(int count)
	{
		int stars[] = new int[]{R.drawable.rate_n,R.drawable.rate_n,R.drawable.rate_n,R.drawable.rate_n,R.drawable.rate_n};
		if(count == 1)
			stars[0] = R.drawable.rate_p;
		else if(count == 2)
			stars[0] = stars[1] = R.drawable.rate_p;
		else if(count == 3)
			stars[0] = stars[1] = stars[2] = R.drawable.rate_p;
		else if(count == 4)
			stars[0] = stars[1] = stars[2] = stars[3] = R.drawable.rate_p;
		else if(count == 5)
			stars[0] = stars[1] = stars[2] = stars[3] = stars[4] = R.drawable.rate_p;
		return stars;
	}
	
	public static void add_app_users_todb(Context c,JSONObject user)
	{
		try {
			
			JSONArray users = user.getJSONArray(PARAMS.DATA.get_param());
			for(int i = 0;i < users.length();i++)
			{
				JSONObject _user = users.getJSONObject(i);
				
				WTP_Contact contact = new WTP_Contact(_user.getString(PARAMS.USER_ID.get_param()),
												  _user.getString(PARAMS.USER_NAME.get_param()),
												  _user.getString(PARAMS.C_CODE.get_param()),
												  _user.getString(PARAMS.NUMBER.get_param()),
												  _user.getString(PARAMS.STATUS.get_param()),
												  _user.isNull(PARAMS.THUMBNAIL.get_param()) ? null : _user.getString(PARAMS.THUMBNAIL.get_param()),
												  _user.getString("u_time"),
												  C.number_Exists(c, _user.getString(PARAMS.NUMBER.get_param()))[0]
												  );
				DatabaseHelper h = new DatabaseHelper(c);
				h.add_app_user(contact);
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void update_users(Context c,JSONObject user)
	{
try {
			
			JSONArray users = user.getJSONArray(PARAMS.DATA.get_param());
			for(int i = 0;i < users.length();i++)
			{
				JSONObject _user = users.getJSONObject(i);
				String contact_id = null;
				String[] contact_d = C.number_Exists(c, _user.getString(PARAMS.NUMBER.get_param()));
				if(contact_d != null)
				{
					contact_id = contact_d[0];
				}
						  
				WTP_Contact contact = new WTP_Contact(_user.getString(PARAMS.USER_ID.get_param()),
												  _user.getString(PARAMS.USER_NAME.get_param()),
												  _user.getString(PARAMS.C_CODE.get_param()),
												  _user.getString(PARAMS.NUMBER.get_param()),
												  _user.getString(PARAMS.STATUS.get_param()),
												  _user.isNull(PARAMS.THUMBNAIL.get_param()) ? null : _user.getString(PARAMS.THUMBNAIL.get_param()),
												  _user.getString("u_time"),
												  contact_id
												  );
				DatabaseHelper h = new DatabaseHelper(c);
				h.update_app_users(contact);
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void alert(Context c, String msg) {
		Toast.makeText(c, msg, Toast.LENGTH_SHORT).show();
	}
	public static void net_alert(Context c) {
		Toast.makeText(c, "No internet connection", Toast.LENGTH_SHORT).show();
	}

	@SuppressLint("SimpleDateFormat")
	public static String getDateDifference(String date) {
		
		DateFormat sdf = null;
		if(date.contains("/"))
		{
			sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		}
		else if(date.contains("-"))
		{
			sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		}
		Date thenDate = null;
		try {
			thenDate = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Calendar now = Calendar.getInstance();
		Calendar then = Calendar.getInstance();
		now.setTime(new Date());
		then.setTime(thenDate);

		// Get the represented date in milliseconds
		long nowMs = now.getTimeInMillis();
		long thenMs = then.getTimeInMillis();

		// Calculate difference in milliseconds
		long diff = nowMs - thenMs;

		// Calculate difference in seconds
		long diffMinutes = diff / (60 * 1000);
		long diffHours = diff / (60 * 60 * 1000);
		long diffDays = diff / (24 * 60 * 60 * 1000);

		if (diffMinutes < 60) {
			if (diffMinutes == 1)
				return diffMinutes + " minute ago";
			else if(diffMinutes < 0)
				return "0 minutes ago";
			else
				return diffMinutes + " minute ago";
		} else if (diffHours < 24) {
			if (diffHours == 1)
				return diffHours + " hour ago";
			else
				return diffHours + " hours ago";
		} else if (diffDays < 30) {
			if (diffDays == 1)
				return diffDays + " day ago";
			else
				return diffDays + " days ago";
		} else {
			return "a long time ago..";
		}
	}

	@SuppressLint("SimpleDateFormat")
	public static String get_date() {
		
		SimpleDateFormat dateFormatutc = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		dateFormatutc.setTimeZone(TimeZone.getTimeZone("UTC"));

		//Local time zone   
		SimpleDateFormat dateFormatLocal = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		dateFormatLocal.format(new Date());
		//Time in GMT
		try {
			return dateFormatutc.format(dateFormatLocal.parse(dateFormatLocal.format(new Date())));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
	@SuppressLint("SimpleDateFormat")
	public static String get_universal_date(String time)
	{
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        df.setTimeZone(TimeZone.getDefault());
        String formattedDate = df.format(date);
        Log.d("time",formattedDate);
        return formattedDate;
	}

	@SuppressLint("SimpleDateFormat")
	public static String[] get_date_as_day_month(String date)
	{
        String values[] = new String[2];
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM");
		Date d = null;
		try {
			d = sdf1.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        if(d != null)
        {
            values = sdf2.format(d).toString().split("/");
        }
		return values;
	}
	public static ArrayList<String> readContacts(Context c) {
		ArrayList<String> nos = new ArrayList<String>();
		ContentResolver cr = c.getContentResolver();
		String sortOrder = Phone.DISPLAY_NAME
				+ " COLLATE LOCALIZED ASC";
		Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
				null, null, sortOrder);
		while (cursor.moveToNext()) {
			String name = "", phone = null;
			String id = cursor.getString(cursor
					.getColumnIndex(ContactsContract.Contacts._ID));
		if (Integer
					.parseInt(cursor.getString(cursor
							.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
				Cursor pCur = cr.query(
						Phone.CONTENT_URI,
						null, Phone.CONTACT_ID
								+ " = ?", new String[] { id }, null);
				while (pCur.moveToNext()) {
					phone = pCur
							.getString(pCur
									.getColumnIndex(Phone.NUMBER));
					name = cursor
							.getString(cursor
									.getColumnIndex(Phone.DISPLAY_NAME));
					
					if (phone != null ) 
					{
						if(phone.length() > 9 && phone.length() < 20)
						{
							String num = null;
							for ( int i = 0; i < phone.length(); ++i ) 
							{  
								char cha = phone.charAt( i );
								if(i == 0)
								{
									int chacode = cha;
									if(chacode == 43 || Character.isDigit(cha))
									{
										num += cha;
									}
								}
								else if(Character.isDigit(cha))
								{
									num += cha;
								}
							}
							phone = num;
						//	phone = phone.replaceAll("\\s+", "");
						//	phone = phone.replaceAll("[^+][^0-9]", "");
						//	phone = phone.replaceAll("\\s+", "");
						//	phone = phone.trim();
						if(phone.length() >= 10)
							{
								if(phone.length() > 10)
								{
									phone = phone.substring(phone.length()-10);
									
								}
								
								nos.add(phone);
								Log.i("name", phone + " : "+name);
							}
					}
					
					}
					phone = null;
				}
				pCur.close();
			}

		}
		cursor.close();
		return nos;

	}
	public static String[] number_Exists(Context c,String number) {
		String[] details = null;
		/// number is the phone number
		Uri lookupUri = Uri.withAppendedPath(
				PhoneLookup.CONTENT_FILTER_URI, 
		Uri.encode(number));
		String[] mPhoneNumberProjection = { PhoneLookup._ID, PhoneLookup.NUMBER, PhoneLookup.DISPLAY_NAME };
		Cursor cur = c.getContentResolver().query(lookupUri,mPhoneNumberProjection, null, null, null);
		try {
		   if (cur.moveToFirst()) 
		   {
			   details = new String[2];
			   details[0] = cur
						.getString(cur
								.getColumnIndex(Phone._ID));
			   details[1] = cur
						.getString(cur
								.getColumnIndex(Phone.DISPLAY_NAME));
		     
		}
		} 
		finally 
		{
		if (cur != null)
		   cur.close();
		}
		return details;
		}
	public static String Contact_Exists(Context c,String c_id) {
		String id = null;
		/// number is the phone number
		String[] mPhoneNumberProjection = { Phone._ID,Phone.DISPLAY_NAME};
		Cursor cur = c.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,mPhoneNumberProjection, Phone._ID+"="+c_id, null, null);
		try {
		   if (cur.moveToFirst()) 
		   {
			   id = cur
						.getString(cur
								.getColumnIndex(Phone.DISPLAY_NAME));
		     
		}
		} finally {
		if (cur != null)
		   cur.close();
		}
		return id;
		}
	public static void open_maps_for_location(final Context c,
			final String location) {
		LocationManager lm = null;
		boolean gps_enabled = false, network_enabled = false;
		if (lm == null)
			lm = (LocationManager) c.getSystemService(Context.LOCATION_SERVICE);
		try {
			gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
		} catch (Exception ex) {
		}
		try {
			network_enabled = lm
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		} catch (Exception ex) {
		}

		if (!gps_enabled && !network_enabled) {
			Builder dialog = new Builder(c);
			dialog.setTitle("Map details");
			dialog.setMessage("Currently GPS network not enabled.\n Want to enable ?");
			dialog.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(
								DialogInterface paramDialogInterface,
								int paramInt) {
							// TODO Auto-generated method stub
							Intent myIntent = new Intent(
									Settings.ACTION_SECURITY_SETTINGS);
							c.startActivity(myIntent);
							// get gps
						}
					});
			dialog.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(
								DialogInterface paramDialogInterface,
								int paramInt) {
							// TODO Auto-generated method stub

						}
					});
			dialog.show();

		}

		else {
			Intent intent = new Intent(
					Intent.ACTION_VIEW,// Uri.parse("geo:0,0?q="+"mantri square, Sampige Road, Malleshwaram,Bangalore, Karnataka-560003"));
					Uri.parse("http://maps.google.com/maps?f=d&daddr="
							+ location ));//"mantri square, Sampige Road, Malleshwaram,Bangalore, Karnataka-560003"));
			intent.setClassName("com.google.android.apps.maps",
					"com.google.android.maps.MapsActivity");
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			c.startActivity(intent);
		}
	}
	
	public static void open_dialer(Context c,String no)
	{
		Intent intent = new Intent(Intent.ACTION_DIAL);
		intent.setData(Uri.parse("tel:"+no));
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		c.startActivity(intent);
	}
	
	
	
	
	///////////////////////////////////// image codes
	public  Bitmap get_lowsize_bitmap(String imageUri)
	{
		String filePath = imageUri;
		Bitmap scaledBitmap = null;
		//ByteArrayOutputStream baos = new ByteArrayOutputStream();
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

		int actualHeight = options.outHeight;
		int actualWidth = options.outWidth;
		float maxHeight = 816.0f;
		float maxWidth = 612.0f;
		float imgRatio = actualWidth / actualHeight;
		float maxRatio = maxWidth / maxHeight;
		if (actualHeight > maxHeight || actualWidth > maxWidth) {
			if (imgRatio < maxRatio) 
			{ 	
				imgRatio = maxHeight / actualHeight;
				actualWidth = (int) (imgRatio * actualWidth);
				actualHeight = (int) maxHeight;
			} 
			else if (imgRatio > maxRatio) 
			{
				imgRatio = maxWidth / actualWidth;
				actualHeight = (int) (imgRatio * actualHeight);
				actualWidth = (int) maxWidth;
			} 
			else 
			{
				actualHeight = (int) maxHeight;
				actualWidth = (int) maxWidth;

			}
		}
		options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
		options.inJustDecodeBounds = false;
		options.inPurgeable = true;
		options.inInputShareable = true;
		options.inTempStorage = new byte[16 * 1024];

		try {
			bmp = BitmapFactory.decodeFile(filePath, options);
		} catch (OutOfMemoryError exception) {
			exception.printStackTrace();

		}
		try {
			scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Config.ARGB_8888);
		} catch (OutOfMemoryError exception) {
			exception.printStackTrace();
		}

		float ratioX = actualWidth / (float) options.outWidth;
		float ratioY = actualHeight / (float) options.outHeight;
		float middleX = actualWidth / 2.0f;
		float middleY = actualHeight / 2.0f;

		Matrix scaleMatrix = new Matrix();
		scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

		Canvas canvas = new Canvas(scaledBitmap);
		canvas.setMatrix(scaleMatrix);
		canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
		ExifInterface exif;
		try {
			exif = new ExifInterface(filePath);

			int orientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION, 0);
			Log.d("EXIF", "Exif: " + orientation);
			Matrix matrix = new Matrix();
			if (orientation == 6) {
				matrix.postRotate(90);
				Log.d("EXIF", "Exif: " + orientation);
			} else if (orientation == 3) {
				matrix.postRotate(180);
				Log.d("EXIF", "Exif: " + orientation);
			} else if (orientation == 8) {
				matrix.postRotate(270);
				Log.d("EXIF", "Exif: " + orientation);
			}
			scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
					scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
					true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return scaledBitmap;
	}
	

	
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public static boolean add_event_to_calendar(Activity c,Event event)
	{
		String calendarUriBase = null;
		Uri calendars = Uri.parse("content://calendar/calendars");
		Cursor managedCursor = null;
		try {
		managedCursor = c.managedQuery(calendars, null, null, null, null);
		} catch (Exception e) {
		}
		if (managedCursor != null) {
		calendarUriBase = "content://calendar/";
		} else {
		calendars = Uri.parse("content://com.android.calendar/calendars");
		try {
		managedCursor = c.managedQuery(calendars, null, null, null, null);
		} catch (Exception e) {
		}
		if (managedCursor != null) {
		calendarUriBase = "content://com.android.calendar/";
		}
		}
		 Calendar cal = Calendar.getInstance();
		String timezone = cal.getTimeZone().getDisplayName();
		String[] date = event.get_event_date().split("/");
		String[] s_time = event.get_event_s_time().split(":");
		
		
		cal.set(Calendar.YEAR, Integer.parseInt(date[2]));
		cal.set(Calendar.MONTH, Integer.parseInt(date[1])-1);
		cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date[0])); //add a day
		cal.set(Calendar.HOUR_OF_DAY, 9);//Integer.parseInt(s_time[0].substring(0, s_time[0].length() - 3))); //set hour to last hour
		cal.set(Calendar.MINUTE, 0);//Integer.parseInt(s_time[1])); //set minutes to last minute
		cal.set(Calendar.SECOND, 0); //set seconds to last second
		cal.set(Calendar.MILLISECOND, 000); //set milliseconds to last millisecond
		String ampm = s_time[0].substring(s_time[0].length() - 3,s_time[0].length() - 1);
		int am_pm = Calendar.AM;
		if(ampm.equalsIgnoreCase("pm"))
		{
			am_pm = Calendar.PM;
		}
		cal.set(Calendar.AM_PM, Calendar.AM);
		
		
		ContentValues values = new ContentValues();
		values.put("calendar_id", 1);
		values.put("eventTimezone", timezone);
		values.put("title", event.get_event_name());
		values.put("allDay", 0);
		values.put("dtstart", cal.getTimeInMillis() + 00*60*1000); // event starts at 00 minutes from now
		values.put("dtend", cal.getTimeInMillis()+60*60*1000); // ends 60 minutes from now
		values.put("description", "");
		values.put("eventLocation", C.clubs.get(event.get_venue_id()).get_venue_address());
		values.put("eventStatus", 0);
		values.put("hasAlarm", 1);
		
		Uri e = c.getContentResolver().insert(Events.CONTENT_URI, values);
		
		Uri REMINDERS_URI = Uri.parse(calendarUriBase + "reminders");
		values = new ContentValues();
		values.put( "event_id", Long.parseLong(e.getLastPathSegment()));
		values.put( "method", 1 );
		values.put( "minutes", -10 );
		Uri r = c.getContentResolver().insert( REMINDERS_URI, values );
		if(Long.parseLong(r.getLastPathSegment()) > 0)
		{
			return true;
		}
		else
			return false;
	}
	
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public static boolean add_deals_to_calendar(Activity c,Deals deal)
	{
		V_Details venue = C.clubs.get(deal.get_venue_id());
		String calendarUriBase = null;
		Uri calendars = Uri.parse("content://calendar/calendars");
		Cursor managedCursor = null;
		try {
		managedCursor = c.managedQuery(calendars, null, null, null, null);
		} catch (Exception e) {
		}
		if (managedCursor != null) {
		calendarUriBase = "content://calendar/";
		} else {
		calendars = Uri.parse("content://com.android.calendar/calendars");
		try {
		managedCursor = c.managedQuery(calendars, null, null, null, null);
		} catch (Exception e) {
		}
		if (managedCursor != null) {
		calendarUriBase = "content://com.android.calendar/";
		}
		}
		 Calendar cal = Calendar.getInstance();
		String timezone = cal.getTimeZone().getDisplayName();
		String[] date = deal.get_e_date().split("/");
		//String[] s_time = event.get_event_s_time().split(":");
		
		
		cal.set(Calendar.YEAR, Integer.parseInt(date[2]));
		cal.set(Calendar.MONTH, Integer.parseInt(date[1])-1);
		cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date[0])); //add a day
		cal.set(Calendar.HOUR, 8);//Integer.parseInt(s_time[0].substring(0, s_time[0].length() - 3))); //set hour to last hour
		cal.set(Calendar.MINUTE, 0);//Integer.parseInt(s_time[1])); //set minutes to last minute
		cal.set(Calendar.SECOND, 0); //set seconds to last second
		cal.set(Calendar.MILLISECOND, 000); //set milliseconds to last millisecond
		//String ampm = s_time[0].substring(s_time[0].length() - 3,s_time[0].length() - 1);
		int am_pm = Calendar.AM;
		/*if(ampm.equalsIgnoreCase("pm"))
		{
			am_pm = Calendar.PM;
		}*/
		cal.set(Calendar.AM_PM, Calendar.AM);
		
		
		ContentValues values = new ContentValues();
		values.put("calendar_id", 1);
		values.put("eventTimezone", timezone);
		values.put("title", deal.get_name());
		values.put("allDay", 0);
		values.put("dtstart", cal.getTimeInMillis() + 00*60*1000); // event starts at 00 minutes from now
		values.put("dtend", cal.getTimeInMillis()+2*60*60*1000); // ends 60 minutes from now
		values.put("description", "");
		values.put("eventLocation", venue.get_venue_address());
		values.put("eventStatus", 0);
		values.put("hasAlarm", 1);
		
		Uri e = c.getContentResolver().insert(Events.CONTENT_URI, values);
		
		Uri REMINDERS_URI = Uri.parse(calendarUriBase + "reminders");
		values = new ContentValues();
		values.put( "event_id", Long.parseLong(e.getLastPathSegment()));
		values.put( "method", 1 );
		values.put( "minutes", -(24*60) );
		Uri r = c.getContentResolver().insert( REMINDERS_URI, values );
		if(Long.parseLong(r.getLastPathSegment()) > 0)
		{
			return true;
		}
		else
			return false;
	}
	
	@SuppressLint("NewApi")
	public static boolean delete_event(Activity c,Deals deal)
	{
		int row = 0;
		V_Details venue = C.clubs.get(deal.get_venue_id());
		
		if(venue != null)
		row = c.getContentResolver().delete(Events.CONTENT_URI, "calendar_id=? and eventLocation=? and description=? ", new String[]{"1", venue.get_venue_address(),""});
		if(row > 0)
			return true;
		else
			return false;
	}
	//////////////////////////////////////
	public void start_fb_upload(String _url)
	{
		
		Bundle params = new Bundle();
		params.putString("name", "testing");
		params.putString("caption", "testing");
		params.putString("description", "testing");
	//	Request. r;
	    //.request(null, params, "POST",new SampleUploadListener(f), null);
     }
	///////////////////////////////////////////////////////////////////////
	
	
	
	public void get_event_items_from_json(JSONObject obj,Context c)
    {
   	 		C.items.clear();
   	 		C.clubs.clear();
   	 		C.artists.clear();
   	 		C.deals.clear();
        C.event_ads.clear();
        C.club_ads.clear();
        C.deal_ads.clear();
        C.artist_ads.clear();
        C.full_ads.clear();
							JSONObject data;
							try {

								data = obj.getJSONObject(PARAMS.DATA.get_param());
                                if(data.has("ads"))
                                {
                                    JSONObject ads = data.getJSONObject("ads");
                                    JSONArray e_ads = ads.getJSONArray("e_ads");
                                    JSONArray c_ads = ads.getJSONArray("c_ads");
                                    JSONArray d_ads = ads.getJSONArray("d_ads");
                                    JSONArray a_ads = ads.getJSONArray("a_ads");
                                    JSONArray f_ads = ads.getJSONArray("f_ads");
                                    for(int i = 0;i < e_ads.length();i++)
                                    {
                                        JSONObject o = e_ads.getJSONObject(i);
                                        C.event_ads.add(new Ads(null,null,o.getString("url"),-1));
                                    }
                                    for(int i = 0;i < c_ads.length();i++)
                                    {
                                        JSONObject o = c_ads.getJSONObject(i);
                                        C.club_ads.add(new Ads(null,null,o.getString("url"),-1));
                                    }
                                    for(int i = 0;i < d_ads.length();i++)
                                    {
                                        JSONObject o = d_ads.getJSONObject(i);
                                        C.deal_ads.add(new Ads(null,null,o.getString("url"),-1));
                                    }
                                    for(int i = 0;i < a_ads.length();i++)
                                    {
                                        JSONObject o = a_ads.getJSONObject(i);
                                        C.artist_ads.add(new Ads(null,null,o.getString("url"),-1));
                                    }
                                    for(int i = 0;i < f_ads.length();i++)
                                    {
                                        JSONObject o = f_ads.getJSONObject(i);
                                        C.full_ads.add(new Ads(null,null,o.getString("url"),-1));
                                    }
                                }
								JSONArray event_array = data.getJSONArray("events");
								for(int i = 0;i < event_array.length();i++)
								{
									JSONObject event = event_array.getJSONObject(i);
									
									Event e = this.get_event(event);
									
									if(e != null)
									C.items.put(e.get_event_id(), e);
									
								}
								JSONArray club_array = data.getJSONArray("clubs");
								for(int i = 0;i < club_array.length();i++)
								{
									JSONObject club = club_array.getJSONObject(i);
									
									V_Details venue = this.get_venue_details(club);
									if(venue != null)
									C.clubs.put(venue.get_vd_id(), venue);

                                    this.get_venue_deals(club.getJSONArray(PARAMS.DEALS.get_param()));
									
								}
								
								
								JSONArray artist_array = data.getJSONArray("artists");
								for(int i = 0;i < artist_array.length();i++)
								{
									JSONObject artist = artist_array.getJSONObject(i);
									
									Artist a = this.get_artist(artist);
									if(a != null)
									C.artists.put(a.get_id(), a);
									
								}
								
								if(data.has(PARAMS.GUEST_LIST.get_param()))
								{
                                    ArrayList<Guestlist> guestlists = new ArrayList<>();
									JSONArray guest_list = data.getJSONArray(PARAMS.GUEST_LIST.get_param());
									DatabaseHelper db = new DatabaseHelper(c);
									for(int i = 0;i < guest_list.length();i++)
									{
										JSONObject guest = guest_list.getJSONObject(i);
										Log.d("C class", guest.toString());
										String name = null,guest_id = null,eid = null,date = null,code = null;
                                        name = guest.getString(PARAMS.NAME.get_param());
                                        guest_id = guest.getString(PARAMS.GUEST_ID.get_param());
										eid = guest.getString(PARAMS.EVENT_ID.get_param());
										date = guest.getString(PARAMS.DATE.get_param());
										if(!guest.isNull(PARAMS.UNIQUE_CODE.get_param()))
										code = guest.getString(PARAMS.UNIQUE_CODE.get_param());
                                        if(items.get(eid) != null)
                                            date = C.items.get(eid).get_event_date();
                                        guestlists.add(new Guestlist(name,guest_id,eid,date,code));


										
									}
                                    db.update_guestlist(guestlists);
								}

								if(data.has("my_deals"))
								{

									JSONArray deals = data.getJSONArray("my_deals");
                                    Log.d("my_deals",deals.toString());
									DatabaseHelper db = new DatabaseHelper(c);
									for(int i = 0;i < deals.length();i++)
									{
										JSONObject deal = deals.getJSONObject(i);
										String name = null,id = null,vdid = null,code = null,edate = null;
                                        name = deal.getString(PARAMS.NAME.get_param());
                                        id = deal.getString(PARAMS.ID.get_param());
										vdid = deal.getString(PARAMS.VD_ID.get_param());
										edate = deal.getString(PARAMS.E_DATE.get_param());
										if(!deal.isNull(PARAMS.UNIQUE_CODE.get_param()))
										code = deal.getString(PARAMS.UNIQUE_CODE.get_param());
										Deals d = new Deals(name,id,vdid,code,edate);
										db.update_mydeals(name,id, vdid, code, edate);
									}



								}


							} catch (JSONException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}





    }

	public Event get_event(JSONObject obj)
	{
		Event event = null;
		String vd_id = null;
		String artist_id = null;
		ArrayList<Images> e_images = null;
        ArrayList<Lineup> lineups = new ArrayList<>();
		String url = null,ticket_url = null,ticket_price = null;
		int priority = 100;
		try {
			if(!obj.isNull(PARAMS.VD_ID.get_param()))
			{

				vd_id = obj.getString(PARAMS.VD_ID.get_param());


			}
			if(!obj.isNull(PARAMS.ARTIST_ID.get_param()))
			{

                artist_id = obj.getString(PARAMS.ARTIST_ID.get_param());

			}
			if(!obj.isNull(PARAMS.IMAGES.get_param()))
			{
				e_images = this.get_image(obj.getJSONArray(PARAMS.IMAGES.get_param()));
			}

			if(!obj.isNull(PARAMS.URL.get_param()))
			{
				url = obj.getString(PARAMS.URL.get_param());
			}
            if(!obj.isNull(PARAMS.TICKEC_PRICE.get_param()))
            {
                ticket_price = obj.getString(PARAMS.TICKEC_PRICE.get_param());
            }
            if(!obj.isNull(PARAMS.TICKET_LINK.get_param()))
            {
                ticket_url = obj.getString(PARAMS.TICKET_LINK.get_param());
            }
			if(!obj.isNull(PARAMS.PRIORITY.get_param()))
			{
				priority = obj.getInt(PARAMS.PRIORITY.get_param());
			}
            if(!obj.isNull(PARAMS.DJ_LINEUP.get_param()))
            {
               JSONArray array = obj.getJSONArray(PARAMS.DJ_LINEUP.get_param());
                for(int i = 0;i < array.length();i++)
                {
                    String id = null,name = null,stime = null,etime = null;
                    JSONObject object = array.getJSONObject(i);
                    if(!object.isNull(PARAMS.ID.get_param()))
                    {
                        id = object.getString(PARAMS.ID.get_param());
                    }
                    if(!object.isNull(PARAMS.NAME.get_param()))
                    {
                        name = object.getString(PARAMS.NAME.get_param());
                    }
                    if(!object.isNull(PARAMS.S_TIME.get_param()))
                    {
                        stime = object.getString(PARAMS.S_TIME.get_param());
                    }
                    if(!object.isNull(PARAMS.E_TIME.get_param()))
                    {
                        etime = object.getString(PARAMS.E_TIME.get_param());
                    }
                    lineups.add(new Lineup(id,name,stime,etime));
                }
            }
			event = new Event(vd_id,
					 			obj.getString(PARAMS.EVENT_ID.get_param()),
                                obj.getInt(PARAMS.TYPE.get_param()),
					 			obj.getString(PARAMS.NAME.get_param()),
                                obj.getString(PARAMS.MUSIC_TYPE.get_param()),
					 			obj.getString(PARAMS.DATE.get_param()),
					 			obj.getString(PARAMS.S_TIME.get_param()),
					 			obj.getString(PARAMS.E_TIME.get_param()),
					 			obj.getString(PARAMS.DJ_LINEUP.get_param()),
					 			obj.getString(PARAMS.DRESS_CODE.get_param()),
					 			obj.getString(PARAMS.ENTRY_RULE.get_param()),
                                ticket_price,
					 			obj.getString(PARAMS.ENTRY_PRICE.get_param()),
					 			obj.getString(PARAMS.COVER_CHARGE.get_param()),
					 			obj.getString(PARAMS.DEALS.get_param()),
					 			obj.getString(PARAMS.DETAILS.get_param()),
					 			obj.getString(PARAMS.GUEST_LIST.get_param()),
					 			obj.getInt(PARAMS.ATTEND_TYPE.get_param()),
					 			obj.getString(PARAMS.GOING_COUNT.get_param()),
					 			obj.getString(PARAMS.MAYBE_COUNT.get_param()),
					 			obj.getString(PARAMS.GUEST_COUNT.get_param()),
					 			obj.getString(PARAMS.IN_GUEST_LIST.get_param()),
					 			obj.getString(PARAMS.GUEST_TIME.get_param()),
					 			url,
					 			e_images,
                                lineups,
                                ticket_url,
					 			priority
								);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return event;
	}
	public V_Details get_venue_details(JSONObject obj)
	{
        Log.d("club",obj.toString());
		V_Details v_details = null;
		ArrayList<Images> v_images = null;
		ArrayList<String> menu_images = null;
		ArrayList<Review> reviews = null;
		int type = 0,bar_type = 0,priority = 100;
		String map = null,details = null;
		try {
			if(!obj.isNull(PARAMS.TYPE.get_param()))
			{
				type = obj.getInt(PARAMS.TYPE.get_param());
			}
			if(!obj.isNull(PARAMS.BAR_TYPE.get_param()))
			{
				bar_type = obj.getInt(PARAMS.BAR_TYPE.get_param());
			}
			if(!obj.isNull(PARAMS.MAP.get_param()))
			{
				map = obj.getString(PARAMS.MAP.get_param());
			}
			if(!obj.isNull(PARAMS.REVIEWS.get_param()))
			{
				reviews = this.get_reviews(obj.getJSONArray(PARAMS.REVIEWS.get_param()));
			}
			if(!obj.isNull(PARAMS.PRIORITY.get_param()))
			{
				priority = obj.getInt(PARAMS.PRIORITY.get_param());
				
			}
			if(!obj.isNull(PARAMS.DETAILS.get_param()))
			{
				details = obj.getString(PARAMS.DETAILS.get_param());
				
			}
			v_images = this.get_image(obj.getJSONArray(PARAMS.IMAGES.get_param()));
			menu_images = this.get_images(obj.getJSONArray(PARAMS.LIQUOR_MENU.get_param()));
			
			v_details = new V_Details(
										obj.getString(PARAMS.VD_ID.get_param()),
										type,
										bar_type,
										obj.getString(PARAMS.NAME.get_param()),
										map,
										obj.getString(PARAMS.CUISINE.get_param()),
										obj.getString(PARAMS.ADDRESS.get_param()),
										obj.getString(PARAMS.CONTACT_INFO.get_param()),
										obj.getString(PARAMS.AREA.get_param()),
										obj.getString(PARAMS.CITY.get_param()),
										obj.getString(PARAMS.STATE.get_param()),
										obj.getString(PARAMS.FAVORITE_COUNT.get_param()),
										obj.getInt(PARAMS.FAVORITE.get_param()),
										obj.getString(PARAMS.STARS.get_param()),
										obj.getString(PARAMS.RATING.get_param()),
										v_images,
										menu_images,
										reviews,
										priority,
										details
										);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return v_details;
	}

    public void get_venue_deals(JSONArray deals)
    {
        ArrayList<Deals> values = new ArrayList<Deals>();
        String vd_id = "-1";
        for(int i = 0;i < deals.length();i++)
        {

            JSONObject deal = null;
            try {
                deal = deals.getJSONObject(i);
                Log.d("deals",deal.toString());
                String id = null,vdid = null,name = null,type = null,sdate = null,edate = null,total_count ="0",remaining_count = "0",details = null;
                Images image = null;
                id = deal.getString(PARAMS.ID.get_param());
               vd_id = vdid = deal.getString(PARAMS.VD_ID.get_param());
                name = deal.getString(PARAMS.NAME.get_param());
                type = deal.getString(PARAMS.TYPE.get_param());
                sdate = deal.getString(PARAMS.S_DATE.get_param());
                edate = deal.getString(PARAMS.E_DATE.get_param());
                details = deal.getString(PARAMS.DETAILS.get_param());
                total_count = deal.getString("total_count");
                remaining_count = deal.getString("remaining_count");
                if(!deal.isNull(PARAMS.IMAGE.get_param()))
                {
                    JSONArray img =  deal.getJSONArray(PARAMS.IMAGE.get_param());
                    ArrayList<Images> images = get_image(img);
                    if(images.size() > 0)
                    image = images.get(0);

                }




                values.add(new Deals(id, vdid, name, type, sdate, edate, total_count, remaining_count,details, image));

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        if(values.size() > 0)
        {
            C.deals.put(vd_id,values);
        }
    }
	
	public ArrayList<String> get_images(JSONArray array)
	{
		ArrayList<String> images = new ArrayList<String>();
		for(int m = 0;m < array.length();m++)
		{
			JSONObject img;
			try {
				img = array.getJSONObject(m);
				images.add(img.getString(PARAMS.IMAGE.get_param()));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return images;
	}
	
	public ArrayList<Images> get_image(JSONArray array)
	{
		ArrayList<Images> images = new ArrayList<Images>();
		for(int m = 0;m < array.length();m++)
		{
			JSONObject img;
			try {
				img = array.getJSONObject(m);
				images.add(new Images(img.getString(PARAMS.IMAGE_ID.get_param()),img.getString(PARAMS.IMAGE.get_param())));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return images;
	}
	public Images get_single_image(JSONArray array)
	{
		Images images = null;
		for(int m = 0;m < array.length();m++)
		{
			JSONObject img;
			try {
				img = array.getJSONObject(m);
				images = new Images(img.getString(PARAMS.IMAGE_ID.get_param()),img.getString(PARAMS.IMAGE.get_param()));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return images;
	}
	public ArrayList<Review> get_reviews(JSONArray array)
	{
		ArrayList<Review> reviews = new ArrayList<Review>();
		for(int m = 0;m < array.length();m++)
		{
			String id,name,userid,txt,time;
			JSONObject review;
			try {
				review = array.getJSONObject(m);
				id = review.getString(PARAMS.ID.get_param());
				name = review.getString(PARAMS.USER_NAME.get_param());
				userid = review.getString(PARAMS.USER_ID.get_param());
				txt = review.getString(PARAMS.REVIEW.get_param());
				time = review.getString(PARAMS.TIME.get_param());

				reviews.add(new Review(id,userid,name,txt,time));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return reviews;
	}
	public ArrayList<Tracks> get_tracks(JSONArray array)
	{
		ArrayList<Tracks> tracks = new ArrayList<Tracks>();
		for(int k = 0;k < array.length();k++)
		{
			JSONObject track_obj;
			try {
				track_obj = array.getJSONObject(k);
				tracks.add(new Tracks(track_obj.getString(PARAMS.ID.get_param()),
						  track_obj.getInt(PARAMS.TYPE.get_param()),
						  track_obj.getString(PARAMS.CAPTION.get_param()),
						  track_obj.getString(PARAMS.URL.get_param())
						  ));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		return tracks;
	}
	
	public Artist get_artist(JSONObject obj)
	{
		String artistid = null,e_date = null,e_name = null,f_count = "0",place = null,fb_id = null,twit_id = null,fbname = null,twitname = null,details = null;
		Artist artist = null;
		ArrayList<Tracks> tracks = null;
		ArrayList<Images> images = new ArrayList<Images>();
		
		
		try {
            if(!obj.isNull(PARAMS.ARTIST_ID.get_param()))
            {
                artistid = obj.getString(PARAMS.ARTIST_ID.get_param());

            }
			if(!obj.isNull(PARAMS.TRACKS.get_param()))
			{
				tracks = this.get_tracks(obj.getJSONArray(PARAMS.TRACKS.get_param()));
									
			}
			
			if(!obj.isNull(PARAMS.IMAGES.get_param()))
			{
				images = this.get_image(obj.getJSONArray(PARAMS.IMAGES.get_param()));
									
			}
			
			if(!obj.isNull(PARAMS.PLACE.get_param()))
				place = obj.getString(PARAMS.PLACE.get_param());
			
			if(!obj.isNull(PARAMS.DETAILS.get_param()))
				details = obj.getString(PARAMS.DETAILS.get_param());
			
			if(!obj.isNull(PARAMS.FOLLOW_COUNT.get_param())) {
                f_count = obj.getString(PARAMS.FOLLOW_COUNT.get_param());
                if(Integer.parseInt(f_count) < 0)
                {
                    f_count = "0";
                }
            }
			
			if(!obj.isNull(PARAMS.FACEBOOK_ID.get_param()))
				fb_id = obj.getString(PARAMS.FACEBOOK_ID.get_param());
			
			if(!obj.isNull(PARAMS.TWITTER_ID.get_param()))
				twit_id = obj.getString(PARAMS.TWITTER_ID.get_param());
			
			if(!obj.isNull(PARAMS.FACEBOOK_NAME.get_param()))
				fbname = obj.getString(PARAMS.FACEBOOK_NAME.get_param());
			
			if(!obj.isNull(PARAMS.TWITTER_NAME.get_param()))
				twitname = obj.getString(PARAMS.TWITTER_NAME.get_param());

            for(Event e : C.items.values())
            {
                Log.d("dj line up",e.getLineups().toString());
                boolean exit = false;
                for(int i = 0;i < e.getLineups().size();i++) {
                    if (e.getLineups().get(i).getArtistid().equalsIgnoreCase(artistid)) {

                        e_date = e.get_event_date();
                        e_name = e.get_event_name();
                        exit = true;
                        break;

                    }
                }
                if(exit)
                    break;
            }

			artist = new Artist(artistid,
												obj.getString(PARAMS.NAME.get_param()),
												place,
												details,
												obj.getString(PARAMS.GENRE.get_param()),
												obj.getString(PARAMS.FOLLOWING.get_param()),
												f_count,
												images,
												0,//artist_obj.getInt(C.PARAMS.STARS.get_param()),
												e_date,
												e_name,
												fb_id,
												twit_id,
												fbname,
												twitname,
												tracks
					   						);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return artist;
	}
	
	
	public void set_user_details(JSONObject obj)
	{
		String id = null,name = null,email = null,ccode = null,points = "0",number = null,image = null,thmbnail = null,fbid = null,twitid = null,token = null;
		boolean tuto = false;
		try {
			if(!obj.isNull(PARAMS.USER_ID.get_param()))
				id = obj.getString(PARAMS.USER_ID.get_param());
			
			if(!obj.isNull(PARAMS.USER_NAME.get_param()))
				name = obj.getString(PARAMS.USER_NAME.get_param());
			
			if(!obj.isNull(PARAMS.EMAIL.get_param()))
				email = obj.getString(PARAMS.EMAIL.get_param());
			
			if(!obj.isNull(PARAMS.C_CODE.get_param()))
				ccode = obj.getString(PARAMS.C_CODE.get_param());
			
			if(!obj.isNull(PARAMS.NUMBER.get_param()))
				number = obj.getString(PARAMS.NUMBER.get_param());
			
			if(!obj.isNull(PARAMS.IMAGE.get_param()))
				image = obj.getString(PARAMS.IMAGE.get_param());
			
			if(!obj.isNull(PARAMS.THUMBNAIL.get_param()))
				thmbnail = obj.getString(PARAMS.THUMBNAIL.get_param());
			
			if(!obj.isNull(PARAMS.TUTORIAL.get_param()))
				tuto = obj.getInt(PARAMS.TUTORIAL.get_param()) == 1 ? true : false;
			
		//	if(!obj.isNull(C.PARAMS.STATUS.get_param()))
		//		status = obj.getString(C.PARAMS.STATUS.get_param());
			
			if(!obj.isNull(PARAMS.TOKEN.get_param()))
				token = obj.getString(PARAMS.TOKEN.get_param());
			if(!obj.isNull(PARAMS.POINTS.get_param()))
				points = obj.getString(PARAMS.POINTS.get_param());
			
			if(!obj.isNull(PARAMS.FACEBOOK_ID.get_param()))
			{
				fbid = obj.getString(PARAMS.FACEBOOK_ID.get_param());
				if(fbid.length() > 0)
				if(Long.parseLong(fbid) < 0)
				{
					fbid = null;
				}
			}
			
			if(!obj.isNull(PARAMS.TWITTER_ID.get_param()))
			{
				twitid = obj.getString(PARAMS.TWITTER_ID.get_param());
				if(twitid.length() > 0)
				if(Long.parseLong(twitid) < 0)
				{
					twitid = null;
				}
			}
			
			Log.d("time",C.get_universal_date(obj.getString(PARAMS.TIME.get_param())));
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		App.set_user_details(id, name, email, ccode, number, image, thmbnail,points,token,tuto);
		App.set_fbid(fbid);
		App.set_twitid(twitid);
		
	}
	
	public ArrayList<Guestlist> get_guest_list(JSONArray array)
	{
		ArrayList<Guestlist> guest_list = new ArrayList<Guestlist>();
		for(int k = 0;k < array.length();k++)
		{
			JSONObject guest_obj;
			try {
				String code = null;
				guest_obj = array.getJSONObject(k);
				if(!guest_obj.isNull(PARAMS.UNIQUE_CODE.get_param()))
				{
					code = guest_obj.getString(PARAMS.UNIQUE_CODE.get_param());
				}
				guest_list.add(new Guestlist(guest_obj.getString(PARAMS.NAME.get_param()),guest_obj.getString(PARAMS.GUEST_ID.get_param()),
						guest_obj.getString(PARAMS.EVENT_ID.get_param()),
						guest_obj.getString(PARAMS.DATE.get_param()),
						code
						  ));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		return guest_list;
	}
	
	////////////////////////////////
	public static void updateListViewHeight(ListView myListView) {
	     ListAdapter myListAdapter = myListView.getAdapter();
	     if (myListAdapter == null) {            
	              return;
	     }
	    //get listview height
	    int totalHeight = 0;
	    int adapterCount = myListAdapter.getCount();
	    for (int size = 0; size < adapterCount ; size++) {
	        View listItem = myListAdapter.getView(size, null, myListView);
	        listItem.measure(0, 0);
	        totalHeight += listItem.getMeasuredHeight();
	    }
	    //Change Height of ListView 
	    ViewGroup.LayoutParams params = myListView.getLayoutParams();
	    params.height = totalHeight + (myListView.getDividerHeight() * (adapterCount - 1));
	    myListView.setLayoutParams(params);
	}
	
	public static Comparator<Artist> artist_sort = new Comparator<Artist>() {
		 
		 @Override
	        public int compare(Artist a1, Artist a2) {
	            return a1.get_name().compareTo(a2.get_name());
	        }
	};

    public static void expand(Context c,final Text v,final boolean opened) {
         Animation a;
        int h = 0;
        final int min_height = (int)c.getResources().getDimension(R.dimen.button_height);
        if(!opened)
        {
            v.setMaxLines(Integer.MAX_VALUE);

            /*
            v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
           final int targetHeight = measureViewHeight(v);//v.getMeasuredHeight();
            h = targetHeight;
        //    v.getLayoutParams().height = 0;
            a = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                  //  v.getLayoutParams().height = interpolatedTime == 1
                   //         ? LinearLayout.LayoutParams.WRAP_CONTENT
                   //         : (int) (targetHeight * interpolatedTime);
                    v.getLayoutParams().height = min_height + (int)((targetHeight - min_height)*interpolatedTime);
                    v.requestLayout();
                }

                @Override
                public boolean willChangeBounds() {
                    return true;
                }
            };
            */
        }
        else
        {
            v.setMaxLines(3);
            v.setEllipsize(TextUtils.TruncateAt.END);
            /*
            final int initialHeight = v.getMeasuredHeight();//int) c.getResources().getDimension(R.dimen.button_height);
            h = initialHeight;
            a = new Animation()
            {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    if(interpolatedTime == 1){

                    }else{
                        v.getLayoutParams().height = initialHeight - (int)((initialHeight-min_height) * interpolatedTime);
                        v.requestLayout();
                    }
                }

                @Override
                public boolean willChangeBounds() {
                    return true;
                }
            };
            */
        }
        v.getRootView().invalidate();
        // 1dp/ms
     //   a.setDuration(222);//(int)(h / v.getContext().getResources().getDisplayMetrics().density));
     //   v.startAnimation(a);
    }

    private static int measureViewHeight( View v) {
        try {
            Method m = v.getClass().getDeclaredMethod("onMeasure", int.class, int.class);
            m.setAccessible(true);
            m.invoke(v,
                    View.MeasureSpec.makeMeasureSpec(v.getWidth(), View.MeasureSpec.AT_MOST),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        } catch (Exception e) {
            return -1;
        }

        int measuredHeight = v.getMeasuredHeight();
        return measuredHeight;
    }

    public static String[] getaudio_file_in_bytes(String filepath)
    {
        FileInputStream fileInputStream=null;
        String base64string = null;
        String[] fileDetails = new String[2];
        File file = new File(filepath);
        if(file.exists())
        {
            ByteArrayBuffer baf = new ByteArrayBuffer((int) file.length());
            try {
                fileInputStream = new FileInputStream(file);
                BufferedInputStream bis = new BufferedInputStream(fileInputStream,1024);

                //get the bytes one by one
                int current = 0;

                while ((current = bis.read()) != -1) {

                    baf.append((byte) current);
                }

                byte[] bytes  = baf.toByteArray();
                base64string = Base64.encodeToString(bytes, Base64.DEFAULT);
                fileDetails[0] = filepath;
                fileDetails[1] = base64string;
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


        }
        return fileDetails;
    }

    //////////////////////////////
    ///////////////convert image to lower size
    public static String get_compressed_image(String imageUri) {

        String filePath = imageUri;
        Bitmap scaledBitmap = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio)
            {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            }
            else if (imgRatio > maxRatio)
            {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            }
            else
            {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = App.APP_FOLDER + "/" +"IMG_"+ System.currentTimeMillis() + ".jpg";
        try {
            out = new FileOutputStream(filename);


            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
            baos.writeTo(out);

            out.flush();
            out.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
       
        return filename;

    }
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height/ (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;      }       final float totalPixels = width * height;       final float totalReqPixelsCap = reqWidth * reqHeight * 2;       while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }


    //Create thumbnail of image
    public static String Create_Thumbnail(String image_path)
    {
        String filename = null;
        try
        {

            final int THUMBNAIL_SIZE = 100;

            FileInputStream fis = new FileInputStream(image_path);
            Bitmap imageBitmap = BitmapFactory.decodeStream(fis);

            Float width = new Float(imageBitmap.getWidth());
            Float height = new Float(imageBitmap.getHeight());
            Float ratio = width/height;
            imageBitmap = Bitmap.createScaledBitmap(imageBitmap, (int)(THUMBNAIL_SIZE * ratio), THUMBNAIL_SIZE, false);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            FileOutputStream out = null;
            filename = App.APP_FOLDER + "/" +"THUMB_"+ System.currentTimeMillis() + ".jpg";
            try {
                out = new FileOutputStream(filename);

                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                baos.writeTo(out);

                out.flush();
                out.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        catch(Exception ex) {

        }
        ///////////////////////////////////////////

        return filename;
    }

    // Creates Bitmap from InputStream and returns it
    private static Bitmap downloadImage(String url) {
        Bitmap bitmap = null;
        InputStream stream = null;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inSampleSize = 1;

        try {
            stream = getHttpConnection(url);
            bitmap = BitmapFactory.
                    decodeStream(stream, null, bmOptions);

            stream.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return bitmap;
    }
    // Makes HttpURLConnection and returns InputStream
    private static InputStream getHttpConnection(String urlString)
            throws IOException {
        InputStream stream = null;
        URL url = new URL(urlString);
        URLConnection connection = url.openConnection();

        try {
            HttpURLConnection httpConnection = (HttpURLConnection) connection;
            httpConnection.setRequestMethod("GET");
            httpConnection.connect();

            if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                stream = httpConnection.getInputStream();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return stream;
    }




    public static String Download_Image_From_Url(String url)
    {
        String filename = null;
        Bitmap bitmap = downloadImage(url);
        try
        {
            final int THUMBNAIL_SIZE = 100;

            Float width = new Float(bitmap.getWidth());
            Float height = new Float(bitmap.getHeight());
            Float ratio = width/height;
            //bitmap = Bitmap.createScaledBitmap(bitmap, (int)(THUMBNAIL_SIZE * ratio), THUMBNAIL_SIZE, false);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            FileOutputStream out = null;
            filename = App.APP_FOLDER + "/ChatMedia/" +"CHAT_"+ System.currentTimeMillis() + ".jpg";
            try {
                out = new FileOutputStream(filename);

                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                baos.writeTo(out);

                out.flush();
                out.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        catch(Exception ex) {

        }
        ///////////////////////////////////////////

        return filename;
    }







}
