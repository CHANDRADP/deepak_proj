package com.dpc.wtp.Utils;

import android.R.color;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.dpc.wtp.R;

@SuppressLint("NewApi")
public class MyAlert extends SherlockDialogFragment{
	String header,body;

public OnOKListener ok_listener;
public MyAlert(){}
@SuppressLint("ValidFragment")

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Bundle args = this.getArguments();
		if(args.containsKey("head"))
		{
			this.header = args.getString("head");
		}
		if(args.containsKey("body"))
		{
			this.body = args.getString("body");
		}
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		final Dialog dialog = new Dialog(getActivity());
		dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		dialog.setContentView(R.layout.dialog_with_single_button);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(color.transparent));
		dialog.show();
		Text head = (Text)dialog.findViewById(R.id.header);
		Text body = (Text)dialog.findViewById(R.id.body);
		head.setText(header);
		body.setText(this.body);
		Button one = (Button) dialog.findViewById(R.id.one);
		one.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				ok_listener.setOKSubmitListener(1);
				dismiss();
			}
			
			
		});
		return dialog;
	}

	public interface OnOKListener{
		void setOKSubmitListener(int value);
	}

}
