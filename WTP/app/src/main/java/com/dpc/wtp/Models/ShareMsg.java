package com.dpc.wtp.Models;

public class ShareMsg
{


    private int checked = 0, user_id, is_group;


    public ShareMsg(int user_id, int is_group, int checked) {
        this.user_id = user_id;
        this.is_group = is_group;
        this.checked = checked;
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getIs_group() {
        return is_group;
    }

    public void setIs_group(int is_group) {
        this.is_group = is_group;
    }
}
