package com.dpc.wtp.interfaces;

public interface OnDetectScrollListener {

    void onUpScrolling();

    void onDownScrolling();
    
    void onLeftScrolling();
    
    void onRightScrolling();
}
