package com.dpc.wtp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.actionbarsherlock.view.MenuItem;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.fragments.Myapproves_Fragment;
import com.google.android.gms.analytics.HitBuilders;


public class Activity_Myapproves extends MyActivity {

    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.actionbar);
		final Text title = (Text)findViewById(android.R.id.title);
		title.setText(R.string.ac_myapproves);
        super.onCreate(savedInstanceState);
        app_tracker.setScreenName(C.SCREEN.SCREEN_MY_APPROVALS.get_screen());
        app_tracker.send(new HitBuilders.AppViewBuilder().build());
        setContentView(R.layout.ac_calender);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Myapproves_Fragment fragment = new Myapproves_Fragment();
        transaction.replace(R.id.sample_content_fragment, fragment);
        transaction.commit();


    }

    @Override
    protected void onPause() {
        super.onPause();
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;

    }

    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId())
		{
	
		case android.R.id.home:
			go_back();
			super.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
    }

    @Override
	public void onBackPressed() 
	{
		// TODO Auto-generated method stub
		
		go_back();
		super.onBackPressed();
	}
    private void go_back()
    {
    	Intent i = new Intent(this,Activity_Home.class);
    	i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
    	startActivity(i);
    	
    }
    
    
}