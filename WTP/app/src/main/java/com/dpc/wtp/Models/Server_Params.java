package com.dpc.wtp.Models;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.pm.PackageInfo;
import android.os.Build;

import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.activities.App;

public class Server_Params {

	ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
	public String get_version()
	{
		return Build.VERSION.RELEASE;
	}
	public String getDeviceName() {
		  String manufacturer = Build.MANUFACTURER;
		  String model = Build.MODEL;
		  if (model.startsWith(manufacturer)) {
		    return capitalize(model);
		  } else {
		    return capitalize(manufacturer) + " " + model;
		  }
		}


		private String capitalize(String s) {
		  if (s == null || s.length() == 0) {
		    return "";
		  }
		  char first = s.charAt(0);
		  if (Character.isUpperCase(first)) {
		    return s;
		  } else {
		    return Character.toUpperCase(first) + s.substring(1);
		  }
		}


	public ArrayList<NameValuePair> get_login_params(String email,String password,String gcmid,String time)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.LOGIN.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.EMAIL.get_param(),email));
		params.add(new BasicNameValuePair(PARAMS.PASSWORD.get_param(),password));
		params.add(new BasicNameValuePair(PARAMS.GCM_ID.get_param(),gcmid));
		params.add(new BasicNameValuePair(PARAMS.PHONE_TYPE.get_param(),"1"));
		params.add(new BasicNameValuePair(PARAMS.PHONE_MODEL.get_param(),this.getDeviceName()+" : Android "+this.get_version()));
		return params;
	}
	public ArrayList<NameValuePair> get_fblogin_params(String id,String email,String gcmid,String time)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.LOGIN_FB.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.FACEBOOK_ID.get_param(),id));
		params.add(new BasicNameValuePair(PARAMS.EMAIL.get_param(),email));
		params.add(new BasicNameValuePair(PARAMS.GCM_ID.get_param(),gcmid));
		params.add(new BasicNameValuePair(PARAMS.PHONE_TYPE.get_param(),"1"));
		params.add(new BasicNameValuePair(PARAMS.PHONE_MODEL.get_param(),this.getDeviceName()+" : Android "+this.get_version()));
		
		return params;
	}
	public ArrayList<NameValuePair> get_fbupdate_params(String id,String fbid,String gender)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.UPDATE_FB.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.FACEBOOK_ID.get_param(),fbid));
        params.add(new BasicNameValuePair(PARAMS.GENDER.get_param(),gender));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),id));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		
		return params;
	}
	public ArrayList<NameValuePair> get_twitupdate_params(String id,String twitid)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.UPDATE_TWITTER.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.TWITTER_ID.get_param(),twitid));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),id));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		return params;
	}
	public ArrayList<NameValuePair> get_twitlogin_params(String id,String gcmid,String time)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.LOGIN_TWITTER.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.TWITTER_ID.get_param(),id));
		params.add(new BasicNameValuePair(PARAMS.GCM_ID.get_param(),gcmid));
		params.add(new BasicNameValuePair(PARAMS.TIME.get_param(),time));
		params.add(new BasicNameValuePair(PARAMS.PHONE_TYPE.get_param(),"1"));
		return params;
	}
	public ArrayList<NameValuePair> get_reset_pass_params(String email)
	{

		params.add(new BasicNameValuePair(C.METHOD.SEND_PASSWORD.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.EMAIL.get_param(),email));
		return params;
	}
    public ArrayList<NameValuePair> get_feedback_params(String details)
    {

        params.add(new BasicNameValuePair(C.METHOD.ADD_FEEDBACK.get_method()," "));
        params.add(new BasicNameValuePair(PARAMS.DETAILS.get_param(),details));
        params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),App.get_userid()));
        params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
        return params;
    }
	public ArrayList<NameValuePair> get_register_params(String name,String email,String password,String ccode,String number,String dob,String gcmid,String time,String image,String thumbnail)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.INSERT_USER.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.USER_NAME.get_param(),name));
		params.add(new BasicNameValuePair(PARAMS.EMAIL.get_param(),email));
		params.add(new BasicNameValuePair(PARAMS.PASSWORD.get_param(),password));
		params.add(new BasicNameValuePair(PARAMS.C_CODE.get_param(),ccode));
		params.add(new BasicNameValuePair(PARAMS.NUMBER.get_param(),number));
		params.add(new BasicNameValuePair(PARAMS.DOB.get_param(),dob));
		params.add(new BasicNameValuePair(PARAMS.GCM_ID.get_param(),gcmid));
		params.add(new BasicNameValuePair(PARAMS.PHONE_TYPE.get_param(),"1"));
		params.add(new BasicNameValuePair(PARAMS.PHONE_MODEL.get_param(),this.getDeviceName()+" : Android "+this.get_version()));
		
		if(App.get_fbid() != null)
		params.add(new BasicNameValuePair(PARAMS.FACEBOOK_ID.get_param(),App.get_fbid()));
		if(App.get_twitid() != null)
		params.add(new BasicNameValuePair(PARAMS.TWITTER_ID.get_param(),App.get_twitid()));
		if(image != null)
		{
			params.add(new BasicNameValuePair(PARAMS.IMAGE.get_param(),image));
		}
		if(thumbnail != null)
		{
			params.add(new BasicNameValuePair(PARAMS.THUMBNAIL.get_param(),thumbnail));
		}
		return params;
	}
	public ArrayList<NameValuePair> get_update_points_params(String uid)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.UPDATE_POINTS.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),uid));
		params.add(new BasicNameValuePair(PARAMS.POINTS.get_param(),String.valueOf(App.get_userpoints())));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		return params;
	}
	
	public ArrayList<NameValuePair> get_update_tutrs_params(String uid)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.UPDATE_TUTRS.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),uid));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		return params;
	}
	public ArrayList<NameValuePair> get_all_artist_params(String uid)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.ARTIST_DETAILS.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),uid));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		return params;
	}
/*	public ArrayList<NameValuePair> get_artist_params(String uid,String artistid)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.ARTIST_DETAILS.get_method()," "));
		params.add(new BasicNameValuePair(C.PARAMS.USER_ID.get_param(),uid));
		params.add(new BasicNameValuePair(C.PARAMS.ARTIST_ID.get_param(),artistid));
		return params;
	}
	*/
	public ArrayList<NameValuePair> get_venue_params(String uid)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.SEARCH_VENUES.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),uid));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		return params;
	}
	public ArrayList<NameValuePair> get_details_by_location(String uid)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.SEARCH_BY_LOCATION.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),uid));
		params.add(new BasicNameValuePair(PARAMS.LOCATION.get_param(),App.get_city()));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		return params;
	}
	public ArrayList<NameValuePair> get_concert_params(String uid,String details)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.CONCERT_DETAILS.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),uid));
		params.add(new BasicNameValuePair(PARAMS.DETAILS.get_param(),details));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		return params;
	}
	public ArrayList<NameValuePair> get_add_follow_params(String uid,String artist_id)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.ADD_FOLLOW.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),uid));
		params.add(new BasicNameValuePair(PARAMS.ID.get_param(),artist_id));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		return params;
	}
	public ArrayList<NameValuePair> get_add_favorite_params(String uid,String vd_id)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.ADD_FAVORITE.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),uid));
		params.add(new BasicNameValuePair(PARAMS.ID.get_param(),vd_id));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		return params;
	}
	public ArrayList<NameValuePair> get_remove_follow_params(String uid,String artist_id)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.REMOVE_FOLLOW.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),uid));
		params.add(new BasicNameValuePair(PARAMS.ID.get_param(),artist_id));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		return params;
	}
	public ArrayList<NameValuePair> get_remove_favorite_params(String uid,String vd_id)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.REMOVE_FAVORITE.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),uid));
		params.add(new BasicNameValuePair(PARAMS.ID.get_param(),vd_id));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		return params;
	}
	public ArrayList<NameValuePair> get_add_star_params(String uid,String id,String star_no)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.ADD_STAR.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),uid));
		params.add(new BasicNameValuePair(PARAMS.ID.get_param(),id));
		params.add(new BasicNameValuePair(PARAMS.STAR_NO.get_param(),star_no));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		return params;
	}
	public ArrayList<NameValuePair> get_update_star_params(String uid,String id,String star_no)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.UPDATE_STAR.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),uid));
		params.add(new BasicNameValuePair(PARAMS.ID.get_param(),id));
		params.add(new BasicNameValuePair(PARAMS.STAR_NO.get_param(),star_no));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		return params;
	}
	public ArrayList<NameValuePair> get_add_review_params(String uid,String vdid,String review)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.ADD_REVIEW.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),uid));
		params.add(new BasicNameValuePair(PARAMS.VD_ID.get_param(),vdid));
		params.add(new BasicNameValuePair(PARAMS.REVIEW.get_param(),review));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		return params;
	}
	public ArrayList<NameValuePair> get_add_photo_params(String uid,String image,String thumbnail)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.ADD_PHOTO.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),uid));
		params.add(new BasicNameValuePair(PARAMS.IMAGE.get_param(),image));
		params.add(new BasicNameValuePair(PARAMS.THUMBNAIL.get_param(),thumbnail));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		return params;
	}

	public ArrayList<NameValuePair> get_update_photo_params(String uid,String image,String thumbnail)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.UPDATE_PHOTO.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),uid));
		params.add(new BasicNameValuePair(PARAMS.IMAGE.get_param(),image));
		params.add(new BasicNameValuePair(PARAMS.THUMBNAIL.get_param(),thumbnail));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		return params;
	}
	public ArrayList<NameValuePair> get_add_guest_params(String uid,String vdid,String e_id)
	{
		params.add(new BasicNameValuePair(C.METHOD.ADD_GUEST.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),uid));
		params.add(new BasicNameValuePair(PARAMS.VD_ID.get_param(),vdid));
		params.add(new BasicNameValuePair(PARAMS.EVENT_ID.get_param(),e_id));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		return params;
	}

    public ArrayList<NameValuePair> get_request_deal_params(String uid,String vdid,String deal_id)
    {

        params.add(new BasicNameValuePair(C.METHOD.DEAL_REQUEST.get_method()," "));
        params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),uid));
        params.add(new BasicNameValuePair(PARAMS.VD_ID.get_param(),vdid));
        params.add(new BasicNameValuePair(PARAMS.DEAL_ID.get_param(),deal_id));
        params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
        return params;
    }
	public ArrayList<NameValuePair> get_invite_params(String[] uids,String e_id)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.INVITE_TO_EVENT.get_method()," "));
        params.add(new BasicNameValuePair(PARAMS.ID.get_param(),App.get_userid()));
		for(int i = 0;i < uids.length;i++)
		{
			params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param()+"[]",uids[i]));
			
		}
		params.add(new BasicNameValuePair(PARAMS.EVENT_ID.get_param(),e_id));
		params.add(new BasicNameValuePair(PARAMS.USER_NAME.get_param(),App.get_username()));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		return params;
	}
	public ArrayList<NameValuePair> get_update_status_params(String uid,String status)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.UPDATE_STATUS.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),uid));
		params.add(new BasicNameValuePair(PARAMS.STATUS.get_param(),status));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		
		return params;
	}
	public ArrayList<NameValuePair> get_add_attend_params(String uid,String type,String eventid)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.ADD_ATTEND.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),uid));
		params.add(new BasicNameValuePair(PARAMS.TYPE.get_param(),type));
		params.add(new BasicNameValuePair(PARAMS.EVENT_ID.get_param(),eventid));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		
		return params;
	}
	public ArrayList<NameValuePair> get_update_attend_params(String userid,String type,String eventid)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.UPDATE_ATTEND.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),userid));
		params.add(new BasicNameValuePair(PARAMS.TYPE.get_param(),type));
		params.add(new BasicNameValuePair(PARAMS.EVENT_ID.get_param(),eventid));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		
		return params;
	}
	
	public ArrayList<NameValuePair> get_event_details_params(String uid,String e_id)
	{
		
		params.add(new BasicNameValuePair(C.METHOD.GET_E_DETAILS.get_method()," "));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),uid));
		params.add(new BasicNameValuePair(PARAMS.EVENT_ID.get_param(),e_id));
		params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		
		
		return params;
	}
    public ArrayList<NameValuePair> get_app_version_params(String version)
    {

        params.add(new BasicNameValuePair(C.METHOD.UPDATE_APP_VERSION.get_method()," "));
        params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),App.get_userid()));
        params.add(new BasicNameValuePair(PARAMS.VERSION.get_param(),version));
        params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));


        return params;
    }
}
