package com.dpc.wtp.activities;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.dpc.wtp.R;
import com.dpc.wtp.Models.My_Marker;
import com.dpc.wtp.Models.V_Details;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.customs.MyActivity;
import com.facebook.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
public class Activity_Places_Nearby  extends MyActivity{
	
	private GoogleMap googleMap;
	
	public static Location my_location = null;
	
	private  LatLng latLng = null;
	
	private ArrayList<My_Marker> club_markers = new ArrayList<My_Marker>();
	
	@Override
	protected void onCreate(Bundle arg0) {
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.actionbar);
		final Text title = (Text)findViewById(android.R.id.title);
		title.setText(R.string.ac_nearbyme);
		super.onCreate(arg0);
        app_tracker.setScreenName(C.SCREEN.SCREEN_NEARBY_PLACES.get_screen());
        app_tracker.send(new HitBuilders.AppViewBuilder().build());
		this.setContentView(R.layout.ac_places_nearby);
		if(my_location != null)
		{
			latLng = new LatLng(my_location.getLatitude(),my_location.getLongitude());
		}
        else
        {
            if(App.get_lati() != null)
            latLng = new LatLng(Double.parseDouble(App.get_lati()),Double.parseDouble(App.get_long()));
        }


		
		

	}
	private void prepare_map()
	{
		if (googleMap == null) 
		{
			googleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(
					R.id.map)).getMap();

			// check if map is created successfully or not
			if (googleMap == null) 
			{
				C.alert(getApplicationContext(),"Sorry! unable to create maps");
						
				return;
			}
		}
		try {
			

			googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			googleMap.setMyLocationEnabled(true);
			googleMap.getUiSettings().setZoomControlsEnabled(false);
			googleMap.getUiSettings().setMyLocationButtonEnabled(true);
			googleMap.getUiSettings().setCompassEnabled(true);
			googleMap.getUiSettings().setRotateGesturesEnabled(true);
			googleMap.getUiSettings().setZoomGesturesEnabled(true);
			googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {

		           @Override
		           public void onMyLocationChange(Location l) 
		           {
		        	   my_location = l;
		        	   if(latLng == null)
		        	   {
		        		   latLng = new LatLng(l.getLatitude(),l.getLongitude());
		        		   move_camera();
		        	   }
		        	   latLng = new LatLng(l.getLatitude(),l.getLongitude());
                       App.update_lati(String.valueOf(l.getLatitude()));
                       App.update_long(String.valueOf(l.getLongitude()));
		        	   
		        	}
		          });
			googleMap.setOnMyLocationButtonClickListener(new OnMyLocationButtonClickListener(){

				@Override
				public boolean onMyLocationButtonClick() {
					if(latLng != null)
					move_camera();
					else
						googleMap.clear();
					get_clubs();
					add_markers();
					return false;
				}
				
			});

			move_camera_to_location();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private void move_camera()
	{
		if(googleMap != null)
		{
			googleMap.clear();
	 	   	googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
		    googleMap.animateCamera(CameraUpdateFactory.zoomTo(7));
		    googleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.user_pin)).title("Me"));
		}
	}
	private void move_camera_to_location()
	{
		googleMap.clear();
		double[] loc = this.get_geo();
		if(loc != null)
		{
			googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(loc[0],loc[1])));
 	   		googleMap.animateCamera(CameraUpdateFactory.zoomTo(7));
		}
		get_clubs();
		
	}
	private double[] get_geo()
	{
		return this.get_geo_point(Activity_City.cities[Integer.parseInt(App.get_city())-1]);
		
	}
	private void get_clubs()
	{
		club_markers.clear();
		for(V_Details c : C.clubs.values())
		{
			Log.d("address", c.get_venue_address());
			double[] loc = this.get_geo_point(c.get_venue_address());
				if(loc != null)
				{
                    Log.d("geo details", String.valueOf(loc));
					String distance = this.get_distance(loc);
					if(distance != null)
					club_markers.add(new My_Marker(c.get_vd_id(), c.get_venue_name(),c.get_venue_address(),distance,loc,false));
				}
            else
                {
                    Log.d("geo details", "not available");
                }
				
			
		}
	}
	private void add_markers()
	{
		if(club_markers.size() > 0)
        {
            for (My_Marker myMarker : club_markers)
            {

               if(!myMarker.is_user())
               {
	                View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.marker_bg, null);
	        		TextView distance = (TextView) marker.findViewById(R.id.text);
	        		distance.setText(myMarker.get_name() + " ("+myMarker.get_distance()+")");
	        		
	        		googleMap.addMarker(new MarkerOptions()
	        		.position(new LatLng(myMarker.get_lat(), myMarker.get_lng()))
	        		.title(myMarker.get_id())
	        		.snippet("Description")
	        		.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(marker))));
         
                }
            }
            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
            {
                @Override
                public boolean onMarkerClick(Marker marker)
                {
                	String id = marker.getTitle();
                	if(!id.equalsIgnoreCase("Me"))
                	{
	                    marker.hideInfoWindow();
	                    Intent i = new Intent(Activity_Places_Nearby.this,Activity_Club_Details.class);
	                    i.putExtra(PARAMS.VD_ID.get_param(), id);
	                    startActivity(i);
                	}
                	else
                	{
                		marker.showInfoWindow();
                	}
                    return true;
                }
            });
        
        }
	}
	// Convert a view to bitmap
		public  Bitmap createDrawableFromView(View view) {
			DisplayMetrics displayMetrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
			view.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
			view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
			view.buildDrawingCache();
			Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
	 
			Canvas canvas = new Canvas(bitmap);
			view.draw(canvas);
	 
			return bitmap;
		}
	@Override
	protected void onResume() 
	{
		super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
		AppEventsLogger.activateApp(this,getResources().getString(R.string.fb_app_id));
		
		/*if(latLng != null)
			move_camera();
			else if(googleMap != null)
				googleMap.clear();
			*/
        if(club_markers.size() == 0) {

            new Update().execute();
        }
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		AppEventsLogger.deactivateApp(this,getResources().getString(R.string.fb_app_id));
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
	}
	
	
	@SuppressLint("NewApi")
	private double[] get_geo_point(String location)
	{
		double[] lat_lng = new double[2];
		Geocoder gc = new Geocoder(this);

		if(Geocoder.isPresent())
		{
		  List<Address> list;
		try {
				list = gc.getFromLocationName(location, 1);
				if(list.size() > 0)
				{
					Address address = list.get(0);
					lat_lng[0] = address.getLatitude();
					lat_lng[1] = address.getLongitude();
				}
			 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		}
		return lat_lng;
	}
	
	private String get_distance(double[] loc)
	{
		String dist = null;
		int distance = 0;
		Location l = new Location("") ;
		l.setLatitude(loc[0]);
		l.setLongitude(loc[1]);
	//	l.setTime(time);
		if(my_location != null)
		distance = (int)my_location.distanceTo(l)/1000;
		dist = String.valueOf(distance);
		if(distance == 0)
		{
			dist = null;
		}
		else if(distance >= 1)
		{
			dist = dist+" km";
		}
		else
		{
			dist = dist+" m";
		}
		
		return dist;


	}
	@Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
    	
			
        return super.onCreateOptionsMenu(menu);
    }
	boolean search = false;
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId())
		{
	
		case android.R.id.home:
			super.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
    }
    ///////////////////////////////////
public class Update extends AsyncTask<String, Long, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            prepare_map();
            if(latLng != null)
                move_camera();
        }

        @Override
		protected Boolean doInBackground(String... params) {
			get_clubs();
			
			return null;
		}

		@Override
		protected void onPostExecute(Boolean result) {


			add_markers();

		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
		}
}
	
	

}
