package com.dpc.wtp.tasks;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;

import com.dpc.wtp.JSONParser;
import com.dpc.wtp.Models.Server_Params;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.activities.App;

public class Update_Tutorial extends AsyncTask<String, Long, Boolean> {
	Context c;

	public Update_Tutorial(Context c) {
		this.c = c;
		
	}

	

	@Override
	protected Boolean doInBackground(String... params) {
		
		return update_points();
	}

	@Override
	protected void onPostExecute(Boolean result) {
		
	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
	}
	
	public boolean update_points()
	{
		boolean success = false;
		JSONParser parser = new JSONParser();
		JSONObject user = null;
		try {
		
		user = parser.makeHttpRequest(C.SERVER_URL, "POST",
				new Server_Params().get_update_tutrs_params(App.get_userid()));
			if(user.getInt(PARAMS.STATUS.get_param()) > 0)
			{
				success = true;
				App.set_tutorial(success);
			}
		
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return success;
	}

}
