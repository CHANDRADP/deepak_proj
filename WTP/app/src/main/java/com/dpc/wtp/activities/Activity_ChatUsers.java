package com.dpc.wtp.activities;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.SubMenu;
import com.dpc.wtp.Models.ActiveChatDetails;
import com.dpc.wtp.Models.ChatMsg;
import com.dpc.wtp.Models.GroupDetails;
import com.dpc.wtp.Models.WTP_Contact;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.customs.CustomDialogWithTwoButtons;
import com.dpc.wtp.customs.Custom_Send_Message;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.interfaces.CustomDilogInterface;
import com.dpc.wtp.services.WTP_Service;

import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;


public class Activity_ChatUsers extends MyActivity
{
    ListView MyChatUsersList;
    private ArrayList<ActiveChatDetails> stringList = new ArrayList();
    ArrayList<ActiveChatDetails> userlist;
    Intent intent;
    int user_id = 0;
    int chat_id = 0;
    CharSequence[] options;

    ActivityChatWindow activity;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_users);
        db = new DatabaseHelper(this);
        intent = new Intent(this, WTP_Service.class);
        MyChatUsersList = (ListView) findViewById(R.id.MyChatUsersList);
        userlist = new ArrayList<ActiveChatDetails>();
        WTP_Service.CURRENT_CHAT_USER="@hellochat";
        get_all_active_usersids();
        activity = new ActivityChatWindow();

        MyChatUsersList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id)
            {

                ActiveChatDetails itemValue = (ActiveChatDetails) MyChatUsersList.getItemAtPosition(position);

                Intent i = new Intent(Activity_ChatUsers.this, ActivityChatWindow.class);

                i.putExtra("unreadcount", itemValue.getUnread_count());
                i.putExtra("isgroup", itemValue.getIs_Group());
                i.putExtra("userid", itemValue.getUserServerId());
                i.putExtra("chatid", itemValue.getChatid());
                i.putExtra("comingFrom", "chatusers");
                startActivity(i);
            }
        });




        MyChatUsersList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()

               {
                     @Override
                      public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
                     {


                           AlertDialog(position);
                           return true;
                     }
               }
        );

    }

    public String get_current_time()
    {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
        return sdf.format(c.getTime());
    }

    public String get_universal_date(String time)
    {
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm a");


        Date date = null;
        try
        {
            //Here you say to java the initial timezone. This is the secret
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = sdf.parse(time);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        //Here you set to your timezone

        sdf.setTimeZone(TimeZone.getDefault());
        time = sdf.format(date);
        return time;
    }





    public void AlertDialog(int position)
    {
        String contact_no = null;

        final ActiveChatDetails itemValue = (ActiveChatDetails) MyChatUsersList.getItemAtPosition(position);

        if(itemValue.getIs_Group()==0)
        {
            options= new CharSequence[]{"Call", "Delete Conversation", "View Profile"};
        }
        else if(itemValue.getIs_Group()==1)
        {
            options= new CharSequence[]{"Leave Group", "Group Details"};
        }

        if(itemValue.getIs_Group()==0)
        {
            WTP_Contact contact = (WTP_Contact) MyChatUsersList.getItemAtPosition(position);
            contact_no = contact.get_number();
            user_id = Integer.parseInt(contact.get_wtp_id());
        }
        else
        {
            user_id = itemValue.getUserServerId();
            chat_id = itemValue.getChatid();

        }

        final String finalContact_no = contact_no;
        AlertDialog.Builder builder=new AlertDialog.Builder(Activity_ChatUsers.this)
                .setTitle("")
                .setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int index) {
                        // TODO Auto-generated method stub
                        Toast.makeText(getApplicationContext(),
                                "The selected option is " + options[index], Toast.LENGTH_LONG).show();
                        if (options[index].equals("Call")) {
                            Intent phoneCallIntent = new Intent(Intent.ACTION_CALL);
                            phoneCallIntent.setData(Uri.parse(finalContact_no));
                            startActivity(phoneCallIntent);
                        }
                        else if(options[index].equals("Delete Conversation"))
                        {
                            delete_conversation(itemValue.getIs_Group());
                        }
                        else if(options[index].equals("View Profile"))
                        {

                        }
                        else if(options[index].equals("Leave Group"))
                        {
                            leave_group();
                        }
                        else if(options[index].equals("Group Details"))
                        {
                            Intent intent = new Intent(Activity_ChatUsers.this, Activity_EditGroup.class);
                            intent.putExtra("servergroupid", user_id);
                            startActivity(intent);

                        }



                    }
                });
        AlertDialog alertdialog=builder.create();
        alertdialog.show();


    }


    public void leave_group()
    {
        FragmentManager fm = getSupportFragmentManager();
        CustomDialogWithTwoButtons dialogtwo = new CustomDialogWithTwoButtons();
        Bundle args = new Bundle();
        args.putString("displayTextSecond", "Are you sure you want to exit from the group?");
        dialogtwo.setArguments(args);

        dialogtwo.show(fm, "Dialog Fragment");


        dialogtwo.setButtonListener(new CustomDilogInterface() {
            @Override
            public void onButtonClickedNew(boolean value) {
                if (value == true) {

                    String localTime = get_current_time();
                    String msgDateTime = get_universal_date(localTime);

                    //get current user wtp id
                    int my_wtp_id = Integer.parseInt(App.get_userid());
                    Log.d("My WTP Id", Integer.toString(my_wtp_id));

                    //Get the group members user ids
                    ArrayList<GroupDetails> groupdata;
                    groupdata = db.get_group_details(Integer.toString(user_id));
                    GroupDetails gd = groupdata.get(0);
                    String members = gd.getGroup_users();
                    String[] memberlist = members.split(", "); // don't remove space else it only 1 name checked
                    List<String> memberarraylist = new LinkedList(Arrays.asList(memberlist));
                    Log.d("My members arraylist", memberarraylist.toString());

                    //remove the current user's user id from list
                    memberarraylist.remove(Integer.toString(my_wtp_id));

                    String admin = null;
                    if (my_wtp_id==Integer.parseInt(App.get_userid()) && memberarraylist.size() > 0) {
                        admin = memberarraylist.get(0);
                    } else {
                        admin = gd.getGroup_admin();
                    }


                    String users = memberarraylist.toString();
                    users = users.substring(1, users.length() - 1);
                    Log.d("My members arraylist without brackets", users);

                    db.update_group_users(users, Integer.toString(user_id), admin); // first so that I should not get this update

                    Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),user_id,"Hi, This is a group update.", C.GROUP_MEMBERS_UPDATE);
                    csm.send_group_message();

                    db.delete_user_complete_conversation(Integer.toString(user_id)); // deleted from chat table & conversation table
                    db.delete_group_details(Integer.toString(user_id));

                }
                if (value == false) {
                    Toast.makeText(Activity_ChatUsers.this, "False", Toast.LENGTH_SHORT).show();
                }


            }


            @Override
            public void onButtonClickedString(String result) {
            }
        });
    }

    public void delete_conversation(final int isgroup)
    {

        FragmentManager fm = getSupportFragmentManager();
        CustomDialogWithTwoButtons dialogtwo = new CustomDialogWithTwoButtons();
        Bundle args = new Bundle();
        args.putString("displayTextSecond", "Are you sure you want to delete complete conversation?");
        dialogtwo.setArguments(args);

        dialogtwo.show(fm, "Dialog Fragment");


        dialogtwo.setButtonListener(new CustomDilogInterface() {
            @Override
            public void onButtonClickedNew(boolean value) {
                if (value == true) {
                    if(isgroup==1)
                    {
                        Log.d("deleting group from conversation table with chat id", chat_id + "userid:"+user_id );
                       // db.delete_group_complete_conversation(chat_id);
                    }
                    else
                    {
                        Log.d("deleting user from chats table with id", Integer.toString(user_id) );
                        //db.delete_user_complete_conversation(user_id);
                    }
                    setUiValues();
                }
                if (value == false) {
                    Toast.makeText(Activity_ChatUsers.this, "False", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onButtonClickedString(String result) {
            }
        });
    }






    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            get_all_active_usersids();
        }
    };

    private void get_all_active_usersids()
    {
        stringList.clear();
        stringList = db.get_active_usernames();
        setUiValues();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
        startService(intent); // remove it after integration
        get_all_active_usersids();
        registerReceiver(broadcastReceiver, new IntentFilter(WTP_Service.BROADCAST_ACTION));
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;
    }

    private void setUiValues()
    {
        userlist.clear();
        for(int p = 0; p<=stringList.size()-1;p++)
        {
            ActiveChatDetails act = stringList.get(p);
            ActiveChatDetails user = new ActiveChatDetails(act.getChatid(),act.getUserServerId(),act.getIs_Group(), act.getLast_msg(), act.getLast_time(), act.getUnread_count());
            userlist.add(user);
        }
        MyChatUsersList.setAdapter(new MyAdapter(userlist, Activity_ChatUsers.this));
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getSupportMenuInflater().inflate(R.menu.open_nonchat_users_menu, menu);
        SubMenu submenu = menu.addSubMenu("");
        submenu.setIcon(R.drawable.abs__ic_menu_moreoverflow_normal_holo_dark);

        submenu.add(2, 1, 1, "Create Group");
        submenu.add(2, 2, 1, "Exit");
        submenu.add(2, 3, 1, "Reset");
        submenu.add(2, 4, 1, "Chat Status");
        submenu.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS |
                MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        return super.onCreateOptionsMenu(menu);
    }


    public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item)
    {
        // Take appropriate action for each action item click
        Intent intent;
        switch (item.getItemId())
        {
            case android.R.id.home:
                intent = new Intent(Activity_ChatUsers.this, Activity_Home.class);
                startActivity(intent);
                finish();
                return true;

            case R.id.open:

                intent = new Intent(this, Activity_NonChatUsers.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;


            case 1:
                Log.d("5th menu called", "Create Group");
                intent  = new Intent(Activity_ChatUsers.this, Activity_CreateGroup.class);
                startActivity(intent);
                return true;

            case 2:
                intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;

            case 3:
                db.clear_all_chat_tables();
                get_all_active_usersids();
                return true;

            case 4:
                FragmentManager fm = getSupportFragmentManager();
                CustomDialogWithTwoButtons dialogtwo = new CustomDialogWithTwoButtons();
                Bundle args = new Bundle();
                String msg = null;
                if(C.MY_CHAT_STATUS == C.ME_ONLINE)
                {
                    msg = "Are you sure you want to go offline";
                }
                else
                {
                    msg = "Are you sure you want to go online";
                }
                args.putString("displayTextSecond", msg);
                dialogtwo.setArguments(args);
                dialogtwo.show(fm, "Dialog Fragment");

                dialogtwo.setButtonListener(new CustomDilogInterface()
                {
                    @Override
                    public void onButtonClickedNew(boolean value) {
                        if (value == true)
                        {
                            if(C.MY_CHAT_STATUS==C.ME_ONLINE)
                            {
                                Presence presence = new Presence(Presence.Type.unavailable);
                                WTP_Service.connection.sendPacket(presence);
                                C.MY_CHAT_STATUS = C.ME_OFFLINE;
                            }
                            else
                            {
                                Presence presence = new Presence(Presence.Type.available);
                                WTP_Service.connection.sendPacket(presence);
                                C.MY_CHAT_STATUS = C.ME_ONLINE;
                            }
                            Roster roster = WTP_Service.connection.getRoster();
                            Presence availability = roster.getPresence(App.get_userid()+"@hellochat");


                        }

                    }

                    @Override
                    public void onButtonClickedString(String result) {
                    }
                });


                return true;


        }
        return super.onOptionsItemSelected(item);
    }


    public static interface OnMessageReceivedListener
    {
        void onMessageReceived(Message message);
    }

    // Callback that performs when device retrieves incoming message.
    private OnMessageReceivedListener onMessageReceivedListener;

    public OnMessageReceivedListener getOnMessageReceivedListener()
    {
        return onMessageReceivedListener;
    }

    public void setOnMessageReceivedListener(OnMessageReceivedListener onMessageReceivedListener)
    {
        this.onMessageReceivedListener = onMessageReceivedListener;
    }
    public void sendMessage(Message message) throws XMPPException
    {
        WTP_Service.connection.sendPacket(message);
    }


    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent intent = new Intent(Activity_ChatUsers.this, Activity_Home.class);
        startActivity(intent);
        finish();
    }

    class MyAdapter extends BaseAdapter
    {

        View v;
        LayoutInflater inflater;
        ViewHolder holder;
        private Context c;
        private ArrayList<ActiveChatDetails> myusers;

        public MyAdapter(ArrayList<ActiveChatDetails> mCurrentList, Context mContext)
        {
            this.myusers = mCurrentList;
            this.c = mContext;
        }

        @Override
        public int getCount()
        {
            if (myusers != null)
                return myusers.size();
            else
                return 0;
        }

        @Override
        public Object getItem(int position)
        {
            if (myusers != null)
                return myusers.get(position);
            else
                return null;
        }

        @Override
        public long getItemId(int position)
        {
            return position;
        }

        private class ViewHolder
        {
            Text txtName;
            Text txtCount;
            Text txtLastMsg;
            Text txtTime;
        }

        @Override
        public View getView(final int arg0,final View convertView, ViewGroup arg2)
        {
            ActiveChatDetails chatobj = myusers.get(arg0);
            v = convertView;
            if (v == null)
            {
                inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.chat_users_list_row, null);
                holder = new ViewHolder();
                holder.txtName = (Text) v.findViewById(R.id.txtName);
                holder.txtCount = (Text) v.findViewById(R.id.txtCount);
                holder.txtLastMsg = (Text) v.findViewById(R.id.txtLastMsg);
                holder.txtTime = (Text) v.findViewById(R.id.txtTime);
                v.setTag(holder);
            }
            else
            {
                holder = (ViewHolder) v.getTag();
            }


            int isgroup = chatobj.getIs_Group();
            int userid = chatobj.getUserServerId();
            Log.d("Isgroup Test", Integer.toString(isgroup));
            Log.d("User id test", Integer.toString(user_id));
            if(isgroup==0)
            {
                String username = db.get_membername_by_id(Integer.toString(userid));
                if(username==null)// delete it later ////////////////////////////////////////////////
                {
                    username = Integer.toString(userid);
                }
                holder.txtName.setText(username);
            }
            else
            {
                String groupname = db.get_groupname_by_id(Integer.toString(userid));
                holder.txtName.setText(groupname);
            }

            if(chatobj.getUnread_count()==0)
            {
                holder.txtCount.setVisibility(View.INVISIBLE);
            }
            else
            {
                holder.txtCount.setText(String.valueOf(chatobj.getUnread_count()));
            }
            String mdate = chatobj.getLast_time();
            if(mdate.equalsIgnoreCase(""))
            {
                holder.txtTime.setText("");
            }
            else
            {
                String[] parts = mdate.split(" ");
                Log.d("Parts",mdate);////////////////////////////////

                String fetcheddate = parts[1];
                String fetchampm = parts[2];
                holder.txtTime.setText(fetcheddate+" "+fetchampm);
            }

            holder.txtLastMsg.setText(activity.getSmiledText(Activity_ChatUsers.this, Html.fromHtml(chatobj.getLast_msg())));
            return v;
        }


    }
}
