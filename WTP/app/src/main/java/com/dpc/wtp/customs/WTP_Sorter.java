package com.dpc.wtp.customs;

import java.util.Comparator;

import com.dpc.wtp.Models.WTP_Contact;

public class WTP_Sorter implements Comparator<WTP_Contact> {
    public int compare(WTP_Contact c1, WTP_Contact c2) {
    	String left = null,right = null;
		left = c1.get_name();
		right = c2.get_name();
    	 
        return left.compareToIgnoreCase(right);
    }
}