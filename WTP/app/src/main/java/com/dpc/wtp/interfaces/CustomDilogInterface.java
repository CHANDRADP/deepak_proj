package com.dpc.wtp.interfaces;

public interface CustomDilogInterface {

    public abstract void onButtonClickedNew(boolean value);

    public abstract void onButtonClickedString(String result);

}
