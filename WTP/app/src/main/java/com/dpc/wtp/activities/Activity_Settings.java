package com.dpc.wtp.activities;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.adapters.Settings_Adapter;
import com.dpc.wtp.customs.Feedback_Dialog;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.customs.Twitter_Login;
import com.facebook.AppEventsLogger;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.analytics.HitBuilders;

public class Activity_Settings extends MyActivity{
	
	public void onCreate(Bundle savedInstanceState) {
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.actionbar);
		final Text title = (Text)findViewById(android.R.id.title);
		title.setText(R.string.ac_settings);
        super.onCreate(savedInstanceState);
        app_tracker.setScreenName(C.SCREEN.SCREEN_SETTINGS.get_screen());
        app_tracker.send(new HitBuilders.AppViewBuilder().build());
        this.setContentView(R.layout.ac_settings);
        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);
        if (savedInstanceState != null) 
		{
		    pendingPublishReauthorization = 
		        savedInstanceState.getBoolean(PENDING_PUBLISH_KEY, false);
		}
		ListView list = (ListView)findViewById(R.id.list);
        list.setDivider(getResources().getDrawable(R.color.txt_header));
        list.setDividerHeight(1);
		list.setAdapter(new Settings_Adapter(this,Arrays.asList(getResources().getStringArray(R.array.settings_list))));
		list.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int posi,
					long arg3) {
				switch(posi)
				{
				case 0:
					Feedback_Dialog feedback = new Feedback_Dialog(Activity_Settings.this);
					feedback.show(getSupportFragmentManager(), "");
					break;
				case 1:
					Session session = Session.getActiveSession();
					if(session != null)
					{
						publishStory();
						C.alert(getApplicationContext(), "Sharing...");
                                                app_tracker.send(new C().generate_analytics(C.CATEGORY.APP_SHARE.get_category(),C.ACTION.ACTION_SHARE.get_action(),C.LABEL.FB.get_label()));

                    }
					else
					{
						
						if (!session.isOpened() && !session.isClosed()){
						    session.openForRead(new Session.OpenRequest(Activity_Settings.this).setPermissions(Arrays.asList("public_profile","user_friends")).setCallback(callback));
						}else{
						    Session.openActiveSession(Activity_Settings.this, true, callback);
						}
					}
					break;
				case 2:
					break;
				case 3:
					clear_all();
					break;
				
				}
			}
			
		});

	}

	private void clear_all()
	{
        GCMRegistrar.unregister(this);
		App.clear_data();
		Session s = Session.getActiveSession();
		if(s != null)
		{
			s.closeAndClearTokenInformation();
		}
		new Twitter_Login(this).clear_twitter();
		App.set_exit(true);
		C.artists.clear();
		C.items.clear();
		C.events.clear();
		C.clubs.clear();
		finish();
		
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		AppEventsLogger.deactivateApp(this,getResources().getString(R.string.fb_app_id));
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;
	}



	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
		AppEventsLogger.activateApp(this,getResources().getString(R.string.fb_app_id));
		
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        
    }
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}


	@Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
    	
			
        return super.onCreateOptionsMenu(menu);
    }
	boolean search = false;
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId())
		{
	
		case android.R.id.home:
			super.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
    }
	
 // ////////////////////////////////////////
 	private UiLifecycleHelper uiHelper;
 	protected Session.StatusCallback callback = new Session.StatusCallback() {
 		@Override
 		public void call(Session session, SessionState state,
 				Exception exception) {
 			onSessionStateChange(session, state, exception);
 		}
 	};
 	private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
 		@Override
 		public void onError(FacebookDialog.PendingCall pendingCall,
 				Exception error, Bundle data) {
 			Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
 		}

 		@Override
 		public void onComplete(FacebookDialog.PendingCall pendingCall,
 				Bundle data) {
 			Log.d("HelloFacebook", "Success!");
 		}
 	};
 	
 	@Override
 	public void onSaveInstanceState(Bundle outState) {
 		super.onSaveInstanceState(outState);
 		outState.putBoolean(PENDING_PUBLISH_KEY, pendingPublishReauthorization);
 		uiHelper.onSaveInstanceState(outState);
 	}

 	private void onSessionStateChange(Session session, SessionState state,
 			Exception exception) {
 		if (state.isOpened()) {
 			if (pendingPublishReauthorization
 					&& state.equals(SessionState.OPENED_TOKEN_UPDATED)) {
 				pendingPublishReauthorization = false;
 				publishStory();
 			}
 		} else if (state.isClosed()) {
 			// shareButton.setVisibility(View.INVISIBLE);
 		}
 	}

 	private void publishStory() {
 		Session session = Session.getActiveSession();

 		if (session != null) {

 			// Check for publish permissions
 			List<String> permissions = session.getPermissions();
 			if (!isSubsetOf(PERMISSIONS, permissions)) {
 				pendingPublishReauthorization = true;
 				Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(
 						this, PERMISSIONS);
 				session.requestNewPublishPermissions(newPermissionsRequest);
 				return;
 			}
 			
 			super.publishStory(getResources().getString(R.string.app_name),C.app_caption,C.app_info,C.app_url,C.app_logo,true,false);
 		}

 	}

 	private boolean isSubsetOf(Collection<String> subset,
 			Collection<String> superset) {
 		for (String string : subset) {
 			if (!superset.contains(string)) {
 				return false;
 			}
 		}
 		return true;
 	}
 }
