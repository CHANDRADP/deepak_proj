package com.dpc.wtp.adapters;


import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.dpc.wtp.R;
import com.dpc.wtp.Models.WTP_Contact;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.activities.App;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class Invite_Friends_Adapter extends BaseAdapter {
	LayoutInflater inflater;
	ViewHolder holder;
	View v;
	List<WTP_Contact> items = new ArrayList<WTP_Contact>();
	Context c;
	ImageLoader loader;
	DisplayImageOptions options;
	private class ViewHolder {
		Text name;
		ImageView user_pick,chack_box;
		
		
	}
	public Invite_Friends_Adapter(Context c,List<WTP_Contact> l,ImageLoader loader,DisplayImageOptions options)
	{
		this.c = c;
		this.items = l;
		this.loader = loader;
		this.options = options;
		
	}
	public ArrayList<WTP_Contact> get_selected()
	{
		ArrayList<WTP_Contact> contacts = new ArrayList<WTP_Contact>();
		for(WTP_Contact c : items)
		{
			if(c.is_selected())
			{
				contacts.add(c);
				Log.d("id", c.get_wtp_id());
			}
		}
		return contacts;
	}
	
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		WTP_Contact contact = items.get(position);
		
		v = convertView;
		if (v == null) 
		{
			 inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 v = inflater.inflate(R.layout.row_friends, null);
			 holder = new ViewHolder();
			 holder.name = (Text) v.findViewById(R.id.user_name);
			 holder.user_pick = (ImageView) v.findViewById(R.id.user_pick);
			 holder.chack_box = (ImageView) v.findViewById(R.id.check_box);
			 v.setTag(holder);
			 
		}
		else
			holder = (ViewHolder) v.getTag();
		
		
		holder.name.setText(contact.get_name());
		//if(contact.is_fb_contact())
		loader.displayImage(contact.get_thumbnail(), holder.user_pick, options);
		//else if(contact.get_thumbnail() != null)
		//	holder.user_pick.setImageBitmap(new C().get_image(contact.get_thumbnail()));
		//else
		//	holder.user_pick.setBackgroundResource(R.drawable.user);
		
		set_favorite(holder.chack_box,contact.is_selected());
		
		holder.chack_box.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				items.get(position).set_selected(!items.get(position).is_selected());
				set_favorite((ImageView) v,items.get(position).is_selected());
			}
			
		});
		
		
		
		return v;

	}

	private void set_favorite(ImageView img,boolean selected)
	{
		if(!selected)
		{
			img.setBackgroundResource(R.drawable.going_d);
		}
		else
		{
			img.setBackgroundResource(R.drawable.going_n);
		}
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}


	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
	}


	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

}