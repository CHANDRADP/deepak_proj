package com.dpc.wtp.tasks;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.dpc.wtp.Models.PendingGroupMsg;
import com.dpc.wtp.Models.UploadDetails;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.Utils.NetworkUtil;
import com.dpc.wtp.activities.ActivityChatWindow;
import com.dpc.wtp.activities.App;
import com.dpc.wtp.customs.Custom_Send_Message;
import com.dpc.wtp.services.WTP_Service;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by user on 3/25/2015.
 */



public class NetworkChangeReceiverTask extends BroadcastReceiver {
    private Net_Detect net;
    DatabaseHelper db;
    public static XMPPConnection connection;
    Context cxt;
    ArrayList<UploadDetails> chats = new ArrayList<UploadDetails>();
    ArrayList<PendingGroupMsg> pgm = new ArrayList<PendingGroupMsg>();



    @Override
    public void onReceive(final Context context, final Intent intent)
    {
        String status = NetworkUtil.getConnectivityStatusString(context);
        Toast.makeText(context, status, Toast.LENGTH_LONG).show();
        net = new Net_Detect(context);
        db = new DatabaseHelper(context);
        cxt = context;
       if(status.equalsIgnoreCase("Wifi enabled"))
       {
           if(net.isConnectingToInternet())
           {



               WTP_Service wtp_service = new WTP_Service();
               wtp_service.create_connection(context);

               //send_pending_nongroup_messages(context);
               if(WTP_Service.connection.isConnected())
               {
                   send_pending_group_messages(context);
                   send_pending_nongroup_messages(context);
               }
               else
               {
                   try {
                       Thread.sleep(5000);
                   } catch (InterruptedException e) {
                       e.printStackTrace();
                   }
                   if(WTP_Service.connection.isConnected())
                   {
                       send_pending_group_messages(context);
                       send_pending_nongroup_messages(context);
                   }

               }



           }
       }
        else if(status.equalsIgnoreCase("Mobile data enabled"))
       {
           if(net.isConnectingToInternet())
           {

               WTP_Service wtp_service = new WTP_Service();
               wtp_service.create_connection(context);

               //send_pending_nongroup_messages(context);
               //send_pending_group_messages(context);
           }
       }

    }

    public void send_pending_nongroup_messages(Context context) {
        // get data from db with status Uploading and send msg
        Log.d("Send pending non group messages", "");
        chats = db.get_notsent_nongroup_messages();
        for (int i = 0; i < chats.size(); i++) {
            UploadDetails ud = chats.get(i);
            int messagetype = ud.getMediatype();
            String displayMesssageType = null;

            boolean sent = false;
            Custom_Send_Message csm = new Custom_Send_Message(context, ud.getTo(), ud.getTextmsg(), messagetype);
            sent = csm.send_simple_text_message(Integer.toString(ud.getTo()));
            if(sent)
            {
                db.update_pending_msg_status_to_sent(Integer.toString(ud.getRowId()));
            }

        }
    }



    public void send_pending_group_messages(Context context)
    {
        // get data from db with status Uploading and send msg
        Log.d("Send pending group messages", "");
        pgm = db.get_notsent_group_messages();
        for(int i=0;i<pgm.size();i++)
        {
            PendingGroupMsg ud = pgm.get(i);
            int messagetype = ud.getMediatype();

            boolean sent = false;
            Custom_Send_Message csm = new Custom_Send_Message(context, ud.getServergroupid(),ud.getTextmsg(),messagetype);
            sent = csm.send_pending_group_message(Integer.toString(ud.getMemberid()), Integer.toString(ud.getServergroupid()));
            if(sent)
            {
               db.delete_pending_group_msg_entry(Integer.toString(ud.getRowId()));
            }
        }
    }



}