package com.dpc.wtp.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.customs.MyActivity;
import com.facebook.AppEventsLogger;
import com.facebook.Session;
import com.google.android.gms.analytics.HitBuilders;

public class Activity_City extends MyActivity{
	
	public static String[] cities = new String[]{"Bangalore","Chennai","Delhi","Mumbai","Hydrabad","Kolkata","Pune"};
	
	public void onCreate(Bundle savedInstanceState) {
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.actionbar);
		final Text title = (Text)findViewById(android.R.id.title);
		title.setText(R.string.ac_city);
		
        super.onCreate(savedInstanceState);
        app_tracker.setScreenName(C.SCREEN.SCREEN_CITY.get_screen());
        app_tracker.send(new HitBuilders.AppViewBuilder().build());
        this.setContentView(R.layout.ac_settings);
        App.set_exit(false);
		ListView list = (ListView)findViewById(R.id.list);
		list.setDivider(new ColorDrawable(getResources().getColor(R.color.txt_header)));
		list.setDividerHeight(1);
		list.setAdapter(new City_Adapter());
		list.setPadding(5, 0, 5, 0);
		list.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int posi,
					long arg3) {

				App.set_city(String.valueOf(posi+1));

                Intent i = new Intent(Activity_City.this,Activity_Home.class);
				i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(i);

                finish();
                app_tracker.send(new C().generate_analytics(C.CATEGORY.LOCATION.get_category(),C.ACTION.ACTION_OK.get_action(),cities[posi]));

            }
			
		});


	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
		AppEventsLogger.activateApp(this,getResources().getString(R.string.fb_app_id));
		
		if(!App.is_tutorial_finished())
		{
			startActivity(new Intent(this,Activity_Tutorial.class));
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		AppEventsLogger.deactivateApp(this,getResources().getString(R.string.fb_app_id));
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;
		
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
    	
			
        return super.onCreateOptionsMenu(menu);
    }
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId())
		{
	
		case android.R.id.home:
			super.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
    }
    
    
	public class City_Adapter extends BaseAdapter {
		ViewHolder holder;
		View v;
		LayoutInflater inflater;
		String city = App.get_city();
		private class ViewHolder {
			Text item;
			ImageView icon;
			
		}
		
		public View getView(final int position, View convertView, ViewGroup parent)
		{
			v = convertView;
			if (v == null) 
			{
				 inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				 v = inflater.inflate(R.layout.row_city, null);
				 holder = new ViewHolder();
				 holder.item = (Text) v.findViewById(R.id.name);
				 holder.icon = (ImageView) v.findViewById(R.id.icon);
				 v.setTag(holder);
				 
			}
			else
				holder = (ViewHolder) v.getTag();
			
			holder.item.setText(cities[position]);
			if(city != null)
			{
				if(city.equalsIgnoreCase(String.valueOf(position+1)))
				{
					holder.icon.setBackgroundResource(R.drawable.going_n);
					holder.icon.setVisibility(View.VISIBLE);
				}
				else
				{
					holder.icon.setVisibility(View.GONE);
				}
			}
			
			
			return v;

		}


		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return cities.length;
		}


		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return cities[position];
		}


		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

	}
	

}
