package com.dpc.wtp.tasks;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.dpc.wtp.JSONParser;
import com.dpc.wtp.Models.Artist;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Models.Review;
import com.dpc.wtp.Models.Server_Params;
import com.dpc.wtp.Models.V_Details;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.activities.App;

public class Offline_Task extends AsyncTask<String, Long, Boolean> {
	Context c;
	private DatabaseHelper h;
	JSONParser parser = new JSONParser();
	JSONObject user = null;
	public Offline_Task(Context c) 
	{
		this.c = c;
		h = new DatabaseHelper(c);
	}

	
	@Override
	protected Boolean doInBackground(String... params) {
		ArrayList<Event> attends = h.get_pending_event_status();
		for(int i = 0;i < attends.size();i++)
		{
			Event event = attends.get(i);
			if(update_attend_type(event.get_type() == 1 ? true : false,String .valueOf(event.get_event_going_type()),event.get_event_id()))
			{
				
				h.remove_attend_type(event.get_dbid());
			//	Event e = C.items.get(event.get_event_id());
			//	C.update_items(e);
				
			}
		}
		ArrayList<V_Details> stars = h.get_pending_venue_stars();
		for(int i = 0;i < stars.size();i++)
		{
			V_Details star = stars.get(i);
			if(update_stars(star.get_venue_type() == 1 ? true : false,star.get_vd_id(),String.valueOf(star.get_stars())))
			{
				h.remove_club_rating(star.get_dbid());
			//	V_Details club = C.clubs.get(vd_id);
			//	club.set_rating(user.getInt(PARAMS.STATUS.get_param()));
			//	C.update_clubs(club);
			}
		}
		ArrayList<Artist> artists = h.get_pending_follows();
		for(int i = 0;i < artists.size();i++)
		{
			Artist artist = artists.get(i);
			if(update_follow(!artist.is_following(),artist.get_id()))
			{
				h.remove_artist_follow(artist.get_dbid());
			}
			
		}
		ArrayList<Review> reviews = h.get_pending_reviews();
		for(int i = 0;i < reviews.size();i++)
		{
			Review review = reviews.get(i);
			if(update_review(review.get_id(),review.get_review(),review.get_time()))
			{
				h.remove_review(review.get_dbid());
			}
			
		}
	
		return true;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		
		Intent i = new Intent(C.BROADCAST_MSG);
		i.putExtra(PARAMS.VENUE_DETAILS.get_param(), " ");
		i.putExtra(PARAMS.EVENT_ID.get_param(), " ");
		i.putExtra(PARAMS.ARTIST.get_param(), " ");
		c.sendBroadcast(i);

	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
	}
	public boolean update_attend_type(boolean add,String type,String event_id)
	{
		boolean success = false;
		
		try {
		if(add)
		{
		user = parser.makeHttpRequest(C.SERVER_URL, "POST",
				new Server_Params().get_add_attend_params(App.get_userid(),type, event_id));
			if(user.getInt(PARAMS.STATUS.get_param()) > 0)
			{
				success = true;
			}
		}
		else
		{
			user = parser.makeHttpRequest(C.SERVER_URL, "POST",
					new Server_Params().get_update_attend_params(App.get_userid(),type, event_id));
			if(user.getInt(PARAMS.STATUS.get_param()) > 0)
			{
				success = true;
			}
		}
		
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return success;
	}
	public boolean update_stars(boolean add,String vd_id,String stars_no)
	{
		boolean success = false;
		user = null;
		try {
		if(add)
		{
		user = parser.makeHttpRequest(C.SERVER_URL, "POST",
				new Server_Params().get_add_star_params(App.get_userid(), vd_id,stars_no));
			if(user.getInt(PARAMS.STATUS.get_param()) > 0)
			{
				success = true;
			}
		}
		else
		{
			user = parser.makeHttpRequest(C.SERVER_URL, "POST",
					new Server_Params().get_update_star_params(App.get_userid(), vd_id,stars_no));
			if(user.getInt(PARAMS.STATUS.get_param()) > 0)
			{
				V_Details club = C.clubs.get(vd_id);
				club.set_rating(user.getInt(PARAMS.STATUS.get_param()));
				C.update_clubs(club);
				success = true;
			}
		}
		
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return success;
	}
	
	public boolean update_follow(boolean add,String artist_id)
	{
		boolean success = false;
		user = null;
		try {
		if(add)
		{
		user = parser.makeHttpRequest(C.SERVER_URL, "POST",
				new Server_Params().get_add_follow_params(App.get_userid(),artist_id));
			if(user.getInt(PARAMS.STATUS.get_param()) > 0)
			{
				success = true;
			}
		}
		else
		{
			user = parser.makeHttpRequest(C.SERVER_URL, "POST",
					new Server_Params().get_remove_follow_params(App.get_userid(), artist_id));
			if(user.getInt(PARAMS.STATUS.get_param()) > 0)
			{
				success = true;
			}
		}
		
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return success;
	}

	
	public boolean update_review(String vd_id,String review,String time)
	{
		boolean success = false;
		user = null;
		try {
			 ArrayList<NameValuePair> params = new Server_Params().get_add_review_params(App.get_userid(), vd_id,review);
			 params.add(new BasicNameValuePair(C.PARAMS.TIME.get_param(),time));
			 user = parser.makeHttpRequest(C.SERVER_URL, "POST",params
				);
			if(user.getInt(PARAMS.STATUS.get_param()) > 0)
			{
				success = true;
			}
		
		
		
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return success;
	}
}