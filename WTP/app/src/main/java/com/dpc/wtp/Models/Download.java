package com.dpc.wtp.Models;




public class Download
{
    private String url;
    private String content_type;


    public Download(String url, String content_type) {
        this.url = url;
        this.content_type = content_type;

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }
}
