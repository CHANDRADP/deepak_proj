package com.dpc.wtp.customs;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.activities.Activity_City;
import com.dpc.wtp.activities.App;
import com.dpc.wtp.tasks.Update_FB_TWIT;
import com.dpc.wtp.tasks.Update_points;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionDefaultAudience;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public abstract class MyActivity extends SherlockFragmentActivity{

    protected Tracker app_tracker = null;

	protected DatabaseHelper db;
	protected  ImageLoader imageLoader  = ImageLoader.getInstance();;
	protected DisplayImageOptions options = new DisplayImageOptions.Builder()
	.showStubImage(R.drawable.default_bg)
	.showImageForEmptyUri(R.drawable.default_bg)
	.showImageOnFail(R.drawable.default_bg)
	.cacheInMemory(true)
//	.cacheOnDisc(true)
	.bitmapConfig(Bitmap.Config.RGB_565)
    .displayer(new FadeInBitmapDisplayer(5))
	.build();
	protected static final List<String> PERMISSIONS = Arrays
			.asList("publish_actions");
	protected static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
	protected boolean pendingPublishReauthorization = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

        app_tracker = ((App) getApplication()).getTracker(
                App.WTP_Tracker.WTP_TRACKER);
		// TODO Auto-generated method stub
		if(!imageLoader.isInited())
    	{
    		App.initImageLoader(this.getApplicationContext());
    		imageLoader = ImageLoader.getInstance();
    	}
		
		super.onCreate(savedInstanceState);
		
	}
	@Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        

        
    }
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
	}
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
         
    }
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}
	
	private void request_permissions()
	{
		Session session = Session.getActiveSession();
		List<String> permissions = session.getPermissions();
        if (!permissions.contains(PERMISSIONS)) {
        	pendingPublishReauthorization = true;
            requestPublishPermissions(session);
            return;
        }
	}
	private static final int REAUTH_ACTIVITY_CODE = 100;
    
	private void requestPublishPermissions(Session session) {
        if (session != null) {
            Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(this, PERMISSIONS)
                    // demonstrate how to set an audience for the publish permissions,
                    // if none are set, this defaults to FRIENDS
                    .setDefaultAudience(SessionDefaultAudience.FRIENDS)
                    .setRequestCode(REAUTH_ACTIVITY_CODE);
            session.requestNewPublishPermissions(newPermissionsRequest);
        }
    }
	public void publishStory(String name,String caption,String desc,String link,String picture,final boolean from_settings,final boolean is_event) {
		final Session session = Session.getActiveSession();
	    
	    
	    
        
        Bundle postParams = new Bundle();
        postParams.putString("name", name);
        postParams.putString("caption", caption);
        postParams.putString("description", desc);
        postParams.putString("link", link);
        postParams.putString("picture", picture);
        //postParams.putString("place", "155021662189");
        
        Request.Callback callback= new Request.Callback() {
            public void onCompleted(Response response) {
                GraphObject graphResponse = response
                                           .getGraphObject();
                           
                if(graphResponse != null)
                {
                	JSONObject obj = graphResponse.getInnerJSONObject();
                	 String postId = null;
                     try {
                         postId = obj.getString("id");
                         if(postId != null && (from_settings || is_event))
                         {
                        	 C.alert(getApplicationContext(), "Shared successfully");
                         }
                         
                     } catch (JSONException e) {
                         Log.i("fb",
                             "JSON error "+ e.getMessage());
                     }
                     FacebookRequestError error = response.getError();
                     if (error != null) {
                    	 C.alert(getApplicationContext(), "error in sharing,please try after some time");
                         } else {
                             
                     } 
                }
               
            }
        };
        
        final Request request = new Request(session, "me/feed", postParams, 
                              HttpMethod.POST, callback);
        
      // RequestAsyncTask task = new RequestAsyncTask(request);
      //  task.execute();
        Request request1 = new Request(session, "me/friends", null, 
                HttpMethod.GET, new Request.Callback() {
    				
    				@Override
    				public void onCompleted(Response response) {
    					// TODO Auto-generated method stub
    					
    					try {
    						int count = response
    						        .getGraphObject()
    						        .getInnerJSONObject().getJSONObject("summary").getInt("total_count");
    						Log.d("frnds_count", String.valueOf(count));
    						if(from_settings)
    						{
    							count *= 2;
    						}
    						else if(is_event)
    						{
    							count = 50;
    						}
    						count += App.get_userpoints();
    						//points +=  points;
    						App.update_userpoints(String.valueOf(count));
    						new Update_points(MyActivity.this).execute();
    						RequestAsyncTask task = new RequestAsyncTask(request);
    					        task.execute();
    					        
    					}
    					 catch (JSONException e) {
    							// TODO Auto-generated catch block
    							e.printStackTrace();
    						}
    				}
     	});
     	request1.executeAsync();
     	App.set_shared_onfb(true);
        
   
	}

	
	public void generate_SHA_key_for_facebook()
	{
				try {
		    PackageInfo info = getPackageManager().getPackageInfo(
		            "com.dpc.wtp", 
		            PackageManager.GET_SIGNATURES);
		    for (Signature signature : info.signatures) {
		        MessageDigest md = MessageDigest.getInstance("SHA");
		        md.update(signature.toByteArray());
		        Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
		        }
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
	
	/*@SuppressWarnings({ "deprecation", "unused" })
	private void signInWithFacebook() {

		SessionTracker  mSessionTracker = new SessionTracker(getBaseContext(), new StatusCallback() {

			@Override
			public void call(Session session, SessionState state,
					Exception exception) {
				// TODO Auto-generated method stub
				
			}

	        
	    }, null, false);

	    String applicationId = Utility.getMetadataApplicationId(getBaseContext());
	     mCurrentSession = mSessionTracker.getSession();

	    if (mCurrentSession == null || mCurrentSession.getState().isClosed()) {
	        mSessionTracker.setSession(null);
	        Session session = new Session.Builder(getBaseContext()).setApplicationId(applicationId).build();
	        Session.setActiveSession(session);
	        mCurrentSession = session;
	    }

	    if (!mCurrentSession.isOpened()) {
	        Session.OpenRequest openRequest = null;
	        openRequest = new Session.OpenRequest(this);

	        if (openRequest != null) {
	            openRequest.setDefaultAudience(SessionDefaultAudience.FRIENDS);
	            openRequest.setPermissions(Arrays.asList("public_profile","user_friends"));
	           // openRequest.setLoginBehavior(SessionLoginBehavior.SSO_WITH_FALLBACK);

	            mCurrentSession.openForRead(openRequest);
	        }
	    }else {
	        Request.executeMeRequestAsync(mCurrentSession, new Request.GraphUserCallback() {
	              @Override
	              public void onCompleted(GraphUser user, Response response) {
	                  Log.w("myConsultant", user.getId() + " " + user.getName() + " " + user.getInnerJSONObject());
	              }
	            });
	    }
	}
	*/
	////////////////////////////////
	
	
}
