/**
* @author D-P-C
* Jul 17, 2014 2014

*/
package com.dpc.wtp.services;

import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.activities.App;
import com.dpc.wtp.tasks.Offline_Task;
import com.dpc.wtp.tasks.Update_Users;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;

/**
 * @author D-P-C
 *
 */
public class DataChanged  extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
    	Log.d("data state", intent.getAction());
    	if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION))
         {
    		Net_Detect c = new Net_Detect(context);
			if(c.isConnectingToInternet())
			{
				Log.d("data state", "CONNECTED");
				if(!App.is_service_running())
		        {
					context.startService(new Intent(context,WTP_Service.class));
		        }
	    		new Update_Users(context).execute();
	    		new Offline_Task(context).execute();
			}
    		
    		
         }
    	 
    }

}