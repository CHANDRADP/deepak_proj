package com.dpc.wtp.Models;



/**
 * Created by user on 1/12/2015.
 */
public class ActiveChatDetails
{


    private String last_msg, last_time;
    private int chatid,userServerId, is_Group, unread_count;

    public ActiveChatDetails(int chatid, int userServerId, int isgroup, String last_msg, String last_time, int unread_count) {
        this.userServerId = userServerId;
        this.is_Group = isgroup;
        this.chatid = chatid;
        this.last_msg = last_msg;
        this.last_time = last_time;
        this.unread_count = unread_count;
    }

    public String getLast_msg() {
        return last_msg;
    }

    public void setLast_msg(String last_msg) {
        this.last_msg = last_msg;
    }

    public String getLast_time() {
        return last_time;
    }

    public void setLast_time(String last_time) {
        this.last_time = last_time;
    }

    public int getChatid() {
        return chatid;
    }

    public void setChatid(int chatid) {
        this.chatid = chatid;
    }

    public int getUserServerId() {
        return userServerId;
    }

    public void setUserServerId(int userServerId) {
        this.userServerId = userServerId;
    }

    public int getIs_Group() {
        return is_Group;
    }

    public void setIs_Group(int is_Group) {
        this.is_Group = is_Group;
    }

    public int getUnread_count() {
        return unread_count;
    }

    public void setUnread_count(int unread_count) {
        this.unread_count = unread_count;
    }
}
