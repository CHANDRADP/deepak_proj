package com.dpc.wtp.adapters;


import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.dpc.wtp.R;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.Utils.TextB;
import com.dpc.wtp.tasks.Update_Going_Status;
import com.google.android.gms.analytics.Tracker;

public class Club_Details_Adapter extends BaseAdapter {
	LayoutInflater inflater;
	ViewHolder holder;
	View v;
	List<Event> items;
	Context c;
	private Net_Detect net;
	DatabaseHelper db;
    Tracker app_tracker;
	private class ViewHolder {
		TextB date;
		Text name,artist,genre;
		ImageView status_icon;
		
		
	}
	public Club_Details_Adapter(Context c,List<Event> l,Tracker tracker)
	{
		this.c = c;
		this.items = l;
		db = new DatabaseHelper(c);
		net = new Net_Detect(this.c);
        this.app_tracker = tracker;
	}
	
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		final Event event = items.get(position);
		
		v = convertView;
		if (v == null) 
		{
			 inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 v = inflater.inflate(R.layout.row_club_details,parent, false);
			 holder = new ViewHolder();
			 holder.date = (TextB) v.findViewById(R.id.date);
			 holder.name = (Text) v.findViewById(R.id.e_name);
			 holder.artist = (Text) v.findViewById(R.id.artist);
			 holder.genre = (Text) v.findViewById(R.id.genre);
			 holder.status_icon = (ImageView) v.findViewById(R.id.status_icon);
			 v.setTag(holder);
			 
		}
		else
			holder = (ViewHolder) v.getTag();
		
		String[] date = C.get_date_as_day_month(event.get_event_date());
		holder.date.setText(date[1]+"\n"+C.get_date_suffix(date[0].toUpperCase()));
		set_text(holder.name,event.get_event_name());
        if(event.getLineups().size() > 0)
        set_text(holder.artist,event.getLineups().get(0).getName());
        for(int i = 0;i < event.getLineups().size();i++)
            if(C.artists.get(event.getLineups().get(i).getArtistid()) != null) {
                set_text(holder.genre, C.artists.get(event.getLineups().get(i).getArtistid()).get_genre_type());
                break;
            }
		
		if(event.get_event_going_type() == 1)
		holder.status_icon.setBackgroundResource(R.drawable.going_p);
		else
			holder.status_icon.setBackgroundResource(R.drawable.going_n);
		
		holder.status_icon.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				String type = null;
				boolean add = false;
				if(event.get_event_going_type() == 0)
				{
					add = true;
					type = "1";
                    app_tracker.send(new C().generate_analytics(C.CATEGORY.EVENTS.get_category(),C.ACTION.ACTION_ATTEND.get_action(),C.LABEL.YES.get_label()));

                }
				else if(event.get_event_going_type() > 0)
				{
					type = "-1";
                    app_tracker.send(new C().generate_analytics(C.CATEGORY.EVENTS.get_category(),C.ACTION.ACTION_ATTEND.get_action(),C.LABEL.NO.get_label()));

                }
				else
				{
					type = "1";
                    app_tracker.send(new C().generate_analytics(C.CATEGORY.EVENTS.get_category(),C.ACTION.ACTION_ATTEND.get_action(),C.LABEL.YES.get_label()));

                }
				
				if(net.isConnectingToInternet())
				{
					
					new Update_Going_Status(c,event.get_event_id(),type,add).execute();
					
				}
				else
				{
					db.update_event_attending_type(event.get_event_id(), add == true ? "1" : "2", type);
				}
				items.get(position).set_goingtype(Integer.parseInt(type));
                set_going((ImageView)v,type);
			}
			
		});
		
		holder.artist.setVisibility(View.VISIBLE);
		return v;

	}

    private void set_going(ImageView img,String type)
    {
        if(type.equals("-1"))
        {
            img.setBackgroundResource(R.drawable.going_n);
        }
        else
        {
            img.setBackgroundResource(R.drawable.going_p);
        }
    }
	private void set_text(Text t_view, String txt) {
		// TODO Auto-generated method stub
		t_view.setText(txt);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}


	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
	}


	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

}