package com.dpc.wtp.Models;

/**
 * Created by user on 1/12/2015.
 */
public class ChatMsg
{
    private String datetime, msg, server_url;
    private int sno, chatid, isDate,poster_type,media_type, media_status, delivery_status;

    public ChatMsg(int sno, int chatid, String datetime, int poster_type, String msg, int isDate, int media_type, int media_status, String server_url, int delivery_status) {

        this.sno = sno;
        this.chatid = chatid;
        this.datetime = datetime;
        this.poster_type = poster_type;
        this.msg = msg;
        this.isDate=isDate;
        this.media_type=media_type;
        this.media_status = media_status;
        this.server_url = server_url;
        this.delivery_status = delivery_status;

    }

    public int getDelivery_status() {
        return delivery_status;
    }

    public void setDelivery_status(int delivery_status) {
        this.delivery_status = delivery_status;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_url() {
        return server_url;
    }

    public void setServer_url(String server_url) {
        this.server_url = server_url;
    }

    public int getSno() {
        return sno;
    }

    public void setSno(int sno) {
        this.sno = sno;
    }

    public int getChatid() {
        return chatid;
    }

    public void setChatid(int chatid) {
        this.chatid = chatid;
    }

    public int getIsDate() {
        return isDate;
    }

    public void setIsDate(int isDate) {
        this.isDate = isDate;
    }

    public int getPoster_type() {
        return poster_type;
    }

    public void setPoster_type(int poster_type) {
        this.poster_type = poster_type;
    }

    public int getMedia_type() {
        return media_type;
    }

    public void setMedia_type(int media_type) {
        this.media_type = media_type;
    }

    public int getMedia_status() {
        return media_status;
    }

    public void setMedia_status(int media_status) {
        this.media_status = media_status;
    }
}
