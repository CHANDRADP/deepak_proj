package com.dpc.wtp.adapters;

import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dpc.wtp.R;
import com.dpc.wtp.activities.ActivityChatWindow;
import com.dpc.wtp.amazon.UploadService;


public class ViewPager_Adapter extends PagerAdapter {

    private Activity _activity;
    private ArrayList<Integer> smiley;
    private LayoutInflater inflater;
    private boolean sticker = false;
    private GridView grid;

    public static final int STICKER = 5;
    MediaRecorder myAudioRecorder = null;
    String outputFile = null;
    Button start,stop,play;
    // constructor
    public ViewPager_Adapter(Activity activity,
                             ArrayList<Integer> smileys,boolean sticker) {
        this._activity = activity;
        this.smiley = smileys;
        this.sticker = sticker;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int posi) {

        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.grid_view, container,
                false);
        grid = (GridView) viewLayout.findViewById(R.id.grid);
        if(posi == 0)
        {
            grid.setNumColumns(5);
            Smiley_Adapter	smiley_adapter = new Smiley_Adapter(_activity,false);
            grid.setAdapter(smiley_adapter);
            grid.setOnItemClickListener(new OnItemClickListener()
            {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, final int position,
                                        long arg3) {

                    ActivityChatWindow.update_txt(_activity, position);
                }
            });
        }
        else if(posi == 1)
        {
            grid.setNumColumns(3);
            Sticker_Adapter	sticker_adapter = new Sticker_Adapter(_activity,true);
            grid.setAdapter(sticker_adapter);
            grid.setOnItemClickListener(new OnItemClickListener()
            {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, final int position,
                                        long arg3) {
                    Intent intent = new Intent(UploadService.UPLOAD_COMPLETE_BROADCAST);
                    intent.putExtra("customsticker",position);
                    intent.putExtra("messagetype", STICKER);
                    _activity.sendBroadcast(intent);
                }
            });
        }

        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }



    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((LinearLayout) object);

    }

}
