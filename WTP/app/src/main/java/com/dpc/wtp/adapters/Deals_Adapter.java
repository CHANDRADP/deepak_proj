package com.dpc.wtp.adapters;


import java.io.ByteArrayOutputStream;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.dpc.wtp.R;
import com.dpc.wtp.Models.Deals;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.Utils.TextB;
import com.dpc.wtp.tasks.Display_Image;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class Deals_Adapter extends BaseAdapter {
	LayoutInflater inflater;
	ViewHolder holder;
	View v;
	List<Deals> items;
//	int[] deals = new int[]{R.drawable.deal1,R.drawable.deal2,R.drawable.deal3};
	Context c;
	ImageLoader loader;
	DisplayImageOptions options;
	DatabaseHelper db;
	private class ViewHolder {
		Text name,location,count;
        TextB grab;
		ImageView image;
		
		
	}
	public Deals_Adapter(Context c,List<Deals> l, ImageLoader imageLoader, DisplayImageOptions options)
	{
		this.c = c;
		this.items = l;
		this.loader = imageLoader;
		this.options = new DisplayImageOptions.Builder()
		.showStubImage(R.drawable.default_bg)
		.showImageForEmptyUri(R.drawable.default_bg)
		.showImageOnFail(R.drawable.default_bg)
		//.cacheInMemory(true)
		//.cacheOnDisc(true)
                .displayer(new FadeInBitmapDisplayer(C.ANIM_DURATION))
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
		db = new DatabaseHelper(c);
	}
	
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		final Deals deal = items.get(position);
		
		v = convertView;
		if (v == null) 
		{
			 inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 v = inflater.inflate(R.layout.row_deals, null);
			 holder = new ViewHolder();
			 holder.name = (Text) v.findViewById(R.id.v_name);
            holder.location = (Text) v.findViewById(R.id.v_location);
            holder.count = (Text) v.findViewById(R.id.count);
            holder.grab = (TextB) v.findViewById(R.id.grab);
			 holder.image = (ImageView) v.findViewById(R.id.deal_image);
			 v.setTag(holder);
			 
		}
		else
			holder = (ViewHolder) v.getTag();

        holder.name.setText(deal.get_name());
        holder.location.setText(C.clubs.get(deal.get_venue_id()).get_venue_name());
        holder.count.setText(String.valueOf(C.deals.get(deal.get_venue_id()).size()));
		if(deal.get_image() != null)
		if(db.is_image_available(deal.get_image().get_id()))
		{
			new Display_Image(c,deal.get_image().get_id(),holder.image).execute();
			
		}
		else
		{
			loader.displayImage(deal.get_image().get_image(), holder.image, options, new ImageLoadingListener(){

				@Override
				public void onLoadingStarted(String imageUri, View view) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onLoadingFailed(String imageUri, View view,
						FailReason failReason) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onLoadingComplete(String imageUri, View view,
						Bitmap loadedImage) {
					// TODO Auto-generated method stub
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					loadedImage.compress(Bitmap.CompressFormat.JPEG, 80, baos);
					byte[] b = baos.toByteArray(); 
					String image = Base64.encodeToString(b, Base64.DEFAULT);
					
					long id1 = db.add_images_for_optimization(deal.get_image().get_id(), image);
					if(id1 > 0)
					{
						Log.d("optimized image saved", "optimized image saved");
					}
				}

				@Override
				public void onLoadingCancelled(String imageUri, View view) {
					// TODO Auto-generated method stub
					
				}
				
			});
		}
		else
		{
			holder.image.setBackgroundResource(R.drawable.default_bg);
		}
	
		
		return v;

	}


	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}


	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
	}


	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

}