package com.dpc.wtp.activities;

import java.util.ArrayList;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.actionbarsherlock.view.MenuItem;
import com.dpc.wtp.R;
import com.dpc.wtp.Models.Artist;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.adapters.Following_Adapter;
import com.dpc.wtp.customs.MyActivity;
import com.facebook.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

public class Activity_Favorites extends MyActivity {

	private ArrayList<Artist> list_items_following = new ArrayList<Artist>();

	private Following_Adapter following_adapter;

	private ListView list;

	public Activity_Favorites() {
		options = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.artist)
				.showImageForEmptyUri(R.drawable.artist)
				.showImageOnFail(R.drawable.artist).cacheInMemory(true)
				.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565).build();

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.actionbar);
		Text title = (Text) findViewById(android.R.id.title);
		title.setText("Following artists");
		super.onCreate(savedInstanceState);
        app_tracker.setScreenName(C.SCREEN.SCREEN_FAVORITES.get_screen());
        app_tracker.send(new HitBuilders.AppViewBuilder().build());
		this.setContentView(R.layout.frag_invite_frnds);
		list = (ListView) findViewById(R.id.list);

		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int posi,
					long arg3) {
				Intent i;
				i = new Intent(getApplicationContext(),
						Activity_Artist_Details.class);
				i.putExtra(PARAMS.ARTIST_ID.get_param(), list_items_following
						.get(posi).get_id());

				startActivity(i);

			}

		});


    }

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		AppEventsLogger.deactivateApp(this,getResources().getString(R.string.fb_app_id));
		
		this.getApplicationContext().unregisterReceiver(NewMessageReceiver);
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;

    }

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
		AppEventsLogger.activateApp(this,getResources().getString(R.string.fb_app_id));
		
		getApplicationContext().registerReceiver(NewMessageReceiver,
				new IntentFilter(C.BROADCAST_MSG));
		new Update().execute();

	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId())
		{
	
		case android.R.id.home:
			super.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
    }
	private final BroadcastReceiver NewMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent i) {
			if (i.hasExtra(PARAMS.ARTIST.get_param())) {
				new Update().execute();
			}
		}
	};

	public class Update extends AsyncTask<String, Long, Boolean> {

		@Override
		protected Boolean doInBackground(String... params) {
			list_items_following.clear();
			for (Artist a : C.artists.values()) {
				if (a.is_following()) {
					list_items_following.add(a);
				}
			}

			return null;
		}

		@Override
		protected void onPostExecute(Boolean result) {

			following_adapter = new Following_Adapter(getApplicationContext(),
					list_items_following, imageLoader, options);
			list.setAdapter(following_adapter);

		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
		}
	}
}
