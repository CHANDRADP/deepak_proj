package com.dpc.wtp.tasks;

import java.io.IOException;

import twitter4j.StatusUpdate;
import twitter4j.TwitterException;
import twitter4j.auth.AccessToken;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.TwitterUtil;
import com.dpc.wtp.activities.App;
import com.dpc.wtp.customs.MyActivity;

public class Twitter_Share extends AsyncTask<String, String, Boolean> {
	private Context c;
	private String name,title,details,url,date,venue;
    private boolean app_sharing = false;

    public Twitter_Share(Context c, String name, String title,String date,String venue, String details, String url,boolean app_sharing) {
        this.c = c;
        this.name = name;
        this.title = title;
        this.date = date;
        this.details = details;
        this.venue = venue;
        this.url = url;
        this.app_sharing = app_sharing;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        C.alert(c, "Sharing on twitter...");
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (result)
        {
            C.alert(c, "Successfully shared on twitter.");
            App.update_userpoints(String.valueOf(App.get_userpoints() + 50));

        }
        else
        	C.alert(c, "Failed to share on twitter.");
    }

    @Override
    protected Boolean doInBackground(String... params) {
        try {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
            String accessTokenString = sharedPreferences.getString(C.PREFERENCE_TWITTER_OAUTH_TOKEN, "");
            String accessTokenSecret = sharedPreferences.getString(C.PREFERENCE_TWITTER_OAUTH_TOKEN_SECRET, "");

            if (accessTokenString != null && accessTokenSecret != null) {
                AccessToken accessToken = new AccessToken(accessTokenString, accessTokenSecret);
                String name = this.name;
                if(!app_sharing) {
                    name = name + " (" + date + " )";

                }
                name += name+"\n"+
                        this.url+"\n";
                        //     "Venue : "+venue+"\n"+
                        //      this.details+" \n"+
                        String temp = name + this.title;
                if(temp.length() <= 140)
                {
                    name += this.title;
                }
                 //       this.url;
                StatusUpdate status = new StatusUpdate(name);
              //  if(app_sharing)
                try {
					status.setMedia("logo.png", c.getAssets().open("logo.png"));

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}//.setMedia(new File(c.getCacheDir()+ "/logo.png"));

                 twitter4j.Status result = TwitterUtil.getInstance().getTwitterFactory().getInstance(accessToken)
                		.updateStatus(status);
                Log.d("status", String.valueOf(result.getId()));
                new Update_points(c).execute();
                return true;
            }

        } catch (TwitterException e) {
            e.printStackTrace();  
        }
        return false; 

    }
}