/**
* @author D-P-C
* Jul 17, 2014 2014

*/
package com.dpc.wtp.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;



/**
 * @author D-P-C
 *
 */
public class BootReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
         if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED))
         {
        	
        		context.startService(new Intent(context,WTP_Service.class));
        }
    }

}
