package com.dpc.wtp.customs;

import java.util.Arrays;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.actionbarsherlock.app.SherlockFragment;
import com.dpc.wtp.R;
import com.dpc.wtp.interfaces.Updatable;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.activities.App;
import com.dpc.wtp.tasks.Update_FB_TWIT;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class MyFragment extends SherlockFragment implements Updatable{
	
	private final String TAG = "MyFragment";

    protected Tracker app_tracker = null;

	protected static final int TAB_VIEW_PADDING_DIPS = 5;
	
	protected LinearLayout sub_tabs;
	
	protected DatabaseHelper db;
	
	protected Net_Detect net;
	
	protected   ImageLoader imageLoader = ImageLoader.getInstance();
	protected  DisplayImageOptions options = new DisplayImageOptions.Builder()
	.showStubImage(R.drawable.user)
	.showImageForEmptyUri(R.drawable.user)
	.showImageOnFail(R.drawable.user)
	.cacheInMemory(true)
	.cacheOnDisc(true)
    .displayer(new FadeInBitmapDisplayer(5))
	.bitmapConfig(Bitmap.Config.RGB_565)
	.build();
	
	protected static final List<String> PERMISSIONS = Arrays
			.asList("publish_actions");
	protected static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
	protected boolean pendingPublishReauthorization = false;
	Session mCurrentSession;
	protected UiLifecycleHelper uiHelper;
	
    
    @Override
	public void onCreate(Bundle savedInstanceState) {
        app_tracker = ((App) getSherlockActivity().getApplication()).getTracker(
                App.WTP_Tracker.WTP_TRACKER);
		// TODO Auto-generated method stub
    	if(!imageLoader.isInited())
    	{
    		App.initImageLoader(getSherlockActivity());
    		imageLoader = ImageLoader.getInstance();
    	}
		
		super.onCreate(savedInstanceState);
		net = new Net_Detect(this.getSherlockActivity().getApplicationContext());
        db = new DatabaseHelper(this.getSherlockActivity().getApplicationContext());
		
	}
	@Override
	public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        

        
    }
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		
	}
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
	}
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        
        if(Session.getActiveSession() != null)
        if (Session.getActiveSession().isOpened()) {
          Request.newMeRequest(Session.getActiveSession(), new Request.GraphUserCallback() {

                // callback after Graph API response with user object
                @Override
                public void onCompleted(GraphUser user, Response response) {
                    Log.w("myConsultant", user.getId() + " " + user.getName() + " " + user.getInnerJSONObject());
               if(user != null)
               {
            	   Log.d("details", user.toString());
            	   App.set_fbid(user.getId());
               		new Update_FB_TWIT(true).execute();
               }
                }
              }).executeAsync();
          }
    }
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}
	
	
	
	
	/*	try {
    PackageInfo info = getPackageManager().getPackageInfo(
            "com.dpc.wtp", 
            PackageManager.GET_SIGNATURES);
    for (Signature signature : info.signatures) {
        MessageDigest md = MessageDigest.getInstance("SHA");
        md.update(signature.toByteArray());
        Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
        }
} catch (NameNotFoundException e) {

} catch (NoSuchAlgorithmException e) {

}
*/
	
/*	private void signInWithFacebook() {

		SessionTracker  mSessionTracker = new SessionTracker(this.getSherlockActivity().getApplicationContext(), new StatusCallback() {

			@Override
			public void call(Session session, SessionState state,
					Exception exception) {
				// TODO Auto-generated method stub
				
			}

	        
	    }, null, false);

	    String applicationId = Utility.getMetadataApplicationId(this.getSherlockActivity().getApplicationContext());
	     mCurrentSession = mSessionTracker.getSession();

	    if (mCurrentSession == null || mCurrentSession.getState().isClosed()) {
	        mSessionTracker.setSession(null);
	        Session session = new Session.Builder(this.getSherlockActivity().getApplicationContext()).setApplicationId(applicationId).build();
	        Session.setActiveSession(session);
	        mCurrentSession = session;
	    }

	    if (!mCurrentSession.isOpened()) {
	        Session.OpenRequest openRequest = null;
	        openRequest = new Session.OpenRequest(this);

	        if (openRequest != null) {
	            openRequest.setDefaultAudience(SessionDefaultAudience.FRIENDS);
	            openRequest.setPermissions(Arrays.asList("public_profile","user_friends"));
	           // openRequest.setLoginBehavior(SessionLoginBehavior.SSO_WITH_FALLBACK);

	            mCurrentSession.openForRead(openRequest);
	        }
	    }else {
	        Request.executeMeRequestAsync(mCurrentSession, new Request.GraphUserCallback() {
	              @Override
	              public void onCompleted(GraphUser user, Response response) {
	                  Log.w("myConsultant", user.getId() + " " + user.getName() + " " + user.getInnerJSONObject());
	              }
	            });
	    }
	}
	*/
	@Override
	public void update(Context c) {
		// TODO Auto-generated method stub
		
	}
	
	@SuppressLint("NewApi")
	protected Text createDefaultTabView() {
        Text textView = (Text) this.getSherlockActivity().getLayoutInflater().inflate(R.layout.sub_tab_item, null).findViewById(R.id.txt);// new Text(this.getSherlockActivity());
        //textView.setAllCaps(true);
        int padding = (int) (TAB_VIEW_PADDING_DIPS * getResources().getDisplayMetrics().density);
        LayoutParams param = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        param.setMargins(padding, 0, padding, 0);
        textView.setLayoutParams(param);
   /*     textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, this.getResources().getDimension(R.dimen.eight_sp));
       
        // textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setTextColor(this.getResources().getColor(R.color.white));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // If we're running on Honeycomb or newer, then we can use the Theme's
            // selectableItemBackground to ensure that the View has a pressed state
            TypedValue outValue = new TypedValue();
            this.getSherlockActivity().getTheme().resolveAttribute(android.R.attr.selectableItemBackground,
                    outValue, true);
           // textView.setBackgroundResource(outValue.resourceId);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            // If we're running on ICS or newer, enable all-caps to match the Action Bar tab style
            textView.setAllCaps(true);
        }

        int padding = (int) (TAB_VIEW_PADDING_DIPS * getResources().getDisplayMetrics().density);
        textView.setPadding(padding, padding, padding, padding);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        param.setMargins(padding, 0, padding, 0);
        textView.setLayoutParams(param);
        textView.setBackgroundResource(R.drawable.round_bg);
        
      //  int padding = (int) (TAB_VIEW_PADDING_DIPS * getResources().getDisplayMetrics().density);
      //  LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
      //  param.setMargins(padding, 0, padding, 0);
      //  textView.setLayoutParams(param);
       
       */
        return textView;
    }
	
	
}
