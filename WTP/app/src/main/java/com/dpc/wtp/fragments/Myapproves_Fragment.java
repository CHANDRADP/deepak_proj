package com.dpc.wtp.fragments;


import java.util.ArrayList;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.dpc.wtp.R;
import com.dpc.wtp.Models.Deals;
import com.dpc.wtp.Models.Guestlist;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.adapters.Guestlist_Adapter;
import com.dpc.wtp.customs.MyFragment;
import com.dpc.wtp.customs.SlidingTabLayout;
import com.facebook.AppEventsLogger;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

public class Myapproves_Fragment extends MyFragment {

	private static final String[] tabs = new String[]{"My guest list"," My deals"};
	private SlidingTabLayout mSlidingTabLayout;
	private ViewPager mViewPager;
    private SamplePagerAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        return inflater.inflate(R.layout.frag_home, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        adapter = new SamplePagerAdapter();
        mViewPager.setAdapter(adapter);
        mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setCustomTabView(R.layout.tab_item, R.id.txt);
        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setSelectedIndicatorColors(new int[]{getResources().getColor(R.color.red)});
        mSlidingTabLayout.setTabTextsize((int)this.getResources().getDimension(R.dimen.eighteen_sp));
        
    }
    
    @Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		AppEventsLogger.deactivateApp(this.getSherlockActivity(),getResources().getString(R.string.fb_app_id));
		
		getSherlockActivity().unregisterReceiver(NewMessageReceiver);
	}



	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		AppEventsLogger.activateApp(this.getSherlockActivity(),getResources().getString(R.string.fb_app_id));
		
		getSherlockActivity().registerReceiver(NewMessageReceiver, new IntentFilter(
				C.BROADCAST_MSG));
		
	}
	
	private final BroadcastReceiver NewMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent i) 
		{
			adapter.notifyDataSetChanged();
			
		}
	};
    class SamplePagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return tabs.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {
            return o == view;
        }

        @Override
        public CharSequence getPageTitle(int position) {
        	
            return tabs[position];
        }
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = getActivity().getLayoutInflater().inflate(R.layout.ac_settings,
                    container, false);
            container.addView(view);

            final ListView list = (ListView) view.findViewById(R.id.list);
            if(position == 0)
            {
            	DatabaseHelper h = new DatabaseHelper(getSherlockActivity().getApplicationContext());
	            	final ArrayList<Guestlist> list_items = h.get_user_guestlist();
	                final  Guestlist_Adapter list_adapter = new Guestlist_Adapter(getSherlockActivity().getApplicationContext(),list_items,null,false);
	        		list.setAdapter(list_adapter);

        		
            }
            else if(position == 1)
            {
            	
            	final ArrayList<Deals> list_items = new DatabaseHelper(getSherlockActivity().getApplicationContext()).get_user_deals();
            		final  Guestlist_Adapter list_adapter = new Guestlist_Adapter(getSherlockActivity().getApplicationContext(),null,list_items,true);
        			list.setAdapter(list_adapter);

            }
            
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
           
        }

    }
}
