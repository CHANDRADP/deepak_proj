package com.dpc.wtp.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnActionExpandListener;

import com.dpc.wtp.R;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Models.Slide_Menu_Item;
import com.dpc.wtp.Utils.AppRater;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.adapters.Slidingdrawer_Adapter;

import com.dpc.wtp.adapters.SuggestionsAdapter;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.customs.MyGesture;
import com.dpc.wtp.customs.MySearchView;
import com.dpc.wtp.fragments.Home_Fragment;
import com.dpc.wtp.interfaces.OnDetectScrollListener;
import com.dpc.wtp.services.WTP_Service;
import com.dpc.wtp.tasks.Register_Task;


public class Activity_Home extends MyActivity  implements OnDetectScrollListener{




private String[] types = new String[]{"","Location","Area","Venue","Artist","Genre"};

    private GestureDetector gestureDetector;
	
	private DrawerLayout slide_menu_layout;
	
	private LinearLayout list_bg;
	
	private ListView slide_listview,suggestion_list;
	
	private ActionBarDrawerToggle slide_menu_listener;

	private  Text title;
	
	private CharSequence actionbar_title = "Home";

	private String[] slide_menu_titles;
	
	private ArrayList<Slide_Menu_Item> slide_menu_items;
	
	private Slidingdrawer_Adapter slide_menu_adapter;
	
	private SuggestionsAdapter mSuggestionsAdapter;
	
	public static HashMap<String,String> searched_values = new HashMap<String,String>();
	
	
	private boolean slide_open = false;
	
	private int search_type = 1;
	
	public static int height; 
	
	private int[] icons = new int[]{
			R.drawable.favorite_n,
			R.drawable.menu_calendar,
			R.drawable.menu_location,
			R.drawable.menu_favorite,
			R.drawable.menu_cab,
			R.drawable.menu_city,
			R.drawable.menu_approves,
			R.drawable.menu_setting,
			};

	
    
	private MenuItem search_item;
	private MySearchView searchView;
	private String[] COLUMNS = new String[] { "_id", "text", "type"};
	private MatrixCursor cursor = new MatrixCursor(COLUMNS);
	
	@Override
    public boolean dispatchTouchEvent(MotionEvent me){
		gestureDetector.onTouchEvent(me);
         return super.dispatchTouchEvent(me);
    }
	
    @Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if(!App.is_closed())
		{
			if(imageLoader.isInited())
			{
				//imageLoader.clearDiscCache();
				//imageLoader.clearMemoryCache();
			}
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
		if(App.is_closed())
		{
			if(imageLoader.isInited())
			{
				imageLoader.clearDiscCache();
				imageLoader.clearMemoryCache();
				this.imageLoader.destroy();
			}
			
			startActivity(new Intent(this,Activity_Login.class));
			finish();
		}
	}
	

	@Override
    protected void onCreate(Bundle savedInstanceState) {
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.actionbar);
		title = (Text)findViewById(android.R.id.title);
		setTitle(actionbar_title);
		///////////////////////////////////////////////////////

        int width = 0;
        if (android.os.Build.VERSION.SDK_INT >= 13) {
            width = get_width_for_higher_versions();
        } else if (android.os.Build.VERSION.SDK_INT < 13) {
            width = get_width_for_lower_versions();
        }
		Home_Fragment.tab_position = 0;
        AppRater.app_launched(this) ;
		////////////////////////////////////////////////////////
		if(!App.is_service_running())
        {
        	this.startService(new Intent(this,WTP_Service.class));
        }

        super.onCreate(savedInstanceState);
        if(!App.is_shared_onfb())
        {
            super.publishStory(getResources().getString(R.string.app_name),C.app_caption,C.app_info,C.app_url,C.app_logo,false,false);
        }
        setContentView(R.layout.ac_new_home);

        gestureDetector = new GestureDetector(getApplicationContext(), new MyGesture(this));
        
        slide_menu_titles = getResources().getStringArray(R.array.slide_menu_titles);
        slide_menu_layout = (DrawerLayout) findViewById(R.id.drawer_layout);
		slide_listview = (ListView) findViewById(R.id.list_slidermenu);
		suggestion_list = (ListView)findViewById(R.id.list);
		list_bg = (LinearLayout)findViewById(R.id.search_list_bg);
		slide_listview.setMinimumWidth(width);//.setLayoutParams(new DrawerLayout.LayoutParams(width, DrawerLayout.LayoutParams.MATCH_PARENT));
		slide_menu_items = new ArrayList<Slide_Menu_Item>();
		
		for(int i = 0;i < slide_menu_titles.length;i++)
		{
			slide_menu_items.add(new Slide_Menu_Item(slide_menu_titles[i], icons[i]));

		}
		slide_listview.setOnItemClickListener(new SlideMenuClickListener());
		slide_menu_adapter = new Slidingdrawer_Adapter(getApplicationContext(), slide_menu_items,imageLoader);
		slide_listview.setAdapter(slide_menu_adapter);
		slide_listview.setSelection(0);

		slide_menu_listener = new ActionBarDrawerToggle(this, slide_menu_layout,R.drawable.slider,R.string.app_name,R.string.app_name)
		{

			public void onDrawerClosed(View view) {
				slide_open = false;
				setTitle(actionbar_title);
				invalidateOptionsMenu();
				//////////////////////////////////////
				slide_menu_adapter.reset_profile_points();
				///////////////////////////////////////
			}


			public void onDrawerOpened(View drawerView) {
				//////////////////////////////////////
				slide_menu_adapter.update_profile_points();
				///////////////////////////////////////
				slide_open = true;
				actionbar_title = title.getText();
				setTitle(getResources().getString(R.string.app_name));
				InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				if(searchView != null)
				inputMethodManager.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
				
				invalidateOptionsMenu();
			}
		};
		
		slide_menu_layout.setDrawerListener(slide_menu_listener);

        ///////////////////////////////////////////
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Home_Fragment fragment = new Home_Fragment();
        transaction.replace(R.id.sample_content_fragment, fragment);
        transaction.commit();
        
        suggestion_list.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				searched_values.clear();
				Cursor c = (Cursor) mSuggestionsAdapter.getItem(position);
				int id = c.getInt(c.getColumnIndex("_id"));
				String query = c.getString(c.getColumnIndex("text"));
				Log.d("query", query);
				if(id < 0)
				{
					search_type = position;
				//	set_search_adapter(searchView.getQuery().toString().toLowerCase());
				}
				else
				{
			        
			      //  searchView.setQuery(query, false);
			        if(id == 1)
			        {

                        query = C.CITY.valueOf(query).get_city();
			        	for(Event e : C.items.values())
			            {
			            	if(C.clubs.get(e.get_venue_id()).get_event_city().equalsIgnoreCase(query))// && search_type == 0)
			            	{
			            		searched_values.put(e.get_event_id(), e.get_event_id());//e.get_event_id());
			            		
			            	}
			            }
			        }
			        else if(id == 2)
			        {
			        	for(Event e : C.items.values())
			        	if(C.clubs.get(e.get_venue_id()).get_venue_area().equalsIgnoreCase(query))// && search_type == 1)
		            	{
			        		searched_values.put(e.get_event_id(), e.get_event_id());//e.get_event_id());
		            		
		            	}
			        }
			        else if(id == 3)
			        {
			        	for(Event e : C.items.values())
			        	if(C.clubs.get(e.get_venue_id()).get_venue_name().equalsIgnoreCase(query))// && search_type == 2)
		            	{
			        		searched_values.put(e.get_event_id(), e.get_event_id());//e.get_event_id());
		            		
		            	}
			        }
			        else if(id == 4)
			        {
			        	for(Event e : C.items.values())
                        for(int i = 0;i < e.getLineups().size();i++)
			        	if(C.artists.get(e.getLineups().get(i).getArtistid()).get_name().equalsIgnoreCase(query))// && search_type == 3)
		            	{
			        		Log.d("added", "added");
			        		searched_values.put(e.get_event_id(), e.get_event_id());//e.get_event_id());
		            		break;
		            	}
			        }
			        else if(id == 5)
			        {
			        	for(Event e : C.items.values())
                            for(int i = 0;i < e.getLineups().size();i++)
                                if(C.artists.get(e.getLineups().get(i).getArtistid()).get_genre_type().equalsIgnoreCase(query))// && search_type == 3)
                                {
			        		searched_values.put(e.get_event_id(), e.get_event_id());//e.get_event_id());
                                    break;
		            		
		            	}
			        }
			        Intent i = new Intent(getApplicationContext(),Activity_Event_List.class);
			        i.putExtra(PARAMS.EVENT_ID.get_param(), c.getString(c.getColumnIndex("_id")));
			        startActivity(i);
                    app_tracker.send(new C().generate_analytics(C.CATEGORY.SEARCH.get_category(),C.ACTION.ACTION_OK.get_action(),types[id]));

                }
			}
        	
        });

        if(!App.is_gcm_updated() && App.get_gcmid() != null)
        {
            new Register_Task(this,App.get_gcmid()).execute();
        }


    }

    private  int get_width_for_lower_versions()
    {
        DisplayMetrics display = new DisplayMetrics();
       getWindowManager().getDefaultDisplay().getMetrics(display);
        return display.widthPixels;
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private  int get_width_for_higher_versions()
    {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        width = (width/10)*8;
        return width;
    }

    
	private TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                int count) {
        	if(s.length() > 0)
        	set_search_adapter(s.toString().toLowerCase());
        	
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                int after) {
            // TODO Auto-generated method stub

        }

        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub

        }
    };
    
	@Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        if(!slide_open)
        {
		getSupportMenuInflater().inflate(R.menu.home_frag_menu, menu);
		
		final View v = (View) menu.findItem(
                R.id.search).getActionView();
		
        final EditText et = ((EditText) v.findViewById(R.id.search_txt));
        et.addTextChangedListener(textWatcher);
        MenuItem menuItem = menu.findItem(R.id.search);
        
        menuItem.setOnActionExpandListener(new OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
            	list_bg.setVisibility(View.GONE);
                // Do something when collapsed
            	InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(et.getWindowToken(), 0);
                return true; // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
            	suggestion_list.setAdapter(null);
            	list_bg.setVisibility(View.VISIBLE);
            	et.setText("");
            	et.requestFocus();
            	((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
            	return true; // Return true to expand action view
            }
        });

    	}

        return super.onCreateOptionsMenu(menu);
    }
	
	
	private void set_search_adapter(String txt)
	{
		HashMap<String,String> values = new HashMap<String,String>();
		
		searched_values.clear();
		if (search_type >= 0) 
		{
			cursor = new MatrixCursor(COLUMNS);
            for(Event e : C.items.values())
            {

            	if(C.clubs.get(e.get_venue_id()).get_venue_address().toLowerCase().contains(txt))// && search_type == 0)
            	{
            		values.put(C.clubs.get(e.get_venue_id()).get_event_city(), "1");//e.get_event_id());
            		
            	}
            	if(C.clubs.get(e.get_venue_id()).get_venue_area().toLowerCase().contains(txt))// && search_type == 1)
            	{
            		values.put(C.clubs.get(e.get_venue_id()).get_venue_area(), "2");//e.get_event_id());
            		
            	}
            	else if(C.clubs.get(e.get_venue_id()).get_venue_name().toLowerCase().contains(txt))// && search_type == 2)
            	{
            		values.put(C.clubs.get(e.get_venue_id()).get_venue_name(), "3");//e.get_event_id());
            		
            	}
            	else if(e.getLineups() != null)// && search_type == 3)
            	{
                    for(int i = 0;i < e.getLineups().size();i++) {
                        if(e.getLineups().get(i).getName().toLowerCase().contains(txt))
                        values.put(e.getLineups().get(i).getName(), "4");//e.get_event_id());
                        if(C.artists.get(e.getLineups().get(i).getArtistid()).get_genre_type().toLowerCase().contains(txt))// && search_type == 4)
                        {
                            values.put(C.artists.get(e.getLineups().get(i).getArtistid()).get_genre_type(), "5");//e.get_event_id());

                        }
                    }
            		
            	}

            }
            Iterator<Entry<String, String>> it = values.entrySet().iterator();
            while (it.hasNext()) 
            {
                Entry<String, String> pairs = (Entry<String, String>)it.next();
                String key = pairs.getKey();
                if(pairs.getValue().equals("1"))
                    key = Activity_City.cities[Integer.parseInt(pairs.getKey())-1];
                cursor.addRow(new String[]{pairs.getValue(),key ,"3"});
                Log.d("added", pairs.getValue());
                it.remove(); 
            }
            mSuggestionsAdapter = new SuggestionsAdapter(getSupportActionBar().getThemedContext(), cursor);
            mSuggestionsAdapter.notifyDataSetChanged();
        }
		else
		{
			cursor = new MatrixCursor(COLUMNS);
			cursor.addRow(new String[]{"-1","Location"," "});
			cursor.addRow(new String[]{"-1","Area"," "});
			cursor.addRow(new String[]{"-1","Venue"," "});
			cursor.addRow(new String[]{"-1", "Artist", " "});
			cursor.addRow(new String[]{"-1","Genre"," "});
			mSuggestionsAdapter = new SuggestionsAdapter(getSupportActionBar().getThemedContext(), cursor);
		}
		suggestion_list.setAdapter(mSuggestionsAdapter);
     //   searchView.setSuggestionsAdapter(mSuggestionsAdapter);
	}
	
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(this.slide_open)
		{
			slide_menu_layout.closeDrawer(slide_listview);
		}
		else
		super.onBackPressed();
	}

    @Override
    protected void onPause() {
        super.onPause();
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;

    }

    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId())
		{
	/*	case R.id.search:
			Search_Popup popup = new Search_Popup(this);
			popup.button_listener = new SearchClickListener(){

				@Override
				public void setOnSearchClickListener(String[] details) 
				{
					// TODO Auto-generated method stub
					
				}};
				popup.show(getSupportFragmentManager(), "");
			break;
			
		case R.id.user_location:
			GPS_Dialog gps = new GPS_Dialog("Help us to find your Location");
			gps.twobbutton_listener = this;
			gps.show(getSupportFragmentManager(), "");
			
			
			break;
			*/

        case R.id.chat:

            Intent intent = new Intent(Activity_Home.this, Activity_ChatUsers.class);
            startActivity(intent);
            break;

		case android.R.id.home:
			toggle_drawer();
			
			break;
		}
		return super.onOptionsItemSelected(item);
    }
    
    
    private class SlideMenuClickListener implements
			OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			open_fragment(position);
		}
	}

	
	private void open_fragment(int position) 
	{
		Intent i = null;
		switch (position) 
		{
		
		case 0:
			i = new Intent(getApplicationContext(),Activity_Profile.class);
            app_tracker.send(new C().generate_analytics(C.CATEGORY.SLIDE_MENU.get_category(),C.ACTION.ACTION_OK.get_action(),C.SCREEN.SCREEN_PROFILE.get_screen()));

            break;
		case 1:
			i = new Intent(getApplicationContext(),Activity_Calendar.class);
            app_tracker.send(new C().generate_analytics(C.CATEGORY.SLIDE_MENU.get_category(),C.ACTION.ACTION_OK.get_action(),C.SCREEN.SCREEN_CALENDAR.get_screen()));

            break;
		case 2:
			i = new Intent(getApplicationContext(),Activity_Places_Nearby.class);
            app_tracker.send(new C().generate_analytics(C.CATEGORY.SLIDE_MENU.get_category(),C.ACTION.ACTION_OK.get_action(),C.SCREEN.SCREEN_NEARBY_PLACES.get_screen()));

            break;
		case 3:
			i = new Intent(getApplicationContext(),Activity_Favorites.class);
            app_tracker.send(new C().generate_analytics(C.CATEGORY.SLIDE_MENU.get_category(),C.ACTION.ACTION_OK.get_action(),C.SCREEN.SCREEN_FAVORITES.get_screen()));


            break;
		case 4:
			i = new Intent(getApplicationContext(),Activity_Cabs.class);
            app_tracker.send(new C().generate_analytics(C.CATEGORY.SLIDE_MENU.get_category(),C.ACTION.ACTION_OK.get_action(),C.SCREEN.SCREEN_CAB.get_screen()));


            break;
		case 5:
			i = new Intent(getApplicationContext(),Activity_City.class);
            app_tracker.send(new C().generate_analytics(C.CATEGORY.SLIDE_MENU.get_category(),C.ACTION.ACTION_OK.get_action(),C.SCREEN.SCREEN_CITY.get_screen()));

            break;
		case 6:
			i = new Intent(getApplicationContext(),Activity_Myapproves.class);
            app_tracker.send(new C().generate_analytics(C.CATEGORY.SLIDE_MENU.get_category(),C.ACTION.ACTION_OK.get_action(),C.SCREEN.SCREEN_MY_APPROVALS.get_screen()));

            break;
		case 7:
			i = new Intent(getApplicationContext(),Activity_Settings.class);
            app_tracker.send(new C().generate_analytics(C.CATEGORY.SLIDE_MENU.get_category(),C.ACTION.ACTION_OK.get_action(),C.SCREEN.SCREEN_SETTINGS.get_screen()));

            break;

		default:
			//fragment = new Tabs_Fragment();
			break;
		}
		
		if(i != null)
		{
			startActivity(i);
		}
		
		slide_menu_layout.closeDrawer(slide_listview);
		
	}
	@Override
	public void setTitle(CharSequence title1) {
		title.setText(title1);
	}

	public void settitle()
	{
		title.setText(actionbar_title);
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		slide_menu_listener.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		slide_menu_listener.onConfigurationChanged(newConfig);
	}
    ///////////////////////////////////////////////////////////////////////
	@Override
	public void onUpScrolling() 
	{
		/*if (New_Home_Fragment.mSlidingTabLayout.getVisibility() == View.VISIBLE) 
		{
			MyAnimation a = new MyAnimation(
					New_Home_Fragment.mSlidingTabLayout, 100,
					MyAnimation.COLLAPSE);
			height = a.getHeight();
			New_Home_Fragment.mSlidingTabLayout.startAnimation(a);
		}*/
		
	}

	@Override
	public void onDownScrolling() 
	{
	/*	if (New_Home_Fragment.mSlidingTabLayout.getVisibility() == View.GONE) 
		{
			MyAnimation a = new MyAnimation(
					New_Home_Fragment.mSlidingTabLayout, 100, MyAnimation.EXPAND);
			a.setHeight(height);
			New_Home_Fragment.mSlidingTabLayout.startAnimation(a);
		}
		*/
		
	}

	@Override
	public void onLeftScrolling() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onRightScrolling() {
		// TODO Auto-generated method stub
		if(Home_Fragment.tab_position == 0)
		{
			toggle_drawer();
		}
	}

	private void toggle_drawer()
	{
		boolean drawerOpen = slide_menu_layout.isDrawerOpen(slide_listview);
		if(!drawerOpen)
		slide_menu_layout.openDrawer(slide_listview);
		else
			slide_menu_layout.closeDrawer(slide_listview);
	}

}