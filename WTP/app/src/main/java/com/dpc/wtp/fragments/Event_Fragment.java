package com.dpc.wtp.fragments;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.widget.SearchView;
import com.dpc.wtp.JSONParser;
import com.dpc.wtp.R;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Models.Server_Params;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.CirclePageIndicator;
import com.dpc.wtp.activities.Activity_Calendar;
import com.dpc.wtp.activities.Activity_Event_Details;

import com.dpc.wtp.activities.App;
import com.dpc.wtp.adapters.Events_Adapter;
import com.dpc.wtp.adapters.ImagePagerAdapter;
import com.dpc.wtp.customs.MyFragment;
import com.dpc.wtp.customs.MyList;
import com.google.android.gms.analytics.HitBuilders;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
public class Event_Fragment extends MyFragment implements SearchView.OnQueryTextListener,SwipeRefreshLayout.OnRefreshListener
{
	
	String TAG = "home";
	
	//int[] ad = new int[]{R.drawable.ad1,R.drawable.ad2,R.drawable.ad3,R.drawable.ad4};
	
	public  boolean running = true;
	
	private SwipeRefreshLayout refreshLayout;
	
	private ViewPager viewPager;
	
	private ImageView ads,progress;
	
	private ArrayList<String> images = new ArrayList<String>();
	
	private ArrayList<Event> list_items = new ArrayList<Event>();
	
	private Events_Adapter list_adapter;
	
	private MyList list;
	
	int tabheight = 0;
	
	private View v;
	
	boolean attached = false;
	
	private AnimationDrawable animation;
	
	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		attached = false;
	}
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setHasOptionsMenu(true);
        app_tracker.setScreenName(C.SCREEN.SCREEN_EVENTS.get_screen());
        app_tracker.send(new HitBuilders.AppViewBuilder().build());
        attached = true;
        options = new DisplayImageOptions.Builder()
    	.showStubImage(R.drawable.default_bg)
    	.showImageForEmptyUri(R.drawable.default_bg)
    	.showImageOnFail(R.drawable.default_bg)
    	.cacheInMemory(true)
    	//.cacheOnDisc(true)
    	.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
    	.bitmapConfig(Bitmap.Config.ARGB_8888)
    	.build();
    }
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		v = inflater.inflate(R.layout.frag_home_pages, container, false);
		v.findViewById(R.id.line).setVisibility(View.GONE);
		View top = inflater.inflate(R.layout.header_view_page, null);
		
        progress = (ImageView)v.findViewById(R.id.progress);
	
		viewPager = (ViewPager) top.findViewById(R.id.viewpager);
		images.add("aaaaa");
		images.add("aaaaa");
		images.add("aaaaa");
		images.add("aaaaa");
		ImagePagerAdapter adap = new ImagePagerAdapter(getSherlockActivity(),imageLoader,options);
        viewPager.setAdapter(adap);
        final CirclePageIndicator mIndicator = (CirclePageIndicator) top.findViewById(R.id.indicator);
		mIndicator.setViewPager(viewPager);
		if(images.size() > 1)
		start_sliding();
		
		
		list = (MyList)v.findViewById(R.id.list);
		list.addHeaderView(top);
		
	
		/////////////////////////
	    update_list();
	    list.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int posi,
					long arg3) 
			{
				if(posi != 0)
				{
					Intent i = new Intent(getSherlockActivity().getApplicationContext(),Activity_Event_Details.class);
					i.putExtra(PARAMS.EVENT_ID.get_param(), list_items.get(posi-1).get_event_id());
					startActivity(i);
				}
			}
			
		});
	    
	    refreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
		refreshLayout.setOnRefreshListener(this);
	    setAppearance();
	    enableSwipe();
	    refreshLayout.setEnabled(false);
		update(getSherlockActivity());
		
		return v;
	}

	private void update_list()
	{
		
		if(getSherlockActivity() != null)
		{
			list_items.clear();
			for(Event e : C.items.values())
			{
				//if(App.get_city().equalsIgnoreCase(e.get_venue().get_event_city()))
				list_items.add(e);
				
			}
			list_adapter = new Events_Adapter(getSherlockActivity().getApplicationContext(),list_items,imageLoader,options,app_tracker);
			list.setAdapter(list_adapter);
			
		
		}
		
	}
	


	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		update_list();
		getSherlockActivity().registerReceiver(NewMessageReceiver, new IntentFilter(
				C.BROADCAST_MSG));
		
	}
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onResume();
		getSherlockActivity().unregisterReceiver(NewMessageReceiver);
		
	}
	@Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater) {
		//ActionBar actionBar = getSherlockActivity().getSupportActionBar();
		
        super.onCreateOptionsMenu(menu, inflater);
    }
	@Override
	public boolean onQueryTextSubmit(String query) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean onQueryTextChange(String newText) {
		// TODO Auto-generated method stub
		return false;
	}
	
	int current_item = 0;
    private Handler handler;
    private Runnable image_slider;
    @Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if(images.size() > 1)
		{
			if(handler != null)
			handler.removeCallbacks(image_slider);
			
		}
	}
	private void start_sliding()
    {
    	handler = new Handler();
    	image_slider= new Runnable() {
            @Override
            public void run() {
                if(images.size() > 1)
                {
                	slide_image();
                    handler.postDelayed(image_slider, 2000);
                   
                }
            }
        };
    	handler.post(image_slider);
    }

    private void slide_image() 
	{
		if(current_item < images.size())
		{
			 viewPager.setCurrentItem(current_item, true);
			// ads.setBackgroundResource(ad[current_item]);
			 current_item++;
			 if(current_item >= images.size())
			 {
				 current_item = 0;
			 }
		}
	}   
    
    /////////////////////////////////////////
     public class Get_Events extends AsyncTask<ArrayList<NameValuePair>, Long, JSONObject> {
			private Context c;
			public Get_Events(Context c)
			{
				this.c = c;
			}
			@Override
			protected void onPreExecute() {
				if(C.items.size() == 0)
				{
					progress.setBackgroundResource(R.drawable.progress);
					progress.setVisibility(View.VISIBLE);
					animation = (AnimationDrawable) progress.getBackground();
					animation.start();
				}

			}

			@Override
			protected JSONObject doInBackground(ArrayList<NameValuePair> ... params) {
				return get_events(params[0]);
			}

			

			@Override
			protected void onPostExecute(JSONObject obj) {
				running = true;
				if(animation != null)
				{
					progress.setVisibility(View.GONE);
					animation.stop();
				}
				hideSwipeProgress();
				get_event_items_from_json(obj,c);
				
			}

			@Override
			protected void onCancelled() {
				super.onCancelled();
			}

		}
     
     public JSONObject get_events(ArrayList<NameValuePair> params)
		{
			JSONParser parser = new JSONParser();
			JSONObject user;
			user = parser.makeHttpRequest(C.SERVER_URL, "POST",params);
			
			return user;
		}
     public void get_event_items_from_json(JSONObject obj,Context c)
     {
    	 if(obj != null)
			{
				if(obj.has(PARAMS.STATUS.get_param()))
				{
					try {
						int status = obj.getInt(PARAMS.STATUS.get_param());
						if(status > 0)
						{
							if(c != null)
							{
								new C().get_event_items_from_json(obj,c);
								new Activity_Calendar().add_calendar_events();
								Intent intent = new Intent(C.BROADCAST_MSG);
								intent.putExtra(PARAMS.VENUE_DETAILS.get_param(), " ");
								intent.putExtra(PARAMS.LOCATION.get_param(), " ");
		 						c.sendBroadcast(intent);
		 						update_list();
							}
							
							
						}
						else
						{
							C.alert(c, "Please try again after some time");
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				else
				{
					C.alert(c, "Please try again after some time");
				}
			}
     }
	
	
	@SuppressWarnings("unchecked")
	public void update(Context c) {
		// TODO Auto-generated method stub
		super.update(c);
		
		if(running)
		{
			running = false;
			//new Get_Events().execute(new Server_Params().get_venue_params(App.get_userid()));
			new Get_Events(c).execute(new Server_Params().get_details_by_location(App.get_userid()));
			
			
		}
		
	}
	
	///////////////////////////////
	private final BroadcastReceiver NewMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent i) 
		{
			if(i.hasExtra(PARAMS.ATTEND.get_param()) || i.hasExtra(PARAMS.LOCATION.get_param()))
			{
				update_list();
				
			}
		}
	};
	///////////////////////////////////////


	@SuppressLint("InlinedApi")
	private void setAppearance() {
        refreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
        android.R.color.holo_green_light,
        android.R.color.holo_orange_light,
        android.R.color.holo_red_light);
        
    }
 
    /**
     * It shows the SwipeRefreshLayout progress
     */
    public void showSwipeProgress() {
    	if(refreshLayout != null)
        refreshLayout.setRefreshing(true);
    }
 
    /**
     * It shows the SwipeRefreshLayout progress
     */
    public void hideSwipeProgress() {
    	if(refreshLayout != null)
        refreshLayout.setRefreshing(false);
    }
 
    /**
     * Enables swipe gesture
     */
    public void enableSwipe() {
    	if(refreshLayout != null)
        refreshLayout.setEnabled(true);
    }
 
    /**
     * Disables swipe gesture. It prevents manual gestures but keeps the option tu show
     * refreshing programatically.
     */
    public void disableSwipe() {
    	if(refreshLayout != null)
        refreshLayout.setEnabled(false);
    }
 
    /**
     * It must be overriden by parent classes if manual swipe is enabled.
     */
    @Override public void onRefresh() {
        // Empty implementation
    	showSwipeProgress();
    	update(getSherlockActivity());
    }
	
////////////////////////////////////////////////////
   

}
