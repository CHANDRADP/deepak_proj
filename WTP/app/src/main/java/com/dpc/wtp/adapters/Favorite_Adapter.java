package com.dpc.wtp.adapters;


import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.dpc.wtp.R;
import com.dpc.wtp.Models.V_Details;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.tasks.Display_Image;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class Favorite_Adapter extends BaseAdapter {
	LayoutInflater inflater;
	ViewHolder holder;
	View v;
	List<V_Details> items = new ArrayList<V_Details>();
	Context c;
	ImageLoader loader;
	DisplayImageOptions options;
	DatabaseHelper db;
	private Net_Detect net;
	private class ViewHolder {
		Text name;
		ImageView user_pick,chack_box;
		
		
	}
	public Favorite_Adapter(Context c,List<V_Details> l,ImageLoader loader,DisplayImageOptions options)
	{
		this.c = c;
		this.items = l;
		this.loader = loader;
		this.options = options;
		db = new DatabaseHelper(c);
		net = new Net_Detect(this.c);
	}
	
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		V_Details club = items.get(position);
		
		v = convertView;
		if (v == null) 
		{
			 inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 v = inflater.inflate(R.layout.row_friends, null);
			 holder = new ViewHolder();
			 holder.name = (Text) v.findViewById(R.id.user_name);
			 holder.user_pick = (ImageView) v.findViewById(R.id.user_pick);
			 holder.chack_box = (ImageView) v.findViewById(R.id.check_box);
			 v.setTag(holder);
			 
		}
		else
			holder = (ViewHolder) v.getTag();
		
		
		holder.name.setText(club.get_venue_name());
		
		if(db.is_image_available(club.get_images().get(0).get_id()))
		{
			new Display_Image(c,club.get_images().get(0).get_id(),holder.user_pick).execute();
			
		}
		else
		{
			loader.displayImage(club.get_images().get(0).get_image(), holder.user_pick, options);
		}
		//set_favorite(holder.user_pick,!items.get(position).is_following());
		holder.chack_box.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//items.get(position).set_favorite(favorite)
				
			}
			
		});
		
		
		
		return v;

	}
	private void set_favorite(ImageView img,boolean following)
	{
		if(!following)
		{
			img.setBackgroundResource(R.drawable.favorite_n);
		}
		else
		{
			img.setBackgroundResource(R.drawable.favorite_p);
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}


	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
	}


	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

}