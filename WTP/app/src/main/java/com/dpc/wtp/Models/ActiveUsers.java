package com.dpc.wtp.Models;



/**
 * Created by user on 1/12/2015.
 */
public class ActiveUsers
{
    private String userName, lastMsg, lastTime;
    private int unreadCount;


    public ActiveUsers(String userName, String lastMsg, String lastTime, int unreadCount) {
        this.userName = userName;
        this.lastMsg = lastMsg;
        this.lastTime = lastTime;
        this.unreadCount = unreadCount;

    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLastMsg() {
        return lastMsg;
    }

    public void setLastMsg(String lastMsg) {
        this.lastMsg = lastMsg;
    }

    public String getLastTime() {
        return lastTime;
    }

    public void setLastTime(String lastTime) {
        this.lastTime = lastTime;
    }

    public int getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }
}
