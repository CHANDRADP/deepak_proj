package com.dpc.wtp.customs;

import java.util.Comparator;

import com.dpc.wtp.Models.Artist;

public class Alpha_Sorter implements Comparator<Artist> {
    public int compare(Artist c1, Artist c2) {
    	String left = null,right = null;
		left = c1.get_name();
		right = c2.get_name();
    	 
        return left.compareToIgnoreCase(right);
    }
}