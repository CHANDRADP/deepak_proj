package com.dpc.wtp.tasks;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.dpc.wtp.JSONParser;
import com.dpc.wtp.Models.Server_Params;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.activities.App;
import com.dpc.wtp.amazon.UploadService;
import com.dpc.wtp.customs.MyProgress;

public class Photo_Update_Task extends AsyncTask<String, Long, Boolean> {
	private MyProgress dialog;
	
	String image,thumbnail;
	Context c;

	public Photo_Update_Task(Context c,String image,String thumbnail) {
		this.c = c;
		this.image = image;
		this.thumbnail = thumbnail;
	//	dialog = new MyProgress(c);
	}

	@Override
	protected void onPreExecute() {
		//dialog.setMessage("Please wait...");
		//dialog.setCancelable(false);
		//dialog.show();

	}

	@Override
	protected Boolean doInBackground(String... params) {
		
		return update_pick();
	}

	@Override
	protected void onPostExecute(Boolean result) {
		//dialog.dismiss();
        Intent intent = new Intent(UploadService.UPLOAD_STATE_CHANGED_ACTION);
        Bundle b = new Bundle();
        b.putInt(UploadService.UPLOAD_STATE, 1);


		if (result) 
		{
			C.alert(c, "Photo uploaded successfully");
			b.putString(PARAMS.IMAGE.get_param(), "1");
		}
		else
		{
			C.alert(c, "Error in uploading photo");
			b.putString(PARAMS.IMAGE.get_param(), "-1");
		}
        intent.putExtras(b);
		c.sendBroadcast(intent);

	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
	}
	
	public boolean update_pick()
	{
		boolean success = false;
		JSONParser parser = new JSONParser();
		JSONObject user = null;
		try {
		if(App.get_image() == null)
		{
		user = parser.makeHttpRequest(C.SERVER_URL, "POST",
				new Server_Params().get_add_photo_params(
						App.get_userid(),
						image,
						thumbnail));
            if(user != null)
		if(user.has(PARAMS.STATUS.get_param()))
			if(user.getInt(PARAMS.STATUS.get_param()) > 0)
			{
				App.set_thumbnail(thumbnail);
				App.set_image(image);
				success = true;
			}
		}
		else
		{
			user = parser.makeHttpRequest(C.SERVER_URL, "POST",
					new Server_Params().get_update_photo_params(
							App.get_userid(),
							image,
							thumbnail));
			if(user.getInt(PARAMS.STATUS.get_param()) > 0)
			{
                App.set_image(image);
				App.set_thumbnail(thumbnail);
				success = true;
			}
		}
		
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return success;
	}

}