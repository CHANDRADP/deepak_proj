package com.dpc.wtp.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Models.Images;
import com.dpc.wtp.Models.V_Details;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.tasks.Display_Image;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

import java.io.ByteArrayOutputStream;
import java.util.List;

/**
 * Created by D-P-C on 10-Feb-15.
 */
public class SliderAdapter extends RecyclingPagerAdapter {

    private Context context;
    private int type = 1;
    private List<Event> events;
    private List<V_Details> venues;

    private boolean       isInfiniteLoop;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private DatabaseHelper db;

    public SliderAdapter(Context c,ImageLoader loader,DisplayImageOptions option, List<Event> items) {
        this.context = c;
        this.events = items;
        this.imageLoader = loader;
        this.options = option;
        isInfiniteLoop = false;
        db = new DatabaseHelper(c);
        type = 1;
    }

    public SliderAdapter(Context c,DisplayImageOptions option,ImageLoader loader, List<V_Details> items) {
        this.context = c;
        this.venues = items;
        this.imageLoader = loader;
        this.options = option;
        isInfiniteLoop = false;
        db = new DatabaseHelper(c);
        type = 2;
    }

    @Override
    public int getCount() {
        // Infinite loop
        if (type == 1)
            return this.events.size();
        else if (type == 2)
            return this.venues.size();
        return 0;
    }

    /**
     * get really position
     *
     * @param position
     * @return
     */
    private int getPosition(int position) {
        return  position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup container) {
        Images image = null;
        if(type == 1)
        {
            if( events.size() > 0 && events.get(position).get_images().size() > 0)
                image = events.get(position).get_images().get(0);
        }
        else if(type == 2)
        {
            if( venues.size() > 0 && venues.get(position).get_images().size() > 0)
                image = venues.get(position).get_images().get(0);
        }
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = holder.imageView = new ImageView(context);
            view.setTag(holder);
        } else {
            holder = (ViewHolder)view.getTag();
        }

      //  holder.imageView.setImageResource(imageIdList.get(getPosition(position)));
        if(image != null)
        if(image.get_id() != null && db.is_image_available(image.get_id()))
        {
            new Display_Image(context,image.get_id(),holder.imageView).execute();

        }
        else
        {
            imageLoader.displayImage(image.get_image(), holder.imageView, options, new ImageLoadingListener(){

                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onLoadingFailed(String imageUri, View view,
                                            FailReason failReason) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onLoadingComplete(String imageUri, View view,
                                              Bitmap loadedImage) {
                    // TODO Auto-generated method stub
                    if(loadedImage != null) {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        loadedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        byte[] b = baos.toByteArray();
                        String image = Base64.encodeToString(b, Base64.DEFAULT);
                        Images img = null;
                        if (type == 1) {
                            img = events.get(position).get_images().get(0);
                        } else if (type == 2) {
                            img = venues.get(position).get_images().get(0);
                        }
                        long id1 = db.add_images_for_optimization(img.get_id(), image);
                        if (id1 > 0) {
                            Log.d("optimized image saved", "optimized image saved");
                        }
                    }


                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    // TODO Auto-generated method stub

                }



            });
        }
        return view;
    }

    private static class ViewHolder {

        ImageView imageView;
    }

    /**
     * @return the isInfiniteLoop
     */
    public boolean isInfiniteLoop() {
        return isInfiniteLoop;
    }

    /**
     * @param isInfiniteLoop the isInfiniteLoop to set
     */
    public SliderAdapter setInfiniteLoop(boolean isInfiniteLoop) {
        this.isInfiniteLoop = isInfiniteLoop;
        return this;
    }
}