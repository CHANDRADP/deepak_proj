package com.dpc.wtp.adapters;


import java.io.ByteArrayOutputStream;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;

import com.dpc.wtp.JSONParser;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Models.Server_Params;
import com.dpc.wtp.R;
import com.dpc.wtp.Models.Deals;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.activities.App;
import com.dpc.wtp.customs.MyProgress;
import com.dpc.wtp.fragments.Home_Fragment;
import com.dpc.wtp.tasks.Display_Image;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

import org.json.JSONException;
import org.json.JSONObject;

public class Club_Deal_Adapter extends BaseAdapter {
    LayoutInflater inflater;
    ViewHolder holder;
    View v;
    List<Deals> items;
    //	int[] deals = new int[]{R.drawable.deal1,R.drawable.deal2,R.drawable.deal3};
    Activity c;
    ImageLoader loader;
    DisplayImageOptions options;
    DatabaseHelper db;
    Net_Detect netDetect;
    Tracker app_tracker;
    private class ViewHolder {
        Text header,details;
        Button grabit;
        ImageView image;


    }
    public Club_Deal_Adapter(Activity c,List<Deals> l, ImageLoader imageLoader, DisplayImageOptions options,Tracker tracker)
    {
        this.c = c;
        this.items = l;
        this.loader = imageLoader;
        this.options = new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.default_bg)
                .showImageForEmptyUri(R.drawable.default_bg)
                .showImageOnFail(R.drawable.default_bg)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        db = new DatabaseHelper(c);
        this.app_tracker = tracker;
    }

    public View getView(final int position, View convertView, ViewGroup parent)
    {
        final Deals deal = items.get(position);

        v = convertView;
        if (v == null)
        {
            inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.row_deal_details, parent,false);
            holder = new ViewHolder();
            holder.header = (Text) v.findViewById(R.id.header);
            holder.grabit = (Button) v.findViewById(R.id.grab);
            holder.details = (Text) v.findViewById(R.id.details);
          //  holder.image = (ImageView) v.findViewById(R.id.deal_image);
            v.setTag(holder);

        }
        else
            holder = (ViewHolder) v.getTag();

        holder.header.setText(deal.get_name());
        holder.details.setText(deal.getDetails());
        holder.details.setOnClickListener(new View.OnClickListener() {
            boolean opened = false;
            @Override
            public void onClick(View v) {
                Text txt = (Text) v;
                C.expand(c,txt,opened);
                opened = !opened;
            }
        });


        holder.grabit.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                netDetect = new Net_Detect(c);
                if (netDetect.isConnectingToInternet()) {
                    new Request_for_Deal(position).execute();
                    Deals d = items.get(position);
                    app_tracker.send(new C().generate_analytics(C.CATEGORY.DEALS.get_category(), C.ACTION.ACTION_REQUEST.get_action(), Home_Fragment.deals_tabs[Integer.parseInt(d.get_type())]));

                }
                else
                {
                    C.net_alert(c);
                }
            }
        });
        return v;

    }



    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return items.size();
    }


    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return items.get(position);
    }


    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Request_for_Deal extends AsyncTask<String, Long, Boolean> {
        private MyProgress progress;
        private boolean exist = false;
        private String code = null;
        private int posi,closed = 0;

        public Request_for_Deal(int posi) {
            this.posi = posi;
            progress = new MyProgress(c);

        }


        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress.show();
        }


        @Override
        protected Boolean doInBackground(String... params) {

            return send_request();
        }

        @Override
        protected void onPostExecute(Boolean result) {

            progress.dismiss();

            if (result)
            {
                C.alert(c, "Success, please check my deals");
                notifyDataSetChanged();

            }
            else if(exist)
            {
                C.alert(c, "You have already got the requested deal");
            }
            else
            {
                if(closed == 1)
                {
                    C.alert(c, "Sorry this deal is not available right now, please try some other");
                    notifyDataSetChanged();
                }
                else
                C.alert(c, "Error occurred while processing the request");
            }

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        public boolean send_request()
        {
            Deals d = items.get(this.posi);
            boolean success = false;
            JSONParser parser = new JSONParser();
            JSONObject user = null;
            try {

                user = parser.makeHttpRequest(C.SERVER_URL, "POST",
                        new Server_Params().get_request_deal_params(App.get_userid(), d.get_venue_id(), d.get_id()));
                if(user != null)
                if(user.has(C.PARAMS.STATUS.get_param()))
                {
                    String v = user.getString(C.PARAMS.STATUS.get_param());
                    if(v.length() > 2)
                    {
                        code = user.getString(C.PARAMS.STATUS.get_param());
                        db.update_mydeals(C.clubs.get(d.get_venue_id()).get_venue_name(),d.get_id(),d.get_venue_id(),code,d.get_e_date());
                        items.remove(this.posi);
                        success = true;


                    }
                    else if(Integer.parseInt(v) == 0)
                    {
                        success = false;
                        exist = true;
                    }
                    else if(Integer.parseInt(v) == -2)
                    {
                        items.remove(this.posi);
                        closed = 1;
                    }
                    else
                    {
                        success = false;
                    }

                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return success;
        }

    }

}