package com.dpc.wtp.activities;

import java.util.ArrayList;
import java.util.Collections;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;

import com.actionbarsherlock.view.MenuItem;
import com.dpc.wtp.R;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.adapters.Events_Adapter;
import com.dpc.wtp.customs.Event_Priority_Sorter;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.customs.list.ParallaxListView;
import com.facebook.AppEventsLogger;

public class Activity_Event_List extends MyActivity{

	private ArrayList<Event> events  = new ArrayList<Event>();
	
	private ParallaxListView list;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.actionbar);
		final Text title = (Text)findViewById(android.R.id.title);
		title.setText("Events");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.frag_home_pages);
		SwipeRefreshLayout refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		refreshLayout.setEnabled(false);
		for(String e : Activity_Home.searched_values.values())
		{
			events.add(C.items.get(e));
		}
		Collections.sort(events, new Event_Priority_Sorter());
		list = (ParallaxListView)this.findViewById(R.id.list);
		list.setAdapter(new Events_Adapter(getApplicationContext(),events,imageLoader,options,app_tracker));

    }

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		AppEventsLogger.deactivateApp(this,getResources().getString(R.string.fb_app_id));
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;

    }

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
		AppEventsLogger.activateApp(this,getResources().getString(R.string.fb_app_id));
		
	}

	 @Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// TODO Auto-generated method stub
			switch(item.getItemId())
			{
		
			case android.R.id.home:
				super.onBackPressed();
				break;
			}
			return super.onOptionsItemSelected(item);
	    }
}
