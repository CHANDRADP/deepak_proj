package com.dpc.wtp.Models;



/**
 * Created by user on 1/12/2015.
 */
public class GroupUsers
{


    private String groupname, creationdate, admin, memberIds;
    private int serverid;


    public GroupUsers(String groupname, String creationdate, String admin, int serverid, String memberIds) {
        this.groupname = groupname;
        this.creationdate = creationdate;

        this.admin = admin;
        this.serverid = serverid;
        this.memberIds = memberIds;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getCreationdate() {
        return creationdate;
    }

    public void setCreationdate(String creationdate) {
        this.creationdate = creationdate;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public int getServerid() {
        return serverid;
    }

    public void setServerid(int serverid) {
        this.serverid = serverid;
    }

    public String getMemberIds() {
        return memberIds;
    }

    public void setMemberIds(String memberIds) {
        this.memberIds = memberIds;
    }
}
