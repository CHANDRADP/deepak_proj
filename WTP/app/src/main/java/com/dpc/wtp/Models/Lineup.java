package com.dpc.wtp.Models;

/**
 * Created by RADICALITY on 15/01/2015.
 */
public class Lineup {
    private String artistid = null, name = null,s_time = null,e_time = null;

    public Lineup(String artistid, String name, String s_time, String e_time) {
        this.artistid = artistid;
        this.name = name;
        this.s_time = s_time;
        this.e_time = e_time;
    }

    public String getArtistid() {
        return artistid;
    }

    public void setArtistid(String artistid) {
        this.artistid = artistid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getS_time() {
        return s_time;
    }

    public void setS_time(String s_time) {
        this.s_time = s_time;
    }

    public String getE_time() {
        return e_time;
    }

    public void setE_time(String e_time) {
        this.e_time = e_time;
    }
}
