package com.dpc.wtp.tasks;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.dpc.wtp.JSONParser;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.activities.App;

public class Update_Users extends AsyncTask <Void, Void, Boolean>
{
	String result = null;
	DatabaseHelper h;
	Context c;
	public Update_Users(Context c)
	{
		this.c = c;
	}
    @Override
    protected Boolean doInBackground(Void... unsued) {
    	
    	
    	return update();
    
    }

	
	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		Intent i = new Intent(C.BROADCAST_MSG);
		i.putExtra(PARAMS.USER_ID.get_param(), "1");
		c.sendBroadcast(i);
		
	}
    
	
	boolean update()
	{
		boolean success = false;
		JSONParser parser = new JSONParser();
			JSONObject user;
			ArrayList<String> nos = new ArrayList<String>();
			h = new DatabaseHelper(c);
			if(App.is_contacts_updated())
			{
				nos = h.get_all_android_contacts();
			}
			else
			{
				 nos = C.readContacts(c);
				if(nos != null)
				{
					App.set_contacts_updated(true);
					for(int i = 0;i < nos.size();i++)
					{
						h = new DatabaseHelper(c);
						h.add_new_android_contact(nos.get(i));
						
					}
				
				}
			}
			
			ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(C.METHOD.GET_APP_USERS.get_method()," "));
			
			for(int i = 0;i < nos.size();i++)
    		{
    			params.add(new BasicNameValuePair(PARAMS.NUMBER.get_param()+"[]",nos.get(i)));
    		}
			params.add(new BasicNameValuePair(C.PARAMS.USER_ID.get_param(),App.get_userid()));
			params.add(new BasicNameValuePair(C.PARAMS.TOKEN_STR.get_param(),App.get_token()));
			
			user = parser.makeHttpRequest(C.SERVER_URL, "POST", params);
			if(user != null)
	    		{
	    			try {
						if(user.getInt(PARAMS.STATUS.get_param()) > 0)
						{
							success = true;
							C.update_users(c,user);
							
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	    		}
				
		
		
		return success;
		
	}
}