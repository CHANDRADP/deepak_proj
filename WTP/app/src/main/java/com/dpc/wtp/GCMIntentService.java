package com.dpc.wtp;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.util.Log;


import com.dpc.wtp.Models.Guestlist;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.WakeLocker;
import com.dpc.wtp.activities.Activity_Event_Details;
import com.dpc.wtp.activities.Activity_Myapproves;
import com.dpc.wtp.activities.App;
import com.dpc.wtp.tasks.Register_Task;
import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;

import java.util.ArrayList;

public class GCMIntentService extends GCMBaseIntentService {
	private static final String TAG = "GCMIntentService";
	public GCMIntentService() {
        super(C.SENDER_ID);
        
       
    }

    /**
     * Method called on device registered
     **/
    @Override
    protected void onRegistered(Context context, String registrationId) {
        Log.i(TAG, "Device registered: regId = " + registrationId);
        App.gcm_updated(false);
        App.update_gcmid(registrationId);
        GCMRegistrar.setRegisteredOnServer(context, true);
        new Register_Task(context,registrationId).execute();
		
     
       
    }

    /**
     * Method called on device un registred
     * */
    @Override
    protected void onUnregistered(Context context, String registrationId) {
    	GCMRegistrar.setRegisteredOnServer(context, false);

    }

  
    /////////////////////////

    /**
     * Method called on Receiving a new message
     * */
    @Override
    protected void onMessage(Context context, Intent intent) 
    {
    	WakeLocker.acquire(getApplicationContext());
    	
    	Log.i(TAG, intent.getExtras().toString());
  
    	String message = intent.getExtras().getString(PARAMS.MESSAGE.get_param());
    	
    	boolean notify = false;
    	try 
		{
			JSONObject	obj = new JSONObject(message);
			if(obj.has(PARAMS.USER_NAME.get_param()))
			{
				String e_id,u_name;
				u_name = obj.getString(PARAMS.USER_NAME.get_param());
				e_id = obj.getString(PARAMS.EVENT_ID.get_param());
				
				notify = true;
				if(notify)
				{
					generateNotification(context,u_name+" have invited you to an event",e_id);
				}
			}
			else if(obj.has(PARAMS.GUEST_ID.get_param()))
			{
				String guest_id,code,eid;
				guest_id = obj.getString(PARAMS.GUEST_ID.get_param());
				code = obj.getString(PARAMS.UNIQUE_CODE.get_param());
				eid = obj.getString(PARAMS.EVENT_ID.get_param());
                ArrayList<Guestlist> list = new ArrayList<>();
                list.add(new Guestlist(null,guest_id,eid,null,code));
				long value = new DatabaseHelper(context).update_guestlist(list);
                Log.d("updated",String.valueOf(value));
				//generate notification to notify that the user is added to guest list
				this.generate_confirm_Notification(context, "Your requested guest list has been approved.",eid);
			}
			else if(obj.has(PARAMS.DEAL_ID.get_param()))
			{
				String deal_id,code;
				deal_id = obj.getString(PARAMS.DEAL_ID.get_param());
				code = obj.getString(PARAMS.UNIQUE_CODE.get_param());
				new DatabaseHelper(context).update_mydeals(null,deal_id, null, code, null);
				//generate notification to notify that the user is added to guest list
				this.generate_confirm_Notification(context, "Your requested deal has been approved.",deal_id);
			}
            else if(obj.has(PARAMS.EVENT_ID.get_param()))
            {
                String e_id;
                e_id = obj.getString(PARAMS.EVENT_ID.get_param());
               //generate notification to notify that the user is added to guest list
              //  generateNotification(context,"A new event is happening at your location, have look and apply for guest list",e_id);
            }
			
			
		} 
		catch (JSONException e2) 
		{
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		
    
		WakeLocker.release();
       
    }
    
   
    /**
     * Method called on receiving a deleted message
     * */
    @Override
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
      //  generateNotification(context, message);
    }

    /**
     * Method called on Error
     * */
    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
        
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        return super.onRecoverableError(context, errorId);
    }
    
    /**
     * Issues a notification to inform the user that server has sent a message.
     */
   @SuppressLint("NewApi")
public  void generateNotification(Context context,String msg,String e_id) {
    	Intent notificationIntent = new Intent(context, Activity_Event_Details.class);
    	notificationIntent.putExtra(PARAMS.EVENT_ID.get_param(), e_id);
    	//notificationIntent.putExtra(PARAMS.USER_NAME.get_param(), name);
    	notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    	PendingIntent contentIntent = PendingIntent.getActivity(context,
    	        (int) System.currentTimeMillis(), notificationIntent,
    	        0);
    	

    	NotificationManager nm = (NotificationManager) context
    	        .getSystemService(Context.NOTIFICATION_SERVICE);

    	Resources res = context.getResources();
    	Notification.Builder builder = new Notification.Builder(context);

    	builder.setContentIntent(contentIntent)
    				.setSmallIcon(R.drawable.notification)
    	            .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.logo))
    	            .setTicker(msg)
    	            .setWhen(System.currentTimeMillis())
    	            .setAutoCancel(true)
    	            //.setOngoing(ongoing)
    	            .setDefaults(Notification.DEFAULT_SOUND|Notification.DEFAULT_VIBRATE)
    	            .setContentTitle(res.getString(R.string.app_name))
    	            .setContentText(msg);
    	Notification n = builder.build();

    	nm.notify(Integer.parseInt(App.get_userid()+e_id), n);
    }
   @SuppressLint("NewApi")
public  void generate_confirm_Notification(Context context,String msg,String e_id) {
   	Intent notificationIntent = new Intent(context, Activity_Myapproves.class);
   	notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
   	PendingIntent contentIntent = PendingIntent.getActivity(context,
   	        (int) System.currentTimeMillis(), notificationIntent,
   	     PendingIntent.FLAG_UPDATE_CURRENT);
   	

   	NotificationManager nm = (NotificationManager) context
   	        .getSystemService(Context.NOTIFICATION_SERVICE);

   	Resources res = context.getResources();
   	Notification.Builder builder = new Notification.Builder(context);

   	builder.setContentIntent(contentIntent)
   				.setSmallIcon(R.drawable.notification)
   	            .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.logo))
   	            .setTicker(msg)
   	            .setWhen(System.currentTimeMillis())
   	            .setAutoCancel(true)
   	            //.setOngoing(ongoing)
   	            .setDefaults(Notification.DEFAULT_SOUND|Notification.DEFAULT_VIBRATE)
   	            .setContentTitle(res.getString(R.string.app_name))
   	            .setContentText(msg);
   	Notification n = builder.build();

   	nm.notify(Integer.parseInt(App.get_userid()+e_id), n);
   }
    
   
  
   
}
