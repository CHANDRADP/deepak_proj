package com.dpc.wtp.adapters;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.dpc.wtp.Models.Event;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.Utils.TextB;
import com.dpc.wtp.activities.Activity_Event_Details;

import com.dpc.wtp.tasks.Display_Image;
import com.dpc.wtp.tasks.Update_Going_Status;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.io.ByteArrayOutputStream;
import java.util.List;

public class Events_Adapter extends BaseAdapter {
	LayoutInflater inflater;
	ViewHolder holder;
	View v;
	private List<Event> items;
	Context c;
	ImageLoader loader;
	DisplayImageOptions options;
	private Net_Detect net;
	DatabaseHelper db;
    Tracker app_tracker;

	private class ViewHolder {
		Text e_name,status_txt,e_location;
		TextB e_time;
		ImageView e_image,going,may_be,not_going;
		LinearLayout ll_location,ll_left,ll_center,ll_right;
        RelativeLayout blur;
		
	}
	public Events_Adapter(Context c, List<Event> l, ImageLoader loader, DisplayImageOptions options,Tracker tracker)
	{
		this.c = c;
		if(items != null)
		{
			items.clear();
		}
		this.items = l;
		this.loader = loader;
		this.options = new DisplayImageOptions.Builder()
		.showStubImage(R.drawable.default_bg)
		.showImageForEmptyUri(R.drawable.default_bg)
		.showImageOnFail(R.drawable.default_bg)
	//	.cacheInMemory(true)
		//.cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new FadeInBitmapDisplayer(C.ANIM_DURATION))
		.build();
		db = new DatabaseHelper(c);
		net = new Net_Detect(this.c);
        this.app_tracker = tracker;

	}
	
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		final Event event = items.get(position);
		
		v = convertView;
		if (v == null) 
		{
			 inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 v = inflater.inflate(R.layout.row_event, null);
			 holder = new ViewHolder();

            holder.blur = (RelativeLayout)v.findViewById(R.id.blur);
			 holder.e_time = (TextB) v.findViewById(R.id.date);
			 holder.e_name = (Text) v.findViewById(R.id.e_name);
			 holder.e_location = (Text) v.findViewById(R.id.e_location);
			 holder.e_image = (ImageView) v.findViewById(R.id.event_image);
			 
		//	 holder.ll_location = (LinearLayout)v.findViewById(R.id.ll_location);
		//	 holder.ll_left = (LinearLayout)v.findViewById(R.id.left);
		//	 holder.ll_center = (LinearLayout)v.findViewById(R.id.center);
		//	 holder.ll_right = (LinearLayout)v.findViewById(R.id.right);
			 
		//	 holder.status_txt = (Text) v.findViewById(R.id.status);
			 
			 
			 holder.going = (ImageView) v.findViewById(R.id.status_icon);
		//	 holder.may_be = (ImageView) v.findViewById(R.id.status_two_icon);
		//	 holder.not_going = (ImageView) v.findViewById(R.id.status_three_icon);
			 v.setTag(holder);
			 
		}
		else
			holder = (ViewHolder) v.getTag();
		
		if(event.get_images().size() > 0)
		{
			//
			if(db.is_image_available(event.get_images().get(0).get_id()))
			{
				new Display_Image(c,event.get_images().get(0).get_id(),holder.e_image).execute();
				
			}
			else
			{
				loader.displayImage(event.get_images().get(0).get_image(), holder.e_image, options, new ImageLoadingListener(){

					@Override
					public void onLoadingStarted(String imageUri, View view) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onLoadingFailed(String imageUri, View view,
							FailReason failReason) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onLoadingComplete(String imageUri, View view,
							Bitmap loadedImage) {
						// TODO Auto-generated method stub
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						loadedImage.compress(Bitmap.CompressFormat.JPEG, 80, baos);
						byte[] b = baos.toByteArray(); 
						String image = Base64.encodeToString(b, Base64.DEFAULT);
						
						long id1 = db.add_images_for_optimization(event.get_images().get(0).get_id(), image);
						if(id1 > 0)
						{
							Log.d("optimized image saved", "optimized image saved");
						}
					//	blur(((BitmapDrawable)((ImageView)view).getDrawable()).getBitmap(),holder.blur,2);
					}

					@Override
					public void onLoadingCancelled(String imageUri, View view) {
						// TODO Auto-generated method stub
						
					}
					

					
				});
			}
		}
		else
			holder.e_image.setImageDrawable(c.getResources().getDrawable(R.drawable.default_bg));

		String[] date = C.get_date_as_day_month(event.get_event_date());
		//set_text(holder.status_txt,event.get_status_txt());
		holder.e_time.setText(date[1].toUpperCase()+"\n"+C.get_date_suffix(date[0]));
		holder.e_name.setText(event.get_event_name());
		holder.e_location.setText(C.clubs.get(event.get_venue_id()).get_venue_name()+"/ "+C.clubs.get(event.get_venue_id()).get_venue_area()+"/ "+event.get_event_s_time());
		
	//	set_text(holder.e_location,event.get_venue().get_venue_area());
		
		if(event.get_event_going_type() == 1)
		{
			holder.going.setBackgroundResource(R.drawable.going_p);
		}
		else
		{
			holder.going.setBackgroundResource(R.drawable.going_n);
		}
		holder.e_name.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent i = new Intent(c,Activity_Event_Details.class);
				i.putExtra(C.PARAMS.EVENT_ID.get_param(), event.get_event_id());
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				c.startActivity(i);
			}
			
		});
		
		holder.going.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				String type = null;
				boolean add = false;
				if(event.get_event_going_type() == 0)
				{
					add = true;
					type = "1";
                    app_tracker.send(new C().generate_analytics(C.CATEGORY.EVENTS.get_category(),C.ACTION.ACTION_ATTEND.get_action(),C.LABEL.YES.get_label()));

                }
				else if(event.get_event_going_type() > 0)
				{
					type = "-1";
                    app_tracker.send(new C().generate_analytics(C.CATEGORY.EVENTS.get_category(),C.ACTION.ACTION_ATTEND.get_action(),C.LABEL.NO.get_label()));

                }
				else
				{
					type = "1";
                    app_tracker.send(new C().generate_analytics(C.CATEGORY.EVENTS.get_category(),C.ACTION.ACTION_ATTEND.get_action(),C.LABEL.YES.get_label()));

                }
				
				if(net.isConnectingToInternet())
				{
					
					new Update_Going_Status(c,event.get_event_id(),type,add).execute();
					
				}
				else
				{
					db.update_event_attending_type(event.get_event_id(), add == true ? "1" : "2", type);
				}
				items.get(position).set_goingtype(Integer.parseInt(type));
                set_going((ImageView)v,type);
			}
			
		});
	
		return v;

	}
    private void set_going(ImageView img,String type)
    {
        if(type.equals("-1"))
        {
            img.setBackgroundResource(R.drawable.going_n);
        }
        else
        {
            img.setBackgroundResource(R.drawable.going_p);
        }
    }

	private void set_text(Text t_view, String txt) {
		// TODO Auto-generated method stub
		t_view.setText(txt);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}


	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
	}


	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void blur(Bitmap bkg, View view, float radius) {
        Bitmap overlay = Bitmap.createBitmap(
                view.getMeasuredWidth(),
                view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(overlay);

        canvas.drawBitmap(bkg, -view.getLeft(),
                -view.getTop(), null);

        RenderScript rs = RenderScript.create(c);

        Allocation overlayAlloc = Allocation.createFromBitmap(
                rs, overlay);

        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(
                rs, overlayAlloc.getElement());

        blur.setInput(overlayAlloc);

        blur.setRadius(radius);

        blur.forEach(overlayAlloc);

        overlayAlloc.copyTo(overlay);

        view.setBackground(new BitmapDrawable(
                c.getResources(), overlay));

        rs.destroy();
    }
}