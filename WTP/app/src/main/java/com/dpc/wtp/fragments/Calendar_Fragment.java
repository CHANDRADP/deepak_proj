/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dpc.wtp.fragments;


import java.util.ArrayList;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.dpc.wtp.R;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.activities.Activity_Event_Details;
import com.dpc.wtp.adapters.Events_Adapter;
import com.dpc.wtp.customs.MyFragment;
import com.dpc.wtp.customs.SlidingTabLayout;
import com.facebook.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * A basic sample which shows how to use {@link com.dpc.wtp.customs.SlidingTabLayout}
 * to display a custom {@link android.support.v4.view.ViewPager} title strip which gives continuous feedback to the user
 * when scrolling.
 */
public class Calendar_Fragment extends MyFragment {

    static final String LOG_TAG = "SlidingTabsBasicFragment";

    /**
     * A custom {@link android.support.v4.view.ViewPager} title strip which looks much like Tabs present in Android v4.0 and
     * above, but is designed to give continuous feedback to the user when scrolling.
     */
    private SlidingTabLayout mSlidingTabLayout;

    /**
     * A {@link android.support.v4.view.ViewPager} which will be used in conjunction with the {@link com.dpc.wtp.customs.SlidingTabLayout} above.
     */
    private ViewPager mViewPager;
    private SamplePagerAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app_tracker.setScreenName(C.SCREEN.SCREEN_CALENDAR.get_screen());
        app_tracker.send(new HitBuilders.AppViewBuilder().build());
    }

    /**
     * Inflates the {@link android.view.View} which will be displayed by this {@link android.support.v4.app.Fragment}, from the app's
     * resources.
     */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	options = new DisplayImageOptions.Builder()
    	.showStubImage(R.drawable.default_bg)
    	.showImageForEmptyUri(R.drawable.default_bg)
    	.showImageOnFail(R.drawable.default_bg)
    	.cacheInMemory(true)
    	.cacheOnDisc(true)
    	.bitmapConfig(Bitmap.Config.RGB_565)
    	.build();
        return inflater.inflate(R.layout.frag_home, container, false);
    }

    // BEGIN_INCLUDE (fragment_onviewcreated)
    /**
     * This is called after the {@link #onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)} has finished.
     * Here we can pick out the {@link android.view.View}s we need to configure from the content view.
     *
     * We set the {@link android.support.v4.view.ViewPager}'s adapter to be an instance of {@link Calendar_Fragment.SamplePagerAdapter}. The
     * {@link com.dpc.wtp.customs.SlidingTabLayout} is then given the {@link android.support.v4.view.ViewPager} so that it can populate itself.
     *
     * @param view View created in {@link #onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)}
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // BEGIN_INCLUDE (setup_viewpager)
        // Get the ViewPager and set it's PagerAdapter so that it can display items
        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        adapter = new SamplePagerAdapter();
        mViewPager.setAdapter(adapter);
        // END_INCLUDE (setup_viewpager)

        // BEGIN_INCLUDE (setup_slidingtablayout)
        // Give the SlidingTabLayout the ViewPager, this must be done AFTER the ViewPager has had
        // it's PagerAdapter set.
        mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setCustomTabView(R.layout.tab_item, R.id.txt);
        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setSelectedIndicatorColors(new int[]{getResources().getColor(R.color.red)});
        mSlidingTabLayout.setTabTextsize((int)this.getResources().getDimension(R.dimen.eighteen_sp));
        // END_INCLUDE (setup_slidingtablayout)
        
    }
    
    @Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		AppEventsLogger.deactivateApp(this.getSherlockActivity(),getResources().getString(R.string.fb_app_id));
		
		getSherlockActivity().unregisterReceiver(NewMessageReceiver);
	}



	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		AppEventsLogger.activateApp(this.getSherlockActivity(),getResources().getString(R.string.fb_app_id));
		
		getSherlockActivity().registerReceiver(NewMessageReceiver, new IntentFilter(
				C.BROADCAST_MSG));
		
	}
	
	private final BroadcastReceiver NewMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent i) 
		{
			if(i.hasExtra(PARAMS.EVENT_ID.get_param()))
			{
				adapter.notifyDataSetChanged();
			}
		}
	};
    // END_INCLUDE (fragment_onviewcreated)

    /**
     * The {@link android.support.v4.view.PagerAdapter} used to display pages in this sample.
     * The individual pages are simple and just display two lines of text. The important section of
     * this class is the {@link #getPageTitle(int)} method which controls what is displayed in the
     * {@link com.dpc.wtp.customs.SlidingTabLayout}.
     */
    class SamplePagerAdapter extends PagerAdapter {

        /**
         * @return the number of pages to display
         */
        @Override
        public int getCount() {
            return C.events.size();
        }

        /**
         * @return true if the value returned from {@link #instantiateItem(android.view.ViewGroup, int)} is the
         * same object as the {@link android.view.View} added to the {@link android.support.v4.view.ViewPager}.
         */
        @Override
        public boolean isViewFromObject(View view, Object o) {
            return o == view;
        }

        // BEGIN_INCLUDE (pageradapter_getpagetitle)
        /**
         * Return the title of the item at {@code position}. This is important as what this method
         * returns is what is displayed in the {@link com.dpc.wtp.customs.SlidingTabLayout}.
         * <p>
         * Here we construct one using the position value, but for real application the title should
         * refer to the item's contents.
         */
        @Override
        public CharSequence getPageTitle(int position) {
        	String[] temp = C.get_date_as_day_month(C.events.get(position).get(0).get_event_date());
        	String date = temp[1]+" "+temp[0];
        	
            return C.get_date_suffix(date);
        }
        // END_INCLUDE (pageradapter_getpagetitle)

        /**
         * Instantiate the {@link android.view.View} which should be displayed at {@code position}. Here we
         * inflate a layout from the apps resources and then change the text view to signify the position.
         */
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            // Inflate a new layout from our resources
            View view = getActivity().getLayoutInflater().inflate(R.layout.ac_settings,
                    container, false);
            // Add the newly created View to the ViewPager
            container.addView(view);

            // Retrieve a TextView from the inflated View, and update it's text
           final ListView list = (ListView) view.findViewById(R.id.list);
            list.setDividerHeight(5);
            final ArrayList<Event> list_items = C.events.get(position);
            final Events_Adapter list_adapter = new Events_Adapter(getSherlockActivity().getApplicationContext(),list_items,imageLoader,options,app_tracker);
    		list.setAdapter(list_adapter);
    		list.setOnItemClickListener(new OnItemClickListener(){

    			@Override
    			public void onItemClick(AdapterView<?> arg0, View v, int posi,
    					long arg3) {
    				Intent i = new Intent(getSherlockActivity().getApplicationContext(),Activity_Event_Details.class);
    				i.putExtra(PARAMS.EVENT_ID.get_param(), list_items.get(posi).get_event_id());
    				startActivity(i);
    				
    			}
    			
    		});
            return view;
        }

        /**
         * Destroy the item from the {@link android.support.v4.view.ViewPager}. In our case this is simply removing the
         * {@link android.view.View}.
         */
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
           
        }

    }
}
