package com.dpc.wtp.Models;

public class WTP_Contact {
private String id,name,ccode,number,status,thumbnail,time,contactid;
private boolean selected = false,is_fb = false;
public WTP_Contact(String wtp_id,String name,String c_code,String number,String status,String thumbnail,String time,String contact_id)
{
	this.id = wtp_id;
	this.name = name;
	this.ccode = c_code;
	this.number = number;
	this.status = status;
	this.thumbnail = thumbnail;
	this.time = time;
	this.contactid = contact_id;
	}
public WTP_Contact(String id,String name,String image,boolean selected,boolean is_fb)
{
	this.id = id;
	this.name = name;
	this.thumbnail = image;
	this.selected = selected;
	this.is_fb = is_fb;
	}
public WTP_Contact() {
	// TODO Auto-generated constructor stub
}
public boolean is_fb_contact()
{
	return is_fb;}
public void set_selected(boolean selected)
{
	this.selected = selected;
	}
public boolean is_selected()
{
	return selected;}

public String get_wtp_id()
{
	return id;
}

public String get_name()
{
	return name;
}
public String get_c_code()
{
	return ccode;
}
public String get_number()
{
	return number;
}
public String get_status()
{
	return status;
}
public String get_thumbnail()
{
	return thumbnail;
}
public String get_time()
{
	return time;
}
public String get_contact_id()
{
	return contactid;
}

}
