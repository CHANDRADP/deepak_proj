package com.dpc.wtp.Models;

public class Slide_Menu_Item {
	
	private String title;
	private int icon;
	
	public Slide_Menu_Item(){}

	public Slide_Menu_Item(String title, int icon){
		this.title = title;
		this.icon = icon;
	}
	
	
	
	public String getTitle(){
		return this.title;
	}
	
	public int getIcon(){
		return this.icon;
	}
	
	
	public void setTitle(String title){
		this.title = title;
	}
	
	public void setIcon(int icon){
		this.icon = icon;
	}
	
	
	
}
