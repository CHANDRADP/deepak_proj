package com.dpc.wtp.customs;

import twitter4j.auth.RequestToken;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.TwitterUtil;
import com.dpc.wtp.activities.App;
import com.dpc.wtp.tasks.Twitter_Share;

public class Twitter_Login {

	private Context c;
	private boolean isUseWebViewForAuthentication = false;
    public Twitter_Login(Context c)
	{
		this.c = c;
	}
	public void clear_twitter()
	{
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
		SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(C.PREFERENCE_TWITTER_OAUTH_TOKEN, null);
        editor.putString(C.PREFERENCE_TWITTER_OAUTH_TOKEN_SECRET, null);
        editor.putBoolean(C.PREFERENCE_TWITTER_IS_LOGGED_IN, false);
        editor.commit();
	}
	public boolean is_twitter_login()
	{
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
	    return sharedPreferences.getBoolean(C.PREFERENCE_TWITTER_IS_LOGGED_IN,false);
	}
	public void login_to_twitter()
	{
       new TwitterAuthenticateTask().execute();

	}

    
class TwitterAuthenticateTask extends AsyncTask<String, String, RequestToken> {

        

        @Override
        protected RequestToken doInBackground(String... params) {
        	return TwitterUtil.getInstance().getRequestToken(true);
        }
        
        @Override
        protected void onPostExecute(RequestToken requestToken) {
        	if (requestToken!=null)
            {
        		
                if (isUseWebViewForAuthentication)
                {

                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(requestToken.getAuthenticationURL()));
                    c.startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(c.getApplicationContext(), OAuthActivity.class);
                    intent.putExtra(C.STRING_EXTRA_AUTHENCATION_URL,requestToken.getAuthenticationURL());
                    c.startActivity(intent);
                }
            }
        }
    }


}
