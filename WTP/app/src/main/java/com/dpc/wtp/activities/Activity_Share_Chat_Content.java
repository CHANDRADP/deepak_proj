package com.dpc.wtp.activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.actionbarsherlock.app.ActionBar;
import com.dpc.wtp.Models.ActiveChatDetails;
import com.dpc.wtp.Models.ChatMsg;
import com.dpc.wtp.Models.GroupDetails;
import com.dpc.wtp.Models.ShareMsg;
import com.dpc.wtp.Models.TextMedia;
import com.dpc.wtp.Models.UploadDetails;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.CirclePageIndicator;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.adapters.Share_ViewPager_Adapter;
import com.dpc.wtp.adapters.ViewPager_Adapter;
import com.dpc.wtp.amazon.UploadService;
import com.dpc.wtp.customs.CustomDialogWithTwoButtons;
import com.dpc.wtp.customs.Custom_Send_Message;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.services.WTP_Service;

import org.jivesoftware.smack.packet.Message;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

public class Activity_Share_Chat_Content extends MyActivity implements ActionBar.TabListener{

    private ViewPager viewPager;
    private Share_ViewPager_Adapter mAdapter;

    Button btnSend;
    int messagetype = 0,chatId=0;
    String displayMesssageType=null, conversationMessageType=null, txtmsg=null, serverurl = null;
    List<String> ids_arraylist = null;
    public static HashMap<Integer, ShareMsg> selectedIds = new HashMap<Integer, ShareMsg>();

    public static String CURRENT_CHAT_USER;

    private String[] tabs = { "Active", "Non Active", "Groups" };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_chat_content);


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        btnSend = (Button) findViewById(R.id.btnSend);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        mAdapter = new Share_ViewPager_Adapter(getSupportFragmentManager());
        viewPager.setAdapter(mAdapter);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // on changing the page
                // make respected tab selected
                getSupportActionBar().setSelectedNavigationItem(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });

        db = new DatabaseHelper(this);

        for (String tab_name : tabs) {
            getSupportActionBar().addTab(getSupportActionBar().newTab().setText(tab_name)
                    .setTabListener(this));
        }
        getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);


        String selectedItems = null;
        Intent i = getIntent();
        if (i.hasExtra("selectedItems")) {
            selectedItems = i.getStringExtra("selectedItems");
            String[] ids = selectedItems.split(", ");
            ids_arraylist = Arrays.asList(ids);

        }




        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String localTime = get_current_time();
                String msgDateTime = get_universal_date(localTime);

                Set set = selectedIds.entrySet();

                Iterator i = set.iterator();

                while(i.hasNext()) {
                      //SHARE_WITH_IDS_LIST - users list

                    Map.Entry me = (Map.Entry)i.next();
                    int sendTo = Integer.parseInt(me.getKey().toString());
                    String to = me.getKey() + "@hellochat";
                    ShareMsg user = (ShareMsg) me.getValue();


                    for (int j = 0; j <= ids_arraylist.size() - 1; j++) // ids_arraylist - message ids
                    {
                        String id = ids_arraylist.get(j);
                        TextMedia data = db.get_conversation_by_message_id(ids_arraylist.get(j));

                        messagetype = data.getMediatype();
                        displayMesssageType = null;
                        if (messagetype == 1) {
                            displayMesssageType = "message";
                        } else if (messagetype == 2) {
                            displayMesssageType = "image";
                        } else if (messagetype == 3) {
                            displayMesssageType = "audio";
                        } else if (messagetype == 4) {
                            displayMesssageType = "video";
                        } else if (messagetype == 5) {
                            displayMesssageType = "sticker";
                        }


                        if (messagetype == 1) {
                            conversationMessageType = "Message";
                        } else if (messagetype == 2) {
                            conversationMessageType = "Image";
                        } else if (messagetype == 3) {
                            conversationMessageType = "Audio";
                        } else if (messagetype == 4) {
                            conversationMessageType = "Video";
                        } else if (messagetype == 5) {
                            conversationMessageType = "Sticker";
                        }




                        String uploadChatId = null;
                        if(messagetype ==1)
                        {
                            String chatid = null;
                            if (user.getIs_group()== 0)
                            {
                                int unreadcount = db.get_unread_msg_count_with_id(Integer.toString(sendTo));
                                if (!db.check_chatId_exist(Integer.toString(sendTo)))// if name not present then insert in db
                                {
                                    long id2 = db.insertChatDetails(Integer.toString(sendTo), txtmsg, msgDateTime, Integer.toString(user.getIs_group()), "0"); //change "0"
                                    chatid = String.valueOf(id2);
                                }
                                else
                                {
                                    chatid = db.get_chatid_of_member_by_userid(Integer.toString(sendTo));
                                }

                                db.update_chat_lastmsg_time_count(Integer.toString(sendTo), data.getTextmsg(), msgDateTime, Integer.toString(unreadcount));
                                long id3 = db.insertConversationDetails(chatid, data.getTextmsg(), msgDateTime, Integer.toString(C.POSTER_ME), Integer.toString(C.STATUS_READ), Integer.toString(messagetype), Integer.toString(C.UPLOADING), "Not Applicable",Integer.toString(C.STATUS_SENT));
                                uploadChatId = String.valueOf(id3);
                                Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),sendTo,data.getTextmsg(), messagetype);
                                csm.send_simple_text_message(uploadChatId);
                            }
                            else if (user.getIs_group()==1)
                            {
                                int unreadcount = db.get_unread_msg_count_with_id(Integer.toString(sendTo));
                                if (!db.check_chatId_exist(Integer.toString(sendTo)))// if name not present then insert in db
                                {
                                    long id1 = db.insertChatDetails(Integer.toString(sendTo), txtmsg, msgDateTime, Integer.toString(user.getIs_group()), Integer.toString(0)); //change "0"
                                    chatid = String.valueOf(id1);
                                }
                                else
                                {
                                    chatid = db.get_chatid_of_member_by_userid(Integer.toString(sendTo));
                                }

                                Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),sendTo,data.getTextmsg(), messagetype);
                                csm.send_group_message();

                                db.update_chat_lastmsg_time_count(Integer.toString(sendTo), data.getTextmsg(), msgDateTime, Integer.toString(unreadcount)); // 1 = poster "me" , 1- readStatus "read"
                                long id2 = db.insertConversationDetails(chatid, data.getTextmsg(), msgDateTime, Integer.toString(C.POSTER_ME), Integer.toString(C.STATUS_READ), Integer.toString(messagetype), Integer.toString(C.UPLOADING), "Not Applicable", Integer.toString(C.STATUS_SENT));
                                uploadChatId = String.valueOf(id2);
                            }
                        }
                        else if(messagetype==5)
                        {
                            String chatid = null;
                            if (user.getIs_group()==0)
                            {
                                int unreadcount = db.get_unread_msg_count_with_id(Integer.toString(sendTo));
                                if (!db.check_chatId_exist(Integer.toString(sendTo)))// if name not present then insert in db
                                {
                                    long id3 = db.insertChatDetails(Integer.toString(sendTo), "Sticker", msgDateTime, "0", "1"); //change "0"
                                    chatid = String.valueOf(id3);
                                }
                                else
                                {
                                    chatid = db.get_chatid_of_member_by_userid(Integer.toString(sendTo));
                                }


                                db.update_chat_lastmsg_time_count(Integer.toString(sendTo), "Sticker", msgDateTime, Integer.toString(unreadcount));
                                long id1 = db.insertConversationDetails(chatid, data.getTextmsg(), msgDateTime, Integer.toString(C.POSTER_ME), Integer.toString(C.STATUS_READ), Integer.toString(messagetype), Integer.toString(C.UPLOADING), "Not Applicable", Integer.toString(C.STATUS_SENT));
                                uploadChatId = String.valueOf(id1);
                                Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),sendTo,data.getTextmsg(), messagetype);
                                csm.send_simple_text_message(uploadChatId);
                            }
                            else if (user.getIs_group()==1)
                            {
                                int unreadcount = db.get_unread_msg_count_with_id(Integer.toString(sendTo));
                                if (!db.check_chatId_exist(Integer.toString(sendTo)))// if name not present then insert in db
                                {
                                    long id4 = db.insertChatDetails(Integer.toString(sendTo), "Sticker", msgDateTime, "0", "1"); //change "0"
                                    chatid = String.valueOf(id4);
                                }
                                else
                                {
                                    chatid = db.get_chatid_of_member_by_userid(Integer.toString(sendTo));
                                }

                                Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),sendTo,data.getTextmsg(), messagetype);
                                csm.send_group_message();
                                db.update_chat_lastmsg_time_count(Integer.toString(sendTo), displayMesssageType, msgDateTime, Integer.toString(unreadcount)); // 1 = poster "me" , 1- readStatus "read"
                                long id2 = db.insertConversationDetails(chatid, data.getTextmsg(), msgDateTime, Integer.toString(C.POSTER_ME), Integer.toString(C.STATUS_READ), Integer.toString(messagetype), Integer.toString(C.UPLOADING), "Not Applicable", Integer.toString(C.STATUS_SENT));
                                uploadChatId = String.valueOf(id2);
                            }
                        }
                        else
                        {
                            String chatid = null;
                            if (user.getIs_group()==0)
                            {
                                int unreadcount = db.get_unread_msg_count_with_id(Integer.toString(sendTo));
                                if (!db.check_chatId_exist(Integer.toString(sendTo)))// if name not present then insert in db
                                {
                                    long id5 = db.insertChatDetails(Integer.toString(sendTo), conversationMessageType, msgDateTime, "0", "1"); //change "0"
                                    chatid = String.valueOf(id5);
                                }
                                else
                                {
                                    chatid = db.get_chatid_of_member_by_userid(Integer.toString(sendTo));
                                }

                                //send_simple_text_message(sendTo, data.getTextmsg(), displayMesssageType);
                                db.update_chat_lastmsg_time_count(Integer.toString(sendTo), conversationMessageType, msgDateTime, Integer.toString(unreadcount));
                                long id4 = db.insertConversationDetails(chatid, data.getTextmsg(), msgDateTime, Integer.toString(C.POSTER_ME), Integer.toString(C.STATUS_READ), Integer.toString(messagetype), Integer.toString(C.UPLOADING), data.getServerurl(), Integer.toString(C.STATUS_SENT));
                                uploadChatId = String.valueOf(id4);
                            }
                            else if (user.getIs_group()==1)
                            {
                                int unreadcount = db.get_unread_msg_count_with_id(Integer.toString(sendTo));
                                if (!db.check_chatId_exist(Integer.toString(sendTo)))// if name not present then insert in db
                                {
                                    long id6 = db.insertChatDetails(Integer.toString(sendTo), "Sticker", msgDateTime, "0", "1"); //change "0"
                                    chatid = String.valueOf(id6);
                                }
                                else
                                {
                                    chatid = db.get_chatid_of_member_by_userid(Integer.toString(sendTo));
                                }

                                //send_group_message(sendTo, data.getTextmsg(), displayMesssageType);
                                db.update_chat_lastmsg_time_count(Integer.toString(sendTo), conversationMessageType, msgDateTime, Integer.toString(unreadcount)); // 1 = poster "me" , 1- readStatus "read"
                                long id5 = db.insertConversationDetails(chatid, data.getTextmsg(), msgDateTime,Integer.toString(C.POSTER_ME), Integer.toString(C.STATUS_READ), Integer.toString(messagetype), Integer.toString(C.UPLOADING), data.getServerurl(), Integer.toString(C.STATUS_SENT));
                                uploadChatId = String.valueOf(id5);
                            }

                            UploadDetails upload = new UploadDetails(data.getTextmsg(),data.getMediatype(),Integer.parseInt(uploadChatId),data.getServerurl(),user.getIs_group(),sendTo);

                            UploadService.share_list_queue.add(upload);







                            if(!isMyServiceRunning())
                            {
                                Intent intent = new Intent(getApplicationContext(), UploadService.class);
                                intent.putExtra("sharing", "yes");

                                Activity_Share_Chat_Content.this.startService(intent);
                            }


                        }
                    }
                }
                finish();


            }
        });

    }

    private boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("com.dpc.wtp.amazon.UploadService".equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public String get_current_time() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
        return sdf.format(c.getTime());
    }

    public String get_universal_date(String time) {
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm a");


        Date date = null;
        try {
            //Here you say to java the initial timezone. This is the secret
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = sdf.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //Here you set to your timezone

        sdf.setTimeZone(TimeZone.getDefault());
        time = sdf.format(date);
        return time;
    }




    @Override
    protected void onPause() {
        super.onPause();

       selectedIds.clear();
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;


    }

    @Override
    public void onResume()
    {
        super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;


    }


    public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);

    }

    /**
     * Called when a tab enters the selected state.
     *
     * @param tab The tab that was selected
     * @param ft  A {@link android.support.v4.app.FragmentTransaction} for queuing fragment operations to execute
     *            during a tab switch. The previous tab's unselect and this tab's select will be
     *            executed in a single transaction. This FragmentTransaction does not support
     */
    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        viewPager.setCurrentItem(tab.getPosition());

    }



    /**
     * Called when a tab exits the selected state.
     *
     * @param tab The tab that was unselected
     * @param ft  A {@link android.support.v4.app.FragmentTransaction} for queuing fragment operations to execute
     *            during a tab switch. This tab's unselect and the newly selected tab's select
     *            will be executed in a single transaction. This FragmentTransaction does not
     */
    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    /**
     * Called when a tab that is already selected is chosen again by the user.
     * Some applications may use this action to return to the top level of a category.
     *
     * @param tab The tab that was reselected.
     * @param ft  A {@link android.support.v4.app.FragmentTransaction} for queuing fragment operations to execute
     *            once this method returns. This FragmentTransaction does not support
     */
    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }
}
