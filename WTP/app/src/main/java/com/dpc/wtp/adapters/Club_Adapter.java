package com.dpc.wtp.adapters;


import java.io.ByteArrayOutputStream;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dpc.wtp.R;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Models.V_Details;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.tasks.Display_Image;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class Club_Adapter extends BaseAdapter {
	LayoutInflater inflater;
	ViewHolder holder;
	View v;
	List<V_Details> items;
	Context c;
	ImageLoader loader;
	DisplayImageOptions options;
	DatabaseHelper db;

	private class ViewHolder {
		Text c_name,c_location,c_price;
		ImageView c_image,one,two,three,four,five;//,going,may_be,not_going;
		LinearLayout ll_location;//,ll_left,ll_center,ll_right;
		
	}
	public Club_Adapter(Context c,List<V_Details> l,ImageLoader loader,DisplayImageOptions options)
	{
		this.c = c;
		this.items = l;
		this.loader = loader;
		this.options = new DisplayImageOptions.Builder()
		.showStubImage(R.drawable.default_bg)
		.showImageForEmptyUri(R.drawable.default_bg)
		.showImageOnFail(R.drawable.default_bg)
		//.cacheInMemory(true)
		//.cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new FadeInBitmapDisplayer(C.ANIM_DURATION))
		.build();
		db = new DatabaseHelper(c);

	}
	
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		final V_Details club = items.get(position);
		
		v = convertView;
		if (v == null) 
		{
			 inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 v = inflater.inflate(R.layout.row_clubs, null);
			 holder = new ViewHolder();
			// holder.ll_location = (LinearLayout)v.findViewById(R.id.ll_location);
			// holder.ll_left = (LinearLayout)v.findViewById(R.id.left);
			// holder.ll_center = (LinearLayout)v.findViewById(R.id.center);
			// holder.ll_right = (LinearLayout)v.findViewById(R.id.right);
			 holder.c_name = (Text) v.findViewById(R.id.c_name);
			 holder.c_location = (Text) v.findViewById(R.id.c_location);
			 holder.c_price = (Text) v.findViewById(R.id.c_price);
			 
			 
			 holder.c_image = (ImageView) v.findViewById(R.id.club_image);
			 holder.one = (ImageView) v.findViewById(R.id.star_one);
			 holder.two = (ImageView) v.findViewById(R.id.star_two);
			 holder.three = (ImageView) v.findViewById(R.id.star_three);
			 holder.four = (ImageView) v.findViewById(R.id.star_four);
			 holder.five = (ImageView) v.findViewById(R.id.star_five);
			// holder.going = (ImageView) v.findViewById(R.id.status_one_icon);
			// holder.may_be = (ImageView) v.findViewById(R.id.status_two_icon);
			// holder.not_going = (ImageView) v.findViewById(R.id.status_three_icon);
			 v.setTag(holder);
			 
		}
		else
			holder = (ViewHolder) v.getTag();
		
		if(club.get_images().size() > 0)
		{
			if(db.is_image_available(club.get_images().get(0).get_id()))
			{
				new Display_Image(c,club.get_images().get(0).get_id(),holder.c_image).execute();

			}
			else
			{
				loader.displayImage(club.get_images().get(0).get_image(), holder.c_image, options, new ImageLoadingListener(){

					@Override
					public void onLoadingStarted(String imageUri, View view) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onLoadingFailed(String imageUri, View view,
							FailReason failReason) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onLoadingComplete(String imageUri, View view,
							Bitmap loadedImage) {
						// TODO Auto-generated method stub
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						loadedImage.compress(Bitmap.CompressFormat.JPEG, 80, baos);
						byte[] b = baos.toByteArray(); 
						String image = Base64.encodeToString(b, Base64.DEFAULT);
						
						long id1 = db.add_images_for_optimization(club.get_images().get(0).get_id(), image);
						if(id1 > 0)
						{
							Log.d("optimized image saved", "optimized image saved");
						}
					}

					@Override
					public void onLoadingCancelled(String imageUri, View view) {
						// TODO Auto-generated method stub
						
					}
					
				});
			}
		}
			else
				holder.c_image.setImageBitmap(BitmapFactory.decodeResource(c.getResources(),
                        R.drawable.default_bg));

		set_text(holder.c_name,club.get_venue_name());
       // set_text(holder.c_location,"");
        for(Event e : C.items.values())
        {
            if(club.get_vd_id().equals(e.get_venue_id()))
            {
                String txt = e.get_event_name();
                if(e.get_event_date() != null  || e.get_event_date() != "")
                {
                    Log.d("date",e.get_event_date());
                    String[] date = C.get_date_as_day_month(e.get_event_date());
                    txt += " / " + date[0]+"-"+date[1].toUpperCase();
                    set_text(holder.c_location,txt);
                    break;
                }

            }
            else
            {
                set_text(holder.c_location,"");
            }
        }
		//set_text(holder.c_location,club.get_venue_name()+"/ "+club.get_venue_area());
		//set_text(holder.c_price,"Cost for two : Rs"+event.get_entry_price()+"/-");
		set_rating(club,new ImageView[]{holder.one,holder.two,holder.three,holder.four,holder.five});
		
	
		
		return v;

	}

	private void set_rating(V_Details c,ImageView[] stars)
	{
		int[] star = C.set_rating(c.get_rating());
		stars[0].setBackgroundResource(star[0]);
		stars[1].setBackgroundResource(star[1]);
		stars[2].setBackgroundResource(star[2]);
		stars[3].setBackgroundResource(star[3]);
		stars[4].setBackgroundResource(star[4]);
		
	}
	private void set_text(Text t_view, String txt) {
		// TODO Auto-generated method stub
		t_view.setText(txt);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}


	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
	}


	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

}