package com.dpc.wtp.adapters;

import android.app.SearchManager;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SuggestionsAdapter extends CursorAdapter {
	public SuggestionsAdapter(Context context, Cursor c) {
		super(context, c);
		// TODO Auto-generated constructor stub
	}
	
	

	


	




	@Override
	public void bindView(View arg0, Context arg1, Cursor arg2) {
		// TODO Auto-generated method stub
		if(arg1 != null)
		{
			TextView tv = (TextView) arg0;
	        final int textIndex = arg2.getColumnIndex("text");
	        tv.setText(arg2.getString(textIndex));
	        Log.d("display", arg2.getString(textIndex));
		}
	}

	@Override
	public View newView(Context arg0, Cursor arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = LayoutInflater.from(arg0);
        View v = inflater.inflate(android.R.layout.simple_list_item_1, arg2, false);
        return v;
	}

}
