package com.dpc.wtp.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.dpc.wtp.R;
import com.dpc.wtp.Models.Deals;
import com.dpc.wtp.Models.Guestlist;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.Utils.TextB;

public class Guestlist_Adapter extends BaseAdapter{

	private ArrayList<Guestlist> guest_items;
	private ArrayList<Deals> deals_items;
	private Context c;
	LayoutInflater inflater;
	ViewHolder holder;
	View v;
	private int count = 0;
	private boolean is_deal;
	public Guestlist_Adapter(Context mContext,ArrayList<Guestlist> guestlist,ArrayList<Deals> deals,boolean is_deals) {
		if(!is_deals)
		{
			guest_items = guestlist;
			count = guest_items.size();
		}
		else
		{
			deals_items = deals;
			count = deals_items.size();
		}
		is_deal = is_deals;
		this.c = mContext;
		
	}
	
	
	@Override
	public int getCount() {
		return count;
	}

	@Override
	public Object getItem(int position) {
		if (is_deal)
			return deals_items.get(position);
		else
			return guest_items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	
	private class ViewHolder {
		TextB name,code;
		Text date;

	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		
		
		v = convertView;
		if (v == null) 
		{
			 inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 v = inflater.inflate(R.layout.row_guestlist, null);
			 holder = new ViewHolder();
			 holder.name = (TextB) v.findViewById(R.id.name);
			 holder.date = (Text) v.findViewById(R.id.date);
			 holder.code = (TextB) v.findViewById(R.id.code);
			  v.setTag(holder);
			 
		}
		else
			holder = (ViewHolder) v.getTag();
		
		if(is_deal)
		{
			Deals deal = deals_items.get(position);
			holder.name.setText(deal.get_name());
            holder.date.setText(deal.get_e_date());
			//holder.code.setText(Html.fromHtml("<font color='#BE1E2D'>"+deal.get_unique_id()+" </font> "+deal.get_e_date()));
			String code = deal.get_unique_id();
            if(code == null)
                code = "Pending";
            holder.code.setText(code);
		}
		else
		{
			Guestlist guest = guest_items.get(position);
			if(C.items.size() > 0)
			{
				holder.name.setText(guest.get_name());
				//holder.e_name.setText(C.items.get(e_id).get_event_name());
				//holder.e_name.setVisibility(View.VISIBLE);
				String code = guest.get_code();
				if(code == null)
				code = "Pending";
                holder.code.setText(code);
                if(guest.get_date() != null) {
                    String[] date = C.get_date_as_day_month(guest.get_date());
                    holder.date.setText(date[0] + " - " + date[1]);
                }

			}
			
			
			
		}
		
		
		return v;
	}



}
