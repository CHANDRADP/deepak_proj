package com.dpc.wtp.Models;

public class Guestlist {
    String name = null, guestid = null, eid = null, date = null, code = null;

    public Guestlist(String name, String id, String eid, String date, String code) {
        this.name = name;
        this.guestid = id;
        this.eid = eid;
        this.date = date;
        this.code = code;
    }

    public String get_name() {
        return name;
    }

    public String get_id() {
        return guestid;
    }

    public String get_eid() {
        return eid;
    }

    public String get_date() {
        return date;
    }

    ;

    public String get_code() {
        return code;
    }

    public void set_code(String code) {
        this.code = code;
    }

}
