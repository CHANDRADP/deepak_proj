package com.dpc.wtp.customs;

import android.R.color;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.dpc.wtp.R;
import com.dpc.wtp.Models.Review;
import com.dpc.wtp.Models.V_Details;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.EText;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.activities.App;
import com.dpc.wtp.adapters.Review_Adapter;
import com.dpc.wtp.interfaces.ListViewClickListener;
import com.dpc.wtp.tasks.Display_Image;
import com.dpc.wtp.tasks.Review_Task;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

@SuppressLint("ValidFragment")
public class Review_Dialog extends MyDialog{
	
	private ListView list;
	private String id = null;
	V_Details club = null;
	Review_Adapter adapter;
	private Context c;
	private Net_Detect net;
	public ListViewClickListener list_listener;
		
		public Review_Dialog(Context c) {
		// TODO Auto-generated constructor stub
			this.c = c;
	}

		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			db = new DatabaseHelper(this.getSherlockActivity().getApplicationContext());
			net = new Net_Detect(this.getSherlockActivity().getApplicationContext());
			options = new DisplayImageOptions.Builder()
	    	.showStubImage(R.drawable.default_bg)
	    	.showImageForEmptyUri(R.drawable.default_bg)
	    	.showImageOnFail(R.drawable.default_bg)
	    	.cacheInMemory(true)
	    //	.cacheOnDisc(true)
                    .displayer(new FadeInBitmapDisplayer(5))
	    	.bitmapConfig(Bitmap.Config.RGB_565)
	    	.build();
			Bundle args = this.getArguments();
			if(args != null)
			if(args.containsKey(PARAMS.VD_ID.get_param()))
			{
				this.id = args.getString(PARAMS.VD_ID.get_param());
				if(id != null)
				club = C.clubs.get(id);
			}
		}
		
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			final Dialog dialog = new Dialog(c);
			dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

			dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
         //   Rect displayRectangle = new Rect();
         //   Window window = this.getSherlockActivity().getWindow();
          //  window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
         //   LayoutInflater inflater = (LayoutInflater)getSherlockActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         //   View layout = inflater.inflate(R.layout.review_dialog, null);
          //  layout.setMinimumWidth((int)(displayRectangle.width() * 0.9f));
         //   layout.setMinimumHeight((int)(displayRectangle.height() * 0.5f));
          //  RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            dialog.setContentView(R.layout.review_dialog);

			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(color.transparent));
			dialog.show();
		//	ImageView logo = (ImageView)dialog.findViewById(R.id.logo);
			
		//	Text name = (Text)dialog.findViewById(R.id.name);
			if(club != null)
			{
                if(club.get_images().size() > 0)
				if(db.is_image_available(club.get_images().get(0).get_id()))
				{
				//	new Display_Image(this.getSherlockActivity().getApplicationContext(),club.get_images().get(0).get_id(),logo).execute();
					
				}
				else
				{
					//imageLoader.displayImage(club.get_images().get(0).get_image(), logo, options);
				}
				//name.setText(club.get_venue_name());
			}
			View v = dialog.findViewById(R.id.view);
            v.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
			list = (ListView) dialog.findViewById(R.id.list);
			list.setClickable(false);
            list.setStackFromBottom(true);
			adapter = new Review_Adapter(getSherlockActivity().getApplicationContext(),C.clubs.get(id).get_reviews());
			list.setAdapter(adapter);
			if(adapter.getCount() > 0)
			list.setSelection(adapter.getCount() - 1);
			final EText review = (EText)dialog.findViewById(R.id.review);
            review.requestFocus();
            ((InputMethodManager)getSherlockActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
			Text send = (Text)dialog.findViewById(R.id.send);	
			send.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String txt = review.getText().toString();
					if(txt.length() > 0)
					{
						Review r = new Review(null,App.get_userid(),"You",txt,C.get_date());
						V_Details club = C.clubs.get(id);
						club.get_reviews().add(r);
						C.update_clubs(club);
						adapter.refresh(C.clubs.get(id).get_reviews());
						list.setSelection(adapter.getCount() -1);
						review.setText("");
						
						if(net.isConnectingToInternet())
						{
							new Review_Task(getSherlockActivity().getApplicationContext(),id,txt).execute();
                            //app_tracker.send(new C().generate_analytics(C.CATEGORY.VENUES.get_category(),C.ACTION.ACTION_REVIEW.get_action(),club.get_venue_name()));

                        }
						else
						{
							db.update_user_reviews(id, r.get_review(), r.get_time());
						}
					}
					
						
				}
				
			});
			return dialog;
		}

		

}
