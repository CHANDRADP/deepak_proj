package com.dpc.wtp.tasks;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;

import com.dpc.wtp.JSONParser;
import com.dpc.wtp.Models.Server_Params;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.activities.App;

public class Feedback_Task extends AsyncTask<String, Long, Boolean> {
    private Context cContext;
    private String details;
    public Feedback_Task(Context c,String details) {
        this.cContext = c;
        this.details = details;

    }



    @Override
    protected Boolean doInBackground(String... params) {

        return send_feedback();
    }

    @Override
    protected void onPostExecute(Boolean result) {

        if (!result)
        {
            C.alert(cContext, "Please try after some time");
        }


    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    public boolean send_feedback()
    {
        boolean success = false;
        JSONParser parser = new JSONParser();
        JSONObject user = null;
        try {

            user = parser.makeHttpRequest(C.SERVER_URL, "POST",
                    new Server_Params().get_feedback_params(details));
            if(user.getInt(PARAMS.STATUS.get_param()) > 0)
            {
                success = true;
            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return success;
    }

}