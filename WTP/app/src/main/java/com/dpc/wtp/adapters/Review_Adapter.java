package com.dpc.wtp.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.dpc.wtp.R;
import com.dpc.wtp.Models.Review;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.Utils.TextB;

public class Review_Adapter extends BaseAdapter{

	private ArrayList<Review> items;
	private Context c;
	LayoutInflater inflater;
	ViewHolder holder;
	View v;
	public Review_Adapter(Context mContext,ArrayList<Review> reviews) {
		items = reviews;
		this.c = mContext;
		
	}
	public void refresh(ArrayList<Review> reviews)
	{
		items = reviews;
		
		this.notifyDataSetChanged();
	}
	@Override
	public int getCount() {
		if (items != null)
			return items.size();
		else
			return 0;
	}

	@Override
	public Review getItem(int position) {
		if (items != null)
			return items.get(position);
		else
			return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	
	private class ViewHolder {
		TextB name;
		Text review,time;
		
		
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		
		Review review = items.get(position);
		
		v = convertView;
		if (v == null) 
		{
			 inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 v = inflater.inflate(R.layout.row_review, null);
			 v.setFocusable(false);
			 v.setFocusableInTouchMode(false);
			 holder = new ViewHolder();
			 holder.name = (TextB) v.findViewById(R.id.name);
			 holder.review = (Text) v.findViewById(R.id.review);
			 holder.time = (Text) v.findViewById(R.id.time);
			 v.setTag(holder);
			 
		}
		else
			holder = (ViewHolder) v.getTag();
		
		holder.name.setText(review.get_name());
		holder.review.setText(review.get_review());

		holder.time.setText(C.getDateDifference(C.get_universal_date(review.get_time())));
		
		
		return v;
	}



}
