package com.dpc.wtp.adapters;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.dpc.wtp.Models.Deals;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Models.Images;
import com.dpc.wtp.Models.V_Details;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.activities.Activity_Club_Details;
import com.dpc.wtp.activities.Activity_Event_Details;
import com.dpc.wtp.activities.App;
import com.dpc.wtp.tasks.Display_Image;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

public class ImagePagerAdapter extends PagerAdapter{
    private int type = 1;
	private List<Event> events;
    private List<V_Details> venues;
    private LayoutInflater inflater;
	private Activity cxt;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	private View v;
	private DatabaseHelper db;

    public ImagePagerAdapter(Activity c,ImageLoader loader,DisplayImageOptions option) {
		this.cxt = c;
		this.imageLoader = loader;
		this.options = option;
        inflater = cxt.getLayoutInflater();
        db = new DatabaseHelper(cxt);
	}
    public void set_event_banners(List<Event> items)
    {
        this.events = items;
        type = 1;
    }

    public void set_club_banners(List<V_Details> items)
    {
        this.venues = items;
        type = 2;
    }


	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager) container).removeView((View) object);
	}

	@Override
	public void finishUpdate(View container) {
	}

	@Override
	public int getCount() {
        if (type == 1)
		return this.events.size();
        else if (type == 2)
            return this.venues.size();
        else return 0;
	}

	@Override
	public Object instantiateItem(ViewGroup view, final int position) {
		Images image = new Images();
		v = inflater.inflate(R.layout.slider_item, view, false);
		final ImageView imageView = (ImageView) v.findViewById(R.id.image);
		final ProgressBar spinner = (ProgressBar) v.findViewById(R.id.loading);
        if(type == 1)
        {
            if(events.get(position).get_images().size() > 0)
            image = events.get(position).get_images().get(0);
        }
        else if(type == 2)
        {
            if(venues.get(position).get_images().size() > 0)
            image = venues.get(position).get_images().get(0);
        }

        if(image.get_id() != null && db.is_image_available(image.get_id()))
        {
            new Display_Image(cxt,image.get_id(),imageView).execute();

        }
        else
        {
            imageLoader.displayImage(image.get_image(), imageView, options, new ImageLoadingListener(){

                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    // TODO Auto-generated method stub
                        spinner.setVisibility(View.VISIBLE);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view,
                                            FailReason failReason) {
                    // TODO Auto-generated method stub
                    spinner.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view,
                                              Bitmap loadedImage) {
                    // TODO Auto-generated method stub
                    if(loadedImage != null) {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        loadedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        byte[] b = baos.toByteArray();
                        String image = Base64.encodeToString(b, Base64.DEFAULT);
                        Images img = null;
                        if (type == 1) {
                            img = events.get(position).get_images().get(0);
                        } else if (type == 2) {
                            img = venues.get(position).get_images().get(0);
                        }
                        long id1 = db.add_images_for_optimization(img.get_id(), image);
                        if (id1 > 0) {
                            Log.d("optimized image saved", "optimized image saved");
                        }
                    }
                    spinner.setVisibility(View.GONE);

                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    // TODO Auto-generated method stub
                    spinner.setVisibility(View.GONE);
                }



            });
        }
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(type == 1)
                {
                    Intent i = new Intent(cxt,Activity_Event_Details.class);
                    i.putExtra(C.PARAMS.EVENT_ID.get_param(), events.get(position).get_event_id());
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    cxt.startActivity(i);
                }
                else if(type == 2)
                {
                    Intent i = new Intent(cxt,Activity_Club_Details.class);
                    i.putExtra(C.PARAMS.VD_ID.get_param(), venues.get(position).get_vd_id());
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    cxt.startActivity(i);
                }
            }
        });
		((ViewPager) view).addView(v, 0);
		return v;
	}
	

	@Override
	public boolean isViewFromObject(View view, Object o) {
		return o == view;
	}

	@Override
	public void restoreState(Parcelable state, ClassLoader loader) {
	}

	@Override
	public Parcelable saveState() {
		return null;
	}

	@Override
	public void startUpdate(View container) {
	}

	
	
		
}
