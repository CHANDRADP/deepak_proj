package com.dpc.wtp.customs;

import java.util.Comparator;

import com.dpc.wtp.Models.Artist;
import com.dpc.wtp.Models.Event;

public class Event_Priority_Sorter implements Comparator<Event> {
    public int compare(Event c1, Event c2) {
    	int left = 100,right = 100;
		left = c1.get_priority();
		right = c2.get_priority();
    	 
        return left - right;
    }
}