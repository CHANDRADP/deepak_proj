package com.dpc.wtp.activities;

import java.io.IOException;

import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.customs.VideoControllerView;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.FrameLayout;

public class Activity_VideoPlayer extends MyActivity implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener, VideoControllerView.MediaPlayerControl {

    private SurfaceView videoSurface;
    private MediaPlayer player;
    private VideoControllerView controller;
    private String video_url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	this.getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_videoplayer);
        Intent i = this.getIntent();
        if(i != null)
        {
        	if(i.hasExtra(PARAMS.URL.get_param()))
        	{
        		video_url = i.getStringExtra(PARAMS.URL.get_param());
        	}
        }
        if(video_url == null)
        {
        	show_error();
        	return;
        }
        videoSurface = (SurfaceView) findViewById(R.id.videoSurface);
        SurfaceHolder videoHolder = videoSurface.getHolder();
        videoHolder.addCallback(this);

        player = new MediaPlayer();
        controller = new VideoControllerView(this);
        
        try {
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setDataSource(video_url);//this, Uri.parse(video_url));
            player.setOnPreparedListener(this);
        } catch (IllegalArgumentException e) {
        	show_error();
            e.printStackTrace();
        } catch (SecurityException e) {
        	show_error();
            e.printStackTrace();
        } catch (IllegalStateException e) {
        	show_error();
            e.printStackTrace();
        } catch (IOException e) {
        	show_error();
            e.printStackTrace();
        }

    }
    
    private void show_error()
    {
    	C.alert(getApplicationContext(), "Error in playing video");
    }
    
    @Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if(player != null)
		{
			player.release();
		}
	}

    @Override
    protected void onPause() {
        super.onPause();
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;
    }

    @Override
    protected void onResume() {
        super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        controller.show();
        return false;
    }

    // Implement SurfaceHolder.Callback
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
    	player.setDisplay(holder);
        player.prepareAsync();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        
    }
    // End SurfaceHolder.Callback

    // Implement MediaPlayer.OnPreparedListener
    @Override
    public void onPrepared(MediaPlayer mp) {
        controller.setMediaPlayer(this);
        controller.setAnchorView((FrameLayout) findViewById(R.id.videoSurfaceContainer));
        player.start();
    }
    // End MediaPlayer.OnPreparedListener

    // Implement VideoMediaController.MediaPlayerControl
    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public int getCurrentPosition() {
        return player.getCurrentPosition();
    }

    @Override
    public int getDuration() {
        return player.getDuration();
    }

    @Override
    public boolean isPlaying() {
        return player.isPlaying();
    }

    @Override
    public void pause() {
        player.pause();
    }

    @Override
    public void seekTo(int i) {
        player.seekTo(i);
    }

    @Override
    public void start() {
        player.start();
    }

    @Override
    public boolean isFullScreen() {
        return false;
    }

    @Override
    public void toggleFullScreen() {
        
    }
    // End VideoMediaController.MediaPlayerControl

}
