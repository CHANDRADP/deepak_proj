package com.dpc.wtp.fragments;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;

import com.dpc.wtp.Models.ActiveChatDetails;
import com.dpc.wtp.Models.ShareMsg;
import com.dpc.wtp.Models.WTP_Contact;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.activities.Activity_Share_Chat_Content;
import com.dpc.wtp.activities.App;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class NonActive_Users_Fragment extends Fragment
{

    private ListView list;
    MyAdapter myAdapter ;
    DatabaseHelper db;

    private ArrayList<WTP_Contact> all_users_List = new ArrayList();
    private ArrayList<ActiveChatDetails> activeList = new ArrayList();
    private ArrayList<WTP_Contact> nonactiveList = new ArrayList();
    ArrayList<ShareMsg> counterlist = new ArrayList();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {

        View rootView = inflater.inflate(R.layout.list_view, container, false);
        list = (ListView) rootView.findViewById(R.id.list);

        db = new DatabaseHelper(getActivity());
        activeList = db.get_only_active_users();
        all_users_List = db.get_all_users_dummy();

        for(int j=0;j<=activeList.size()-1;j++)
        {
            ActiveChatDetails active = activeList.get(j);

            for(int p = 0; p<=all_users_List.size()-1;p++)
            {
                WTP_Contact all = all_users_List.get(p);
                String a = all.get_wtp_id();
                int b = active.getUserServerId();
                if(Integer.parseInt(all.get_wtp_id())==(active.getUserServerId()))
                {
                    all_users_List.remove(p);
                }
            }
        }
        Log.d("Result: Non Active Users", all_users_List.toString());
        nonactiveList = all_users_List;



        counterlist.clear();
        for(int p = 0; p<=nonactiveList.size()-1;p++)
        {
            Log.d("P value", Integer.toString(p));
            WTP_Contact contact = nonactiveList.get(p);
            String wtpid  = contact.get_wtp_id();
            int id = Integer.parseInt(wtpid);

            String saved_selected_ids = null;
            List<String> memberarraylist = null;
            int checked = 0;
            if(Activity_Share_Chat_Content.selectedIds.size()>0)
            {
                if(Activity_Share_Chat_Content.selectedIds.containsKey(id))
                {
                    checked=1;
                }

            }

            ShareMsg my_message = new ShareMsg(id,0,checked);// change it to id received from server
            counterlist.add(my_message);
            Log.d("counterlist", counterlist.toString());
        }
        if(counterlist.size()>0)
        {
            list.setAdapter(new MyAdapter(counterlist,getActivity()));
        }

        return rootView;
    }


    class MyAdapter extends BaseAdapter {

        View v;
        LayoutInflater inflater;
        ViewHolder holder;
        private Context c;
        private ArrayList<ShareMsg> mygroupusers;



        public MyAdapter(ArrayList<ShareMsg> mCurrentList, Context mContext) {
            this.mygroupusers = mCurrentList;
            this.c = mContext;

        }


        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            if (mygroupusers != null)
                return mygroupusers.size();
            else
                return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            if (mygroupusers != null)
                return mygroupusers.get(position);
            else
                return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        private class ViewHolder {
            Text txtName;
            CheckBox customCheckBox;



        }

        @Override
        public View getView(final int arg0,final View convertView, ViewGroup arg2)
        {
            // TODO Auto-generated method stub


            final ShareMsg temp = mygroupusers.get(arg0);

            v = convertView;

            if (v == null)
            {
                inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.create_group_single_row, null);
                holder = new ViewHolder();
                holder.txtName = (Text) v.findViewById(R.id.txtName);
                holder.customCheckBox = (CheckBox) v.findViewById(R.id.customCheckBox);

                v.setTag(holder);

            }
            else
            {
                holder = (ViewHolder) v.getTag();
            }


            holder.txtName.setText(temp.getUser_id());

            if(temp.getChecked()==1)
            {
                holder.customCheckBox.setChecked(true);
            }
            else
            {
                holder.customCheckBox.setChecked(false);
            }

            holder.customCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if(isChecked)
                    {
                            Activity_Share_Chat_Content.selectedIds.put(temp.getUser_id(), temp);
                    }
                    else
                    {
                            Activity_Share_Chat_Content.selectedIds.remove(temp.getUser_id());
                    }
                    Set set = Activity_Share_Chat_Content.selectedIds.entrySet();

                    Iterator i = set.iterator();

                    while(i.hasNext()) {
                        Map.Entry me = (Map.Entry)i.next();
                        System.out.print("Non : "+me.getKey() + ": ");
                        System.out.println("Non : "+me.getValue());
                    }

                }
            });




            return v;
        }


    }
}