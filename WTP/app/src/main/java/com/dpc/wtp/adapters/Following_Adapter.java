package com.dpc.wtp.adapters;


import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.dpc.wtp.R;
import com.dpc.wtp.Models.Artist;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.tasks.Display_Image;
import com.dpc.wtp.tasks.Following_Task;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class Following_Adapter extends BaseAdapter {
	LayoutInflater inflater;
	ViewHolder holder;
	View v;
	List<Artist> items = new ArrayList<Artist>();
	Context c;
	ImageLoader loader;
	private Net_Detect net;
	DatabaseHelper db;
	DisplayImageOptions options;
	private class ViewHolder {
		Text name;
		ImageView user_pick,chack_box;
		
		
	}
	public Following_Adapter(Context c,List<Artist> l,ImageLoader loader,DisplayImageOptions options)
	{
		this.c = c;
		this.items = l;
		this.loader = loader;
		this.options = options;
		db = new DatabaseHelper(c);
		net = new Net_Detect(this.c);
	}
	
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		Artist artist = items.get(position);
		
		v = convertView;
		if (v == null) 
		{
			 inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 v = inflater.inflate(R.layout.row_friends, null);
			 holder = new ViewHolder();
			 holder.name = (Text) v.findViewById(R.id.user_name);
			 holder.user_pick = (ImageView) v.findViewById(R.id.user_pick);
			 holder.chack_box = (ImageView) v.findViewById(R.id.check_box);
			 v.setTag(holder);
			 
		}
		else
			holder = (ViewHolder) v.getTag();
		
		
		holder.name.setText(artist.get_name());
        if(artist.get_images().size() > 0) {
            if (db.is_image_available(artist.get_images().get(0).get_id())) {
                new Display_Image(c, artist.get_images().get(0).get_id(), holder.user_pick).execute();

            } else {
                loader.displayImage(artist.get_images().get(0).get_image(), holder.user_pick, options);
            }
        }
        else
        {
            holder.user_pick.setImageDrawable(c.getResources().getDrawable(R.drawable.artist));
        }
		set_favorite(holder.chack_box,artist.is_following());
		holder.chack_box.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				
				ImageView img = (ImageView)v;
				
				items.get(position).set_following(items.get(position).is_following() ? "-1" : "1");
				
				set_favorite(img,items.get(position).is_following());
				
				int count = Integer.parseInt(items.get(position).get_following_count()) + (items.get(position).is_following() ? 1 : -1);
				items.get(position).set_following_count(String.valueOf(count));
				
				if(net.isConnectingToInternet())
				{
					
					new Following_Task(c.getApplicationContext(),items.get(position).get_id(),items.get(position).is_following()).execute();
					
					
				}
				else
				{
					int following = 1;
					Artist a = items.get(position);
					if(a.is_following())
						following = -1;
					
					db.update_artist_follow(items.get(position).get_id(), following);
				}
			}
			
		});
		
		
		
		return v;

	}

	private void set_favorite(ImageView img,boolean following)
	{
		if(!following)
		{
			img.setBackgroundResource(R.drawable.favorite_n);
		}
		else
		{
			img.setBackgroundResource(R.drawable.favorite_p);
		}
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}


	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
	}


	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

}