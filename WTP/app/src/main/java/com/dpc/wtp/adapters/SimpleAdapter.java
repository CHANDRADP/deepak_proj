package com.dpc.wtp.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.dpc.wtp.R;
import com.dpc.wtp.Utils.Text;

public class SimpleAdapter extends BaseAdapter{

	private ArrayList<String> items;
	private Context c;
	LayoutInflater inflater;
	ViewHolder holder;
	View v;
	public SimpleAdapter(ArrayList<String> mCurrentList, Context mContext) {
		this.items = mCurrentList;
		this.c = mContext;
		
	}

	@Override
	public int getCount() {
		if (items != null)
			return items.size();
		else
			return 0;
	}

	@Override
	public String getItem(int position) {
		if (items != null)
			return items.get(position);
		else
			return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	
	private class ViewHolder {
		Text name;
		
		
		
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		
		String txt = items.get(position);
		
		v = convertView;
		if (v == null) 
		{
			 inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 v = inflater.inflate(R.layout.dialog_listrow, null);
			 holder = new ViewHolder();
			 holder.name = (Text) v.findViewById(R.id.txt);
			 v.setTag(holder);
			 
		}
		else
			holder = (ViewHolder) v.getTag();
		
		holder.name.setText(txt);
		
		return v;
	}



}
