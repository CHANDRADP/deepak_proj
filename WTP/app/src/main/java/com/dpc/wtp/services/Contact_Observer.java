package com.dpc.wtp.services;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.util.Log;

import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.tasks.Update_newuser;

public class Contact_Observer extends ContentObserver
{
 	private  Context cxt;
 	private String name = null;
 	private String phone = null;
 	DatabaseHelper h;
  public Contact_Observer(Handler handler, Context cxt)
  {
    super(handler);

    this.cxt = cxt;
    
  }

  /**
   * This function is fired when a change occurs on the contacts table
   *
   * @param selfChange
   */
  @Override
  public void onChange(boolean selfChange)
  {
	
	  String columns[] = new String[]{ Phone._ID,Phone.NUMBER, Phone.DISPLAY_NAME, };
	  Cursor c = cxt.getContentResolver().query(Phone.CONTENT_URI, columns,null, null, Phone._ID+" DESC");
	  if (c != null) 
	  {
          if (c.moveToFirst()) 
          {
        	  name = c.getString(c.getColumnIndex(Phone.DISPLAY_NAME));
        	  Log.i(name, name);
        	  phone = c.getString(c.getColumnIndex(Phone.NUMBER));
        	 
        	  if (phone != null ) 
				{
					if(phone.length() > 9 && phone.length() < 20)
					{
						String num = null;
						for ( int i = 0; i < phone.length(); ++i ) 
						{  
							char cha = phone.charAt( i );
							if(i == 0)
							{
								int chacode = cha;
								if(chacode == 43 || Character.isDigit(cha))
								{
									num += cha;
								}
							}
							else if(Character.isDigit(cha))
							{
								num += cha;
							}
						}
						phone = num;
					if(phone.length() >= 10)
						{
							if(phone.length() > 10)
							{
								phone = phone.substring(phone.length()-10);
								
							}
							
							h = new DatabaseHelper(cxt);
			        		  if(!h.is_user_exist(phone))
			        		  {
			        			  h = new DatabaseHelper(cxt);
			          			  h.add_new_android_contact(phone);
			        			  new Update_newuser(cxt,phone).execute();
			        		  }
						}
				}
				
				}
          }
         
		
		    c.close();
	  }
   
 
  }
  

  
  
}