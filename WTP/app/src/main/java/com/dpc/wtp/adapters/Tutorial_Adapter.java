package com.dpc.wtp.adapters;

import android.app.Activity;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dpc.wtp.R;

public class Tutorial_Adapter extends PagerAdapter{

	View v;
	ViewHolder holder;
	ImageView imageView;
	Activity c;
	LayoutInflater inflater;
	int[] images = new int[]{R.drawable.t1,R.drawable.t2,R.drawable.t3,R.drawable.t4,R.drawable.t5};
	public Tutorial_Adapter(Activity c) {
		this.c = c;
		
		inflater = c.getLayoutInflater();
	}
	private class ViewHolder {
		ImageView image;
		
		
	}
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager) container).removeView((View) object);
	}

	@Override
	public void finishUpdate(View container) {
	}

	@Override
	public int getCount() {
		return images.length;
	}

	@Override
	public Object instantiateItem(ViewGroup view, final int position) {
		
		v = inflater.inflate(R.layout.tutorial_page_item, view, false);
		imageView = (ImageView) v.findViewById(R.id.image);
		imageView.setBackgroundResource(images[position]);
		
		((ViewPager) view).addView(v, 0);
		return v;
	}
	

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view.equals(object);
	}

	@Override
	public void restoreState(Parcelable state, ClassLoader loader) {
	}

	@Override
	public Parcelable saveState() {
		return null;
	}

	@Override
	public void startUpdate(View container) {
	}

	
	
		
}
