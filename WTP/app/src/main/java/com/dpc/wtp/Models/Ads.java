package com.dpc.wtp.Models;

/**
 * Created by D-P-C on 12/01/2015.
 */
public class Ads {
    private String id,name,url;
    private int type;

    public Ads(String id, String name, String url, int type) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
