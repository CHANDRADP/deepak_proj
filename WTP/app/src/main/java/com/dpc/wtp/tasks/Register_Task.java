package com.dpc.wtp.tasks;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;

import com.dpc.wtp.JSONParser;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.METHOD;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.activities.App;
import com.google.android.gcm.GCMRegistrar;

public class Register_Task extends AsyncTask<String,String,String>
{
	private Context c;
	private String gcm_id;
	JSONParser jParser = new JSONParser();
	JSONObject json = null;
	ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
	
	public Register_Task(Context c,String gcm_id)
	{
		this.c = c;
		this.gcm_id = gcm_id;
		
	}
	@Override
	protected void onPreExecute() {
		
        
    }
	@Override
	protected String doInBackground(String... arg0) {
		// TODO Auto-generated method stub
		params.add(new BasicNameValuePair(METHOD.UPDATE_GCMID.get_method(), " "));
		params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(), App.get_userid()));
        params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(), App.get_token()));
		params.add(new BasicNameValuePair(PARAMS.GCM_ID.get_param(),gcm_id));
		json = jParser.makeHttpRequest(C.SERVER_URL, "POST", params);
		GCMRegistrar.setRegisteredOnServer(c, true);
		return null;
	}
	@Override
	protected void onProgressUpdate(String... values) {
		// TODO Auto-generated method stub
		super.onProgressUpdate(values);
	}
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);

        if(json != null)
            if(json.has(PARAMS.STATUS.get_param()))
                try {
                    if(json.getInt(PARAMS.STATUS.get_param()) > 0)
                    {
                        App.gcm_updated(true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
    }
}
