package com.dpc.wtp.activities;

import java.io.File;
import java.util.HashMap;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Color;
import android.os.Environment;
import android.preference.PreferenceManager;

import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.tasks.Update_App_version;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

public class App extends Application
{
	public static int profile_bg_color;
	public static int profile_current_color;
	private static SharedPreferences app_data;
	private final static String SHARED_FB = "shared_fb";
    private final static String GCM_ID = "gcm_id";
	private final static String FB_LOGIN = "fb_login";
	private final static String SHARED_TWIT = "shared_twit";
	private final static String REGISTERED = "register";
    private final static String UPDATE_GCM = "update_gcm";
    private final static String VERSION = "version";
	private final static String EXIT = "exit";
	private final static String SERVICE_RUNNING = "service_running";
    public static final String KEY_USER_ID = "member_id";



    public static final String KEY_COUNT_CHANGE_FLAG = "count_change_flag";
    public static final String MAINFOLDER = "/WTP",APP_FOLDER = Environment.getExternalStorageDirectory().getAbsolutePath() + MAINFOLDER;
    public static final String MEDIAFOLDER = Environment.getExternalStorageDirectory().getAbsolutePath() + "/WTP/ChatMedia";
    public enum WTP_Tracker {
        WTP_TRACKER,
    }
    HashMap<WTP_Tracker, Tracker> mTrackers = new HashMap<WTP_Tracker, Tracker>();

   public synchronized Tracker getTracker(WTP_Tracker trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);

            Tracker t = analytics.newTracker(getResources().getString(R.string.ga_trackingId));
            //t.set("&uid", user.getId());
            mTrackers.put(trackerId, t);

        }
        return mTrackers.get(trackerId);
    }

	@Override
	public void onCreate() {
		

		super.onCreate();
		File folder = new File(APP_FOLDER);
		if (!folder.exists()) 
		{
			folder.mkdir();
		}
        File mediafolder = new File(MEDIAFOLDER);
        if (!mediafolder.exists())
        {
            mediafolder.mkdir();
        }
		app_data = PreferenceManager.getDefaultSharedPreferences(this);
		initImageLoader(getApplicationContext());
        if(App.get_app_version() == null || !App.get_app_version().equals(App.get_version_from_manifest(this.getApplicationContext())))
        {
            new Update_App_version(this.getApplicationContext()).execute();
        }
	}
    public static String get_version_from_manifest(Context context)
    {
        String version = "1.0";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = String.valueOf(pInfo.versionName);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }
	
	public static void initImageLoader(Context context) {
		// This configuration tuning is custom. You can tune every option, you may tune some of them,
		// or you can create default configuration by
		//  ImageLoaderConfiguration.createDefault(this);
		// method.
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
				.threadPriority(Thread.NORM_PRIORITY - 2)
				.denyCacheImageMultipleSizesInMemory()
				.discCacheFileNameGenerator(new Md5FileNameGenerator())
				.tasksProcessingOrder(QueueProcessingType.FIFO)
				.discCacheExtraOptions(300, 200, CompressFormat.JPEG, 75, null)
				.build();
		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);
	}
	///////////////////////////////////////////////////////
	public static void clear_data()
	{
		Editor editor = app_data.edit();
		editor.clear();
		editor.commit();
	}

    public static  void gcm_updated(boolean update)
    {
        Editor editor = app_data.edit();
        editor.putBoolean(UPDATE_GCM, update);
        editor.commit();
    }

    public static  boolean is_gcm_updated()
    {
        return app_data.getBoolean(UPDATE_GCM, false);
    }
	
	public static  void set_registered(boolean shared)
	  {
		  Editor editor = app_data.edit();
		  editor.putBoolean(REGISTERED, shared);
		  editor.commit();
	  }
    public static void set_version(Context context)
    {
        Editor editor = app_data.edit();
        editor.putString(VERSION, App.get_version_from_manifest(context));
        editor.commit();
    }
    public static String get_app_version()
    {
        return app_data.getString(VERSION, null);
    }
	  public static boolean is_fb_loggedin()
	  {
		  return app_data.getBoolean(App.FB_LOGIN, false);
	  }
	  public static void set_fb_login(boolean login)
	  {
		  Editor editor = app_data.edit();
		  editor.putBoolean(FB_LOGIN, login);
		  editor.commit();
	  }
	  public static boolean is_registered()
	  {
		  return app_data.getBoolean(REGISTERED, false);
	  }
	public static  void set_shared_onfb(boolean shared)
	  {
		  Editor editor = app_data.edit();
		  editor.putBoolean(SHARED_FB, shared);
		  editor.commit();
	  }
	  public static boolean is_shared_onfb()
	  {
		  return app_data.getBoolean(SHARED_FB, false);
	  }
	  public static  void set_shared_ontwitter(boolean shared)
	  {
		  Editor editor = app_data.edit();
		  editor.putBoolean(SHARED_TWIT, shared);
		  editor.commit();
	  }
	  public static boolean is_shared_ontwitter()
	  {
		  return app_data.getBoolean(SHARED_TWIT, false);
	  }
	  public static  void set_exit(boolean exit)
	  {
		  Editor editor = app_data.edit();
		  editor.putBoolean(EXIT, exit);
		  editor.commit();
	  }
	  public static boolean is_closed()
	  {
		  return app_data.getBoolean(EXIT, false);
	  }
	public static  void set_contacts_updated(boolean updated)
	  {
		  Editor editor = app_data.edit();
		  editor.putBoolean(C.PARAMS.CONTACT_INFO.get_param(), updated);
		  editor.commit();
	  }
	  public static boolean is_contacts_updated()
	  {
		  return app_data.getBoolean(C.PARAMS.CONTACT_INFO.get_param(), false);
	  }
	public static  void set_userid(String id)
	  {
		  Editor editor = app_data.edit();
		  editor.putString(C.PARAMS.USER_ID.get_param(), id);
		  editor.commit();
	  }
	  public static String get_userid()/////////////////////////////////////////////////////////////
	  {
          return "2";
		  //return app_data.getString(C.PARAMS.USER_ID.get_param(), null);
          //return "8";
	  }

    public static  void update_gcmid(String id)
    {
        Editor editor = app_data.edit();
        editor.putString(GCM_ID, id);
        editor.commit();
    }
    public static String get_gcmid()
    {
        return app_data.getString(GCM_ID, null);
    }
	  
	  public static  void set_gender(String gender)
	  {
		  Editor editor = app_data.edit();
		  editor.putString(C.PARAMS.GENDER.get_param(), gender);
		  editor.commit();
	  }
	  public static String get_gender()
	  {
		  return app_data.getString(C.PARAMS.GENDER.get_param(), null);
	  }

    public static  void set_token(String token)
    {
        Editor editor = app_data.edit();
        editor.putString(C.PARAMS.TOKEN.get_param(), token);
        editor.commit();
    }
    public static String get_token()
    {
        return app_data.getString(C.PARAMS.TOKEN.get_param(), null);
    }
	  
	  public static  void set_user_details(String id,String name,String email,String c_code,String number,String image,String thumbnail,String points,String token,boolean tutor)
	  {
		  Editor editor = app_data.edit();
		  editor.putString(C.PARAMS.USER_ID.get_param(), id);
		  editor.putString(C.PARAMS.USER_NAME.get_param(), name);
		  editor.putString(C.PARAMS.EMAIL.get_param(), email);
		  editor.putString(C.PARAMS.C_CODE.get_param(), c_code);
		  editor.putString(C.PARAMS.NUMBER.get_param(), number);
		  editor.putString(C.PARAMS.IMAGE.get_param(), image);
		  editor.putString(C.PARAMS.THUMBNAIL.get_param(), thumbnail);
		  
		  editor.putInt(C.PARAMS.POINTS.get_param(), Integer.parseInt(points));
		  editor.putString(C.PARAMS.TOKEN.get_param(), token);
		  
		  editor.putBoolean(C.PARAMS.TUTORIAL.get_param(), tutor);
		  
		  editor.commit();
	  }
	  
	  public static  void set_image(String image)
	  {
		  Editor editor = app_data.edit();
		  editor.putString(C.PARAMS.IMAGE.get_param(), image);
		  editor.commit();
	  }


	  public static  void set_thumbnail(String thumbnail)
	  {
		  Editor editor = app_data.edit();
		  editor.putString(C.PARAMS.THUMBNAIL.get_param(), thumbnail);
		  editor.commit();
	  }


	  
	  public static String get_username()
	  {
		  return app_data.getString(C.PARAMS.USER_NAME.get_param(), null);
	  }
	  
	  public static String get_useremail()
	  {
		  return app_data.getString(C.PARAMS.EMAIL.get_param(), null);
	  }
	  
	  public static String get_c_code()
	  {
		  return app_data.getString(C.PARAMS.C_CODE.get_param(), null);
	  }
	  
	  public static String get_number()
	  {
		  return app_data.getString(C.PARAMS.NUMBER.get_param(), null);
	  }
	  
	  public static String get_image()
	  {
		  return app_data.getString(C.PARAMS.IMAGE.get_param(), null);
	  }
	  
	  public static String get_thumbnail()
	  {
		  return app_data.getString(C.PARAMS.THUMBNAIL.get_param(), null);
	  }
	  
	  public static int get_userpoints()
	  {
		  return app_data.getInt(C.PARAMS.POINTS.get_param(), 0);
	  }
	  ///////////////////////
	  public static  void update_userpoints(String points)
	  {
		  Editor editor = app_data.edit();
		  editor.putInt(C.PARAMS.POINTS.get_param(), Integer.parseInt(points));
		  editor.commit();
	  }
	  public static  void set_fbid(String id)
	  {
		  Editor editor = app_data.edit();
		  editor.putString(C.PARAMS.FACEBOOK_ID.get_param(), id);
		  editor.commit();
	  }
	  public static String get_fbid()
	  {
		  return app_data.getString(C.PARAMS.FACEBOOK_ID.get_param(), null);
	  }
	  public static  void set_twitid(String id)
	  {
		  Editor editor = app_data.edit();
		  editor.putString(C.PARAMS.TWITTER_ID.get_param(), id);
		  editor.commit();
	  }
	  public static String get_twitid()
	  {
		  return app_data.getString(C.PARAMS.TWITTER_ID.get_param(), null);
	  }
	  public static  void set_city(String id)
	  {
		  Editor editor = app_data.edit();
		  editor.putString(C.PARAMS.CITY.get_param(), id);
		  editor.commit();
	  }
	  public static String get_city()
	  {
		  return app_data.getString(C.PARAMS.CITY.get_param(), null);
	  }
	  
	  public static  void set_servicerunning(boolean running)
	  {
		  Editor editor = app_data.edit();
		  editor.putBoolean(App.SERVICE_RUNNING, running);
		  editor.commit();
	  }
	  public static boolean is_service_running()
	  {
		  return app_data.getBoolean(App.SERVICE_RUNNING, false);
	  }
	  
	  ///////////////////////////////
	  public static  void set_tutorial(boolean tutorial)
	  {
		  Editor editor = app_data.edit();
		  editor.putBoolean(C.PARAMS.TUTORIAL.get_param(), tutorial);
		  editor.commit();
	  }
	  public static boolean is_tutorial_finished()
	  {
		  return app_data.getBoolean(C.PARAMS.TUTORIAL.get_param(), false);
	  }

    public static String get_lati()
    {
        return app_data.getString("lati", null);
    }

    public static  void update_lati(String id)
    {
        Editor editor = app_data.edit();
        editor.putString("lati", id);
        editor.commit();
    }
    public static String get_long()
    {
        return app_data.getString("long", null);
    }

    public static  void update_long(String id)
    {
        Editor editor = app_data.edit();
        editor.putString("long", id);
        editor.commit();
    }

    //********************************************************//
    // Functions related to chat

    public static void saveUser(String id)
    {
        Editor editor = app_data.edit();
        editor.putString(KEY_USER_ID, id);
        editor.commit();
    }

    public static void saveCountChangeFlag(String flag)
    {
        Editor editor = app_data.edit();
        editor.putString(KEY_COUNT_CHANGE_FLAG, flag);
        editor.commit();
    }

    public static HashMap<String, String> getUser()
    {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_USER_ID, app_data.getString(KEY_USER_ID, null));
        return user;
    }



    public static void deleteUser()
    {
        Editor editor = app_data.edit();
        editor.remove(KEY_USER_ID);
        editor.commit();
    }



    public static void deleteCountChangeFlag()
    {
        Editor editor = app_data.edit();
        editor.remove(KEY_COUNT_CHANGE_FLAG);
        editor.commit();
    }
	  
}
