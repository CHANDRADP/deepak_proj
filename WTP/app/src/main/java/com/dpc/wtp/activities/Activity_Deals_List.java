package com.dpc.wtp.activities;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.actionbarsherlock.view.MenuItem;
import com.dpc.wtp.R;
import com.dpc.wtp.Models.Deals;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.adapters.Club_Deal_Adapter;
import com.dpc.wtp.customs.MyActivity;
import com.facebook.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

public class Activity_Deals_List extends MyActivity{
	
	private Intent i;
	
	private String vd_id = null,type = null;
	
	private ListView list;
	
	private ArrayList<Deals> list_items = new ArrayList<Deals>();
	
	private Club_Deal_Adapter list_adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.actionbar);
		final Text title = (Text)findViewById(android.R.id.title);
		
		super.onCreate(savedInstanceState);
        app_tracker.setScreenName(C.SCREEN.SCREEN_DEAL_DETAILS.get_screen());
        app_tracker.send(new HitBuilders.AppViewBuilder().build());
		this.setContentView(R.layout.ac_settings);
        new C().blur_it(this);
		list = (ListView)this.findViewById(R.id.list);
        list.setDivider(null);
        list.setDividerHeight((int) getResources().getDimension(R.dimen.all_margin));
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );
        int margin = (int)getResources().getDimension(R.dimen.ten_dp);
        params.setMargins(margin, margin, margin, margin);
        list.setLayoutParams(params);
		options = new DisplayImageOptions.Builder()
    	.showStubImage(R.drawable.default_bg)
    	.showImageForEmptyUri(R.drawable.default_bg)
    	.showImageOnFail(R.drawable.default_bg)
    	.cacheInMemory(true)
    //	.cacheOnDisc(true)
    	.bitmapConfig(Bitmap.Config.RGB_565)
    	.build();
		i = this.getIntent();
		if(i.hasExtra(PARAMS.TYPE.get_param()))
		{
			type = i.getStringExtra(PARAMS.TYPE.get_param());
		}
        if(i.hasExtra(PARAMS.VD_ID.get_param()))
        {
            vd_id = i.getStringExtra(PARAMS.VD_ID.get_param());
        }
		title.setText(C.clubs.get(vd_id).get_venue_name());
		list.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int posi,
					long arg3) {
			//	Intent i = new Intent(getApplicationContext(),Activity_Deals_Details.class);
			//		i.putExtra(PARAMS.VD_ID.get_param(), list_items.get(posi).get_id());
			//		startActivity(i);
				
			}
			
		});
        new Update().execute();

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		AppEventsLogger.deactivateApp(this,getResources().getString(R.string.fb_app_id));
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;
		
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
		AppEventsLogger.activateApp(this,getResources().getString(R.string.fb_app_id));
		

	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId())
		{
	
		case android.R.id.home:
			super.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
    }
///////////////////////////////
public class Update extends AsyncTask<String, Long, Boolean> {
		
		@Override
		protected Boolean doInBackground(String... params) {
			
			get_clubs_deals();
			return null;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			
			update_list();
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
		}
}

	private void get_clubs_deals()
	{
        list_items = C.deals.get(vd_id);

	}
	
	private void update_list()
	{
		list_adapter = new Club_Deal_Adapter(this,list_items,imageLoader,options,app_tracker);
		list.setAdapter(list_adapter);
	}
}
