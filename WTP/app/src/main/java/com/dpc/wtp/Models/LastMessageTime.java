package com.dpc.wtp.Models;

public class LastMessageTime
{
    private String message, time;

    public LastMessageTime(String dbindex, String conversation_id) {
        this.message = dbindex;
        this.time = conversation_id;

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
