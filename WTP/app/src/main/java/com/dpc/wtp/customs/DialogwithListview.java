package com.dpc.wtp.customs;

import java.util.ArrayList;

import android.R.color;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.adapters.SimpleAdapter;
import com.dpc.wtp.interfaces.ListViewClickListener;


@SuppressLint({ "NewApi", "ValidFragment" })
public class DialogwithListview extends SherlockDialogFragment{
ListView list;
ArrayList<String> values = new ArrayList<String>();
public ListViewClickListener list_listener;
String header;
	@SuppressLint("ValidFragment")
	public DialogwithListview(ArrayList<String> list_values)
	{
		this.values = list_values;
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Bundle args = this.getArguments();
		if(args != null)
		if(args.containsKey("head"))
		{
			this.header = args.getString("head");
		}
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		final Dialog dialog = new Dialog(getActivity());
		dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		dialog.setContentView(R.layout.dialog_with_list);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(color.transparent));
		dialog.show();
		Text head = (Text)dialog.findViewById(R.id.header);
		head.setText(header);
		list = (ListView) dialog.findViewById(R.id.list);
		list.setAdapter(new SimpleAdapter(values,getActivity().getApplicationContext()));
		list.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int posi,
					long arg3) {
				list_listener.setOnSubmitListener(posi);
				dismiss();
			}
			
		});
		
		return dialog;
	}

	

}

