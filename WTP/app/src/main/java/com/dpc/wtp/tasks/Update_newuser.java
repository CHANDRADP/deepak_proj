package com.dpc.wtp.tasks;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;

import com.dpc.wtp.JSONParser;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.activities.App;

public class Update_newuser extends AsyncTask <Void, Void, Boolean>{

	String result = null,number = null;
	DatabaseHelper h;
	Context c;
	public Update_newuser(Context c,String number)
	{
		this.c = c;
		this.number = number;
	}
    @Override
    protected Boolean doInBackground(Void... unsued) {
    	
    	
    	return update();
    
    }

	
	
    
	
	boolean update()
	{
		boolean success = false;
		JSONParser parser = new JSONParser();
			JSONObject user;
			h = new DatabaseHelper(c);
			
			ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(C.METHOD.GET_APP_USERS.get_method()," "));
			
			params.add(new BasicNameValuePair(PARAMS.NUMBER.get_param()+"[]",number));
			params.add(new BasicNameValuePair(C.PARAMS.USER_ID.get_param(),App.get_userid()));
			params.add(new BasicNameValuePair(C.PARAMS.TOKEN_STR.get_param(),App.get_token()));
			
			user = parser.makeHttpRequest(C.SERVER_URL, "POST", params);
			if(user != null)
	    		{
	    			try {
						if(user.getInt(PARAMS.STATUS.get_param()) > 0)
						{
							success = true;
							C.update_users(c,user);
							
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	    		}
				
		
		
		return success;
		
	}
	
}
