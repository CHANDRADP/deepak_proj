package com.dpc.wtp.Models;

public class Tracks {
private String id,caption,url;
private int type;
public Tracks(String id,int type,String caption,String url)
{
	this.id = id;
	this.type = type;
	this.caption = caption;
	this.url = url;
}

public String get_id()
{
	return id;
	}
public int get_type()
{
	return type;
	}
public String get_caption()
{
	return caption;
	}
public String get_url()
{
	return url;
	}
}
