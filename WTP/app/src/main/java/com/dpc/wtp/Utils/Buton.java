package com.dpc.wtp.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class Buton extends Button{
    public Buton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public Buton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Buton(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"Proxima.otf");
        setTypeface(tf);
    }

}
