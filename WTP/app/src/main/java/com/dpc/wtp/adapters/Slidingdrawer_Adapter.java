package com.dpc.wtp.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.dpc.wtp.R;
import com.dpc.wtp.Models.Slide_Menu_Item;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.activities.App;
import com.dpc.wtp.customs.CircleImage;
import com.dpc.wtp.customs.ProgressWheel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

public class Slidingdrawer_Adapter extends BaseAdapter {
	
	private Context context;
	private ArrayList<Slide_Menu_Item> navDrawerItems;
	private ViewHolder holder;
	private ProgressWheel profile_points;
	private CircleImage icon;
	private boolean running;
	private int progress = 0,points = 0;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
	final Runnable r = new Runnable() {
		public void run() {
			running = true;
			double break_points = 0.0;
			int previous = 0;
			if(points <= 1000)
			{
				App.profile_bg_color = context.getResources().getColor(R.color.transparent);
				App.profile_current_color = context.getResources().getColor(R.color.c_one);
				break_points = 1000;
				
			}
			else if(points <= 5000)
			{
				App.profile_bg_color = context.getResources().getColor(R.color.c_one);
				App.profile_current_color = context.getResources().getColor(R.color.c_two);
				break_points = 5000;
				previous = 1000;
			}
			else if(points <= 10000)
			{
				App.profile_bg_color = context.getResources().getColor(R.color.c_two);
				App.profile_current_color = context.getResources().getColor(R.color.c_three);
				break_points = 10000;
				previous = 5000;
			}
			else if(points <= 25000)
			{
				App.profile_bg_color = context.getResources().getColor(R.color.c_three);
				App.profile_current_color = context.getResources().getColor(R.color.c_four);
				break_points = 25000;
				previous = 10000;
			}
			else if(points <= 50000)
			{
				App.profile_bg_color = context.getResources().getColor(R.color.c_four);
				App.profile_current_color = context.getResources().getColor(R.color.c_five);
				break_points = 50000;
				previous = 25000;
			}
			else if(points <= 75000)
			{
				App.profile_bg_color = context.getResources().getColor(R.color.c_five);
				App.profile_current_color = context.getResources().getColor(R.color.c_six);
				break_points = 75000;
				previous = 50000;
			}
			else if(points <= 100000)
			{
				App.profile_bg_color = context.getResources().getColor(R.color.c_six);
				App.profile_current_color = context.getResources().getColor(R.color.c_seven);
				break_points = 100000;
				previous = 75000;
			}
			else if(points <= 150000)
			{
				App.profile_bg_color = context.getResources().getColor(R.color.c_seven);
				App.profile_current_color = context.getResources().getColor(R.color.c_eight);
				break_points = 150000;
				previous = 100000;
			}
			else if(points <= 200000)
			{
				App.profile_bg_color = context.getResources().getColor(R.color.c_eight);
				App.profile_current_color = context.getResources().getColor(R.color.c_nine);
				break_points = 200000;
				previous = 150000;
			}
			else if(points <= 300000)
			{
				App.profile_bg_color = context.getResources().getColor(R.color.c_nine);
				App.profile_current_color = context.getResources().getColor(R.color.c_ten);
				break_points = 300000;
				previous = 200000;
			}
			else if(points <= 400000)
			{
				App.profile_bg_color = context.getResources().getColor(R.color.c_ten);
				App.profile_current_color = context.getResources().getColor(R.color.c_eleven);
				break_points = 400000;
				previous = 300000;
			}
			else if(points <= 500000)
			{
				App.profile_bg_color = context.getResources().getColor(R.color.c_eleven);
				App.profile_current_color = context.getResources().getColor(R.color.c_twelve);
				break_points = 500000;
				previous = 400000;
			}
			profile_points.setRimColor(App.profile_bg_color);
			profile_points.setBarColor(App.profile_current_color);
			profile_points.setTextColor(App.profile_current_color);
			double p_limit = ((360/(break_points-previous)) * (points - previous));
			while(progress < p_limit) 
			{
				
				profile_points.incrementProgress();
				int p = (int) ((progress/(360/(break_points-previous))+previous));
				profile_points.setText(String.valueOf((p)));
				progress++;
				
				try {
					Thread.sleep(2);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			profile_points.setText(String.valueOf((points)));
			running = false;
		}
    };
    
    public void update_profile_points()
    {

        imageLoader.displayImage(App.get_thumbnail(), icon, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {
               // icon.setImageBitmap(new C().getRoundedCornerBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.user)));
            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                if(bitmap != null)
                icon.setImageBitmap(new C().getRoundedCornerBitmap(bitmap));
            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });
   // 	if(App.get_userpoints() != null && App.get_userpoints() != "")
    		points = App.get_userpoints();
    	if(!running) {
			progress = 0;
			profile_points.resetCount();
			Thread s = new Thread(r);
			s.start();
		}
    	
    }
    public void reset_profile_points()
    {
    	progress = 0;
		profile_points.resetCount();
    }
	public Slidingdrawer_Adapter(Context context, ArrayList<Slide_Menu_Item> navDrawerItems,ImageLoader imageLoader){
		this.context = context;
		this.navDrawerItems = navDrawerItems;
        this.imageLoader = imageLoader;
        this.options = new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.user)
                .showImageForEmptyUri(R.drawable.user)
                .showImageOnFail(R.drawable.user)
                .cacheInMemory(true)
//	.cacheOnDisc(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                        //  .displayer(new FadeInBitmapDisplayer(5))
                .build();
		//if(App.get_userpoints() != null && App.get_userpoints() != "")
		points = App.get_userpoints();
	}
	private class ViewHolder {
		ImageView icon;
		
		Text title;
		
		
	}
	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {		
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		
		View v = convertView;
		if(position == 0)
		{
			LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
             v = mInflater.inflate(R.layout.slidingdrawer_user_row, null);
             icon  = (CircleImage)v.findViewById(R.id.icon);
             Text title = (Text) v.findViewById(R.id.title);
            imageLoader.displayImage(App.get_thumbnail(), icon, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {
                    icon.setImageBitmap(new C().getRoundedCornerBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.user)));
                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    if(bitmap != null)
                    icon.setImageBitmap(new C().getRoundedCornerBitmap(bitmap));
                }

                @Override
                public void onLoadingCancelled(String s, View view) {

                }
            });
             title.setText(App.get_username());
             profile_points = (ProgressWheel) v.findViewById(R.id.progress);
		}
		else
		{
			LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
             v = mInflater.inflate(R.layout.slidingdrawer_list_item, null);
			 holder = new ViewHolder();
			 holder.icon = (ImageView) v.findViewById(R.id.icon);
			 holder.title = (Text) v.findViewById(R.id.title);
			
			 holder.icon.setImageResource(navDrawerItems.get(position).getIcon());        
			 holder.title.setText(navDrawerItems.get(position).getTitle());
			 
		}
		
		
		
		
        
        
        
        return v;
	}

}
