package com.dpc.wtp.Utils;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.dpc.wtp.Models.ActiveChatDetails;
import com.dpc.wtp.Models.Artist;
import com.dpc.wtp.Models.ChatMsg;
import com.dpc.wtp.Models.Deals;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Models.GroupDetails;
import com.dpc.wtp.Models.Guestlist;
import com.dpc.wtp.Models.PendingGroupMsg;
import com.dpc.wtp.Models.Review;
import com.dpc.wtp.Models.TextMedia;
import com.dpc.wtp.Models.UploadDetails;
import com.dpc.wtp.Models.V_Details;
import com.dpc.wtp.Models.WTP_Contact;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.activities.App;
import com.dpc.wtp.amazon.UploadService;

public class DatabaseHelper extends SQLiteOpenHelper{

	private Context cxt;

    private Net_Detect net;
	
	private static int DATABASE_VERSION = 1;
	
	public static final String DATABASE_NAME = "WTP_DB_Manager";
	
	private static final String WTP_USERS_TABLE = "wtp_users";
	
	private static final String EVENTS = "Events";
	
	private static final String VENUES = "Venues";
	
	private static final String ARTISTS = "Artists";
	
	private static final String GUEST_LIST = "Guest_List";
	
	private static final String MY_DEALS = "MY_Deals";
	
	private static final String REVIEWS = "Reviews";
	
	private static final String IMAGE_OPTIMIZE = "image_optimize";
	
	private static final String ALL_CONTACTS_TABLE = "all_phone_contacts";
    private static final String TABLE_CONVERSATION = "Conversations";

    private static final String TABLE_CHAT = "Chats";

    private static final String TABLE_GROUP = "Groups";

    private static final String TABLE_GROUPS_PENDING_MSGS = "Groups_Pending_Msgs";





	
	///////////////////////////////////////
	
	private static final String CONTACT_ID = "contact_id";
	
	private static final String CONTACT_DB_ID = "contact_db_id";
	
	///////////////////////////////////
	
	private static final String ID = "db_id";
	
	///////////////////////////////////////////////
	private static final String NAME = "name";
	private static final String WTP_ID = "user_id";
	private static final String C_CODE = "c_code";
	private static final String NUMBER = "phone_number";
	private static final String STATUS = "status";
	private static final String THUMBNAIL = "thumb";
	private static final String UPDATED_AT = "updated";


    // Conversation Table Columns names
    private static final String MSG_ID = "Msg_Id";
    public static final String CHATID = "ChatId";
    public static final String MSG = "Msg";
    public static final String DATETIME = "DateTime";
    public static final String POSTER = "Poster";
    public static final String READSTATUS = "ReadStatus";
    public static final String MESSAGECONTENTTYPE = "Msg_Content_Type";
    public static final String MEDIASTATUS = "Media_Status";
    public static final String DELIVERYSTATUS = "Delivery_Status";
    public static final String SERVERURL = "Server_Url";


    // Chat Table Column names

    private static final String LASTMSG = "Last_Msg";
    private static final String LASTTIME = "Last_Time";
    private static final String UNREADCOUNT = "Unread_Count";
    private static final String ISGROUP = "Is_Group";


    // Group Chat Table Column names
    private static final String GROUPID = "Group_Id";
    private static final String GROUPNAME = "Group_Name";
    private static final String CREATIONDATE = "Creation_Date";
    private static final String ADMIN = "Group_Admin";
    private static final String SERVERGROUPID = "ServerGroupId";
    private static final String GROUPUSERS_IDS = "Group_Users_Ids";

    //Table Groups pending msgs column names

    private static final String PENDING_GROUP_MSGID = "Pending_Group_MsgId";
    private static final String PENDING_GROUP_SERVERGROUPID = "Pending_Group_ServerGroupId";
    private static final String PENDING_GROUP_MEMBERID = "Pending_Group_MemberId";
    private static final String PENDING_GROUP_MESSAGETYPE = "Pending_Group_MessageType";
    private static final String PENDING_GROUP_MESSAGE = "Pending_Group_Message";


    private static int SENT = 7;
    private static int NOT_SENT = 8;
    public static  int UPLOADING = 5;
    public static  int ITS_GROUP_MSG = 1;
    public static  int ITS_NOT_GROUP_MSG = 0;


    ///////////////////////////////////
	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		cxt = context;
        net = new Net_Detect(context);
		
	}
	
	

	@Override
	public void onOpen(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		super.onOpen(db);
		if (!db.isReadOnly()) {
	        // Enable foreign key constraints
	        db.execSQL("PRAGMA foreign_keys=ON;");
	        onCreate(db);
	    }
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		String CREATE_WTP_USERS_TABLE = "CREATE TABLE IF NOT EXISTS " + WTP_USERS_TABLE + "(" + 
				 ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
				 WTP_ID + " INTEGER," +
				 NAME + " TEXT," +
				 C_CODE + " TEXT," +
				 NUMBER + " TEXT," +
				 STATUS + " TEXT," +
				 THUMBNAIL + " TEXT," +
				 UPDATED_AT + " DATETIME," +
				 CONTACT_DB_ID + " INTEGER )" ;
		String CREATE_ALL_CONTACTS_TABLE = "CREATE TABLE  IF NOT EXISTS " + ALL_CONTACTS_TABLE + "(" + 
				 CONTACT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + 
				NUMBER + " TEXT)" ;
		
		String CREATE_EVENTS_TABLE = "CREATE TABLE  IF NOT EXISTS " + DatabaseHelper.EVENTS + "(" + 
				 ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + 
				PARAMS.EVENT_ID.get_param() + " INTEGER," +
				PARAMS.TYPE.get_param() + " INTEGER," +
				PARAMS.ATTEND_TYPE.get_param() + " INTEGER)";
		
		String CREATE_VENUES_TABLE = "CREATE TABLE  IF NOT EXISTS " + DatabaseHelper.VENUES + "(" + 
				 ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + 
				PARAMS.VD_ID.get_param() + " INTEGER," +
				PARAMS.STARS.get_param() + " INTEGER," +
				PARAMS.TYPE.get_param() + " INTEGER)";
		
		String CREATE_ARTISTS_TABLE = "CREATE TABLE  IF NOT EXISTS " + DatabaseHelper.ARTISTS + "(" + 
				 ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + 
				PARAMS.ARTIST_ID.get_param() + " INTEGER," +
				PARAMS.FOLLOWING.get_param() + " INTEGER)";
		
		String CREATE_REVIEWS_TABLE = "CREATE TABLE  IF NOT EXISTS " + DatabaseHelper.REVIEWS + "(" + 
				 ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + 
				PARAMS.VD_ID.get_param() + " INTEGER," +
				PARAMS.REVIEW.get_param() + " TEXT," +
				PARAMS.TIME.get_param() + " TEXT)";
		
		String CREATE_GUEST_LIST_TABLE = "CREATE TABLE  IF NOT EXISTS " + DatabaseHelper.GUEST_LIST + "(" + 
				 ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + 
				PARAMS.EVENT_ID.get_param() + " INTEGER," +
				PARAMS.GUEST_ID.get_param() + " INTEGER," +
                PARAMS.NAME.get_param() + " TEXT," +
				PARAMS.DATE.get_param() + " TEXT," +
				PARAMS.UNIQUE_CODE.get_param() + " TEXT)";
		
		String CREATE_DEALS_TABLE = "CREATE TABLE  IF NOT EXISTS " + DatabaseHelper.MY_DEALS + "(" + 
				 ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + 
				PARAMS.ID.get_param() + " INTEGER," +
				PARAMS.VD_ID.get_param() + " INTEGER," +
                PARAMS.NAME.get_param() + " TEXT," +
				PARAMS.UNIQUE_CODE.get_param() + " TEXT," +
				PARAMS.DATE.get_param() + " TEXT)";
		
		String CREATE_IMAGE_OPTIMTZE_TABLE = "CREATE TABLE  IF NOT EXISTS " + DatabaseHelper.IMAGE_OPTIMIZE + "(" + 
				PARAMS.IMAGE_ID.get_param() + " INTEGER," +
				PARAMS.IMAGE.get_param() + " TEXT)";

        String CREATE_CHAT_TABLE=
                "CREATE TABLE  IF NOT EXISTS "+ TABLE_CHAT +" ("
                        +CHATID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
                        +WTP_ID +" INTEGER,"
                        +LASTMSG +" TEXT ,"
                        +LASTTIME +" TEXT ,"
                        +ISGROUP +" INTEGER ,"
                        +UNREADCOUNT +" INTEGER );";

        String CREATE_CONVERSATION_TABLE = "CREATE TABLE  IF NOT EXISTS " + DatabaseHelper.TABLE_CONVERSATION+" ("+
                MSG_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"+
                POSTER +" INTEGER ,"+
                MSG +" TEXT ,"+
                DATETIME +" TEXT ,"+
                READSTATUS +" INTEGER ,"+
                CHATID +" INTEGER ," +
                MESSAGECONTENTTYPE +" INTEGER ," +
                MEDIASTATUS +" INTEGER ," +
                DELIVERYSTATUS +" INTEGER ," +
                SERVERURL +" TEXT ," +
                "FOREIGN KEY ("+CHATID+") REFERENCES "+TABLE_CHAT+" ("+CHATID+") ON DELETE CASCADE );";


        String CREATE_GROUP_TABLE = "CREATE TABLE  IF NOT EXISTS " + DatabaseHelper.TABLE_GROUP+" ("+
                GROUPID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"+
                GROUPNAME +" TEXT ,"+
                CREATIONDATE +" TEXT ,"+
                ADMIN +" INTEGER ,"+
                SERVERGROUPID +" INTEGER ,"+
                GROUPUSERS_IDS +" TEXT  );";


        String CREATE_GROUP_PENDING_MSG_TABLE = "CREATE TABLE  IF NOT EXISTS " + DatabaseHelper.TABLE_GROUPS_PENDING_MSGS+" ("+
                PENDING_GROUP_MSGID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"+
                PENDING_GROUP_MEMBERID +" INTEGER ,"+
                PENDING_GROUP_MESSAGE +" TEXT ,"+
                PENDING_GROUP_MESSAGETYPE +" INTEGER ,"+
                PENDING_GROUP_SERVERGROUPID +" INTEGER );";




        db.execSQL(CREATE_WTP_USERS_TABLE);
		db.execSQL(CREATE_ALL_CONTACTS_TABLE);
		
		db.execSQL(CREATE_EVENTS_TABLE);
		db.execSQL(CREATE_VENUES_TABLE);
		db.execSQL(CREATE_ARTISTS_TABLE);
		db.execSQL(CREATE_REVIEWS_TABLE);
		db.execSQL(CREATE_GUEST_LIST_TABLE);
		db.execSQL(CREATE_DEALS_TABLE);
		db.execSQL(CREATE_IMAGE_OPTIMTZE_TABLE);
        db.execSQL(CREATE_CHAT_TABLE);
        db.execSQL(CREATE_CONVERSATION_TABLE);
        db.execSQL(CREATE_GROUP_TABLE);
        db.execSQL(CREATE_GROUP_PENDING_MSG_TABLE);



    }

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + WTP_USERS_TABLE);
		db.execSQL("DROP TABLE IF EXISTS " + ALL_CONTACTS_TABLE);
		
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.EVENTS);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.VENUES);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.ARTISTS);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.REVIEWS);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.GUEST_LIST);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.MY_DEALS);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.IMAGE_OPTIMIZE);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.TABLE_CHAT);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.TABLE_CONVERSATION);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.TABLE_GROUP);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.TABLE_GROUPS_PENDING_MSGS);

        onCreate(db);
	}
	
	public void delete_tables()
	{
		SQLiteDatabase db = this.getReadableDatabase();
		db.execSQL("DROP TABLE IF EXISTS " + WTP_USERS_TABLE);
		db.execSQL("DROP TABLE IF EXISTS " + ALL_CONTACTS_TABLE);
		
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.EVENTS);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.VENUES);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.ARTISTS);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.REVIEWS);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.GUEST_LIST);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.MY_DEALS);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.IMAGE_OPTIMIZE);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.TABLE_CHAT);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.TABLE_CONVERSATION);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.TABLE_GROUP);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.TABLE_GROUPS_PENDING_MSGS);
		db.close();
	}
	
	public long add_images_for_optimization(String id,String image)
	{
		long insert = -1;
		SQLiteDatabase db = this.getWritableDatabase();
		
		Cursor c = db.query(IMAGE_OPTIMIZE, null, PARAMS.IMAGE_ID.get_param()+"= ?", new String[]{id}, null, null, null);
		if(c != null)
		{
			if(!c.moveToLast())
			{
				ContentValues values = new ContentValues();
					values.put(PARAMS.IMAGE_ID.get_param(), id);
					values.put(PARAMS.IMAGE.get_param(), image);
					insert = db.insert(IMAGE_OPTIMIZE, null, values);
				
			}
		}
		else
		{
			ContentValues values = new ContentValues();
			values.put(PARAMS.IMAGE_ID.get_param(), id);
			values.put(PARAMS.IMAGE.get_param(), image);
			insert = db.insert(IMAGE_OPTIMIZE, null, values);
		}
		c.close();
		db.close();
		return insert;
	}
	public String get_local_storage_image(String id)
	{
		String image = null;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(IMAGE_OPTIMIZE, null, PARAMS.IMAGE_ID.get_param()+"= ?", new String[]{id}, null, null, null);
		if(c != null)
		{
			if(c.moveToLast())
			{
				if(c.getCount() > 0)
				{
					image = c.getString(c.getColumnIndex(PARAMS.IMAGE.get_param()));
				}
			}
		}
		c.close();
		db.close();
		return image;
	}
	
	public boolean is_image_available(String id)
	{
		boolean image = false;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(IMAGE_OPTIMIZE, null, PARAMS.IMAGE_ID.get_param()+"= ?", new String[]{id}, null, null, null);
		if(c != null)
		{
			if(c.moveToLast())
			{
				image = true;
			}
		}
		c.close();
		db.close();
		return image;
	}
	
	public void update_local_image(ArrayList<String> ids)
	{
		HashMap<String,String> map_ids = new HashMap<String,String>();
		for(int i = 0;i<ids.size();i++)
		{
			map_ids.put(ids.get(i), ids.get(i));
		}
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(IMAGE_OPTIMIZE, null, null, null, null, null, null);
		if(c != null)
		{
			while(c.moveToNext())
			{
				String id = c.getString(c.getColumnIndex(PARAMS.IMAGE_ID.get_param()));
				if(!map_ids.containsKey(id))
				{
					db.delete(IMAGE_OPTIMIZE, PARAMS.IMAGE_ID.get_param()+"= ?", new String[]{id});
				}
			}
		}
		c.close();
		db.close();
		
	}
	public long add_new_android_contact(String number)
	{
		long insert = -1;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor c = db.query(ALL_CONTACTS_TABLE, null, NUMBER+"= ?", new String[]{number}, null, null, null);
		if(c != null)
		{
			if(c.moveToLast())
			{
				if(c.getCount() == 0)
				{
					ContentValues values = new ContentValues();
					values.put(NUMBER, number);
					insert = db.insert(ALL_CONTACTS_TABLE, null, values);
				}
			}
			c.close();
		}
		
		
		db.close();
		return insert;
	}
	
	public ArrayList<String> get_all_android_contacts()
	{
		ArrayList<String> contacts = new ArrayList<String>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(ALL_CONTACTS_TABLE, null, null, null, null, null,null);
		if(c != null)
		{
			while(c.moveToNext())
			{
				contacts.add(c.getString(1));
				
			}
		}
		c.close();
		db.close();
		return contacts;
	}
	
	public long add_app_users(ArrayList<WTP_Contact> contacts)
	{
		HashMap<String,WTP_Contact> contacts_map = new HashMap<String,WTP_Contact>();
		HashMap<String,WTP_Contact> stored_contacts = new HashMap<String,WTP_Contact>();
		ArrayList<WTP_Contact> stored_contacts_array = get_app_users();
		if(stored_contacts_array.size() > 0)
		{
			for(int i = 0;i < stored_contacts_array.size();i++) 
			{
				stored_contacts.put(stored_contacts_array.get(i).get_wtp_id(), stored_contacts_array.get(i));
			}
			for(int i = 0;i < contacts.size();i++)
			{
				
					if(!stored_contacts.containsKey(contacts.get(i).get_wtp_id()))
						contacts_map.put(contacts.get(i).get_wtp_id(), contacts.get(i));
				
			}
			
		}
		else 
		{	for(int i = 0;i < contacts.size();i++)
			{
				contacts_map.put(contacts.get(i).get_wtp_id(), contacts.get(i));
			}
		}
		long insert = -1;
		SQLiteDatabase db = this.getWritableDatabase();
		for(Entry<String, WTP_Contact> entry : contacts_map.entrySet()) 
		{
		    //String key = entry.getKey();
			WTP_Contact c = entry.getValue();
		    ContentValues values = new ContentValues();
			values.put(WTP_ID, c.get_wtp_id());
			values.put(NAME, c.get_name());
			values.put(C_CODE, c.get_c_code());
			values.put(NUMBER, c.get_number());
			values.put(STATUS, c.get_status());
			values.put(THUMBNAIL, c.get_thumbnail());
			values.put(UPDATED_AT, c.get_time());
			values.put(CONTACT_DB_ID, C.number_Exists(cxt, c.get_number())[0]);
			
			insert = db.insert(WTP_USERS_TABLE, null, values);
		   
		}
		
		db.close();
		return insert;
	}
	public long add_app_user(WTP_Contact contact)
	{
		HashMap<String,WTP_Contact> contacts_map = new HashMap<String,WTP_Contact>();
		HashMap<String,WTP_Contact> stored_contacts = new HashMap<String,WTP_Contact>();
		ArrayList<WTP_Contact> stored_contacts_array = get_app_users();
		if(stored_contacts_array.size() > 0)
		{
			for(int i = 0;i < stored_contacts_array.size();i++) 
			{
				stored_contacts.put(stored_contacts_array.get(i).get_wtp_id(), stored_contacts_array.get(i));
			}
			if(!stored_contacts.containsKey(contact.get_wtp_id()))
						contacts_map.put(contact.get_wtp_id(), contact);
			
			
		}
		else 
		{	contacts_map.put(contact.get_wtp_id(), contact);
		}
		long insert = -1;
		SQLiteDatabase db = this.getWritableDatabase();
		for(Entry<String, WTP_Contact> entry : contacts_map.entrySet()) 
		{
		    //String key = entry.getKey();
			WTP_Contact c = entry.getValue();
		    ContentValues values = new ContentValues();
			values.put(WTP_ID, c.get_wtp_id());
			values.put(NAME, c.get_name());
			values.put(C_CODE, c.get_c_code());
			values.put(NUMBER, c.get_number());
			values.put(STATUS, c.get_status());
			values.put(THUMBNAIL, c.get_thumbnail());
			values.put(UPDATED_AT, c.get_time());
			values.put(CONTACT_DB_ID, C.number_Exists(cxt, c.get_number())[0]);
			
			insert = db.insert(WTP_USERS_TABLE, null, values);
		   
		}
		
		db.close();
		return insert;
	}
	public int update_app_users(WTP_Contact c) {
		int update = 0;
	    SQLiteDatabase db = this.getWritableDatabase();
	    Cursor cur = db.query(WTP_USERS_TABLE, null, NUMBER+"= ?", new String[]{c.get_number()}, null, null, null);
	 	if(cur != null)
			{
				if(cur.moveToLast())
				{
					if(cur.getCount() == 0)
					{
						ContentValues values = new ContentValues();
						values.put(WTP_ID, c.get_wtp_id());
						values.put(NAME, c.get_name());
						values.put(C_CODE, c.get_c_code());
						values.put(NUMBER, c.get_number());
						values.put(STATUS, c.get_status());
						values.put(THUMBNAIL, c.get_thumbnail());
						values.put(UPDATED_AT, c.get_time());
						if(c.get_contact_id() != null)
						values.put(CONTACT_DB_ID, c.get_contact_id());
						db.insert(WTP_USERS_TABLE, null, values);
					}
					else
					{
						 ContentValues values = new ContentValues();
							values.put(NAME, c.get_name());
							values.put(STATUS, c.get_status());
							values.put(THUMBNAIL, c.get_thumbnail());
							values.put(UPDATED_AT, c.get_time());
							// updating row
					    update = db.update(WTP_USERS_TABLE, values, WTP_ID + " = ?",
					            new String[] { c.get_wtp_id() });
					}
				}
				cur.close();
			}
	 
		   
	
	    db.close();
	    return update;
	}
	public ArrayList<WTP_Contact> get_app_users()
	{
		ArrayList<WTP_Contact> contacts = new ArrayList<WTP_Contact>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(WTP_USERS_TABLE, null, null, null, null, null, NAME + " DESC");
		if(c != null)
		{
			while(c.moveToNext())
			{
				//if(!c.getString(1).equals(SmoochApp.get_userid()))
				{
					String[] name = C.number_Exists(cxt, c.getString(c.getColumnIndex(NUMBER)));//.Contact_Exists(cxt, c.getString(c.getColumnIndex(CONTACT_DB_ID)));
					String fname = null;
					if(name != null)
					{
						fname = name[1];
					}
					else
					{
						fname = c.getString(c.getColumnIndex(NAME));
					}
					WTP_Contact contact = new WTP_Contact(c.getString(c.getColumnIndex(WTP_ID)),fname,c.getString(c.getColumnIndex(C_CODE)),c.getString(c.getColumnIndex(NUMBER)),c.getString(c.getColumnIndex(STATUS)),c.getString(c.getColumnIndex(THUMBNAIL)),c.getString(c.getColumnIndex(UPDATED_AT)),c.getString(c.getColumnIndex(CONTACT_DB_ID)));
					contacts.add(contact);
				}
			
			}
		}
		c.close();
		db.close();
		Collections.sort(contacts, new User_Comparator());
		return contacts;
	}
	public ArrayList<String> get_app_user_ids()
	{
		ArrayList<String> contacts = new ArrayList<String>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(WTP_USERS_TABLE, null, null, null, null, null, NAME + " DESC");
		if(c != null)
		{
			while(c.moveToNext())
			{
				contacts.add(c.getString(1));
			}
		}
		c.close();
		db.close();
		
		return contacts;
	}
	public WTP_Contact get_app_user_details(String id)
	{
		WTP_Contact contact = null;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(WTP_USERS_TABLE, null, WTP_ID+"= ?", new String[]{id}, null, null, null);
		if(c != null)
		{
			if(c.moveToNext())
			{
				String name = C.Contact_Exists(cxt, c.getString(c.getColumnIndex(CONTACT_DB_ID)));
				if(name == null)
				{
					name = c.getString(2);
				}
				 contact = new WTP_Contact(c.getString(c.getColumnIndex(WTP_ID)),c.getString(c.getColumnIndex(NAME)),c.getString(c.getColumnIndex(C_CODE)),c.getString(c.getColumnIndex(NUMBER)),c.getString(c.getColumnIndex(STATUS)),c.getString(c.getColumnIndex(THUMBNAIL)),c.getString(c.getColumnIndex(UPDATED_AT)),c.getString(c.getColumnIndex(CONTACT_DB_ID)));
				
				
			}
		}
		c.close();
		db.close();
		return contact;
	}
	public boolean is_user_exist(String pno)
	{
		boolean exist = false;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(WTP_USERS_TABLE, null, NUMBER+"= ?", new String[]{pno}, null, null, null);
		if(c != null)
		{
			c.moveToLast();
			int count = c.getCount();
			if(count > 0)
			{
				exist = true;
			}
			
		}
		c.close();
		db.close();
		return exist;
	}
	public boolean is_wtp_id_exist(String id)
	{
		boolean exist = false;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(WTP_USERS_TABLE, null, WTP_ID+"= ?", new String[]{id}, null, null, null);
		if(c != null)
		{
			c.moveToLast();
			int count = c.getCount();
			if(count > 0)
			{
				exist = true;
			}

		}
		c.close();
		db.close();
		return exist;
	}

	///////////////////////////////////////////
	public int update_profilepick(String photo,String thumbnail) {
		int update = 0;
	    SQLiteDatabase db = this.getWritableDatabase();
	 
	    ContentValues values = new ContentValues();
	    values.put(THUMBNAIL, thumbnail);
	 
	    // updating row
	    update = db.update(WTP_USERS_TABLE, values, WTP_ID + " = ?",
	            new String[] { App.get_userid() });
	    db.close();
	    return update;
	}
	public int update_status(String status) {
		int update = 0;
	    SQLiteDatabase db = this.getWritableDatabase();
	 
	    ContentValues values = new ContentValues();
	    values.put(STATUS, status);
	    
	 
	    // updating row
	    update = db.update(WTP_USERS_TABLE, values, WTP_ID + " = ?",
	            new String[] { App.get_userid() });
	    db.close();
	    return update;
	}
	
	///////////////////////////////////////////store the details in local db when data connection is not available
	
	public long update_event_attending_type(String id,String type,String attend_type)
	{
		long update = -1;
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
	    values.put(PARAMS.EVENT_ID.get_param(), id);
	    values.put(PARAMS.TYPE.get_param(), type);
	    values.put(PARAMS.ATTEND_TYPE.get_param(), attend_type);
	    
		Cursor c = db.query(DatabaseHelper.EVENTS, null, PARAMS.EVENT_ID.get_param()+"= ?", new String[]{id}, null, null, null);
		if(c != null)
		{
			c.moveToLast();
			int count = c.getCount();
			if(count > 0)
			{
				update = db.update(DatabaseHelper.EVENTS, values, ID + " = ?",
			            new String[] { String.valueOf(c.getColumnIndex(ID)) });
			}
			else
			{
				update = db.insertOrThrow(EVENTS, null, values);
			}
			
		}
		c.close();
		db.close();
		return update;
	}
	
	public long update_clubs_rating(String id,int stars,int type)
	{
		long update = -1;
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
	    values.put(PARAMS.VD_ID.get_param(), id);
	    values.put(PARAMS.STARS.get_param(), stars);
	    values.put(PARAMS.TYPE.get_param(), type);
	    
		Cursor c = db.query(DatabaseHelper.VENUES, null, PARAMS.VD_ID.get_param()+"= ?", new String[]{id}, null, null, null);
		if(c != null)
		{
			c.moveToLast();
			int count = c.getCount();
			if(count > 0)
			{
				update = db.update(DatabaseHelper.VENUES, values, ID + " = ?",
			            new String[] { String.valueOf(c.getColumnIndex(ID)) });
			}
			else
			{
				update = db.insertOrThrow(VENUES, null, values);
			}
			
		}
		c.close();
		db.close();
		return update;
	}
	
	public long update_artist_follow(String id,int following)
	{
		long update = -1;
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
	    values.put(PARAMS.ARTIST_ID.get_param(), id);
	    values.put(PARAMS.FOLLOWING.get_param(), following);
	    
	    
		Cursor c = db.query(DatabaseHelper.ARTISTS, null, PARAMS.ARTIST_ID.get_param()+"= ?", new String[]{id}, null, null, null);
		if(c != null)
		{
			c.moveToLast();
			int count = c.getCount();
			if(count > 0)
			{
				update = db.update(DatabaseHelper.ARTISTS, values, PARAMS.ARTIST_ID.get_param() + " = ?",
			            new String[] { id });
			}
			else
			{
				update = db.insert(ARTISTS, null, values);
			}
			
		}
		c.close();
		db.close();
		return update;
	}
	
	public long update_guestlist(ArrayList<Guestlist> guest_list)
	{
		long update = -1;
        Cursor c = null;

        ArrayList<Guestlist> db_list = new ArrayList<>();
        db_list = this.get_user_guestlist();
        HashMap<String,Guestlist> map = new HashMap<>();
        SQLiteDatabase db = this.getWritableDatabase();
        for(int i = 0;i < guest_list.size();i++) {

            Guestlist guestlist = guest_list.get(i);
            map.put(guestlist.get_eid(), guestlist);
            ContentValues values = new ContentValues();
            if (guestlist.get_name() != null)
                values.put(PARAMS.NAME.get_param(), guestlist.get_name());
            if (guestlist.get_eid() != null)
                values.put(PARAMS.EVENT_ID.get_param(), guestlist.get_eid());
            if (guestlist.get_id() != null)
                values.put(PARAMS.GUEST_ID.get_param(), guestlist.get_id());
            if (guestlist.get_date() != null)
                values.put(PARAMS.DATE.get_param(), guestlist.get_date());
            if (guestlist.get_code() != null)
                values.put(PARAMS.UNIQUE_CODE.get_param(), guestlist.get_code());


            c = db.query(DatabaseHelper.GUEST_LIST, null, PARAMS.EVENT_ID.get_param() + "= ?", new String[]{guestlist.get_eid()}, null, null, null);
            if (c != null) {
                c.moveToLast();
                int count = c.getCount();
                if (count > 0) {
                    update = db.update(DatabaseHelper.GUEST_LIST, values, PARAMS.EVENT_ID.get_param() + " = ?",
                            new String[]{guestlist.get_eid()});
                } else {
                    update = db.insertOrThrow(GUEST_LIST, null, values);
                }

            }

        }
        for(int i = 0;i < db_list.size();i++)
        {
            if(!map.containsKey(db_list.get(i).get_eid()))
            {
                db.delete(DatabaseHelper.GUEST_LIST, PARAMS.EVENT_ID.get_param() + " = ?",
                        new String[]{db_list.get(i).get_eid()});
            }
        }
        if(c != null)
		c.close();
        if(db != null)
		db.close();
		return update;
	}
	public long update_mydeals(String club_name,String id,String vdid,String uniq_id,String edate)
	{
		long update = -1;
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
        if(club_name != null)
            values.put(PARAMS.NAME.get_param(), club_name);
		if(id != null)
	    values.put(PARAMS.ID.get_param(), id);
		if(vdid != null)
	    values.put(PARAMS.VD_ID.get_param(), vdid);
		if(edate != null)
	    values.put(PARAMS.DATE.get_param(), edate);
		if(uniq_id != null)
	    values.put(PARAMS.UNIQUE_CODE.get_param(), uniq_id);
	    
		Cursor c = db.query(DatabaseHelper.MY_DEALS, null, PARAMS.ID.get_param()+"= ?", new String[]{id}, null, null, null);
		if(c != null)
		{
			c.moveToLast();
			int count = c.getCount();
			if(count > 0)
			{
				update = db.update(DatabaseHelper.MY_DEALS, values, PARAMS.ID.get_param() + " = ?",
			            new String[] { id });
			}
			else
			{
				update = db.insertOrThrow(MY_DEALS, null, values);
			}
			
		}
		c.close();
		db.close();
		return update;
	}
	public ArrayList<Guestlist> get_user_guestlist()
	{
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date chatdate = null;
		ArrayList<Guestlist> guestlist = new ArrayList<Guestlist>();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor c = db.query(DatabaseHelper.GUEST_LIST, null, null, null, null, null, null);
		if(c != null)
		{
			while(c.moveToNext())
			{
                try {
                    if(c.getString(c.getColumnIndex(PARAMS.DATE.get_param())) != null)
                    chatdate =  sdf.parse(c.getString(c.getColumnIndex(PARAMS.DATE.get_param())));
                    Calendar chatcal = Calendar.getInstance();
                    chatcal.setTime(chatdate);
                    if(!C.is_olderday(chatdate,new Date()))
                    {
                        String code = c.getString(c.getColumnIndex(PARAMS.GUEST_ID.get_param()));
                        if(code != null)
                        {
                            code = c.getString(c.getColumnIndex(PARAMS.UNIQUE_CODE.get_param()));
                        }
                        else
                        {
                            code = null;
                        }
                        Guestlist e = new Guestlist(c.getString(c.getColumnIndex(PARAMS.NAME.get_param())),c.getString(c.getColumnIndex(PARAMS.GUEST_ID.get_param())),c.getString(c.getColumnIndex(PARAMS.EVENT_ID.get_param())),c.getString(c.getColumnIndex(PARAMS.DATE.get_param())),code);
                        guestlist.add(e);
                    }
                    else
                    {
                        db.delete(DatabaseHelper.GUEST_LIST, PARAMS.EVENT_ID.get_param() + " = ?",
                                new String[]{c.getString(c.getColumnIndex(PARAMS.EVENT_ID.get_param()))});
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }


			}
		}
		c.close();
		db.close();
		return guestlist;
	}
	public ArrayList<Deals> get_user_deals()
	{
		ArrayList<Deals> deals = new ArrayList<Deals>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(DatabaseHelper.MY_DEALS, null, null, null, null, null, null);
		if(c != null)
		{
			while(c.moveToNext())
			{
				String code = c.getString(c.getColumnIndex(PARAMS.UNIQUE_CODE.get_param()));
				Deals e = new Deals(c.getString(c.getColumnIndex(PARAMS.NAME.get_param())),c.getString(c.getColumnIndex(PARAMS.ID.get_param())),c.getString(c.getColumnIndex(PARAMS.VD_ID.get_param())),code,c.getString(c.getColumnIndex(PARAMS.DATE.get_param())));
				deals.add(e);
                Log.d("name",e.get_name());
                Log.d("id",e.get_id());
                Log.d("vdid",e.get_venue_id());
                Log.d("code",e.get_unique_id());
			}
		}
		c.close();
		db.close();
		return deals;
	}
	@SuppressLint("SimpleDateFormat")
	public boolean check_guest_list(String eid,String date)
	{
		boolean exist = false;
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(DatabaseHelper.GUEST_LIST, null,null, null, null, null, null);
		if(c != null)
		{
			try {
				Date d1 = format.parse(date);
				while(c.moveToNext())
				{
					String temp_date = c.getString(c.getColumnIndex(PARAMS.DATE.get_param()));
					
					Date d2 = format.parse(temp_date);
					if(d1.compareTo(d2) == 0)
					{
						exist = true;
						break;
					}
					
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		c.close();
		db.close();
		return exist;
	}
	
	public long update_user_reviews(String id,String review,String time)
	{
		long update = -1;
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
	    values.put(PARAMS.VD_ID.get_param(), id);
	    values.put(PARAMS.REVIEW.get_param(), review);
	    values.put(PARAMS.TIME.get_param(), time);
	    
		update = db.insertOrThrow(REVIEWS, null, values);
		
		if(db != null)
			db.close();
		return update;
	}
	///////////////////////////get offline stored details
	public ArrayList<Event> get_pending_event_status()
	{
		ArrayList<Event> events = new ArrayList<Event>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(EVENTS, null, null, null, null, null, null);
		if(c != null)
		{
			while(c.moveToNext())
			{
				Event e = new Event(c.getString(c.getColumnIndex(ID)),c.getString(c.getColumnIndex(PARAMS.EVENT_ID.get_param())),c.getInt(c.getColumnIndex(PARAMS.ATTEND_TYPE.get_param())),c.getInt(c.getColumnIndex(PARAMS.TYPE.get_param())));
				events.add(e);
			}
		}
		c.close();
		db.close();
		return events;
	}
	
	public ArrayList<V_Details> get_pending_venue_stars()
	{
		ArrayList<V_Details> venues = new ArrayList<V_Details>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(VENUES, null, null, null, null, null, null);
		if(c != null)
		{
			while(c.moveToNext())
			{
				V_Details v = new V_Details(c.getString(c.getColumnIndex(ID)),c.getString(c.getColumnIndex(PARAMS.VD_ID.get_param())),c.getString(c.getColumnIndex(PARAMS.STARS.get_param())),c.getInt(c.getColumnIndex(PARAMS.TYPE.get_param())));
				venues.add(v);
			}
		}
		c.close();
		db.close();
		return venues;
	}
	
	public ArrayList<Artist> get_pending_follows()
	{
		ArrayList<Artist> artists = new ArrayList<Artist>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(ARTISTS, null, null, null, null, null, null);
		if(c != null)
		{
			while(c.moveToNext())
			{
				Artist a = new Artist(c.getString(c.getColumnIndex(ID)),c.getString(c.getColumnIndex(PARAMS.ARTIST_ID.get_param())),c.getString(c.getColumnIndex(PARAMS.FOLLOWING.get_param())));
				artists.add(a);
			}
		}
		c.close();
		db.close();
		return artists;
	}
	
	public ArrayList<Review> get_pending_reviews()
	{
		ArrayList<Review> reviews = new ArrayList<Review>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(REVIEWS, null, null, null, null, null, null);
		if(c != null)
		{
			while(c.moveToNext())
			{
				Review r = new Review(c.getString(c.getColumnIndex(ID)),c.getString(c.getColumnIndex(PARAMS.VD_ID.get_param())),c.getString(c.getColumnIndex(PARAMS.REVIEW.get_param())),c.getString(c.getColumnIndex(PARAMS.TIME.get_param())));
				reviews.add(r);
			}
		}
		c.close();
		db.close();
		return reviews;
	}
	//////////////////////delete the updated details
	public void remove_attend_type(String id)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(EVENTS, ID +"=?", new String[]{id});
		if(db != null)
		db.close();
	}
	
	public void remove_club_rating(String id)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(VENUES, ID +"=?", new String[]{id});
		if(db != null)
		db.close();
	}
	
	public void remove_artist_follow(String id)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(ARTISTS, ID +"=?", new String[]{id});
		if(db != null)
		db.close();
	}
	
	public void remove_review(String id)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(REVIEWS, ID +"=?", new String[]{id});
		if(db != null)
		db.close();
	}
	
	public void remove_guest_list(String guest_id)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(GUEST_LIST, PARAMS.GUEST_ID.get_param() +"=?", new String[]{guest_id});
		if(db != null)
		db.close();
	}

    // ***************************************************** //

    //All db functions related to chat

    //***********************************************************************//
    public long insertConversationDetails(String chatid, String Msg, String DateTime,String Poster, String ReadStatus, String msg_content_type, String media_status, String server_url, String delivery_status)
    {
        long id = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(CHATID,chatid);
        cv.put(MSG, Msg);
        cv.put(DATETIME, DateTime);
        cv.put(POSTER, Poster);
        cv.put(READSTATUS, ReadStatus);
        cv.put(MESSAGECONTENTTYPE, msg_content_type);
        cv.put(MEDIASTATUS, media_status);
        cv.put(SERVERURL, server_url);
        cv.put(DELIVERYSTATUS, delivery_status);

        Log.d(" In DB Inserting Conversation Details","ChatId:"+chatid+" Msg:"+Msg+" DateTime:"+DateTime+" Poster:"+Poster+" ReadStatus:"+ReadStatus);

        id = db.insert(TABLE_CONVERSATION, null, cv);
        db.close();
        return id;
    }

    //********************************************************************//


    public void insertGroupDetails(String group_name, String creation_date, String admin,  String user_ids,String servergroupid)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(GROUPNAME, group_name);
        cv.put(CREATIONDATE, creation_date);
        cv.put(ADMIN, admin);
        cv.put(GROUPUSERS_IDS, user_ids);
        cv.put(SERVERGROUPID, servergroupid);

        Log.d("In DB","Inserting Group Details: Groupname:"+group_name+" Creationdate:"+creation_date+" Admin:"+admin+" UserIds:"+user_ids+" ServerGroupId:"+servergroupid);

        db.insert(TABLE_GROUP, null, cv);
        db.close();
    }

    //Update Group name
    public void update_group_name(String group_name,String servergroupid)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(GROUPNAME, group_name);

        Log.d("In DB Updating Group Name", "GroupName:"+group_name+" Servergroupid:"+servergroupid);

        String[] value = new String[] { servergroupid }; // The value for the where clause.

        // Update the database (all columns in TABLE_NAME where my_column has a value of 2 will be changed to 5)
        db.update(TABLE_GROUP, cv, SERVERGROUPID+" = ?", value);
        db.close();
    }

    //Update pending msg status to sent
    public void update_pending_msg_status_to_sent(String msgid)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(MEDIASTATUS, Integer.toString(SENT));

        Log.d("In DB Updating pending msg status to sent", "Msgid:"+msgid);

        String[] value = new String[] { msgid }; // The value for the where clause.

        // Update the database (all columns in TABLE_NAME where my_column has a value of 2 will be changed to 5)
        db.update(TABLE_CONVERSATION, cv, MSG_ID+" = ?", value);
        db.close();
    }



    //Delete pending group msg status to sent
    public void delete_pending_group_msg_entry(String msgid)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_GROUPS_PENDING_MSGS, PENDING_GROUP_MSGID +"=?", new String[]{msgid});
        if(db != null)
            db.close();
    }



    //Update Group users
    public void update_group_users(String user_ids,String servergroupid, String admin)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(GROUPUSERS_IDS, user_ids);
        cv.put(ADMIN,admin);

        Log.d("In DB Updating Group Details", "UserIds:"+user_ids+" Servergroupid:"+servergroupid);

        String[] value = new String[] { servergroupid }; // The value for the where clause.

        // Update the database (all columns in TABLE_NAME where my_column has a value of 2 will be changed to 5)
        db.update(TABLE_GROUP, cv, SERVERGROUPID+" = ?", value);
        db.close();
    }



    public ArrayList<GroupDetails> get_group_details(String groupid)
    {
        String count=null;
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<GroupDetails> details = new ArrayList<GroupDetails>();
        Cursor c = db.query(TABLE_GROUP, null,SERVERGROUPID+" = ? ", new String[] { groupid }, null,null,null);
        Log.d("In DB","Getting Group Details of groupid:"+groupid);

        if(c != null)
        {
            while(c.moveToNext())
            {

                GroupDetails a = new GroupDetails(c.getString(c.getColumnIndex(GROUPNAME)), c.getString(c.getColumnIndex(CREATIONDATE)), c.getString(c.getColumnIndex(ADMIN)), c.getString(c.getColumnIndex(GROUPUSERS_IDS)),  c.getInt(c.getColumnIndex(SERVERGROUPID)));
                Log.d("In DB Group Name :: ", c.getString(c.getColumnIndex(GROUPNAME)));
                Log.d("In DB Creation Date :: ", c.getString(c.getColumnIndex(CREATIONDATE)));
                Log.d("In DB Group Admin :: ", c.getString(c.getColumnIndex(ADMIN)));
                Log.d("In DB Group User Ids :: ", c.getString(c.getColumnIndex(GROUPUSERS_IDS)));
                Log.d("In DB Group Server Id :: ", c.getString(c.getColumnIndex(SERVERGROUPID)));
                details.add(a);
            }
        }
        c.close();
        db.close();
        return details;
    }




//*****************************************************************************//

    public long insertChatDetails(String userid, String last_msg, String last_time,  String is_group, String unread_count)
    {
        long id=0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(WTP_ID, userid);
        cv.put(LASTMSG, last_msg);
        cv.put(LASTTIME, last_time);
        cv.put(ISGROUP, is_group);
        cv.put(UNREADCOUNT, unread_count);


        Log.d("In DB","Inserting Chat** Details: ChatId:"+userid+" LastMsg:"+last_msg+" IsGroup:"+is_group+" Unreadcount:"+unread_count);

        id=db.insert(TABLE_CHAT, null, cv);
        db.close();
        return id;
    }



    // Querys

    public void delete_conversation_row(String chatid)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONVERSATION, MSG_ID+ "='" + chatid + "'", null);
        Log.d("In DB","Deleting chatrow with chatid:"+chatid);
        if(db != null)
            db.close();
    }

    public void clear_all_chat_tables()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONVERSATION, null, null);
        db.delete(TABLE_GROUPS_PENDING_MSGS, null, null);
        db.delete(TABLE_GROUP, null, null);
        db.delete(TABLE_CHAT, null, null);
        Log.d("In DB", "Clearing all chat tables");
        if(db != null)
            db.close();
    }



    public void delete_user_complete_conversation(String userid)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CHAT, WTP_ID+ "='" + userid + "'", null);
        Log.d("In DB","Deleting complete conversation with chatid:"+userid);
        if(db != null)
            db.close();
    }

    public void delete_group_complete_conversation(String chatid)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONVERSATION, CHATID+ "='" + chatid + "'", null);
        Log.d("In DB","Deleting complete conversation of group with chatid:"+chatid);
        if(db != null)
            db.close();
    }

    public void delete_group_details(String serverid)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_GROUP, SERVERGROUPID+ "='" + serverid + "'", null);
        Log.d("In DB","Deleting group details with serverid:"+serverid);
        if(db != null)
            db.close();
    }

    public boolean check_chatId_exist(String user)
    {
        boolean exist = false;
        SQLiteDatabase db = this.getReadableDatabase();
        Log.d("In DB","Checking chat id exist or not with userid as:"+user);
        Cursor check_id = db.query(TABLE_CHAT, null,WTP_ID+" = ? ", new String[] { user }, null, null, null);
        if(check_id!=null)
        {
            if(check_id.moveToNext())
            {
                exist=true;
                Log.d("In DB Chat Id Exist","true");

            }
        }
        check_id.close();
        db.close();
        return exist;
    }



    //Query to get the unread msg count with specific chat id
    public int get_unread_msg_count_with_id(String userid)
    {
        int count=0;
        SQLiteDatabase db = this.getReadableDatabase();
        Log.d("In DB","Get unread message count with chatid as:"+userid);
        Cursor get_unreadmsg_count = db.query(TABLE_CHAT, new String[]{UNREADCOUNT},WTP_ID+" = ?", new String[] { userid }, null, null, null);
        if(get_unreadmsg_count!=null)
        {
            if(get_unreadmsg_count.moveToNext())
            {
                count = get_unreadmsg_count.getInt(0);
            }
        }
        get_unreadmsg_count.close();
        db.close();
        return count;
    }


    // Insert Group pending messages
    public long insertPendingGroupMsgs(String memberid, String msg, String messagetype,String servergroupid)
    {
        long id = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(PENDING_GROUP_MEMBERID,memberid);
        cv.put(PENDING_GROUP_MESSAGE, msg);
        cv.put(PENDING_GROUP_MESSAGETYPE, messagetype);
        cv.put(PENDING_GROUP_SERVERGROUPID, servergroupid);

        Log.d(" In DB Inserting Pending Group Messages","MemberId:"+memberid+" Msg:"+msg+" MessageType:"+messagetype+" ServerGroupId:"+servergroupid);

        id = db.insert(TABLE_GROUPS_PENDING_MSGS, null, cv);
        db.close();
        return id;
    }



    //Query to get number of conversations from where user has received messages
    public int get_unread_conversation_count()
    {
        int count=0;
        SQLiteDatabase db = this.getReadableDatabase();
        Log.d("In DB","Get total unread conversation count");
        Cursor get_count = db.query(TABLE_CONVERSATION, new String[]{"DISTINCT("+CHATID+")",},READSTATUS+" = '0' ", new String[]{}, null, null, null);

        if(get_count!=null)
        {
            if(get_count.moveToNext())
            {
                count = get_count.getInt(0);
            }
        }
        get_count.close();
        db.close();
        return count;
    }

    //Query to get number of unread messages
    public String get_unread_messages_count()
    {
        String msgcount=null;
        SQLiteDatabase db = this.getReadableDatabase();
        Log.d("In DB","Get total unread message count");
        Cursor get_msgcount =db.query(TABLE_CONVERSATION, new String[]{"count(*)",},READSTATUS+" = '0' ", new String[]{}, null, null, null);
        if(get_msgcount!=null)
        {
            if(get_msgcount.moveToNext())
            {
                msgcount = get_msgcount.getString(0);
            }
        }
        get_msgcount.close();
        db.close();
        return msgcount;
    }



    //Query to get member name
    public String get_membername_by_id(String memberid)
    {
        String name=null;
        SQLiteDatabase db = this.getReadableDatabase();
        Log.d("In DB","Get member name having member id as:"+memberid);
        Cursor get_name =db.query(WTP_USERS_TABLE, new String[]{NAME},WTP_ID+" = ?  ", new String[]{memberid}, null, null, null);
        if(get_name!=null)
        {
            if(get_name.moveToNext())
            {
                name = get_name.getString(0);
            }
        }
        get_name.close();
        db.close();
        return name;
    }

    //Query to get chatid of group
    public String get_chatid_of_group_by_serverid(String serverid)
    {
        String name=null;
        SQLiteDatabase db = this.getReadableDatabase();
        Log.d("In DB","Get chat id of group having server id as:"+serverid);
        Cursor get_id =db.query(TABLE_CHAT, new String[]{CHATID},WTP_ID+" = ?  ", new String[]{serverid}, null, null, null);
        if(get_id!=null)
        {
            if(get_id.moveToNext())
            {
                name = get_id.getString(0);
            }
        }
        get_id.close();
        db.close();
        return name;
    }

    //Query to get chatid of member
    public String get_chatid_of_member_by_userid(String userid)
    {
        String name=null;
        SQLiteDatabase db = this.getReadableDatabase();
        Log.d("In DB","Get chat id of member having user id as:"+userid);
        Cursor get_id =db.query(TABLE_CHAT, new String[]{CHATID},WTP_ID+" = ?  ", new String[]{userid}, null, null, null);
        if(get_id!=null)
        {
            if(get_id.moveToNext())
            {
                name = get_id.getString(0);
            }
        }
        get_id.close();
        db.close();
        return name;
    }

    //Query to get group name
    public String get_groupname_by_id(String groupid)
    {
        String name=null;
        SQLiteDatabase db = this.getReadableDatabase();
        Log.d("In DB","Get group name having group id as:"+groupid);
        Cursor get_name =db.query(TABLE_GROUP, new String[]{GROUPNAME},SERVERGROUPID+" = ?  ", new String[]{groupid}, null, null, null);
        if(get_name!=null)
        {
            if(get_name.moveToNext())
            {
                name = get_name.getString(0);
            }
        }
        get_name.close();
        db.close();
        return name;
    }



    //Query to get group members
    public List<String> get_all_group_members(String groupid)
    {
        List<String> members = new ArrayList<String>();
        String mem = null;
        Log.d("In DB","Get all group members having group id as:"+groupid);
        SQLiteDatabase db = this.getReadableDatabase();
        Log.d("My group id", groupid);
        Cursor get_members =db.query(TABLE_GROUP, new String[]{GROUPUSERS_IDS},SERVERGROUPID+"= ? ", new String[]{groupid}, null, null, null);
        if(get_members!=null)
        {
            if(get_members.moveToNext())
            {
                mem = get_members.getString(0);
                Log.d("Mem", mem);
            }

            String[] parts = mem.split(", ");
            members = new ArrayList<String>(Arrays.asList(parts));
            Log.d("members", members.toString());

        }
        get_members.close();
        db.close();
        return members;
    }

    public ArrayList<ChatMsg> get_conversation_by_userid(String chatid)
    {
        String lastRowDate=null;
        SQLiteDatabase db = this.getReadableDatabase();
        Log.d("In DB","Get conversation having chat id as:"+chatid);
        ArrayList<ChatMsg> chats = new ArrayList<ChatMsg>();
        Cursor c = db.query(TABLE_CONVERSATION, null,CHATID+" = ?", new String[] { chatid }, null, null, null);
        if(c != null)
        {
            while(c.moveToNext())
            {
                String mdate = c.getString(c.getColumnIndex(DATETIME));
                String[] parts = mdate.split(" ");
                String fetcheddate = parts[0];

                if(lastRowDate==null)
                {
                    lastRowDate=fetcheddate;
                }
                if(!fetcheddate.equalsIgnoreCase(lastRowDate))
                {
                    ChatMsg mypost = new ChatMsg(0, 0,fetcheddate, 1,null, 1,0,3,"",C.STATUS_DELIVERED);
                    chats.add(mypost);
                }
                lastRowDate=fetcheddate;
                ChatMsg a = new ChatMsg(c.getInt(c.getColumnIndex(MSG_ID)),c.getInt(c.getColumnIndex(CHATID)),c.getString(c.getColumnIndex(DATETIME)),c.getInt(c.getColumnIndex(POSTER)),c.getString(c.getColumnIndex(MSG)),0, c.getInt(c.getColumnIndex(MESSAGECONTENTTYPE)), c.getInt(c.getColumnIndex(MEDIASTATUS)), c.getString(c.getColumnIndex(SERVERURL)), c.getInt(c.getColumnIndex(DELIVERYSTATUS)) );
                chats.add(a);
            }
        }

        c.close();
        db.close();
        return chats;
    }

    class LoadUrlASYNC extends AsyncTask<String, Void, Boolean>
    {
        boolean exist = false;

        @Override
        protected Boolean doInBackground(String... urls)
        {
            try
            {
                URL url = new URL(urls[0]);
                URLConnection urlConnection = url.openConnection();

                HttpURLConnection.setFollowRedirects(false);
                HttpURLConnection httpURLConnection = (HttpURLConnection) urlConnection;
                httpURLConnection.setRequestMethod("HEAD");

                if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK)
                {
                    System.out.println("URL Exist");
                    exist = true;
                }
            }
            catch(UnknownHostException unknownHostException)
            {
                System.out.println("UnkownHost");
                System.out.println(unknownHostException);
            }
            catch (Exception e)
            {
                System.out.println(e);
            }
            return exist;
        }

        @Override
        protected void onPostExecute(Boolean result)
        {

        }
    }

    public boolean checkURLExists(String myurl)
    {
        boolean exist = false;
        if(net.isConnectingToInternet())
        {
            try {
                exist = new LoadUrlASYNC().execute(myurl).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(cxt,"No Internet Connection", Toast.LENGTH_LONG).show();
        }

        return exist;
    }



    public TextMedia get_conversation_by_message_id(String msgid)
    {

        SQLiteDatabase db = this.getReadableDatabase();
        Log.d("In DB","Get conversation having chat id as:"+msgid);
        TextMedia chats = null;
        Cursor c = db.query(TABLE_CONVERSATION, null,MSG_ID+" = ?", new String[] { msgid }, null, null, null);
        if(c != null)
        {
            while(c.moveToNext())
            {
                int messagetype = Integer.parseInt(c.getString(c.getColumnIndex(MESSAGECONTENTTYPE)));
                if(messagetype==2||messagetype==3||messagetype==4)
                {
                    String myurl = c.getString(c.getColumnIndex(SERVERURL));
                    boolean urlexist = checkURLExists(myurl);
                    if(urlexist)
                    {
                        chats = new TextMedia(c.getString(c.getColumnIndex(MSG)), c.getInt(c.getColumnIndex(MESSAGECONTENTTYPE)), c.getInt(c.getColumnIndex(CHATID)), c.getString(c.getColumnIndex(SERVERURL)));
                    }
                    else
                    {
                        chats = new TextMedia(c.getString(c.getColumnIndex(MSG)), c.getInt(c.getColumnIndex(MESSAGECONTENTTYPE)), c.getInt(c.getColumnIndex(CHATID)), "Not Present");
                    }
                }
                else
                {
                    chats = new TextMedia(c.getString(c.getColumnIndex(MSG)), c.getInt(c.getColumnIndex(MESSAGECONTENTTYPE)), c.getInt(c.getColumnIndex(CHATID)), c.getString(c.getColumnIndex(SERVERURL)));
                }

            }
        }

        c.close();
        db.close();
        return chats;
    }


    //Get not sent messages
    public ArrayList<UploadDetails> get_notsent_nongroup_messages()
    {
        ArrayList<UploadDetails> chats = new ArrayList<UploadDetails>();
        SQLiteDatabase db = this.getReadableDatabase();
        Log.d("In DB","Get not sent and non group messages");

        String MY_QUERY = "SELECT * FROM "+TABLE_CONVERSATION+" conversation INNER JOIN "+TABLE_CHAT+" chat ON conversation.ChatId=chat.ChatId WHERE conversation.Media_Status=?";

        Cursor c = db.rawQuery(MY_QUERY, new String[]{Integer.toString(UPLOADING)});

        if(c != null)
        {
            while(c.moveToNext())
            {
                UploadDetails ud = new UploadDetails(c.getString(c.getColumnIndex(MSG)), c.getInt(c.getColumnIndex(MESSAGECONTENTTYPE)), c.getInt(c.getColumnIndex(MSG_ID)), c.getString(c.getColumnIndex(SERVERURL)), c.getInt(c.getColumnIndex(ISGROUP)),c.getInt(c.getColumnIndex(WTP_ID)));
                chats.add(ud);
            }
        }

        c.close();
        db.close();
        return chats;
    }

    //Get not sent messages
    public ArrayList<PendingGroupMsg> get_notsent_group_messages()
    {
        ArrayList<PendingGroupMsg> chats = new ArrayList<PendingGroupMsg>();
        SQLiteDatabase db = this.getReadableDatabase();
        Log.d("In DB","Get not sent and group messages");

        Cursor c = db.query(TABLE_GROUPS_PENDING_MSGS, null,null, null, null, null, null);

        if(c != null)
        {
            while(c.moveToNext())
            {
                PendingGroupMsg ud = new PendingGroupMsg(c.getString(c.getColumnIndex(PENDING_GROUP_MESSAGE)), c.getInt(c.getColumnIndex(PENDING_GROUP_MESSAGETYPE)), c.getInt(c.getColumnIndex(PENDING_GROUP_MSGID)), c.getInt(c.getColumnIndex(PENDING_GROUP_MEMBERID)), 1,c.getInt(c.getColumnIndex(PENDING_GROUP_SERVERGROUPID)));
                chats.add(ud);
            }
        }

        c.close();
        db.close();
        return chats;
    }



    public boolean check_group_present(String servergroupid)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        boolean exist = false;
        Log.d("In DB","Checking group present or not");
        Cursor c = db.query(TABLE_GROUP, null,SERVERGROUPID+" = ? ", new String[] { servergroupid }, null, null, null);

        if(c != null)
        {
            while(c.moveToNext())
            {
                exist = true;
                break;
            }
        }
        c.close();
        db.close();
        return exist;
    }

    public boolean check_conversation_present(String chatid)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        boolean exist = false;
        Log.d("In DB","Checking conversation present or not");
        Cursor c = db.query(TABLE_CONVERSATION, null,CHATID+" = ? ", new String[] { chatid }, null, null, null);

        if(c != null)
        {
            while(c.moveToNext())
            {
                exist = true;
                Log.d("In DB","Conversation is present");
                break;
            }
        }
        c.close();
        db.close();
        return exist;
    }


    //Query to get all active user names
    public ArrayList<ActiveChatDetails> get_active_usernames()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<ActiveChatDetails> userlist = new ArrayList<ActiveChatDetails>();
        Log.d("In DB","Getting list of active usernames ");
        Cursor c = db.query(TABLE_CHAT,null,null,null,null,null,null);

        if(c != null)
        {
            while(c.moveToNext())
            {
                ActiveChatDetails nameid = new ActiveChatDetails(c.getInt(c.getColumnIndex(CHATID)),c.getInt(c.getColumnIndex(WTP_ID)), c.getInt(c.getColumnIndex(ISGROUP)), c.getString(c.getColumnIndex(LASTMSG)), c.getString(c.getColumnIndex(LASTTIME)), c.getInt(c.getColumnIndex(UNREADCOUNT)));
                userlist.add(nameid);
                // Log.d("WTPID", Integer.toString(c.getInt(c.getColumnIndex(WTP_ID))));
                //Log.d("ISGROUP", Integer.toString(c.getInt(c.getColumnIndex(ISGROUP))));
                //Log.d("LASTMSG", (c.getString(c.getColumnIndex(LASTMSG))));
                //Log.d("LASTTIME", (c.getString(c.getColumnIndex(LASTTIME))));
                //Log.d("UNREADCOUNT", Integer.toString(c.getInt(c.getColumnIndex(UNREADCOUNT))));
            }
        }

        c.close();
        db.close();
        return userlist;
    }


    public ArrayList<ActiveChatDetails> get_only_active_users()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<ActiveChatDetails> userlist = new ArrayList<ActiveChatDetails>();
        Log.d("In DB","Getting list of active users only ");
        Cursor c = db.query(TABLE_CHAT,null,ISGROUP+" = ? ",new String[] { "0" },null,null,null);

        if(c != null)
        {
            while(c.moveToNext())
            {
                ActiveChatDetails nameid = new ActiveChatDetails(c.getInt(c.getColumnIndex(CHATID)),c.getInt(c.getColumnIndex(WTP_ID)), c.getInt(c.getColumnIndex(ISGROUP)), c.getString(c.getColumnIndex(LASTMSG)), c.getString(c.getColumnIndex(LASTTIME)), c.getInt(c.getColumnIndex(UNREADCOUNT)));
                userlist.add(nameid);

            }
        }

        c.close();
        db.close();
        return userlist;
    }


    public ArrayList<ActiveChatDetails> get_only_groups_available()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<ActiveChatDetails> userlist = new ArrayList<ActiveChatDetails>();
        Log.d("In DB","Getting list of active users only ");
        Cursor c = db.query(TABLE_CHAT,null,ISGROUP+" = ? ",new String[] { "1" },null,null,null);

        if(c != null)
        {
            while(c.moveToNext())
            {
                ActiveChatDetails nameid = new ActiveChatDetails(c.getInt(c.getColumnIndex(CHATID)),c.getInt(c.getColumnIndex(WTP_ID)), c.getInt(c.getColumnIndex(ISGROUP)), c.getString(c.getColumnIndex(LASTMSG)), c.getString(c.getColumnIndex(LASTTIME)), c.getInt(c.getColumnIndex(UNREADCOUNT)));
                userlist.add(nameid);

            }
        }

        c.close();
        db.close();
        return userlist;
    }

    public ArrayList<WTP_Contact> get_all_users_dummy()
    {
        //SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<WTP_Contact> userlist = new ArrayList<WTP_Contact>();
        Log.d("In DB","Getting list of all users only ");

                WTP_Contact user1 = new WTP_Contact("1","Akshay","+91", "9021555551","","abcd.jpg","09/03/2015 05:34:34","1");
                WTP_Contact user2 = new WTP_Contact("2","Vijay","+91", "9021555552","","abcd.jpg","09/03/2015 05:34:34","2");
                WTP_Contact user3 = new WTP_Contact("3","Manjunath","+91", "9021555553","","abcd.jpg","09/03/2015 05:34:34","3");
                WTP_Contact user4 = new WTP_Contact("4","Sagar","+91", "9021555554","","abcd.jpg","09/03/2015 05:34:34","4");
                WTP_Contact user5 = new WTP_Contact("5","Sachin","+91", "9021555555","","abcd.jpg","09/03/2015 05:34:34","5");
                WTP_Contact user6 = new WTP_Contact("6","Sanchita","+91", "9021555556","","abcd.jpg","09/03/2015 05:34:34","6");
                WTP_Contact user7 = new WTP_Contact("7","Samar","+91", "9021555557","","abcd.jpg","09/03/2015 05:34:34","7");
                WTP_Contact user8 = new WTP_Contact("8","Deepak","+91", "8123772458","","abcd.jpg","09/03/2015 05:34:34","8");
                userlist.add(user1);
                userlist.add(user2);
                userlist.add(user3);
                userlist.add(user4);
                userlist.add(user5);
                userlist.add(user6);
                userlist.add(user7);
                userlist.add(user8);

        //db.close();
        return userlist;
    }



    public void set_read_status_in_conversation_table(String chatid)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(READSTATUS, "1");
        Log.d("In DB","Setting ReadStatus of chatid:"+chatid);


        String where = CHATID+"=?"; // The where clause to identify which columns to update.
        String[] value = new String[] { chatid }; // The value for the where clause.

        // Update the database (all columns in TABLE_NAME where my_column has a value of 2 will be changed to 5)
        db.update(TABLE_CONVERSATION, cv, CHATID+" = ?", value);
        db.close();
    }

    public void update_count(String userid)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(UNREADCOUNT, "0");

        Log.d("In DB","Updating UnreadCount of userid:"+userid);

        String[] value = new String[] { userid }; // The value for the where clause.

        // Update the database (all columns in TABLE_NAME where my_column has a value of 2 will be changed to 5)
        db.update(TABLE_CHAT, cv, WTP_ID+" = ?", value);
        db.close();
    }

    public void update_msg_status_to_sent(String msgid)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(MEDIASTATUS, Integer.toString(C.STATUS_SENT));
        cv.put(DELIVERYSTATUS, Integer.toString(C.STATUS_SENT));

        Log.d("In DB","Updating msg status to sent of msgid:"+msgid);

        String[] value = new String[] { msgid }; // The value for the where clause.

        // Update the database (all columns in TABLE_NAME where my_column has a value of 2 will be changed to 5)
        db.update(TABLE_CONVERSATION, cv, MSG_ID+" = ?", value);
        db.close();
    }

    public void update_msg_status_to_delivered(String msgid)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(MEDIASTATUS, Integer.toString(C.STATUS_DELIVERED));
        cv.put(DELIVERYSTATUS, Integer.toString(C.STATUS_DELIVERED));

        Log.d("In DB","Updating msg status to sent of msgid:"+msgid);

        String[] value = new String[] { msgid }; // The value for the where clause.

        // Update the database (all columns in TABLE_NAME where my_column has a value of 2 will be changed to 5)
        db.update(TABLE_CONVERSATION, cv, MSG_ID+" = ?", value);
        db.close();
    }





    public void update_download_status(String msg_id, String msg)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(MEDIASTATUS, "3"); //3-DOWNLOADED
        cv.put(MSG, msg);
        Log.d("In DB","Updating Media status to downloaded for msg_id"+msg_id);

        String[] value = new String[] { msg_id }; // The value for the where clause.

        // Update the database (all columns in TABLE_NAME where my_column has a value of 2 will be changed to 5)
        db.update(TABLE_CONVERSATION, cv, MSG_ID+" = ?", value);
        db.close();
    }
    public void update_status_to_downloading(String msg_id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(MEDIASTATUS, "2"); //2-DOWNLOADING

        Log.d("In DB","Updating Media status to downloaded for msg_id"+msg_id);

        String[] value = new String[] { msg_id }; // The value for the where clause.

        // Update the database (all columns in TABLE_NAME where my_column has a value of 2 will be changed to 5)
        db.update(TABLE_CONVERSATION, cv, MSG_ID+" = ?", value);
        db.close();
    }


    public void update_status_to_uploaded_with_server_url(String msg_id, String serverurl)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(MEDIASTATUS, "6");
        cv.put(SERVERURL, serverurl);
        Log.d("In DB","Updating Media status to uploaded for msg_id"+msg_id);

        String[] value = new String[] { msg_id }; // The value for the where clause.

        // Update the database (all columns in TABLE_NAME where my_column has a value of 2 will be changed to 5)
        db.update(TABLE_CONVERSATION, cv, MSG_ID+" = ?", value);
        db.close();
    }


    //Update Chat last msg time & count
    public void update_chat_lastmsg_time_count(String userid, String last_msg, String last_time, String unreadcount)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(LASTMSG, last_msg);
        cv.put(LASTTIME, last_time);
        cv.put(UNREADCOUNT, unreadcount);

        Log.d("In DB Updating Chat Table", "LastMsg:"+last_msg+" Lasttime:"+last_time+" Count:"+unreadcount);



        String[] value = new String[] { userid }; // The value for the where clause.

        // Update the database (all columns in TABLE_NAME where my_column has a value of 2 will be changed to 5)
        db.update(TABLE_CHAT, cv, WTP_ID+" = ?", value);
        db.close();
    }

    public boolean check_userid_present(String WTP_ID)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        boolean exist = false;
        Log.d("In DB","Checking userid present or not with WTP Id as :"+WTP_ID);
        Cursor c = db.query(TABLE_CHAT, null,WTP_ID+" = ? ", new String[] { WTP_ID }, null, null, null);

        if(c != null)
        {
            while(c.moveToNext())
            {
                exist = true;
                break;
            }
        }
        c.close();
        db.close();
        return exist;
    }

    //**************************************************************************//
}
