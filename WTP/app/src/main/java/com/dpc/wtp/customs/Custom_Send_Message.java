package com.dpc.wtp.customs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.dpc.wtp.Models.GroupDetails;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.activities.App;
import com.dpc.wtp.services.WTP_Service;

import org.jivesoftware.smack.packet.Message;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by user on 3/23/2015.
 */
public class Custom_Send_Message
{
    Context cxt;
    DatabaseHelper db;
    Net_Detect net;
    String textmsg = null;
    int sendTo = 0, messagetype = 0;
    String sendMessageType = null;
    public static final String DELIVERY_STATUS_BROADCAST = "com.dpc.wtp.DELIVERY_STATUS_BROADCAST";

    public Custom_Send_Message(Context mContext, int sendTo, String textmsg, int messagetype)
    {
        this.cxt = mContext;
        this.sendTo = sendTo;
        this.textmsg = textmsg;
        this.messagetype = messagetype;
        db = new DatabaseHelper(cxt);
        net= new Net_Detect(cxt);

        if(messagetype==1)
        {
            sendMessageType="message";
        }
        else if(messagetype==2)
        {
            sendMessageType="image";
        }
        else if(messagetype==3)
        {
            sendMessageType="audio";
        }
        else if(messagetype==4)
        {
            sendMessageType="video";
        }
        else if(messagetype==5)
        {
            sendMessageType="sticker";
        }
        else if(messagetype==6)
        {
            sendMessageType="group_members_update";
        }
        else if(messagetype==7)
        {
            sendMessageType="invite";
        }
        else if(messagetype==8)
        {
            sendMessageType="group_name_update";
        }
        else if(messagetype==9)
        {
            sendMessageType="delivery_acknowledgement";
        }
        else if(messagetype==10)
        {
            sendMessageType="typing_status";
        }
        else if(messagetype==11)
        {
            sendMessageType="online_status_request";
        }
        else if(messagetype==12)
        {
            sendMessageType="online_status_response";
        }

    }

    public boolean send_simple_text_message(String msgid) {
        String localTime = get_current_time();
        String msgDateTime = get_universal_date(localTime);
        boolean sent = false;

        Message msg = new Message(sendTo+"@hellochat", Message.Type.chat);
        msg.setBody(textmsg);
        msg.setProperty("messagetype", sendMessageType); // text
        msg.setProperty("isgroupmessage", "0");
        msg.setProperty("messagesenttime", msgDateTime);
        msg.setProperty("sendersmsgid", msgid);

        if(net.isConnectingToInternet())
        {
            sent = true;
            WTP_Service.connection.sendPacket(msg);
            db.update_msg_status_to_sent(msgid);

            if(WTP_Service.CURRENT_CHAT_USER.equalsIgnoreCase(Integer.toString(sendTo)))
            {
                broadcastDeliveryStatus();
            }
        }
        return sent;

    }

    public boolean send_delivery_acknowledgement_message(String msgid) {
        String localTime = get_current_time();
        String msgDateTime = get_universal_date(localTime);
        boolean sent = false;

        Message msg = new Message(sendTo+"@hellochat", Message.Type.chat);
        msg.setBody("Message Delivered Successfully");
        msg.setProperty("messagetype", sendMessageType); // text
        msg.setProperty("isgroupmessage", "0");
        msg.setProperty("messagesenttime", msgDateTime);
        msg.setProperty("sendersmsgid", msgid);
        WTP_Service.connection.sendPacket(msg);
        return sent;

    }

    public void send_typing_status_to_single_user(String username, int status) // status - typing/ not typing
    {
        Message msg = new Message(sendTo+"@hellochat", Message.Type.chat);
        if(status == C.TYPING)
        {
            msg.setBody("s typing");
        }
        else if(status == C.STOPPED_TYPING)
        {
            msg.setBody("online");
        }

        msg.setProperty("isgroupmessage", "0");
        msg.setProperty("messagetype", sendMessageType); // text
        WTP_Service.connection.sendPacket(msg);
    }



    public void send_typing_status_to_group(String username, int status) {
        ArrayList<GroupDetails> group = new ArrayList<GroupDetails>();
        group = db.get_group_details(Integer.toString(sendTo));
        GroupDetails grpd = group.get(0);// 0 -as only 1 record

        List<String> receipents = new ArrayList<String>();
        receipents = db.get_all_group_members(Integer.toString(sendTo)); // without @hellochat
        receipents.remove(App.get_userid());

        for (int p = 0; p <= receipents.size() - 1; p++) {
            Message msg = new Message(receipents.get(p) + "@hellochat", Message.Type.chat);
            if(status == C.TYPING)
            {
                msg.setBody("user typing");
            }
            else if(status == C.STOPPED_TYPING)
            {
                msg.setBody("");
            }
            msg.setProperty("isgroupmessage", "1");
            msg.setProperty("servergroupid", grpd.getServergroupid());
            msg.setProperty("messagetype", sendMessageType);

            if(net.isConnectingToInternet())
            {
                WTP_Service.connection.sendPacket(msg);
            }
            else
            {
                db.insertPendingGroupMsgs(receipents.get(p),textmsg,sendMessageType,Integer.toString(grpd.getServergroupid()));
            }

        }
    }

    public void request_online_status_to_single_user()
    {
        Message msg = new Message(sendTo+"@hellochat", Message.Type.chat);
        msg.setBody("Request for your online status");
        msg.setProperty("isgroupmessage", "0");
        msg.setProperty("messagetype", sendMessageType); // text
        WTP_Service.connection.sendPacket(msg);
    }

    public void response_for_online_status_to_single_user(String status)
    {
        Message msg = new Message(sendTo+"@hellochat", Message.Type.chat);
        msg.setBody("Response contains status :"+status);
        msg.setProperty("onlinestatus", status);
        msg.setProperty("isgroupmessage", "0");
        msg.setProperty("messagetype", sendMessageType); // text
        WTP_Service.connection.sendPacket(msg);
    }




    public boolean send_group_message() {
        boolean sent = false;
        ArrayList<GroupDetails> group = new ArrayList<GroupDetails>();
        Log.d("Get group details by id", Integer.toString(sendTo));
        //String[] userid = sendTo.split("@hellochat");
        group = db.get_group_details(Integer.toString(sendTo));


        GroupDetails grpd = group.get(0);// 0 -as only 1 record

        List<String> receipents = new ArrayList<String>();
        receipents = db.get_all_group_members(Integer.toString(sendTo)); // without @hellochat
        receipents.remove(App.get_userid());
        Log.d("Group listwww", receipents.toString());

        String localTime = get_current_time();
        String msgDateTime = get_universal_date(localTime);

        for (int p = 0; p <= receipents.size() - 1; p++) {
            Message msg = new Message(receipents.get(p) + "@hellochat", Message.Type.chat);
            msg.setBody(textmsg);
            msg.setProperty("messagetype", sendMessageType);
            msg.setProperty("isgroupmessage", "1");
            msg.setProperty("groupname", grpd.getGroup_name());
            msg.setProperty("creationdate", grpd.getCreation_date());
            msg.setProperty("admin", grpd.getGroup_admin());
            msg.setProperty("groupusers", grpd.getGroup_users());
            msg.setProperty("servergroupid", grpd.getServergroupid());
            msg.setProperty("messagesenttime", msgDateTime);


            if(net.isConnectingToInternet())
            {
                WTP_Service.connection.sendPacket(msg);

                sent=true;
            }
            else
            {
                db.insertPendingGroupMsgs(receipents.get(p),textmsg,sendMessageType,Integer.toString(grpd.getServergroupid()));
            }

        }
        return sent;
    }


    public boolean send_group_members_update_message(String updateduserlist) {
        boolean sent = false;
        ArrayList<GroupDetails> group = new ArrayList<GroupDetails>();
        Log.d("Get group details by id", Integer.toString(sendTo));
        //String[] userid = sendTo.split("@hellochat");
        group = db.get_group_details(Integer.toString(sendTo));


        GroupDetails grpd = group.get(0);// 0 -as only 1 record

        List<String> receipents = new ArrayList<String>();
        receipents = db.get_all_group_members(Integer.toString(sendTo)); // without @hellochat
        receipents.remove(App.get_userid());
        Log.d("Group listwww", receipents.toString());

        String localTime = get_current_time();
        String msgDateTime = get_universal_date(localTime);

        for (int p = 0; p <= receipents.size() - 1; p++) {
            Message msg = new Message(receipents.get(p) + "@hellochat", Message.Type.chat);
            msg.setBody(textmsg);
            msg.setProperty("messagetype", sendMessageType);
            msg.setProperty("isgroupmessage", "1");
            msg.setProperty("groupname", grpd.getGroup_name());
            msg.setProperty("creationdate", grpd.getCreation_date());
            msg.setProperty("admin", grpd.getGroup_admin());
            msg.setProperty("groupusers", updateduserlist);
            msg.setProperty("servergroupid", grpd.getServergroupid());
            msg.setProperty("messagesenttime", msgDateTime);
            if(net.isConnectingToInternet())
            {
                WTP_Service.connection.sendPacket(msg);

                sent=true;
            }
            else
            {
                db.insertPendingGroupMsgs(receipents.get(p),textmsg,sendMessageType,Integer.toString(grpd.getServergroupid()));
            }

        }
        return sent;
    }




    public boolean send_pending_group_message(String userid, String groupid) {
        boolean sent = false;
        ArrayList<GroupDetails> group = new ArrayList<GroupDetails>();
        Log.d("Get group details by id", Integer.toString(sendTo));
        //String[] userid = sendTo.split("@hellochat");
        group = db.get_group_details(groupid);


        GroupDetails grpd = group.get(0);// 0 -as only 1 record




        String localTime = get_current_time();
        String msgDateTime = get_universal_date(localTime);


            Message msg = new Message(userid+ "@hellochat", Message.Type.chat);
            msg.setBody(textmsg);
            msg.setProperty("messagetype", sendMessageType);
            msg.setProperty("isgroupmessage", "1");
            msg.setProperty("groupname", grpd.getGroup_name());
            msg.setProperty("creationdate", grpd.getCreation_date());
            msg.setProperty("admin", grpd.getGroup_admin());
            msg.setProperty("groupusers", grpd.getGroup_users());
            msg.setProperty("servergroupid", grpd.getServergroupid());
            msg.setProperty("messagesenttime", msgDateTime);
            if(net.isConnectingToInternet())
            {
                sent = true;
                WTP_Service.connection.sendPacket(msg);

            }
            else
            {
                db.insertPendingGroupMsgs(userid,textmsg,sendMessageType,userid);
            }

        return sent;
    }

    public String get_current_time() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
        return sdf.format(c.getTime());
    }

    public String get_universal_date(String time) {
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm a");

        Date date = null;
        try {
            //Here you say to java the initial timezone. This is the secret
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = sdf.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //Here you set to your timezone

        sdf.setTimeZone(TimeZone.getDefault());
        time = sdf.format(date);
        return time;
    }

    private void broadcastDeliveryStatus()
    {
        Intent intent = new Intent(DELIVERY_STATUS_BROADCAST);
        cxt.sendBroadcast(intent);
    }


}
