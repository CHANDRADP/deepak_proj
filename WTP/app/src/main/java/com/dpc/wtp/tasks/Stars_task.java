package com.dpc.wtp.tasks;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.dpc.wtp.JSONParser;
import com.dpc.wtp.Models.Server_Params;
import com.dpc.wtp.Models.V_Details;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.activities.App;

public class Stars_task extends AsyncTask<String, Long, Boolean> {
	private boolean add = false;
	private String vd_id,stars_no;
	Context c;

	public Stars_task(Context c,String vd_id,String star_no,boolean add,String type) {
		this.c = c;
		this.vd_id = vd_id;
		this.stars_no = star_no;
		this.add = add;
		
		
	}

	
	@Override
	protected Boolean doInBackground(String... params) {
		
		return update_stars();
	}

	@Override
	protected void onPostExecute(Boolean result) {
		//dialog.dismiss();
		if (!result) 
		{
			C.alert(c, "Error in rating venue");
			
			
		}
		
		Intent i = new Intent(C.BROADCAST_MSG);
		i.putExtra(PARAMS.VENUE_DETAILS.get_param(), " ");
		c.sendBroadcast(i);

	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
	}
	
	public boolean update_stars()
	{
		boolean success = false;
		JSONParser parser = new JSONParser();
		JSONObject user = null;
		try {
		if(add)
		{
		user = parser.makeHttpRequest(C.SERVER_URL, "POST",
				new Server_Params().get_add_star_params(App.get_userid(), vd_id,stars_no));
			if(user.getInt(PARAMS.STATUS.get_param()) > 0)
			{
				V_Details club = C.clubs.get(vd_id);
				club.set_rating(user.getInt(PARAMS.STATUS.get_param()));
				C.update_clubs(club);
				success = true;
			}
		}
		else
		{
			user = parser.makeHttpRequest(C.SERVER_URL, "POST",
					new Server_Params().get_update_star_params(App.get_userid(), vd_id,stars_no));
			if(user.getInt(PARAMS.STATUS.get_param()) > 0)
			{
				V_Details club = C.clubs.get(vd_id);
				club.set_rating(user.getInt(PARAMS.STATUS.get_param()));
				C.update_clubs(club);
				success = true;
			}
		}
		
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return success;
	}

}