package com.dpc.wtp.adapters;


import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.dpc.wtp.R;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.activities.App;

public class Settings_Adapter extends BaseAdapter {
	LayoutInflater inflater;
	ViewHolder holder;
	View v;
	List<String> items = new ArrayList<String>();
	Context c;
	private class ViewHolder {
		Text item;
		
		
	}
	public Settings_Adapter(Context c,List<String> l)
	{
		this.c = c;
		this.items = l;
		
	}
	
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		String txt = items.get(position);
		
		v = convertView;
		if (v == null) 
		{
			 inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 v = inflater.inflate(R.layout.row_settings, null);
			 holder = new ViewHolder();
			 holder.item = (Text) v.findViewById(R.id.item);
			 v.setTag(holder);
			 
		}
		else
			holder = (ViewHolder) v.getTag();

		if(position == 2) {
            holder.item.setText(txt +" "+ App.get_version_from_manifest(c));
        }
		else
		holder.item.setText(Html.fromHtml(txt));
		
		
		
		return v;

	}


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}


	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
	}


	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

}