package com.dpc.wtp.tasks;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;

import com.dpc.wtp.JSONParser;
import com.dpc.wtp.Models.Server_Params;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.activities.App;

public class Invite_Task extends AsyncTask<String, Long, Boolean> {
	private String event_id;
	private String[] uids;
	Context c;

	public Invite_Task(Context c,String[] uids,String event_id) {
		this.c = c;
		this.uids = uids;
		this.event_id = event_id;
		
		
	}

	
	@Override
	protected Boolean doInBackground(String... params) {
		
		return send_invite();
	}

	@Override
	protected void onPostExecute(Boolean result) {


		if (result) 
		{
				C.alert(c, "Invitation sent successfully.");
				
		}
		else
		{
			//C.alert(c, "Failure to invite");
		}

	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
	}
	
	public boolean send_invite()
	{
		boolean success = false;
		JSONParser parser = new JSONParser();
		JSONObject user = null;
		try {
		
		user = parser.makeHttpRequest(C.SERVER_URL, "POST",
				new Server_Params().get_invite_params(uids, event_id));
            if(user != null)
            if(user.has(PARAMS.STATUS.get_param()))
			if(user.getInt(PARAMS.STATUS.get_param()) > 0)
			{
				success = true;
			}
		
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return success;
	}

}