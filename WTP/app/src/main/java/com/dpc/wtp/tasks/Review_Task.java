package com.dpc.wtp.tasks;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.dpc.wtp.JSONParser;
import com.dpc.wtp.Models.Server_Params;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.activities.App;

public class Review_Task extends AsyncTask<String, Long, Boolean> {
	private String vd_id,review;
	Context c;

	public Review_Task(Context c,String vd_id,String review) {
		this.c = c;
		this.vd_id = vd_id;
		this.review = review;
		
	}

	
	@Override
	protected Boolean doInBackground(String... params) {
		
		return update_reviews();
	}

	@Override
	protected void onPostExecute(Boolean result) {
		//dialog.dismiss();
		if (!result) 
		{
			C.alert(c, "Error in updating review");
			
		}
		
		Intent i = new Intent(C.BROADCAST_MSG);
		i.putExtra(PARAMS.VENUE_DETAILS.get_param(), " ");
		c.sendBroadcast(i);

	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
	}
	
	public boolean update_reviews()
	{
		boolean success = false;
		JSONParser parser = new JSONParser();
		JSONObject user = null;
		try {
		user = parser.makeHttpRequest(C.SERVER_URL, "POST",
				new Server_Params().get_add_review_params(App.get_userid(), vd_id,review));
			if(user.getInt(PARAMS.STATUS.get_param()) > 0)
			{
				success = true;
			}
		
		
		
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return success;
	}

}