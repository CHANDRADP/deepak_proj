package com.dpc.wtp.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class Text extends TextView{
	Typeface tfn,tfb;
    public Text(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public Text(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Text(Context context) {
        super(context);
        init();
    }

    private void init() {
        tfn = Typeface.createFromAsset(getContext().getAssets(),"Proxima.otf");
        tfb = Typeface.createFromAsset(getContext().getAssets(),"Proximab.otf");
        change_to_normal();
    }
    public void change_to_bold()
    {
    	setTypeface(tfb);
    	//setTypeface(Typeface.DEFAULT_BOLD);
    }
    
    public void change_to_normal()
    {
    	setTypeface(tfn);
    	//setTypeface(Typeface.DEFAULT);
    }

	

}
