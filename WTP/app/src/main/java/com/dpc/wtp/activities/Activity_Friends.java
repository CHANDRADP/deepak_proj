package com.dpc.wtp.activities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.dpc.wtp.JSONParser;
import com.dpc.wtp.R;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Models.Server_Params;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.Utils.TextB;
import com.dpc.wtp.adapters.Favorite_tabs_pager_adapter;
import com.dpc.wtp.customs.MyActivity;
import com.google.android.gms.analytics.HitBuilders;

public class Activity_Friends extends MyActivity {

	private TabHost tabHost;

	private Favorite_tabs_pager_adapter pager_adapter;

	private ViewPager viewPager;

	private String[] tabs = new String[] { "Friends", "WTP Friends" };

	private String event_id = null;



    @Override
    public void onCreate(Bundle savedInstanceState) {
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.actionbar);
		final Text title = (Text)findViewById(android.R.id.title);
		title.setText(R.string.ac_invite_frnds);
		super.onCreate(savedInstanceState);
        app_tracker.setScreenName(C.SCREEN.SCREEN_INVITE_FRIENDS.get_screen());
        app_tracker.send(new HitBuilders.AppViewBuilder().build());
		if(this.getIntent().hasExtra(PARAMS.EVENT_ID.get_param()))
		{
			event_id = this.getIntent().getStringExtra(PARAMS.EVENT_ID.get_param());
		}
		setContentView(R.layout.frag_tabs);
        viewPager = (ViewPager)findViewById(R.id.pager);
        
        tabHost=(TabHost)findViewById(R.id.tabHost);
        
        tabHost.setup();
        pager_adapter = new Favorite_tabs_pager_adapter(getSupportFragmentManager(),tabs.length,1,event_id);
		viewPager.setOffscreenPageLimit(tabs.length);
        viewPager.setAdapter(pager_adapter);
		tabHost.setOnTabChangedListener(new OnTabChangeListener()
		{

			@Override
			public void onTabChanged(String id) 
			{
				
				viewPager.setCurrentItem(tabHost.getCurrentTab());
				if(pager_adapter != null)
				pager_adapter.notifyDataSetChanged();
			}
		});
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				// on changing the page
				// make respected tab selected
				tabHost.setCurrentTab(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
		for(int i = 0;i < tabs.length;i++)
		{
			setupTab(i);
		}


    }
	private void setupTab(final int i) {
		TabSpec spec = tabHost.newTabSpec(String.valueOf(i));
		spec.setIndicator(createTabView(this,i,spec));
        tabHost.addTab(spec);
	}
    private  View createTabView(final Context context,final int i,TabSpec spec) {
    	
    	View view = null;
    	
    	spec.setContent(R.id.view);
    	view = LayoutInflater.from(context).inflate(R.layout.tabs_bg, null);
    	Text txt = (Text)view.findViewById(R.id.txt);
    	TextB txtb = (TextB)view.findViewById(R.id.txtbold);
    	
    	txt.setText(tabs[i]);
    	txtb.setText(tabs[i]);
        return view;
    }

    @Override
    protected void onResume() {
        super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
    }

    @Override
    protected void onPause() {
        super.onPause();
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
    	return super.onCreateOptionsMenu(menu);
    }
	 @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId())
		{
	
		case android.R.id.home:
			super.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
    }
    /*
    public class Get_Events extends AsyncTask<String, Long, JSONObject> {
		
		
		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(getSherlockActivity());
			dialog.setMessage("Loading please wait...");
			dialog.setCancelable(false);
			//dialog.show();

		}

		@Override
		protected JSONObject doInBackground(String... params) {
			return get_events();
		}

		

		@Override
		protected void onPostExecute(JSONObject obj) {
			dialog.dismiss();
			if(obj != null)
			{
				if(obj.has(C.PARAMS.STATUS.get_param()))
				{
					try {
						int status = obj.getInt(C.PARAMS.STATUS.get_param());
						if(status > 0)
						{
							JSONArray event_array = obj.getJSONArray(C.PARAMS.DATA.get_param());
							for(int i = 0;i < event_array.length();i++)
							{
								JSONObject event = event_array.getJSONObject(i);
								
								/////// get event images
								JSONArray images_array = event.getJSONArray(C.PARAMS.IMAGES.get_param());
								
								ArrayList<Images> images = new ArrayList<Images>();
								for(int j = 0;j < images_array.length();j++)
								{
									JSONObject image = images_array.getJSONObject(j);
									images.add(new Images(image.getString(C.PARAMS.IMAGE.get_param()),
														  image.getString(C.PARAMS.THUMBNAIL.get_param())
											             ));
								}
							/////// get event artist and details
								Artist artist = null;
								if(!event.isNull(C.PARAMS.ARTIST.get_param()))
								{
								JSONObject artist_obj = event.getJSONObject(C.PARAMS.ARTIST.get_param());
								///////////////get artist tracks and details
														JSONArray tracks_array = artist_obj.getJSONArray(C.PARAMS.TRACKS.get_param());
														ArrayList<Tracks> tracks = new ArrayList<Tracks>();
														for(int k = 0;k < tracks_array.length();k++)
														{
															JSONObject track_obj = tracks_array.getJSONObject(k);
															tracks.add(new Tracks(track_obj.getString(C.PARAMS.TRACK_ID.get_param()),
																				  track_obj.getString(C.PARAMS.ARTIST_ID.get_param()),
																				  track_obj.getString(C.PARAMS.NAME.get_param()),
																				  track_obj.getString(C.PARAMS.DETAILS.get_param())
																				  ));
															
														}
														
								artist = new Artist(artist_obj.getString(C.PARAMS.ARTIST_ID.get_param()),
														   artist_obj.getString(C.PARAMS.NAME.get_param()),
														   artist_obj.getString(C.PARAMS.PLACE.get_param()),
														   artist_obj.getString(C.PARAMS.GENRE.get_param()),
														   artist_obj.getString(C.PARAMS.FOLLOWING.get_param()),
														   artist_obj.getString(C.PARAMS.FOLLOW_COUNT.get_param()),
														   artist_obj.getString(C.PARAMS.IMAGE.get_param()),
														   artist_obj.getString(C.PARAMS.THUMBNAIL.get_param()),
														   tracks
														   );
								}
								
								list_items.add(new Event(event.getString(C.PARAMS.V_ID.get_param()),
										 event.getString(C.PARAMS.NAME.get_param()),
										 event.getString(C.PARAMS.TYPE.get_param()),
										 event.getString(C.PARAMS.CONTACT_INFO.get_param()),
										 event.getString(C.PARAMS.ADDRESS.get_param()),
										 event.getString(C.PARAMS.AREA.get_param()),
										 event.getString(C.PARAMS.POST_CODE.get_param()),
										 event.getString(C.PARAMS.CITY.get_param()),
										 event.getString(C.PARAMS.STATE.get_param()),
										 event.getString(C.PARAMS.FAVORITE_COUNT.get_param()),
										 event.getString(C.PARAMS.ID.get_param()),
										 event.getString(C.PARAMS.EVENT_NAME.get_param()),
										 event.getString(C.PARAMS.DATE.get_param()),
										 event.getString(C.PARAMS.S_TIME.get_param()),
										 event.getString(C.PARAMS.E_TIME.get_param()),
										 event.getString(C.PARAMS.MUSIC_TYPE.get_param()),
										 event.getString(C.PARAMS.GUEST_COUNT.get_param()),
										 event.getInt(C.PARAMS.ATTEND.get_param()),
										 images,
										 artist,
										 event.getInt(C.PARAMS.FAVORITE.get_param())
										 ));
							}
							
							if(list_items.size() > 0)
							{
								Collections.sort(list_items, new Sort_Date());
								for(int i = 0;i < list_items.size();i++)
								{
									Log.d("date", list_items.get(i).get_e_date());
								}
								C.events.clear();
								SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
								ArrayList<Event> event = new ArrayList<Event>();
								for(int i = 0;i < list_items.size();i++)
								{
									try {
										Date d0 = format.parse(list_items.get(i).get_e_date());
										Date d1;
											if(i < list_items.size() - 1)
											{
												d1 = format.parse(list_items.get(i+1).get_e_date());
												if(d0.compareTo(d1) != 0)
												{
													event.add(list_items.get(i));
													C.events.add(event);
													event = new ArrayList<Event>();
												}
												else
												{
													event.add(list_items.get(i));
												}
											}
											else
											{
												event.add(list_items.get(i));
												C.events.add(event);
											}
										
									} catch (ParseException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								for(int i = 0;i < C.events.size();i++)
								{
									setupTab(i);
									
								}
								pager_adapter = new Calendar_Tabs_Pager_Adapter(getChildFragmentManager(),C.events.size());
								viewPager.setOffscreenPageLimit(C.events.size());
						        viewPager.setAdapter(pager_adapter);
							}
							
							
						}
						else
						{
							C.alert(getSherlockActivity(), "Please try again after some time");
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				else
				{
					C.alert(getSherlockActivity(), "Please try again after some time");
				}
			}
			
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
		}

	}
 */
 public JSONObject get_events()
	{
		JSONParser parser = new JSONParser();
		JSONObject user;
		user = parser.makeHttpRequest(C.SERVER_URL, "POST",
										new Server_Params().get_venue_params
										(
										App.get_userid()
										));
		
		return user;
	}
 
 public class Sort_Date implements Comparator<Event> {
	    @SuppressLint("SimpleDateFormat")
		public int compare(Event c1, Event c2) {
	    	SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	    	Date left = null,right = null;
			try {
				left = format.parse(c1.get_event_date());
				right = format.parse(c2.get_event_date());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	 
	        return left.compareTo(right);
	    }
	}


   
}
