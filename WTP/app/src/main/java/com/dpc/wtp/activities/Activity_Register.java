package com.dpc.wtp.activities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.dpc.wtp.JSONParser;
import com.dpc.wtp.R;
import com.dpc.wtp.Models.Server_Params;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.DatabaseHelper;

import com.dpc.wtp.Utils.EText;
import com.dpc.wtp.Utils.MyAlert.OnOKListener;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.customs.DialogwithListview;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.customs.MyProgress;
import com.dpc.wtp.interfaces.ListViewClickListener;
import com.dpc.wtp.tasks.Update_FB_TWIT;
import com.facebook.Session;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.analytics.HitBuilders;

public class Activity_Register extends MyActivity implements OnClickListener,ListViewClickListener,OnOKListener{
	
	private EText name,email,password,number,dob;
	
	private Button signup;
	
	private LoginButton loginButton;

    private LinearLayout fb,reg;
	
	private String gcmId;
	
	private ArrayList<String> mail_ids;
	
	private int day,month,year;
	
	@SuppressLint("SimpleDateFormat")
	private SimpleDateFormat date1 = new SimpleDateFormat("dd/MM/yyyy");
	
	private DatabaseHelper h;
	
	private Text title;
	
	private Net_Detect net;
	
	private void init_net_details()
	{
		net = new Net_Detect(this);
		
	}
	
	
   
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.actionbar);
		
		title = (Text)findViewById(android.R.id.title);
		title.setText(R.string.ac_signup);
		register();
		super.onCreate(savedInstanceState);
        app_tracker.setScreenName(C.SCREEN.SCREEN_REGISTER.get_screen());
        app_tracker.send(new HitBuilders.AppViewBuilder().build());
		init_net_details();
		setContentView(R.layout.ac_register);
		set_layout();
		final Calendar c = Calendar.getInstance();
		day = c.get(Calendar.DAY_OF_MONTH);
		month = c.get(Calendar.MONTH);
		year = c.get(Calendar.YEAR);
		final AccountManager manager = AccountManager.get(this);
		final Account[] accounts = manager.getAccountsByType("com.google");
		final int size = accounts.length;
		mail_ids = new ArrayList<String>();
		for (int i = 0; i < size; i++) 
		{
			mail_ids.add(accounts[i].name);
		}


	}

	

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
		Session session = Session.getActiveSession();
        boolean fb_loggedin = (session != null && session.isOpened());
        if(!fb_loggedin && App.get_userid() != null)
        {
        	reg.setVisibility(View.GONE);
        	fb.setVisibility(View.VISIBLE);
        }
	}
	

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;
		
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
	}
	@Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        
        
    }
	
	 @Override
	    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	        super.onActivityResult(requestCode, resultCode, data);
	        
	    }
	 @Override
	    public boolean onCreateOptionsMenu(Menu menu) 
	    {
	    	
				
	        return super.onCreateOptionsMenu(menu);
	    }
		boolean search = false;
	    @Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// TODO Auto-generated method stub
			switch(item.getItemId())
			{
		
			case android.R.id.home:
				super.onBackPressed();
				break;
			}
			return super.onOptionsItemSelected(item);
	    }

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	
	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
		case R.id.signup:
            if(App.get_gcmid() == null)
            {
                register();
                C.alert(getApplicationContext(),"Device not yet registered, please try again");
            }
			check_user_details();
			break;
		case R.id.email:
			Bundle b = new Bundle();
			b.putString("head", "Select a Gmail ID");
			DialogwithListview dialog1 = new DialogwithListview(mail_ids);
			dialog1.setArguments(b);
			dialog1.list_listener = Activity_Register.this;
			dialog1.show(getSupportFragmentManager(), "");
			
			break;
		case R.id.dob:
			showDialog(1);
			break;
		
		}
		
	}
	@Override
	public void setOnSubmitListener(int value) {
		// TODO Auto-generated method stub
		email.setText(mail_ids.get(value));
	}
	@Override
	public void setOKSubmitListener(int value) {
		// TODO Auto-generated method stub
		
	}
	private void set_layout() {
		reg = (LinearLayout)findViewById(R.id.reg);
		fb = (LinearLayout)findViewById(R.id.fb);
		
		loginButton = (LoginButton) findViewById(R.id.login_button);
		
		signup = (Button)findViewById(R.id.signup);
		
		name = (EText)findViewById(R.id.name);
		email = (EText)findViewById(R.id.email);
		password = (EText)findViewById(R.id.password);
		number = (EText)findViewById(R.id.number);
		dob = (EText)findViewById(R.id.dob);
		
		
		signup.setOnClickListener(this);
		email.setOnClickListener(this);
		dob.setOnClickListener(this);
		
		dob.setOnFocusChangeListener(new OnFocusChangeListener(){

			@SuppressWarnings("deprecation")
			@Override
			public void onFocusChange(View arg0, boolean focused) {
				// TODO Auto-generated method stub
				if(focused)
				{
					showDialog(1);
				}
			}
			
		});
		loginButton.setReadPermissions(Arrays.asList("public_profile","user_friends"));
		loginButton.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
            @Override
            public void onUserInfoFetched(GraphUser user) {
            	if(user != null)
                {
                	if(App.get_userid() != null)
                	{
                		App.set_fbid(user.getId());
                		new Update_FB_TWIT(true).execute();
                		//if(!sharing)
                	 //   	open_share_dialog(Session.getActiveSession());
                		Intent i = new Intent(Activity_Register.this,Activity_City.class);
                		startActivity(i);
                		finish();
                	}
                }
               
                
            }
        });

		
		
	}
	
	
private void check_user_details() 
{
    String _name = name.getText().toString();
    _name.trim();
	if (_name != null && _name.length() > 0)
	{

        String _email = email.getText().toString();
        _email.trim();
		if (_email != null	&& _email.length() > 0	&& _email.contains("@"))
		{

            String _pass = password.getText().toString();
            _pass.trim();
			if (_pass != null&& _pass.length() >= 6)
			{
                String _num = number.getText().toString();
                _num.trim();
				if (_num != null	&& _num.length() == 10)
				{
					if (dob.getText().toString() != null && dob.getText().toString().length() > 9) 
					{
						if(net.isConnectingToInternet())
						{
							new Register_User().execute();
						}
						else
						{
							
							net.alert(getSupportFragmentManager());
							return;
						}
					}
					else 
					{
						C.alert(getApplicationContext(),
								"Please provide your date of birth");
					}
				}
				else 
				{
					C.alert(getApplicationContext(),
							"Please enter proper phone number");
				}
				
				
				
			} 
			else 
			{
				C.alert(getApplicationContext(),
						"Password length should be more than 5");
			}
		}
		else 
		{
				C.alert(getApplicationContext(),
						"Please enter proper email address.");
		}
		
		
	}
	else
	{
		C.alert(getApplicationContext(),
				"Please enter proper name");
	}

}


@Override
protected Dialog onCreateDialog(int id) {
	
		return new DatePickerDialog(this, startdatePickerListener, year, month,day);
} 
private DatePickerDialog.OnDateSetListener startdatePickerListener = new DatePickerDialog.OnDateSetListener() {

	// when dialog box is closed, below method will be called.
	public void onDateSet(DatePicker view, int selectedYear,
			int selectedMonth, int selectedDay) {
		Calendar calendar = Calendar.getInstance();
		
		calendar.set(selectedYear, selectedMonth, selectedDay);
		String date = date1.format(calendar.getTime());
		
		dob.setText(date);
	}
};

///////////////////////////////////////////
public class Register_User extends AsyncTask<Void, Void, Boolean> {
	private MyProgress dialog = new MyProgress(Activity_Register.this);
	boolean success = false,user_exist = false;
	@Override
	protected void onPreExecute() {
		dialog.setMessage("Loading please wait...");
		dialog.setCancelable(false);
		dialog.show();

	}

	@Override
	protected Boolean doInBackground(Void... params) {
		JSONObject obj = register_user();
		if(obj != null)
		{
			
			try {
				
				if(obj.has(PARAMS.STATUS.get_param()))
				{
					int status = obj.getInt(PARAMS.STATUS.get_param());
					if(status == 0)
					{
						user_exist = true;
						
					}
					else if(status > 0)
					{
						String thmbnail = null,image = null,points = "0";
						boolean tuto = false;
						if(!obj.isNull(PARAMS.THUMBNAIL.get_param()))
						{
							thmbnail = PARAMS.THUMBNAIL.get_param();
							image = PARAMS.IMAGE.get_param();
						}
						String token = null;
						if(!obj.isNull(PARAMS.TOKEN.get_param()))
							token = obj.getString(PARAMS.TOKEN.get_param());
						
						if(!obj.isNull(PARAMS.POINTS.get_param()))
							points = obj.getString(PARAMS.POINTS.get_param());
						
						if(!obj.isNull(PARAMS.TUTORIAL.get_param()))
							tuto = obj.getInt(PARAMS.TUTORIAL.get_param()) == 1 ? true : false;
						
						App.set_user_details(obj.getString(PARAMS.USER_ID.get_param()),name.getText().toString(),email.getText().toString(),"+91",number.getText().toString(),image,thmbnail,points,token,tuto);
						
						publishProgress();
			        	success = contactstask();
			        	
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return user_exist;
	}

	@Override
	protected void onProgressUpdate(Void... values) {
		super.onProgressUpdate(values);
		
			C.alert(getApplicationContext(), "User registered successfully.");
			//dialog.setMessage("Reading contacts...");
			
			
		
		
	}
	@Override
	protected void onPostExecute(Boolean result) {
		dialog.dismiss();
		if(result)
		{
			C.alert(getApplicationContext(), "Given user already exist");
			
		}
		else if(success)
		{
		//	Intent i = new Intent(Activity_Register.this,Activity_Home.class);
		//	startActivity(i);
		//	finish();
			//reg.setVisibility(View.GONE);
			if(!App.is_fb_loggedin())
			{
                Intent i = new Intent(Activity_Register.this,Activity_Facebook.class);
                startActivity(i);
                finish();
			}
			else
			{
				Intent i = new Intent(Activity_Register.this,Activity_City.class);
					startActivity(i);
					finish();
			}
			
		}
		
		
	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
	}

}

public JSONObject register_user()
{
	JSONParser parser = new JSONParser();
	JSONObject user = null;
	
	user = parser.makeHttpRequest(C.SERVER_URL, "POST",
					new Server_Params().get_register_params
					(
					name.getText().toString(),
					email.getText().toString(),
					password.getText().toString(),
					"+91",
					number.getText().toString(),
					dob.getText().toString(),
					App.get_gcmid(),
					C.get_date(),
					null,
					null
					));
	
	return user;
}

public boolean contactstask()
{
	boolean success = false;;
	JSONObject user;
	ArrayList<String> nos = null;
	JSONParser parser = new JSONParser();
		
    	ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		
		params.add(new BasicNameValuePair(C.METHOD.GET_APP_USERS.get_method()," "));
		if(!App.is_contacts_updated())
		nos = C.readContacts(getApplicationContext());
		if(nos != null)
		{
			success = true;
			App.set_contacts_updated(true);
			for(int i = 0;i < nos.size();i++)
			{
				params.add(new BasicNameValuePair(PARAMS.NUMBER.get_param()+"[]",nos.get(i)));
				h = new DatabaseHelper(this);
				h.add_new_android_contact(nos.get(i));
				
			}
			params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),App.get_userid()));
			params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
		
		}
		user = parser.makeHttpRequest(C.SERVER_URL, "POST", params);
		if(user != null)
		{
			try {
				if(user.getInt(PARAMS.STATUS.get_param()) > 0)
				{
					C.add_app_users_todb(this,user);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
			
    	return success;
   
}
//////////////////////////////////////
void register()
{
	gcmId = GCMRegistrar.getRegistrationId(this.getApplicationContext());
	if (gcmId.equals("")) 
	{
		GCMRegistrar.register(this.getApplicationContext(), C.SENDER_ID);
	} 
	else 
	{
		
		final Context context = this.getApplicationContext();
		if (GCMRegistrar.isRegisteredOnServer(this.getApplicationContext())) 
		{
			
			gcmId = GCMRegistrar.getRegistrationId(this.getApplicationContext());
            App.update_gcmid(gcmId);
		//	Register_Task task = new Register_Task(context,gcmId);
		//	task.execute();
		} 
		else 
		{
			
			GCMRegistrar.setRegisteredOnServer(context, true);
					
		}
		
	}
	Log.i("gcmId", "Device registered: regId = " + gcmId);
}



}
