package com.dpc.wtp.customs;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.activities.Activity_Event_Details;


public class OAuthActivity extends MyActivity {
	private WebView webView;
    public void onCreate(Bundle savedInstanceState) {
    	getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.actionbar);
		final Text title = (Text)findViewById(android.R.id.title);
		title.setText("Twitter login");
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.oauth_webview);
        webView = (WebView)findViewById(R.id.webViewOAuth);

        String authenticationUrl = getIntent().getStringExtra(C.STRING_EXTRA_AUTHENCATION_URL);
        webView.loadUrl(authenticationUrl);
        webView.setWebViewClient(new WebViewClient()
        {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.contains("oauth_verifier="))
                {

                    Intent intent = new Intent(getApplicationContext(), Activity_Event_Details.class);
                    intent.setData( Uri.parse(url));
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(intent);
                    finish();
                }
                view.loadUrl(url);
                return true;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
    	
			
        return super.onCreateOptionsMenu(menu);
    }
	
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId())
		{
	
		case android.R.id.home:
			super.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
    }

}