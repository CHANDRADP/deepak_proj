package com.dpc.wtp.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.dpc.wtp.Models.ChatMsg;
import com.dpc.wtp.Models.Download;
import com.dpc.wtp.Models.GroupDetails;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.CirclePageIndicator;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.EText;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.adapters.ViewPager_Adapter;
import com.dpc.wtp.amazon.UploadService;
import com.dpc.wtp.customs.CustomDialogWithTwoButtons;
import com.dpc.wtp.customs.Custom_Send_Message;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.interfaces.CustomDilogInterface;
import com.dpc.wtp.services.WTP_Service;

import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import android.os.Handler;

import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ActivityChatWindow extends MyActivity
{
    private static EText mSendText;
    private ListView mList;
    LinearLayout gridview;
    RelativeLayout contents;
    MediaPlayer player=null;
    Handler mHandler;
    Timer timer;
    TimerTask task;
    String chat_subtitle = "";
    int sent_status = C.STATUS_NOT_SENT;

    int mycount = 0;
    int mynextcount = 0;

    private static SharedPreferences Smooch_data;
    private BroadcastReceiver broadcastReceiver;
    private BroadcastReceiver uploadbroadcastReceiver;
    private BroadcastReceiver deliveryStatusbroadcastReceiver;
    private MyAdapter myAdapter;
    private Net_Detect net;


    private ViewPager viewPager;



    public static final int[] smileys = new int[] {R.drawable.a,
            R.drawable.b,
            R.drawable.c,
            R.drawable.d,
            R.drawable.e,
            R.drawable.f,
            R.drawable.g,
            R.drawable.h,

    };

    public static final int[] stickers = new int[] {R.drawable.a,
            R.drawable.b,
            R.drawable.c,
            R.drawable.d,
            R.drawable.e,
            R.drawable.f,
            R.drawable.g,
            R.drawable.h,

    };

    public static final String[] chars = new String[]{	":)",
            ":-)",
            ":_)",
            "::)",
            ":+)",
            ":))",
            ":()",
            ":*)",
    };
    public static Context cxt = null;


    private static final Spannable.Factory spannableFactory = Spannable.Factory.getInstance();

    public static final Map<Pattern, Integer> emoticons = new HashMap<Pattern, Integer>();
    private static void addPattern(String smile,
                                   int resource)
    {
        emoticons.put(Pattern.compile(Pattern.quote(smile)), resource);
    }
    public static boolean addSmiles(Context context, Spannable spannable)
    {
        boolean hasChanges = false;
        for (Map.Entry<Pattern, Integer> entry : emoticons.entrySet())
        {
            Matcher matcher = entry.getKey().matcher(spannable);
            while (matcher.find())
            {
                boolean set = true;
                for (ImageSpan span : spannable.getSpans(matcher.start(),
                        matcher.end(), ImageSpan.class))
                    if (spannable.getSpanStart(span) >= matcher.start()
                            && spannable.getSpanEnd(span) <= matcher.end())
                        spannable.removeSpan(span);
                    else
                    {
                        set = false;
                        break;
                    }
                if (set)
                {
                    hasChanges = true;
                    Drawable drawable = context.getResources().getDrawable(entry.getValue());
                    spannable.setSpan(new ImageSpan(scaleDrawable(drawable, 30, 30)),
                            matcher.start(), matcher.end(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
        }
        return hasChanges;
    }


    void set_extras_header(int num)
    {
        if (num == 1)
        {
            gridview.setVisibility(View.VISIBLE);
        }
    }

    static public Drawable scaleDrawable(Drawable drawable, int width, int
            height)
    {
        int wi = drawable.getIntrinsicWidth();
        int hi = drawable.getIntrinsicHeight();
        int dimDiff = Math.abs(wi - width) - Math.abs(hi - height);
        float scale = (dimDiff > 0) ? width/(float)wi : height/
                (float)hi;
        Rect bounds = new Rect(0, 0, (int)(scale*wi), (int)(scale*hi));
        drawable.setBounds(bounds);
        return drawable;
    }
    public static Spannable getSmiledText(Context context, CharSequence text) {
        Spannable spannable = spannableFactory.newSpannable(text);
        addSmiles(context, spannable);
        return spannable;
    }

    Text title;
    Button btnselect;

    ArrayList<ChatMsg> chats;
    //ArrayList<Integer> multiselectPositionsList = new ArrayList<Integer>();
    ArrayList<String> multiselectMsgIdList = new ArrayList<>();

    int flag_multiselect=0;
    int smiley_toggle = 0;


    int userid=0, isgroup=0, unreadcount=0, chatid=0;




    int focusPosition=0;

    CharSequence get_char(final int posi)
    {

        Html.ImageGetter imageGetter = new Html.ImageGetter() {


            @Override
            public Drawable getDrawable(String source) {
                Drawable d = getResources().getDrawable(ActivityChatWindow.smileys[posi]);
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                return d;
            }
        };

        return	Html.fromHtml(
                "<img src ='"
                        + getResources().getDrawable(ActivityChatWindow.smileys[posi])
                        + "'/>", imageGetter, null);
    }

    public static void update_txt(Activity a, final int posi)
    {
        mSendText.getText().insert(mSendText.getSelectionStart(), getSmiledText(a, chars[posi]));
    }


    @Override
    public void onCreate(Bundle bundle)
    {

        cxt = this;

        C.ACTIVITY_CHAT_WINDOW_RUNNING_STATUS = C.ACTIVITY_RUNNING;
        for(int i = 0;i < ActivityChatWindow.smileys.length;i++)
        {
            addPattern(ActivityChatWindow.chars[i], ActivityChatWindow.smileys[i]);
        }
        Smooch_data = PreferenceManager.getDefaultSharedPreferences(cxt);

        Intent i = getIntent();
        if(i.hasExtra("userid"))
        {
            userid = i.getIntExtra("userid",0);
            WTP_Service.CURRENT_CHAT_USER = Integer.toString(userid);
        }
        if(i.hasExtra("isgroup"))
        {
            isgroup = i.getIntExtra("isgroup",0);
        }
        if(i.hasExtra("unreadcount"))
        {
            unreadcount = i.getIntExtra("unreadcount",0);
        }
        if(i.hasExtra("chatid"))
        {
            chatid = i.getIntExtra("chatid",0);
        }

        mHandler = new Handler();

        Log.d("IsGroupfffffffffffffff", isgroup+" "+userid+" "+chatid);
        focusPosition = unreadcount;



        db = new DatabaseHelper(this);

        if(isgroup==0)
        {
            get_User_Mode(Integer.toString(userid)+"@hellochat/Smack");
        }





        init_net_details();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar);


        title = (Text)findViewById(android.R.id.title);

        super.onCreate(bundle);
        Log.i("XMPPClient", "onCreate called");
        setContentView(R.layout.activity_chat_window);



        mSendText = (EText) this.findViewById(R.id.sendText);
        mList = (ListView) this.findViewById(R.id.listMessages);
        //multiselectPositionsList = new ArrayList();
        multiselectMsgIdList = new ArrayList<>();
        btnselect = (Button) findViewById(R.id.btnselect);
        gridview = (LinearLayout)findViewById(R.id.smileygrid);
        contents = (RelativeLayout) findViewById(R.id.contents);

        contents.setVisibility(View.GONE); // so that smileys should be hidden by default


        myAdapter = new MyAdapter(this, R.id.listMessages, chats);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        ViewPager_Adapter adap = new ViewPager_Adapter(this,null,false);
        viewPager.setAdapter(adap);
        CirclePageIndicator mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        mIndicator.setViewPager(viewPager);


        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener(){

            @Override
            public void onPageScrollStateChanged(int position) {}
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {}
            @Override
            public void onPageSelected(int position) {
                // TODO Auto-generated method stub
                switch(position){
                    case 0:
                        findViewById(R.id.first_tab).setVisibility(View.VISIBLE);
                        findViewById(R.id.second_tab).setVisibility(View.INVISIBLE);
                        break;

                    case 1:
                        findViewById(R.id.first_tab).setVisibility(View.INVISIBLE);
                        findViewById(R.id.second_tab).setVisibility(View.VISIBLE);
                        break;
                }
            }

        });

        btnselect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Code to hide keypad
                InputMethodManager imm = (InputMethodManager)getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mSendText.getWindowToken(), 0);

                //code for smiley keypad
                if(smiley_toggle==0)
                {
                    contents.setVisibility(View.VISIBLE);
                    //contents.getLayoutParams().height = 100;

                    set_extras_header(1);
                    //InputMethodManager inputMethodManager=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    //inputMethodManager.hideSoftInputFromWindow(mSendText.getApplicationWindowToken(), 0);
                    smiley_toggle=1;
                }
                else
                {
                    contents.setVisibility(View.GONE);
                    smiley_toggle=0;
                }


            }
        });


        mList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()

                                         {
                                             @Override
                                             public boolean onItemLongClick (AdapterView < ? > parent, View view,int position, long id)
                                             {
                                                 Log.d("Final Task", Integer.toString(position));
                                                 ChatMsg temp = chats.get(position);
                                                 int checkdate = temp.getIsDate();
                                                 if (checkdate==1)
                                                 {
                                                     view.setBackgroundColor(Color.TRANSPARENT);
                                                 }
                                                 else
                                                 {
                                                     flag_multiselect = 1;

                                                     view.setBackgroundColor(Color.BLUE);
                                                 }

                                                 invalidateOptionsMenu();
                                                 return false;
                                             }
                                         }

        );

        mList.setOnItemClickListener(new AdapterView.OnItemClickListener()

                                     {
                                         @Override
                                         public void onItemClick (AdapterView < ? > parent, View view,int position, long id){
                                             ChatMsg temp = chats.get(position);

                                             if (flag_multiselect == 1) {
                                                 if (multiselectMsgIdList.contains(Integer.toString(temp.getSno())))
                                                 {
                                                     multiselectMsgIdList.remove(Integer.toString(temp.getSno()));
                                                     view.setBackgroundColor(Color.TRANSPARENT);
                                                     if (multiselectMsgIdList.size() == 0) {
                                                         flag_multiselect = 0;
                                                         invalidateOptionsMenu();
                                                     }
                                                 } else {
                                                     temp = chats.get(position);
                                                     int checkdate = temp.getIsDate();
                                                     if (checkdate==1) {
                                                         view.setBackgroundColor(Color.TRANSPARENT);
                                                     } else {
                                                         multiselectMsgIdList.add(Integer.toString(temp.getSno()));
                                                         view.setBackgroundColor(Color.BLUE);
                                                         Log.d("Msg Id of selected position", Integer.toString(temp.getSno()));
                                                     }
                                                 }
                                             }


                                         }
                                     }

        );
        chats=new ArrayList<ChatMsg>();

        if(chatid!=0)
        {
            set_readStatus(chatid);
        }



       // updateUI();





        broadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive (Context context, Intent intent)
            {
                reset_timer_task();
                if(intent.hasExtra("chat_subtitle"))
                {
                    chat_subtitle = intent.getStringExtra("chat_subtitle");
                    title.setText(userid+" "+chat_subtitle);
                }
                else
                {
                    updateUI();
                }


            }
        };

        deliveryStatusbroadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive (Context context, Intent intent)
            {

                Log.d("Delivery Status Broadcast Receiver got notified", "");
                updateUI();
            }
        };



        uploadbroadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive (Context context, Intent intent)
            {

                String url = null;
                int uploadChatId = 0, messagetype=0;
                boolean Sharing = false;
                if (intent.hasExtra("messagetype"))
                {
                    messagetype = intent.getIntExtra("messagetype",0);
                }
                if (intent.hasExtra("Sharing"))
                {
                    Sharing = intent.getBooleanExtra("Sharing", false);
                }
                if (intent.hasExtra("uploadurl"))
                {
                    url = intent.getStringExtra("uploadurl");
                }
                if (intent.hasExtra("uploadChatId"))
                {
                    uploadChatId = intent.getIntExtra("uploadChatId",0);
                }
                db.update_status_to_uploaded_with_server_url(Integer.toString(uploadChatId),url);
                db.update_msg_status_to_sent(Integer.toString(uploadChatId));
                updateUI();

                if(Sharing==false) // If shared the Upload service will send msg
                {
                    if(messagetype==2||messagetype==3||
                            messagetype==4)
                    {

                        Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),userid,url, messagetype);

                        if (isgroup==1) {
                           // send_group_message(url, messagetype);
                            csm.send_group_message();
                        }
                        else
                        {
                            String to = userid + "@hellochat";
                            csm.send_simple_text_message(Integer.toString(uploadChatId));
                            //send_simple_text_message(to, url, messagetype);
                        }
                    }

                    else if(intent.getIntExtra("messagetype",0)==C.STICKER)
                    {
                        Activity a = null;
                        int position =intent.getIntExtra("customsticker", 0);
                        // update the UI, post the sticker image
                        String localTime = get_current_time();
                        String msgDateTime = get_universal_date(localTime);


                        db.update_chat_lastmsg_time_count(Integer.toString(userid),"Sticker",localTime,Integer.toString(0));
                        long id = db.insertConversationDetails(Integer.toString(chatid),Integer.toString(position),localTime,Integer.toString(C.POSTER_ME),Integer.toString(C.STATUS_READ),Integer.toString(C.STICKER),Integer.toString(C.UPLOADING),"Locally Stored", Integer.toString(C.STATUS_NOT_SENT));
                        String msgid = String.valueOf(id);
                        Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),userid,Integer.toString(position), C.STICKER);

                        ChatMsg mypost = new ChatMsg(0,chatid, localTime,C.POSTER_ME, Integer.toString(position),C.ISNOTDATE, C.STICKER, C.UPLOADED,"Locally Stored", C.STATUS_SENT); // "" - blank-as no sno, 1 - me
                        chats.add(mypost);

                        // myAdapter.notifyDataSetChanged();
                        mList.setAdapter(new MyAdapter(ActivityChatWindow.this,R.id.listMessages, chats));

                        if(isgroup==1)
                        {
                            csm.send_group_message();
                        }
                        else
                        {
                            csm.send_simple_text_message(msgid);
                        }


                    }
                }
            }
        };

        // Set a listener to send a chat text message
        Text send = (Text) this.findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener()
        {
                                    public void onClick (View view)
                                    {

                                        invalidateOptionsMenu();

                                        String localTime = get_current_time();
                                        String msgDateTime = get_universal_date(localTime);

                                        String to = userid + "@hellochat";
                                        String text1 = mSendText.getText().toString();
                                        String text = getSmiledText(ActivityChatWindow.this,text1).toString();
                                        // getSmiledText(Conversation.this,chat_items.get(position).get_message().toString

                                        if(text.equalsIgnoreCase(""))
                                        {
                                            Toast.makeText(getApplicationContext(), "Please enter some message", Toast.LENGTH_LONG).show();
                                        }
                                        else
                                        {
                                            mSendText.setText("");

                                            Log.i("XMPPClient", "WTP Sending text [" + text + "] to [" + to + "]");
                                            Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),userid,text, C.MESSAGE);
                                            if (isgroup==0) {

                                                if(!db.check_chatId_exist(Integer.toString(userid)))
                                                {
                                                    long idnew = db.insertChatDetails(Integer.toString(userid),text,localTime,"0","0");
                                                    chatid = Integer.parseInt(String.valueOf(idnew));
                                                }
                                                else
                                                {
                                                    db.update_chat_lastmsg_time_count(Integer.toString(userid), text, localTime, "0");
                                                    String id = db.get_chatid_of_member_by_userid(Integer.toString(userid));
                                                    chatid = Integer.parseInt(id);
                                                }

                                                long id = db.insertConversationDetails(Integer.toString(chatid), text, localTime, Integer.toString(C.POSTER_ME), Integer.toString(C.STATUS_READ), Integer.toString(C.MESSAGE), Integer.toString(C.UPLOADING), "",Integer.toString(C.STATUS_NOT_SENT));
                                                String msgid = String.valueOf(id);
                                                csm.send_simple_text_message(msgid); // updating here onlu sent status

                                            } else {
                                                //send_group_message(text, "message");
                                                db.update_chat_lastmsg_time_count(Integer.toString(userid), text, localTime, "0"); // 1 = poster "me" , 1- readStatus "read"
                                                db.insertConversationDetails(Integer.toString(chatid), text, localTime, Integer.toString(C.POSTER_ME), Integer.toString(C.STATUS_READ), Integer.toString(C.MESSAGE), Integer.toString(C.UPLOADING), "", Integer.toString(C.STATUS_NOT_SENT));
                                                csm.send_group_message();
                                            }

                                            ChatMsg mypost = new ChatMsg(0, chatid, localTime, C.POSTER_ME, text, C.ISNOTDATE, C.MESSAGE, C.UPLOADING, "Not Required", C.STATUS_SENT); // "" - blank-as no sno, 1 - me
                                            chats.add(mypost);
                                            // myAdapter.notifyDataSetChanged();

                                            mList.setAdapter(new MyAdapter(ActivityChatWindow.this, R.id.listMessages, chats));

                                            if (focusPosition == 0) {
                                                mList.setSelection(mList.getAdapter().getCount() - 1);
                                            } else {
                                                mList.setSelection(mList.getAdapter().getCount() - focusPosition);
                                            }
                                        }

                                    }

                                }


        );


        mSendText.addTextChangedListener(new TextWatcher()
        {

            int length_before = 0, length_after = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(mSendText.getText().length()>0 && sent_status==C.STATUS_NOT_SENT)
                {
                    Log.d("Typing","sent");
                    //send typing
                    Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),userid, App.get_username()+" typing",C.TYPING_STATUS);
                    if(isgroup==C.ISGROUP)
                    {
                        length_before = mSendText.getText().length();
                        csm.send_typing_status_to_group(App.get_userid(), C.TYPING); // leter change to getname
                        sent_status = C.STATUS_SENT;
                    }
                    else
                    {
                        length_before = mSendText.getText().length();
                        csm.send_typing_status_to_single_user(App.get_username(), C.TYPING);
                        sent_status = C.STATUS_SENT;
                    }
                }
                else if(mSendText.getText().length()==0)
                {
                    //send online
                    Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),userid, App.get_username()+" typing",C.TYPING_STATUS);

                    if(isgroup == C.ISGROUP )
                    {
                        csm.send_typing_status_to_group(App.get_userid(), C.STOPPED_TYPING); // leter change to getname
                        sent_status = C.STATUS_NOT_SENT;
                    }
                    else
                    {
                        csm.send_typing_status_to_single_user(App.get_username(), C.STOPPED_TYPING);
                        sent_status = C.STATUS_NOT_SENT;
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

                length_before = mSendText.getText().length();
                 mHandler.postDelayed(new Runnable() {
                     @Override
                     public void run() {
                         length_after = mSendText.getText().length();
                         if(length_before == length_after) {
                             Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(), userid, App.get_username() + " typing", C.TYPING_STATUS);

                             if (isgroup == C.ISGROUP) {
                                 csm.send_typing_status_to_group(App.get_userid(), C.STOPPED_TYPING); // leter change to getname
                                 sent_status = C.STATUS_NOT_SENT;
                                 length_before = length_after;
                             } else {
                                 csm.send_typing_status_to_single_user(App.get_username(), C.STOPPED_TYPING);
                                 sent_status = C.STATUS_NOT_SENT;
                                 length_before = length_after;
                             }
                         }



                     }
                 }, 10000 );



            }
        });

    }

    private void ask_user_online_status()
    {
        Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),userid, "Get Online Status",C.ONLINE_STATUS_REQUEST);
        if(isgroup==C.ISNOTGROUP)
        {
            Log.d("Asking user online status","Hi");
            csm.request_online_status_to_single_user();
        }
    }

    private void reset_timer_task()
    {
        Log.d("Resetting Timer Task","Hi");
        task.cancel();
        set_timer_task();
    }

    private void set_timer_task()
    {
       timer = new Timer();

        task = new TimerTask() {

            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        ask_user_online_status();

                    }
                });
            }
        };

        timer.scheduleAtFixedRate(task, 30000, 30000);

    }

    public void get_User_Mode(String userid)
    {
        //WTP_Service wtp_service = new WTP_Service();
        //wtp_service.create_connection(getApplicationContext());
        Roster roster = WTP_Service.connection.getRoster();
        Presence availability = roster.getPresence(userid);
        Presence.Mode userMode = availability.getMode();

        retrieveState_mode(availability.getMode(),availability.isAvailable());
    }
    public static int retrieveState_mode(Presence.Mode userMode, boolean isOnline) {
        int userState = 0;
        /** 0 for offline, 1 for online, 2 for away,3 for busy*/
        if(userMode == Presence.Mode.dnd) {
            userState = 3;
            Log.d("The user mode is","DND");
        } else if (userMode == Presence.Mode.away || userMode == Presence.Mode.xa) {
            userState = 2;
            Log.d("The user mode is","Away");
        } else if (isOnline) {
            userState = 1;
            Log.d("The user mode is","Online");
        }
        return userState;
    }






    public void AlertDialog()
    {
        final CharSequence[] options;

        options= new CharSequence[]{"Record audio", "Select from storage"};


        AlertDialog.Builder builder=new AlertDialog.Builder(ActivityChatWindow.this)
                .setTitle("")
                .setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int index) {
                        // TODO Auto-generated method stub
                        Toast.makeText(getApplicationContext(),
                                "The selected option is " + options[index], Toast.LENGTH_LONG).show();
                        if (options[index].equals("Record audio")) {
                            Intent intent = new Intent(ActivityChatWindow.this, Record_Audio.class);
                            startActivityForResult(intent, C.RESULT_RECORD_AUDIO);
                        } else if (options[index].equals("Select from storage")) {
                            Intent intent = new Intent(
                                    Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(intent, C.RESULT_LOAD_AUDIO);
                        }


                    }
                });
        AlertDialog alertdialog=builder.create();
        alertdialog.show();


    }







    private void init_net_details()
    {
        net = new Net_Detect(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        Log.d("Options menu Refreshing","asd");
        if(chats.size()!=0)
        {
            Log.d("Chats Size",Integer.toString(chats.size()));
            if (flag_multiselect != 1)
            {
                Log.d("Displaying Full Clear Conversation",Integer.toString(flag_multiselect));
                SubMenu submenu = menu.addSubMenu("");
                submenu.add(1, 4, 1, "Add Image");
                submenu.add(1, 5, 1, "Add Audio");
                submenu.add(1, 6, 1, "Add Video");
                submenu.setIcon(R.drawable.abs__ic_menu_moreoverflow_normal_holo_dark);
                submenu.add(1, Menu.FIRST, 1, "Clear Conversation");
                if(isgroup==1)
                {
                    submenu.add(1, 2, 1, "Edit Group");
                    submenu.add(1, 3, 1, "Leave Group");

                }
                submenu.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS |
                        MenuItem.SHOW_AS_ACTION_WITH_TEXT);
            }
            else
            {
                Log.d("Displaying Specific Delete",Integer.toString(flag_multiselect));
                getSupportMenuInflater().inflate(R.menu.chat_box_menu, menu);
            }
        }
        else
        {
            if(isgroup==1)
            {
                SubMenu submenu = menu.addSubMenu("");
                submenu.setIcon(R.drawable.abs__ic_menu_moreoverflow_normal_holo_dark);
                submenu.add(1, 2, 1, "Edit Group");
                submenu.add(1, 3, 1, "Leave Group");
                submenu.add(1, 4, 1, "Add Image");
                submenu.add(1, 5, 1, "Add Audio");
                submenu.add(1, 6, 1, "Add Video");
                submenu.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS |
                        MenuItem.SHOW_AS_ACTION_WITH_TEXT);
            }
            else
            {
                SubMenu submenu = menu.addSubMenu("");
                submenu.setIcon(R.drawable.abs__ic_menu_moreoverflow_normal_holo_dark);
                submenu.add(1, 4, 1, "Add Image");
                submenu.add(1, 5, 1, "Add Audio");
                submenu.add(1, 6, 1, "Add Video");
                submenu.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS |
                        MenuItem.SHOW_AS_ACTION_WITH_TEXT);
            }
        }



        return super.onCreateOptionsMenu(menu);
    }


    public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
        // Take appropriate action for each action item click
        Intent intent;
        FragmentManager fm;
        CustomDialogWithTwoButtons dialogtwo;
        Bundle args;
        switch (item.getItemId()) {

            case android.R.id.home:
                if(flag_multiselect==1)
                {

                    multiselectMsgIdList.clear();
                    invalidateOptionsMenu();
                    mList.setAdapter(new MyAdapter(ActivityChatWindow.this,R.id.listMessages, chats));
                    flag_multiselect=0;
                }
                else
                {
                    intent = new Intent(ActivityChatWindow.this, Activity_ChatUsers.class);
                    startActivity(intent);
                }

                return true;

            case Menu.FIRST:

                fm = getSupportFragmentManager();
                dialogtwo = new CustomDialogWithTwoButtons();
                args = new Bundle();
                args.putString("displayTextSecond", "Are you sure you want to delete complete conversation?");
                dialogtwo.setArguments(args);

                dialogtwo.show(fm, "Dialog Fragment");


                dialogtwo.setButtonListener(new CustomDilogInterface() {
                    @Override
                    public void onButtonClickedNew(boolean value) {
                        if (value == true) {
                            if(isgroup==1)
                            {
                                db.delete_group_complete_conversation(Integer.toString(chatid));
                            }
                            else
                            {
                                db.delete_user_complete_conversation(Integer.toString(userid));
                            }

                            chats.clear();
                            invalidateOptionsMenu();
                            mList.setAdapter(new MyAdapter(ActivityChatWindow.this, R.id.listMessages, chats));
                        }
                        if (value == false) {
                            Toast.makeText(ActivityChatWindow.this, "False", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onButtonClickedString(String result) {
                    }
                });

                return true;

            case 2:
                intent = new Intent(ActivityChatWindow.this, Activity_EditGroup.class);
                intent.putExtra("servergroupid", userid);
                startActivity(intent);

                return true;

            case 3:
                fm = getSupportFragmentManager();
                dialogtwo = new CustomDialogWithTwoButtons();
                args = new Bundle();
                args.putString("displayTextSecond", "Are you sure you want to exit from the group?");
                dialogtwo.setArguments(args);

                dialogtwo.show(fm, "Dialog Fragment");


                dialogtwo.setButtonListener(new CustomDilogInterface() {
                    @Override
                    public void onButtonClickedNew(boolean value) {
                        if (value == true)
                        {

                            String localTime = get_current_time();
                            String msgDateTime = get_universal_date(localTime);

                            //get current user wtp id
                            String my_wtp_id = App.get_userid();
                            Log.d("My WTP Id", my_wtp_id);

                            //Get the group members user ids
                            ArrayList<GroupDetails> groupdata;
                            groupdata = db.get_group_details(Integer.toString(userid));
                            GroupDetails gd = groupdata.get(0);
                            String members = gd.getGroup_users();
                            String[] memberlist = members.split(", "); // don't remove space else it only 1 name checked
                            List<String> memberarraylist = new LinkedList(Arrays.asList(memberlist));
                            Log.d("My members arraylist", memberarraylist.toString());

                            //remove the current user's user id from list
                            memberarraylist.remove(my_wtp_id);

                            String admin = null;
                            if(my_wtp_id.equalsIgnoreCase(App.get_userid()) && memberarraylist.size()>0)
                            {
                                admin = memberarraylist.get(0);
                            }
                            else
                            {
                                admin = gd.getGroup_admin();
                            }



                            String users = memberarraylist.toString();
                            users = users.substring(1, users.length()-1);
                            Log.d("My members arraylist without brackets",users);

                            db.update_group_users(users, Integer.toString(userid),admin); // first so that I should not get this update

                            Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),userid,"Hi, This is a group update.",C.GROUP_MEMBERS_UPDATE);
                            csm.send_group_message();


                            db.delete_user_complete_conversation(Integer.toString(userid)); // deleted from chat table & conversation table
                            db.delete_group_details(Integer.toString(userid));
                            finish();
                        }
                        if (value == false) {
                            Toast.makeText(ActivityChatWindow.this, "False", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onButtonClickedString(String result) {
                    }
                });
                return true;

            case 4:
                intent = new Intent(
                        Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, C.RESULT_LOAD_IMAGE);
                return true;

            case 5:

                AlertDialog();
                return true;

            case 6:

                intent = new Intent(
                        Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, C.RESULT_LOAD_VIDEO);
                return true;



            case R.id.delete_selected:

                FragmentManager fm1 = getSupportFragmentManager();
                dialogtwo = new CustomDialogWithTwoButtons();
                args = new Bundle();
                Log.d("ActivityChatWindow", "List Item Selected Ids"+multiselectMsgIdList.toString());
                args.putString("displayTextSecond", "Are you sure you want to delete selected conversation?");
                dialogtwo.setArguments(args);
                dialogtwo.show(fm1, "Dialog Fragment");


                dialogtwo.setButtonListener(new CustomDilogInterface() {
                    @Override
                    public void onButtonClickedNew(boolean value) {
                        if (value == true) {

                            flag_multiselect=0;
                            for (int i = (multiselectMsgIdList.size() - 1); i >= 0; i--)
                            {

                                Log.d("ActivityChatWindow","Deleting Msg Id : "+multiselectMsgIdList.get(i).toString());
                                db.delete_conversation_row(multiselectMsgIdList.get(i));

                                invalidateOptionsMenu();
                                mList.setAdapter(new MyAdapter(ActivityChatWindow.this, R.id.listMessages, chats));
                                updateUI();

                            }
                            multiselectMsgIdList.clear();

                        }
                        if (value == false) {
                            Toast.makeText(ActivityChatWindow.this, "False", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onButtonClickedString(String result) {
                    }
                });

                return true;

            case R.id.share_selected:
                String selectedids = multiselectMsgIdList.toString();
                selectedids = selectedids.substring(1, selectedids.length()-1);

                intent = new Intent(this, Activity_Share_Chat_Content.class);
                intent.putExtra("selectedItems",selectedids);
                startActivity(intent);
                return true;


        }
        return super.onOptionsItemSelected(item);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == C.RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            String localTime = get_current_time();
            String msgDateTime = get_universal_date(localTime);

            int unreadcount = db.get_unread_msg_count_with_id(Integer.toString(userid));
            db.update_chat_lastmsg_time_count(Integer.toString(userid),"Image",localTime,Integer.toString(unreadcount+1));
            long id = db.insertConversationDetails(Integer.toString(chatid),picturePath,localTime,Integer.toString(C.POSTER_ME),Integer.toString(C.STATUS_READ),Integer.toString(C.IMAGE),Integer.toString(C.UPLOADING),"Locally Stored", Integer.toString(C.STATUS_NOT_SENT));
            int uploadChatId = (int) id;

            ChatMsg mypost = new ChatMsg(0,chatid, localTime,C.POSTER_ME, picturePath,C.ISNOTDATE, C.IMAGE, C.UPLOADING,"Locally Stored", C.STATUS_SENT); // "" - blank-as no sno, 1 - me
            chats.add(mypost);
            Log.d("Chats Size 1", Integer.toString(chats.size()));
            //myAdapter.notifyDataSetChanged();


            mList.setAdapter(new MyAdapter(ActivityChatWindow.this,R.id.listMessages, chats));

            if(net.isConnectingToInternet())
            {
                Intent intent = new Intent(this, UploadService.class);
                intent.putExtra(C.PARAMS.IMAGE.get_param(), picturePath);
                intent.putExtra("ChatImages", 1);
                intent.putExtra("messagetype", C.IMAGE);
                intent.putExtra("sharing", false);
                intent.putExtra("uploadChatId",uploadChatId);
                this.startService(intent);
            }

        }

        else if (requestCode == C.RESULT_LOAD_AUDIO && resultCode == RESULT_OK && null != data) {
            Uri selectedAudio = data.getData();
            String[] filePathColumn = { MediaStore.Audio.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedAudio,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String audioPath = cursor.getString(columnIndex);
            cursor.close();

            String localTime = get_current_time();
            String msgDateTime = get_universal_date(localTime);

            int unreadcount = db.get_unread_msg_count_with_id(Integer.toString(userid));
            db.update_chat_lastmsg_time_count(Integer.toString(userid),"Audio",localTime,Integer.toString(unreadcount+1));

            long id = db.insertConversationDetails(Integer.toString(chatid),audioPath,localTime,Integer.toString(C.POSTER_ME),Integer.toString(C.STATUS_READ),Integer.toString(C.AUDIO),Integer.toString(C.UPLOADING),"", Integer.toString(C.STATUS_NOT_SENT));
            int uploadChatId = (int) id;
            ChatMsg mypost = new ChatMsg(0,chatid, localTime,C.POSTER_ME, audioPath,C.ISNOTDATE, C.AUDIO, C.UPLOADING, "Locally Stored", C.STATUS_SENT); // "" - blank-as no sno, 1 - me
            chats.add(mypost);
            Log.d("Chats Size 1", Integer.toString(chats.size()));
            //myAdapter.notifyDataSetChanged();


            mList.setAdapter(new MyAdapter(ActivityChatWindow.this,R.id.listMessages, chats));

            if(net.isConnectingToInternet())
            {
                Intent intent = new Intent(this, UploadService.class);
                intent.putExtra(C.PARAMS.AUDIO.get_param(), audioPath);
                intent.putExtra("ChatAudio", 1);
                intent.putExtra("messagetype", C.AUDIO);
                intent.putExtra("sharing", false);
                intent.putExtra("uploadChatId", uploadChatId);
                this.startService(intent);
            }

        }
        else if (requestCode == C.RESULT_RECORD_AUDIO && resultCode == RESULT_OK && null != data) {

            String audioPath = data.getStringExtra("audiofilepath");
            String localTime = get_current_time();
            String msgDateTime = get_universal_date(localTime);

            int unreadcount = db.get_unread_msg_count_with_id(Integer.toString(userid));
            db.update_chat_lastmsg_time_count(Integer.toString(userid),"Audio",localTime,Integer.toString(unreadcount+1));

            long id = db.insertConversationDetails(Integer.toString(chatid),audioPath,localTime,Integer.toString(C.POSTER_ME),Integer.toString(C.STATUS_READ),Integer.toString(C.AUDIO),Integer.toString(C.UPLOADING),"", Integer.toString(C.STATUS_NOT_SENT));
            int uploadChatId = (int) id;
            ChatMsg mypost = new ChatMsg(0,chatid, localTime,C.POSTER_ME, audioPath,C.ISNOTDATE, C.AUDIO, C.UPLOADING, "Locally Stored", C.STATUS_SENT); // "" - blank-as no sno, 1 - me
            chats.add(mypost);
            Log.d("Chats Size 1", Integer.toString(chats.size()));
            //myAdapter.notifyDataSetChanged();


            mList.setAdapter(new MyAdapter(ActivityChatWindow.this,R.id.listMessages, chats));

            if(net.isConnectingToInternet())
            {
                Intent intent = new Intent(this, UploadService.class);
                intent.putExtra(C.PARAMS.AUDIO.get_param(), audioPath);
                intent.putExtra("ChatAudio", 1);
                intent.putExtra("messagetype", C.AUDIO);
                intent.putExtra("sharing",false);
                intent.putExtra("uploadChatId", uploadChatId);
                this.startService(intent);
            }

        }
        else if (requestCode == C.RESULT_LOAD_VIDEO && resultCode == RESULT_OK && null != data) {
            Uri selectedVideo = data.getData();
            String[] filePathColumn = { MediaStore.Video.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedVideo,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String videopath = cursor.getString(columnIndex);
            cursor.close();

            String localTime = get_current_time();
            String msgDateTime = get_universal_date(localTime);

            int unreadcount = db.get_unread_msg_count_with_id(Integer.toString(userid));
            db.update_chat_lastmsg_time_count(Integer.toString(userid),"Video",localTime,Integer.toString(unreadcount+1));
            long id = db.insertConversationDetails(Integer.toString(chatid),videopath,localTime,Integer.toString(C.POSTER_ME),Integer.toString(C.STATUS_READ),Integer.toString(C.VIDEO),Integer.toString(C.UPLOADING),"", Integer.toString(C.STATUS_NOT_SENT));
            int uploadchatid = (int) id;
            ChatMsg mypost = new ChatMsg(0,chatid, localTime,C.POSTER_ME, videopath,C.ISNOTDATE, C.VIDEO, C.UPLOADING,"Locally Stored", C.STATUS_SENT); // "" - blank-as no sno, 1 - me
            chats.add(mypost);
            Log.d("Chats Size 1", Integer.toString(chats.size()));
            //myAdapter.notifyDataSetChanged();


            mList.setAdapter(new MyAdapter(ActivityChatWindow.this,R.id.listMessages, chats));

            if(net.isConnectingToInternet())
            {
                Intent intent = new Intent(this, UploadService.class);
                intent.putExtra(C.PARAMS.VIDEO.get_param(), videopath);
                intent.putExtra("ChatVideo", 1);
                intent.putExtra("messagetype", C.VIDEO);
                intent.putExtra("sharing", false);
                intent.putExtra("uploadChatId", uploadchatid);
                this.startService(intent);
            }

        }
    }


    private void set_readStatus(int chatid)
    {
        if(db.check_conversation_present(Integer.toString(chatid)))
        {
            db.set_read_status_in_conversation_table(Integer.toString(chatid));
            db.update_count(Integer.toString(userid));
        }
    }

    private void updateUI()
    {
        if(isgroup==0)
        {
            String name = db.get_membername_by_id(Integer.toString(userid));
            if(name == null)// delete later
            {
                name = Integer.toString(userid);
            }


            if(chat_subtitle!=null)
            {
                title.setText(name+" "+chat_subtitle);
            }
            else
            {
                title.setText(name);
            }


        }
        else
        {
            String name = db.get_groupname_by_id(Integer.toString(userid));
            title.setText(name+" "+chat_subtitle);
        }
        if(db.check_chatId_exist(Integer.toString(userid)))// if name not present then insert in db
        {
            chats = db.get_conversation_by_userid(Integer.toString(chatid));
            mList.setAdapter(new MyAdapter(this, R.id.listMessages, chats));
            if(db.check_conversation_present(Integer.toString(chatid)))
            {
                set_readStatus(chatid);
            }

            if (focusPosition == 0) {
                mList.setSelection(mList.getAdapter().getCount() - 1);
            } else {
                mList.setSelection(mList.getAdapter().getCount() - focusPosition);
            }
        }

    }

    @Override
    public void onResume()
    {
        super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
        set_timer_task();

        updateUI();
        registerReceiver(broadcastReceiver, new IntentFilter(WTP_Service.BROADCAST_ACTION));
        registerReceiver(uploadbroadcastReceiver, new IntentFilter(UploadService.UPLOAD_COMPLETE_BROADCAST));
        registerReceiver(deliveryStatusbroadcastReceiver, new IntentFilter(Custom_Send_Message.DELIVERY_STATUS_BROADCAST));
        ask_user_online_status();
    }

    public String get_current_time()
    {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
        return sdf.format(c.getTime());
    }

    public String get_universal_date(String time)
    {
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm a");


        Date date = null;
        try
        {
            //Here you say to java the initial timezone. This is the secret
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = sdf.parse(time);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        //Here you set to your timezone

        sdf.setTimeZone(TimeZone.getDefault());
        time = sdf.format(date);
        return time;
    }




    @Override
    public void onPause()
    {
        super.onPause();
        task.cancel();
        C.ACTIVITY_CHAT_WINDOW_RUNNING_STATUS = C.ACTIVITY_STOPPED;
        WTP_Service.CURRENT_CHAT_USER = "@hellochat";
        unregisterReceiver(broadcastReceiver);
        unregisterReceiver(uploadbroadcastReceiver);
        unregisterReceiver(deliveryStatusbroadcastReceiver);
        multiselectMsgIdList.clear();
        invalidateOptionsMenu();
        flag_multiselect=0;
        //unregisterReceiver(shareuploadbroadcastReceiver);
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;
    }



    @Override
    public void onBackPressed() {

        if(smiley_toggle==1)
        {
            contents.setVisibility(View.GONE);
            smiley_toggle=0;
        }
        else if(flag_multiselect==1)
        {
            multiselectMsgIdList.clear();
            invalidateOptionsMenu();
            mList.setAdapter(new MyAdapter(ActivityChatWindow.this,R.id.listMessages, chats));
            flag_multiselect=0;
        }
        else
        {
            Intent intent = new Intent(ActivityChatWindow.this, Activity_ChatUsers.class);
            startActivity(intent);
        }
    }



    private Bitmap get_bitmap_from_filepath(String filepath)
    {
        File imgFile = new  File(filepath);
        Bitmap myBitmap = null;
        if(imgFile.exists()){
            myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        }
        return myBitmap;
    }

    private MediaPlayer play_audio_using_filepath(String filepath)
    {
        final MediaPlayer mPlayer = new MediaPlayer();
        Uri myUri1 = Uri.parse(filepath);
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try
        {
            mPlayer.setDataSource(getApplicationContext(), myUri1);
        } catch (IllegalArgumentException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        } catch (SecurityException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        } catch (IllegalStateException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try
        {
            mPlayer.prepare();
        }
        catch (IllegalStateException e)
        {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        }
        catch (IOException e)
        {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        }
        return mPlayer;
    }

    class MyAdapter extends ArrayAdapter<ChatMsg> {
        View v;
        LayoutInflater inflater;
        ViewHolder holder;
        List<ChatMsg> chats;
        private Context context;
        int drawable_id=0;


        Bitmap sticker = null;


        public MyAdapter(Context context, int resourceId,
                         List<ChatMsg> chats) {
            super(context, resourceId, chats);
            this.context = context;
            this.chats = chats;
            inflater = LayoutInflater.from(context);
        }

        private class ViewHolder {
            TextView txtTimeLeft, txtTimeRight, txtDate, txtMsgLeft, txtMsgRight;
            ImageView imageLeft, imageRight, statusRight;
            ProgressBar progressLeft, progressRight;
            LinearLayout FullContainer;
            RelativeLayout triangleLeft, triangleRight, leftContent, rightContent, rightLayout, leftLayout;
            LinearLayout dateTouch;

        }


        @Override
        public View getView(final int position,final View convertView, ViewGroup arg2)
        {
            final ChatMsg chatobj = chats.get(position);

            ActivityChatWindow activity = null;


            activity = ActivityChatWindow.this;


            v = convertView;

            if (v == null)
            {
                mycount = mycount+1;
                Log.d("My Count",Integer.toString(mycount)+" "+chats.size());

                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.chat_box,null);

                holder = new ViewHolder();

                holder.txtDate = (TextView) v.findViewById(R.id.txtDate);

                holder.txtMsgLeft = (TextView) v.findViewById(R.id.txtMsgLeft);
                holder.txtMsgRight = (TextView) v.findViewById(R.id.txtMsgRight);

                holder.txtTimeLeft = (TextView) v.findViewById(R.id.txtTimeLeft);
                holder.txtTimeRight = (TextView) v.findViewById(R.id.txtTimeRight);


                holder.imageLeft = (ImageView) v.findViewById(R.id.imageLeft);
                holder.imageRight = (ImageView) v.findViewById(R.id.imageRight);

                holder.leftLayout = (RelativeLayout) v.findViewById(R.id.leftlayout);
                holder.rightLayout = (RelativeLayout) v.findViewById(R.id.rightlayout);

                holder.progressLeft = (ProgressBar) v.findViewById(R.id.progressLeft);
                holder.progressRight = (ProgressBar) v.findViewById(R.id.progressRight);


                holder.triangleLeft = (RelativeLayout) v.findViewById(R.id.leftTriangle);
                holder.triangleRight = (RelativeLayout) v.findViewById(R.id.rightTriangle);

                holder.statusRight = (ImageView) v.findViewById(R.id.statusRight);

                holder.FullContainer = (LinearLayout) v.findViewById(R.id.FullConatainer);

                holder.dateTouch = (LinearLayout) v.findViewById(R.id.DateTouch);

                v.setTag(holder);
            }
            else
            {
                holder = (ViewHolder) v.getTag();
            }

            if(flag_multiselect==1)
            {
                ChatMsg temp = chats.get(position);
                if(multiselectMsgIdList.contains(temp.getSno()))
                {
                    v.setBackgroundColor(Color.BLUE);
                }
                else
                {
                    v.setBackgroundColor(Color.TRANSPARENT);
                }
            }
            mynextcount=mynextcount+1;
            Log.d("My Next Count" ,Integer.toString(mynextcount));

            if(chatobj.getPoster_type()==C.POSTER_FRIEND)
            {
                invalidateOptionsMenu();
                holder.rightLayout.setVisibility(View.GONE);
                holder.leftLayout.setVisibility(View.VISIBLE);

                if (chatobj.getIsDate()==C.ISDATE)
                {
                    holder.FullContainer.setVisibility(View.GONE);
                    holder.dateTouch.setVisibility(View.VISIBLE);
                    holder.txtDate.setText(chatobj.getDatetime());
                }
                else
                {
                    String mdate = chatobj.getDatetime();
                    String[] parts = mdate.split(" ");
                    String fetcheddate = parts[1];
                    String fetchampm = parts[2];

                    holder.dateTouch.setVisibility(View.GONE);

                    if (chatobj.getMedia_type()==C.MESSAGE) {
                        holder.imageLeft.setVisibility(View.GONE);
                        holder.progressLeft.setVisibility(View.GONE);
                        holder.txtMsgLeft.setVisibility(View.VISIBLE);
                        holder.txtMsgLeft.setText(getSmiledText(ActivityChatWindow.this, Html.fromHtml(chatobj.getMsg())));
                    }
                    else if (chatobj.getMedia_type()==C.IMAGE) {
                        Bitmap bitmap;
                        holder.txtMsgLeft.setVisibility(View.GONE);
                        holder.imageLeft.setImageBitmap(null);
                        holder.imageLeft.setVisibility(View.VISIBLE);
                        holder.progressLeft.setVisibility(View.GONE);


                        if (chatobj.getMedia_status()==C.DOWNLOADED) {
                            bitmap = get_bitmap_from_filepath(chatobj.getMsg());
                            holder.imageLeft.setImageBitmap(bitmap);
                            holder.imageLeft.setScaleType(ImageView.ScaleType.CENTER_CROP);

                        } else if (chatobj.getMedia_status()==C.DOWNLOADING) {
                            holder.progressLeft.setVisibility(View.VISIBLE);
                        } else {
                            Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.download);
                            holder.imageLeft.setImageBitmap(largeIcon);
                        }

                        holder.imageLeft.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v)
                            {

                                if (chatobj.getMedia_status()==C.DOWNLOADED) {
                                    final Dialog dialog = new Dialog(ActivityChatWindow.this);
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.setContentView(R.layout.large_image);
                                    ImageView image = (ImageView) dialog
                                            .findViewById(R.id.dispImage);
                                    Bitmap finalBitmap = get_bitmap_from_filepath(chatobj.getMsg());
                                    image.setImageBitmap(null);
                                    image.setImageBitmap(finalBitmap);
                                    image.setScaleType(ImageView.ScaleType.FIT_XY);
                                    dialog.show();
                                } else if (chatobj.getMedia_status()==C.NOT_DOWNLOADED) {

                                    db.update_status_to_downloading(Integer.toString(chatobj.getSno()));
                                    holder.progressLeft.setVisibility(View.VISIBLE);
                                    Bitmap bitmap;
                                    Download mydownload = new Download(chatobj.getMsg(), ".jpg");

                                    String filepath = null;

                                    new DownloadFileUsingUrl(holder.progressLeft, chatobj.getSno()).execute(mydownload);

                                    updateUI();
                                }

                            }
                        });
                    } else if (chatobj.getMedia_type()==C.AUDIO) {
                        holder.txtMsgLeft.setVisibility(View.GONE);
                        holder.imageLeft.setImageBitmap(null);
                        holder.imageLeft.setVisibility(View.VISIBLE);
                        holder.progressLeft.setVisibility(View.GONE);
                        holder.txtTimeLeft.setText(fetcheddate + " " + fetchampm);


                        if(chatobj.getMedia_status()==C.DOWNLOADED)
                        {
                            if(C.MSG_ID_OF_PLAYED_AUDIO == -1)
                            {
                                Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.play_black);
                                holder.imageLeft.setImageBitmap(largeIcon);
                            }
                            else if(chatobj.getSno()==C.MSG_ID_OF_PLAYED_AUDIO)
                            {
                                Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.pause_black);
                                holder.imageLeft.setImageBitmap(largeIcon);
                            }
                            else
                            {
                                Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.play_black);
                                holder.imageLeft.setImageBitmap(largeIcon);
                            }
                        }
                        else if(chatobj.getMedia_status()==C.DOWNLOADING)
                        {
                            holder.progressLeft.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.download);
                            holder.imageLeft.setImageBitmap(largeIcon);
                        }

                        holder.imageLeft.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {
                                if(chatobj.getMedia_status()==C.DOWNLOADED)
                                {
                                    if( C.MSG_ID_OF_PLAYED_AUDIO == -1)
                                    {
                                        player = play_audio_using_filepath(chatobj.getMsg());
                                        C.MSG_ID_OF_PLAYED_AUDIO = chatobj.getSno();
                                        player.start();
                                        notifyDataSetChanged();
                                        //mList.setSelection(position);
                                        mList.setSelected(true);
                                    }
                                    else if(chatobj.getSno()==C.MSG_ID_OF_PLAYED_AUDIO)
                                    {

                                        C.MSG_ID_OF_PLAYED_AUDIO = -1;
                                        if (player != null) {
                                            if (player.isPlaying()) {
                                                player.stop();
                                            }
                                            player.release();
                                        }
                                        notifyDataSetChanged();
                                        //mList.setSelection(position);
                                        mList.setSelected(true);

                                    }
                                    player.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
                                    {
                                        @Override
                                        public void onCompletion(MediaPlayer mp)
                                        {
                                            Log.d("Media Playing Completed", "CallingOnCompletion");
                                            C.MSG_ID_OF_PLAYED_AUDIO = -1;
                                            notifyDataSetChanged();
                                            //mList.setSelection(position);
                                            mList.setSelected(true);
                                        }
                                    });
                                }
                                else if(chatobj.getMedia_status()==C.NOT_DOWNLOADED)
                                {

                                    db.update_status_to_downloading(Integer.toString(chatobj.getSno()));
                                    holder.progressLeft.setVisibility(View.VISIBLE);
                                    Download mydownload = new Download(chatobj.getMsg(),".mp3");
                                    new DownloadFileUsingUrl(holder.progressLeft, chatobj.getSno()).execute(mydownload);

                                    updateUI();
                                }

                            }
                        });

                    } else if (chatobj.getMedia_type()==C.VIDEO) {
                        Bitmap bmThumbnail = null;
                        File file = null;

                        holder.txtMsgLeft.setVisibility(View.GONE);
                        holder.imageLeft.setImageBitmap(null);
                        holder.imageLeft.setVisibility(View.VISIBLE);
                        holder.progressLeft.setVisibility(View.GONE);
                        holder.txtTimeLeft.setText(fetcheddate + " " + fetchampm);


                        if (chatobj.getMedia_status()==C.DOWNLOADED) {
                            final String videoFilePath = chatobj.getMsg();
                            file = new File(videoFilePath);
                            bmThumbnail = ThumbnailUtils.createVideoThumbnail(videoFilePath, MediaStore.Video.Thumbnails.MICRO_KIND);
                            holder.imageLeft.setImageBitmap(bmThumbnail);
                        } else if (chatobj.getMedia_status()==C.DOWNLOADING) {
                            holder.progressLeft.setVisibility(View.VISIBLE);
                        } else {
                            Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.download);
                            holder.imageLeft.setImageBitmap(largeIcon);
                        }


                        final File finalFile = file;
                        holder.imageLeft.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (chatobj.getMedia_status()==C.DOWNLOADED) {
                                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
                                    intent.setDataAndType(Uri.fromFile(finalFile), "video/*");
                                    startActivity(intent);
                                } else if (chatobj.getMedia_status()==C.NOT_DOWNLOADED) {
                                    db.update_status_to_downloading(Integer.toString(chatobj.getSno()));
                                    Download mydownload = new Download(chatobj.getMsg(), ".mp4");
                                    String videofilepath = null;

                                    new DownloadFileUsingUrl(holder.progressLeft, chatobj.getSno()).execute(mydownload);

                                    Bitmap bmThumbnail = ThumbnailUtils.createVideoThumbnail(videofilepath, MediaStore.Video.Thumbnails.MICRO_KIND);
                                    holder.imageLeft.setImageBitmap(bmThumbnail);

                                    updateUI();
                                }
                            }
                        });

                    } else if (chatobj.getMedia_type()==C.STICKER) {

                        holder.txtMsgLeft.setVisibility(View.GONE);
                        holder.imageLeft.setImageBitmap(null);
                        holder.imageLeft.setVisibility(View.VISIBLE);
                        holder.progressLeft.setVisibility(View.GONE);

                        holder.txtTimeLeft.setText(fetcheddate + " " + fetchampm);
                        drawable_id = stickers[Integer.parseInt(chatobj.getMsg())];
                        sticker = BitmapFactory.decodeResource(getResources(), drawable_id);
                        holder.imageLeft.setImageBitmap(sticker);


                        holder.imageLeft.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                final Dialog dialog = new Dialog(ActivityChatWindow.this);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setContentView(R.layout.large_image);
                                ImageView image = (ImageView) dialog
                                        .findViewById(R.id.dispImage);
                                image.setImageBitmap(null);
                                sticker = BitmapFactory.decodeResource(getResources(), drawable_id);
                                image.setImageBitmap(sticker);
                                image.setScaleType(ImageView.ScaleType.FIT_XY);
                                dialog.show();

                            }
                        });
                    }
                    holder.txtTimeLeft.setText(fetcheddate+" "+fetchampm);
                }
            }
            else if(chatobj.getPoster_type()==C.POSTER_ME) // poster is me
            {
                holder.leftLayout.setVisibility(View.GONE);
                holder.rightLayout.setVisibility(View.VISIBLE);

                if (chatobj.getIsDate()==C.ISDATE)
                {
                    holder.FullContainer.setVisibility(View.GONE);
                    holder.dateTouch.setVisibility(View.VISIBLE);
                    holder.txtDate.setText(chatobj.getDatetime());
                }
                else
                {
                    String mdate = chatobj.getDatetime();
                    String[] parts = mdate.split(" ");
                    String fetcheddate = parts[1];
                    String fetchampm = parts[2];
                    Bitmap deliveryStatus = null;
                    int sent_status = chatobj.getDelivery_status();
                    if(isgroup==C.ISGROUP)
                    {
                        deliveryStatus = BitmapFactory.decodeResource(getResources(), R.drawable.sent);
                        /*if(sent_status==C.STATUS_SENT)
                        {
                            deliveryStatus = BitmapFactory.decodeResource(getResources(), R.drawable.sent);
                        }
                        else if(sent_status==C.STATUS_DELIVERED)
                        {
                            deliveryStatus = BitmapFactory.decodeResource(getResources(), R.drawable.sent);
                        }
                        else if(sent_status==C.STATUS_NOT_SENT)
                        {
                            deliveryStatus = BitmapFactory.decodeResource(getResources(), R.drawable.sent);
                        }*/
                    }
                    else
                    {
                        if(sent_status==C.STATUS_SENT)
                        {
                            deliveryStatus = BitmapFactory.decodeResource(getResources(), R.drawable.sent);
                        }
                        else if(sent_status==C.STATUS_DELIVERED)
                        {
                            deliveryStatus = BitmapFactory.decodeResource(getResources(), R.drawable.delivered);
                        }
                        else if(sent_status==C.STATUS_NOT_SENT)
                        {
                            deliveryStatus = BitmapFactory.decodeResource(getResources(), R.drawable.not_sent);
                        }
                    }


                    holder.dateTouch.setVisibility(View.GONE);

                    if (chatobj.getMedia_type()==C.MESSAGE)
                    {
                        holder.imageRight.setVisibility(View.GONE);
                        holder.progressRight.setVisibility(View.GONE);
                        holder.txtMsgRight.setVisibility(View.VISIBLE);

                        holder.statusRight.setVisibility(View.VISIBLE);
                        holder.statusRight.setImageBitmap(null);
                        holder.statusRight.setImageBitmap(deliveryStatus);

                        holder.txtMsgRight.setText(getSmiledText(ActivityChatWindow.this, Html.fromHtml(chatobj.getMsg())));
                    }
                    else if (chatobj.getMedia_type()==C.IMAGE)
                    {
                        holder.txtMsgRight.setVisibility(View.GONE);
                        holder.imageRight.setImageBitmap(null);
                        holder.imageRight.setVisibility(View.VISIBLE);

                        Bitmap bitmap = null;
                        if(chatobj.getMedia_status()==C.UPLOADING)
                        {
                            holder.progressRight.setVisibility(View.VISIBLE);
                            holder.statusRight.setVisibility(View.GONE);
                        }
                        else if(chatobj.getMedia_status()==C.UPLOADED || chatobj.getMedia_status()==C.STATUS_DELIVERED || chatobj.getMedia_status()==C.STATUS_SENT)
                        {
                            holder.progressRight.setVisibility(View.GONE);
                            bitmap = get_bitmap_from_filepath(chatobj.getMsg());
                            holder.imageRight.setImageBitmap(bitmap);
                            holder.imageRight.setScaleType(ImageView.ScaleType.CENTER_CROP);

                            holder.statusRight.setVisibility(View.VISIBLE);
                            holder.statusRight.setImageBitmap(null);
                            holder.statusRight.setImageBitmap(deliveryStatus);
                        }

                        final Bitmap finalBitmap = bitmap;

                        holder.imageRight.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {

                                final Dialog dialog = new Dialog(ActivityChatWindow.this);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setContentView(R.layout.large_image);
                                ImageView image = (ImageView) dialog
                                        .findViewById(R.id.dispImage);
                                image.setImageBitmap(null);
                                image.setImageBitmap(finalBitmap);
                                image.setScaleType(ImageView.ScaleType.FIT_XY);

                                dialog.show();
                            }
                        });
                    }
                    else if(chatobj.getMedia_type()==C.AUDIO)
                    {
                        holder.txtMsgRight.setVisibility(View.GONE);
                        holder.imageRight.setImageBitmap(null);
                        holder.imageRight.setVisibility(View.VISIBLE);
                        holder.progressRight.setVisibility(View.GONE);

                        if(chatobj.getMedia_status()==C.UPLOADING)
                        {
                            holder.progressRight.setVisibility(View.VISIBLE);
                            holder.statusRight.setVisibility(View.GONE);
                        }
                        else if(chatobj.getMedia_status()==C.UPLOADED || chatobj.getMedia_status()==C.STATUS_DELIVERED || chatobj.getMedia_status()==C.STATUS_SENT)
                        {
                            holder.progressRight.setVisibility(View.GONE);

                            holder.statusRight.setVisibility(View.VISIBLE);
                            holder.statusRight.setImageBitmap(null);
                            holder.statusRight.setImageBitmap(deliveryStatus);

                            if(C.MSG_ID_OF_PLAYED_AUDIO == -1)
                            {
                                Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.play_white);
                                holder.imageRight.setImageBitmap(largeIcon);
                            }
                            else if(chatobj.getSno()==C.MSG_ID_OF_PLAYED_AUDIO)
                            {
                                Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.pause_white);
                                holder.imageRight.setImageBitmap(largeIcon);
                            }
                            else
                            {
                                Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.play_white);
                                holder.imageRight.setImageBitmap(largeIcon);
                            }
                        }


                        holder.imageRight.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Log.d("GGGGGGGGGG","HHHHHHHHHHHH");


                                if(C.MSG_ID_OF_PLAYED_AUDIO == -1)
                                {
                                    player = play_audio_using_filepath(chatobj.getMsg());
                                    C.MSG_ID_OF_PLAYED_AUDIO = Integer.parseInt(Integer.toString(chatobj.getSno()));
                                    Log.d("JJJJJJJJ","KKKKKKK");
                                    player.start();
                                    notifyDataSetChanged();
                                    //mList.setSelection(position);
                                    mList.setSelected(true);
                                }
                                else if(chatobj.getSno()==C.MSG_ID_OF_PLAYED_AUDIO)
                                {
                                    Log.d("LLLLL","MMMMM");
                                    C.MSG_ID_OF_PLAYED_AUDIO = -1;
                                    if (player != null) {
                                        if (player.isPlaying()) {
                                            player.stop();
                                        }
                                        player.release();
                                    }
                                    notifyDataSetChanged();
                                    //mList.setSelection(position);
                                    mList.setSelected(true);
                                }
                                player.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
                                {
                                    @Override
                                    public void onCompletion(MediaPlayer mp)
                                    {
                                        Log.d("Completed Media Playing","Done");
                                        C.MSG_ID_OF_PLAYED_AUDIO = -1;
                                        notifyDataSetChanged();
                                        //mList.setSelection(position);
                                        mList.setSelected(true);
                                    }
                                });

                            }
                        });
                    }
                    else if(chatobj.getMedia_type()==C.VIDEO)
                    {
                        holder.txtMsgRight.setVisibility(View.GONE);
                        holder.imageRight.setImageBitmap(null);
                        holder.imageRight.setVisibility(View.VISIBLE);

                        if(chatobj.getMedia_status()==C.UPLOADING)
                        {
                            holder.progressRight.setVisibility(View.VISIBLE);
                            holder.statusRight.setVisibility(View.GONE);
                        }
                        else if(chatobj.getMedia_status()==C.UPLOADED || chatobj.getMedia_status()==C.STATUS_DELIVERED || chatobj.getMedia_status()==C.STATUS_SENT)
                        {
                            holder.statusRight.setVisibility(View.VISIBLE);
                            holder.statusRight.setImageBitmap(null);
                            holder.statusRight.setImageBitmap(deliveryStatus);

                            holder.progressRight.setVisibility(View.GONE);

                            Bitmap bmThumbnail = ThumbnailUtils.createVideoThumbnail(chatobj.getMsg(), MediaStore.Video.Thumbnails.MICRO_KIND);
                            holder.imageRight.setImageBitmap(bmThumbnail);
                        }


                        File file = new File(chatobj.getMsg());
                        final File finalFile1 = file;
                        holder.imageRight.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.fromFile(finalFile1), "video/*");
                                startActivity(intent);
                            }
                        });
                    }
                    else if(chatobj.getMedia_type()==C.STICKER)
                    {
                        holder.txtMsgRight.setVisibility(View.GONE);
                        holder.imageRight.setImageBitmap(null);
                        holder.imageRight.setVisibility(View.VISIBLE);
                        holder.progressRight.setVisibility(View.GONE);

                        drawable_id = stickers[Integer.parseInt(chatobj.getMsg())];
                        sticker = BitmapFactory.decodeResource(getResources(), drawable_id);
                        holder.imageRight.setImageBitmap(sticker);

                        holder.imageRight.setScaleType(ImageView.ScaleType.CENTER_CROP);

                        holder.statusRight.setVisibility(View.VISIBLE);
                        holder.statusRight.setImageBitmap(null);
                        holder.statusRight.setImageBitmap(deliveryStatus);


                        holder.imageRight.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {

                                final Dialog dialog = new Dialog(ActivityChatWindow.this);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setContentView(R.layout.large_image);
                                ImageView image = (ImageView) dialog
                                        .findViewById(R.id.dispImage);
                                image.setImageBitmap(null);
                                sticker = BitmapFactory.decodeResource(getResources(), drawable_id);
                                image.setImageBitmap(sticker);
                                image.setScaleType(ImageView.ScaleType.FIT_XY);

                                dialog.show();
                            }
                        });
                    }
                    holder.txtTimeRight.setText(fetcheddate+" "+fetchampm);
                }

            }
            return v;
        }



    }

    ///////////////////////////////
    class DownloadFileUsingUrl extends AsyncTask<Download, String, String> {
        private ProgressBar v;
        private int msg_id=0;
        public DownloadFileUsingUrl(ProgressBar v, int msg_id)
        {
            this.v = v;
            this.msg_id = msg_id;
        }
        private Handler handler;



        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            v.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(Download... aurl) {
            int count;
            String filepath = null;
            Download[] down = aurl;
            try {

                URL url = new URL(down[0].getUrl());
                URLConnection conexion = url.openConnection();
                conexion.connect();

                String extention = down[0].getContent_type();



                int lenghtOfFile = conexion.getContentLength();
                Log.d("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);

                InputStream input = new BufferedInputStream(url.openStream());
                filepath = App.APP_FOLDER + "/ChatMedia/" +"WTP_"+ System.currentTimeMillis() + extention;
                OutputStream output = new FileOutputStream(filepath);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress(""+(int)((total*100)/lenghtOfFile));
                    output.write(data, 0, count);
                }

                output.flush();
                output.close();
                input.close();
                db.update_download_status(Integer.toString(msg_id), filepath);
            } catch (Exception e) {}
            return filepath;
        }
        protected void onProgressUpdate(final String... progress) {
            Log.d("ANDRO_ASYNC",progress[0]);

        }


        @Override
        protected void onPostExecute(String unused) {


            updateUI();

        }
    }


}

