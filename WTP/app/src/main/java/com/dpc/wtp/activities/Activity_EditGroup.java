package com.dpc.wtp.activities;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.SubMenu;
import com.dpc.wtp.Models.GroupDetails;
import com.dpc.wtp.Models.NewMsgCount;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.EText;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.customs.Custom_Send_Message;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.services.WTP_Service;


import org.jivesoftware.smack.packet.Message;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;


public class Activity_EditGroup extends MyActivity {

    ListView MyChatUsersList;
    Integer[] Uservalues;
    Text btnModify;
    Text title;
    EText txtGroupName;
    List<String> stringList;
    private BroadcastReceiver broadcastReceiver;

    ArrayList<NewMsgCount> counterlist;

    ArrayList<GroupDetails> groupdata;
    int servergroupid=0;
    String[] memberlist;
    String groupname, group_creationdate, group_admin;
    List<String> memberarraylist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar);

        title = (Text)findViewById(android.R.id.title);

        super.onCreate(savedInstanceState);
        db = new DatabaseHelper(this);
        setContentView(R.layout.activity_edit_group);
        MyChatUsersList = (ListView) findViewById(R.id.MyChatUsersList);
        txtGroupName = (EText) findViewById(R.id.txtGroupName);
        btnModify = (Text) findViewById(R.id.btnModify);
        counterlist = new ArrayList<NewMsgCount>();

        groupdata = new ArrayList<GroupDetails>();
        WTP_Service.CURRENT_CHAT_USER = "@hellochat";

        Intent i = getIntent();

        if(i.hasExtra("servergroupid"))
        {
            servergroupid = i.getIntExtra("servergroupid",0);
            Log.d("Getting server group id in edit group as", Integer.toString(servergroupid));
        }

        MyChatUsersList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    final int position, long id)
            {
                NewMsgCount itemValue = (NewMsgCount) MyChatUsersList.getItemAtPosition(position);
                if(Integer.parseInt(itemValue.getUsername())!=Integer.parseInt(App.get_userid()))
                {
                    AlertDialog(position);
                }

            }
        });



        broadcastReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                Log.d("ChatWindow Broadcast Receiver got notified","");
                setUiValues();
                invalidateOptionsMenu();
            }
        };

        setUiValues();

        btnModify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = txtGroupName.getText().toString();



                if(name.equalsIgnoreCase(groupname))
                {
                    Toast.makeText(getApplicationContext(),"Old and new name are same.", Toast.LENGTH_LONG).show();
                }
                else if(name.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(),"Please enter group name", Toast.LENGTH_LONG).show();
                }
                else
                {
                    db.update_group_name(name, Integer.toString(servergroupid));

                    Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),servergroupid,"Hi, This is a group name update.", C.GROUP_NAME_UPDATE);
                    csm.send_group_message();

                    title.setText(name);
                    finish();
                }
                txtGroupName.setText("");
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        Log.d("Current User id and admin id is", App.get_userid()+" "+group_admin);
        if(Integer.parseInt(App.get_userid())==Integer.parseInt(group_admin))
        {
            SubMenu submenu = menu.addSubMenu("");
            submenu.setIcon(R.drawable.abs__ic_menu_moreoverflow_normal_holo_dark);
            submenu.add(1,1, 1, "Add Members");

            submenu.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS |
                    MenuItem.SHOW_AS_ACTION_WITH_TEXT);

        }



        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;

            case 1:
                intent = new Intent(Activity_EditGroup.this, Activity_NonGroupMembers.class);
                intent.putExtra("servergroupid", servergroupid);
                startActivity(intent);

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public String get_current_time()
    {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
        return sdf.format(c.getTime());
    }

    public String get_universal_date(String time)
    {
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm a");


        Date date = null;
        try
        {
            //Here you say to java the initial timezone. This is the secret
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = sdf.parse(time);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        //Here you set to your timezone

        sdf.setTimeZone(TimeZone.getDefault());
        time = sdf.format(date);
        return time;
    }


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }

    public void AlertDialog(final int position)
    {
        final CharSequence[] options;




            if(Integer.parseInt(App.get_userid())==Integer.parseInt(group_admin))
            {
                options= new CharSequence[]{"Add to Contacts", "Remove from Group"};
            }
            else
            {
                options= new CharSequence[]{"Add to Contacts"};
            }





        AlertDialog.Builder builder=new AlertDialog.Builder(Activity_EditGroup.this)
                .setTitle("")
                .setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int index) {
                        // TODO Auto-generated method stub
                        Toast.makeText(getApplicationContext(),
                                "The selected option is " + options[index], Toast.LENGTH_LONG).show();
                        if (options[index].equals("Add to Contacts")) {


                            String mobile = "9878767656";//itemValue.get_number();
                            Intent intent = new Intent(
                                    ContactsContract.Intents.SHOW_OR_CREATE_CONTACT,
                                    Uri.parse("tel:" + mobile));
                            intent.putExtra(ContactsContract.Intents.EXTRA_FORCE_CREATE, true);
                            startActivity(intent);
                        }
                        else if (options[index].equals("Remove from Group"))
                        {

                            NewMsgCount itemValue = (NewMsgCount) MyChatUsersList.getItemAtPosition(position);
                            String selectedid = itemValue.getUsername();

                            String localTime = get_current_time();
                            String msgDateTime = get_universal_date(localTime);

                            //Get the group members user ids
                            ArrayList<GroupDetails> groupdata;
                            groupdata = db.get_group_details(Integer.toString(servergroupid));
                            GroupDetails gd = groupdata.get(0);
                            String members = gd.getGroup_users();
                            String[] memberlist = members.split(", "); // don't remove space else it only 1 name checked
                            List<String> memberarraylist = new LinkedList(Arrays.asList(memberlist));
                            Log.d("My members arraylist", memberarraylist.toString());


                            //remove the selected user's user id from list
                            memberarraylist.remove(selectedid);
                            String users = memberarraylist.toString();
                            users = users.substring(1, users.length() - 1);
                            Log.d("My members arraylist without brackets", users);


                            Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),servergroupid,"Hi, Hi, This is a group update.", C.GROUP_MEMBERS_UPDATE);
                            csm.send_group_members_update_message(users);



                            db.update_group_users(users, Integer.toString(servergroupid), gd.getGroup_admin()); // first so that I should not get this update

                            setUiValues();
                        }

                    }
                });
        AlertDialog alertdialog=builder.create();
        alertdialog.show();
        setUiValues();


    }


    @Override
    public void onResume()
    {
        super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
        registerReceiver(broadcastReceiver, new IntentFilter(WTP_Service.BROADCAST_ACTION));
        groupdata = db.get_group_details(Integer.toString(servergroupid));
        GroupDetails gd = groupdata.get(0);
        final String members = gd.getGroup_users();
        groupname = gd.getGroup_name();
        group_admin = gd.getGroup_admin();
        memberlist = members.split(", "); // don't remove space else it only 1 name checked
        memberarraylist = Arrays.asList(memberlist);

        txtGroupName.setText(groupname);
        Log.d("Memberlist", memberlist.toString());
        Log.d("Member array list", memberarraylist.toString());
        Log.d("Getting server group id in edit group as", Integer.toString(servergroupid));

        stringList = new ArrayList<String>(Arrays.asList(memberlist));
        setUiValues();

    }


    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;

    }

    private void setUiValues()
    {
        groupdata = db.get_group_details(Integer.toString(servergroupid));
        GroupDetails gd = groupdata.get(0);
        final String members = gd.getGroup_users();
        groupname = gd.getGroup_name();
        group_creationdate = gd.getCreation_date();
        group_admin = gd.getGroup_admin();

        memberlist = members.split(", "); // don't remove space else it only 1 name checked
        memberarraylist = new LinkedList(Arrays.asList(memberlist));

        txtGroupName.setText(groupname);
        title.setText(groupname);
        Log.d("Memberlist", memberlist.toString());
        Log.d("Member array list", memberarraylist.toString());
        Log.d("Getting server group id in edit group as", Integer.toString(servergroupid));



        stringList = new ArrayList<String>(Arrays.asList(memberlist));
        counterlist.clear();
        for(int p = 0; p<=stringList.size()-1;p++)
        {
            Log.d("P value", Integer.toString(p));
            String name  = stringList.get(p).toString();

            NewMsgCount mymsgcount = new NewMsgCount(name,0, "", "",1,101);// change it to id received from server
            counterlist.add(mymsgcount);
            Log.d("counterlist", counterlist.toString());
        }
        MyChatUsersList.setAdapter(new MyAdapter(counterlist,Activity_EditGroup.this));
    }

    class MyAdapter extends BaseAdapter {

        View v;
        LayoutInflater inflater;
        ViewHolder holder;
        private Context c;
        private ArrayList<NewMsgCount> mygroupusers;

        public MyAdapter(ArrayList<NewMsgCount> mCurrentList, Context mContext) {
            this.mygroupusers = mCurrentList;
            this.c = mContext;

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            if (mygroupusers != null)
                return mygroupusers.size();
            else
                return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            if (mygroupusers != null)
                return mygroupusers.get(position);
            else
                return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        private class ViewHolder {
            Text txtName;
        }

        @Override
        public View getView(final int arg0,final View convertView, ViewGroup arg2)
        {
            final NewMsgCount temp = mygroupusers.get(arg0);
            v = convertView;
            if (v == null)
            {
                inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.edit_group_single_row, null);
                holder = new ViewHolder();
                holder.txtName = (Text) v.findViewById(R.id.txtName);

                v.setTag(holder);
            }
            else
            {
                holder = (ViewHolder) v.getTag();
            }


            if(Integer.parseInt(temp.getUsername())==Integer.parseInt(group_admin))
            {
                holder.txtName.setText(temp.getUsername()+" - Admin");
            }
            else
            {
                holder.txtName.setText(temp.getUsername());
            }
            return v;
        }
    }




}
