package com.dpc.wtp.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.dpc.wtp.JSONParser;
import com.dpc.wtp.Models.Server_Params;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.DatabaseHelper;

import com.dpc.wtp.Utils.EText;
import com.dpc.wtp.Utils.MyAlert.OnOKListener;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.Utils.TwitterUtil;
import com.dpc.wtp.customs.DialogwithListview;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.customs.MyProgress;
import com.dpc.wtp.customs.Twitter_Login;
import com.dpc.wtp.interfaces.ListViewClickListener;
import com.dpc.wtp.tasks.Update_FB_TWIT;
import com.dpc.wtp.tasks.Update_Users;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginButton;
import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.analytics.HitBuilders;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

public class Activity_Login extends MyActivity implements OnClickListener,ListViewClickListener,OnOKListener{
	
	private Button login,forgot;
	
	private LoginButton fb;
	
	private Text login_txt,signup_email,forgot_txt,twitter,tc;
	
	private EText email,password,f_email;
	
	private LinearLayout ll,ll_twitter,ll_login,ll_forgot;
	
	private ArrayList<String> mail_ids;
	
	private DatabaseHelper h;
	
	private String gcmId = null;
	
	private Text title;
	
	private static Activity a;
	
	private MyProgress dialog;
	
	private Net_Detect net;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.actionbar);
		
		title = (Text)findViewById(android.R.id.title);
		title.setText(R.string.ac_login);
		if(App.get_userid() != null)
		{
			Session s  = Session.getActiveSession();
			if(App.is_fb_loggedin())
			{
				if(App.get_city() == null)
				{
					Intent i = new Intent(Activity_Login.this,Activity_City.class);
					startActivity(i);
					finish();
				}
				else
				{
						Update_Users update = new Update_Users(this);
						update.execute();
						Intent i = new Intent(Activity_Login.this,Activity_Home.class);
						startActivity(i);
						finish();
				}
			}
			else
			{
				Intent i = new Intent(Activity_Login.this,Activity_Facebook.class);
				startActivity(i);
				finish();
			}
	
		}
		else
		{
			Session s = Session.getActiveSession();
			if(s != null)
			{
				s.closeAndClearTokenInformation();
			}
		}
		super.onCreate(savedInstanceState);

        if(App.get_userid() == null)
        {
            app_tracker.setScreenName(C.SCREEN.SCREEN_LOGIN.get_screen());
            app_tracker.send(new HitBuilders.AppViewBuilder().build());
        }
		init_net_details();
		a = this;
		register();
		setContentView(R.layout.ac_login);
		
		
		dialog = new MyProgress(this);
		set_layout();
		final AccountManager manager = AccountManager.get(this);
		final Account[] accounts = manager.getAccountsByType("com.google");
		final int size = accounts.length;
		mail_ids = new ArrayList<String>();
		for (int i = 0; i < size; i++) 
		{
			mail_ids.add(accounts[i].name);
		}
		uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);
		if (savedInstanceState != null) 
		{
		    pendingPublishReauthorization = 
		        savedInstanceState.getBoolean(PENDING_PUBLISH_KEY, false);
		}


    }
	
	private void init_net_details()
	{
		net = new Net_Detect(this);
		
	}
	
	public static void stop()
	{
		if(a != null)
		{
			a.finish();
		}
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
		if(new Twitter_Login(this).is_twitter_login())
		{
			twitter.setText("Logout from twitter");
		}
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;


    }

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
	}
	
	
	 @Override
	    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	        super.onActivityResult(requestCode, resultCode, data);
	        
	        uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
	        Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	        if (Session.getActiveSession() != null && Session.getActiveSession().isOpened())
	        {
	            
	        	App.set_fb_login(true);


	         }
	    }

	@Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
    	
			
        return super.onCreateOptionsMenu(menu);
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId())
		{
	
		case android.R.id.home:
			super.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
    }

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}
	@Override
	public void setOKSubmitListener(int value) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void setOnSubmitListener(int value) {
		// TODO Auto-generated method stub
		email.setText(mail_ids.get(value));
		f_email.setText(mail_ids.get(value));
	}
	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
		case R.id.email:
		case R.id.forgot_email:
			Bundle b = new Bundle();
			b.putString("head", "Select a Gmail ID");
			DialogwithListview dialog1 = new DialogwithListview(mail_ids);
			dialog1.setArguments(b);
			dialog1.list_listener = Activity_Login.this;
			dialog1.show(getSupportFragmentManager(), "");
			
			break;
		case R.id.login:
			register();
			check_user_details();
			
			break;
		case R.id.login_txt:
			title.setText(R.string.ac_login_email);
			ll.setVisibility(View.GONE);
			ll_login.setVisibility(View.VISIBLE);
			email.setText("");
			new Twitter_Login(this).clear_twitter();
			Session s = Session.getActiveSession();
			if(s != null)
			{
				s.closeAndClearTokenInformation();
			}
			App.set_fbid(null);
			App.set_twitid(null);
			break;
		case R.id.signup_email:
			open_register();
			
			break;
		case R.id.fb:
			if(!net.isConnectingToInternet())
			{
				net.alert(this.getSupportFragmentManager());
                return;
			}
            dialog = new MyProgress(this);
            dialog.show();
			break;
		case R.id.ll_twitter:
			Twitter_Login log = new Twitter_Login(this);
			if(log.is_twitter_login())
			{
				log.clear_twitter();
				twitter.setText("Connect with Twitter");
			}
			else
			{
				new Twitter_Login(this).login_to_twitter();
			}
			break;
		case R.id.forgot_txt:
			title.setText(R.string.ac_resetpassword);
			ll_login.setVisibility(View.GONE);
			ll_forgot.setVisibility(View.VISIBLE);
			f_email.setText("");
			break;
		case R.id.reset:
			if (f_email.getText().toString() != null	&& f_email.getText().toString().length() > 0	&& f_email.getText().toString().contains("@")) 
			{
			
				new Reset_Pass().execute();
			}
			else
			{
				C.alert(getApplicationContext(), "Please enter proper email address");
			}
			break;
		}
		
	}
	private void open_register()
	{
		Intent i = new Intent(this,Activity_Register.class);
		startActivity(i);
		finish();
	}
	private void set_layout() {
		// TODO Auto-generated method stub
		signup_email = (Text)findViewById(R.id.signup_email);
		forgot_txt = (Text)findViewById(R.id.forgot_txt);
		login_txt = (Text)findViewById(R.id.login_txt);
		twitter = (Text)findViewById(R.id.twitter);
        tc = (Text)findViewById(R.id.tc);
        tc.setText(Html.fromHtml(
                "By registering, you agree to our <br/>" +
                        "<a href=\"http://wtp.club/inner1/tandc.html\">"+"<font color='#4e6bac'>"+"T &amp C</font></a>" +
                        "."));
        tc.setMovementMethod(LinkMovementMethod.getInstance());

        fb = (LoginButton)findViewById(R.id.fb);
		login = (Button)findViewById(R.id.login);
		forgot = (Button)findViewById(R.id.reset);
		
		email = (EText)findViewById(R.id.email);
		f_email = (EText)findViewById(R.id.forgot_email);
		password = (EText)findViewById(R.id.password);
		
		ll = (LinearLayout)findViewById(R.id.ll);
		ll_login = (LinearLayout)findViewById(R.id.ll_login);
		ll_twitter = (LinearLayout)findViewById(R.id.ll_twitter);
		ll_forgot = (LinearLayout)findViewById(R.id.ll_forgotpass);

        tc.setOnClickListener(this);
		login_txt.setOnClickListener(this);
		forgot_txt.setOnClickListener(this);
		signup_email.setOnClickListener(this);
		email.setOnClickListener(this);
		f_email.setOnClickListener(this);
		login.setOnClickListener(this);
		ll_twitter.setOnClickListener(this);
		fb.setOnClickListener(this);
		forgot.setOnClickListener(this);
		
		email.setBackgroundResource(R.drawable.ed_new);
		fb.setText("Connect with facebook");
		fb.setReadPermissions(Arrays.asList("public_profile","user_friends","email"));
		
		fb.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
            @Override
            public void onUserInfoFetched(GraphUser user) {
                if(dialog != null)
                {
                    dialog.dismiss();
                }
                if(user != null)
                {
                    try {
                        String gender = user.getInnerJSONObject().getString(C.PARAMS.GENDER.get_param());
                        if(gender.equalsIgnoreCase("male"))
                        {
                            gender = "1";
                        }
                        else if(gender.equalsIgnoreCase("female"))
                        {
                            gender = "2";
                        }
                        App.set_gender(gender);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                	if(App.get_userid() == null)
						try {
							new social_login(user.getId(),user.getInnerJSONObject().getString("email")).execute();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
                
                }
               
                
            }
        });
		
	}

	private void check_user_details() 
	{
		if (email.getText().toString() != null	&& email.getText().toString().length() > 0	&& email.getText().toString().contains("@")) 
			{
				
				if (password.getText().toString() != null&& password.getText().toString().length() >= 6) 
				{
					
							if(net.isConnectingToInternet())
							{
								new Login_User().execute();
								
							}
							else
							{
								net.alert(getSupportFragmentManager());
								return;
							}
					
					
				} 
				else 
				{
					C.alert(getApplicationContext(),
							"Password length should be more than 5");
				}
			}
			else 
			{
					C.alert(getApplicationContext(),
							"Please enter proper email address.");
			}
			
			
		}



///////////////////////////////////////////
public class Login_User extends AsyncTask<String, Long, Boolean> {

boolean success = false,user_not_exist = false;;
@Override
protected void onPreExecute() {
	//dialog.setMessage("Loading please wait...");
	dialog.setCancelable(false);
	dialog.show();

}

@Override
protected Boolean doInBackground(String... params) {
	JSONObject obj = login_user();
	if(obj != null)
	{
		try {
			if(obj.has(PARAMS.STATUS.get_param()))
			{
				int status = obj.getInt(PARAMS.STATUS.get_param());
				if(status < 0)
				{
					user_not_exist = true;
					
				}
				else if(status == 1)
				{
					new C().set_user_details(obj);
					if(!App.is_contacts_updated())
					{
						publishProgress();
			        	success = contactstask();
					}
					
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	return user_not_exist;
}

@Override
protected void onProgressUpdate(Long... progress) {
	C.alert(getApplicationContext(), "Log in successfull.");
	//dialog.setMessage("Reading contacts...");
}

@Override
protected void onPostExecute(Boolean result) {
dialog.dismiss();
if(result)
{
	C.alert(getApplicationContext(), "User not exist");
	
}
else// if(success)
{
	Intent i = new Intent(Activity_Login.this,Activity_Facebook.class);
	startActivity(i);
    app_tracker.send(new C().generate_analytics(C.CATEGORY.LOGIN.get_category(),C.ACTION.ACTION_OK.get_action(),C.LABEL.WTP.get_label()));
	finish();
}


}

@Override
protected void onCancelled() {
super.onCancelled();
}

}

public JSONObject login_user()
{
	JSONParser parser = new JSONParser();
	JSONObject user = null;
    if(App.get_gcmid() != null) {
        user = parser.makeHttpRequest(C.SERVER_URL, "POST", new Server_Params().get_login_params(email.getText().toString(), password.getText().toString(), App.get_gcmid(), C.get_date()));
    }
    else
    {
        register();
        Log.d("gcmid","missing");
    }
	return user;
}

public boolean contactstask()
{
	boolean success = false;;
	JSONObject user;
	ArrayList<String> nos = null;
	JSONParser parser = new JSONParser();
		
    	ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		
		params.add(new BasicNameValuePair(C.METHOD.GET_APP_USERS.get_method()," "));
		if(!App.is_contacts_updated())
		nos = C.readContacts(getApplicationContext());
		if(nos != null)
		{
			success = true;
			App.set_contacts_updated(true);
			for(int i = 0;i < nos.size();i++)
			{
				params.add(new BasicNameValuePair(PARAMS.NUMBER.get_param()+"[]",nos.get(i)));
				h = new DatabaseHelper(this);
				h.add_new_android_contact(nos.get(i));
				
			}
			params.add(new BasicNameValuePair(PARAMS.USER_ID.get_param(),App.get_userid()));
			params.add(new BasicNameValuePair(PARAMS.TOKEN_STR.get_param(),App.get_token()));
			
		
		}
		user = parser.makeHttpRequest(C.SERVER_URL, "POST", params);
		if(user != null)
		{
			try {
				if(user.getInt(PARAMS.STATUS.get_param()) > 0)
				{
					C.add_app_users_todb(this,user);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
			
    	return success;
   
}	


void register()
{
	gcmId = GCMRegistrar.getRegistrationId(this.getApplicationContext());
	if (gcmId.equals("")) 
	{
		GCMRegistrar.register(this, C.SENDER_ID);
	} 
	else 
	{
		
		final Context context = this.getApplicationContext();
		if (GCMRegistrar.isRegisteredOnServer(this.getApplicationContext())) 
		{
			
			gcmId = GCMRegistrar.getRegistrationId(this.getApplicationContext());
			 
		} 
		else 
		{
			
			GCMRegistrar.setRegisteredOnServer(context, true);
					
		}
		
	}
	Log.i("gcmId", "Device registered: regId = " + gcmId);
}

///////////////////////////////////
public class Reset_Pass extends AsyncTask<String, Long, Boolean> {
private ProgressDialog dialog = new ProgressDialog(Activity_Login.this);
boolean success = false,user_not_exist = false;;
@Override
protected void onPreExecute() {
	dialog.setMessage("Loading please wait...");
	dialog.setCancelable(false);
	dialog.show();

}

@Override
protected Boolean doInBackground(String... params) {
	JSONObject obj = send_pass();
	if(obj != null)
	{
		try {
			if(obj.has(PARAMS.STATUS.get_param()))
			{
				int status = obj.getInt(PARAMS.STATUS.get_param());
				if(status < 0)
				{
					user_not_exist = true;
					
				}
				else if(status == 1)
				{
					success = true;
					
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	return user_not_exist;
}

@Override
protected void onPostExecute(Boolean result) {
dialog.dismiss();
if(result)
{
	C.alert(getApplicationContext(), "User not exist");
	
}
else if(success)
{
	C.alert(getApplicationContext(), "Please check your email for password recovery");
	finish();
}
else
{
	C.alert(getApplicationContext(), "Error in sending password,please try again");
	}


}

}

public JSONObject send_pass()
{
	JSONParser parser = new JSONParser();
	JSONObject user = null;
	user = parser.makeHttpRequest(C.SERVER_URL, "POST", new Server_Params().get_reset_pass_params(f_email.getText().toString()));
	
	return user;
}
public JSONObject log_in(String id,String email)
{
	JSONParser parser = new JSONParser();
	JSONObject user = null;
	if(App.get_gcmid() != null) {


        user = parser.makeHttpRequest(C.SERVER_URL, "POST", new Server_Params().get_fblogin_params(id, email, App.get_gcmid(), C.get_date()));

    }else {
        register();
        Log.d("no gcm id", "gcm id not found");
    }
   // else if(gcmId != null && gcmId != "") {
    //    register();
     //   user = parser.makeHttpRequest(C.SERVER_URL, "POST", new Server_Params().get_twitlogin_params(id, gcmId, C.get_date()));
    //}
	return user;
}
////////////////////////////////////////////////////////////////
public class social_login extends AsyncTask<String, Long, Boolean> {
private MyProgress dialog = new MyProgress(Activity_Login.this);
boolean success = false,user_not_exist = false;
String id,email;
public social_login(String id,String email)
{
	this.id = id;
	this.email = email;
}
@Override
protected void onPreExecute() {
	dialog.setMessage("Please wait...");
	dialog.setCancelable(false);
	dialog.show();

}

@Override
protected Boolean doInBackground(String... params) {
	JSONObject obj = log_in(id,email);
	if(obj != null)
	{
		try {
			if(obj.has(PARAMS.STATUS.get_param()))
			{
				int status = obj.getInt(PARAMS.STATUS.get_param());
				if(status == 0)
				{
					user_not_exist = true;

					
				}
				else if(status == 1)
				{
					success = true;
					App.set_registered(success);
					new C().set_user_details(obj);
                    if(!App.is_contacts_updated())
                    {
                        publishProgress();
                        contactstask();
                    }
					
				}

        }
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	return user_not_exist;
}

@Override
protected void onPostExecute(Boolean result) {
	dialog.dismiss();
	if(result)
	{
        App.set_fb_login(true);
		C.alert(getApplicationContext(), "User not exist in WTP, please register");
		open_register();
        app_tracker.send(new C().generate_analytics(C.CATEGORY.LOGIN.get_category(),C.ACTION.ACTION_OK.get_action(),C.LABEL.FB.get_label()));
	}
	else if(success)
	{
        App.set_fb_login(true);
		if(!App.is_shared_onfb())
		{
			publishStory();
		}
		C.alert(getApplicationContext(), "Logged in successfully");
		Intent i = new Intent(Activity_Login.this,Activity_City.class);
		startActivity(i);
        app_tracker.send(new C().generate_analytics(C.CATEGORY.LOGIN.get_category(),C.ACTION.ACTION_OK.get_action(),C.LABEL.FB.get_label()));

        finish();
	}
	else
	{
		C.alert(getApplicationContext(), "Please try after some time");
	}
	


}

}
/////////////////////////////////////////////////////
class TwitterGetAccessTokenTask extends AsyncTask<String, String, String> {

    @Override
    protected void onPostExecute(String userName) {
       // textViewUserName.setText(Html.fromHtml("<b> Welcome " + userName + "</b>"));
    	if(userName != null)
    	{
    		C.alert(getApplicationContext(), "Successfully logged in as "+userName);
    		//new Update_FB_TWIT(false).execute();
    		//new social_login(false,App.get_twitid()).execute();
        	
    	}
         else
        	 C.alert(getApplicationContext(), "Failed to login.");
    }

    @Override
    protected String doInBackground(String... params) {

        Twitter twitter = TwitterUtil.getInstance().getTwitter();
        RequestToken requestToken = TwitterUtil.getInstance().getRequestToken(true);
        if (params[0] != null) {
            try {

                AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, params[0]);
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(C.PREFERENCE_TWITTER_OAUTH_TOKEN, accessToken.getToken());
                editor.putString(C.PREFERENCE_TWITTER_OAUTH_TOKEN_SECRET, accessToken.getTokenSecret());
                editor.putBoolean(C.PREFERENCE_TWITTER_IS_LOGGED_IN, true);
                editor.commit();
                App.set_twitid(String.valueOf(accessToken.getUserId()));
                return twitter.showUser(accessToken.getUserId()).getName();
            } catch (TwitterException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        } else {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String accessTokenString = sharedPreferences.getString(C.PREFERENCE_TWITTER_OAUTH_TOKEN, "");
            String accessTokenSecret = sharedPreferences.getString(C.PREFERENCE_TWITTER_OAUTH_TOKEN_SECRET, "");
            AccessToken accessToken = new AccessToken(accessTokenString, accessTokenSecret);
            try {
                TwitterUtil.getInstance().setTwitterFactory(accessToken);
                App.set_twitid(String.valueOf(accessToken.getUserId()));
                return TwitterUtil.getInstance().getTwitter().showUser(accessToken.getUserId()).getName();
            } catch (TwitterException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
//////////////////////////////////////////
private UiLifecycleHelper uiHelper;
protected Session.StatusCallback callback = new Session.StatusCallback() {
    @Override
    public void call(Session session, SessionState state, Exception exception) {
        onSessionStateChange(session, state, exception);
    }
};
private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
    @Override
    public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
        Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
    }

    @Override
    public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
        Log.d("HelloFacebook", "Success!");
    }
};

@Override
public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putBoolean(PENDING_PUBLISH_KEY, pendingPublishReauthorization);
    uiHelper.onSaveInstanceState(outState);
}
private void onSessionStateChange(Session session, SessionState state, Exception exception) {
    if (state.isOpened()) {
    	App.set_fb_login(true);
    	if (pendingPublishReauthorization && 
    	        state.equals(SessionState.OPENED_TOKEN_UPDATED)) {
    	    pendingPublishReauthorization = false;
    	    publishStory();
    	}
    } else if (state.isClosed()) {
       // shareButton.setVisibility(View.INVISIBLE);
    }
}

private void publishStory() {
    Session session = Session.getActiveSession();

    if (session != null){

        // Check for publish permissions    
        List<String> permissions = session.getPermissions();
        if (!isSubsetOf(PERMISSIONS, permissions)) {
            pendingPublishReauthorization = true;
            Session.NewPermissionsRequest newPermissionsRequest = new Session
                    .NewPermissionsRequest(this, PERMISSIONS);
        session.requestNewPublishPermissions(newPermissionsRequest);
            return;
        }

        super.publishStory(getResources().getString(R.string.app_name),C.app_caption,C.app_info,C.app_url,C.app_logo,true,false);
 		
    }

}
private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
    for (String string : subset) {
        if (!superset.contains(string)) {
            return false;
        }
    }
    return true;
}
}

	

