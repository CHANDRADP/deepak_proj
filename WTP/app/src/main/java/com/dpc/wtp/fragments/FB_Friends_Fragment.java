package com.dpc.wtp.fragments;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.dpc.wtp.R;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Models.WTP_Contact;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.EText;
import com.dpc.wtp.Utils.TextB;
import com.dpc.wtp.adapters.Invite_Friends_Adapter;
import com.dpc.wtp.customs.MyFragment;
import com.dpc.wtp.customs.MyProgress;
import com.dpc.wtp.customs.WTP_Sorter;
import com.facebook.AppEventsLogger;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestBatch;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.model.OpenGraphAction;
import com.facebook.model.OpenGraphObject;
import com.facebook.widget.FacebookDialog;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
public class FB_Friends_Fragment extends MyFragment implements OnClickListener
{
	private final String TAG = ((Object)this).getClass().getName();
	private ArrayList<WTP_Contact> list_items = new ArrayList<WTP_Contact>();
	private Invite_Friends_Adapter list_adapter;
	private ListView list;
	private EText ed;
	private TextB invite;
	private String event_id = null;
	private Event e;
	List<GraphUser> tags = new ArrayList<GraphUser>();
	MyProgress progress;
	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        options = new DisplayImageOptions.Builder()
    	.showStubImage(R.drawable.user)
    	.showImageForEmptyUri(R.drawable.user)
    	.showImageOnFail(R.drawable.user)
    	.cacheInMemory(true)
    	.cacheOnDisc(true)
    	.bitmapConfig(Bitmap.Config.RGB_565)
    	.build();
     //   setHasOptionsMenu(true);
        uiHelper = new UiLifecycleHelper(this.getSherlockActivity(), callback);
        uiHelper.onCreate(savedInstanceState);
        if (savedInstanceState != null) 
		{
		    pendingPublishReauthorization = 
		        savedInstanceState.getBoolean(PENDING_PUBLISH_KEY, false);
		}
        Bundle b = this.getArguments();
        if(b != null)
        if(b.containsKey(PARAMS.EVENT_ID.get_param()))
        {
        	event_id = b.getString(PARAMS.EVENT_ID.get_param(),null);
        }
        progress = new MyProgress(this.getSherlockActivity());
		progress.setCancelable(false);
		progress.show();
    }
	
	
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		AppEventsLogger.deactivateApp(this.getSherlockActivity(),getResources().getString(R.string.fb_app_id));
		
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
        Session.getActiveSession().onActivityResult(this.getSherlockActivity(), requestCode, resultCode, data);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.frag_invite_frnds, container, false);
		v.findViewById(R.id.bottom).setVisibility(View.VISIBLE);
		list = (ListView)v.findViewById(R.id.list);
		ed = (EText)v.findViewById(R.id.txt);
		invite = (TextB)v.findViewById(R.id.send);
		invite.setOnClickListener(this);
		if(event_id != null)
		{
			 e = C.items.get(event_id); 
			ed.setText("Event name 	: "+e.get_event_name()+"\n"+
						"Event date : "+e.get_event_date()+"\n"+
						"Address    :"+C.clubs.get(e.get_venue_id()).get_venue_address());
		}
		list_adapter = new Invite_Friends_Adapter(getSherlockActivity().getApplicationContext(),list_items,imageLoader,options);
		list.setAdapter(list_adapter);
		

		list.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int posi,
					long arg3) 
			{
				
				
			}
			
		});
		//update(getSherlockActivity());
		
		get_fb_friends();
		
		
		return v;
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		AppEventsLogger.activateApp(this.getSherlockActivity(),getResources().getString(R.string.fb_app_id));
		
		
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
		case R.id.send:
			//C.alert(getSherlockActivity(), "clecked");
			
			ArrayList<WTP_Contact> friends = list_adapter.get_selected();
			
			String ids = "";
			for(int i = 0;i < friends.size();i++)
			{
				GraphUser user = GraphObject.Factory.create(GraphUser.class);
				user.setId(friends.get(i).get_wtp_id());
				tags.add(user);
				//ids += friends.get(i).get_wtp_id()+",";
			}
			publishStory();
		
			
			break;
		}
	}
	
	
	
	
    /////////////////////////////////////////
	private void get_fb_friends()
	{
		Session session = Session.getActiveSession();
		if(session != null)
		{
		if(session.isOpened())
		{
			Request request1 = new Request(session, "me/taggable_friends", null, 
            HttpMethod.GET, new Request.Callback() {
				
				@Override
				public void onCompleted(Response response) {
					// TODO Auto-generated method stub
					
					try {
						JSONArray array = response
						        .getGraphObject()
						        .getInnerJSONObject().getJSONArray("data");
						list_items.clear();
						for(int i = 0;i < array.length();i++)
						{
							String id,name,image;
							JSONObject o = array.getJSONObject(i);
							id = o.getString("id");
							name = o.getString("name");
							image = o.getJSONObject("picture").getJSONObject("data").getString("url");
							list_items.add(new WTP_Contact(id,name,image,false,true));
						}
						Collections.sort(list_items, new WTP_Sorter());
						if(getSherlockActivity().getApplicationContext() != null)
						{
							list_adapter = new Invite_Friends_Adapter(getSherlockActivity().getApplicationContext(),list_items,imageLoader,options);
							list.setAdapter(list_adapter);
						}
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					progress.dismiss();
				}
			});
			request1.executeAsync();
		}
		else
			progress.dismiss();
		
		}
		else 
			progress.dismiss();
		
		
		
	}
	// ////////////////////////////////////////
	protected Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};
		private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
			@Override
			public void onError(FacebookDialog.PendingCall pendingCall,
					Exception error, Bundle data) {
				Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
			}

			@Override
			public void onComplete(FacebookDialog.PendingCall pendingCall,
					Bundle data) {
				Log.d("HelloFacebook", "Success!");
			}
		};
		
		@Override
		public void onSaveInstanceState(Bundle outState) {
			super.onSaveInstanceState(outState);
			outState.putBoolean(PENDING_PUBLISH_KEY, pendingPublishReauthorization);
			uiHelper.onSaveInstanceState(outState);
		}

		private void onSessionStateChange(Session session, SessionState state,
				Exception exception) {
			if (state.isOpened()) {
				if (pendingPublishReauthorization
						&& state.equals(SessionState.OPENED_TOKEN_UPDATED)) {
					pendingPublishReauthorization = false;
					publishStory();
				}
			} else if (state.isClosed()) {
				// shareButton.setVisibility(View.INVISIBLE);
			}
		}

		private void publishStory() {
			Session session = Session.getActiveSession();

			if (session != null) {

				// Check for publish permissions
				List<String> permissions = session.getPermissions();
				if (!isSubsetOf(PERMISSIONS, permissions)) {
					pendingPublishReauthorization = true;
					Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(
							this, PERMISSIONS);
					session.requestNewPublishPermissions(newPermissionsRequest);
					return;
				}
				publishStory(tags);
				
			}

		}

		private boolean isSubsetOf(Collection<String> subset,
				Collection<String> superset) {
			for (String string : subset) {
				if (!superset.contains(string)) {
					return false;
				}
			}
			return true;
		}
		protected void publishStory(List<GraphUser> tags) {
			// Un-comment the line below to turn on debugging of requests
			//Settings.addLoggingBehavior(LoggingBehavior.REQUESTS);
			
			    
			 // Show a progress dialog because the batch request could take a while.
			progress = new MyProgress(this.getSherlockActivity());
			progress.setCancelable(false);
			progress.show();
			    // Create a batch request
			    RequestBatch requestBatch = new RequestBatch();

			    // Request: Object request
			    // --------------------------------------------

			    String url = "https://s3.amazonaws.com/wtp-user-pics/wtp/512.png";
				
			    OpenGraphObject event = OpenGraphObject.Factory.createForPost("unikkapps:event");
			    event.setProperty("title", e.get_event_name());
			    event.setProperty("description", e.get_event_details());
			    event.setProperty("image", url);
			    event.setProperty("url", url);
                event.setProperty("type", "unikkapps:event");
			 // Request: Publish action request
			 // --------------------------------------------
			    OpenGraphAction action = GraphObject.Factory.create(OpenGraphAction.class);
			    action.setType("unikkapps:share"); 
			   // action.setProperty("title", "Rice");
			    action.setProperty("event", event);
			    action.setTags(tags);
			    action.setExplicitlyShared(true);
			 // Set up the action request callback
			 Request.Callback actionCallback = new Request.Callback() {

			     @Override
			     public void onCompleted(Response response) {
			         dismissProgressDialog();
			         FacebookRequestError error = response.getError();
			         if (error != null) {
			             C.alert(getSherlockActivity(), error.getErrorMessage());
			             
			         } else {
			             String actionId = null;
			             try {
			                 JSONObject graphResponse = response
			                 .getGraphObject()
			                 .getInnerJSONObject();
			                 actionId = graphResponse.getString("id");
			                 C.alert(getSherlockActivity(), "Invited successfully");
			             } catch (JSONException e) {
			                 Log.i(TAG,
			                         "JSON error "+ e.getMessage());
			             }
			            
			         }
			         Log.d("response", response.toString());
			         FB_Friends_Fragment.this.getSherlockActivity().finish();
			     }
			 };

			 // Create the publish action request
			 Request actionRequest = Request.newPostOpenGraphActionRequest(Session.getActiveSession(),
			         action, actionCallback);

			 // Add the request to the batch
			 requestBatch.add(actionRequest);
			 requestBatch.executeAsync();
		
		}
		
		protected void dismissProgressDialog() {
			// Dismiss the progress dialog
			if (progress != null) {
				progress.dismiss();
				progress = null;
	        }
		}
 ///////////////////////////////////////////////////
/*	private void open_fb_dialog()
	{
		FacebookDialog.MessageDialogBuilder builder = new FacebookDialog.MessageDialogBuilder(getSherlockActivity())
	    .setLink("https://developers.facebook.com/docs/android/share/")
	    .setName("Message Dialog Tutorial")
	    .setCaption("Build great social apps that engage your friends.")
	    .setPicture("http://i.imgur.com/g3Qc1HN.png")
	    .setDescription("Allow your users to message links from your app using the Android SDK.")
	    .setFragment(this);

	// If the Facebook app is installed and we can present the share dialog
	if (builder.canPresent()) {
		
		FacebookDialog dialog = builder.build();
		
		dialog.present();
	  // Enable button or other UI to initiate launch of the Message Dialog
	}  else {
	  // Disable button or other UI for Message Dialog 
	}
	}
	*/
}
