package com.dpc.wtp.Models;

import java.util.ArrayList;

public class Event {
	
	private String[] status = new String[]{"I am going","May be","Not going"};
	
	private String db_id,guest_list,guest_count,in_guest_list,guest_list_time,e_id,vd_id,e_name,deals,e_details,e_date,s_time,e_time,m_type,going_count,maybe_count,price,dj_lineup,dress_code,rule,cover_charge,url,ticket_url,ticket_price;
	
	private int going_type,type = 1,priority = 100;
	
	private ArrayList<Images> images;

    private ArrayList<Lineup> lineups = new ArrayList<>();

	public Event(String vdid,String eid,int type, String ename,String mtype,String date,String stime,String etime,String djlineup,String dresscode,String entryrule,String ticket_price,String entryprice,String covercharge,String deals,String details,String guestlist,int g_type,String g_count,String maybe_count,String guestcount,String inguestlist,String guestlisttime,String url,ArrayList<Images> images,ArrayList<Lineup> lineups,String ticket_url,int priority)
	{
		this.vd_id = vdid;
		this.e_id = eid;
        this.type = type;
		this.e_name = ename;
		this.m_type = mtype;
		this.e_date = date;
		this.s_time = stime;
		this.e_time = etime;
		this.dj_lineup = djlineup;
		this.dress_code = dresscode;
		this.rule = entryrule;
        this.ticket_price = ticket_price;
		this.price = entryprice;
		this.cover_charge = covercharge;
		this.deals = deals;
		this.e_details = details;
		this.guest_list = guestlist;
		this.guest_count = guestcount;
		this.in_guest_list = inguestlist;
		this.guest_list_time = guestlisttime;
		this.ticket_url = ticket_url;
		this.going_type = g_type;
		this.going_count = g_count;
		this.maybe_count = maybe_count;
		this.url = url;
		this.images = images;
        this.lineups = lineups;
		this.priority = priority;
		
	}

    public String getTicket_price() {
        return ticket_price;
    }

    public void setTicket_price(String ticket_price) {
        this.ticket_price = ticket_price;
    }

    public String getTicket_url() {
        return ticket_url;
    }

    public void setTicket_url(String ticket_url) {
        this.ticket_url = ticket_url;
    }

    public Event(String dbid,String id,int attend_type,int type)
	{
		this.db_id = dbid;
		this.e_id = id;
		this.going_type = attend_type;
		this.type = type;
	}
	public int get_type()
	{
		return type;
	}
	public String get_dbid()
	{
		return this.db_id;
	}
	public Event() {
		// TODO Auto-generated constructor stub
	}

	public void set_goingtype(int type)
	{
		this.going_type = type;
		
	}
	public void set_goingcount(String count)
	{
		this.going_count = count;
	}
	public void set_maybecount(String count)
	{
		this.maybe_count = count;
	}
	public void set_guest_list(String guest_list)
	{
		this.guest_list = guest_list;
	}
	public String get_deals()
	{
		return deals;
	}
	
	public String get_venue_id()
	{
		return vd_id;
	}
		
	public String get_event_id()
	{
		return e_id;
	}
	public String get_event_name()
	{
		return e_name;
	}
	public String get_event_details()
	{
		return e_details;
	}
	public String get_guest_list()
	{
		return guest_list;
	}
	public String get_event_date()
	{
		return e_date;
	}
	
	public String get_event_s_time()
	{
		return s_time;
	}
	
	public String get_event_e_time()
	{
		return e_time;
	}
	
	public String get_event_m_type()
	{
		return m_type;
	}
	
	public String get_event_going_count()
	{
		return going_count;
	}
	
	public String get_event_maybe_count()
	{
		return maybe_count;
	}
	public String get_entry_price()
	{
		return price;
	}
	
	public int get_event_going_type()
	{
		return going_type;
	}
	
	public String get_status_txt()
	{
		if(going_type > 0)
		return status[going_type - 1];
		else
			return "";
	}
	public String get_url()
	{
		return url;
	}
	public ArrayList<Images> get_images()
	{
		return images;
	}
	

	public String get_djlineup()
	{
		return dj_lineup;
	}
	public String get_cover_charge()
	{
		return cover_charge;
	}
	public String get_entry_rule()
	{
		return rule;
	}
	public String get_dress_code()
	{
		return dress_code;
	}
	public int get_priority()
	{
		return priority;
	}
	public void set_guest_count(String count)
	{
		this.going_count = count;
	}
	public String get_guest_count()
	{
		return going_count;
	}
	public boolean is_in_guest_list()
	{
		return Integer.parseInt(in_guest_list) > 0 ? true : false;
	}
	public String get_guest_list_time()
	{
		return this.guest_list_time;
	}
	public String get_guest_id()
	{
		return this.in_guest_list;
	}
	public void set_guest_id(String id)
	{
		this.in_guest_list = id;
	}

    public ArrayList<Lineup> getLineups() {
        return lineups;
    }

    public void setLineups(ArrayList<Lineup> lineups) {
        this.lineups = lineups;
    }
}
