package com.dpc.wtp.services;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.provider.ContactsContract;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;

import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.activities.ActivityChatWindow;
import com.dpc.wtp.activities.Activity_ChatUsers;
import com.dpc.wtp.activities.App;
import com.dpc.wtp.customs.Custom_Send_Message;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.util.StringUtils;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

public class WTP_Service extends Service{

    private Contact_Observer contacts;
    DatabaseHelper db;
    Calendar c;
    public static XMPPConnection connection;
    public static String CURRENT_CHAT_USER;
    ActivityChatWindow myclient;
    private NotificationManager myNotificationManager;

    long unread_conversation_count=0;
    String unread_messages_count=null;
    public static final String BROADCAST_ACTION = "main.project.app.mychatclient";
    Intent intent;
    String servergroupid = null;
    String isgroupmsg=null;
    String chatid = null;



    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        App.set_servicerunning(true);
        contacts = new Contact_Observer(new Handler(), this);
        getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true, contacts);
        Log.d("Service Started", "***************************************************");
        return super.onStartCommand(intent, flags, startId);

    }


    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();

        Log.d("Service Created", "***************************************************");
        db = new DatabaseHelper(getApplicationContext());
        myclient = new ActivityChatWindow();
        c = Calendar.getInstance();
        new ConnectMessagingServer().execute();
    }

    public void create_connection(Context context)
    {
        new ConnectMessagingServer().execute();
    }


    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        App.set_servicerunning(false);
        getContentResolver().unregisterContentObserver(contacts);
    }

    public String get_current_time()
    {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
        return sdf.format(c.getTime());
    }

    public String get_universal_date(String time)
    {
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
        Date date = null;
        try
        {
            //Here you say to java the initial timezone. This is the secret
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = sdf.parse(time);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        //Here you set to your timezone

        sdf.setTimeZone(TimeZone.getDefault());
        time = sdf.format(date);
        return time;
    }

    public String get_local_datetime_from_utc_time(String utc_time)
    {
        Calendar cal = Calendar.getInstance();
        String tz = cal.getTimeZone().getID();
        SimpleDateFormat sdf = null;

        Log.d("Time zone: ", tz);

        try
        {
            sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
            sdf.setTimeZone(TimeZone.getTimeZone(tz));
        }
        catch (Exception e)
        {
            Log.d("UTC TO LOCAL, Exception",e.toString());
        }



        String localTime = sdf.format(utc_time); // I assume your timestamp is in seconds and you're converting to milliseconds?
        Log.d("Time: ", localTime);
        return localTime;
    }

    public  void setConnection()
    {
        if (connection != null)
        {
            // Add a packet listener to get messages sent to us
            PacketFilter filter = new MessageTypeFilter(Message.Type.chat);
            connection.addPacketListener(new PacketListener()
            {
                public void processPacket(Packet packet)
                {
                    final Message message = (Message) packet;
                    db = new DatabaseHelper(getApplicationContext());
                    if (message.getBody() != null)
                    {
                        String localTime = get_current_time();
                        String msgDateTime = get_universal_date(localTime);

                        final String msgfromId = StringUtils.parseBareAddress(message.getFrom());
                        Log.i("XMPPClient", "Got text [" + message.getBody() + "] from [" + msgfromId + "]");
                        final String[] sender = msgfromId.split("@");
                        String msgtype = message.getProperty("messagetype").toString();
                        isgroupmsg = message.getProperty("isgroupmessage").toString();

                        String msgsenttime=null;
                        String chat_subtitle = null;


                        if(!sender[0].equalsIgnoreCase(App.get_userid()))
                        {
                            if (msgtype.equalsIgnoreCase("invite"))
                            {
                                Log.i("Group Name ", message.getProperty("groupname").toString());
                                String name = message.getProperty("groupname").toString();
                                Log.i("Creation Date ", message.getProperty("creationdate").toString());
                                String creationdate = message.getProperty("creationdate").toString();
                               // String creationdate = get_local_datetime_from_utc_time(creation_utc_date);
                                Log.i("Admin ", message.getProperty("admin").toString());
                                String admin = message.getProperty("admin").toString();
                                Log.i("Users ", message.getProperty("groupusers").toString());
                                String groupmembers = message.getProperty("groupusers").toString();
                                Log.i("ServerGroupId ", message.getProperty("servergroupid").toString());
                                servergroupid = message.getProperty("servergroupid").toString();


                                if (!db.check_group_present(servergroupid))
                                {
                                    Log.d("Group does not exist", "WTP");
                                    db.insertGroupDetails(name, creationdate, admin, groupmembers, servergroupid);
                                    long id = db.insertChatDetails(servergroupid, message.getBody(), creationdate, "1", "0"); // change it to id received from server
                                    chatid = String.valueOf(id);
                                    if(id > 0) {
                                        db.insertConversationDetails(chatid, message.getBody(), creationdate, "0", "0", Integer.toString(C.MESSAGE), Integer.toString(C.DOWNLOADED),"", Integer.toString(C.STATUS_RECEIVED)); // change it to id received from server
                                        Log.d("Group Creation Completed", "Success");
                                    }
                                    else
                                        Log.d("error","der s no chat id");
                                }

                            }
                            else if (msgtype.equalsIgnoreCase("message"))
                            {
                                if (isgroupmsg.equalsIgnoreCase("1"))
                                {
                                    Log.i("Group Name ", message.getProperty("groupname").toString());
                                    String name = message.getProperty("groupname").toString();
                                    Log.i("Creation Date ", message.getProperty("creationdate").toString());
                                    String creationdate = message.getProperty("creationdate").toString();
                                   // String creationdate = get_local_datetime_from_utc_time(creation_utc_date);
                                    Log.i("Admin ", message.getProperty("admin").toString());
                                    String admin = message.getProperty("admin").toString();
                                    Log.i("Users ", message.getProperty("groupusers").toString());
                                    String groupmembers = message.getProperty("groupusers").toString();
                                    Log.i("ServerGroupId ", message.getProperty("servergroupid").toString());
                                    servergroupid = message.getProperty("servergroupid").toString();
                                    //String msgsenttime_utc = message.getProperty("messagesenttime").toString();
                                    //msgsenttime = get_local_datetime_from_utc_time(msgsenttime_utc);
                                    msgsenttime = message.getProperty("messagesenttime").toString();

                                    if (db.check_group_present(servergroupid))
                                    {
                                        Log.d("Group already", "WTP");
                                        int unreadcount = db.get_unread_msg_count_with_id(servergroupid);
                                        db.update_chat_lastmsg_time_count(servergroupid, message.getBody(), msgsenttime, Integer.toString(unreadcount + 1));
                                        chatid = db.get_chatid_of_group_by_serverid(servergroupid);
                                        db.insertConversationDetails(chatid, message.getBody(), msgsenttime, "0", "0", Integer.toString(C.MESSAGE), Integer.toString(C.DOWNLOADED),"", Integer.toString(C.STATUS_RECEIVED)); // change it to id received from server
                                    }
                                    else
                                    {
                                        Log.d("Group does not exist", "WTP");
                                        db.insertGroupDetails(name, creationdate, admin, groupmembers, servergroupid);
                                        int unreadcount = 0; // as first time insertion
                                        long id = db.insertChatDetails(servergroupid, message.getBody(), msgsenttime, "1", Integer.toString(unreadcount + 1)); // change it to id received from server
                                        chatid = String.valueOf(id);
                                        db.insertConversationDetails(chatid, message.getBody(), msgsenttime, "0", "0", Integer.toString(C.MESSAGE), Integer.toString(C.DOWNLOADED), "", Integer.toString(C.STATUS_RECEIVED)); // change it to id received from server
                                    }
                                }
                                else
                                {
                                    chatid = null;  // dont delete
                                    String[] prefixname = msgfromId.split("@");
                                    Log.d("My Username", prefixname[0]);


                                    //String msgsenttime_utc = message.getProperty("messagesenttime").toString();
                                   // msgsenttime = get_local_datetime_from_utc_time(msgsenttime_utc);
                                    msgsenttime = message.getProperty("messagesenttime").toString();

                                    Log.d("My msgsenttime", msgsenttime);
                                    String userid = prefixname[0];


                                    if (!db.check_chatId_exist(userid))// if name not present then insert in db
                                    {
                                        Log.d("Entered If", "");

                                        long id = db.insertChatDetails(prefixname[0], message.getBody(), msgsenttime, "0", "1"); //change "0"
                                        chatid = String.valueOf(id);

                                        Log.d("2nd", chatid);
                                    }
                                    else
                                    {
                                        int unreadcount = db.get_unread_msg_count_with_id(prefixname[0]);
                                        db.update_chat_lastmsg_time_count(prefixname[0], message.getBody(), msgsenttime, Integer.toString(unreadcount + 1));
                                    }
                                    Log.d("My id", prefixname[0]);
                                    Log.d("My Body", message.getBody());
                                    Log.d("My Date", localTime);
                                    if (chatid == null) {
                                        chatid = db.get_chatid_of_member_by_userid(userid);
                                    }

                                    db.insertConversationDetails(chatid, message.getBody(), msgsenttime, "0", "0", Integer.toString(C.MESSAGE), Integer.toString(C.DOWNLOADED),"", Integer.toString(C.STATUS_RECEIVED)); // 0-"poster" ,0 - readstatus "unread"
                                    String sendermsgid = message.getProperty("sendersmsgid").toString();
                                    Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),Integer.parseInt(prefixname[0]),"Delivery Acknowledgement",C.DELIVERY_ACKNOWLEDGEMENT);
                                    csm.send_delivery_acknowledgement_message(sendermsgid);
                                    Log.d("Inserted", "h");
                                }
                            } else if (msgtype.equalsIgnoreCase("image")){


                                if (isgroupmsg.equalsIgnoreCase("1"))
                                {
                                    Log.i("Group Name ", message.getProperty("groupname").toString());
                                    String name = message.getProperty("groupname").toString();
                                    Log.i("Creation Date ", message.getProperty("creationdate").toString());
                                    String creationdate = message.getProperty("creationdate").toString();
                                    //String creationdate = get_local_datetime_from_utc_time(creation_utc_date);
                                    Log.i("Admin ", message.getProperty("admin").toString());
                                    String admin = message.getProperty("admin").toString();
                                    Log.i("Users ", message.getProperty("groupusers").toString());
                                    String groupmembers = message.getProperty("groupusers").toString();
                                    Log.i("ServerGroupId ", message.getProperty("servergroupid").toString());
                                    servergroupid = message.getProperty("servergroupid").toString();
                                    //String msgsenttime_utc = message.getProperty("messagesenttime").toString();
                                    //msgsenttime = get_local_datetime_from_utc_time(msgsenttime_utc);
                                    msgsenttime = message.getProperty("messagesenttime").toString();

                                    if (db.check_group_present(servergroupid))
                                    {
                                        Log.d("Image :Group already", "WTP");
                                        int unreadcount = db.get_unread_msg_count_with_id(servergroupid);
                                        db.update_chat_lastmsg_time_count(servergroupid, "Image", msgsenttime, Integer.toString(unreadcount + 1));
                                        chatid = db.get_chatid_of_group_by_serverid(servergroupid);
                                        db.insertConversationDetails(chatid, message.getBody(), msgsenttime, "0", "0", Integer.toString(C.IMAGE), Integer.toString(C.NOT_DOWNLOADED),message.getBody(), Integer.toString(C.STATUS_RECEIVED)); // change it to id received from server
                                    }
                                    else
                                    {
                                        Log.d("Group does not exist", "WTP");
                                        db.insertGroupDetails(name, creationdate, admin, groupmembers, servergroupid);
                                        int unreadcount = 0; // as first time insertion
                                        long id = db.insertChatDetails(servergroupid, "Image", msgsenttime, "1", Integer.toString(unreadcount + 1)); // change it to id received from server
                                        chatid = String.valueOf(id);
                                        db.insertConversationDetails(chatid, message.getBody(), msgsenttime, "0", "0", Integer.toString(C.IMAGE), Integer.toString(C.NOT_DOWNLOADED), message.getBody(), Integer.toString(C.STATUS_RECEIVED)); // change it to id received from server
                                    }
                                }
                                else
                                {
                                    chatid = null;  // dont delete
                                    String[] prefixname = msgfromId.split("@");
                                    Log.d("My Username", prefixname[0]);


                                    //String msgsenttime_utc = message.getProperty("messagesenttime").toString();
                                    //msgsenttime = get_local_datetime_from_utc_time(msgsenttime_utc);
                                    msgsenttime = message.getProperty("messagesenttime").toString();

                                    Log.d("My msgsenttime", msgsenttime);
                                    String userid = prefixname[0];


                                    if (!db.check_chatId_exist(userid))// if name not present then insert in db
                                    {
                                        Log.d("Entered If", "");

                                        long id = db.insertChatDetails(prefixname[0], "Image", msgsenttime, "0", "1"); //change "0"
                                        chatid = String.valueOf(id);
                                        Log.d("2nd", chatid);
                                    }
                                    else
                                    {
                                        int unreadcount = db.get_unread_msg_count_with_id(prefixname[0]);
                                        db.update_chat_lastmsg_time_count(prefixname[0], "Image", msgsenttime, Integer.toString(unreadcount + 1));
                                    }
                                    Log.d("My id", prefixname[0]);
                                    Log.d("My Body", message.getBody());
                                    Log.d("My Date", localTime);
                                    if (chatid == null) {
                                        chatid = db.get_chatid_of_member_by_userid(userid);
                                    }

                                    db.insertConversationDetails(chatid, message.getBody(), msgsenttime, "0", "0", Integer.toString(C.IMAGE), Integer.toString(C.NOT_DOWNLOADED),message.getBody(), Integer.toString(C.STATUS_RECEIVED)); // 0-"poster" ,0 - readstatus "unread"
                                    String sendermsgid = message.getProperty("sendersmsgid").toString();
                                    Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),Integer.parseInt(prefixname[0]),"Delivery Acknowledgement",C.DELIVERY_ACKNOWLEDGEMENT);
                                    csm.send_delivery_acknowledgement_message(sendermsgid);
                                    Log.d("Inserted", "h");
                                }
                            }else if (msgtype.equalsIgnoreCase("audio")){
                                //String audioFilePath = C.saveDownloadAudio(message.getBody());

                                //Log.d("AudioFilePath", audioFilePath);

                                if (isgroupmsg.equalsIgnoreCase("1"))
                                {
                                    Log.i("Group Name ", message.getProperty("groupname").toString());
                                    String name = message.getProperty("groupname").toString();
                                    Log.i("Creation Date ", message.getProperty("creationdate").toString());
                                    String creationdate = message.getProperty("creationdate").toString();
                                    //String creationdate = get_local_datetime_from_utc_time(creation_utc_date);
                                    Log.i("Admin ", message.getProperty("admin").toString());
                                    String admin = message.getProperty("admin").toString();
                                    Log.i("Users ", message.getProperty("groupusers").toString());
                                    String groupmembers = message.getProperty("groupusers").toString();
                                    Log.i("ServerGroupId ", message.getProperty("servergroupid").toString());
                                    servergroupid = message.getProperty("servergroupid").toString();
                                    //String msgsenttime_utc = message.getProperty("messagesenttime").toString();
                                    //msgsenttime = get_local_datetime_from_utc_time(msgsenttime_utc);
                                    msgsenttime = message.getProperty("messagesenttime").toString();

                                    if (db.check_group_present(servergroupid))
                                    {
                                        Log.d("Image :Group already", "WTP");
                                        int unreadcount = db.get_unread_msg_count_with_id(servergroupid);
                                        db.update_chat_lastmsg_time_count(servergroupid, "Audio", msgsenttime, Integer.toString(unreadcount + 1));
                                        chatid = db.get_chatid_of_group_by_serverid(servergroupid);
                                        db.insertConversationDetails(chatid, message.getBody(), msgsenttime, "0", "0", Integer.toString(C.AUDIO), Integer.toString(C.NOT_DOWNLOADED),message.getBody(), Integer.toString(C.STATUS_RECEIVED)); // change it to id received from server
                                    }
                                    else
                                    {
                                        Log.d("Group does not exist", "WTP");
                                        db.insertGroupDetails(name, creationdate, admin, groupmembers, servergroupid);
                                        int unreadcount = 0; // as first time insertion
                                        long id = db.insertChatDetails(servergroupid, "Audio", msgsenttime, "1", Integer.toString(unreadcount + 1)); // change it to id received from server
                                        chatid = String.valueOf(id);
                                        db.insertConversationDetails(chatid, message.getBody(), msgsenttime, "0", "0", Integer.toString(C.AUDIO), Integer.toString(C.NOT_DOWNLOADED), message.getBody(), Integer.toString(C.STATUS_RECEIVED)); // change it to id received from server
                                    }
                                }
                                else
                                {
                                    chatid = null;  // dont delete
                                    String[] prefixname = msgfromId.split("@");
                                    Log.d("My Username", prefixname[0]);


                                    //String msgsenttime_utc = message.getProperty("messagesenttime").toString();
                                    //msgsenttime = get_local_datetime_from_utc_time(msgsenttime_utc);
                                    msgsenttime = message.getProperty("messagesenttime").toString();

                                    Log.d("My msgsenttime", msgsenttime);
                                    String userid = prefixname[0];


                                    if (!db.check_chatId_exist(userid))// if name not present then insert in db
                                    {
                                        Log.d("Entered If", "");

                                        long id = db.insertChatDetails(prefixname[0], "Audio", msgsenttime, "0", "1"); //change "0"
                                        chatid = String.valueOf(id);
                                        Log.d("2nd", chatid);
                                    }
                                    else
                                    {
                                        int unreadcount = db.get_unread_msg_count_with_id(prefixname[0]);
                                        db.update_chat_lastmsg_time_count(prefixname[0], "Audio", msgsenttime, Integer.toString(unreadcount + 1));
                                    }
                                    Log.d("My id", prefixname[0]);
                                    Log.d("My Body", message.getBody());
                                    Log.d("My Date", localTime);
                                    if (chatid == null) {
                                        chatid = db.get_chatid_of_member_by_userid(userid);
                                    }

                                    db.insertConversationDetails(chatid, message.getBody(), msgsenttime, "0", "0", Integer.toString(C.AUDIO), Integer.toString(C.NOT_DOWNLOADED),message.getBody(), Integer.toString(C.STATUS_RECEIVED)); // 0-"poster" ,0 - readstatus "unread"
                                    String sendermsgid = message.getProperty("sendersmsgid").toString();
                                    Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),Integer.parseInt(prefixname[0]),"Delivery Acknowledgement",C.DELIVERY_ACKNOWLEDGEMENT);
                                    csm.send_delivery_acknowledgement_message(sendermsgid);
                                    Log.d("Inserted", "h");
                                }
                            }
                            else if (msgtype.equalsIgnoreCase("video")){

                                // String videoFilePath = C.DownloadVideoFromUrl(message.getBody());



                                if (isgroupmsg.equalsIgnoreCase("1"))
                                {
                                    Log.i("Group Name ", message.getProperty("groupname").toString());
                                    String name = message.getProperty("groupname").toString();
                                    Log.i("Creation Date ", message.getProperty("creationdate").toString());
                                    String creationdate = message.getProperty("creationdate").toString();
                                    //String creationdate = get_local_datetime_from_utc_time(creation_utc_date);
                                    Log.i("Admin ", message.getProperty("admin").toString());
                                    String admin = message.getProperty("admin").toString();
                                    Log.i("Users ", message.getProperty("groupusers").toString());
                                    String groupmembers = message.getProperty("groupusers").toString();
                                    Log.i("ServerGroupId ", message.getProperty("servergroupid").toString());
                                    servergroupid = message.getProperty("servergroupid").toString();
                                    //String msgsenttime_utc = message.getProperty("messagesenttime").toString();
                                   // msgsenttime = get_local_datetime_from_utc_time(msgsenttime_utc);
                                    msgsenttime = message.getProperty("messagesenttime").toString();

                                    if (db.check_group_present(servergroupid))
                                    {
                                        Log.d("Image :Group already", "WTP");
                                        int unreadcount = db.get_unread_msg_count_with_id(servergroupid);
                                        db.update_chat_lastmsg_time_count(servergroupid, "Video", msgsenttime, Integer.toString(unreadcount + 1));
                                        chatid = db.get_chatid_of_group_by_serverid(servergroupid);
                                        db.insertConversationDetails(chatid, message.getBody(), msgsenttime, "0", "0", Integer.toString(C.VIDEO), Integer.toString(C.NOT_DOWNLOADED),message.getBody(), Integer.toString(C.STATUS_RECEIVED)); // change it to id received from server
                                    }
                                    else
                                    {
                                        Log.d("Group does not exist", "WTP");
                                        db.insertGroupDetails(name, creationdate, admin, groupmembers, servergroupid);
                                        int unreadcount = 0; // as first time insertion
                                        long id = db.insertChatDetails(servergroupid, "Video", msgsenttime, "1", Integer.toString(unreadcount + 1)); // change it to id received from server
                                        chatid = String.valueOf(id);
                                        db.insertConversationDetails(chatid, message.getBody(), msgsenttime, "0", "0", Integer.toString(C.VIDEO), Integer.toString(C.NOT_DOWNLOADED), message.getBody(), Integer.toString(C.STATUS_RECEIVED)); // change it to id received from server
                                    }
                                }
                                else
                                {
                                    chatid = null;  // dont delete
                                    String[] prefixname = msgfromId.split("@");
                                    Log.d("My Username", prefixname[0]);


                                    //String msgsenttime_utc = message.getProperty("messagesenttime").toString();
                                    //msgsenttime = get_local_datetime_from_utc_time(msgsenttime_utc);
                                    msgsenttime = message.getProperty("messagesenttime").toString();

                                    Log.d("My msgsenttime", msgsenttime);
                                    String userid = prefixname[0];


                                    if (!db.check_chatId_exist(userid))// if name not present then insert in db
                                    {
                                        Log.d("Entered If", "");

                                        long id = db.insertChatDetails(prefixname[0], "Video", msgsenttime, "0", "1"); //change "0"
                                        chatid = String.valueOf(id);
                                        Log.d("2nd", chatid);
                                    }
                                    else
                                    {
                                        int unreadcount = db.get_unread_msg_count_with_id(prefixname[0]);
                                        db.update_chat_lastmsg_time_count(prefixname[0], "Video", msgsenttime, Integer.toString(unreadcount + 1));
                                    }
                                    Log.d("My id", prefixname[0]);
                                    Log.d("My Body", message.getBody());
                                    Log.d("My Date", localTime);
                                    if (chatid == null) {
                                        chatid = db.get_chatid_of_member_by_userid(userid);
                                    }

                                    db.insertConversationDetails(chatid, message.getBody(), msgsenttime, "0", "0", Integer.toString(C.VIDEO), Integer.toString(C.NOT_DOWNLOADED),message.getBody(), Integer.toString(C.STATUS_RECEIVED)); // 0-"poster" ,0 - readstatus "unread"
                                    String sendermsgid = message.getProperty("sendersmsgid").toString();
                                    Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),Integer.parseInt(prefixname[0]),"Delivery Acknowledgement",C.DELIVERY_ACKNOWLEDGEMENT);
                                    csm.send_delivery_acknowledgement_message(sendermsgid);
                                    Log.d("Inserted", "h");
                                }
                            }else if (msgtype.equalsIgnoreCase("group_name_update")) {
                                String groupname = message.getProperty("groupname").toString();
                                servergroupid = message.getProperty("servergroupid").toString();
                                String creationdate = message.getProperty("creationdate").toString();
                               // String creationdate = get_local_datetime_from_utc_time(creation_utc_date);
                                String admin = message.getProperty("admin").toString();
                                String groupmembers = message.getProperty("groupusers").toString();
                                //String msgsenttime_utc = message.getProperty("messagesenttime").toString();
                                //msgsenttime = get_local_datetime_from_utc_time(msgsenttime_utc);
                                msgsenttime = message.getProperty("messagesenttime").toString();
                                if (db.check_group_present(servergroupid)) // if group present
                                {
                                    db.update_group_name(groupname, servergroupid);
                                } else {
                                    db.insertGroupDetails(groupname, creationdate, admin, groupmembers, servergroupid);
                                    int unreadcount = 0; // as first time insertion
                                    long id = db.insertChatDetails(servergroupid, "", msgsenttime, "1", Integer.toString(unreadcount + 1)); // blank message as group now created & its a update
                                }

                            } else if (msgtype.equalsIgnoreCase("group_members_update")) {
                                String groupname = message.getProperty("groupname").toString();
                                String groupusers = message.getProperty("groupusers").toString();
                                servergroupid = message.getProperty("servergroupid").toString();
                                String creationdate = message.getProperty("creationdate").toString();
                                //String creationdate = get_local_datetime_from_utc_time(creation_utc_date);
                                String admin = message.getProperty("admin").toString();
                                String groupmembers = message.getProperty("groupusers").toString();
                                //String msgsenttime_utc = message.getProperty("messagesenttime").toString();
                                //msgsenttime = get_local_datetime_from_utc_time(msgsenttime_utc);

                                String[] memberlist = groupusers.split(", "); // don't remove space else it only 1 name checked
                                List<String> memberarraylist = new LinkedList(Arrays.asList(memberlist));

                                if(!memberarraylist.contains(App.get_userid()))
                                {
                                    db.delete_group_details(servergroupid);
                                    db.delete_user_complete_conversation(servergroupid);
                                }
                                else
                                {
                                    msgsenttime = message.getProperty("messagesenttime").toString();
                                    if (db.check_group_present(servergroupid)) // if group present
                                    {
                                        db.update_group_users(groupusers, servergroupid,admin);
                                    } else {
                                        db.insertGroupDetails(groupname, creationdate, admin, groupmembers, servergroupid);
                                        int unreadcount = 0; // as first time insertion
                                        long id = db.insertChatDetails(servergroupid, "", msgsenttime, "1", Integer.toString(unreadcount)); // blank message as group now created & its a update
                                    }
                                }
                            }

                            else if (msgtype.equalsIgnoreCase("sticker")){


                                if (isgroupmsg.equalsIgnoreCase("1"))
                                {
                                    Log.i("Group Name ", message.getProperty("groupname").toString());
                                    String name = message.getProperty("groupname").toString();
                                    Log.i("Creation Date ", message.getProperty("creationdate").toString());
                                    String creationdate = message.getProperty("creationdate").toString();
                                    //String creationdate = get_local_datetime_from_utc_time(creation_utc_date);
                                    Log.i("Admin ", message.getProperty("admin").toString());
                                    String admin = message.getProperty("admin").toString();
                                    Log.i("Users ", message.getProperty("groupusers").toString());
                                    String groupmembers = message.getProperty("groupusers").toString();
                                    Log.i("ServerGroupId ", message.getProperty("servergroupid").toString());
                                    servergroupid = message.getProperty("servergroupid").toString();
                                    //String msgsenttime_utc = message.getProperty("messagesenttime").toString();
                                    //msgsenttime = get_local_datetime_from_utc_time(msgsenttime_utc);
                                    msgsenttime = message.getProperty("messagesenttime").toString();

                                    if (db.check_group_present(servergroupid))
                                    {
                                        Log.d("Sticker :Group already", "WTP");
                                        int unreadcount = db.get_unread_msg_count_with_id(servergroupid);
                                        db.update_chat_lastmsg_time_count(servergroupid, "Sticker", msgsenttime, Integer.toString(unreadcount + 1));
                                        chatid = db.get_chatid_of_group_by_serverid(servergroupid);
                                        db.insertConversationDetails(chatid, message.getBody(), msgsenttime, "0", "0", Integer.toString(C.STICKER), Integer.toString(C.DOWNLOADED),"", Integer.toString(C.STATUS_RECEIVED)); // change it to id received from server
                                    }
                                    else
                                    {
                                        Log.d("Group does not exist", "WTP");
                                        db.insertGroupDetails(name, creationdate, admin, groupmembers, servergroupid);
                                        int unreadcount = 0; // as first time insertion
                                        long id = db.insertChatDetails(servergroupid, "Sticker", msgsenttime, "1", Integer.toString(unreadcount + 1)); // change it to id received from server
                                        chatid = String.valueOf(id);
                                        db.insertConversationDetails(chatid, message.getBody(), msgsenttime, "0", "0", Integer.toString(C.STICKER), Integer.toString(C.DOWNLOADED), "", Integer.toString(C.STATUS_RECEIVED)); // change it to id received from server
                                    }
                                }
                                else
                                {
                                    chatid = null;  // dont delete
                                    String[] prefixname = msgfromId.split("@");
                                    Log.d("My Username", prefixname[0]);


                                    //String msgsenttime_utc = message.getProperty("messagesenttime").toString();
                                    //msgsenttime = get_local_datetime_from_utc_time(msgsenttime_utc);
                                    msgsenttime = message.getProperty("messagesenttime").toString();

                                    Log.d("My msgsenttime", msgsenttime);
                                    String userid = prefixname[0];


                                    if (!db.check_chatId_exist(userid))// if name not present then insert in db
                                    {
                                        Log.d("Entered If", "");

                                        long id = db.insertChatDetails(prefixname[0], "Sticker", msgsenttime, "0", "1"); //change "0"
                                        chatid = String.valueOf(id);
                                        Log.d("2nd", chatid);
                                    }
                                    else
                                    {
                                        int unreadcount = db.get_unread_msg_count_with_id(prefixname[0]);
                                        db.update_chat_lastmsg_time_count(prefixname[0], "Sticker", msgsenttime, Integer.toString(unreadcount + 1));
                                    }
                                    Log.d("My id", prefixname[0]);
                                    Log.d("My Body", message.getBody());
                                    Log.d("My Date", localTime);
                                    if (chatid == null) {
                                        chatid = db.get_chatid_of_member_by_userid(userid);
                                    }

                                    db.insertConversationDetails(chatid, message.getBody(), msgsenttime, "0", "0", Integer.toString(C.STICKER), Integer.toString(C.DOWNLOADED),"", Integer.toString(C.STATUS_RECEIVED)); // 0-"poster" ,0 - readstatus "unread"
                                    String sendermsgid = message.getProperty("sendersmsgid").toString();
                                    Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),Integer.parseInt(prefixname[0]),"Delivery Acknowledgement",C.DELIVERY_ACKNOWLEDGEMENT);
                                    csm.send_delivery_acknowledgement_message(sendermsgid);
                                    Log.d("Inserted", "h");
                                }
                            }
                            else if (msgtype.equalsIgnoreCase("delivery_acknowledgement")) {
                                String updatemsgid = message.getProperty("sendersmsgid").toString();
                                db.update_msg_status_to_delivered(updatemsgid);
                            }
                            else if (msgtype.equalsIgnoreCase("online_status_request")) {
                                //check your online status and send
                                String[] prefixname = msgfromId.split("@");
                                chat_subtitle = "online";
                                Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),Integer.parseInt(prefixname[0]),"Reply for online request",C.ONLINE_STATUS_REPLY);
                                if(C.MY_ONLINE_STATUS == C.ME_ONLINE)
                                {
                                    csm.response_for_online_status_to_single_user("online");
                                }
                                else if(C.MY_ONLINE_STATUS == C.ME_OFFLINE)
                                {
                                    csm.response_for_online_status_to_single_user("offline");
                                }
                            }
                            else if (msgtype.equalsIgnoreCase("online_status_response")) {


                                chat_subtitle = message.getProperty("onlinestatus").toString();
                                String[] prefixname = msgfromId.split("@");
                                if(CURRENT_CHAT_USER.equalsIgnoreCase(prefixname[0]))
                                {
                                    if(chat_subtitle.equalsIgnoreCase("offline"))
                                    {
                                        chat_subtitle="";
                                    }

                                }

                            }
                            else if (msgtype.equalsIgnoreCase("typing_status")) {
                                String typingmsg = message.getBody();
                                String isgroupmsg = message.getProperty("isgroupmessage").toString();
                                String servergroupid = null;
                                chat_subtitle = "online";
                                if(Integer.parseInt(isgroupmsg)==C.ISGROUP)
                                {
                                    servergroupid = message.getProperty("servergroupid").toString();
                                    if(CURRENT_CHAT_USER.equalsIgnoreCase(servergroupid))
                                    {
                                        chat_subtitle = message.getBody();
                                    }
                                }
                                else
                                {
                                    String[] prefixname = msgfromId.split("@");
                                    if(CURRENT_CHAT_USER.equalsIgnoreCase(prefixname[0]))
                                    {
                                        chat_subtitle = message.getBody();
                                    }
                                }

                            }
                        }


                        intent = new Intent(BROADCAST_ACTION);
                        intent.putExtra("chat_subtitle", chat_subtitle);

                        sendBroadcast(intent);
                        GetUnreadConversationCount();


                        if(!CURRENT_CHAT_USER.equalsIgnoreCase(msgfromId)|| CURRENT_CHAT_USER.equalsIgnoreCase("@hellochat"))
                        {
                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {

                                   Toast.makeText(getApplicationContext(), message.getBody(), Toast.LENGTH_LONG).show();
                                    Log.d("Displaying notification", "Sender Id:"+msgfromId+" IsgroupMsg:"+isgroupmsg+" ChatId:"+chatid);
                                    if(isgroupmsg.equalsIgnoreCase("1")) {
                                        if (!sender[0].equalsIgnoreCase(App.get_userid())) {
                                            displayNotification(servergroupid, isgroupmsg, chatid);
                                        }
                                    }
                                    else
                                    {
                                        if(!sender[0].equalsIgnoreCase(App.get_userid()))
                                        {
                                            displayNotification(msgfromId,isgroupmsg, chatid);
                                        }

                                    }

                                }
                            });
                        }
                    }
                }
            }, filter);


            Roster roster = connection.getRoster();
            Presence availability = roster.getPresence(App.get_userid()+"@hellochat");
            String status = availability.getType().toString();


            if(status.toString().equalsIgnoreCase("available"))
            {
                C.MY_CHAT_STATUS = C.ME_ONLINE;
            }
            else
            {
                C.MY_CHAT_STATUS = C.ME_OFFLINE;
            }


            //Code for Roaster Listener

            roster = connection.getRoster();
            //Get all rosters
            roster.setSubscriptionMode(Roster.SubscriptionMode.accept_all);
            Collection<RosterEntry> entries = roster.getEntries();
            //loop through
            for (RosterEntry entry : entries) {
            //example: get presence, type, mode, status
                Presence entryPresence = roster.getPresence(entry.getUser());
                Presence.Type userType = entryPresence.getType();
                Presence.Mode mode = entryPresence.getMode();
                status = entryPresence.getStatus();


            }

            roster.addRosterListener(new RosterListener() {
                @Override
                public void presenceChanged(Presence presence) {
                    //Called when the presence of a roster entry is changed
                }
                @Override
                public void entriesUpdated(Collection<String> arg0) {
                    // Called when a roster entries are updated.

                }
                @Override
                public void entriesDeleted(Collection<String> arg0) {
                    // Called when a roster entries are removed.
                }
                @Override
                public void entriesAdded(Collection<String> arg0) {
                    // Called when a roster entries are added.
                }
            });




        }
    }


    private void parse_msg(Packet packet)
    {

    }
    private void GetUnreadConversationCount()
    {
        db = new DatabaseHelper(getApplicationContext());
        unread_conversation_count = db.get_unread_conversation_count();
        unread_messages_count = db.get_unread_messages_count();
    }



    protected void displayNotification(String msgfromId, String isgroupmsg, String chatid)
    {
        Intent resultIntent;
        int notificationIdOne = 111;
        // Invoking the default notification service
        NotificationCompat.Builder  mBuilder = new NotificationCompat.Builder(getApplicationContext());


        if(unread_conversation_count==1)
        {
            mBuilder.setContentTitle(unread_messages_count+" messages from "+msgfromId);
            resultIntent = new Intent(getApplicationContext(), ActivityChatWindow.class);
            resultIntent.putExtra("userid", msgfromId);
            resultIntent.putExtra("chatid", chatid);
            resultIntent.putExtra("isgroup", isgroupmsg);
            resultIntent.putExtra("unreadcount", unread_messages_count);
        }
        else
        {
            mBuilder.setContentTitle(unread_messages_count+" messages from "+unread_conversation_count+" conversations");
            resultIntent = new Intent(getApplicationContext(), Activity_ChatUsers.class);
        }
        mBuilder.setContentText("");
        mBuilder.setTicker("New Message received from "+ msgfromId);
        mBuilder.setSmallIcon(R.drawable.notification);
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.notification);
        mBuilder.setLargeIcon(largeIcon);
        mBuilder.setAutoCancel(true);
        // Increase notification number every time a new notification arrives
        //mBuilder.setNumber(++numMessagesOne);

        //This ensures that navigating backward from the Activity leads out of the app to Home page
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
        // Adds the back stack for the Intent
        stackBuilder.addParentStack(ActivityChatWindow.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_ONE_SHOT
                );

        mBuilder.setContentIntent(resultPendingIntent);
        myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        myNotificationManager.notify(notificationIdOne, mBuilder.build());
    }




    private class ConnectMessagingServer extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            ConnectionConfiguration connConfig =
                    new ConnectionConfiguration("192.168.100.108", 5222, "hellochat");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                connConfig.setTruststoreType("AndroidCAStore");
                connConfig.setTruststorePassword(null);
                connConfig.setTruststorePath(null);
            } else {
                connConfig.setTruststoreType("BKS");
                String path = System.getProperty("javax.net.ssl.trustStore");
                if (path == null)
                    path = System.getProperty("java.home") + File.separator + "etc"
                            + File.separator + "security" + File.separator
                            + "cacerts.bks";
                connConfig.setTruststorePath(path);
            }

            if(connection != null) {

                connection.disconnect();
            }
            connection = new XMPPConnection(connConfig);

            try {

                connection.connect();
                Log.i("ChatMsgReceiverService", "[SettingsDialog] Connected to " + connection.getHost());
            } catch (XMPPException ex) {
                Log.e("ChatMsgReceiverService", "[SettingsDialog] Failed to connect to " + connection.getHost());
                Log.e("ChatMsgReceiverService", ex.toString());
            }
            try {
                connection.login("2", "hello2");
                Log.i("ChatMsgReceiverService", "Loggeddd in as " + connection.getUser());
                App.deleteUser();
                App.saveUser(connection.getUser());

                // Set the status to available
                Presence presence = new Presence(Presence.Type.available);
                connection.sendPacket(presence);
                setConnection();
                //Presence response = new Presence(Presence.Type.subscribed);
                //response.setTo("8@hellochat");
                //connection.sendPacket(response);
                /*for(int i = 1;i<=8;i++)
                {
                    Presence response = new Presence(Presence.Type.subscribed);
                    response.setTo(i+"@hellochat");
                    connection.sendPacket(response);
                }*/
                //Presence presence = new Presence(Presence.Type.available);
                //presence.setMode(Presence.Mode.available);

               // presence.setStatus("I'm all ears");
                //connection.sendPacket(presence);


                //Presence presence = new Presence(Presence.Mode.chat);
                //connection.sendPacket(presence);
                //setConnection();


            } catch (XMPPException ex) {
                Log.e("ChatMsgReceiverService", ex.toString());
            }

            return null;

        }

        @Override
        protected void onPostExecute(String result) {

        }

    }




}
