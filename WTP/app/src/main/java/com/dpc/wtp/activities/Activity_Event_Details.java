package com.dpc.wtp.activities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.dpc.wtp.JSONParser;
import com.dpc.wtp.Models.Lineup;
import com.dpc.wtp.Models.V_Details;
import com.dpc.wtp.R;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Models.Server_Params;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.Utils.TextB;
import com.dpc.wtp.Utils.TwitterUtil;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.customs.MyProgress;
import com.dpc.wtp.customs.Twitter_Login;
import com.dpc.wtp.fragments.WTP_Friends_Fragment;
import com.dpc.wtp.tasks.Add_Guest;
import com.dpc.wtp.tasks.Display_Image;
import com.dpc.wtp.tasks.Twitter_Share;
import com.dpc.wtp.tasks.Update_FB_TWIT;
import com.facebook.AppEventsLogger;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.google.android.gms.analytics.HitBuilders;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

public class Activity_Event_Details extends MyActivity implements
		OnClickListener, OnGestureListener {

	private LinearLayout.LayoutParams wrap = new LinearLayout.LayoutParams(
			LinearLayout.LayoutParams.MATCH_PARENT,
			LinearLayout.LayoutParams.WRAP_CONTENT);

   private LinearLayout.LayoutParams mini;

    private LinearLayout call,invite,ll_lineup;

    private boolean from_notification = false;

	private TextB date, name, add_guestlist, add_guestlist1, buy_ticket,
			cab,buy;

	private Text  address, time, favourite_txt,
			description, event_rules, dj_lineup;// read_more,going_count,maybe_count;

	private ImageView event_image, map,  fb, twitter;

    private View divide;

	//private ImageView invite;// ,rate,view_fb;

	private ScrollView scroll;

	private GestureDetector gesture;

	private String event_id;

	private Event event = null;

	private boolean toggle = false;

	private Intent i;

	private Add_Guest add_guest;

	private Net_Detect net;

	public Activity_Event_Details() {
		options = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.default_bg)
				.showImageForEmptyUri(R.drawable.default_bg)
				.showImageOnFail(R.drawable.default_bg).cacheInMemory(true)
				.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565).build();
	}

	private void init_net_details() {
		net = new Net_Detect(this);

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// gesture.onTouchEvent(event);
		return super.onTouchEvent(event);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.actionbar);
		final Text title = (Text) findViewById(android.R.id.title);
		title.setText(R.string.ac_eventdetails);
		super.onCreate(savedInstanceState);
        app_tracker.setScreenName(C.SCREEN.SCREEN_EVENT_DETAILS.get_screen());
        app_tracker.send(new HitBuilders.AppViewBuilder().build());
        mini = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, (int)getResources().getDimension(R.dimen.button_height));
		uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);
        if (savedInstanceState != null) 
		{
		    pendingPublishReauthorization = 
		        savedInstanceState.getBoolean(PENDING_PUBLISH_KEY, false);
		}
		i = this.getIntent();
		if (i.hasExtra(PARAMS.EVENT_ID.get_param())) {

			event_id = i.getStringExtra(PARAMS.EVENT_ID.get_param());
			if (event_id == null) {
				finish();
			}

		}

		init_net_details();
		db = new DatabaseHelper(this);
		//initControl(this.getIntent());
		setContentView(R.layout.ac_event_details);
        new C().blur_it(this);
		gesture = new GestureDetector(this, this);

		set_layout();


    }

	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);
        from_notification = true;
        initControl(intent);
		if (intent.hasExtra(PARAMS.EVENT_ID.get_param())) {

			event_id = intent.getStringExtra(PARAMS.EVENT_ID.get_param());
			if (event_id == null) {
				finish();
			}
			event = C.items.get(event_id);
			update_views();
			Log.d("event id", event_id);

		}
	}

	private void initControl(Intent i) {
		Uri uri = i.getData();
        if (uri != null)
        Log.d("twitter details",uri.toString());
		if (uri != null
				&& uri.toString().startsWith(C.TWITTER_CALLBACK_URL_EVENT)) {

			String verifier = uri
					.getQueryParameter(C.URL_PARAMETER_TWITTER_OAUTH_VERIFIER);
			new TwitterGetAccessTokenTask().execute(verifier);
		}
		 else
		 new TwitterGetAccessTokenTask().execute("");
	}

	@SuppressLint("NewApi")
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		AppEventsLogger.deactivateApp(this,getResources().getString(R.string.fb_app_id));
		
		unregisterReceiver(NewMessageReceiver);
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;

    }

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
		AppEventsLogger.activateApp(this,getResources().getString(R.string.fb_app_id));
		
		registerReceiver(NewMessageReceiver, new IntentFilter(C.BROADCAST_MSG));

		update_views();

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

	}

	

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		go_back();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getSupportMenuInflater().inflate(R.menu.share_menu, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {

		case R.id.share:
            String content = "Date : "+event.get_event_date();
            content += "\nVenue : "+C.clubs.get(event.get_venue_id()).get_venue_address();
            content += "\n "+event.get_event_details();
			share("Event : " + event.get_event_name(), content);
			break;
		case android.R.id.home:

			go_back();

			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void go_back() {
        if(from_notification) {
            Intent i = new Intent(this, Activity_Home.class);
            i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(i);
        }
        else
            super.onBackPressed();

	}

	public void share(String subject, String content) {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("text/plain");
		i.putExtra(Intent.EXTRA_SUBJECT, subject);
		i.putExtra(Intent.EXTRA_TEXT, content);
		startActivity(Intent.createChooser(i, "Share via"));
	}

	private void set_layout() {

		scroll = (ScrollView) findViewById(R.id.scroll);
		scroll.setSmoothScrollingEnabled(true);
		scroll.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				gesture.onTouchEvent(event);
				if (event.getAction() == MotionEvent.ACTION_UP
						|| event.getAction() == MotionEvent.ACTION_SCROLL) {
					check_guest_button();

				}
				return false;
			}

		});

        call = (LinearLayout) findViewById(R.id.call);
        ll_lineup = (LinearLayout) findViewById(R.id.ll_lineup);

		date = (TextB) findViewById(R.id.e_date);
		
		//artist = (Text) findViewById(R.id.e_artist);
		//genre = (Text) findViewById(R.id.e_genre);
		name = (TextB) findViewById(R.id.e_name);
		address = (Text) findViewById(R.id.e_address);
		time = (Text) findViewById(R.id.e_time);
		favourite_txt = (Text) findViewById(R.id.favorite_txt);
		description = (Text) findViewById(R.id.description);
		event_rules = (Text) findViewById(R.id.rules);
		dj_lineup = (Text) findViewById(R.id.dj_lineup);
		cab = (TextB) findViewById(R.id.cab);
		add_guestlist = (TextB) findViewById(R.id.add_guestlist);
		add_guestlist1 = (TextB) findViewById(R.id.add_guestlist1);
		buy_ticket = (TextB) findViewById(R.id.buy_ticket);
        buy = (TextB) findViewById(R.id.buy);
        divide = (View) findViewById(R.id.divide);

		event_image = (ImageView) findViewById(R.id.event_image);
		map = (ImageView) findViewById(R.id.e_map);
		//favorite = (ImageView) findViewById(R.id.favorite);
		fb = (ImageView) findViewById(R.id.fb);
		twitter = (ImageView) findViewById(R.id.twitter);

        call.setOnClickListener(this);
		map.setOnClickListener(this);
		//favorite.setOnClickListener(this);
		fb.setOnClickListener(this);
		twitter.setOnClickListener(this);
		add_guestlist.setOnClickListener(this);
		add_guestlist1.setOnClickListener(this);
		buy.setOnClickListener(this);
		cab.setOnClickListener(this);
		description.setOnClickListener(this);

		invite = (LinearLayout) findViewById(R.id.invite);
		invite.setOnClickListener(this);

	}


	private void update_views() {

		if (event == null)
			event = C.items.get(event_id);
		if (event != null) {
		//	artist.setText(event.get_artist().get_name());
		//	genre.setText(event.get_artist().get_genre_type());
			if (event.get_images().size() > 0) {
				if (db.is_image_available(event.get_images().get(0).get_id())) {
					new Display_Image(this, event.get_images().get(0).get_id(),
							event_image).execute();

				} else
					imageLoader.displayImage(event.get_images().get(0)
							.get_image(), event_image, options, null);
			} else
				event_image.setBackgroundResource(R.drawable.default_bg);
			String[] d = C.get_date_as_day_month(event.get_event_date());
			// date.setText(Html.fromHtml(d[1]+"<br>"+d[0]+"<sup><font size='6'>"+C.get_date_suffix(d[0].toUpperCase())+"</font></sup>"));
			date.setText(d[1] + "\n" + d[0]);
            String lineup = "";
            if(event.getLineups().size() > 0) {
                for (int i = 0; i < event.getLineups().size(); i++) {
                    Lineup lineup1 = event.getLineups().get(i);
                    lineup += lineup1.getName() + " " + lineup1.getS_time() + " - " + lineup1.getE_time() + "\n";
                }
                dj_lineup.setText(lineup);
                ll_lineup.setVisibility(View.VISIBLE);
            }


			name.setText(event.get_event_name());
			address.setText(C.clubs.get(event.get_venue_id()).get_venue_address());
			time.setText(event.get_event_s_time() + " - "
					+ event.get_event_e_time());
			description.setText(event.get_event_details());

			if (event.get_type() == 1) {

                String rule = "",entry_price = "",ticket_price = "";
                if(event.get_entry_rule() != null)
                    rule += "Entry rule : "+event.get_entry_rule() + "\n";
                if(event.get_dress_code() != null)
                    rule += "Dress code : "+event.get_dress_code();

                event_rules.setText(rule);

                if(event.get_entry_price() != null)
                {
                    ticket_price += "Entry Price : "+ event.get_entry_price()+"\n"+"( Cover charge : "+event.get_cover_charge()+")";
                }

                if(event.getTicket_price() != null)
                {
                    ticket_price += "\n" + "Ticket Price : "+ event.get_entry_price();
                }
				//buy_ticket.setText("Entry Price : "+ event.get_entry_price()+"\n"+"( Cover charge : "+event.get_cover_charge()+")");
			//	event_rules.setText("Entry rule : "+event.get_entry_rule() + "\n"
				//		+ "Dress code : "+event.get_dress_code());

                buy_ticket.setText(ticket_price);

				add_guestlist.setText("Add to Guestlist\n"
						+ event.get_guest_list_time());
				add_guestlist1.setText("Add to Guestlist\n"
						+ event.get_guest_list_time());

				// JSONObject o = new JSONObject();
				if (Integer.parseInt(event.get_guest_list()) == 0) {
					//add_guestlist.setEnabled(false);
					//add_guestlist1.setEnabled(false);
                    add_guestlist.setVisibility(View.GONE);
                    add_guestlist1.setVisibility(View.GONE);
				}
				if (event.is_in_guest_list()) 
				{
					add_guestlist.setText("Guestlist is Pending for approval");
				//	add_guestlist.setBackgroundColor(getResources().getColor(
				//			R.color.tabs_selected));
					add_guestlist.setEnabled(false);
					add_guestlist1.setVisibility(View.GONE);
					// add_guestlist1.setText("You are in guest list");
					// add_guestlist1.setBackgroundColor(getResources().getColor(R.color.tabs_selected));
					// add_guestlist1.setEnabled(false);
				}

			} else {
				event_rules.setVisibility(View.GONE);
				add_guestlist.setVisibility(View.GONE);
				add_guestlist1.setVisibility(View.GONE);
				buy_ticket.setText("Ticket Price : "
						+ event.get_entry_price()
								);
			}

			//set_favorite_icon();

		} else {
			new Get_Events().execute(new Server_Params()
					.get_event_details_params(App.get_userid(), event_id));

		}
        if(event.getTicket_url() != null && !(event.getTicket_url().length() > 1))
        {
            buy.setVisibility(View.VISIBLE);
            divide.setVisibility(View.VISIBLE);
        }
	}



	@SuppressLint("InlinedApi")
	@Override
	public void onClick(View v) {
		Intent i = null;
		// TODO Auto-generated method stub
		switch (v.getId()) {
        case R.id.call:
            if(C.clubs.get(event.get_venue_id()).get_venue_contactinfo() != null || !C.clubs.get(event.get_venue_id()).get_venue_contactinfo().equals("null"))
            {
                String nos = C.clubs.get(event.get_venue_id()).get_venue_contactinfo().split(",")[0];
                C.open_dialer(getApplicationContext(), nos);
                app_tracker.send(new C().generate_analytics(C.CATEGORY.EVENTS.get_category(),C.ACTION.ACTION_CALL.get_action(),C.clubs.get(event.get_venue_id()).get_venue_name()));

            }
            break;
		case R.id.description:
		    C.expand(this,description,toggle);
			toggle = !toggle;
			break;

		case R.id.e_map:
			C.open_maps_for_location(this, C.clubs.get(event.get_venue_id()).get_venue_address());
			break;
		case R.id.invite:
			i = new Intent(this, WTP_Friends_Fragment.class);
			i.putExtra(PARAMS.EVENT_ID.get_param(), event.get_event_id());
			startActivity(i);
			break;
		case R.id.fb:
			Session session = Session.getActiveSession();
			if (session != null && session.isOpened()) {
				this.publishStory();
                app_tracker.send(new C().generate_analytics(C.CATEGORY.EVENTS.get_category(),C.ACTION.ACTION_SHARE.get_action(),C.LABEL.FB.get_label()));

            } else {

				if (!session.isOpened() && !session.isClosed()) {
					session.openForRead(new Session.OpenRequest(this)
							.setPermissions(
									Arrays.asList("public_profile",
											"user_friends")).setCallback(
									callback));
				} else {
					Session.openActiveSession(this, true, callback);
				}
			}

			break;
		case R.id.twitter:
            Twitter_Login log = new Twitter_Login(this);
            if(log.is_twitter_login())
            {
                String url = null;
                url = C.app_url;
                if(url != null && event.get_url().length() > 2)
                {
                    url = C.app_url;
                }
                new Twitter_Share(getApplicationContext(),event.get_event_name(),C.event_caption,event.get_event_date(),C.clubs.get(event.get_venue_id()).get_venue_address(),event.get_event_details(),url,false).execute();
                app_tracker.send(new C().generate_analytics(C.CATEGORY.EVENTS.get_category(),C.ACTION.ACTION_SHARE.get_action(),C.LABEL.TWIT.get_label()));

            }
            else
            {
                new Twitter_Login(this).login_to_twitter();
            }

			break;
		case R.id.add_guestlist:
		case R.id.add_guestlist1:
			if (!db.check_guest_list(event_id, event.get_event_date())) {
				if (add_guest == null) {
					if (net.isConnectingToInternet()) {
						add_guest = new Add_Guest(this, C.clubs.get(event.get_venue_id())
								.get_vd_id(), event.get_event_id());
						add_guest.execute();
					} else {
						C.net_alert(getApplicationContext());
					}
				}
			} else {
				C.alert(getApplicationContext(),
						"Already requested for a guestlist.");
			}
			break;
		case R.id.buy:

                if (event.getTicket_url() != null) {

                    app_tracker.send(new C().generate_analytics(C.CATEGORY.EVENTS.get_category(), C.ACTION.ACTION_BUY.get_action(), C.LABEL.YES.get_label()));
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(event.getTicket_url()));
                    startActivity(intent);
                } else
                    app_tracker.send(new C().generate_analytics(C.CATEGORY.EVENTS.get_category(), C.ACTION.ACTION_BUY.get_action(), C.LABEL.NO.get_label()));


            break;
		case R.id.cab:
			i = new Intent(this, Activity_Cabs.class);
			startActivity(i);
            app_tracker.send(new C().generate_analytics(C.CATEGORY.EVENTS.get_category(), C.ACTION.ACTION_OK.get_action(), C.LABEL.BOOK_CAB.get_label()));

            break;
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

	}

	private final BroadcastReceiver NewMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent i) {
			if (i.hasExtra(PARAMS.EVENT_ID.get_param())
					|| i.hasExtra(PARAMS.ARTIST.get_param())) {
				update_views();
			}
		}
	};

	// ///////////////////////////////
	class TwitterGetAccessTokenTask extends AsyncTask<String, String, String> {

		@Override
		protected void onPostExecute(String userName) {
			if (userName != null) {
				C.alert(getApplicationContext(), "Successfully logged in as "
						+ userName);
				new Update_FB_TWIT(false).execute();
				if (event != null) {
                    new Twitter_Share(getApplicationContext(), getResources().getString(R.string.app_name), C.app_caption,null,null, C.app_info, C.app_url, true)
                            .execute();
                    new Twitter_Share(getApplicationContext(),event.get_event_name(),C.event_caption,event.get_event_date(),C.clubs.get(event.get_venue_id()).get_venue_address(),event.get_event_details(),event.get_url(),false).execute();

                }

			}
            else
				C.alert(getApplicationContext(), "Twitter login failed, please try after some time");
		}

		@Override
		protected String doInBackground(String... params) {

			Twitter twitter = TwitterUtil.getInstance().getTwitter();
			RequestToken requestToken = TwitterUtil.getInstance()
					.getRequestToken(true);
			if (params[0] != null) {
				try {

					AccessToken accessToken = twitter.getOAuthAccessToken(
							requestToken, params[0]);
					SharedPreferences sharedPreferences = PreferenceManager
							.getDefaultSharedPreferences(getApplicationContext());
					SharedPreferences.Editor editor = sharedPreferences.edit();
					editor.putString(C.PREFERENCE_TWITTER_OAUTH_TOKEN,
							accessToken.getToken());
					editor.putString(C.PREFERENCE_TWITTER_OAUTH_TOKEN_SECRET,
							accessToken.getTokenSecret());
					editor.putBoolean(C.PREFERENCE_TWITTER_IS_LOGGED_IN, true);
					editor.commit();
					App.set_twitid(String.valueOf(accessToken.getUserId()));
					return twitter.showUser(accessToken.getUserId()).getName();
				} catch (TwitterException e) {
					e.printStackTrace(); // To change body of catch statement
											// use File | Settings | File
											// Templates.
				}
			} else {
				SharedPreferences sharedPreferences = PreferenceManager
						.getDefaultSharedPreferences(getApplicationContext());
				String accessTokenString = sharedPreferences.getString(
						C.PREFERENCE_TWITTER_OAUTH_TOKEN, "");
				String accessTokenSecret = sharedPreferences.getString(
						C.PREFERENCE_TWITTER_OAUTH_TOKEN_SECRET, "");
				AccessToken accessToken = new AccessToken(accessTokenString,
						accessTokenSecret);
				try {
					TwitterUtil.getInstance().setTwitterFactory(accessToken);
					App.set_twitid(String.valueOf(accessToken.getUserId()));
					return TwitterUtil.getInstance().getTwitter()
							.showUser(accessToken.getUserId()).getName();
				} catch (TwitterException e) {
					e.printStackTrace(); // To change body of catch statement
											// use File | Settings | File
											// Templates.
				}
			}

			return null; // To change body of implemented methods use File |
							// Settings | File Templates.
		}
	}

	@Override
	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		check_guest_button();
		return false;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub
		check_guest_button();
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub
		check_guest_button();

	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		check_guest_button();
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
		check_guest_button();
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		check_guest_button();
		return false;
	}

	// //////////////////////////////
	private void check_guest_button() {
		if (C.clubs.get(event.get_venue_id()).get_venue_type() != 5) {
			if (!event.is_in_guest_list()) {
				Rect scrollBounds = new Rect();
				Rect guest_bounds = new Rect();
				scroll.getHitRect(scrollBounds);
				add_guestlist.getLocalVisibleRect(guest_bounds);
				if (scrollBounds.contains(guest_bounds)) {
					add_guestlist1.setVisibility(View.GONE);

				} else {
					add_guestlist1.setVisibility(View.VISIBLE);

				}
			}
		}
	}

	// /////////////////////////////////
	public class Get_Events extends
			AsyncTask<ArrayList<NameValuePair>, Long, JSONObject> {
        private MyProgress progress;
		@Override
		protected void onPreExecute() {
            progress = new MyProgress(Activity_Event_Details.this);
            progress.setCancelable(false);
            progress.show();
		}

		@Override
		protected JSONObject doInBackground(ArrayList<NameValuePair>... params) {
			return get_events(params[0]);
		}

		@Override
		protected void onPostExecute(JSONObject obj) {
            if(progress != null)
                progress.dismiss();
			get_event_items_from_json(obj);

		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
		}

	}

	public JSONObject get_events(ArrayList<NameValuePair> params) {
		JSONParser parser = new JSONParser();
		JSONObject user;
		user = parser.makeHttpRequest(C.SERVER_URL, "POST", params);

		return user;
	}

	public void get_event_items_from_json(JSONObject obj) {
		if (obj != null) {
			if (obj.has(PARAMS.STATUS.get_param())) {
				try {
					int status = obj.getInt(PARAMS.STATUS.get_param());
					if (status > 0) {
                        Log.d("details",obj.toString());
						JSONObject data = obj.getJSONObject(PARAMS.DATA
                                .get_param());
						event = new C().get_event(data.getJSONObject("event"));
                        V_Details club = new C().get_venue_details(data.getJSONObject("venue"));
						C.clubs.put(club.get_vd_id(),club);
                        if(club != null)
                        if (event != null)
							this.update_views();

					} else {
						C.alert(this, "Please try again after some time");
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				C.alert(this, "Please try again after some time");
			}
		}
	}

	// ////////////////////////////////////////
	private UiLifecycleHelper uiHelper;
	protected Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};
	private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
		@Override
		public void onError(FacebookDialog.PendingCall pendingCall,
				Exception error, Bundle data) {
			Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
		}

		@Override
		public void onComplete(FacebookDialog.PendingCall pendingCall,
				Bundle data) {
			Log.d("HelloFacebook", "Success!");
		}
	};
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean(PENDING_PUBLISH_KEY, pendingPublishReauthorization);
		uiHelper.onSaveInstanceState(outState);
	}

	private void onSessionStateChange(Session session, SessionState state,
			Exception exception) {
		if (state.isOpened()) {
			if (pendingPublishReauthorization
					&& state.equals(SessionState.OPENED_TOKEN_UPDATED)) {
				pendingPublishReauthorization = false;
				publishStory();
			}
		} else if (state.isClosed()) {
			// shareButton.setVisibility(View.INVISIBLE);
		}
	}

	private void publishStory() {
		//String url = "https://s3-ap-southeast-1.amazonaws.com/wtp-imgs/bannersize01.jpg";
		Session session = Session.getActiveSession();

		if (session != null) {

			// Check for publish permissions
			List<String> permissions = session.getPermissions();
			if (!isSubsetOf(PERMISSIONS, permissions)) {
				pendingPublishReauthorization = true;
				Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(
						this, PERMISSIONS);
				session.requestNewPublishPermissions(newPermissionsRequest);
				return;
			}
			String image = "",event_url = "http://wtp.club";
			if(event.get_images() != null && event.get_images().size() > 0)
				image = event.get_images().get(0).get_image();
			else
				image = C.app_logo;

            if(event.get_url() != null && event.get_url().length() > 2)
            {
                event_url = event.get_url();
            }
			
			super.publishStory(event.get_event_name(),C.event_caption,event.get_event_details(),event_url,image,false,true);
		}

	}

	private boolean isSubsetOf(Collection<String> subset,
			Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}
}
