package com.dpc.wtp.adapters;


import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.dpc.wtp.R;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.Utils.TextB;

public class Artist_Details_Adapter extends BaseAdapter {
	LayoutInflater inflater;
	ViewHolder holder;
	View v;
	List<Event> items;
	Context c;
    private class ViewHolder {
		Text name,genre;
		TextB date;
		ImageView status_icon;
		
		
	}
	public Artist_Details_Adapter(Context c,List<Event> l)
	{
		this.c = c;
		this.items = l;

	}
	
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		Event event = items.get(position);
		
		v = convertView;
		if (v == null) 
		{
			 inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 v = inflater.inflate(R.layout.row_club_details,parent, false);
			 holder = new ViewHolder();
			 holder.date = (TextB) v.findViewById(R.id.date);
			 holder.name = (Text) v.findViewById(R.id.e_name);
			 holder.genre = (Text) v.findViewById(R.id.genre);
			 holder.status_icon = (ImageView) v.findViewById(R.id.status_icon);
			 v.setTag(holder);
			 
		}
		else
			holder = (ViewHolder) v.getTag();
		
		String[] date = C.get_date_as_day_month(event.get_event_date());
		
		holder.date.setText(date[1].toUpperCase()+"\n"+C.get_date_suffix(date[0]));
		set_text(holder.name,event.get_event_name());

		set_text(holder.genre,C.clubs.get(event.get_venue_id()).get_venue_name());
		
		//holder.e_image.setBackgroundResource(resid);
		
		
		return v;

	}


	private void set_text(Text t_view, String txt) {
		// TODO Auto-generated method stub
		t_view.setText(txt);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}


	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
	}


	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

}