package com.dpc.wtp.Models;


public class TextMedia
{
    private String textmsg, serverurl;
    int chatId, mediatype;


    public TextMedia(String textmsg, int mediatype, int chatId, String serverurl) {

        this.textmsg = textmsg;
        this.mediatype = mediatype;
        this.chatId = chatId;
        this.serverurl = serverurl;

    }

    public String getServerurl() {
        return serverurl;
    }

    public void setServerurl(String serverurl) {
        this.serverurl = serverurl;
    }

    public int getChatId() {
        return chatId;
    }

    public void setChatId(int chatId) {
        this.chatId = chatId;
    }

    public int getMediatype() {
        return mediatype;
    }

    public void setMediatype(int mediatype) {
        this.mediatype = mediatype;
    }

    public String getTextmsg() {
        return textmsg;
    }

    public void setTextmsg(String textmsg) {
        this.textmsg = textmsg;
    }
}
