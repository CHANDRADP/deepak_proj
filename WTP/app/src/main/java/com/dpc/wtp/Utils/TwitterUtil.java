package com.dpc.wtp.Utils;

import com.dpc.wtp.tasks.Twitter_Share;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

public final class TwitterUtil {

    private RequestToken requestToken = null;
    private TwitterFactory twitterFactory = null;
    private Twitter twitter;

    private TwitterUtil() {
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.setOAuthConsumerKey(C.TWITTER_CONSUMER_KEY);
        configurationBuilder.setOAuthConsumerSecret(C.TWITTER_CONSUMER_SECRET);
        Configuration configuration = configurationBuilder.build();
        twitterFactory = new TwitterFactory(configuration);

        twitter = twitterFactory.getInstance();

    }

    public TwitterFactory getTwitterFactory()
    {
        return twitterFactory;
    }

    public void setTwitterFactory(AccessToken accessToken)
    {
        twitter = twitterFactory.getInstance(accessToken);
    }

    public Twitter getTwitter()
    {
        return twitter;
    }
    public RequestToken getRequestToken(boolean login) {
        if (requestToken == null) {
            try {
            	if(login)
            	{
            		requestToken = twitterFactory.getInstance().getOAuthRequestToken(C.TWITTER_CALLBACK_URL_EVENT);

            	}
            	else
            		requestToken = twitterFactory.getInstance().getOAuthRequestToken(C.TWITTER_CALLBACK_URL_EVENT);
            } catch (TwitterException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return requestToken;
    }

    static TwitterUtil instance = new TwitterUtil();

    public static TwitterUtil getInstance() {
        return instance;
    }


    public void reset() {
        instance = new TwitterUtil();
    }
}
