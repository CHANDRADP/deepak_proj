package com.dpc.wtp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.actionbarsherlock.view.Menu;
import com.dpc.wtp.Models.WTP_Contact;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.services.WTP_Service;


import java.util.ArrayList;


public class Activity_NonChatUsers extends MyActivity
{

    ListView MyChatUsersList;
    ArrayList<WTP_Contact> appuserlist;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.actionbar);

            super.onCreate(savedInstanceState);
            db = new DatabaseHelper(this);
            setContentView(R.layout.activity_non_chat_users);
            MyChatUsersList = (ListView) findViewById(R.id.MyChatUsersList);
            appuserlist = new ArrayList<WTP_Contact>();

            Log.d("Entered On Create", "Hello");
            setUiValues();

        MyChatUsersList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id)
            {
                String chatid = null;

                WTP_Contact itemValue = (WTP_Contact) MyChatUsersList.getItemAtPosition(position);

                if(!db.check_chatId_exist(itemValue.get_wtp_id()))
                {
                    long idnew = db.insertChatDetails(itemValue.get_wtp_id(),"","","0","0");
                    chatid = String.valueOf(idnew);
                }
                Intent i = new Intent(Activity_NonChatUsers.this, ActivityChatWindow.class);
                i.putExtra("userid",Integer.parseInt(itemValue.get_wtp_id()));
                i.putExtra("unreadcount","0");
                i.putExtra("isgroup","0");
                i.putExtra("chatid",chatid);
                Log.d("WTP ID", itemValue.get_wtp_id());

                startActivity(i);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }


    @Override
    public void onResume()
    {
        super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
        setUiValues();
    }


    @Override
    protected void onPause()
    {
        super.onPause();
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;
    }

    private void setUiValues()
    {
        //appuserlist = db.get_app_users();
        appuserlist = db.get_all_users_dummy();
        MyChatUsersList.setAdapter(new MyAdapter(appuserlist,Activity_NonChatUsers.this));

    }

    class MyAdapter extends BaseAdapter
    {
        View v;
        LayoutInflater inflater;
        ViewHolder holder;
        private Context c;
        private ArrayList<WTP_Contact> myusers;

        public MyAdapter(ArrayList<WTP_Contact> mCurrentList, Context mContext)
        {
            this.myusers = mCurrentList;
            this.c = mContext;
        }

        @Override
        public int getCount()
        {
            if (myusers != null)
                return myusers.size();
            else
                return 0;
        }

        @Override
        public Object getItem(int position)
        {
            if (myusers != null)
                return myusers.get(position);
            else
                return null;
        }

        @Override
        public long getItemId(int position)
        {
            return position;
        }

        private class ViewHolder
        {
            Text txtName;
        }

        @Override
        public View getView(final int arg0,final View convertView, ViewGroup arg2)
        {
            WTP_Contact user = myusers.get(arg0);
            v = convertView;
            if (v == null)
            {
                inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.non_chat_users_list_row, null);
                holder = new ViewHolder();
                holder.txtName = (Text) v.findViewById(R.id.txtName);
                v.setTag(holder);
            }
            else
            {
                holder = (ViewHolder) v.getTag();
            }
            Log.d(" WTP User name", user.get_name());
            holder.txtName.setText(user.get_name());
            return v;
        }
    }
}
