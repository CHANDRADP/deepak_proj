package com.dpc.wtp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.actionbarsherlock.view.MenuItem;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.Net_Detect;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.customs.MyProgress;
import com.dpc.wtp.tasks.Update_FB_TWIT;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginButton;

import org.json.JSONException;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Activity_Facebook extends MyActivity {
	private Text title;
	private LoginButton fb;
	private Net_Detect net;
    private MyProgress dialog;
	private void init_net_details() {
		net = new Net_Detect(this);

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.actionbar);

		title = (Text) findViewById(android.R.id.title);
		title.setText("Login to facebook");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.ac_facebook);
		init_net_details();
		fb = (LoginButton) findViewById(R.id.fb);

		fb.setReadPermissions(Arrays.asList("public_profile", "user_friends",
				"email"));
		fb.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!net.isConnectingToInternet()) {
					net.alert(getSupportFragmentManager());
                    dialog = new MyProgress(getApplicationContext());
                    dialog.setCancelable(false);
                    dialog.show();
				}

			}

		});
		 uiHelper = new UiLifecycleHelper(this, callback);
	        uiHelper.onCreate(savedInstanceState);
	        if (savedInstanceState != null) 
			{
			    pendingPublishReauthorization = 
			        savedInstanceState.getBoolean(PENDING_PUBLISH_KEY, false);
			}

		 fb.setUserInfoChangedCallback(new
		  LoginButton.UserInfoChangedCallback() {

		  @Override public void onUserInfoFetched(GraphUser user) {
              if(dialog != null)
                  dialog.dismiss();
              if(user != null)
              {
                  if(App.get_userid() != null)
                  {

                          App.set_fb_login(true);
                          App.set_fbid(user.getId());
                          try {
                              String gender = user.getInnerJSONObject().getString(C.PARAMS.GENDER.get_param());
                              if(gender.equalsIgnoreCase("male"))
                              {
                                  gender = "1";
                              }
                              else if(gender.equalsIgnoreCase("female"))
                              {
                                  gender = "2";
                              }
                              App.set_gender(gender);
                              new Update_FB_TWIT(true).execute();
                          } catch (JSONException e) {
                              e.printStackTrace();
                          }
                          startActivity(new Intent(Activity_Facebook.this,Activity_City.class));
                          finish();


                  }
              }


		  } });




    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {

		case android.R.id.home:
			super.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
		if (!net.isConnectingToInternet()) {
			net.alert(getSupportFragmentManager());
		}

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;


    }

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

	}

	

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
        Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);


	}

	// ////////////////////////////////////////
	private UiLifecycleHelper uiHelper;
	protected Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};
	private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
		@Override
		public void onError(FacebookDialog.PendingCall pendingCall,
				Exception error, Bundle data) {
			Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
		}

		@Override
		public void onComplete(FacebookDialog.PendingCall pendingCall,
				Bundle data) {
			Log.d("HelloFacebook", "Success!");
		}
	};
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean(PENDING_PUBLISH_KEY, pendingPublishReauthorization);
		uiHelper.onSaveInstanceState(outState);
	}

	private void onSessionStateChange(Session session, SessionState state,
			Exception exception) {
		if (state.isOpened()) {
			App.set_fb_login(true);
			if (pendingPublishReauthorization
					&& state.equals(SessionState.OPENED_TOKEN_UPDATED)) {
				pendingPublishReauthorization = false;
				publishStory();
			}
		} else if (state.isClosed()) {
			// shareButton.setVisibility(View.INVISIBLE);
		}
	}

	private void publishStory() {
		Session session = Session.getActiveSession();

		if (session != null) {

			// Check for publish permissions
			List<String> permissions = session.getPermissions();
			if (!isSubsetOf(PERMISSIONS, permissions)) {
				pendingPublishReauthorization = true;
				Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(
						this, PERMISSIONS);
				session.requestNewPublishPermissions(newPermissionsRequest);
				return;
			}

			super.publishStory(getResources().getString(R.string.app_name),C.app_caption,C.app_info,C.app_url,C.app_logo,true,false);
	 		
		}

	}

	private boolean isSubsetOf(Collection<String> subset,
			Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}
}
