package com.dpc.wtp.activities;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.dpc.wtp.Models.NewMsgCount;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.EText;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.customs.Custom_Send_Message;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.services.WTP_Service;


import org.jivesoftware.smack.packet.Message;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


public class Activity_CreateGroup extends MyActivity {

    ListView MyChatUsersList;
    Integer[] Uservalues;
    Text btnCreate;
    EText txtGroupName;
    List<Integer> stringList;
    ArrayList<NewMsgCount> counterlist;

    ArrayList<String> selectedUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar);

        super.onCreate(savedInstanceState);
        db = new DatabaseHelper(this);
        setContentView(R.layout.activity_create_group);
        MyChatUsersList = (ListView) findViewById(R.id.MyChatUsersList);
        txtGroupName = (EText) findViewById(R.id.txtGroupName);
        btnCreate = (Text) findViewById(R.id.btnCreate);
        counterlist = new ArrayList<NewMsgCount>();
        selectedUsers = new ArrayList<String>();
        WTP_Service.CURRENT_CHAT_USER = "@hellochat";


        Uservalues = new Integer[] { 8,
                2,
                3,
                4,
                5,
                6,
                7 };

        stringList = new ArrayList<Integer>(Arrays.asList(Uservalues));

        setUiValues();

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = txtGroupName.getText().toString();

                String localTime = get_current_time();
                String msgDateTime = get_universal_date(localTime);

                if(selectedUsers.size()<1)
                {
                    Toast.makeText(getApplicationContext(),"Select atleast one user", Toast.LENGTH_LONG).show();
                }
                else if(name.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(),"Please enter group name", Toast.LENGTH_LONG).show();
                }
                else
                {
                    String users = selectedUsers.toString();
                    users = users.substring(1, users.length()-1);
                    db.insertGroupDetails(name, localTime, App.get_userid(),users,"101");
                    db.insertChatDetails("101", "Hi, You have Created a group", localTime, "1", "0"); // change it to id received from server

                    Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),101,"Hi, I have Created a group", C.GROUP_INVITE);
                    csm.send_group_message();


                    txtGroupName.setText("");
                    finish();
                }

            }
        });


    }

    public String get_current_time()
    {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
        return sdf.format(c.getTime());
    }

    public String get_universal_date(String time)
    {
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm a");


        Date date = null;
        try
        {
            //Here you say to java the initial timezone. This is the secret
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = sdf.parse(time);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        //Here you set to your timezone

        sdf.setTimeZone(TimeZone.getDefault());
        time = sdf.format(date);
        return time;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }


    @Override
    public void onResume()
    {
        super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
        setUiValues();

    }


    @Override
    protected void onPause() {
        super.onPause();
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;

    }

    private void setUiValues()
    {
        counterlist.clear();
        for(int p = 0; p<=stringList.size()-1;p++)
        {
            Log.d("P value", Integer.toString(p));
            String name  = stringList.get(p).toString();


            NewMsgCount mymsgcount = new NewMsgCount(name,0, "", "",1,101);// change it to id received from server
            counterlist.add(mymsgcount);
            Log.d("counterlist", counterlist.toString());
        }
        MyChatUsersList.setAdapter(new MyAdapter(counterlist,Activity_CreateGroup.this));

    }

    class MyAdapter extends BaseAdapter {

        View v;
        LayoutInflater inflater;
        ViewHolder holder;
        private Context c;
        private ArrayList<NewMsgCount> mygroupusers;



        public MyAdapter(ArrayList<NewMsgCount> mCurrentList, Context mContext) {
            this.mygroupusers = mCurrentList;
            this.c = mContext;

        }


        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            if (mygroupusers != null)
                return mygroupusers.size();
            else
                return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            if (mygroupusers != null)
                return mygroupusers.get(position);
            else
                return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        private class ViewHolder {
            Text txtName;
            CheckBox customCheckBox;



        }

        @Override
        public View getView(final int arg0,final View convertView, ViewGroup arg2)
        {
            // TODO Auto-generated method stub


            final NewMsgCount temp = mygroupusers.get(arg0);

            v = convertView;

            if (v == null)
            {
                inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.create_group_single_row, null);
                holder = new ViewHolder();
                holder.txtName = (Text) v.findViewById(R.id.txtName);
                holder.customCheckBox = (CheckBox) v.findViewById(R.id.customCheckBox);

                v.setTag(holder);

            }
            else
            {
                holder = (ViewHolder) v.getTag();
            }


            holder.txtName.setText(temp.getUsername());
            holder.customCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked)
                    {
                        selectedUsers.add(temp.getUsername());
                    }
                    else
                    {
                        selectedUsers.remove(temp.getUsername());
                    }

                    Log.d("Selected users", selectedUsers.toString());
                }
            });




            return v;
        }


    }




}
