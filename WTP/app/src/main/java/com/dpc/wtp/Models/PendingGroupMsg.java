package com.dpc.wtp.Models;


public class PendingGroupMsg
{
    private String textmsg;
    private int mediatype, rowId, memberid, isgroup, servergroupid;


    public PendingGroupMsg(String textmsg, int mediatype, int rowid, int memberid, int isgroup, int servergroupid) {

        this.textmsg = textmsg;
        this.mediatype = mediatype;
        this.rowId = rowid;
        this.memberid = memberid;
        this.isgroup = isgroup;
        this.servergroupid = servergroupid;

    }

    public String getTextmsg() {
        return textmsg;
    }

    public void setTextmsg(String textmsg) {
        this.textmsg = textmsg;
    }

    public int getMediatype() {
        return mediatype;
    }

    public void setMediatype(int mediatype) {
        this.mediatype = mediatype;
    }

    public int getRowId() {
        return rowId;
    }

    public void setRowId(int rowId) {
        this.rowId = rowId;
    }

    public int getIsgroup() {
        return isgroup;
    }

    public void setIsgroup(int isgroup) {
        this.isgroup = isgroup;
    }

    public int getMemberid() {
        return memberid;
    }

    public void setMemberid(int memberid) {
        this.memberid = memberid;
    }

    public int getServergroupid() {
        return servergroupid;
    }

    public void setServergroupid(int servergroupid) {
        this.servergroupid = servergroupid;
    }
}
