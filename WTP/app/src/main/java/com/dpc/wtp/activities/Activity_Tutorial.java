package com.dpc.wtp.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.CirclePageIndicator;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.adapters.Tutorial_Adapter;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.tasks.Update_Tutorial;

public class Activity_Tutorial extends MyActivity implements OnClickListener{

	private ViewPager pager;
	private Tutorial_Adapter adapter;
	private Button finish;
	private ImageView close;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	//	getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.actionbar);
		final Text title = (Text)findViewById(android.R.id.title);
		title.setText("Instructions");
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.ac_image_pager);
		close = (ImageView)this.findViewById(R.id.close);
		close.setVisibility(View.VISIBLE);
		finish = (Button)this.findViewById(R.id.finish);
		close.setOnClickListener(this);
		finish.setOnClickListener(this);
		pager = (ViewPager)this.findViewById(R.id.pager);
		adapter = new Tutorial_Adapter(this);
		pager.setAdapter(adapter);
		CirclePageIndicator mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
		mIndicator.setViewPager(pager);
		mIndicator.setVisibility(View.VISIBLE);
        pager.setOnPageChangeListener(new OnPageChangeListener(){

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				if(arg0 == 4)
				{
					finish.setVisibility(View.VISIBLE);
				}
				else
				{
					finish.setVisibility(View.GONE);
				}
			}

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
                if(arg0 == 4)
                {
                    finish.setVisibility(View.VISIBLE);
                }
                else
                {
                    finish.setVisibility(View.GONE);
                }
			}
			
		});

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		App.set_tutorial(true);
	}

    @Override
    protected void onPause() {
        super.onPause();
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;
    }

    @Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
			case R.id.finish:
			case R.id.close:
				App.set_tutorial(true);
				new Update_Tutorial(this).execute();
			finish();
			break;
		}
	}
	

}
