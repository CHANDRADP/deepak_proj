package com.dpc.wtp.customs;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.dpc.wtp.R;
import com.dpc.wtp.interfaces.CustomDilogInterface;


public class CustomDialogWithTwoButtons extends DialogFragment {
    private CustomDilogInterface myinterface;

    public void setButtonListener(CustomDilogInterface myinterface1) {
        this.myinterface = myinterface1;
    }

    public CustomDialogWithTwoButtons() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.custom_dialog_with_two_buttons, container);
        TextView txtDialog = (TextView) view.findViewById(R.id.textDialog);
        Button btnOk = (Button) view.findViewById(R.id.btnOk);
        Button btnCancel = (Button) view.findViewById(R.id.btnCancel);
        String msg = getArguments().getString("displayTextSecond");
        txtDialog.setText(msg);
        getDialog().setTitle("Alert");


        // Capture button clicks
        btnOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                myinterface.onButtonClickedNew(true);
                getDialog().dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                myinterface.onButtonClickedNew(false);
                getDialog().dismiss();
            }
        });
        return view;
    }
}