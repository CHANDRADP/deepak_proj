package com.dpc.wtp.interfaces;

import android.content.Context;

public interface Updatable {
	   public void update(Context c);
	}
