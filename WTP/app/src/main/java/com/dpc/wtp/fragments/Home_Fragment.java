package com.dpc.wtp.fragments;


import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.dpc.wtp.JSONParser;
import com.dpc.wtp.Models.Images;
import com.dpc.wtp.R;
import com.dpc.wtp.Models.Artist;
import com.dpc.wtp.Models.Deals;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Models.Server_Params;
import com.dpc.wtp.Models.V_Details;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.CirclePageIndicator;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.activities.Activity_Artist_Details;
import com.dpc.wtp.activities.Activity_Calendar;
import com.dpc.wtp.activities.Activity_Club_Details;
import com.dpc.wtp.activities.Activity_Deals_List;
import com.dpc.wtp.activities.Activity_Event_Details;
import com.dpc.wtp.activities.App;
import com.dpc.wtp.adapters.Artist_Adapter;
import com.dpc.wtp.adapters.Club_Adapter;
import com.dpc.wtp.adapters.Deals_Adapter;
import com.dpc.wtp.adapters.Events_Adapter;
import com.dpc.wtp.adapters.ImagePagerAdapter;
import com.dpc.wtp.customs.Alpha_Sorter;
import com.dpc.wtp.customs.Deals_Priority_Sorter;
import com.dpc.wtp.customs.Event_Priority_Sorter;
import com.dpc.wtp.customs.MyFragment;
import com.dpc.wtp.customs.MyProgress;
import com.dpc.wtp.customs.SlidingTabLayout;
import com.dpc.wtp.customs.Venue_Priority_Sorter;
import com.dpc.wtp.customs.list.ParallaxListView;
import com.dpc.wtp.interfaces.OnDetectScrollListener;
import com.dpc.wtp.tasks.Display_Image;
import com.facebook.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class Home_Fragment extends MyFragment  implements OnDetectScrollListener, ViewSwitcher.ViewFactory {

    private int club_sub_posi = 0,deal_sub_posi = 0,artist_sub_posi = 0;

  //  private InterstitialAd full_screen_ad;

    private TextView event_ads,venue_ads,deal_ads,artist_ads;

    private ImageView full_ad;


    private ArrayList<ImageView>  e_slide_imgs = new ArrayList<ImageView>() ;
    private ArrayList<ImageView>  c_slide_imgs = new ArrayList<ImageView>() ;


    //private AdView event_ad,club_ad,deal_ad,artist_ad;

    //private AdRequest event_ad_req,club_ad_req,deal_ad_req,artist_ad_req;

	//private GestureDetector gestureDetector;
	
	private Text title;
	
	public static int tab_position = 0;
	
	private String location = null;
	
    public static SlidingTabLayout mSlidingTabLayout;
    
    private LinearLayout sub_tabs;
    RelativeLayout e_slider,c_slider;

    private RelativeLayout full_ad_rl;
    
    private View line;

    private CirclePageIndicator event_slider_indicator,club_slider_indicator;

    private static final String[] tabs_name = new String[]{"Home","Venue","Deals","Artist"};
    
    public static final String[] club_tabs = new String[]{" All "," Bar ","Night club"," Pub ","Microbrewery"};
    
    public static final String[] deals_tabs = new String[]{" All ","Food","Happy Hrs","Hot Deals"};
	
    public static String[] artist_tabs = new String[]{" All ","Bollywood","Deep House"," EDM ","Electronic","Hip Hop","House"," Jazz "," Pop ","Progressive House","Psy-Trance"," RnB "," Rock ","Techno","Techno House","Trance"};
	
	private ArrayList<Event> event_list_items;
	
	private ArrayList<V_Details> club_list_items;
	
	private ArrayList<Deals> deals_list_items;
	
	private ArrayList<Artist> artist_list_items;
	
	private ParallaxListView event_list,club_list,deals_list,artist_list;
	
	private boolean show_progress = false;
	
	
	public Home_Fragment()
	{
		options = new DisplayImageOptions.Builder()
		.showStubImage(R.drawable.default_bg)
		.showImageForEmptyUri(R.drawable.default_bg)
		.showImageOnFail(R.drawable.default_bg)
		.cacheInMemory(true)
		.cacheOnDisc(true)
        .displayer(new FadeInBitmapDisplayer(C.ANIM_DURATION))
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
        club_list_items = new ArrayList<>();
        deals_list_items = new ArrayList<>();
        artist_list_items = new ArrayList<>();
        event_list_items = new ArrayList<>();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app_tracker.setScreenName(C.SCREEN.SCREEN_HOME.get_screen());
        app_tracker.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	
    	location = App.get_city();
    	
    	title =(Text) getSherlockActivity().getSupportActionBar().getCustomView().findViewById(android.R.id.title);
    	View v  = inflater.inflate(R.layout.frag_home, container, false);
    	full_ad_rl = (RelativeLayout)v.findViewById(R.id.full_ad_rl);
        full_ad = (ImageView)v.findViewById(R.id.full_ad);
        ImageView ad_close = (ImageView) v.findViewById(R.id.close);
        ad_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                full_ad_rl.setVisibility(View.GONE);
            }
        });
        return v;
    }

    @SuppressWarnings("unchecked")
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        //initAd();

        ViewPager mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        SamplePagerAdapter adapter = new SamplePagerAdapter();
        mViewPager.setAdapter(adapter);
        mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setSelectedIndicatorColors(new int[]{getResources().getColor(R.color.red)});
        mSlidingTabLayout.setTabTextsize(12);
        mSlidingTabLayout.setCustomTabView(R.layout.tab_item, R.id.txt);
        sub_tabs = (LinearLayout)view.findViewById(R.id.sub_tabs);
        line = (View)view.findViewById(R.id.line);
        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setOnPageChangeListener(new OnPageChangeListener(){

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onPageSelected(int position) {
				tab_position = position;
				if(title != null)
				title.setText(tabs_name[position]);
				if(position == 0)
				{
					
					sub_tabs.removeAllViews();
					sub_tabs.setVisibility(View.GONE);
					line.setVisibility(View.GONE);
                  //  e_slider.invalidate();
				}
				else if(position == 1)
				{
					sub_tabs.removeAllViews();
					add_club_subtabs();
					sub_tabs.setVisibility(View.VISIBLE);
					line.setVisibility(View.VISIBLE);
                  //  c_slider.invalidate();
				}
				else if(position == 2)
				{
					sub_tabs.removeAllViews();
					add_deals_subtabs();
					sub_tabs.setVisibility(View.VISIBLE);
					line.setVisibility(View.VISIBLE);
					
				}
				else if(position == 3)
				{
					sub_tabs.removeAllViews();
					add_artist_subtabs();
					sub_tabs.setVisibility(View.VISIBLE);
					line.setVisibility(View.VISIBLE);
				}
				
				
			}});
       if(C.items.size() == 0)
       {
    	   show_progress = true;
       }
        if(!App.is_closed()) {
            new Get_Events(this.getSherlockActivity()).execute();
        }

     //   start_sliding();
    }
    /*
    public void displayInterstitial() {
        if (full_screen_ad.isLoaded()) {
            full_screen_ad.show();
        }
    }
    */



    @Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		AppEventsLogger.deactivateApp(this.getSherlockActivity(),getResources().getString(R.string.fb_app_id));

	}

    @Override
    public void onStop() {
        super.onStop();
       if(!App.is_closed())
        getSherlockActivity().unregisterReceiver(NewMessageReceiver);

    }


    @SuppressWarnings("unchecked")
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
        if(App.is_closed())
        {
            return;
        }
		AppEventsLogger.activateApp(this.getSherlockActivity(),getResources().getString(R.string.fb_app_id));
		if(!location.equalsIgnoreCase(App.get_city()))
		{
			show_progress = true;
			location = App.get_city();

			new Get_Events(this.getSherlockActivity()).execute(new Server_Params().get_details_by_location(App.get_userid()));
	    }
		getSherlockActivity().registerReceiver(NewMessageReceiver, new IntentFilter(
				C.BROADCAST_MSG));
		
	}
	
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if(handler != null)
			handler.removeCallbacks(image_slider);
       // imageLoader.clearDiscCache();
      //  imageLoader.clearMemoryCache();

	}


	private final BroadcastReceiver NewMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent i) 
		{
			if(i.hasExtra(PARAMS.LOCATION.get_param()))
			{
				update_list();
			}
		}
	};
    private class SamplePagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return tabs_name.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {
            return o == view;
        }

        @Override
        public CharSequence getPageTitle(int position) {
        	return tabs_name[position];
        }
        @Override
        public Object instantiateItem(ViewGroup container, int position) 
        {
        	if(position == 0)
        	{
        		final View view = getSherlockActivity().getLayoutInflater().inflate(R.layout.frag_home_pages, container, false);
                /*event_ad = (AdView) view.findViewById(R.id.adView);
                event_ad_req = new AdRequest.Builder()
                        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                        .build();
                event_ad.loadAd(event_ad_req);
                */
        		//View header = getSherlockActivity().getLayoutInflater().inflate(R.layout.header_view_page, null);
        		update_event_page(view,null);

        		container.addView(view);
        		return view;
        	}
        	if(position == 1)
        	{
        		final View view = getSherlockActivity().getLayoutInflater().inflate(R.layout.frag_home_pages, container, false);
             /*   club_ad = (AdView) view.findViewById(R.id.adView);
                club_ad_req = new AdRequest.Builder()
                        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                        .build();
                club_ad.loadAd(club_ad_req);
                */
        	//	View header = getSherlockActivity().getLayoutInflater().inflate(R.layout.header_view_page, null);
        		update_club_page(view,null);
        		container.addView(view);
        		return view;
        	}
        	if(position == 2)
        	{
        		final View view = getSherlockActivity().getLayoutInflater().inflate(R.layout.frag_home_pages, container, false);
               /* deal_ad = (AdView) view.findViewById(R.id.adView);
                deal_ad_req = new AdRequest.Builder()
                        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                        .build();
                deal_ad.loadAd(deal_ad_req);
                */
        	    update_deals_page(view);
        		container.addView(view);
        		return view;
        	}
        	if(position == 3)
        	{
        		final View view = getSherlockActivity().getLayoutInflater().inflate(R.layout.frag_home_pages, container, false);
              /*  artist_ad = (AdView) view.findViewById(R.id.adView);
                artist_ad_req = new AdRequest.Builder()
                        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                        .build();
                artist_ad.loadAd(artist_ad_req);
                */
        	    update_artist_page(view);
        		container.addView(view);
        		return view;
        	}
        	return null;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);

        }

    }
    private ViewPager event_viewPager,club_viewPager;

   // private ImageView event_image,club_image;
    
   public ArrayList<Event> event_baner_images = new ArrayList<>();
    
    public ArrayList<V_Details> club_baner_images = new ArrayList<>();
    
    private int event_current_item = 0,club_current_item = 0;
    
    private Handler handler;
    
    private Runnable image_slider;
    
    public void update_event_page(View v,View header)
    {
        SwipeRefreshLayout refreshLayout;
        /*
        event_current_item = 0;

    	 //   event_viewPager = (ViewPager) header.findViewById(R.id.viewpager);
          //  ImagePagerAdapter adap = new ImagePagerAdapter(this.getSherlockActivity(), imageLoader, options);
          //  adap.set_event_banners(event_baner_images);
          //  event_viewPager.setAdapter(adap);
            event_slider_indicator = (CirclePageIndicator) header.findViewById(R.id.indicator);
            event_slider_indicator.setIndicator_count(event_baner_images.size());
           // event_slider_indicator.setViewPager(event_viewPager);

		//e_slider = (RelativeLayout)header.findViewById(R.id.ll_slider);
        */
        event_list = (ParallaxListView)v.findViewById(R.id.list);
        set_event_ads(v);
		//event_list.addParallaxedHeaderView(header);
		event_list_items.clear();
		for(Event e : C.items.values())
		{
			event_list_items.add(e);
			
		}
		Collections.sort(event_list_items, new Event_Priority_Sorter());
		event_list.setAdapter(new Events_Adapter(getSherlockActivity().getApplicationContext(),event_list_items,imageLoader,options,app_tracker));

		event_list.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int posi,
					long arg3) 
			{
			//	if(posi != 0)
			//	{
					Intent i = new Intent(getSherlockActivity().getApplicationContext(),Activity_Event_Details.class);
					i.putExtra(PARAMS.EVENT_ID.get_param(), event_list_items.get(posi).get_event_id());
					startActivity(i);
			//	}
			}
			
		});
		refreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
		//	refreshLayout.setOnRefreshListener(this);
		//    setAppearance();
		//    enableSwipe();
		    refreshLayout.setEnabled(false);
		    

    }


    /////////////////////////////////////
    public void update_club_page(View v,View header)
    {
    	 SwipeRefreshLayout refreshLayout;
    	/*//club_viewPager = (ViewPager) header.findViewById(R.id.viewpager);
    		//club_baner_images.clear();
        club_current_item = 0;
    	//	ImagePagerAdapter adap = new ImagePagerAdapter(getSherlockActivity(),imageLoader,options);
         //   adap.set_club_banners(club_baner_images);
    	//	club_viewPager.setAdapter(adap);
            club_slider_indicator = (CirclePageIndicator) header.findViewById(R.id.indicator);
            club_slider_indicator.setIndicator_count(club_baner_images.size());
    		//club_slider_indicator.setViewPager(club_viewPager);

            c_slider = (RelativeLayout)header.findViewById(R.id.ll_slider);
            */
            club_list = (ParallaxListView)v.findViewById(R.id.list);
            set_venue_ads(v);
    	//	club_list.addParallaxedHeaderView(header);//.addHeaderView(header);
    		club_list_items.clear();
    		for(V_Details club : C.clubs.values())
    		{
    			//if(App.get_city().equalsIgnoreCase(e.get_venue().get_event_city()))
    			club_list_items.add(club);
    			
    		}
    		Collections.sort(club_list_items, new Venue_Priority_Sorter());
    		club_list.setAdapter(new Club_Adapter(getSherlockActivity().getApplicationContext(),club_list_items,imageLoader,options));

    		club_list.setOnItemClickListener(new OnItemClickListener(){

    			@Override
    			public void onItemClick(AdapterView<?> arg0, View arg1, int posi,
    					long arg3) {
    				//if(posi != 0)
    				//{
    					Intent i = new Intent(getSherlockActivity().getApplicationContext(),Activity_Club_Details.class);
    					i.putExtra(PARAMS.VD_ID.get_param(), club_list_items.get(posi).get_vd_id());
    					startActivity(i);

    				//}
    			}
    			
    		});
    		
    		 
    		 refreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
    		//	refreshLayout.setOnRefreshListener(this);
    		//    setAppearance();
    		//    enableSwipe();
    		    refreshLayout.setEnabled(false);
    }
    private void add_club_subtabs()
	{
		final View.OnClickListener subtabClickListener = new Club_SubTabClickListener();
		for (int i = 0; i < club_tabs.length; i++) {
            View tabView = null;
            Text tabTitleView = null;
            if (tabView == null) {
                tabView = createDefaultTabView();
            }

            if (tabTitleView == null && TextView.class.isInstance(tabView)) {
                tabTitleView = (Text) tabView;
            }
            tabTitleView.setText(club_tabs[i]);
            tabView.setOnClickListener(subtabClickListener);
            if(i == club_sub_posi)
            {
                tabView.setBackgroundResource(R.drawable.round_bg_pressed);
            }
            else
            {
                tabView.setBackgroundResource(R.drawable.round_bg);
            }
            sub_tabs.addView(tabView);
        }
	}
	private class Club_SubTabClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            club_list_items.clear();
            boolean all = false;
            int posi = 0;
            for (int i = 0; i < sub_tabs.getChildCount(); i++)
            {
              //  if (v == sub_tabs.getChildAt(i))
              //  {
                  //  Intent intent = new Intent(getSherlockActivity().getApplicationContext(),Activity_Club_List.class);
                  //  intent.putExtra(PARAMS.TYPE.get_param(), i);
                  //  startActivity(intent);

               // }

                if (v == sub_tabs.getChildAt(i))
                {
                    club_sub_posi = posi = i;
                    if(i == 0)
                    {
                        all = true;
                    }
                    v.setBackgroundResource(R.drawable.round_bg_pressed);
                }
                else
                {
                    sub_tabs.getChildAt(i).setBackgroundResource(R.drawable.round_bg);
                }


            }
           // sub_tabs.getChildAt(posi).setBackgroundResource(R.drawable.round_bg);
            if(all)
            {
                for(V_Details club : C.clubs.values())
                {
                    club_list_items.add(club);

                }

            }
            else
            {
                for(V_Details club : C.clubs.values())
                {
                    if(club.get_venue_type() == posi)
                    {
                        club_list_items.add(club);
                    }
                }
            }
            Collections.sort(club_list_items, new Venue_Priority_Sorter());
            Club_Adapter adapter = new Club_Adapter(getSherlockActivity().getApplicationContext(),club_list_items,imageLoader,options);
            club_list.setAdapter(adapter);
            adapter.notifyDataSetChanged();

        }
    }
	private class Deals_SubTabClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            boolean all = false;
            int posi = 0;
            deals_list_items.clear();
            for (int i = 0; i < sub_tabs.getChildCount(); i++) {
               // if (v == sub_tabs.getChildAt(i))
             //   {
                  //  Intent intent = new Intent(getSherlockActivity().getApplicationContext(),Activity_Deals_List.class);
                 //   intent.putExtra(PARAMS.TYPE.get_param(), i);
                 //   startActivity(intent);
               // }
                if (v == sub_tabs.getChildAt(i))
                {
                     deal_sub_posi = posi = i;
                    if(i == 0)
                    {
                        all = true;
                    }
                    v.setBackgroundResource(R.drawable.round_bg_pressed);
                }
                else
                {
                    sub_tabs.getChildAt(i).setBackgroundResource(R.drawable.round_bg);
                }


            }
            // sub_tabs.getChildAt(posi).setBackgroundResource(R.drawable.round_bg);
            if(all)
            {
                for(ArrayList<Deals> d : C.deals.values())
                {
                    deals_list_items.add(d.get(0));

                }

            }
            else
            {
                for(ArrayList<Deals> d : C.deals.values())
                {
                    for(int i = 0;i < d.size();i++)
                    {
                        if(Integer.parseInt(d.get(i).get_type()) == posi)
                        {
                            deals_list_items.add(d.get(i));

                        }
                    }


                }
            }
            Collections.sort(deals_list_items, new Deals_Priority_Sorter());
            Deals_Adapter adapter = new Deals_Adapter(getSherlockActivity().getApplicationContext(),deals_list_items,imageLoader,options);
            deals_list.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }
	private class Artist_SubTabClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            boolean all = false;
            int posi = 0;
            artist_list_items.clear();
            for (int i = 0; i < sub_tabs.getChildCount(); i++) {
              //  if (v == sub_tabs.getChildAt(i))
             //   {
             //       Intent intent = new Intent(getSherlockActivity().getApplicationContext(),Activity_Artist_List.class);
              //      intent.putExtra(PARAMS.TYPE.get_param(), i);
             //       startActivity(intent);
             //   }
                if (v == sub_tabs.getChildAt(i))
                {
                   artist_sub_posi = posi = i;
                    if(i == 0)
                    {
                        all = true;
                    }
                    v.setBackgroundResource(R.drawable.round_bg_pressed);
                }
                else
                {
                    sub_tabs.getChildAt(i).setBackgroundResource(R.drawable.round_bg);
                }


            }
            // sub_tabs.getChildAt(posi).setBackgroundResource(R.drawable.round_bg);
            if(all)
            {
                for(Artist a : C.artists.values())
                {
                    artist_list_items.add(a);

                }

            }
            else {
                for(Artist a : C.artists.values())
                {
                if (a.get_genre_type().contains(",")) {
                    String[] ge = a.get_genre_type().split(",");
                    for (int i = 0; i < ge.length; i++) {
                        if (ge[i].equalsIgnoreCase(artist_tabs[posi]) || ge[i].toLowerCase().contains(artist_tabs[posi].toLowerCase())) {
                            artist_list_items.add(a);
                            break;
                        }
                    }

                } else if (a.get_genre_type().equalsIgnoreCase(artist_tabs[posi]) || a.get_genre_type().toLowerCase().contains(artist_tabs[posi].toLowerCase())) {
                    artist_list_items.add(a);
                }
            }
            }
            Collections.sort(artist_list_items, new Alpha_Sorter());
            Artist_Adapter adapter = new Artist_Adapter(getSherlockActivity().getApplicationContext(),artist_list_items,imageLoader,options);
            artist_list.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    ///////////////////////////////
    private void update_deals_page(View v)
    {
    	deals_list = (ParallaxListView)v.findViewById(R.id.list);
        set_deal_ads(v);
        Collections.sort(deals_list_items, new Deals_Priority_Sorter());
		deals_list.setAdapter(new Deals_Adapter(getSherlockActivity().getApplicationContext(),deals_list_items,this.imageLoader,this.options));
		deals_list.setOnScrollListener(new OnScrollListener(){

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				if(deals_list.getAdapter().getCount() > 3)
				deals_list.setOnTouchListener(new OnTouchListener(){

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						// TODO Auto-generated method stub
						//gestureDetector.onTouchEvent(event);
						return false;
					}});
			}

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
				
			}
    		
    	});
		deals_list.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				
				//if(C.add_deals_to_calendar(getSherlockActivity(), deals_list_items.get(arg2)))
				{
					//C.alert(getSherlockActivity().getApplicationContext(), "Coming soon");
				}
				
				Intent i = new Intent(getSherlockActivity().getApplicationContext(),Activity_Deals_List.class);
				i.putExtra(PARAMS.TYPE.get_param(), deals_list_items.get(arg2).get_type());
                i.putExtra(PARAMS.VD_ID.get_param(), deals_list_items.get(arg2).get_venue_id());
				
				startActivity(i);
			}
			
		});
		
		
		
		SwipeRefreshLayout refreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
		//.setOnRefreshListener(this);
	  //  setAppearance();
	    refreshLayout.setEnabled(false);
    }
    private void add_deals_subtabs()
    {
    	final View.OnClickListener subtabClickListener = new Deals_SubTabClickListener();
		for (int i = 0; i < deals_tabs.length; i++) {
            View tabView = null;
            Text tabTitleView = null;
            if (tabView == null) {
                tabView = createDefaultTabView();
            }

            if (tabTitleView == null && TextView.class.isInstance(tabView)) {
                tabTitleView = (Text) tabView;
            }
            //tabTitleView.setLayoutParams(param);
            tabTitleView.setText(deals_tabs[i]);
            tabView.setOnClickListener(subtabClickListener);
            if(i == deal_sub_posi)
            {
                tabView.setBackgroundResource(R.drawable.round_bg_pressed);
            }
            else
            {
                tabView.setBackgroundResource(R.drawable.round_bg);
            }
           // tabView.setBackgroundResource(R.color.transparent);
            sub_tabs.addView(tabView);
        }
    }

    ///////////////////////////////
    private void update_artist_page(View v)
    {
    	artist_list = (ParallaxListView)v.findViewById(R.id.list);
        set_artist_ads(v);
        artist_list_items.clear();
        if(artist_sub_posi > 0)
        {
            for(Artist a : C.artists.values())
            {
                if (a.get_genre_type().contains(",")) {
                    String[] ge = a.get_genre_type().split(",");
                    for (int i = 0; i < ge.length; i++) {
                        if (ge[i].equalsIgnoreCase(artist_tabs[artist_sub_posi]) || ge[i].toLowerCase().contains(artist_tabs[artist_sub_posi].toLowerCase())) {
                            artist_list_items.add(a);
                            break;
                        }
                    }

                } else if (a.get_genre_type().equalsIgnoreCase(artist_tabs[artist_sub_posi]) || a.get_genre_type().toLowerCase().contains(artist_tabs[artist_sub_posi].toLowerCase())) {
                    artist_list_items.add(a);
                }
            }
        }
        else
		for(Artist a : C.artists.values())
		{
			//if(App.get_city().equalsIgnoreCase(e.get_venue().get_event_city()))
			artist_list_items.add(a);
			
		}
		Collections.sort(artist_list_items, new Alpha_Sorter());
		artist_list.setAdapter(new Artist_Adapter(getSherlockActivity().getApplicationContext(),artist_list_items,imageLoader,options));
		artist_list.setOnScrollListener(new OnScrollListener(){

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				if(artist_list.getAdapter().getCount() > 3)
					artist_list.setOnTouchListener(new OnTouchListener(){

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						// TODO Auto-generated method stub
						//gestureDetector.onTouchEvent(event);
						return false;
					}});
			}

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
				
			}
    		
    	});
		artist_list.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent i = new Intent(getSherlockActivity().getApplicationContext(),Activity_Artist_Details.class);
				i.putExtra(PARAMS.ARTIST_ID.get_param(), artist_list_items.get(arg2).get_id());
				
				startActivity(i);
			}
			
		});
		
		
		
		SwipeRefreshLayout refreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
		//.setOnRefreshListener(this);
	  //  setAppearance();
	    refreshLayout.setEnabled(false);
	    
    }
    private void add_artist_subtabs()
    {
    	final View.OnClickListener subtabClickListener = new Artist_SubTabClickListener();
		for (int i = 0; i < artist_tabs.length; i++) {
            View tabView = null;
            Text tabTitleView = null;
            if (tabView == null) {
                tabView = createDefaultTabView();
            }

            if (tabTitleView == null && TextView.class.isInstance(tabView)) {
                tabTitleView = (Text) tabView;
            }
            //tabTitleView.setLayoutParams(param);
            tabTitleView.setText(artist_tabs[i]);
            tabView.setOnClickListener(subtabClickListener);
            if(i == artist_sub_posi)
            {
                tabView.setBackgroundResource(R.drawable.round_bg_pressed);
            }
            else
            {
                tabView.setBackgroundResource(R.drawable.round_bg);
            }
           // tabView.setBackgroundResource(R.color.transparent);
            sub_tabs.addView(tabView);
        }
    }
    private void set_event_ads(View v)
    {

        if(v != null)
        event_ads = (TextView)v.findViewById(R.id.ads);
        if(C.event_ads.size() > 0)
        {
            //
        }

    }
    private void set_venue_ads(View v)
    {
        if(v != null)
        venue_ads = (TextView)v.findViewById(R.id.ads);
        if(C.club_ads.size() > 0)
        {
            //
        }
    }
    private void set_deal_ads(View v)
    {
        if(v != null)
        deal_ads = (TextView)v.findViewById(R.id.ads);
        if(C.deal_ads.size() > 0)
        {
            //
        }
    }
    private void set_artist_ads(View v)
    {
        if(v != null)
        artist_ads = (TextView)v.findViewById(R.id.ads);
        if(C.artist_ads.size() > 0)
        {
            //
        }
    }
    /////////////////////////////
    private void start_sliding()
    {
    	if(handler == null)
    		handler = new Handler();

    	image_slider= new Runnable() {
            @Override
            public void run() {
                slide_image();
                handler.postDelayed(image_slider, 2000);
                
            }
        };
    	handler.post(image_slider);
      //  timer = new Timer(); // At this line a new Thread will be created
      //  timer.scheduleAtFixedRate(new RemindTask(), 0, 5000);

    }

    private void slide_image()
	{


   if(e_slider != null)
            if(event_current_item < event_baner_images.size())
            {
                e_slider.removeAllViews();
                if(e_slide_imgs.get(event_current_item).getParent() != null)
               ((ViewGroup)e_slide_imgs.get(event_current_item).getParent()).removeView(e_slide_imgs.get(event_current_item));
                e_slider.addView(e_slide_imgs.get(event_current_item));
                e_slide_imgs.get(event_current_item).invalidate();
                e_slide_imgs.get(event_current_item).getRootView().invalidate();
                if(event_slider_indicator != null)
                event_slider_indicator.setCurrentItem(event_current_item);
                event_current_item++;
                if(event_current_item >= event_baner_images.size())
                {
                    event_current_item = 0;
                }
            }
        if(c_slider != null)
            if(club_current_item < club_baner_images.size())
            {
                c_slider.removeAllViews();
                 if(c_slide_imgs.get(club_current_item).getParent() != null)
                   ((ViewGroup)c_slide_imgs.get(club_current_item).getParent()).removeView(c_slide_imgs.get(club_current_item));
                c_slider.addView(c_slide_imgs.get(club_current_item));
                c_slide_imgs.get(club_current_item).invalidate();
                c_slide_imgs.get(club_current_item).getRootView().invalidate();
                if(club_slider_indicator != null)
                    club_slider_indicator.setCurrentItem(club_current_item);
                club_current_item++;
                if(club_current_item >= club_baner_images.size())
                {
                    club_current_item = 0;
                }
            }

	}

    //////////////////////////////update list items
    private void update_list()
    {

        if(C.full_ads.size() > 0)
        {
          //  imageLoader.displayImage(C.ads.get(0).getUrl(),event_ads,options);
          //  imageLoader.displayImage(C.ads.get(1).getUrl(),venue_ads,options);
          //  imageLoader.displayImage(C.ads.get(2).getUrl(),deal_ads,options);
          //  imageLoader.displayImage(C.ads.get(3).getUrl(),artist_ads,options);
            imageLoader.displayImage(C.full_ads.get(0).getUrl(),full_ad,options);
        }


        event_list_items.clear();
		for(Event e : C.items.values())
		{
			event_list_items.add(e);
			
		}
		Collections.sort(event_list_items, new Event_Priority_Sorter());

        if(event_baner_images.size() > 0)
        {
            event_baner_images.clear();
            e_slide_imgs.clear();
        }
        if(event_baner_images.size() == 0)
        {
            for (int i = 0; i < 4; i++) {
                if (event_list_items.size() > i) {
                    event_baner_images.add(event_list_items.get(i));
                    e_slide_imgs.add(create_event_slide(event_list_items.get(i)));
                }
            }
        }
		
		club_list_items.clear();
        if(club_sub_posi == 0)
        {
            for(V_Details club : C.clubs.values())
            {
                club_list_items.add(club);

            }

        }
        else
        {
            for(V_Details club : C.clubs.values())
            {
                if(club.get_venue_type() == club_sub_posi)
                {
                    club_list_items.add(club);
                }
            }
        }
		Collections.sort(club_list_items, new Venue_Priority_Sorter());

        if(club_baner_images.size() > 0)
        {
            club_baner_images.clear();
            c_slide_imgs.clear();
        }
        if(club_baner_images.size() == 0)
        {
            for (int i = 0; i < 4; i++) {
                if (club_list_items.size() > i) {
                    club_baner_images.add(club_list_items.get(i));
                    c_slide_imgs.add(create_club_slide(club_list_items.get(i)));

                }
            }
        }

		deals_list_items.clear();
        if(deal_sub_posi == 0)
        {
            for(ArrayList<Deals> d : C.deals.values())
            {
                deals_list_items.add(d.get(0));

            }

        }
        else
        {
            for(ArrayList<Deals> d : C.deals.values())
            {
                for(int i = 0;i < d.size();i++)
                {
                    if(Integer.parseInt(d.get(i).get_type()) == deal_sub_posi)
                    {
                        deals_list_items.add(d.get(i));

                    }
                }


            }
        }
        Collections.sort(deals_list_items, new Deals_Priority_Sorter());


        artist_list_items.clear();
        if(artist_sub_posi > 0)
        {
            for(Artist a : C.artists.values())
            {
                if (a.get_genre_type().contains(",")) {
                    String[] ge = a.get_genre_type().split(",");
                    for (int i = 0; i < ge.length; i++) {
                        if (ge[i].equalsIgnoreCase(artist_tabs[artist_sub_posi]) || ge[i].toLowerCase().contains(artist_tabs[artist_sub_posi].toLowerCase())) {
                            artist_list_items.add(a);
                            break;
                        }
                    }

                } else if (a.get_genre_type().equalsIgnoreCase(artist_tabs[artist_sub_posi]) || a.get_genre_type().toLowerCase().contains(artist_tabs[artist_sub_posi].toLowerCase())) {
                    artist_list_items.add(a);
                }
            }
        }
        else
            for(Artist a : C.artists.values())
            {
                artist_list_items.add(a);

            }
        Collections.sort(artist_list_items, new Alpha_Sorter());
		
		
		if(event_list != null) {
            event_current_item = 0;
          //  ImagePagerAdapter adap = new ImagePagerAdapter(getSherlockActivity(), imageLoader, options);
         //   adap.set_event_banners(event_baner_images);
          //  event_viewPager.setAdapter(adap);
            if(event_slider_indicator != null)
                event_slider_indicator.setIndicator_count(event_baner_images.size());
         //   event_slider_indicator.setViewPager(event_viewPager);

            event_list.setAdapter(new Events_Adapter(this.getSherlockActivity().getApplicationContext(), event_list_items, this.imageLoader, this.options,app_tracker));

        }
            if(club_list != null) {
                club_current_item = 0;
             //   ImagePagerAdapter adap = new ImagePagerAdapter(getSherlockActivity(), imageLoader, options);
             //   adap.set_club_banners(club_baner_images);
             //   club_viewPager.setAdapter(adap);
               if(club_slider_indicator != null)
                    club_slider_indicator.setIndicator_count(club_baner_images.size());
                club_list.setAdapter(new Club_Adapter(this.getSherlockActivity().getApplicationContext(), club_list_items, this.imageLoader, this.options));
            }

         if(deals_list != null)
		deals_list.setAdapter(new Deals_Adapter(this.getSherlockActivity().getApplicationContext(),deals_list_items,this.imageLoader,this.options));
		if(artist_list != null)
		artist_list.setAdapter(new Artist_Adapter(this.getSherlockActivity().getApplicationContext(),artist_list_items,this.imageLoader,this.options));


    }
    
	
    /////////////////////////////////////////
    public class Get_Events extends AsyncTask<ArrayList<NameValuePair>, Long, JSONObject> {
			private Context c;
			MyProgress progress;
			public Get_Events(Context c)
			{
				this.c = c;
			}
			@Override
			protected void onPreExecute() {
				if(show_progress)
				{
                    if(getSherlockActivity() != null) {
                        progress = new MyProgress(Home_Fragment.this.getSherlockActivity());
                        progress.setCancelable(false);
                        progress.show();
                    }
					
				}

			}

			@Override
			protected JSONObject doInBackground(ArrayList<NameValuePair> ... params) {
				return get_events(new Server_Params().get_details_by_location(App.get_userid()));
			}

			

			@Override
			protected void onPostExecute(JSONObject obj) {
				
				if(progress != null)
                    progress.dismiss();
				get_event_items_from_json(obj,c);
				if(show_progress)
				{
					show_progress = false;
					if(Home_Fragment.this.getSherlockActivity() != null && progress != null)
					progress.dismiss();
				}
				
			}

			@Override
			protected void onCancelled() {
				super.onCancelled();
			}

		}
    
    public JSONObject get_events(ArrayList<NameValuePair> params)
		{
			JSONParser parser = new JSONParser();
			JSONObject user;
			user = parser.makeHttpRequest(C.SERVER_URL, "POST",params);
			
			return user;
		}
    public void get_event_items_from_json(JSONObject obj,Context c)
    {
   	 if(obj != null)
			{
   		Log.d("Home", obj.toString());
				if(obj.has(PARAMS.STATUS.get_param()))
				{
					try {
						int status = obj.getInt(PARAMS.STATUS.get_param());
						if(status > 0)
						{
							if(c != null)
							{
								new C().get_event_items_from_json(obj,c);
								new Activity_Calendar().add_calendar_events();
								Intent intent = new Intent(C.BROADCAST_MSG);
								intent.putExtra(PARAMS.VENUE_DETAILS.get_param(), " ");
								intent.putExtra(PARAMS.LOCATION.get_param(), " ");
		 						c.sendBroadcast(intent);
		 						
		 						if(this.getSherlockActivity() != null)
		 							update_list();
		 						
		 						ArrayList<String> image_ids = new ArrayList<String>();
		 						for(Event e : C.items.values())
		 						{
		 							for(int i=0;i<e.get_images().size();i++)
		 							{
		 								image_ids.add(e.get_images().get(i).get_id());
		 							}
		 							
		 						}
		 						for(V_Details v : C.clubs.values())
		 						{
		 							for(int i=0;i<v.get_images().size();i++)
		 							{
		 								image_ids.add(v.get_images().get(i).get_id());
		 							}
		 							
		 						}
		 						for(Artist a : C.artists.values())
		 						{
		 							for(int i=0;i<a.get_images().size();i++)
		 							{
		 								image_ids.add(a.get_images().get(i).get_id());
		 							}
		 							
		 						}
                                for(ArrayList<Deals> deals : C.deals.values())
                                {
                                    for(int i=0;i<deals.size();i++)
                                    {
                                        if(deals.get(i).get_image() != null)
                                        image_ids.add(deals.get(i).get_image().get_id());
                                    }

                                }
		 						new DatabaseHelper(c).update_local_image(image_ids);
							}
							
							
						}
						else
						{
							C.alert(c, "Please try again after some time");
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				else
				{
					C.alert(c, "Please try again after some time");
				}
			}
    }
    /////////////////////////////////////////
private int height;
	@Override
	public void onUpScrolling() {
		// TODO Auto-generated method stub
		/*	if (New_Home_Fragment.mSlidingTabLayout.getVisibility() == View.VISIBLE) 
		{
			MyAnimation a = new MyAnimation(
					New_Home_Fragment.mSlidingTabLayout, 100,
					MyAnimation.COLLAPSE);
			height = a.getHeight();
			New_Home_Fragment.mSlidingTabLayout.startAnimation(a);
		}
		*/
	}

	@Override
	public void onDownScrolling() {
		// TODO Auto-generated method stub
	/*	if (New_Home_Fragment.mSlidingTabLayout.getVisibility() == View.GONE) 
		{
			MyAnimation a = new MyAnimation(
					New_Home_Fragment.mSlidingTabLayout, 100, MyAnimation.EXPAND);
			a.setHeight(height);
			New_Home_Fragment.mSlidingTabLayout.startAnimation(a);
		}
		*/
	}
	
	@Override
	public void onLeftScrolling()
	{
		
	}
	@Override
	public void onRightScrolling()
	{
		
	}
////////////////////////////

private CountDownTimer mCountDownTimer;
    private void initAd() {
        // Create the InterstitialAd and set the adUnitId.
     //   full_screen_ad = new InterstitialAd(this.getSherlockActivity());
        // Defined in values/strings.xml
     //   full_screen_ad.setAdUnitId(getString(R.string.interstitial_ad_unit_id));
     //   AdRequest adRequest = new AdRequest.Builder().build();
     //   full_screen_ad.loadAd(adRequest);
        initTimer();
        mCountDownTimer.start();
    }
    private void displayAd() {
        // Show the ad if it's ready. Otherwise toast and restart the game.
      /*  if (full_screen_ad != null && full_screen_ad.isLoaded()) {
            full_screen_ad.show();
        }
        else
        {
            initTimer();
            mCountDownTimer.start();
        }
        */
        full_ad_rl.setVisibility(View.VISIBLE);
    }


private void initTimer() {
    // Create the game timer, which counts down to the end of the level
    // and shows the "retry" button.
    mCountDownTimer = new CountDownTimer(4000, 1000) {
        @Override
        public void onTick(long millisUnitFinished) {

        }

        @Override
        public void onFinish() {
            displayAd();
        }
    };
}

    Timer timer;
    class RemindTask extends TimerTask {

        @Override
        public void run() {


                    slide_image();



        }
    }


    @Override
    public View makeView() {
        ImageView iView = new ImageView(this.getSherlockActivity());
        iView.setBackgroundResource(R.drawable.default_bg);
        iView.setScaleType(ImageView.ScaleType.FIT_XY);
        iView.setLayoutParams(new ImageSwitcher.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
         return iView;
    }

    public ImageView create_event_slide(final Event event)
    {
        ImageView v = new ImageView(this.getSherlockActivity());
        v.setClickable(true);
        v.setBackgroundResource(R.drawable.default_bg);//setImageBitmap(BitmapFactory.decodeResource(getResources(),
            //    R.drawable.default_bg));
        v.setScaleType(ImageView.ScaleType.FIT_XY);
        v.setLayoutParams(new ImageSwitcher.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getSherlockActivity().getApplicationContext(),Activity_Event_Details.class);
                i.putExtra(PARAMS.EVENT_ID.get_param(), event.get_event_id());
                startActivity(i);
            }
        });
        if(event.get_images().size() > 0)
        {
            //
          //  if(db.is_image_available(event.get_images().get(0).get_id()))
          //  {
          //      new Display_Image(getSherlockActivity(),event.get_images().get(0).get_id(),v).execute();

         //   }
         //   else
          //  {
                imageLoader.displayImage(event.get_images().get(0).get_image(), v, options, new ImageLoadingListener(){

                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view,
                                                FailReason failReason) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view,
                                                  Bitmap loadedImage) {
                        // TODO Auto-generated method stub
                      /*  view.invalidate();
                        view.getRootView().invalidate();
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        loadedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        byte[] b = baos.toByteArray();
                        String image = Base64.encodeToString(b, Base64.DEFAULT);

                        long id1 = db.add_images_for_optimization(event.get_images().get(0).get_id(), image);
                        if(id1 > 0)
                        {
                            Log.d("optimized image saved", "optimized image saved");
                        }
                        */
                        //	blur(((BitmapDrawable)((ImageView)view).getDrawable()).getBitmap(),holder.blur,2);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        // TODO Auto-generated method stub

                    }



                });
          //  }
        }
        v.invalidate();
        return v;
    }
    public ImageView create_club_slide(final V_Details club)
    {
        ImageView v = new ImageView(this.getSherlockActivity());
        v.setClickable(true);
        v.setBackgroundResource(R.drawable.default_bg);//setImageBitmap(BitmapFactory.decodeResource(getResources(),
           //     R.drawable.default_bg));
        v.setScaleType(ImageView.ScaleType.FIT_XY);
        v.setLayoutParams(new ImageSwitcher.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getSherlockActivity().getApplicationContext(),Activity_Club_Details.class);
                i.putExtra(PARAMS.VD_ID.get_param(), club.get_vd_id());
                startActivity(i);
            }
        });
        if(club.get_images().size() > 0)
        {
         //   if(db.is_image_available(club.get_images().get(0).get_id()))
         //   {

        //        new Display_Image(getSherlockActivity(),club.get_images().get(0).get_id(),v).execute();


        //    }
        //    else
        //    {
                imageLoader.displayImage(club.get_images().get(0).get_image(), v, options, new ImageLoadingListener(){

                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view,
                                                FailReason failReason) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view,
                                                  Bitmap loadedImage) {
                        // TODO Auto-generated method stub
                      /*  view.invalidate();
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        loadedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        byte[] b = baos.toByteArray();
                        String image = Base64.encodeToString(b, Base64.DEFAULT);

                        long id1 = db.add_images_for_optimization(club.get_images().get(0).get_id(), image);
                        if(id1 > 0)
                        {
                            Log.d("optimized image saved", "optimized image saved");
                        }
                        */
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        // TODO Auto-generated method stub

                    }

                });
         //   }
        }
        v.invalidate();
        return v;
    }
}
