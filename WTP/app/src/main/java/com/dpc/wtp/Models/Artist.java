package com.dpc.wtp.Models;

import java.util.ArrayList;

import com.dpc.wtp.Utils.C;

public class Artist {
private String db_id,id,name,place,genre,f_count,image,thumbnail,details,following,e_date,e_name,fbid,twitid,fbname,twitname;
private int stars;
private ArrayList<Images> images;
private ArrayList<Tracks> tracks;

public Artist(String artist_id,String name,String place,String details,String genre_type,String following,String follow_count,ArrayList<Images> images,int stars,String edate,String ename,String fbid,String twitid,String fbname,String twitname,ArrayList<Tracks> tracks)
{
	this.id = artist_id;
	this.name = name;
	this.genre = genre_type;
	this.place = place;
	this.following = following;
	this.images = images;
	this.details = details;
	this.f_count = follow_count;
	this.stars = stars;
	this.e_date = edate;
	this.e_name = ename;
	this.fbid = fbid;
	this.twitid = twitid;
	this.fbname = fbname;
	this.twitname = twitname;
	this.tracks = tracks;
}

public Artist(String dbid,String id,String following)
{
	this.db_id = dbid;
	this.id = id;
	this.following = following;
	
}
public String get_dbid()
{
	return this.db_id;}

public Artist() {
	// TODO Auto-generated constructor stub
}



    public void set_id(String id)
{
	this.id = id;
}

public void set_name(String name)
{
	this.name = name;
}

public void set_genre_type(String genre_type)
{
	this.genre = genre_type;
}

public void set_details(String details)
{
	this.details = details;
}

public void set_following(String follow)
{
	this.following = follow;
	
}
public void set_stars(int stars)
{
	this.stars = stars;
	
}
public void set_following_count(String count)
{
	this.f_count = count;
	
}


public void set_images(String image)
{
	this.image = image;
}


public void set_tracks(ArrayList<Tracks> tracks)
{
	this.tracks = tracks;
}

///////////////////////////

public String get_id()
{
	return id;
}

public String get_name()
{
	return name;
}

public String get_place()
{
	return place;
}

public String get_genre_type()
{
	return genre;
}

public String get_details()
{
	return details;
}


public String get_following_count()
{
	return f_count;
}
public boolean is_following()
{
	return Integer.parseInt(following) > 0 ? true : false;
}

public ArrayList<Images> get_images()
{
	return images;
}
public String get_thumbnail()
{
	return thumbnail;
}

public int get_stars()
{
	return stars;
}
public String get_fbid()
{
	return fbid;
}
public String get_twitid()
{
	return twitid;
}
public String get_fbname()
{
	return fbname;
}
public String get_twitname()
{
	return twitname;
}
public String get_event_date()
{
	if(e_date != null)
	{
		String[] date = C.get_date_as_day_month(e_date);
		return date[1]+" "+C.get_date_suffix(date[0])+","+e_name;
	}
	else
		return "No Upcoming Events";
	}
public String get_event_name()
{
	return e_name;}

public ArrayList<Tracks> get_tracks()
{
	return tracks;
}

}
