package com.dpc.wtp.tasks;

import com.dpc.wtp.R;
import com.dpc.wtp.Utils.DatabaseHelper;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Base64;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class Display_Image extends AsyncTask<Bitmap, Long, Bitmap> {
	String image = null;
	ImageView img;
	Context c;
    View blur;

	public Display_Image( Context c,String image,ImageView img) {
		this.image = image;
		this.img = img;
		this.c = c;

	}
    public Display_Image( Context c,String image,ImageView img,View blur) {
        this.image = image;
        this.img = img;
        this.c = c;
        this.blur = blur;
    }

	

	@Override
	protected Bitmap doInBackground(Bitmap... params) {
		byte[] decodedString = Base64.decode(new DatabaseHelper(c).get_local_storage_image(image), Base64.DEFAULT);
		
		return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
	}

	@Override
	protected void onPostExecute(Bitmap result) {
		img.setImageBitmap(result);
        final Animation anim_in  = AnimationUtils.loadAnimation(c, R.anim.fadein);
        anim_in.setAnimationListener(new Animation.AnimationListener() {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation) {}
        });
        img.startAnimation(anim_in);
        result = null;

	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
	}
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void blur(Bitmap bkg, View view, float radius) {
        Bitmap overlay = Bitmap.createBitmap(
                view.getMeasuredWidth(),
                view.getMeasuredHeight(),
                Bitmap.Config.RGB_565);

        Canvas canvas = new Canvas(overlay);

        canvas.drawBitmap(bkg, -view.getLeft(),
                -view.getTop(), null);

        RenderScript rs = RenderScript.create(c);

        Allocation overlayAlloc = Allocation.createFromBitmap(
                rs, overlay);

        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(
                rs, overlayAlloc.getElement());

        blur.setInput(overlayAlloc);

        blur.setRadius(radius);

        blur.forEach(overlayAlloc);

        overlayAlloc.copyTo(overlay);

        view.setBackground(new BitmapDrawable(
                c.getResources(), overlay));

        rs.destroy();
    }
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void blur_it(Bitmap bitmapOriginal,View v)
    {
        final Animation anim_in  = AnimationUtils.loadAnimation(c, R.anim.fadein);
        RenderScript rs = RenderScript.create(c);
        final Allocation input = Allocation.createFromBitmap(rs, bitmapOriginal); //use this constructor for best performance, because it uses USAGE_SHARED mode which reuses memory
        final Allocation output = Allocation.createTyped(rs, input.getType());
        final ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        script.setRadius(8f);
        script.setInput(input);
        script.forEach(output);
        output.copyTo(bitmapOriginal);
        ((ImageView)v).setImageBitmap(bitmapOriginal);

        anim_in.setAnimationListener(new Animation.AnimationListener() {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation) {}
        });
        v.startAnimation(anim_in);

    }

}