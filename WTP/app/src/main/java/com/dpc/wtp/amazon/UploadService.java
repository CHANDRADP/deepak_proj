/***
 * Copyright (c) 2012 readyState Software Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.dpc.wtp.amazon;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.dpc.wtp.Models.TextMedia;
import com.dpc.wtp.Models.UploadDetails;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.activities.Activity_Share_Chat_Content;
import com.dpc.wtp.activities.App;
import com.dpc.wtp.customs.Custom_Send_Message;
import com.dpc.wtp.tasks.Photo_Update_Task;

import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class UploadService extends IntentService
{
    public static final String UPLOAD_STATE_CHANGED_ACTION = "com.dpc.wtp.UPLOAD_STATE_CHANGED_ACTION";
    public static final String UPLOAD_COMPLETE_BROADCAST = "com.dpc.wtp.UPLOAD_COMPLETE_BROADCAST";
    public static final String UPLOAD_CANCELLED_ACTION = "com.dpc.wtp.UPLOAD_CANCELLED_ACTION";
    public static final String UPLOAD_STATE = "upload_state";
    public static final String DELETE_IMAGE = "delete";
    public static final String PERCENT_EXTRA = "percent";
    public static final String MESSAGE = "msg";
    public static boolean SHARING = false;

    public static final List<UploadDetails> share_list_queue = new ArrayList<UploadDetails>();
    private AmazonS3Client s3Client;
    private Uploader uploader;
    int messagetype = 0;
    int uploadChatId = 0;

    DatabaseHelper db;



    public UploadService()
    {
        super("com.dpc.selfie");

    }



    @Override
    public void onCreate()
    {
        super.onCreate();
        s3Client = new AmazonS3Client(new BasicAWSCredentials(C.S3_AK, C.S3_SK));
        IntentFilter f = new IntentFilter();
        db = new DatabaseHelper(getApplicationContext());
        f.addAction(UPLOAD_CANCELLED_ACTION);
        registerReceiver(uploadCancelReceiver, f);
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        broadcastState(0, "Uploading...");
        String image = null;
        String audio = null;
        String video = null;
        int chatimage = 0;
        int chataudio = 0;

        if (intent.hasExtra("ChatImages"))
        {
            if (intent.getIntExtra("ChatImages",0)==1)
            {
                chatimage = 1;
            }
        }
        if (intent.hasExtra("ChatAudio"))
        {
            if (intent.getIntExtra("ChatAudio",0)==1)
            {
                chataudio = 1;
            }
        }
        if (intent.hasExtra("messagetype"))
        {
            messagetype = intent.getIntExtra("messagetype",0);
        }
        if (intent.hasExtra("uploadChatId"))
        {
            uploadChatId = intent.getIntExtra("uploadChatId",0);
        }
        if (intent.hasExtra("sharing"))
        {
            SHARING = intent.getBooleanExtra("sharing",false);

        }





        if(SHARING)
        {
            boolean exist = false;
            int si = share_list_queue.size();
            for(int p=0;p<share_list_queue.size();p++)
            {

                UploadDetails data = share_list_queue.get(p);
                int uploadChatId = 0;
                String messageContenttype = null;
                String textmsg = null, serverurl=null;
                int  mediatype=0, uploadchatId=0,is_groupMsg = 0, to_userid = 0;
                //textmsg = data.getTextmsg();
                mediatype = data.getMediatype();
                uploadchatId = data.getRowId();
                serverurl = data.getServerurl();
                is_groupMsg = data.getIsgroup();
                to_userid = data.getTo();

                if (mediatype==1)
                {
                    messageContenttype = "message";
                }
                else if (mediatype==2)
                {
                    messageContenttype = "image";
                }
                else if (mediatype==3)
                {
                    messageContenttype = "audio";
                }
                else if (mediatype==4)
                {
                    messageContenttype = "video";
                }
                else if (mediatype==5)
                {
                    messageContenttype = "sticker";
                }

                try
                {
                    URL url = new URL(serverurl);
                    URLConnection urlConnection = url.openConnection();

                    HttpURLConnection.setFollowRedirects(false);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) urlConnection;
                    httpURLConnection.setRequestMethod("HEAD");

                    if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK)
                    {
                        System.out.println("URL Exist");
                        exist = true;
                    }
                }
                catch(UnknownHostException unknownHostException)
                {
                    System.out.println("UnkownHost");
                    System.out.println(unknownHostException);
                }
                catch (Exception e)
                {
                    System.out.println(e);
                }

                if(exist) // if url exist
                {
                    //send message

                    Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),to_userid,serverurl, mediatype);
                    if(is_groupMsg==1)
                    {
                        csm.send_group_message();
                    }
                    else
                    {
                        csm.send_simple_text_message(Integer.toString(uploadchatId));
                    }

                    if(C.ACTIVITY_CHAT_WINDOW_RUNNING_STATUS==C.ACTIVITY_RUNNING)
                    {
                        broadcastUploadDetails(serverurl,uploadchatId, mediatype, SHARING); // here control is going out
                    }
                    else
                    {
                        db.update_status_to_uploaded_with_server_url(Integer.toString(uploadChatId),serverurl);
                        db.update_msg_status_to_sent(Integer.toString(uploadChatId));
                    }



                }
                else
                {

                    //repeated all code below in else section
                    if (intent.hasExtra(C.PARAMS.IMAGE.get_param()))
                    {
                        //If Image is not present on server
                        image = intent.getStringExtra(C.PARAMS.IMAGE.get_param());
                        if (image != null)
                        {
                            image = C.get_compressed_image(image);
                            if (image == null)
                            {
                                broadcastState(-1, "Error in retrieving image");

                            }
                            File IMG_ToUpload = new File(image);
                            String s3BucketName = C.S3_BUCKET;

                            // create a new uploader for the image
                            String s3_image_name = App.get_image();
                            if (s3_image_name == null || s3_image_name == "")
                            {
                                if (chatimage == 1)
                                {
                                    s3_image_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + "IMG_" + System.currentTimeMillis();
                                }
                                else
                                {
                                    s3_image_name = "App_Users/" + App.get_userid() + "/" + "IMG_" + System.currentTimeMillis();
                                }
                                s3_image_name += image.substring(image.lastIndexOf("."));
                                Log.d("image_url_new", s3_image_name);
                            }
                            else
                            {
                                String[] split = s3_image_name.split("/");
                                if (split != null)
                                {
                                    if (chatimage == 1)
                                    {
                                        s3_image_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + split[split.length - 1];
                                    }
                                    else
                                    {
                                        s3_image_name = "App_Users/" + App.get_userid() + "/" + split[split.length - 1];
                                    }

                                    Log.d("image_url_reuse", s3_image_name);
                                }
                                else
                                {
                                    if (chatimage == 1)
                                    {
                                        s3_image_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + "IMG_" + System.currentTimeMillis();
                                    }
                                    else
                                    {
                                        s3_image_name = "App_Users/" + App.get_userid() + "/" + "IMG_" + System.currentTimeMillis();
                                    }

                                    s3_image_name += image.substring(image.lastIndexOf("."));
                                    Log.d("image_url_spliterror", s3_image_name);
                                }
                            }


                            uploader = new Uploader(this, s3Client, s3BucketName, s3_image_name, IMG_ToUpload);


                            try
                            {
                                String s3_img_Location = uploader.start(); // initiate the image upload
                                Log.d("Image url", s3_img_Location);
                                if (s3_img_Location != null)
                                {
                                    if (IMG_ToUpload.exists())
                                    {
                                        String thumbnail = C.Create_Thumbnail(image);
                                        File THUMB_ToUpload = new File(thumbnail);
                                        IMG_ToUpload.delete();

                                        // create a new uploader for the thumbnail
                                        String s3_thumb_name = App.get_thumbnail();
                                        if (s3_thumb_name == null)
                                        {
                                            if (chatimage == 1)
                                            {
                                                s3_thumb_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + "TUMB_" + System.currentTimeMillis();
                                            }
                                            else
                                            {
                                                s3_thumb_name = "App_Users/" + App.get_userid() + "/" + "TUMB_" + System.currentTimeMillis();
                                            }

                                            s3_thumb_name += image.substring(image.lastIndexOf("."));

                                        }
                                        else
                                        {
                                            String[] split = s3_thumb_name.split("/");
                                            if (split != null)
                                            {
                                                if (chatimage == 1)
                                                {
                                                    s3_thumb_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + split[split.length - 1];
                                                }
                                                else
                                                {
                                                    s3_thumb_name = "App_Users/" + App.get_userid() + "/" + split[split.length - 1];
                                                }

                                            }
                                            else
                                            {
                                                if (chatimage == 1)
                                                {
                                                    s3_thumb_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + "TUMB_" + System.currentTimeMillis();
                                                }
                                                else
                                                {
                                                    s3_thumb_name = "App_Users/" + App.get_userid() + "/" + "TUMB_" + System.currentTimeMillis();
                                                }

                                                s3_thumb_name += image.substring(image.lastIndexOf("."));
                                            }
                                        }
                                        uploader = new Uploader(this, s3Client, s3BucketName, s3_thumb_name, THUMB_ToUpload);

                                        String s3_thumb_Location = uploader.start(); // initiate the thumbnail upload
                                        if (s3_thumb_Location != null)
                                        {
                                            THUMB_ToUpload.delete();

                                            new Photo_Update_Task(this, C.S3_IMG_PREFIX + s3_image_name, C.S3_IMG_PREFIX + s3_thumb_name).execute();

                                        }

                                    }
                                }
                                if(SHARING)
                                {
                                    //send message
                                    Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),to_userid,serverurl, mediatype);
                                    if(is_groupMsg==1)
                                    {
                                        csm.send_group_message();
                                    }
                                    else
                                    {
                                        csm.send_simple_text_message(Integer.toString(uploadchatId));
                                    }
                                    db.update_status_to_uploaded_with_server_url(Integer.toString(to_userid),s3_img_Location);
                                    broadcastUploadDetails(s3_img_Location, uploadchatId, mediatype, SHARING);
                                }
                                else
                                {

                                    if(C.ACTIVITY_CHAT_WINDOW_RUNNING_STATUS==C.ACTIVITY_RUNNING)
                                    {
                                        broadcastUploadDetails(s3_img_Location, uploadChatId, mediatype, SHARING);
                                    }
                                    else
                                    {
                                        db.update_status_to_uploaded_with_server_url(Integer.toString(uploadChatId),s3_img_Location);
                                        db.update_msg_status_to_sent(Integer.toString(uploadChatId));
                                    }
                                }

                                broadcastState(1, "Profile Picture updated successfully");
                            }
                            catch (UploadIterruptedException uie)
                            {
                                broadcastState(-1, "User interrupted");
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                broadcastState(-1, "Error in uploading the image, please try again");
                            }
                        }
                        else
                        {
                            broadcastState(-1, "Error in retrieving image");
                        }


                    }


                    if (intent.hasExtra(C.PARAMS.AUDIO.get_param()))
                    {
                        audio = intent.getStringExtra(C.PARAMS.AUDIO.get_param());
                        if (audio != null)
                        {

                            if (audio == null)
                            {
                                broadcastState(-1, "Error in retrieving audio");

                            }
                            File AUDIO_ToUpload = new File(audio);
                            String s3BucketName = C.S3_BUCKET;

                            // create a new uploader for the image
                            //String s3_audio_name = App.get_audio();
                            String s3_audio_name = "Audio_"+System.currentTimeMillis()+".mp3";
                            if (s3_audio_name == null || s3_audio_name == "")
                            {
                                if (chataudio == 1)
                                {
                                    s3_audio_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + "AUDIO_" + System.currentTimeMillis();
                                }
                                else
                                {
                                    s3_audio_name = "App_Users/" + App.get_userid() + "/" + "AUDIO_" + System.currentTimeMillis();
                                }
                                s3_audio_name += audio.substring(audio.lastIndexOf("."));
                                Log.d("image_url_new", s3_audio_name);
                            }
                            else
                            {
                                String[] split = s3_audio_name.split("/");
                                if (split != null)
                                {
                                    if (chataudio == 1)
                                    {
                                        s3_audio_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + split[split.length - 1];
                                    }
                                    else
                                    {
                                        s3_audio_name = "App_Users/" + App.get_userid() + "/" + split[split.length - 1];
                                    }

                                    Log.d("image_url_reuse", s3_audio_name);
                                }
                                else
                                {
                                    if (chataudio == 1)
                                    {
                                        s3_audio_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + "AUDIO_" + System.currentTimeMillis();
                                    }
                                    else
                                    {
                                        s3_audio_name = "App_Users/" + App.get_userid() + "/" + "AUDIO_" + System.currentTimeMillis();
                                    }

                                    s3_audio_name += audio.substring(audio.lastIndexOf("."));
                                    Log.d("audio_url_spliterror", s3_audio_name);
                                }
                            }


                            uploader = new Uploader(this, s3Client, s3BucketName, s3_audio_name, AUDIO_ToUpload);


                            try
                            {
                                String s3_audio_Location = uploader.start(); // initiate the image upload
                                Log.d("Audio url", s3_audio_Location);

                                if(SHARING)
                                {
                                    //Send Message

                                    Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),to_userid,serverurl, mediatype);
                                    if(is_groupMsg==1)
                                    {
                                        csm.send_group_message();
                                    }
                                    else
                                    {
                                        csm.send_simple_text_message(Integer.toString(uploadchatId));
                                    }
                                    db.update_status_to_uploaded_with_server_url(Integer.toString(to_userid),s3_audio_Location);
                                    broadcastUploadDetails(s3_audio_Location, uploadchatId, mediatype, SHARING);
                                }
                                else
                                {

                                    if(C.ACTIVITY_CHAT_WINDOW_RUNNING_STATUS==C.ACTIVITY_RUNNING)
                                    {
                                        broadcastUploadDetails(s3_audio_Location, uploadChatId, mediatype, SHARING);
                                    }
                                    else
                                    {
                                        db.update_status_to_uploaded_with_server_url(Integer.toString(uploadChatId),s3_audio_Location);
                                        db.update_msg_status_to_sent(Integer.toString(uploadChatId));
                                    }

                                }

                                broadcastState(1, "Profile Picture updated successfully");
                            }
                            catch (UploadIterruptedException uie)
                            {
                                broadcastState(-1, "User interrupted");
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                broadcastState(-1, "Error in uploading the image, please try again");
                            }
                        }
                        else
                        {
                            broadcastState(-1, "Error in retrieving image");
                        }

                    }






                    if (intent.hasExtra(C.PARAMS.VIDEO.get_param()))
                    {
                        video = intent.getStringExtra(C.PARAMS.VIDEO.get_param());
                        if (video != null)
                        {

                            if (video == null)
                            {
                                broadcastState(-1, "Error in retrieving video");

                            }
                            File VIDEO_ToUpload = new File(video);
                            String s3BucketName = C.S3_BUCKET;


                            String s3_video_name = "Video"+System.currentTimeMillis()+".mp4";
                            if (s3_video_name == null || s3_video_name == "")
                            {
                                if (chatimage == 1)
                                {
                                    s3_video_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + "VIDEO_" + System.currentTimeMillis();
                                }
                                else
                                {
                                    s3_video_name = "App_Users/" + App.get_userid() + "/" + "VIDEO_" + System.currentTimeMillis();
                                }
                                s3_video_name += video.substring(video.lastIndexOf("."));
                                Log.d("video_url_new", s3_video_name);
                            }
                            else
                            {
                                String[] split = s3_video_name.split("/");
                                if (split != null)
                                {
                                    if (chatimage == 1)
                                    {
                                        s3_video_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + split[split.length - 1];
                                    }
                                    else
                                    {
                                        s3_video_name = "App_Users/" + App.get_userid() + "/" + split[split.length - 1];
                                    }

                                    Log.d("image_url_reuse", s3_video_name);
                                }
                                else
                                {
                                    if (chatimage == 1)
                                    {
                                        s3_video_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + "VIDEO_" + System.currentTimeMillis();
                                    }
                                    else
                                    {
                                        s3_video_name = "App_Users/" + App.get_userid() + "/" + "VIDEO_" + System.currentTimeMillis();
                                    }

                                    s3_video_name += video.substring(video.lastIndexOf("."));
                                    Log.d("video_url_spliterror", s3_video_name);
                                }
                            }


                            uploader = new Uploader(this, s3Client, s3BucketName, s3_video_name, VIDEO_ToUpload);


                            try
                            {
                                String s3_video_Location = uploader.start(); // initiate the video upload
                                Log.d("Video url", s3_video_Location);



                                if(SHARING)
                                {
                                    //send message

                                    Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),to_userid,serverurl, mediatype);
                                    if(is_groupMsg==1)
                                    {
                                        csm.send_group_message();
                                    }
                                    else
                                    {
                                        csm.send_simple_text_message(Integer.toString(uploadchatId));
                                    }
                                    db.update_status_to_uploaded_with_server_url(Integer.toString(to_userid),s3_video_Location);
                                    broadcastUploadDetails(s3_video_Location, uploadchatId, mediatype, SHARING);
                                }
                                else
                                {
                                    if(C.ACTIVITY_CHAT_WINDOW_RUNNING_STATUS==C.ACTIVITY_RUNNING)
                                    {
                                        broadcastUploadDetails(s3_video_Location, uploadChatId, mediatype, SHARING);
                                    }
                                    else
                                    {
                                        db.update_status_to_uploaded_with_server_url(Integer.toString(uploadChatId),s3_video_Location);
                                        db.update_msg_status_to_sent(Integer.toString(uploadChatId));
                                    }

                                }

                                broadcastState(1, "Profile Picture updated successfully");
                            }
                            catch (UploadIterruptedException uie)
                            {
                                broadcastState(-1, "User interrupted");
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                broadcastState(-1, "Error in uploading the video, please try again");
                            }
                        }
                        else
                        {
                            broadcastState(-1, "Error in retrieving image");
                        }

                    }

                }
            } // for loop closed
            UploadService.share_list_queue.clear();
        }
        else
        {
            //If uploading from the local storage and not sharing
            if (intent.hasExtra(C.PARAMS.IMAGE.get_param()))
            {
                //If Image is not present on server
                image = intent.getStringExtra(C.PARAMS.IMAGE.get_param());
                if (image != null)
                {
                    image = C.get_compressed_image(image);
                    if (image == null)
                    {
                        broadcastState(-1, "Error in retrieving image");
                        return;
                    }
                    File IMG_ToUpload = new File(image);
                    String s3BucketName = C.S3_BUCKET;

                    // create a new uploader for the image
                    String s3_image_name = App.get_image();
                    if (s3_image_name == null || s3_image_name == "")
                    {
                        if (chatimage == 1)
                        {
                            s3_image_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + "IMG_" + System.currentTimeMillis();
                        }
                        else
                        {
                            s3_image_name = "App_Users/" + App.get_userid() + "/" + "IMG_" + System.currentTimeMillis();
                        }
                        s3_image_name += image.substring(image.lastIndexOf("."));
                        Log.d("image_url_new", s3_image_name);
                    }
                    else
                    {
                        String[] split = s3_image_name.split("/");
                        if (split != null)
                        {
                            if (chatimage == 1)
                            {
                                s3_image_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + split[split.length - 1];
                            }
                            else
                            {
                                s3_image_name = "App_Users/" + App.get_userid() + "/" + split[split.length - 1];
                            }

                            Log.d("image_url_reuse", s3_image_name);
                        }
                        else
                        {
                            if (chatimage == 1)
                            {
                                s3_image_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + "IMG_" + System.currentTimeMillis();
                            }
                            else
                            {
                                s3_image_name = "App_Users/" + App.get_userid() + "/" + "IMG_" + System.currentTimeMillis();
                            }

                            s3_image_name += image.substring(image.lastIndexOf("."));
                            Log.d("image_url_spliterror", s3_image_name);
                        }
                    }


                    uploader = new Uploader(this, s3Client, s3BucketName, s3_image_name, IMG_ToUpload);


                    try
                    {
                        String s3_img_Location = uploader.start(); // initiate the image upload
                        Log.d("Image url", s3_img_Location);
                        if (s3_img_Location != null)
                        {
                            if (IMG_ToUpload.exists())
                            {
                                String thumbnail = C.Create_Thumbnail(image);
                                File THUMB_ToUpload = new File(thumbnail);
                                IMG_ToUpload.delete();

                                // create a new uploader for the thumbnail
                                String s3_thumb_name = App.get_thumbnail();
                                if (s3_thumb_name == null)
                                {
                                    if (chatimage == 1)
                                    {
                                        s3_thumb_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + "TUMB_" + System.currentTimeMillis();
                                    }
                                    else
                                    {
                                        s3_thumb_name = "App_Users/" + App.get_userid() + "/" + "TUMB_" + System.currentTimeMillis();
                                    }

                                    s3_thumb_name += image.substring(image.lastIndexOf("."));

                                }
                                else
                                {
                                    String[] split = s3_thumb_name.split("/");
                                    if (split != null)
                                    {
                                        if (chatimage == 1)
                                        {
                                            s3_thumb_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + split[split.length - 1];
                                        }
                                        else
                                        {
                                            s3_thumb_name = "App_Users/" + App.get_userid() + "/" + split[split.length - 1];
                                        }

                                    }
                                    else
                                    {
                                        if (chatimage == 1)
                                        {
                                            s3_thumb_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + "TUMB_" + System.currentTimeMillis();
                                        }
                                        else
                                        {
                                            s3_thumb_name = "App_Users/" + App.get_userid() + "/" + "TUMB_" + System.currentTimeMillis();
                                        }

                                        s3_thumb_name += image.substring(image.lastIndexOf("."));
                                    }
                                }
                                uploader = new Uploader(this, s3Client, s3BucketName, s3_thumb_name, THUMB_ToUpload);

                                String s3_thumb_Location = uploader.start(); // initiate the thumbnail upload
                                if (s3_thumb_Location != null)
                                {
                                    THUMB_ToUpload.delete();

                                    new Photo_Update_Task(this, C.S3_IMG_PREFIX + s3_image_name, C.S3_IMG_PREFIX + s3_thumb_name).execute();

                                }

                            }
                        }
                        if(C.ACTIVITY_CHAT_WINDOW_RUNNING_STATUS==C.ACTIVITY_RUNNING)
                        {
                            broadcastUploadDetails(s3_img_Location, uploadChatId, messagetype, SHARING);
                        }
                        else
                        {
                            db.update_status_to_uploaded_with_server_url(Integer.toString(uploadChatId),s3_img_Location);
                            db.update_msg_status_to_sent(Integer.toString(uploadChatId));
                        }


                        broadcastState(1, "Profile Picture updated successfully");
                    }
                    catch (UploadIterruptedException uie)
                    {
                        broadcastState(-1, "User interrupted");
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                        broadcastState(-1, "Error in uploading the image, please try again");
                    }
                }
                else
                {
                    broadcastState(-1, "Error in retrieving image");
                }

                return;
            }


            if (intent.hasExtra(C.PARAMS.AUDIO.get_param()))
            {
                audio = intent.getStringExtra(C.PARAMS.AUDIO.get_param());
                if (audio != null)
                {

                    if (audio == null)
                    {
                        broadcastState(-1, "Error in retrieving audio");
                        return;
                    }
                    File AUDIO_ToUpload = new File(audio);
                    String s3BucketName = C.S3_BUCKET;

                    // create a new uploader for the image
                    //String s3_audio_name = App.get_audio();
                    String s3_audio_name = "Audio_"+System.currentTimeMillis()+".mp3";
                    if (s3_audio_name == null || s3_audio_name == "")
                    {
                        if (chatimage == 1)
                        {
                            s3_audio_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + "AUDIO_" + System.currentTimeMillis();
                        }
                        else
                        {
                            s3_audio_name = "App_Users/" + App.get_userid() + "/" + "AUDIO_" + System.currentTimeMillis();
                        }
                        s3_audio_name += audio.substring(audio.lastIndexOf("."));
                        Log.d("image_url_new", s3_audio_name);
                    }
                    else
                    {
                        String[] split = s3_audio_name.split("/");
                        if (split != null)
                        {
                            if (chatimage == 1)
                            {
                                s3_audio_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + split[split.length - 1];
                            }
                            else
                            {
                                s3_audio_name = "App_Users/" + App.get_userid() + "/" + split[split.length - 1];
                            }

                            Log.d("image_url_reuse", s3_audio_name);
                        }
                        else
                        {
                            if (chatimage == 1)
                            {
                                s3_audio_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + "AUDIO_" + System.currentTimeMillis();
                            }
                            else
                            {
                                s3_audio_name = "App_Users/" + App.get_userid() + "/" + "AUDIO_" + System.currentTimeMillis();
                            }

                            s3_audio_name += audio.substring(audio.lastIndexOf("."));
                            Log.d("audio_url_spliterror", s3_audio_name);
                        }
                    }


                    uploader = new Uploader(this, s3Client, s3BucketName, s3_audio_name, AUDIO_ToUpload);


                    try
                    {
                        String s3_audio_Location = uploader.start(); // initiate the image upload
                        Log.d("Audio url", s3_audio_Location);

                        if(C.ACTIVITY_CHAT_WINDOW_RUNNING_STATUS==C.ACTIVITY_RUNNING)
                        {
                            broadcastUploadDetails(s3_audio_Location, uploadChatId, messagetype, SHARING);
                        }
                        else
                        {
                            db.update_status_to_uploaded_with_server_url(Integer.toString(uploadChatId),s3_audio_Location);
                            db.update_msg_status_to_sent(Integer.toString(uploadChatId));
                        }

                        broadcastState(1, "Profile Picture updated successfully");
                    }
                    catch (UploadIterruptedException uie)
                    {
                        broadcastState(-1, "User interrupted");
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                        broadcastState(-1, "Error in uploading the image, please try again");
                    }
                }
                else
                {
                    broadcastState(-1, "Error in retrieving image");
                }
                return;
            }






            if (intent.hasExtra(C.PARAMS.VIDEO.get_param()))
            {
                video = intent.getStringExtra(C.PARAMS.VIDEO.get_param());
                if (video != null)
                {

                    if (video == null)
                    {
                        broadcastState(-1, "Error in retrieving video");
                        return;
                    }
                    File VIDEO_ToUpload = new File(video);
                    String s3BucketName = C.S3_BUCKET;


                    String s3_video_name = "Video"+System.currentTimeMillis()+".mp4";
                    if (s3_video_name == null || s3_video_name == "")
                    {
                        if (chatimage == 1)
                        {
                            s3_video_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + "VIDEO_" + System.currentTimeMillis();
                        }
                        else
                        {
                            s3_video_name = "App_Users/" + App.get_userid() + "/" + "VIDEO_" + System.currentTimeMillis();
                        }
                        s3_video_name += video.substring(video.lastIndexOf("."));
                        Log.d("video_url_new", s3_video_name);
                    }
                    else
                    {
                        String[] split = s3_video_name.split("/");
                        if (split != null)
                        {
                            if (chatimage == 1)
                            {
                                s3_video_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + split[split.length - 1];
                            }
                            else
                            {
                                s3_video_name = "App_Users/" + App.get_userid() + "/" + split[split.length - 1];
                            }

                            Log.d("image_url_reuse", s3_video_name);
                        }
                        else
                        {
                            if (chatimage == 1)
                            {
                                s3_video_name = "App_Users/" + App.get_userid() + "/ChatMedia/" + "VIDEO_" + System.currentTimeMillis();
                            }
                            else
                            {
                                s3_video_name = "App_Users/" + App.get_userid() + "/" + "VIDEO_" + System.currentTimeMillis();
                            }

                            s3_video_name += video.substring(video.lastIndexOf("."));
                            Log.d("video_url_spliterror", s3_video_name);
                        }
                    }


                    uploader = new Uploader(this, s3Client, s3BucketName, s3_video_name, VIDEO_ToUpload);


                    try
                    {
                        String s3_video_Location = uploader.start(); // initiate the video upload
                        Log.d("Video url", s3_video_Location);


                        if(C.ACTIVITY_CHAT_WINDOW_RUNNING_STATUS==C.ACTIVITY_RUNNING)
                        {
                            broadcastUploadDetails(s3_video_Location, uploadChatId, messagetype, SHARING);
                        }
                        else
                        {
                            db.update_status_to_uploaded_with_server_url(Integer.toString(uploadChatId),s3_video_Location);
                            db.update_msg_status_to_sent(Integer.toString(uploadChatId));
                        }


                        broadcastState(1, "Profile Picture updated successfully");
                    }
                    catch (UploadIterruptedException uie)
                    {
                        broadcastState(-1, "User interrupted");
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                        broadcastState(-1, "Error in uploading the video, please try again");
                    }
                }
                else
                {
                    broadcastState(-1, "Error in retrieving image");
                }
                return;
            }

        }
    }










    @Override
    public void onDestroy()
    {
        unregisterReceiver(uploadCancelReceiver);
        super.onDestroy();
    }

    private void broadcastState(int state, String msg)
    {
        Intent intent = new Intent(UPLOAD_STATE_CHANGED_ACTION);
        Bundle b = new Bundle();
        b.putInt(UPLOAD_STATE, state);
        b.putString(MESSAGE, msg);
        intent.putExtras(b);
        sendBroadcast(intent);
    }

    private void broadcastUploadDetails(String url, int uploadChatId, int messagetype, boolean Sharing)
    {
        Intent intent = new Intent(UPLOAD_COMPLETE_BROADCAST);
        Bundle b = new Bundle();
        b.putString("uploadurl", url);
        b.putInt("messagetype", messagetype);
        b.putInt("uploadChatId", uploadChatId);
        b.putBoolean("Sharing", SHARING);
        intent.putExtras(b);
        sendBroadcast(intent);
    }





    private BroadcastReceiver uploadCancelReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if (uploader != null)
            {
                uploader.interrupt();
            }

        }

    };


}
