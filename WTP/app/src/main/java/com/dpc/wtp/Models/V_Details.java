package com.dpc.wtp.Models;

import java.util.ArrayList;

public class V_Details{
	private String db_id,vd_id,name,cuisine,address,contact_info,area,city,state,f_count,stars,details,payment,timings,rating,map;
	private int type,bar_type,is_favorite,priority = 100;
	private ArrayList<Images> images;
	private ArrayList<String> liquor_images;
	private ArrayList<Review> reviews;
	public V_Details(String vdid,int type,int bartype,String name,String map,String cuisine,String address,String contactinfo,String area,String city,String state,String f_count,int isfavorite,String stars,String rating,ArrayList<Images> images,ArrayList<String> liquorimages,ArrayList<Review> reviews, int priority,String details)
	{
		this.vd_id =vdid;
		this.type = type;
		this.bar_type = bartype;
		this.name = name;
		this.map = map;
		this.cuisine = cuisine;
		this.address = address;
		this.contact_info = contactinfo;
		this.area = area;
		this.city = city;
		this.state = state;
		this.f_count = f_count;
		this.is_favorite = isfavorite;
		this.stars = stars;
		this.rating = rating;
		this.images = images;
		this.liquor_images = liquorimages;
		this.reviews = reviews;
		this.priority = priority;
		this.details = details;
	}
	public V_Details(String dbid,String id,String stars,int type)
	{
		this.db_id = dbid;
		this.vd_id = id;
		this.stars = stars;
		this.type = type;
	}
	public String get_dbid()
	{
		return this.db_id;
	}
	public void set_f_count(String f_count)
	{
		this.f_count = f_count;
	}
	
	public void set_favorite(int favorite)
	{
		this.is_favorite = favorite;
	}
	public void set_stars(String stars)
	{
		this.stars = stars;
	}
	
	public String get_vd_id()
	{
		return vd_id;
	}
	
	public int get_venue_type()
	{
		return type;
	}
	
	public int get_bar_type()
	{
		return bar_type;
	}
	
	public String get_venue_name()
	{
		return name;
	}
	
	public String get_map()
	{
		return map;
	}
	
	public String get_cuisine()
	{
		return cuisine;
	}
	
	public String get_venue_contactinfo()
	{
		return contact_info;
	}
	
	public String get_venue_address()
	{
		return address;
	}
	
	public String get_venue_area()
	{
		return area;
	}
	public String get_event_city()
	{
		return city;
	}
	public String get_venue_state()
	{
		return state;
	}
	public String get_venue_favoritecount()
	{
		return f_count;
	}
	public int get_stars()
	{
		return Integer.parseInt(stars);
	}
	public String get_details()
	{
		return details;
	}
	public String get_payment()
	{
		return payment;
	}
	public String get_timings()
	{
		return timings;
	}
	public int get_rating()
	{
		return (int) Float.parseFloat(rating);
	}
	public void set_rating(int rating)
	{
		this.rating = String.valueOf(rating);
	}
	public ArrayList<Images> get_images()
	{
		return images;
	}
	public ArrayList<String> get_liquor_menu_images()
	{
		return this.liquor_images;
	}
	public ArrayList<Review> get_reviews()
	{
		return reviews;
	}
	public boolean is_favorite()
	{
		return is_favorite > 0 ? true : false;
	}
	public int get_priority()
	{
		return priority;
	}
	
}
