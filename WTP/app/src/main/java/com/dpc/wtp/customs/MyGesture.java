package com.dpc.wtp.customs;

import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;

import com.dpc.wtp.fragments.Home_Fragment;
import com.dpc.wtp.interfaces.OnDetectScrollListener;

public class MyGesture extends SimpleOnGestureListener {
	private int swipe_Min_Distance = 30;
	public final static int SCROLL_UP = 1;
	public final static int SCROLL_DOWN = 2;
	
	private OnDetectScrollListener scroll_listener;
	
	public MyGesture(OnDetectScrollListener scroll_listener)
	{
		this.scroll_listener = scroll_listener;
	}
	
	public void set_min_distance(int distance)
	{
		this.swipe_Min_Distance = distance;
	}
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
	//	Log.d("distance", String.valueOf(distanceY));
	//	if (distanceY > New_Home_Fragment.mSlidingTabLayout.getHeight())//this.swipe_Min_Distance)
	//	{
			if (e1.getY() > e2.getY())
			{
				scroll_listener.onUpScrolling();
				
			}
			else
			{
				scroll_listener.onDownScrolling();
				
			}
		

			
	//	}
		return super.onScroll(e1, e2, distanceX, distanceY);
	}
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
    	//final float distanceY = Math.abs(e1.getY() - e2.getY());
    	final float distanceX = Math.abs(e1.getX() - e2.getX());
    	/*if (distanceY > New_Home_Fragment.mSlidingTabLayout.getHeight()) 
    	{
			if (e1.getY() > e2.getY()) // bottom to up
			{
				scroll_listener.onUpScrolling();
				
			}
			else
			{
				scroll_listener.onDownScrolling();
				
			}

			
		}
    	*/
    	if (distanceX > Home_Fragment.mSlidingTabLayout.getHeight())
    	{
			if (e1.getX() > e2.getX()) // bottom to up
			{
				scroll_listener.onLeftScrolling();
				
			}
			else
			{
				scroll_listener.onRightScrolling();
				
			}
    	}

			
		
        return false;
    }

        @Override
    public boolean onDown(MotionEvent e) {
          return true;
    }
}