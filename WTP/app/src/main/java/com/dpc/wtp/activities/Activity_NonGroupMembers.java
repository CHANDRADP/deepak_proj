package com.dpc.wtp.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.dpc.wtp.Models.GroupDetails;
import com.dpc.wtp.Models.NewMsgCount;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.customs.Custom_Send_Message;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.services.WTP_Service;


import org.jivesoftware.smack.packet.Message;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


public class Activity_NonGroupMembers extends MyActivity {

    ListView MyChatUsersList;
    String[] Uservalues;
    Text title;
    private BroadcastReceiver broadcastReceiver;
    Button btnAdd;

    List<String> stringList;
    ArrayList<NewMsgCount> counterlist;
    ArrayList<String> selectedUsers;
    ArrayList<GroupDetails> groupdata;
    int servergroupid=0;
    String[] memberlist;

    String groupname, group_creationdate, group_admin;
    List<String> memberarraylist;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar);

        title = (Text)findViewById(android.R.id.title);

        super.onCreate(savedInstanceState);
        db = new DatabaseHelper(this);
        setContentView(R.layout.activity_non__group_members);
        MyChatUsersList = (ListView) findViewById(R.id.MyChatUsersList);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        counterlist = new ArrayList<NewMsgCount>();
        groupdata = new ArrayList<GroupDetails>();
        selectedUsers = new ArrayList<String>();
        WTP_Service.CURRENT_CHAT_USER = "@hellochat";


        Intent i = getIntent();

        if(i.hasExtra("servergroupid"))
        {
            servergroupid = i.getIntExtra("servergroupid",0);
            Log.d("Getting server group id in edit group as", Integer.toString(servergroupid));
        }


        broadcastReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                Log.d("Activity Non Group Members Broadcast Receiver got notified","");
                setUiValues();
            }
        };

        setUiValues();

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String localTime = get_current_time();
                String msgDateTime = get_universal_date(localTime);



                if(selectedUsers.size()<1)
                {
                    Toast.makeText(getApplicationContext(),"Select atleast one user", Toast.LENGTH_LONG).show();
                }
                else
                {
                    String users = selectedUsers.toString();
                    users = users.substring(1, users.length()-1);
                    db.update_group_users(users, Integer.toString(servergroupid),group_admin);
                    Custom_Send_Message csm = new Custom_Send_Message(getApplicationContext(),servergroupid,"Hi, This is a group update.", C.GROUP_MEMBERS_UPDATE);
                    csm.send_group_message();




                    finish();
                }

            }
        });


    }

    public String get_current_time()
    {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
        return sdf.format(c.getTime());
    }

    public String get_universal_date(String time)
    {
        DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm a");


        Date date = null;
        try
        {
            //Here you say to java the initial timezone. This is the secret
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = sdf.parse(time);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        //Here you set to your timezone

        sdf.setTimeZone(TimeZone.getDefault());
        time = sdf.format(date);
        return time;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }


    @Override
    public void onResume()
    {
        super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
        registerReceiver(broadcastReceiver, new IntentFilter(WTP_Service.BROADCAST_ACTION));
        setUiValues();

    }


    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;
    }

    private void setUiValues()
    {

        groupdata = db.get_group_details(Integer.toString(servergroupid));
        GroupDetails gd = groupdata.get(0);
        final String members = gd.getGroup_users();
        groupname = gd.getGroup_name();
        group_creationdate = gd.getCreation_date();
        group_admin = gd.getGroup_admin();
        selectedUsers.clear();

        memberlist = members.split(", "); // don't remove space else it only 1 name checked
        memberarraylist = Arrays.asList(memberlist);

        title.setText(groupname);



        Uservalues = new String[] { "8",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7" };

        stringList = new ArrayList<String>(Arrays.asList(Uservalues));
        for(int j=0;j<=memberarraylist.size()-1;j++)
        {

            if(stringList.contains(memberarraylist.get(j)))
            {
                selectedUsers.add(memberarraylist.get(j)); // so as to get old users at last, will append new users
                stringList.remove(memberarraylist.get(j));
            }
            Log.d("Selected Old Users", selectedUsers.toString());
        }

        counterlist.clear();
        for(int p = 0; p<=stringList.size()-1;p++)
        {
            Log.d("P value", Integer.toString(p));
            String name  = stringList.get(p).toString();


            NewMsgCount mymsgcount = new NewMsgCount(name,0, "", "",1,101);// change it to id received from server
            counterlist.add(mymsgcount);
            Log.d("counterlist", counterlist.toString());
        }
        MyChatUsersList.setAdapter(new MyAdapter(counterlist,Activity_NonGroupMembers.this));

    }

    class MyAdapter extends BaseAdapter {

        View v;
        LayoutInflater inflater;
        ViewHolder holder;
        private Context c;
        private ArrayList<NewMsgCount> mygroupusers;



        public MyAdapter(ArrayList<NewMsgCount> mCurrentList, Context mContext) {
            this.mygroupusers = mCurrentList;
            this.c = mContext;

        }


        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            if (mygroupusers != null)
                return mygroupusers.size();
            else
                return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            if (mygroupusers != null)
                return mygroupusers.get(position);
            else
                return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        private class ViewHolder {
            Text txtName;
            CheckBox customCheckBox;



        }

        @Override
        public View getView(final int arg0,final View convertView, ViewGroup arg2)
        {
            // TODO Auto-generated method stub


            final NewMsgCount temp = mygroupusers.get(arg0);

            v = convertView;

            if (v == null)
            {
                inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.create_group_single_row, null);
                holder = new ViewHolder();
                holder.txtName = (Text) v.findViewById(R.id.txtName);
                holder.customCheckBox = (CheckBox) v.findViewById(R.id.customCheckBox);

                v.setTag(holder);

            }
            else
            {
                holder = (ViewHolder) v.getTag();
            }


            holder.txtName.setText(temp.getUsername());
            holder.customCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked)
                    {
                        selectedUsers.add(temp.getUsername());
                    }
                    else
                    {
                        selectedUsers.remove(temp.getUsername());
                    }

                    Log.d("Selected users", selectedUsers.toString());
                }
            });




            return v;
        }


    }




}
