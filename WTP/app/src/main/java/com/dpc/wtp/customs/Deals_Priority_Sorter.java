package com.dpc.wtp.customs;

import java.util.Comparator;

import com.dpc.wtp.Models.Deals;

public class Deals_Priority_Sorter implements Comparator<Deals> {
    public int compare(Deals c1, Deals c2) {
    	String left = null,right = null;
		left = c1.get_type();
		right = c2.get_type();
    	 
        return left.compareToIgnoreCase(right);
    }
}