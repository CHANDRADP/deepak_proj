package com.dpc.wtp.customs;

import java.util.Comparator;

import com.dpc.wtp.Models.V_Details;

public class Venue_Priority_Sorter implements Comparator<V_Details> {
    public int compare(V_Details c1, V_Details c2) {
    	 
        return c1.get_priority() - c2.get_priority();
    }
}