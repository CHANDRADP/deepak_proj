package com.dpc.wtp.customs;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;

import com.dpc.wtp.R;

public class MyProgress extends ProgressDialog {
	AnimationDrawable  animation;
    public MyProgress(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
    protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.progress);
      ImageView la = (ImageView) findViewById(R.id.animation);
      la.setBackgroundResource(R.drawable.progress);
      animation = (AnimationDrawable) la.getBackground();
     
    }
	@Override
	public void show() {
	  super.show();
	  animation.start();
	}

	@Override
	public void dismiss() {
	  super.dismiss();
	  if(animation != null)
	  animation.stop();
	}
}