package com.dpc.wtp.Models;

public class NewMsgCount
{
    private String username, lastmsg, time;
    private int count, isgroup, groupid;

    public NewMsgCount(String username, int count, String lastmsg, String time, int isgroup, int groupid) {
        this.username = username;
        this.count = count;
        this.lastmsg = lastmsg;
        this.time = time;
        this.isgroup = isgroup;
        this.groupid = groupid;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getIsgroup() {
        return isgroup;
    }

    public void setIsgroup(int isgroup) {
        this.isgroup = isgroup;
    }

    public int getGroupid() {
        return groupid;
    }

    public void setGroupid(int groupid) {
        this.groupid = groupid;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLastmsg() {
        return lastmsg;
    }

    public void setLastmsg(String lastmsg) {
        this.lastmsg = lastmsg;
    }



    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
