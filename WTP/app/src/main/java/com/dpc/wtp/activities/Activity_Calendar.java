package com.dpc.wtp.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.actionbarsherlock.view.MenuItem;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.R;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.Text;
import com.dpc.wtp.customs.Event_Priority_Sorter;
import com.dpc.wtp.customs.MyActivity;
import com.dpc.wtp.fragments.Calendar_Fragment;
import com.dpc.wtp.services.WTP_Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;


public class Activity_Calendar extends MyActivity {

    WTP_Service wtp_service;

    @Override
    protected void onResume() {
        super.onResume();
        C.MY_ONLINE_STATUS = C.ME_ONLINE;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.actionbar);
		final Text title = (Text)findViewById(android.R.id.title);
		title.setText(R.string.ac_calendar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_calender);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Calendar_Fragment fragment = new Calendar_Fragment();
        transaction.replace(R.id.sample_content_fragment, fragment);
        transaction.commit();

        wtp_service = new WTP_Service();
        wtp_service.create_connection(getApplicationContext());

    }

    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId())
		{
	
		case android.R.id.home:
			super.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        C.MY_ONLINE_STATUS = C.ME_OFFLINE;
    }

    public void add_calendar_events()
    {

        if(App.get_city() != null)
            if(C.items.size() > 0)
            {
                ArrayList<Event> events = new ArrayList<Event>(C.items.values());;
                Collections.sort(events, new Sort_Date());

                C.events.clear();
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                ArrayList<Event> event = new ArrayList<Event>();
                for(int i = 0;i < events.size();i++)
                {
                    try {
                        Date d0 = format.parse(events.get(i).get_event_date());
                        Date d1;
                        if(i < events.size() - 1)
                        {
                            d1 = format.parse(events.get(i+1).get_event_date());
                            if(d0.compareTo(d1) != 0)
                            {
                                if(App.get_city().equalsIgnoreCase(C.clubs.get(events.get(i).get_venue_id()).get_event_city()))
                                {
                                    event.add(events.get(i));
                                    C.events.add(event);
                                    Collections.sort(event, new Event_Priority_Sorter());
                                    event = new ArrayList<Event>();
                                }
                            }
                            else
                            {
                                if(App.get_city() != null)
                                    if(App.get_city().equalsIgnoreCase(C.clubs.get(events.get(i).get_venue_id()).get_event_city()))
                                        event.add(events.get(i));
                            }
                        }
                        else
                        {
                            if(App.get_city().equalsIgnoreCase(C.clubs.get(events.get(i).get_venue_id()).get_event_city()))
                            {
                                event.add(events.get(i));
                                C.events.add(event);
                                Collections.sort(event, new Event_Priority_Sorter());

                            }
                        }

                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

            }
    }

    public class Sort_Date implements Comparator<Event> {
        public int compare(Event c1, Event c2) {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            Date left = null,right = null;
            try {
                left = format.parse(c1.get_event_date());
                right = format.parse(c2.get_event_date());
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return left.compareTo(right);
        }
    }
}