package com.dpc.wtp.Models;

public class Review {
private String db_id,id,uid,name,review,time;

public Review(String id,String uid,String name,String review,String time)
{
	this.id = id;
	this.uid = uid;
	this.name = name;
	this.review = review;
	this.time = time;
	}
public Review(String dbid,String id,String review,String time)
{
	this.db_id = dbid;
	this.id = id;
	this.review = review;
	this.time = time;
}
public String get_dbid(){
	return this.db_id;
}
public void set_id(String id)
{
	this.id = id;}

public void set_uid(String uid)
{
	this.uid = uid;}
public void set_name(String name)
{
	this.name = name;}
public String get_id()
{
	return id;}
public String get_uid()
{
	return uid;}
public String get_name()
{
	return name;}
public String get_review()
{
	return review;}
public String get_time()
{
	return time;
	}
}

