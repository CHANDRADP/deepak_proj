package com.dpc.wtp.tasks;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;

import com.dpc.wtp.JSONParser;
import com.dpc.wtp.Models.Event;
import com.dpc.wtp.Models.Guestlist;
import com.dpc.wtp.Models.Server_Params;
import com.dpc.wtp.Utils.C;
import com.dpc.wtp.Utils.C.PARAMS;
import com.dpc.wtp.Utils.DatabaseHelper;
import com.dpc.wtp.activities.App;
import com.dpc.wtp.customs.MyProgress;

import java.util.ArrayList;

public class Add_Guest extends AsyncTask<String, Long, Boolean> {
	private MyProgress progress;
	private String vdid,event_id,guest_id;
	Activity c;

	public Add_Guest(Activity c,String vdid,String event_id) {
		this.c = c;
		this.vdid = vdid;
		this.event_id = event_id;
		progress = new MyProgress(c);
		
	}

	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		progress.show();
	}


	@Override
	protected Boolean doInBackground(String... params) {
		
		return add_guest();
	}

	@Override
	protected void onPostExecute(Boolean result) {
		
		progress.dismiss();
		if (result) 
		{
				if(guest_id != null) {
                    ArrayList<Guestlist> list = new ArrayList<>();
                    list.add(new Guestlist(C.items.get(event_id).get_event_name(), guest_id, event_id, C.items.get(event_id).get_event_date(), null));
                    new DatabaseHelper(c).update_guestlist(list);
                    Intent i = new Intent(C.BROADCAST_MSG);
                    C.alert(c, "Your request has been processed");
                    i.putExtra(PARAMS.EVENT_ID.get_param(), " ");
                    c.sendBroadcast(i);
                }
			
		}
		else
		{
			C.alert(c, "Error in adding to guest list");
		}

	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
	}
	
	public boolean add_guest()
	{
		boolean success = false;
		JSONParser parser = new JSONParser();
		JSONObject user = null;
		try {
		
		user = parser.makeHttpRequest(C.SERVER_URL, "POST",
				new Server_Params().get_add_guest_params(App.get_userid(),vdid, event_id));
			if(user.getInt(PARAMS.STATUS.get_param()) > 0)
			{
				guest_id = user.getString(PARAMS.STATUS.get_param());
				Event e = C.items.get(event_id);
                if(e != null) {
                    e.set_guest_id(guest_id);
                    C.update_items(e);
                }
				success = true;
			}
		
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return success;
	}

}